<?php

namespace App;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\BankAc;

class Computations
{

    /**
     * method to calculate Debtor Collection Period
     * ------------------------
     * FORMULA
     * ------------------------
     * (Trade Debtors/Sales X 365)
     * @return int
     */
    public function debtorCollectionPeriod(){
        //get trade account for debtors
        //Trade Control account = 12
        $debtors = AccountGroup::find(12);
        $debtorsTotal = 0;

        //get sales account
        //Sales Account = 26
        $sales = Ledger::find(37);
        $salesTotal = 0;

        if($debtors->ledgers->count()){
            foreach ($debtors->ledgers()->get() as $ledger) {
                $result = $ledger->getBalance(true);
                formatNumber($result['balance']);
                $debtorsTotal = bcadd($result['balance'], $debtorsTotal, 2);
            }
        }


        $result = $sales->getBalance(true);
        formatNumber($result['balance']);
        $salesTotal = bcadd($result['balance'], $salesTotal, 2);

        //return 0;

        if($debtorsTotal === 0 || $salesTotal === 0 ){
            return 0;
        } else {
            if ( $salesTotal <= 0 ) {
                return 0;
            } else {
                $ratio = bcdiv($debtorsTotal, $salesTotal, 2);
                $no_of_days = bcmul($ratio, 365);
                return round($no_of_days);
            }
        }

    }

    /**
     * method to calculate Creditor Payment Period
     * ------------------------
     * FORMULA
     * ------------------------
     * (Creditors/Purchases X 365)
     * @return int
     */
    public function creditorPaymentPeriod(){
        //get payables account for creditors
        //Payable Control account = 28
        $payables = AccountGroup::find(28);
        $totalPayables = 0;

        //get sales account
        //Purchase Account = 42
        $purchases = Ledger::find(42);
        $totalPurchases = 0;

        if($payables->ledgers->count()){
            foreach ($payables->ledgers()->get() as $ledger) {
                $result = $ledger->getBalance(true);
                formatNumber($result['balance']);
                $totalPayables = bcadd($result['balance'], $totalPayables, 2);
            }
        }


        $result = $purchases->getBalance(true);
        formatNumber($result['balance']);
        $totalPurchases = bcadd($result['balance'], $totalPurchases, 2);

        //return 0;
         if($totalPayables === 0 || $totalPurchases === 0 ){
             return 0;
         }else {

            if ( $totalPurchases <= 0 ) {
                return 0;
            } else {
                $ratio = bcdiv($totalPayables, $totalPurchases, 2);
                $no_of_days = bcmul($ratio, 365, 2);
                return round($no_of_days);
            }
         }

    }

    /**
     * method to calculate Current Ratio
     * ------------------------
     * FORMULA
     * ------------------------
     * (Current Asset/Current Liabilities)
     * @return int
     */
    public function currentRatio(){
        //where current assets id = 9
        $currentAssets = AccountGroup::find(9);
        $totalCurrentAssets = AccountGroup::getAccountBalance($currentAssets);

        //current liabilities id = 20
        $currentLiabilities = AccountGroup::find(27);
        $totalCurrentLiabilities = AccountGroup::getAccountBalance($currentLiabilities);

        if($totalCurrentAssets === 0 || $totalCurrentLiabilities === 0 ){
            return 0;
        }else {
            if ( $totalCurrentLiabilities <= 0 ) {
                return 0;
            } else {
                return bcdiv($totalCurrentAssets, $totalCurrentLiabilities, 2);
            }
        }
    }

    /**
     * method to calculate Acid Test Ratio
     * ------------------------
     * FORMULA
     * ------------------------
     * (Current Asset - (stock and payment))/Current Liabilities
     * @return int
     */
    public function acidTestRatio(){
        //where current asset id = 9
        $currentAsset = AccountGroup::find(9);
        $totalCurrentAsset = AccountGroup::getAccountBalance($currentAsset);

        //where current liabilities id = 20
        $currentLiabilities = AccountGroup::find(27);
        $totalCurrentLiabilities = AccountGroup::getAccountBalance($currentLiabilities);

        //where prepayment id = 15
        $prepayment = AccountGroup::find(19);
        $totalPrepayment = AccountGroup::getAccountBalance($prepayment);

        //where stock id = 16
        $stock = AccountGroup::find(10);
        $totalStock = AccountGroup::getAccountBalance($stock);
        //add stock and prepayment
        $n1 = bcadd($totalStock, $totalPrepayment, 2);
        //subtract stock and prepayment from current liabilities
        $n2 = bcsub($totalCurrentAsset, $n1, 2);

        if($n2 === 0 || $totalCurrentLiabilities === 0){
            return 0;
        }else{

            if ( $totalCurrentLiabilities <= 0 ) {
                return 0;
            } else {
                return bcdiv($n2, $totalCurrentLiabilities, 2);
            }
        }
    }

    /**
     * method to calculate Acid Test Ratio
     * ------------------------
     * FORMULA
     * ------------------------
     * @return int
     */
    
    public function getBankBalances()
    {
        $banks = BankAc::all();
        return $banks;
    }

    public function getAccruals( $accruals, $year )
    {
      $accrualsTotal = 0;
      foreach( $accruals->groups as $group ) {
        $result = $subs->getBalance(null, null, $year, true);

        if($result['dc'] === 'D') {
          $accrualsTotal = bcsub($accrualsTotal, $result['balance'], 2);
        } elseif($result['dc'] === 'C') {
          $accrualsTotal = bcadd($result['balance'], $accrualsTotal, 2);
        }

        if( $nestedGroup = $group->nestedGroups($group->id) ) {
          foreach($nestedGroup->orderBy('code', 'asc')->get() as $child) {
            $result = $child->getBalance(null, null, $year, true);

            if($result['dc'] === 'D') {
              $accrualsTotal = bcsub($accrualsTotal, $result['balance'], 2);
            } elseif($result['dc'] === 'C') {
              $accrualsTotal = bcadd($result['balance'], $accrualsTotal, 2);
            }

            if( $subChild = $child->nestedGroups($child->id) ) {
              foreach($subChild->orderBy('code', 'asc')->get() as $subs) {
                $result = $subs->getBalance(null, null, $year, true);

                if($result['dc'] === 'D') {
                  $accrualsTotal = bcsub($accrualsTotal, $result['balance'], 2);
                } elseif($result['dc'] === 'C') {
                  $accrualsTotal = bcadd($result['balance'], $accrualsTotal, 2);
                }
              }
            }
          }
        }
      }

      if($accruals->ledgers->count()) {
        foreach($accruals->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
          $currentYearAccrualLedgers = $ledger->getBalance(true, null, null, $year, true, $accruals->name);

          if($currentYearAccrualLedgers['dc'] === 'D') {
            $accrualsTotal = bcsub($accrualsTotal, $currentYearAccrualLedgers['balance'], 2);
          } elseif($currentYearAccrualLedgers['dc'] === 'C') {
            $accrualsTotal = bcadd($currentYearAccrualLedgers['balance'], $accrualsTotal, 2);
          }
        }
      }
      return $accrualsTotal;
    }
}