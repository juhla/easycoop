<?php

namespace App\Console;

use App\Modules\Base\Console\Commands;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        //Commands\IsAccountVerified::class,
        //Commands\IsUserActive::class,
        //Commands\IsUserInActive::class,
        //Commands\OldInActiveUser::class,

        //Commands\ImsCommand::class,
        //Commands\BmacCommand::class,
        //Commands\PayrollCommand::class,
        //Commands\SendAssemblyLineCharge::class,
        //Commands\DatabaseCreateCommand::class,
        // Commands\UpdateUserLicense::class,
        //Commands\SqlDump::class,
//        Commands\TransactionListingReport::class,
//        Commands\RunQueue::class,
//        Commands\PatchDB::class,
//        Commands\MenuCommand::class,


        //Commands\cronThirtyDaysEmailNofication::class,
        // Commands\cronTwentyDaysEmailNotification::class,
        //Commands\cronTenEmailNotification::class,
        // Commands\cronCheckupMail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('user:IsAccountVerified')->hourly(); //needs to run every hour
        $schedule->command('user:IsUserInActive')->daily()->at('12:15');
        $schedule->command('user:IsUserActive')->dailyAt('9:15');

        //$schedule->command('user:ImsCommand')->daily()->at('12:00');
        $schedule->command('user:BmacCommand')->daily()->at('12:00');
        $schedule->command('user:PayrollCommand')->daily()->at('12:00');
        $schedule->command('user:AssemblyLineCharge')->daily()->at('12:00');
        //$schedule->command('sql:dump')->weekly();
        $schedule->command('sql:dump')->daily()->at('10:00');
        $schedule->command('run:queue')->everyMinute()->withoutOverlapping();
        $schedule->command('command:emailThirtyDays')->daily()->at('06:00');
        $schedule->command('command:twentyDaysEmailNotification')->daily()->at('06:00');
        $schedule->command('command:tenEmailNotification')->daily()->at('06:00');
        $schedule->command('command:checkupmail')->daily()->at('06:00');


        // $schedule->command('user:OldInActiveUser')->quarterly();
    }

    protected function commands()
    {

        require base_path('routes/console.php');

    }
}
