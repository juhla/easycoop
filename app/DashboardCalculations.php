<?php

namespace App;

use Carbon\Carbon;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\TransactionItem;

class DashboardCalculations
{
    /**
     *
     * Utility function. Takes an array of object with the attributes ( 'transactionType' and 'amount' ) and calculates a total
     * WHY DO WE RETURN AN ABS VALUE... CANT REALLY REMEMBER BUT I GUESS THIS HAS TO DO WITH THE FACT THAT WE DONT WANT IT TO RETURN
     * a negative value!!!
     *
     * @param [ Ledger({ 'transactionType' => 'C', 'amount' => 12000 }), Transaction({ 'transactionType' => 'D', 'amount' => 30000 })  ]
     * @return int
     */
    private function calculateBalance($items)
    {
        $b = 0;
        foreach ($items as $v) {
            if (strtoupper($v->transactionType) == 'C') {
                $b += $v->total;
            } elseif (strtoupper($v->transactionType) == 'D') {
                $b -= $v->total;
            }
        }
        return abs($b);
    }

    /**
     *
     * Calculates the totals for the AccountGroups, grouped by the 'dc' column.
     *
     * Used to calcuate periodic and cummulative amount totals.
     *
     * @param  array [ $id, $id ] ( AccountGroup IDS ), string/null $startDate, string/null $endDate, string/null $idToExclude, string/null $year, boolean $cummulative
     * @return [ Ledger({ 'transactionType' => 'C', 'amount' => 12000 }), Transaction({ 'transactionType' => 'D', 'amount' => 30000 })  ]
     */
    private function getTransactionTotalsForAccountGroups($g, $s, $e, $y, $exclude, $cummulative)
    {
        $tmp = [];
        $accountGroups = Ledger::whereIn('group_id', $g);
        if ($exclude) {
            $accountGroups = $accountGroups->where('ca_ledgers.id', '!=', $exclude);
        }
        $accountGroups = $accountGroups->join('ca_groups', 'ca_ledgers.group_id', '=', 'ca_groups.id')
            ->select('ca_ledgers.*', 'ca_groups.name as group_name')->get()->groupBy('group_name');

        foreach ($accountGroups as $accountGroupName => $ledgers) {
            $ids = $ledgers->map(function ($item, $key) {
                return $item->id;
            })->all();
            $r = TransactionItem::whereIn('ledger_id', $ids)
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id');
            if ($cummulative && $e) {
                $r = $r->where('ca_transactions.transaction_date', '<=', $e);
            } elseif ($cummulative && $y) {
                $r = $r->whereYear('ca_transactions.transaction_date', '<=', $y);
            } else {
                $r = $r->whereBetween('ca_transactions.transaction_date', [$s, $e]);
            }
            $r = $r->groupBy('ca_transaction_items.dc')
                ->selectRaw('ca_transaction_items.dc as transactionType, sum(ca_transaction_items.amount) as total')
                ->get();
            $tmp[$accountGroupName] = $r;
        }

        return $tmp;
    }

    /**
     *
     * Calculates the totals for the AccountGroups, grouped by the 'dc' column.
     *
     * @param  array [ $id, $id ] ( AccountGroup IDS ), string/null $startDate, string/null $endDate, string/null $year, string/null $idToExclude, boolean $cummulative
     * @return int
     */
    public function calculateLedgerBalanceForAccountGroups($d, $s, $e, $y, $excludedId, $cummulative)
    {
        $b = 0;
        $ts = $this->getTransactionTotalsForAccountGroups($d, $s, $e, $y, $excludedId, $cummulative);
        foreach ($ts as $k => $v) {
            $b += $this->calculateBalance($v);
        }
        return abs($b);
    }

    public function calculateAccountGroupTotal($id, $s, $e, $y, $cummulative = false)
    {
        $bal = 0;
        $profitBeforeTax = 0;
        $gs = AccountGroup::where('parent_id', $id)->select('id')->pluck('id')->all();
        $bal += $this->calculateLedgerBalanceForAccountGroups($gs, $s, $e, $y, null, $cummulative);
        if ($id === 3 && $y) {
            try {
                $profitBeforeTax = $this->calculateRetainedEarningsTemporaryFunction($y)['balance'];
            } catch (\Exception $e) {
            }
        }

        foreach ($gs as $group) {
            $nestedGroups = AccountGroup::where('parent_id', $group)->select('id')->pluck('id')->all();
            $bal += $this->calculateLedgerBalanceForAccountGroups($nestedGroups, $s, $e, $y, null, $cummulative);

            foreach ($nestedGroups as $child) {
                $nestedSubGroups = AccountGroup::where('parent_id', $child)->select('id')->pluck('id')->all();
                if ($nestedSubGroups) {
                    $bal += $this->calculateLedgerBalanceForAccountGroups($nestedSubGroups, $s, $e, $y, null, $cummulative);
                    foreach ($nestedSubGroups as $subChild) {
                        $nestedSubChildGroups = AccountGroup::where('parent_id', $subChild)->select('id')->pluck('id')->all();
                        if ($nestedSubChildGroups) {
                            $bal += $this->calculateLedgerBalanceForAccountGroups($nestedSubChildGroups, $s, $e, $y, null, $cummulative);
                        }
                    }
                }
            }
        }

        $bal += $this->calculateLedgerBalanceForAccountGroups([$id], null, null, $y, false, $cummulative);
        return $bal + $profitBeforeTax;
    }

    public function getBankBalances($groupId = 16)
    {
        $bank_balances = Ledger::where('group_id', $groupId)->with(['transactionItems'])->get();
        //dd($bank_balances);
        $bank_accounts = [];
        $total_account_balance = 0;

        foreach ($bank_balances as $b) {
            $tmp = $b->toArray();
            $_balances = TransactionItem::where('ledger_id', '=', $b->id)
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->groupBy('ca_transaction_items.dc')
                ->selectRaw('ca_transaction_items.dc as transactionType, sum(ca_transaction_items.amount) as total')
                ->get();


            $bal = $this->calculateBalance($_balances);
            $tmp['gl_account'] = null;
            $tmp['account_balance'] = $bal;
            if (array_key_exists($b->name, $bank_accounts)) {
                $bank_accounts[$b->name] += $tmp;
            } else {
                $bank_accounts[$b->name] = $tmp;
            }
            $total_account_balance += $bal;
        }
        return ['total_account_balance' => $total_account_balance, 'bank_accounts' => $bank_accounts];
    }

    public function getBankMonthBalances($groupId = 16, $month_start, $month_end)
    {
        $bank_balances = Ledger::where('group_id', $groupId)->with(['transactionItems'])->get();
        //dd($bank_balances);
        $bank_accounts = [];
        $total_account_balance = 0;

        foreach ($bank_balances as $b) {
            $tmp = $b->toArray();
            $_balances = TransactionItem::where('ledger_id', '=', $b->id)
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->groupBy('ca_transaction_items.dc')
                ->selectRaw('ca_transaction_items.dc as transactionType, sum(ca_transaction_items.amount) as total')
                ->whereBetween('ca_transactions.transaction_date', [$month_start, $month_end])
                ->get();


            $bal = $this->calculateBalance($_balances);
            $tmp['gl_account'] = null;
            $tmp['account_balance'] = $bal;
            if (array_key_exists($b->name, $bank_accounts)) {
                $bank_accounts[$b->name] += $tmp;
            } else {
                $bank_accounts[$b->name] = $tmp;
            }
            $total_account_balance += $bal;
        }
        return ['total_account_balance' => $total_account_balance, 'bank_accounts' => $bank_accounts];
    }


    public function calculateRetainedEarningsTemporaryFunction($previousYear, $startDate = null, $endDate = null)
    {
        $expenses = AccountGroup::with(['groups' => function ($q) {
            $q->where('id', '!=', 37)->orderBy('code', 'asc');
        }])->where('id', '5')->first();
        $income = AccountGroup::where('id', 35)->with(['ledgers'])->first();
        $directCost = AccountGroup::where('id', 37)->with(['ledgers'])->first();
        $otherIncome = AccountGroup::where('id', 36)->with(['ledgers'])->first();

        $income = $income->getBalance($startDate, $endDate, $previousYear, True);
        $directCost = $directCost->getBalance($startDate, $endDate, $previousYear, True);
        $otherIncome = $otherIncome->getBalance($startDate, $endDate, $previousYear, True);

        $yearToDateExp = 0;
        foreach ($expenses->groups as $group) {
            $yearToDateResult = $group->getBalance($startDate, $endDate, $previousYear, True);
            $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2);

            if ($nestedGroup = $group->nestedGroups($group->id)) {
                foreach ($nestedGroup->orderBy('code', 'asc')->get() as $child) {
                    $yearToDateResult = $child->getBalance($startDate, $endDate, $previousYear, True);
                    $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2);

                    if ($subChild = $child->nestedGroups($child->id)) {
                        foreach ($subChild->orderBy('code', 'asc')->get() as $subs) {
                            $yearToDateResult = $subs->getBalance($startDate, $endDate, $previousYear, True);
                            $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2);
                        }
                    }
                }
            }

            if ($expenses->ledgers->count()) {
                foreach ($expenses->ledgers()->where('id', '!=', 44)->orderBy('code', 'asc')->get() as $ledger) {
                    // $yeartodate = $ledger->getBalance(true, null, null, $previousYear, True, $expenses->name );
                    $yeartodate = $ledger->getBalance(true, null, null, $previousYear, True);

                    if ($yeartodate['dc'] === 'D') {
                       // $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2);
                    } elseif ($yeartodate['dc'] === 'C') {
                      //  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2);
                    } else {
                    }
                }
            }
        }

        $grossProfit = bcsub($income['balance'], $directCost['balance'], 2);
        $profitBeforeTax = ($grossProfit + $otherIncome['balance']) - $yearToDateExp;
        return ['balance' => $profitBeforeTax, 'dc' => 'C'];
    }


    public function calculateProfitabilityRatio($startDate, $endDate)
    {
        $assets = 0;
        $expenses = 0;
        $depreciation = AccountGroup::calculateAccumulatedDepreciation($startDate, $endDate);

        $ags = AccountGroup::where('parent_id', 5)->where('id', '!=', 37)->select('id')->pluck('id')->all();
        $assetSubGroups = AccountGroup::whereIn('parent_id', [6, 9])->whereNotIn('id', [7, 8])->select('id')->get();
        $tmpIDs = array_merge($ags, AccountGroup::whereIn('parent_id', $ags)->select('id')->pluck('id')->all());

        array_push($tmpIDs, '5');
        $allExpenses = $this->calculateLedgerBalanceForAccountGroups($tmpIDs, $startDate, $endDate, null, 44, false);

        $investmentTmp = $this->calculateLedgerBalanceForAccountGroups([7, 8], $startDate, $endDate, null, null, true);

        $assets = $investmentTmp - ($depreciation['ipTotal'] + $depreciation['ppeTotal']);
        $assetSubGroupIds = $assetSubGroups->map(function ($item, $key) {
            return $item->id;
        })->all();
        $assets += $this->calculateLedgerBalanceForAccountGroups($assetSubGroupIds, $startDate, $endDate, null, null, false);

        foreach ($assetSubGroups as $group) {
            $nestedGroup = $group->nestedGroups($group->id);

            foreach ($nestedGroup->orderBy('code', 'asc')->get() as $child) {
                $assets += $child->getBalance($startDate, $endDate, null, true)['balance'];
                $subChild = $child->nestedGroups($child->id);

                foreach ($subChild->orderBy('code', 'asc')->get() as $subs) {
                    $assets += $subs->getBalance($startDate, $endDate, null, true)['balance'];
                }
            }

        }

        // what if no items arer returned

        $tmp = $this->getTransactionTotalsForAccountGroups(['35', '36', '37'], $startDate, $endDate, null, null, false);
        $incomeBalance = $this->calculateBalance($tmp['Income']);
        $directCostBalance = $this->calculateBalance($tmp['Direct Cost']);
        $otherIncomeBalance = $this->calculateBalance($tmp['Other Income']);

        $grossProfit = $incomeBalance - $directCostBalance;
        $grossProfitBeforeTax = ($grossProfit + $otherIncomeBalance) - $expenses;
        $netProfit = $grossProfitBeforeTax - ($grossProfitBeforeTax * 0.32);

        try {
            $returnOnCapitalEmployed = ($grossProfitBeforeTax / $assets) * 100;
        } catch (\Exception $e) {
            $returnOnCapitalEmployed = 0;
        }
        try {
            $netProfitMargin = ($netProfit / ($incomeBalance)) * 100;
        } catch (\Exception $e) {
            $netProfitMargin = 0;
        }
        try {
            $grossProfitMargin = ($grossProfit / ($incomeBalance)) * 100;
        } catch (\Exception $e) {
            $grossProfitMargin = 0;
        }

        return [
            'NPM' => $netProfitMargin,
            'GPM' => $grossProfitMargin,
            'ROCE' => $returnOnCapitalEmployed
        ];
    }
}