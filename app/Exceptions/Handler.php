<?php

namespace App\Exceptions;

use App\Modules\Api\Traits\Errors;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

class Handler extends ExceptionHandler
{
    use Errors;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
//        AuthorizationException::class,
        HttpException::class,
//        ModelNotFoundException::class,
//        ValidationException::class,
//        AuthenticationException::class
    ];
    private $sentryID;

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */


    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $prefix = $request->is('api/*');
        $json = $request->expectsJson();
        $ajax = $request->ajax();
        $check = ($prefix == 'api');
        return $prefix
            ? $this->sendError($exception->getMessage())
            : redirect()->guest(route('login'));
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        if (config("app.env") == "production") {
            if (app()->bound('sentry') && $this->shouldReport($e)) {
                $data = [];
                if (Auth::user()) {
                    $data = [
                        'extra' => [
                            'email' => Auth::user()->email,
                            'db' => session('company_db'),
                            'display name' => Auth::user()->getNameAttribute()
                        ]
                    ];
                }

                app('sentry')->captureException($e, $data);

            }
        }
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $prefix = $request->is('api/*');
        $json = $request->expectsJson();
        $ajax = $request->ajax();
        $check = ($prefix == 'api');
        if ($prefix) {
            if ($e instanceof ValidationException) {
                $exceptions = $e->validator->errors()->all();
                return $this->response($exceptions);
            }
            if ($e instanceof MethodNotAllowedException) {
                return $this->sendError('Method Not Allowed for the action');
            }
            if ($e instanceof NotFoundHttpException) {
                return $this->sendError('url not found');
            }
            return $this->sendError($e->getMessage());
        }


        if ($e instanceof AuthenticationException) {
            flash()->error('Your are not allowed to view that page. Please try again');
            return redirect('/secure/sign-in');
        }
        if ($this->isHttpException($e)) {
            return $this->renderHttpException($e);
        }

        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($e instanceof TokenMismatchException) {
            flash()->success('Your page session expired. Please try again');
            return redirect('/secure/sign-in');
        }

        if (config("app.env") == "production") {
            if ($this->shouldReport($e) && !$this->isHttpException($e) && !config('app.debug')) {
                $e = new HttpException(500, 'Whoops!');
            }
        }
        if (config('app.debug')) {
            return $this->renderExceptionWithWhoops($e);
        }

        return parent::render($request, $e);


    }

    /**
     * Render an exception using Whoops.
     *
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    protected function renderExceptionWithWhoops(Exception $e)
    {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());

        return new \Illuminate\Http\Response(
            $whoops->handleException($e),
            $e->getStatusCode(),
            $e->getHeaders()
        );
    }
}
