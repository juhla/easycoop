<?php

namespace App\Http;

use App\Modules\Api\Http\Middleware\Tenant;
use App\Modules\Base\Http\Middleware\SentryContext;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Barryvdh\Cors\HandleCors::class,
        // \App\Modules\Base\Http\Middleware\SentryContext::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Modules\Base\Http\Middleware\EncryptCookies::class,
            // \App\Modules\Base\Http\Middleware\RedirectIfAuthenticated::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Modules\Base\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            // \App\Modules\Base\Http\Middleware\Tenant::class,
            SentryContext::class
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Modules\Base\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Modules\Base\Http\Middleware\RedirectIfAuthenticated::class,
        'after_login' => \App\Modules\Base\Http\Middleware\AfterLogin::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'role' => \Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission' => \Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability' => \Zizaco\Entrust\Middleware\EntrustAbility::class,
        'tenant' => \App\Modules\Base\Http\Middleware\Tenant::class,
        'api_tenant' => Tenant::class,
        'full_tenant' => \App\Modules\Base\Http\Middleware\FullTenant::class,
        'fiscal_year' => \App\Modules\Base\Http\Middleware\FiscalYear::class,
        'verify_influencer_agent' => \App\Modules\Base\Http\Middleware\VerifyInfluencerAgent::class,
        'verify_professional_agent' => \App\Modules\Base\Http\Middleware\VerifyProfessionalAgent::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'verify_business_owner' => \App\Modules\Base\Http\Middleware\VerifyBusinessAgent::class,
        'verify_license' => \App\Modules\Base\Http\Middleware\VerifyLicense::class,
    ];
}
