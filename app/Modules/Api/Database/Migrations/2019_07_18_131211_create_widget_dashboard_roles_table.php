<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWidgetDashboardRolesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_dashboard_roles', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->integer('widget_dashboard_id')->nullable();
            $table->integer('role_id')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('widget_dashboard_roles');
    }

}
