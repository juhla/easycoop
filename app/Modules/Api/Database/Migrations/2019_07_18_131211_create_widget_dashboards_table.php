<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWidgetDashboardsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_dashboards', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->string('method')->nullable();
            $table->string('name')->nullable();
            $table->string('partials')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('widget_dashboards');
    }

}
