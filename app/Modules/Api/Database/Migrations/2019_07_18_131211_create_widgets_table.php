<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;

class CreateWidgetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->string('name')->nullable();
            $table->string('routes')->nullable();
            $table->string('class')->nullable();
            $table->string('div_items')->nullable();
            $table->text('description', 65535)->nullable();
            $table->timestamps();
            $table->string('mode');
        });

        Artisan::call('module:seed', [
                'slug' => 'api',
                '--force' => true
            ]
        );
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('widgets');
    }

}
