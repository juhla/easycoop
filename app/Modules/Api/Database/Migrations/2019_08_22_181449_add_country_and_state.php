<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

class AddCountryAndState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('states', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->integer('country_id');
            $table->string('name');
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->bigInteger('id', true)->unsigned();
            $table->string('name');
            $table->string('country_code_long');
            $table->string('country_code');
            $table->string('dial_code');
            $table->string('time_zone');
            $table->string('region');
            $table->string('currency_name');
            $table->string('currency_code');
            $table->string('currency_symbol');
            $table->double('edu_index');
            $table->tinyInteger('status');
            $table->string('country_flag');
        });

        Artisan::call('module:seed', [
                'slug' => 'api',
                '--class' => 'App\Modules\Api\Database\Seeds\StatesTableSeeder',
                '--force' => true
            ]
        );

        Artisan::call('module:seed', [
                'slug' => 'api',
                '--class' => 'App\Modules\Api\Database\Seeds\CountriesTableSeeder',
                '--force' => true
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
        Schema::dropIfExists('countries');
    }
}
