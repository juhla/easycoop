<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-12
 * Time: 14:39
 */

namespace App\Modules\Api\Database\Seeds;


use Illuminate\Database\Seeder;

class ApiDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ChartOfAccountSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(WidgetDashboardRolesTableSeeder::class);
        $this->call(WidgetDashboardsTableSeeder::class);
        $this->call(WidgetRolesTableSeeder::class);
        $this->call(WidgetsTableSeeder::class);
    }
}