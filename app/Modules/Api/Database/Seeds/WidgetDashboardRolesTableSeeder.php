<?php

namespace App\Modules\Api\Database\Seeds;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetDashboardRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('widget_dashboard_roles')->delete();
        DB::table('widget_dashboard_roles')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'widget_dashboard_id' => 2,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            1 =>
                array(
                    'id' => 2,
                    'widget_dashboard_id' => 3,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            2 =>
                array(
                    'id' => 3,
                    'widget_dashboard_id' => 4,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            3 =>
                array(
                    'id' => 4,
                    'widget_dashboard_id' => 5,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            4 =>
                array(
                    'id' => 5,
                    'widget_dashboard_id' => 6,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            5 =>
                array(
                    'id' => 6,
                    'widget_dashboard_id' => 7,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            6 =>
                array(
                    'id' => 7,
                    'widget_dashboard_id' => 9,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            7 =>
                array(
                    'id' => 8,
                    'widget_dashboard_id' => 10,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            8 =>
                array(
                    'id' => 9,
                    'widget_dashboard_id' => 11,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            9 =>
                array(
                    'id' => 10,
                    'widget_dashboard_id' => 12,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            10 =>
                array(
                    'id' => 11,
                    'widget_dashboard_id' => 13,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            11 =>
                array(
                    'id' => 12,
                    'widget_dashboard_id' => 14,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            12 =>
                array(
                    'id' => 13,
                    'widget_dashboard_id' => 15,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            13 =>
                array(
                    'id' => 14,
                    'widget_dashboard_id' => 16,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            14 =>
                array(
                    'id' => 15,
                    'widget_dashboard_id' => 17,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            15 =>
                array(
                    'id' => 16,
                    'widget_dashboard_id' => 19,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            16 =>
                array(
                    'id' => 17,
                    'widget_dashboard_id' => 20,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            17 =>
                array(
                    'id' => 18,
                    'widget_dashboard_id' => 21,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            18 =>
                array(
                    'id' => 19,
                    'widget_dashboard_id' => 22,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            19 =>
                array(
                    'id' => 20,
                    'widget_dashboard_id' => 23,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            20 =>
                array(
                    'id' => 21,
                    'widget_dashboard_id' => 24,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            21 =>
                array(
                    'id' => 22,
                    'widget_dashboard_id' => 25,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            22 =>
                array(
                    'id' => 23,
                    'widget_dashboard_id' => 26,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            23 =>
                array(
                    'id' => 24,
                    'widget_dashboard_id' => 27,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            24 =>
                array(
                    'id' => 25,
                    'widget_dashboard_id' => 28,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            25 =>
                array(
                    'id' => 26,
                    'widget_dashboard_id' => 29,
                    'role_id' => 5,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            26 =>
                array(
                    'id' => 27,
                    'widget_dashboard_id' => 2,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            27 =>
                array(
                    'id' => 28,
                    'widget_dashboard_id' => 3,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            28 =>
                array(
                    'id' => 29,
                    'widget_dashboard_id' => 4,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            29 =>
                array(
                    'id' => 30,
                    'widget_dashboard_id' => 5,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            30 =>
                array(
                    'id' => 31,
                    'widget_dashboard_id' => 6,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            31 =>
                array(
                    'id' => 32,
                    'widget_dashboard_id' => 7,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            32 =>
                array(
                    'id' => 33,
                    'widget_dashboard_id' => 8,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            33 =>
                array(
                    'id' => 34,
                    'widget_dashboard_id' => 9,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            34 =>
                array(
                    'id' => 35,
                    'widget_dashboard_id' => 10,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            35 =>
                array(
                    'id' => 36,
                    'widget_dashboard_id' => 11,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            36 =>
                array(
                    'id' => 37,
                    'widget_dashboard_id' => 12,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            37 =>
                array(
                    'id' => 38,
                    'widget_dashboard_id' => 13,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            38 =>
                array(
                    'id' => 39,
                    'widget_dashboard_id' => 14,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            39 =>
                array(
                    'id' => 40,
                    'widget_dashboard_id' => 15,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            40 =>
                array(
                    'id' => 41,
                    'widget_dashboard_id' => 16,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            41 =>
                array(
                    'id' => 42,
                    'widget_dashboard_id' => 17,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            42 =>
                array(
                    'id' => 43,
                    'widget_dashboard_id' => 19,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            43 =>
                array(
                    'id' => 44,
                    'widget_dashboard_id' => 20,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            44 =>
                array(
                    'id' => 45,
                    'widget_dashboard_id' => 21,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            45 =>
                array(
                    'id' => 46,
                    'widget_dashboard_id' => 22,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            46 =>
                array(
                    'id' => 47,
                    'widget_dashboard_id' => 23,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            47 =>
                array(
                    'id' => 48,
                    'widget_dashboard_id' => 24,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            48 =>
                array(
                    'id' => 49,
                    'widget_dashboard_id' => 25,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            49 =>
                array(
                    'id' => 50,
                    'widget_dashboard_id' => 26,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            50 =>
                array(
                    'id' => 51,
                    'widget_dashboard_id' => 27,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            51 =>
                array(
                    'id' => 52,
                    'widget_dashboard_id' => 28,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            52 =>
                array(
                    'id' => 53,
                    'widget_dashboard_id' => 29,
                    'role_id' => 6,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            53 =>
                array(
                    'id' => 54,
                    'widget_dashboard_id' => 2,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            54 =>
                array(
                    'id' => 55,
                    'widget_dashboard_id' => 3,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            55 =>
                array(
                    'id' => 56,
                    'widget_dashboard_id' => 4,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            56 =>
                array(
                    'id' => 57,
                    'widget_dashboard_id' => 5,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            57 =>
                array(
                    'id' => 58,
                    'widget_dashboard_id' => 6,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            58 =>
                array(
                    'id' => 59,
                    'widget_dashboard_id' => 7,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            59 =>
                array(
                    'id' => 60,
                    'widget_dashboard_id' => 8,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            60 =>
                array(
                    'id' => 61,
                    'widget_dashboard_id' => 9,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            61 =>
                array(
                    'id' => 62,
                    'widget_dashboard_id' => 10,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            62 =>
                array(
                    'id' => 63,
                    'widget_dashboard_id' => 11,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            63 =>
                array(
                    'id' => 64,
                    'widget_dashboard_id' => 12,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            64 =>
                array(
                    'id' => 65,
                    'widget_dashboard_id' => 13,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            65 =>
                array(
                    'id' => 66,
                    'widget_dashboard_id' => 14,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            66 =>
                array(
                    'id' => 67,
                    'widget_dashboard_id' => 15,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            67 =>
                array(
                    'id' => 68,
                    'widget_dashboard_id' => 16,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            68 =>
                array(
                    'id' => 69,
                    'widget_dashboard_id' => 17,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            69 =>
                array(
                    'id' => 70,
                    'widget_dashboard_id' => 19,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            70 =>
                array(
                    'id' => 71,
                    'widget_dashboard_id' => 20,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            71 =>
                array(
                    'id' => 72,
                    'widget_dashboard_id' => 21,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            72 =>
                array(
                    'id' => 73,
                    'widget_dashboard_id' => 22,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            73 =>
                array(
                    'id' => 74,
                    'widget_dashboard_id' => 23,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            74 =>
                array(
                    'id' => 75,
                    'widget_dashboard_id' => 24,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            75 =>
                array(
                    'id' => 76,
                    'widget_dashboard_id' => 25,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            76 =>
                array(
                    'id' => 77,
                    'widget_dashboard_id' => 26,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            77 =>
                array(
                    'id' => 78,
                    'widget_dashboard_id' => 27,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            78 =>
                array(
                    'id' => 79,
                    'widget_dashboard_id' => 28,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            79 =>
                array(
                    'id' => 80,
                    'widget_dashboard_id' => 29,
                    'role_id' => 7,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            80 =>
                array(
                    'id' => 81,
                    'widget_dashboard_id' => 2,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            81 =>
                array(
                    'id' => 82,
                    'widget_dashboard_id' => 3,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            82 =>
                array(
                    'id' => 83,
                    'widget_dashboard_id' => 4,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            83 =>
                array(
                    'id' => 84,
                    'widget_dashboard_id' => 5,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            84 =>
                array(
                    'id' => 85,
                    'widget_dashboard_id' => 6,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            85 =>
                array(
                    'id' => 86,
                    'widget_dashboard_id' => 7,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            86 =>
                array(
                    'id' => 87,
                    'widget_dashboard_id' => 8,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            87 =>
                array(
                    'id' => 88,
                    'widget_dashboard_id' => 9,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            88 =>
                array(
                    'id' => 89,
                    'widget_dashboard_id' => 10,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            89 =>
                array(
                    'id' => 90,
                    'widget_dashboard_id' => 11,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            90 =>
                array(
                    'id' => 91,
                    'widget_dashboard_id' => 12,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            91 =>
                array(
                    'id' => 92,
                    'widget_dashboard_id' => 13,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            92 =>
                array(
                    'id' => 93,
                    'widget_dashboard_id' => 14,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            93 =>
                array(
                    'id' => 94,
                    'widget_dashboard_id' => 15,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            94 =>
                array(
                    'id' => 95,
                    'widget_dashboard_id' => 16,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            95 =>
                array(
                    'id' => 96,
                    'widget_dashboard_id' => 17,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            96 =>
                array(
                    'id' => 97,
                    'widget_dashboard_id' => 19,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            97 =>
                array(
                    'id' => 98,
                    'widget_dashboard_id' => 20,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            98 =>
                array(
                    'id' => 99,
                    'widget_dashboard_id' => 21,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            99 =>
                array(
                    'id' => 100,
                    'widget_dashboard_id' => 22,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            100 =>
                array(
                    'id' => 101,
                    'widget_dashboard_id' => 23,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            101 =>
                array(
                    'id' => 102,
                    'widget_dashboard_id' => 24,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            102 =>
                array(
                    'id' => 103,
                    'widget_dashboard_id' => 25,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            103 =>
                array(
                    'id' => 104,
                    'widget_dashboard_id' => 26,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            104 =>
                array(
                    'id' => 105,
                    'widget_dashboard_id' => 27,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            105 =>
                array(
                    'id' => 106,
                    'widget_dashboard_id' => 28,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            106 =>
                array(
                    'id' => 107,
                    'widget_dashboard_id' => 29,
                    'role_id' => 11,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            107 =>
                array(
                    'id' => 108,
                    'widget_dashboard_id' => 2,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            108 =>
                array(
                    'id' => 109,
                    'widget_dashboard_id' => 3,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            109 =>
                array(
                    'id' => 110,
                    'widget_dashboard_id' => 4,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            110 =>
                array(
                    'id' => 111,
                    'widget_dashboard_id' => 5,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            111 =>
                array(
                    'id' => 112,
                    'widget_dashboard_id' => 6,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            112 =>
                array(
                    'id' => 113,
                    'widget_dashboard_id' => 7,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            113 =>
                array(
                    'id' => 114,
                    'widget_dashboard_id' => 8,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            114 =>
                array(
                    'id' => 115,
                    'widget_dashboard_id' => 9,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            115 =>
                array(
                    'id' => 116,
                    'widget_dashboard_id' => 10,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            116 =>
                array(
                    'id' => 117,
                    'widget_dashboard_id' => 11,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            117 =>
                array(
                    'id' => 118,
                    'widget_dashboard_id' => 12,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            118 =>
                array(
                    'id' => 119,
                    'widget_dashboard_id' => 13,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            119 =>
                array(
                    'id' => 120,
                    'widget_dashboard_id' => 14,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            120 =>
                array(
                    'id' => 121,
                    'widget_dashboard_id' => 15,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            121 =>
                array(
                    'id' => 122,
                    'widget_dashboard_id' => 16,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            122 =>
                array(
                    'id' => 123,
                    'widget_dashboard_id' => 17,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            123 =>
                array(
                    'id' => 124,
                    'widget_dashboard_id' => 19,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            124 =>
                array(
                    'id' => 125,
                    'widget_dashboard_id' => 20,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            125 =>
                array(
                    'id' => 126,
                    'widget_dashboard_id' => 21,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            126 =>
                array(
                    'id' => 127,
                    'widget_dashboard_id' => 22,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            127 =>
                array(
                    'id' => 128,
                    'widget_dashboard_id' => 23,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            128 =>
                array(
                    'id' => 129,
                    'widget_dashboard_id' => 24,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            129 =>
                array(
                    'id' => 130,
                    'widget_dashboard_id' => 25,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            130 =>
                array(
                    'id' => 131,
                    'widget_dashboard_id' => 26,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            131 =>
                array(
                    'id' => 132,
                    'widget_dashboard_id' => 27,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            132 =>
                array(
                    'id' => 133,
                    'widget_dashboard_id' => 28,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            133 =>
                array(
                    'id' => 134,
                    'widget_dashboard_id' => 29,
                    'role_id' => 12,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            134 =>
                array(
                    'id' => 135,
                    'widget_dashboard_id' => 3,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            135 =>
                array(
                    'id' => 136,
                    'widget_dashboard_id' => 4,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            136 =>
                array(
                    'id' => 137,
                    'widget_dashboard_id' => 5,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            137 =>
                array(
                    'id' => 138,
                    'widget_dashboard_id' => 7,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            138 =>
                array(
                    'id' => 139,
                    'widget_dashboard_id' => 12,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            139 =>
                array(
                    'id' => 140,
                    'widget_dashboard_id' => 13,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            140 =>
                array(
                    'id' => 141,
                    'widget_dashboard_id' => 14,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            141 =>
                array(
                    'id' => 142,
                    'widget_dashboard_id' => 15,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            142 =>
                array(
                    'id' => 143,
                    'widget_dashboard_id' => 16,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            143 =>
                array(
                    'id' => 144,
                    'widget_dashboard_id' => 17,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:49:59',
                    'created_at' => '2017-05-05 21:49:59',
                ),
            144 =>
                array(
                    'id' => 145,
                    'widget_dashboard_id' => 18,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            145 =>
                array(
                    'id' => 146,
                    'widget_dashboard_id' => 19,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            146 =>
                array(
                    'id' => 147,
                    'widget_dashboard_id' => 20,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            147 =>
                array(
                    'id' => 148,
                    'widget_dashboard_id' => 21,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            148 =>
                array(
                    'id' => 149,
                    'widget_dashboard_id' => 22,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            149 =>
                array(
                    'id' => 150,
                    'widget_dashboard_id' => 23,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            150 =>
                array(
                    'id' => 151,
                    'widget_dashboard_id' => 24,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            151 =>
                array(
                    'id' => 152,
                    'widget_dashboard_id' => 26,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            152 =>
                array(
                    'id' => 153,
                    'widget_dashboard_id' => 27,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            153 =>
                array(
                    'id' => 154,
                    'widget_dashboard_id' => 28,
                    'role_id' => 4,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            154 =>
                array(
                    'id' => 155,
                    'widget_dashboard_id' => 3,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            155 =>
                array(
                    'id' => 156,
                    'widget_dashboard_id' => 4,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            156 =>
                array(
                    'id' => 157,
                    'widget_dashboard_id' => 5,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            157 =>
                array(
                    'id' => 158,
                    'widget_dashboard_id' => 6,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            158 =>
                array(
                    'id' => 159,
                    'widget_dashboard_id' => 7,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            159 =>
                array(
                    'id' => 160,
                    'widget_dashboard_id' => 9,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            160 =>
                array(
                    'id' => 161,
                    'widget_dashboard_id' => 10,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            161 =>
                array(
                    'id' => 162,
                    'widget_dashboard_id' => 12,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            162 =>
                array(
                    'id' => 163,
                    'widget_dashboard_id' => 13,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            163 =>
                array(
                    'id' => 164,
                    'widget_dashboard_id' => 14,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            164 =>
                array(
                    'id' => 165,
                    'widget_dashboard_id' => 15,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            165 =>
                array(
                    'id' => 166,
                    'widget_dashboard_id' => 16,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            166 =>
                array(
                    'id' => 167,
                    'widget_dashboard_id' => 17,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            167 =>
                array(
                    'id' => 168,
                    'widget_dashboard_id' => 19,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            168 =>
                array(
                    'id' => 169,
                    'widget_dashboard_id' => 20,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            169 =>
                array(
                    'id' => 170,
                    'widget_dashboard_id' => 21,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            170 =>
                array(
                    'id' => 171,
                    'widget_dashboard_id' => 22,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            171 =>
                array(
                    'id' => 172,
                    'widget_dashboard_id' => 23,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            172 =>
                array(
                    'id' => 173,
                    'widget_dashboard_id' => 24,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            173 =>
                array(
                    'id' => 174,
                    'widget_dashboard_id' => 25,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            174 =>
                array(
                    'id' => 175,
                    'widget_dashboard_id' => 26,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            175 =>
                array(
                    'id' => 176,
                    'widget_dashboard_id' => 27,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            176 =>
                array(
                    'id' => 177,
                    'widget_dashboard_id' => 28,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            177 =>
                array(
                    'id' => 178,
                    'widget_dashboard_id' => 29,
                    'role_id' => 8,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            178 =>
                array(
                    'id' => 179,
                    'widget_dashboard_id' => 3,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            179 =>
                array(
                    'id' => 180,
                    'widget_dashboard_id' => 4,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            180 =>
                array(
                    'id' => 181,
                    'widget_dashboard_id' => 5,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            181 =>
                array(
                    'id' => 182,
                    'widget_dashboard_id' => 6,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            182 =>
                array(
                    'id' => 183,
                    'widget_dashboard_id' => 7,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            183 =>
                array(
                    'id' => 184,
                    'widget_dashboard_id' => 9,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            184 =>
                array(
                    'id' => 185,
                    'widget_dashboard_id' => 10,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            185 =>
                array(
                    'id' => 186,
                    'widget_dashboard_id' => 12,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            186 =>
                array(
                    'id' => 187,
                    'widget_dashboard_id' => 13,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            187 =>
                array(
                    'id' => 188,
                    'widget_dashboard_id' => 14,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            188 =>
                array(
                    'id' => 189,
                    'widget_dashboard_id' => 15,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            189 =>
                array(
                    'id' => 190,
                    'widget_dashboard_id' => 16,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            190 =>
                array(
                    'id' => 191,
                    'widget_dashboard_id' => 17,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            191 =>
                array(
                    'id' => 192,
                    'widget_dashboard_id' => 18,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            192 =>
                array(
                    'id' => 193,
                    'widget_dashboard_id' => 19,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            193 =>
                array(
                    'id' => 194,
                    'widget_dashboard_id' => 20,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            194 =>
                array(
                    'id' => 195,
                    'widget_dashboard_id' => 21,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            195 =>
                array(
                    'id' => 196,
                    'widget_dashboard_id' => 22,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            196 =>
                array(
                    'id' => 197,
                    'widget_dashboard_id' => 23,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            197 =>
                array(
                    'id' => 198,
                    'widget_dashboard_id' => 24,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            198 =>
                array(
                    'id' => 199,
                    'widget_dashboard_id' => 25,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            199 =>
                array(
                    'id' => 200,
                    'widget_dashboard_id' => 26,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            200 =>
                array(
                    'id' => 201,
                    'widget_dashboard_id' => 27,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            201 =>
                array(
                    'id' => 202,
                    'widget_dashboard_id' => 28,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
            202 =>
                array(
                    'id' => 203,
                    'widget_dashboard_id' => 29,
                    'role_id' => 10,
                    'updated_at' => '2017-05-05 21:50:00',
                    'created_at' => '2017-05-05 21:50:00',
                ),
        ));


    }
}