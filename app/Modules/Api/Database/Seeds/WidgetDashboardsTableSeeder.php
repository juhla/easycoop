<?php

namespace App\Modules\Api\Database\Seeds;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetDashboardsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('widget_dashboards')->delete();

        DB::table('widget_dashboards')->insert(array(
            0 =>
                array(
                    'id' => 2,
                    'method' => NULL,
                    'name' => 'Acid Test Ratio',
                    'partials' => 'acid_test_ratio',
                    'description' => '<p>sdfsdfd<br></p>',
                    'created_at' => '2017-04-25 13:13:37',
                    'updated_at' => '2017-04-25 13:13:37',
                ),
            1 =>
                array(
                    'id' => 3,
                    'method' => NULL,
                    'name' => 'Money in the Bank (Bank Balances)',
                    'partials' => 'bank_balances',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            2 =>
                array(
                    'id' => 4,
                    'method' => NULL,
                    'name' => 'Summary state of our Business ',
                    'partials' => 'business_summary',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            3 =>
                array(
                    'id' => 5,
                    'method' => NULL,
                    'name' => 'Cash in Hand',
                    'partials' => 'cash_in_hand',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            4 =>
                array(
                    'id' => 6,
                    'method' => NULL,
                    'name' => 'Average Number of days for us to pay our suppliers (Creditors\' Payment Period)',
                    'partials' => 'creditor_payment_period',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            5 =>
                array(
                    'id' => 7,
                    'method' => NULL,
                    'name' => 'Our Major Customers by Sales ',
                    'partials' => 'customers_by_sales',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            6 =>
                array(
                    'id' => 8,
                    'method' => NULL,
                    'name' => 'Relationship between Loans & Equity (Gearing Ratio)',
                    'partials' => 'gear_ratio',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            7 =>
                array(
                    'id' => 9,
                    'method' => NULL,
                    'name' => 'Trading profit margin (Gross Profit Margin)',
                    'partials' => 'gross_profit_margin',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            8 =>
                array(
                    'id' => 10,
                    'method' => NULL,
                    'name' => 'Profit after running expenses (Net Profit margin)',
                    'partials' => 'net_profit_margin',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            9 =>
                array(
                    'id' => 11,
                    'method' => NULL,
                    'name' => 'Our ability to cover trading liabilities (Current Ratio)',
                    'partials' => 'our_ability_to_cover_trading_liablities',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            10 =>
                array(
                    'id' => 12,
                    'method' => NULL,
                    'name' => 'What we owe suppliers (Payables)',
                    'partials' => 'payables_accounts',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            11 =>
                array(
                    'id' => 13,
                    'method' => NULL,
                    'name' => 'Payment now due to Suppliers (Payable Age Analysis)',
                    'partials' => 'payables_age_analysis',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            12 =>
                array(
                    'id' => 14,
                    'method' => NULL,
                    'name' => 'Purchase History ',
                    'partials' => 'purchase_history',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            13 =>
                array(
                    'id' => 15,
                    'method' => NULL,
                    'name' => 'Age Analysis of amount owed to us (Receivable Age Analysis)',
                    'partials' => 'receivable_age_analysis',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            14 =>
                array(
                    'id' => 16,
                    'method' => NULL,
                    'name' => 'Those who owe us (Receivables)',
                    'partials' => 'receivables_accounts',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            15 =>
                array(
                    'id' => 17,
                    'method' => NULL,
                    'name' => 'Our Major Customers by what we are owed (Outstanding Receivables)',
                    'partials' => 'receivables_owed',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            16 =>
                array(
                    'id' => 18,
                    'method' => NULL,
                    'name' => 'Our Running Expenses (Operating expenses)',
                    'partials' => 'running_expenses',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            17 =>
                array(
                    'id' => 19,
                    'method' => NULL,
                    'name' => 'Sales History',
                    'partials' => 'sales_history',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            18 =>
                array(
                    'id' => 20,
                    'method' => NULL,
                    'name' => 'Summary of stock',
                    'partials' => 'stock_summary',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            19 =>
                array(
                    'id' => 21,
                    'method' => NULL,
                    'name' => 'Our Major Supplier by purchases',
                    'partials' => 'supplier_by_purchases',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            20 =>
                array(
                    'id' => 22,
                    'method' => NULL,
                    'name' => 'Top five purchases of stock',
                    'partials' => 'top_five_purchases_of_stock',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            21 =>
                array(
                    'id' => 23,
                    'method' => NULL,
                    'name' => 'Top five sales of stock',
                    'partials' => 'top_five_sales_of_stock',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            22 =>
                array(
                    'id' => 24,
                    'method' => NULL,
                    'name' => 'Our Major supplier by what we owe (Outstanding Balance of Payables)',
                    'partials' => 'top_payables_accounts',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            23 =>
                array(
                    'id' => 25,
                    'method' => NULL,
                    'name' => 'Average Number of days for our customers to pay up (Debtors\' Collection Period)',
                    'partials' => 'debtors_collection_period',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            24 =>
                array(
                    'id' => 26,
                    'method' => NULL,
                    'name' => 'Summary of Trading Results (Profit & Loss)',
                    'partials' => 'summary_trading',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            25 =>
                array(
                    'id' => 27,
                    'method' => NULL,
                    'name' => 'Summary state of our Business (Year)',
                    'partials' => 'summary_state_year',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            26 =>
                array(
                    'id' => 28,
                    'method' => NULL,
                    'name' => 'Our Monthly Revenue',
                    'partials' => 'monthly_revenue',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
            27 =>
                array(
                    'id' => 29,
                    'method' => NULL,
                    'name' => 'Return on Capital Employed',
                    'partials' => 'return_on_capital_employed',
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                ),
        ));


    }
}