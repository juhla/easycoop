<?php

namespace App\Modules\Api\Database\Seeds;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('widget_roles')->delete();

        DB::table('widget_roles')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'widget_id' => 4,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            1 =>
                array(
                    'id' => 2,
                    'widget_id' => 5,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            2 =>
                array(
                    'id' => 3,
                    'widget_id' => 6,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            3 =>
                array(
                    'id' => 4,
                    'widget_id' => 7,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            4 =>
                array(
                    'id' => 5,
                    'widget_id' => 8,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            5 =>
                array(
                    'id' => 6,
                    'widget_id' => 10,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            7 =>
                array(
                    'id' => 8,
                    'widget_id' => 15,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            8 =>
                array(
                    'id' => 10,
                    'widget_id' => 18,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            9 =>
                array(
                    'id' => 11,
                    'widget_id' => 20,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            11 =>
                array(
                    'id' => 13,
                    'widget_id' => 25,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            13 =>
                array(
                    'id' => 15,
                    'widget_id' => 27,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            14 =>
                array(
                    'id' => 16,
                    'widget_id' => 29,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            15 =>
                array(
                    'id' => 17,
                    'widget_id' => 30,
                    'role_id' => 4,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            16 =>
                array(
                    'id' => 18,
                    'widget_id' => 4,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            17 =>
                array(
                    'id' => 19,
                    'widget_id' => 5,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            18 =>
                array(
                    'id' => 20,
                    'widget_id' => 6,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            19 =>
                array(
                    'id' => 21,
                    'widget_id' => 7,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            20 =>
                array(
                    'id' => 22,
                    'widget_id' => 8,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            21 =>
                array(
                    'id' => 23,
                    'widget_id' => 10,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            22 =>
                array(
                    'id' => 24,
                    'widget_id' => 11,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            23 =>
                array(
                    'id' => 25,
                    'widget_id' => 13,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            24 =>
                array(
                    'id' => 26,
                    'widget_id' => 15,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            25 =>
                array(
                    'id' => 28,
                    'widget_id' => 18,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            26 =>
                array(
                    'id' => 29,
                    'widget_id' => 20,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            27 =>
                array(
                    'id' => 30,
                    'widget_id' => 24,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            28 =>
                array(
                    'id' => 31,
                    'widget_id' => 25,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            29 =>
                array(
                    'id' => 32,
                    'widget_id' => 26,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            30 =>
                array(
                    'id' => 33,
                    'widget_id' => 27,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            31 =>
                array(
                    'id' => 34,
                    'widget_id' => 29,
                    'role_id' => 5,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            32 =>
                array(
                    'id' => 35,
                    'widget_id' => 4,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            33 =>
                array(
                    'id' => 36,
                    'widget_id' => 5,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            34 =>
                array(
                    'id' => 37,
                    'widget_id' => 6,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            35 =>
                array(
                    'id' => 38,
                    'widget_id' => 7,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            36 =>
                array(
                    'id' => 39,
                    'widget_id' => 8,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            37 =>
                array(
                    'id' => 40,
                    'widget_id' => 10,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            38 =>
                array(
                    'id' => 41,
                    'widget_id' => 11,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            40 =>
                array(
                    'id' => 43,
                    'widget_id' => 13,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            41 =>
                array(
                    'id' => 44,
                    'widget_id' => 15,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            42 =>
                array(
                    'id' => 46,
                    'widget_id' => 17,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            43 =>
                array(
                    'id' => 47,
                    'widget_id' => 18,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            44 =>
                array(
                    'id' => 48,
                    'widget_id' => 20,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            45 =>
                array(
                    'id' => 49,
                    'widget_id' => 24,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            46 =>
                array(
                    'id' => 50,
                    'widget_id' => 25,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            47 =>
                array(
                    'id' => 51,
                    'widget_id' => 26,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            48 =>
                array(
                    'id' => 52,
                    'widget_id' => 27,
                    'role_id' => 6,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            49 =>
                array(
                    'id' => 53,
                    'widget_id' => 4,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            50 =>
                array(
                    'id' => 54,
                    'widget_id' => 5,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            51 =>
                array(
                    'id' => 55,
                    'widget_id' => 6,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            52 =>
                array(
                    'id' => 56,
                    'widget_id' => 7,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            53 =>
                array(
                    'id' => 57,
                    'widget_id' => 8,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            54 =>
                array(
                    'id' => 58,
                    'widget_id' => 10,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            55 =>
                array(
                    'id' => 59,
                    'widget_id' => 11,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            57 =>
                array(
                    'id' => 61,
                    'widget_id' => 13,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            58 =>
                array(
                    'id' => 62,
                    'widget_id' => 15,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            59 =>
                array(
                    'id' => 64,
                    'widget_id' => 18,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            60 =>
                array(
                    'id' => 65,
                    'widget_id' => 20,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            61 =>
                array(
                    'id' => 66,
                    'widget_id' => 24,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            62 =>
                array(
                    'id' => 67,
                    'widget_id' => 25,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            63 =>
                array(
                    'id' => 68,
                    'widget_id' => 26,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            64 =>
                array(
                    'id' => 69,
                    'widget_id' => 27,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            65 =>
                array(
                    'id' => 70,
                    'widget_id' => 29,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            66 =>
                array(
                    'id' => 71,
                    'widget_id' => 30,
                    'role_id' => 10,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            67 =>
                array(
                    'id' => 72,
                    'widget_id' => 4,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            68 =>
                array(
                    'id' => 73,
                    'widget_id' => 5,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            69 =>
                array(
                    'id' => 74,
                    'widget_id' => 6,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            70 =>
                array(
                    'id' => 75,
                    'widget_id' => 7,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            71 =>
                array(
                    'id' => 76,
                    'widget_id' => 8,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            72 =>
                array(
                    'id' => 77,
                    'widget_id' => 10,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            73 =>
                array(
                    'id' => 78,
                    'widget_id' => 11,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            75 =>
                array(
                    'id' => 80,
                    'widget_id' => 13,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            76 =>
                array(
                    'id' => 81,
                    'widget_id' => 15,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            77 =>
                array(
                    'id' => 83,
                    'widget_id' => 18,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            78 =>
                array(
                    'id' => 84,
                    'widget_id' => 20,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            79 =>
                array(
                    'id' => 85,
                    'widget_id' => 24,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            80 =>
                array(
                    'id' => 86,
                    'widget_id' => 25,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            81 =>
                array(
                    'id' => 87,
                    'widget_id' => 26,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            82 =>
                array(
                    'id' => 88,
                    'widget_id' => 27,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            83 =>
                array(
                    'id' => 89,
                    'widget_id' => 29,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            84 =>
                array(
                    'id' => 90,
                    'widget_id' => 30,
                    'role_id' => 11,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            85 =>
                array(
                    'id' => 91,
                    'widget_id' => 5,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            86 =>
                array(
                    'id' => 92,
                    'widget_id' => 6,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            87 =>
                array(
                    'id' => 93,
                    'widget_id' => 7,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            88 =>
                array(
                    'id' => 94,
                    'widget_id' => 8,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            89 =>
                array(
                    'id' => 95,
                    'widget_id' => 10,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            91 =>
                array(
                    'id' => 97,
                    'widget_id' => 15,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            92 =>
                array(
                    'id' => 99,
                    'widget_id' => 17,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            93 =>
                array(
                    'id' => 100,
                    'widget_id' => 20,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            94 =>
                array(
                    'id' => 101,
                    'widget_id' => 24,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            95 =>
                array(
                    'id' => 102,
                    'widget_id' => 25,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            96 =>
                array(
                    'id' => 103,
                    'widget_id' => 26,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            97 =>
                array(
                    'id' => 104,
                    'widget_id' => 27,
                    'role_id' => 7,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            98 =>
                array(
                    'id' => 105,
                    'widget_id' => 5,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            99 =>
                array(
                    'id' => 106,
                    'widget_id' => 6,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            100 =>
                array(
                    'id' => 107,
                    'widget_id' => 7,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            101 =>
                array(
                    'id' => 108,
                    'widget_id' => 8,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            102 =>
                array(
                    'id' => 109,
                    'widget_id' => 10,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            104 =>
                array(
                    'id' => 111,
                    'widget_id' => 15,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            105 =>
                array(
                    'id' => 113,
                    'widget_id' => 17,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            106 =>
                array(
                    'id' => 114,
                    'widget_id' => 20,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            107 =>
                array(
                    'id' => 115,
                    'widget_id' => 24,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            108 =>
                array(
                    'id' => 116,
                    'widget_id' => 25,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            109 =>
                array(
                    'id' => 117,
                    'widget_id' => 26,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            110 =>
                array(
                    'id' => 118,
                    'widget_id' => 27,
                    'role_id' => 8,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            111 =>
                array(
                    'id' => 119,
                    'widget_id' => 5,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            112 =>
                array(
                    'id' => 120,
                    'widget_id' => 6,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            113 =>
                array(
                    'id' => 121,
                    'widget_id' => 7,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            114 =>
                array(
                    'id' => 122,
                    'widget_id' => 8,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            115 =>
                array(
                    'id' => 123,
                    'widget_id' => 10,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            116 =>
                array(
                    'id' => 124,
                    'widget_id' => 15,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            117 =>
                array(
                    'id' => 126,
                    'widget_id' => 20,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            118 =>
                array(
                    'id' => 127,
                    'widget_id' => 24,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            119 =>
                array(
                    'id' => 128,
                    'widget_id' => 25,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            120 =>
                array(
                    'id' => 129,
                    'widget_id' => 26,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            121 =>
                array(
                    'id' => 130,
                    'widget_id' => 27,
                    'role_id' => 12,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            122 =>
                array(
                    'id' => 131,
                    'widget_id' => 27,
                    'role_id' => 9,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            123 =>
                array(
                    'id' => 132,
                    'widget_id' => 27,
                    'role_id' => 13,
                    'updated_at' => '2017-12-23 13:55:23',
                    'created_at' => '2017-12-23 13:55:23',
                ),
            124 =>
                array(
                    'id' => 133,
                    'widget_id' => 31,
                    'role_id' => 4,
                    'updated_at' => NULL,
                    'created_at' => NULL,
                ),
            125 =>
                array(
                    'id' => 134,
                    'widget_id' => 31,
                    'role_id' => 10,
                    'updated_at' => NULL,
                    'created_at' => NULL,
                ),
            126 =>
                array(
                    'id' => 135,
                    'widget_id' => 31,
                    'role_id' => 11,
                    'updated_at' => NULL,
                    'created_at' => NULL,
                ),
            127 =>
                array(
                    'id' => 136,
                    'widget_id' => 31,
                    'role_id' => 5,
                    'updated_at' => NULL,
                    'created_at' => NULL,
                ),
        ));


    }
}