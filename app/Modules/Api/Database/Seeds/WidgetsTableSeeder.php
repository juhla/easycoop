<?php

namespace App\Modules\Api\Database\Seeds;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WidgetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('widgets')->delete();

        DB::table('widgets')->insert(array(
            0 =>
                array(
                    'id' => 4,
                    'name' => 'Notebook',
                    'routes' => 'notebook',
                    'class' => 'fa fa-book img-launch notebook-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:14:07',
                    'updated_at' => '2017-04-11 13:14:07',
                    'mode' => '0',
                ),
            1 =>
                array(
                    'id' => 5,
                    'name' => 'Dashboard / Analytics',
                    'routes' => 'dashboard',
                    'class' => 'fa fa-tachometer img-launch dashboard-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:16:12',
                    'updated_at' => '2017-04-11 13:16:12',
                    'mode' => '0',
                ),
            2 =>
                array(
                    'id' => 6,
                    'name' => 'Reports',
                    'routes' => 'reports',
                    'class' => 'fa fa-file-text-o img-launch reports-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:16:19',
                    'updated_at' => '2017-04-11 13:16:19',
                    'mode' => '0',
                ),
            3 =>
                array(
                    'id' => 7,
                    'name' => 'To do List / Notepad',
                    'routes' => 'todo',
                    'class' => 'fa fa-list-ol img-launch transaction-management-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:16:35',
                    'updated_at' => '2017-04-11 13:16:35',
                    'mode' => '0',
                ),
            4 =>
                array(
                    'id' => 8,
                    'name' => 'Calendar',
                    'routes' => '',
                    'class' => 'fa fa-calendar-check-o img-launch calendar-color',
                    'div_items' => 'data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"',
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:16:42',
                    'updated_at' => '2017-04-11 13:16:42',
                    'mode' => '0',
                ),
            5 =>
                array(
                    'id' => 9,
                    'name' => 'Customer Management',
                    'routes' => 'setup/customers',
                    'class' => 'fa fa-users img-launch customer-management-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:17:56',
                    'updated_at' => '2017-04-11 13:17:56',
                    'mode' => '0',
                ),
            6 =>
                array(
                    'id' => 10,
                    'name' => 'My Profile',
                    'routes' => '',
                    'class' => 'fa fa-id-card img-launch profile-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:18:06',
                    'updated_at' => '2017-04-11 13:18:06',
                    'mode' => '0',
                ),
            7 =>
                array(
                    'id' => 11,
                    'name' => 'Inventory Management',
                    'routes' => 'ims/create',
                    'class' => 'fa fa-truck img-launch ims-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:19:42',
                    'updated_at' => '2017-04-11 13:19:42',
                    'mode' => '0',
                ),
            8 =>
                array(
                    'id' => 12,
                    'name' => 'Collaborators',
                    'routes' => 'collaborators/list',
                    'class' => 'fa fa-handshake-o img-launch collaborator-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:19:48',
                    'updated_at' => '2017-04-11 13:19:48',
                    'mode' => '0',
                ),
            9 =>
                array(
                    'id' => 13,
                    'name' => 'HRM',
                    'routes' => 'payroll/create',
                    'class' => 'fa fa-address-card-o img-launch hrm-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:19:59',
                    'updated_at' => '2017-04-11 13:19:59',
                    'mode' => '0',
                ),
            10 =>
                array(
                    'id' => 14,
                    'name' => 'Townhall',
                    'routes' => '',
                    'class' => 'fa fa-users img-launch townhall-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:09',
                    'updated_at' => '2017-04-11 13:20:09',
                    'mode' => '0',
                ),
            11 =>
                array(
                    'id' => 15,
                    'name' => 'FAQ',
                    'routes' => 'faq',
                    'class' => 'fa fa-address-book img-launch faq-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:17',
                    'updated_at' => '2017-04-11 13:20:17',
                    'mode' => '0',
                ),
            12 =>
                array(
                    'id' => 16,
                    'name' => 'Support',
                    'routes' => 'ticket/add-new',
                    'class' => 'fa fa-headphones img-launch support-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:24',
                    'updated_at' => '2017-04-11 13:20:24',
                    'mode' => '0',
                ),
            13 =>
                array(
                    'id' => 17,
                    'name' => 'My Businesses',
                    'routes' => 'collaborators/businesses',
                    'class' => 'fa fa-cubes img-launch my-businesses-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:32',
                    'updated_at' => '2017-04-11 13:20:32',
                    'mode' => '0',
                ),
            14 =>
                array(
                    'id' => 18,
                    'name' => 'Supplier Management',
                    'routes' => 'setup/vendors',
                    'class' => 'fa fa-shopping-basket img-launch supplier-mangement-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:41',
                    'updated_at' => '2017-04-11 13:20:41',
                    'mode' => '0',
                ),
            15 =>
                array(
                    'id' => 19,
                    'name' => 'User Management',
                    'routes' => '',
                    'class' => 'fa fa-address-book img-launch user-management-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:50',
                    'updated_at' => '2017-04-11 13:20:50',
                    'mode' => '0',
                ),
            16 =>
                array(
                    'id' => 20,
                    'name' => 'Calculator',
                    'routes' => '',
                    'class' => 'fa fa-calculator img-launch calculator-color',
                    'div_items' => 'data-toggle="modal" data-target="#myModal"',
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:20:56',
                    'updated_at' => '2017-04-11 13:20:56',
                    'mode' => '0',
                ),
            17 =>
                array(
                    'id' => 21,
                    'name' => 'My Business Banking',
                    'routes' => '',
                    'class' => 'fa fa-university img-launch my-businesses-banking-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:04',
                    'updated_at' => '2017-04-11 13:21:04',
                    'mode' => '0',
                ),
            18 =>
                array(
                    'id' => 22,
                    'name' => 'Transaction Management',
                    'routes' => '',
                    'class' => 'fa fa-exchange img-launch transaction-management-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:16',
                    'updated_at' => '2017-04-11 13:21:16',
                    'mode' => '0',
                ),
            19 =>
                array(
                    'id' => 23,
                    'name' => 'Tax',
                    'routes' => '',
                    'class' => 'fa fa-money img-launch tax-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:23',
                    'updated_at' => '2017-04-11 13:21:23',
                    'mode' => '0',
                ),
            20 =>
                array(
                    'id' => 24,
                    'name' => 'Settings',
                    'routes' => 'account-settings',
                    'class' => 'fa fa-cog img-launch settings-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:29',
                    'updated_at' => '2017-04-11 13:21:29',
                    'mode' => '0',
                ),
            21 =>
                array(
                    'id' => 25,
                    'name' => 'Conversations',
                    'routes' => 'message',
                    'class' => 'fa fa-comments-o img-launch conversation-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:36',
                    'updated_at' => '2017-04-11 13:21:36',
                    'mode' => '0',
                ),
            22 =>
                array(
                    'id' => 26,
                    'name' => 'Walkthrough',
                    'routes' => '#',
                    'class' => 'fa fa-female img-launch walk-through-color',
                    'div_items' => 'id="walkthrough"',
                    'description' => '<p><br></p>',
                    'created_at' => '2017-04-11 13:21:43',
                    'updated_at' => '2017-04-11 13:21:43',
                    'mode' => '0',
                ),
            23 =>
                array(
                    'id' => 27,
                    'name' => 'User Guide',
                    'routes' => 'user-guide',
                    'class' => 'fa fa-compass img-launch transaction-management-color',
                    'div_items' => NULL,
                    'description' => '<p><br></p>',
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'mode' => '0',
                ),
            24 =>
                array(
                    'id' => 28,
                    'name' => 'Bmac',
                    'routes' => 'workspace',
                    'class' => 'fa fa-book img-launch notebook-color',
                    'div_items' => NULL,
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'mode' => '0',
                ),
            25 =>
                array(
                    'id' => 29,
                    'name' => 'Notebook Plus',
                    'routes' => 'notebook-adv',
                    'class' => 'fa fa-newspaper-o img-launch notebook-plus-color',
                    'div_items' => NULL,
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'mode' => '0',
                ),
            26 =>
                array(
                    'id' => 30,
                    'name' => 'Audit Trail',
                    'routes' => 'audit-log',
                    'class' => 'fa fa-binoculars img-launch audit-color',
                    'div_items' => NULL,
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'mode' => '0',
                ),
            27 =>
                array(
                    'id' => 31,
                    'name' => 'Batch Posting',
                    'routes' => 'batch_posting',
                    'class' => 'fa fa-credit-card img-launch collaborator-color',
                    'div_items' => NULL,
                    'description' => NULL,
                    'created_at' => NULL,
                    'updated_at' => NULL,
                    'mode' => '0',
                ),
        ));


    }
}