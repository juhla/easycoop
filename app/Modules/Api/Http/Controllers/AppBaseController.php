<?php

namespace App\Modules\Api\Http\Controllers;

use App\Modules\Api\Traits\Errors;
use App\Modules\Base\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */

ini_set('max_execution_time', 300);
class AppBaseController extends Controller
{
    use Errors;
    public $postingType = ['bank', 'cash'];

}
