<?php

namespace App\Modules\Api\Http\Controllers;

use App\Modules\Api\Models\Ledger;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\BankAC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BankController extends AppBaseController
{
    use DebitCredit;

    public function store(Request $request)
    {
        //create the loan account
        $request = \request()->input();
        if (!isset($request['items'])) {
            $bankItem['items'] = [$request];
        } else {
            $bankItem['items'] = $request['items'];
        }
        $rules = [
            'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
        ];

        $allRequests = $errorLedger = [];
        $counter = 0;

        foreach ($bankItem['items'] as $newItems) {

            $allRequests[] = $newItems;
            $request = $newItems;
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $failedRules = $validator->failed();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
                $this->hasError = true;
            }

            $counter++;
        }
        if (!empty($this->getSaveErrors()) || $this->hasError) {
            return $this->sendErrorWithData(array_merge($this->getSaveErrors()), "There is an Error in your request", 400);
        }

        DB::connection('tenant_conn')->beginTransaction();

        try {
            foreach ($allRequests as $eachRequest) {
                $validator = Validator::make($eachRequest, $rules);
                if ($validator->fails()) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                    break;
                }

                $ledger_group_id = 16; //member's savings
                $ledger = new Ledger;
                $ledger->name = $eachRequest['name'];
                $ledger->group_id = $ledger_group_id;
                $ledger->code = Ledger::getLastBankAccountCode();
                $ledger->opening_balance_type = "D";
                $ledger->save();
                $this->setSavedID($ledger->id);


                $bankAccount = new BankAC;
                $bankAccount->ledger_code = $ledger->code;
                $bankAccount->bank = $ledger->name;
                $bankAccount->ending_reconciled_balance = 0;
                $bankAccount->save();
            }

            if (!empty($this->getSaveErrors())) {
                //DB::connection('tenant_conn')->rollBack();
                return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
            }
        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());
        }
        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        $ledger = Ledger::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($ledger->toArray(), "Bank created successfully");
    }

    public function show($id)
    {

        $ledger = Ledger::where('id', $id)->where('group_id', '16')->get();
        if ($ledger->isEmpty()) {
            return $this->sendError('Bank Account not found');
        }
        return $this->sendResponse($ledger->toArray(), 'Bank Account retrieved successfully');
    }

    public function listBank()
    {
        $membersAccounts = Ledger::where('group_id', 16)->get();
        return $this->sendResponse($membersAccounts->toArray(), "Bank Account List");
    }
}
