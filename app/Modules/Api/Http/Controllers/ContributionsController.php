<?php

namespace App\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Api\Models\Ledger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ContributionsController extends AppBaseController
{
    use DebitCredit;

    public function store(Request $request)
    {
        //create the loan account
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }
        $allRequests = $errorLedger = [];
        $counter = 0;
        foreach ($newRequest['items'] as $eachRequest) {
            $allRequests[] = $eachRequest;
            $request = $eachRequest;
            $rules = [
                'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
            ];
            $ledger = Ledger::where('name', $request['name'])->first();
            if (!empty($ledger)) {
                $errorLedger["existing_ledgers"][$counter] = $ledger->toArray();
                $this->hasError = true;
                //$this->setSaveErrors("This Ledger Exists" . ". index:: [" . $counter . "]");
            }
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $failedRules = $validator->failed();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                if(isset($failedRules['name']['Unique'])) {
                    $object->ledger = $ledger->toArray();
                }
                $this->setSaveErrors($object);
                $this->hasError = true;
            }
            $counter++;
        }

        //dd($this->getSaveErrors(), $errorLedger);
        if (!empty($this->getSaveErrors()) || $this->hasError) {
            return $this->sendErrorWithData(array_merge($this->getSaveErrors()), "There is an Error in your request", 400);
        }
        DB::connection('tenant_conn')->beginTransaction();
        try {
            foreach ($allRequests as $eachRequest) {
                $rules = [
                    'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                    break;
                }

                $ledger_group_id = 44; //member's Contribution
                $ledger = new Ledger;
                $ledger->name = $eachRequest['name'];
                $ledger->group_id = $ledger_group_id;
                $ledger->code = Ledger::getLastContributionsCode();
                $ledger->opening_balance_type = "C";
                $ledger->save();
                $this->setSavedID($ledger->id);
            }
            if (!empty($this->getSaveErrors())) {
                return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 404);
            }
        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());
        }
        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        $ledger = Ledger::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($ledger->toArray(), "Contributions account created successfully");
    }


    public function membersContribution(Request $request)
    {
        $request = $request->all();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }

        //dd($newRequest);

        $allRequests = [];
        $counter = 0;
        foreach ($newRequest['items'] as $eachRequest) {
            $request = $eachRequest;
            $request['payment_type'] = strtolower($request['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($request) {
                            $query->where('id', $request['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
            ];
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($request['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($request) {
                                $query->where('id', $request['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $request['bank_id'];
            }


            $request['credit_ledger'] = $request['member_contribution_account'];
            $request['debit_ledger'] = $ledgerID; //Bank/Cash


            $allRequests['items'][$counter] = $request;

            $counter++;
        }

        //dd($this->getSaveErrors());
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 404);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Disbursement without interest");
        }
    }

    public function interestOnContributionPaid(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }

        //dd($newRequest);

        $allRequests = [];
        $counter = 0;
        $ledgerID = $this->getLedgerIdFromCode(1261);

        foreach ($newRequest['items'] as $eachRequest) {
            $eachRequest['payment_type'] = strtolower($eachRequest['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($eachRequest) {
                            $query->where('id', $eachRequest['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
            ];
            $validator = Validator::make($eachRequest, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            if ($eachRequest['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($eachRequest) {
                                $query->where('id', $eachRequest['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($eachRequest, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $eachRequest['bank_id'];
            }

            $eachRequest['credit_ledger'] = $ledgerID; //Bank/Cash
            $eachRequest['debit_ledger'] = $eachRequest['member_contribution_account'];

            $allRequests['items'][$counter] = $eachRequest;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Disbursement without interest");
        }
    }

    public function withdrawalFromContribution(Request $request)
    {
        $request = \request()->input();
        $ledgerID = $this->getLedgerIdFromCode(1261);
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }

        //dd($newRequest);

        $allRequests = [];
        $counter = 0;

        foreach ($newRequest['items'] as $eachRequest) {
            $eachRequest['payment_type'] = strtolower($eachRequest['payment_type']);

            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($eachRequest) {
                            $query->where('id', $eachRequest['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
            ];
            $validator = Validator::make($eachRequest, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            if ($eachRequest['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($eachRequest) {
                                $query->where('id', $eachRequest['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($eachRequest, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $eachRequest['bank_id'];
            }

            $eachRequest['credit_ledger'] = $ledgerID; //Bank/Cash
            $eachRequest['debit_ledger'] = $eachRequest['member_contribution_account'];

            $allRequests['items'][$counter] = $eachRequest;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Disbursement without interest");
        }
    }


    public function interestAccrued(Request $request)
    {
        $ledgerID = $this->getLedgerIdFromCode(5104); // Interest on Contributions
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }

        $allRequests = [];
        $counter = 0;

        foreach ($newRequest['items'] as $eachRequest) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($eachRequest) {
                            $query->where('id', $eachRequest['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
            ];
            $validator = Validator::make($eachRequest, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $eachRequest['debit_ledger'] = $ledgerID; // Interest on Contribution
            $eachRequest['credit_ledger'] = $eachRequest['member_contribution_account']; //Member Contribution Account

            $allRequests['items'][$counter] = $eachRequest;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Disbursement without interest");
        }
    }


    public function show($id)
    {

        $ledger = Ledger::where('id', $id)->where('group_id', '44')->get();
        if ($ledger->isEmpty()) {
            return $this->sendError('Contribution Account not found');
        }
        return $this->sendResponse($ledger->toArray(), 'Contribution Account retrieved successfully');
    }


    public function listMembersContributionAccount()
    {
        $membersAccounts = Ledger::where('group_id', 44)->get();
        return $this->sendResponse($membersAccounts->toArray(), "Members Contribution Account");
    }
}
