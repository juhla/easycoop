<?php

namespace App\Modules\Api\Http\Controllers;

use App\Modules\Api\Models\Transaction;
use App\Modules\Api\Models\TransactionItem;
use Illuminate\Http\Request;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Api\Models\Ledger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Arr;

class EquityController extends AppBaseController
{
    use DebitCredit;
    private $Errors = [];
    private $isError = false;

    public function store(Request $request)
    {
        //create the loan account
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }
        $allRequests = $errorLedger = [];
        $counter = 0;
        $rules = [
            'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
        ];

        foreach ($newRequest['items'] as $eachRequest) {
            $allRequests[] = $eachRequest;
            $request = $eachRequest;

            $ledger = Ledger::where('name', $eachRequest['name'])->first();
            if (!empty($ledger)) {
                $errorLedger["existing_ledgers"][$counter] = $ledger->toArray();
                $this->hasError = true;
            }

            $validator = Validator::make($eachRequest, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $failedRules = $validator->failed();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                if(isset($failedRules['name']['Unique'])) {
                    $object->ledger = $ledger->toArray();
                }
                $this->setSaveErrors($object);
                $this->hasError = true;
            }
            $counter++;
        }

        if (!empty($this->getSaveErrors()) || $this->hasError) {
            return $this->sendErrorWithData(array_merge($this->getSaveErrors(), $errorLedger), "There is an Error in your request", 400);
        }

        DB::connection('tenant_conn')->beginTransaction();
        try {
            $newCounter = 0;
            foreach ($allRequests as $eachRequest) {
                $rules = [
                    'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
                ];
                $validator = Validator::make($request, $rules);
                if ($validator->fails()) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                    break;
                }

                $ledger_group_id = 47; //Member's Equity
                $ledger = new Ledger();
                $ledger->name = $eachRequest['name'];
                $ledger->group_id = $ledger_group_id;
                $ledger->code = Ledger::getLastMemberEquityCode();
                $ledger->opening_balance_type = "C";
                $ledger->save();
                $this->setSavedID($ledger->id);

                $newCounter++;
            }
            if (!empty($this->getSaveErrors())) {
                return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 404);
            }
        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());
        }
        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        $ledger = Ledger::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($ledger->toArray(), "Equity account created successfully");
    }


    public function show($id)
    {

        $ledger = Ledger::where('id', $id)->where('group_id', '47')->get();
        if ($ledger->isEmpty()) {
            return $this->sendError('Contribution Account not found');
        }
        return $this->sendResponse($ledger->toArray(), 'Members Equity Account retrieved successfully');
    }


    public function listAccount()
    {
        $membersAccounts = Ledger::where('group_id', 47)->get();
        return $this->sendResponse($membersAccounts->toArray(), "Members Equity Account");
    }


    public function purchaseOfShares(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($newRequest['items'] as $equityItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($equityItems) {
                            $query->where('id', $equityItems['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
                'member_equity_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($equityItems) {
                            $query->where('id', $equityItems['member_equity_account'])
                                ->where('group_id', '47');
                        }),
                ],
            ];

            $validator = Validator::make($equityItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }


            $equityItems['debit_ledger'] = $equityItems['member_contribution_account']; //Member Contribution Account
            $equityItems['credit_ledger'] = $equityItems['member_equity_account']; //Member Equity Account

            $allRequests['items'][$counter] = $equityItems;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with purchase of shares");
        }
    }

    public function saleOfShares(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($newRequest['items'] as $newItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
                'member_equity_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['member_equity_account'])
                                ->where('group_id', '47');
                        }),
                ],
            ];

            $validator = Validator::make($newItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }


            $newItems['debit_ledger'] = $newItems['member_equity_account']; //Member Equity Account
            $newItems['credit_ledger'] = $newItems['member_contribution_account']; //Member Contribution Account

            $allRequests['items'][$counter] = $newItems;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Disbursement without interest");
        }
    }


    public function checkUserSell(array $request, \Illuminate\Validation\Validator $validator)
    {
        if ($request['buying_member_equity_account'] == $request['selling_member_equity_account']) {

            $validator->errors()->add('buying_member_equity_account', "Buying Member's Equity Account must not be the same as the Selling Member");
            $this->Errors[] = "Buying Member's Equity Account must not be the same as the Selling Member";
            $this->isError = true;
            return $validator;
        }

        if ($request['buying_member_contribution_account'] == $request['selling_member_contribution_account']) {
            $validator->errors()->add('buying_member_contribution_account', "Buying Contribution's Account must not be the same as the Selling Member");
            $this->Errors[] = "Buying Contribution's Account must not be the same as the Selling Member";
            $this->isError = true;
            return $validator;
        }

        $this->isError = false;

        return $validator;
    }

    public function saleOfSharesBetweenMembers(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $newRequest['items'] = [$request];
        } else {
            $newRequest['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($newRequest['items'] as $newItems) {
            $request = $newItems;
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'buying_member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['buying_member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
                'buying_member_equity_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['buying_member_equity_account'])
                                ->where('group_id', '47');
                        }),
                ],
                'selling_member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['selling_member_contribution_account'])
                                ->where('group_id', '44');
                        }),
                ],
                'selling_member_equity_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($newItems) {
                            $query->where('id', $newItems['selling_member_equity_account'])
                                ->where('group_id', '47');
                        }),
                ],
            ];

            $validator = Validator::make($newItems, $rules);


            $validator->after(function ($validator) use ($newItems) {
                $validator = $this->checkUserSell($newItems, $validator);
            });

            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }


            try {
                $request['reference_no'] = 'TRA' . time();
                DB::connection('tenant_conn')->beginTransaction();
                try {
                    $trans_info = Transaction::create([
                        'transaction_date' => $request['transaction_date'],
                        'amount' => $request['amount'],
                        'transaction_type' => 'api',
                        'reference_no' => $request['reference_no'],
                        'description' => $request['description']
                    ]);
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }

                $item = new TransactionItem;
                $item->transaction_id = $trans_info->id;
                $item->ledger_id = $request['selling_member_equity_account'];
                $item->amount = $request['amount'];
                $item->item_description = $request['description'];
                $item->dc = 'D';

                $transactionItems[] = $item;

                $item = new TransactionItem;
                $item->transaction_id = $trans_info->id;
                $item->ledger_id = $request['buying_member_contribution_account'];
                $item->amount = $request['amount'];
                $item->item_description = $request['description'];
                $item->dc = 'D';

                $transactionItems[] = $item;

                $item = new TransactionItem;
                $item->transaction_id = $trans_info->id;
                $item->ledger_id = $request['buying_member_equity_account'];
                $item->amount = $request['amount'];
                $item->item_description = $request['description'];
                $item->dc = 'C';

                $transactionItems[] = $item;


                $item = new TransactionItem;
                $item->transaction_id = $trans_info->id;
                $item->ledger_id = $request['selling_member_contribution_account'];
                $item->amount = $request['amount'];
                $item->item_description = $request['description'];
                $item->dc = 'C';

                $transactionItems[] = $item;

                $allRequests['items'][$counter] = $transactionItems;
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
                $object = new \stdClass();
                $object->errorMessage = $exception->getMessage();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        if ($trans_info) {
            $allRequests = Arr::flatten($allRequests, 3);
            foreach ($allRequests as $eachItem) {
                try {
                    $eachItem->save();
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    return $this->sendError($exception->getMessage());
                }
            }
            DB::connection('tenant_conn')->commit();
            return $this->sendResponse($trans_info->toArray(), 'Transaction created successfully');
        }
    }
}
