<?php

namespace App\Modules\Api\Http\Controllers;

use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class InterestAccruedController extends AppBaseController
{
    use DebitCredit;

    public function interestAccrued(Request $request)
    {
        //$incomeOnInvestent = 41;
        $incomeOnInvestent = $this->getLedgerIdFromCode(4700);
        $request = \request()->input();
        if (!isset($request['items'])) {
            $interestAccrued['items'] = [$request];
        } else {
            $interestAccrued['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($interestAccrued['items'] as $accountItems) {

            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '13');
                        }),
                ],
            ];

            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $accountItems['debit_ledger'] = $accountItems['member_loan_account']; //Member Loan Account
            $accountItems['credit_ledger'] = $incomeOnInvestent; //Interest income Account

            $allRequests['items'][$counter] = $accountItems;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Member's Loan");
        }
    }
}
