<?php

namespace App\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Models\Ledger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LoanController extends AppBaseController
{
    use DebitCredit;


    public function store(Request $request)
    {
        //create the loan account
        $request = \request()->input();
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }

        $ledgerRules = [
            'name' => 'string|required'
        ];

        $rules = [
            'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
        ];
        $allRequests = $errorLedger = [];
        $counter = 0;
        //dd($loan['items']);
        foreach ($loan['items'] as $accountItems) {
            $allRequests[] = $accountItems;
            $request = $accountItems;
            $ledger = Ledger::where('name', $request['name'])->first();
            if (!empty($ledger)) {
                $errorLedger["existing_ledgers"][$counter] = $ledger->toArray();
                $this->hasError = true;
            }
            //why are we doing validation here??
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $failedRules = $validator->failed();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                if (isset($failedRules['name']['Unique'])) {
                    $object->ledger = $ledger->toArray();
                }
                $this->setSaveErrors($object);
                $this->hasError = true;
            }
            $counter++;
        }


        if (!empty($this->getSaveErrors()) || $this->hasError) {
            return $this->sendErrorWithData(array_merge($this->getSaveErrors()), "There is an Error in your request", 400);
        }

        //dd($errorLedger);
        DB::connection('tenant_conn')->beginTransaction();
        //dd($allRequests);
        try {
            foreach ($allRequests as $eachRequest) {

                $validator = Validator::make($eachRequest, $rules);
                if ($validator->fails()) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                    break;
                }
                $ledger_group_id = 45; //Member's Loan
                $ledger = new Ledger();
                $ledger->name = $eachRequest['name'];
                $ledger->group_id = $ledger_group_id;
                $ledger->code = Ledger::getLastLoanCode();
                $ledger->opening_balance_type = "D";
                $ledger->save();
                $this->setSavedID($ledger->id);
            }
            if (!empty($this->getSaveErrors())) {
                return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
            }
        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());
        }

        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        $ledger = Ledger::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($ledger->toArray(), "Loan account(s) created successfully");
    }


    public function show($id)
    {

        $ledger = Ledger::where('id', $id)->where('group_id', '45')->get();
        if ($ledger->isEmpty()) {
            return $this->sendError('Loan Account not found');
        }
        return $this->sendResponse($ledger->toArray(), 'Loan Account retrieved successfully');
    }

    public function listMembersLoanAccount()
    {
        $membersAccounts = Ledger::where('group_id', 45)->get();
        return $this->sendResponse($membersAccounts->toArray(), "Member's Loan Account");
    }


    public function loanRepayment()
    {
        $request = \request()->input();
        $ledgerID = $this->getLedgerIdFromCode(1261);
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($loan['items'] as $accountItems) {

            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];
            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            if ($accountItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($accountItems) {
                                $query->where('id', $accountItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($accountItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $accountItems['bank_id'];
            }
            $accountItems['debit_ledger'] = $ledgerID; //Bank/Cash;
            $accountItems['credit_ledger'] = $accountItems['member_loan_account'];

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Repayment");
        }
    }


    public function membersLoan(Request $request)
    {
        $request = \request()->input();

        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($loan['items'] as $accountItems) {

            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];

            $bankRules = [
                'bank_id' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['bank_id'])
                                ->where('group_id', '16');
                        }),
                ],
            ];

            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $ledgerID = $this->getLedgerIdFromCode(1261);

            if ($accountItems['payment_type'] == 'bank') {
                $validator = Validator::make($accountItems, $bankRules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $accountItems['bank_id'];
            }
            $accountItems['credit_ledger'] = $ledgerID; //Bank/Cash
            $accountItems['debit_ledger'] = $accountItems['member_loan_account'];

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Member's Loan");
        }
    }


    public function interestOnLoanReceived(Request $request)
    {
        $request = \request()->input();
        $ledgerID = $this->getLedgerIdFromCode(1261);
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($loan['items'] as $accountItems) {
            //dd($accountItems);
            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];
            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            if ($accountItems['payment_type'] == 'bank') {
                $bankRules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($accountItems) {
                                $query->where('id', $accountItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($accountItems, $bankRules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $accountItems['bank_id'];
            }

            $accountItems['debit_ledger'] = $ledgerID; //Bank/Cash
            $accountItems['credit_ledger'] = $accountItems['member_loan_account'];

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }

        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Interest On Loan Received");
        }
    }


    public function interestAccrued(Request $request)
    {
        //$incomeOnInvestent = 41;
        $incomeOnInvestent = $this->getLedgerIdFromCode(4700);
        $request = \request()->input();
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($loan['items'] as $accountItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];

            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $accountItems['debit_ledger'] = $accountItems['member_loan_account']; //Member Loan Account
            $accountItems['credit_ledger'] = $incomeOnInvestent; //Interest income Account

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }

    }

    //penaltiesOnLoanReceived

    public function penaltiesOnLoanReceived(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }

        $counter = 0;
        $allRequests = [];
        foreach ($loan['items'] as $accountItems) {
            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];
            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $ledgerID = $this->getLedgerIdFromCode(1261);

            if ($request['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($accountItems) {
                                $query->where('id', $accountItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($accountItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $accountItems['bank_id'];
            }
            $accountItems['debit_ledger'] = $ledgerID; //Bank/Cash
            $accountItems['credit_ledger'] = $accountItems['member_loan_account'];

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Penalties On Loan Received");
        }
    }


    public function penaltiesOnLoanAccrued(Request $request)
    {

        $ledgerID = $this->getLedgerIdFromCode(4900);
        $request = \request()->input();
        if (!isset($request['items'])) {
            $loan['items'] = [$request];
        } else {
            $loan['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($loan['items'] as $accountItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_loan_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_loan_account'])
                                ->where('group_id', '45');
                        }),
                ],
            ];
            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $accountItems['debit_ledger'] = $accountItems['member_loan_account']; //Member Loan Account
            $accountItems['credit_ledger'] = $ledgerID; //Penalties Income

            $allRequests['items'][$counter] = $accountItems;
            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Interest Accrued");
        }
    }
}
