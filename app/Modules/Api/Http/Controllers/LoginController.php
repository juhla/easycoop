<?php

namespace App\Modules\Api\Http\Controllers;


use App\Modules\Api\Http\Controllers\AppBaseController;
use App\Modules\Api\Models\User;
use App\Modules\Api\Traits\ApOauth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Client;
use League\OAuth2\Client\OptionProvider\HttpBasicAuthOptionProvider;

class LoginController extends AppBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, ApOauth;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            //'remember_me' => 'boolean'
        ]);
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }


    public function webLogin(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
            //'remember_me' => 'boolean'
        ]);
        if ($this->attemptWebLogin($request)) {
            return $this->sendWebLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }


    protected function sendWebLoginResponse(Request $request)
    {
        //$request->session()->regenerate();
        $this->clearLoginAttempts($request);
        $this->authenticated($request, $this->guard()->user());
        //dd('here');
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        $data = $user;
        $data['access_token'] = $tokenResult->accessToken;
        $data['token_type'] = 'Bearer';
        $data['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
        return response()->json($data);

    }


    protected function attemptWebLogin(Request $request)
    {
        $this->init($request['username'], $request['password']);
        if ($this->isError) {
            return false;
        }
        return $this->guard()->attempt(
            $this->credentials($request)
        );
    }


    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request)
        );
    }

    protected function sendLoginResponse(Request $request)
    {
        //$request->session()->regenerate();
        $this->clearLoginAttempts($request);
        $this->authenticated($request, $this->guard()->user());

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        $data = $user;
        $data['access_token'] = $tokenResult->accessToken;
        $data['token_type'] = 'Bearer';
        $data['expires_at'] = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();

        return $this->sendResponse($data, "User Authenticated !");
        // return response()->json($data);

    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //$user->last_login = now();
        //$user->save();
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        if ($this->isError)
            throw ValidationException::withMessages([$this->username() => $this->isErrorMessage]);
        if (!$user = User::where(['email' => $request->email])->first())
            throw ValidationException::withMessages([$this->username() => 'Email address not found']);
        if (!$user->active)
            throw ValidationException::withMessages(['password' => 'Access denied']);
        else
            throw ValidationException::withMessages([$this->username() => 'Incorrect password']);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function logout()
    {
        Auth::logout();
        session()->flush();
        $message = "Successfully Logged out";
        return response()->json(compact('message'));
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return array_merge($request->only($this->username(), 'password'), ['active' => 1]);
    }

}
