<?php


namespace App\Modules\Api\Http\Controllers;

use App\Modules\Auth\Traits\AfterMigration;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Api\Models\User;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Traits\TenantConfig;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends AppBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, TenantConfig, AfterMigration;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $accountName;
    protected $userModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function create()
    {

        $data = request()->input();
        $rules = [
            'name' => 'required|string|max:255',
            'username' => 'unique:users,username|required|string|max:255',
            'password' => 'required',
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $this->isSaved = false;
            return $this->response($validator->errors()->all());
        }

        DB::beginTransaction();
        try {
            $user = User::create([
                'name' => $data['name'],
                'username' => $data['username'],
                'unique_code' => $data['password'],
                'password' => Hash::make($data['password']),
            ]);
            $this->userModel = $user;
            $user->attachRole("4");
            $this->accountName = $data['username'];
        } catch (\Exception $exception) {
            DB::rollBack();
            app('sentry')->captureException($exception);
            return $this->sendError($exception->getMessage());
        }

        try {
            switch (config("app.env")) {
                case "production":
                    {
                        $prefix = "EASY_COOP";
                    }
                    break;
                case "test":
                case "local":
                    {
                        $prefix = "TEST_EASY_COOP";
                    }
                    break;
            }


            $databasename = $this->accountName;
            $dbName = $prefix . '_' . $databasename;
            try {
                DB::statement('CREATE DATABASE ' . $dbName);
            } catch (\Exception $exception) {
                DB::rollBack();
                $this->userModel->delete();
                app('sentry')->captureException($exception);
                return $this->sendError($exception->getMessage());

            }
            $this->userModel->db = $dbName;
            $this->userModel->active = 1;
            $this->userModel->save();


            $this->setConfig($dbName);


            Artisan::call('module:optimize');
            Artisan::call('module:migrate', [
                'slug' => 'auth',
                '--database' => 'tenant_conn',
                '--force' => true
            ]);
            Artisan::call('module:seed', [
                    'slug' => 'auth',
                    '--database' => 'tenant_conn',
                    '--force' => true
                ]
            );

            $this->run();
            DB::commit();
            return $this->sendResponse($user, 'User created successfully');

        } catch (\Exception $exception) {
            DB::rollBack();
            $this->userModel->delete();
            app('sentry')->captureException($exception);
            DB::statement('DROP DATABASE ' . $dbName);
            return $this->sendError("Unable to sign you up. Please try again or contact support@ikooba.com");

        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'unique_code' => 'required',
        ]);
    }


}
