<?php

namespace App\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Api\Models\Ledger;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;


class TargetSavingsController extends AppBaseController
{
    use DebitCredit;

    public function store(Request $request)
    {
        //create the loan account
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $allRequests = $errorLedger = [];
        $counter = 0;
        $rules = [
            'name' => 'string|required|unique:tenant_conn.ca_ledgers,name'
        ];
        foreach ($targetSavings['items'] as $savingsItems) {
            $allRequests[] = $savingsItems;
            $request = $savingsItems;
            $ledger = Ledger::where('name', $request['name'])->first();
            if (!empty($ledger)) {
                $errorLedger["existing_ledgers"][$counter] = $ledger->toArray();
                $this->hasError = true;
            }
            $validator = Validator::make($request, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $failedRules = $validator->failed();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                if(isset($failedRules['name']['Unique'])) {
                    $object->ledger = $ledger->toArray();
                }
                $this->setSaveErrors($object);
                $this->hasError = true;
            }
            $counter++;
        }

        if (!empty($this->getSaveErrors()) || $this->hasError) {
            return $this->sendErrorWithData(array_merge($this->getSaveErrors()), "There is an Error in your request", 400);
        }
        DB::connection('tenant_conn')->beginTransaction();
        try {
            foreach ($allRequests as $eachRequest) {
                $validator = Validator::make($eachRequest, $rules);
                if ($validator->fails()) {
                    DB::connection('tenant_conn')->rollBack();
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                    break;
                }
                $ledger_group_id = 46; //  Member's Target Savings
                $ledger = new Ledger();
                $ledger->name = $eachRequest['name'];
                $ledger->group_id = $ledger_group_id;
                $ledger->code = Ledger::getLastTargetSavingsCode();
                $ledger->opening_balance_type = "C";
                $ledger->save();
                $this->setSavedID($ledger->id);
            }
            if (!empty($this->getSaveErrors())) {
                //DB::connection('tenant_conn')->rollBack();
                return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
            }
        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());
        }
        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        $ledger = Ledger::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($ledger->toArray(), "Accounts created successfully");
    }


    public function show($id)
    {

        $ledger = Ledger::where('id', $id)->where('group_id', '46')->get();
        if ($ledger->isEmpty()) {
            return $this->sendError('Target Savings Account not found');
        }
        return $this->sendResponse($ledger->toArray(), 'Target Savings Account retrieved successfully');
    }


    public function listSavings()
    {
        $membersAccounts = Ledger::where('group_id', 46)->get();
        return $this->sendResponse($membersAccounts->toArray(), "Members Target Savings Account");
    }


    public function make(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($targetSavings['items'] as $savingsItems) {
            $savingsItems['payment_type'] = strtolower($savingsItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];

            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($savingsItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($savingsItems) {
                                $query->where('id', $savingsItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($savingsItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $savingsItems['bank_id'];
            }
            $savingsItems['credit_ledger'] = $savingsItems['member_target_savings_account'];
            $savingsItems['debit_ledger'] = $ledgerID; //Bank/Cash

            $allRequests['items'][$counter] = $savingsItems;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }
    }


    /**
     * This method handles unscheduled withdrawal
     */
    public function penaltiesFromUnscheduledWithdrawal()
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($targetSavings['items'] as $savingsItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];

            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $savingsItems['debit_ledger'] = $savingsItems['member_target_savings_account'];
            $savingsItems['credit_ledger'] = $this->getLedgerIdFromCode(4900); //Penalties Income

            $allRequests['items'][$counter] = $savingsItems;

            $counter++;
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        // dd($allRequests);
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }
    }


    public function withdrawAtScheduledTime()
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($targetSavings['items'] as $savingsItems) {
            $savingsItems['payment_type'] = strtolower($savingsItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];

            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($savingsItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($savingsItems) {
                                $query->where('id', $savingsItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($savingsItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $savingsItems['bank_id'];
            }
            $savingsItems['debit_ledger'] = $savingsItems['member_target_savings_account'];
            $savingsItems['credit_ledger'] = $ledgerID; //cash-bank

            $allRequests['items'][$counter] = $savingsItems;

            $counter++;
        }
        // dd($allRequests);
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }
    }


    public function interestOnContributionPaid(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($targetSavings['items'] as $savingsItems) {
            $savingsItems['payment_type'] = strtolower($savingsItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];
            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($savingsItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($savingsItems) {
                                $query->where('id', $savingsItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($savingsItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $request['bank_id'];
            }
            $savingsItems['credit_ledger'] = $ledgerID; //Bank/Cash
            $savingsItems['debit_ledger'] = $savingsItems['member_target_savings_account'];
            $allRequests['items'][$counter] = $savingsItems;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }
    }

    public function withdrawalFromContribution(Request $request)
    {
        $request = \request()->input();
        $ledgerID = $this->getLedgerIdFromCode(1261);
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];
        foreach ($targetSavings['items'] as $savingsItems) {
            $savingsItems['payment_type'] = strtolower($savingsItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];

            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            if ($savingsItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($savingsItems) {
                                $query->where('id', $savingsItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($savingsItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $request['bank_id'];
            }
            $savingsItems['credit_ledger'] = $ledgerID; //Bank/Cash
            $savingsItems['debit_ledger'] = $savingsItems['member_target_savings_account'];
            $allRequests['items'][$counter] = $savingsItems;
            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data Received");
        }
    }


    public function interestAccrued(Request $request)
    {
        $ledgerID = $this->getLedgerIdFromCode(5104); // Interest on Contribution
        $request = \request()->input();
        if (!isset($request['items'])) {
            $targetSavings['items'] = [$request];
        } else {
            $targetSavings['items'] = $request['items'];
        }
        $counter = 0;
        $allRequests = [];

        foreach ($targetSavings['items'] as $savingsItems) {
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'member_target_savings_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($savingsItems) {
                            $query->where('id', $savingsItems['member_target_savings_account'])
                                ->where('group_id', '46');
                        }),
                ],
            ];

            $validator = Validator::make($savingsItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }
            $savingsItems['debit_ledger'] = $ledgerID; // Interest on Contribution
            $savingsItems['credit_ledger'] = $savingsItems['member_target_savings_account']; //Member Target Savings Account 

            $allRequests['items'][$counter] = $savingsItems;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with data received");
        }
    }
}
