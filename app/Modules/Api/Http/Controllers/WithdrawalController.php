<?php

namespace App\Modules\Api\Http\Controllers;

use App\Modules\Api\Http\Controllers\AppBaseController;
use App\Modules\Api\Traits\DebitCredit;
use App\Modules\Base\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class WithdrawalController extends AppBaseController
{
    use DebitCredit;
    public function unscheduledWithdrawal()
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $withdrawal['items'] = [$request];
        } else {
            $withdrawal['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($withdrawal['items'] as $accountItems) {
            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);

            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_contribution_account'])
                                ->where('group_id', '43');
                        }),
                ],
            ];
            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($accountItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($accountItems) {
                                $query->where('id', $accountItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($accountItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $accountItems['bank_id'];
            }

            $accountItems['credit_ledger'] = $ledgerID; //Bank/Cash;
            $accountItems['debit_ledger'] = $accountItems['member_contribution_account'];

            $allRequests['items'][$counter] = $accountItems;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }

        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Loan Repayment");
        }
    }


    public function withdrawFromDeposit(Request $request)
    {
        $request = \request()->input();
        if (!isset($request['items'])) {
            $withdrawal['items'] = [$request];
        } else {
            $withdrawal['items'] = $request['items'];
        }
        $allRequests = [];
        $counter = 0;
        foreach ($withdrawal['items'] as $accountItems) {
            $accountItems['payment_type'] = strtolower($accountItems['payment_type']);
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'payment_type' => 'required|in:' . implode(',', $this->postingType),
                'description' => 'required',
                'member_contribution_account' => [
                    'required',
                    'exists:tenant_conn.ca_ledgers,id',
                    Rule::exists('tenant_conn.ca_ledgers', 'id')
                        ->where(function ($query) use ($accountItems) {
                            $query->where('id', $accountItems['member_contribution_account'])
                                ->where('group_id', '43');
                        }),
                ],
            ];

            $validator = Validator::make($accountItems, $rules);
            if ($validator->fails()) {
                $object = new \stdClass();
                $object->errorMessage = $validator->errors()->all();
                $object->index = $counter;
                $this->setSaveErrors($object);
            }

            $ledgerID = $this->getLedgerIdFromCode(1261);
            if ($accountItems['payment_type'] == 'bank') {
                $rules = [
                    'bank_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                        Rule::exists('tenant_conn.ca_ledgers', 'id')
                            ->where(function ($query) use ($accountItems) {
                                $query->where('id', $accountItems['bank_id'])
                                    ->where('group_id', '16');
                            }),
                    ],
                ];
                $validator = Validator::make($accountItems, $rules);
                if ($validator->fails()) {
                    $object = new \stdClass();
                    $object->errorMessage = $validator->errors()->all();
                    $object->index = $counter;
                    $this->setSaveErrors($object);
                }
                $ledgerID = $request['bank_id'];
            }

            $accountItems['debit_ledger'] = $accountItems['member_contribution_account'];
            $accountItems['credit_ledger'] = $ledgerID; //cash-bank

            $allRequests['items'][$counter] = $accountItems;

            $counter++;
        }

        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 400);
        }
        try {
            return $this->makePosting($allRequests);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return $this->sendError("There was a problem with Withdraw From Deposit");
        }
    }
}
