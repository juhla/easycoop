<?php

namespace App\Modules\Api\Models;

use App\Modules\Base\Traits\NullingDB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use NullingDB, EntrustUserTrait;
    use HasApiTokens, Notifiable;
    //use Auditable;

    protected $connection = 'mysql';
    protected $table = 'users';

    public function getTableName()
    {
        return $this->table;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'active', 'db', 'unique_code', 'username', 'password'];
    protected $hidden = ['password', 'unique_code'];


    /**
     * Update user last login time
     */
    public function updateOnline()
    {
        $this->last_active_time = time();
        $this->save();
    }

    /**
     * update user online status
     * @param $s
     * @return bool
     */
    public function updateStatus($s)
    {
        if (!\Auth::user()) return false;
        $this->online_status = $s;

        $this->save();
    }

    /**
     * Check if the user has activated his account
     * @return bool
     */
    public function isVerified()
    {
        if ($this->confirmed == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the account is active
     * @return bool
     */
    public function isActive()
    {
        if ($this->active == 1) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $permission
     * @return bool
     *
     */
    public function hasAccess($permission)
    {
        $permissions = @unserialize($this->permissions);
        if (!$permissions) return false;
        if (isset($permissions[$permission]) or in_array($permission, $permissions)) return true;
        return false;
    }


    public function displayAvatar()
    {
        if ($this->profile_pic) {
            return asset('uploads/thumbs/thumb_' . $this->profile_pic);
        } else {
            return asset('images/avatar_default.jpg');
        }
    }

}
