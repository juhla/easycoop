<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['middlewareGroups' => ['auth:api'], 'prefix' => 'api'], function () {
    Route::get('/', function () {
        dd('API for BMAC / EASY COOP Integration');
    });
    Route::post('sign-up', ['uses' => 'RegisterController@create']);
    Route::post('login', 'LoginController@login');
    Route::post('web-login', 'LoginController@webLogin');
    Route::get('logout', 'LoginController@logout');
});


Route::group([
    'middleware' => ['auth:api', 'api_tenant'],
], function () {

    //Route::get('/list-members-loan-account', 'MembersLoanController@listMembersLoanAccount');
    Route::post('/invest-money', 'InvestMoneyController@investMoney');
    Route::post('/unscheduled-withdrawal', 'WithdrawalController@unscheduledWithdrawal');
    Route::Resource('/interest_accrued', 'InterestAccruedController');
    //Route::Resource('/withdrawal_from_deposit', 'WithdrawalFromDepositController');
    //Route::Resource('/members_at_scheduled_date', 'WithdrawalAtScheduledDteController');


    //Route::get('/list-members-contribution-account', 'MembersContributionController@listMembersContributionAccount');
    Route::post('/interest_accrued', 'InterestAccruedController@interestAccrued');
    Route::post('/withdrawal/unscheduled-withdrawal-penalty', 'WithdrawalController@penaltiesFromUnscheduledWithdrawal');


    Route::post('/loan/repay', 'LoanController@loanRepayment');


    //Route::get('/members-contribution', 'ContributionsController@membersContribution');


    Route::group(['prefix' => 'withdrawal'], function () {
        Route::post('/from_deposit', 'WithdrawalController@withdrawFromDeposit');
        Route::post('/at_schedule', 'WithdrawalController@withdrawAtScheduledTime');
        Route::post('/unscheduled', 'WithdrawalController@unscheduledWithdrawal');
        Route::post('/penalties', 'WithdrawalController@penaltiesFromUnscheduledWithdrawal');
    });

    Route::group(['prefix' => 'bank'], function () {
        Route::get('/all', 'BankController@listBank'); ///
        Route::post('/create', 'BankController@store');
        Route::get('/{id}', 'BankController@show');
    });

    Route::group(['prefix' => 'contribution'], function () {
        Route::get('/all', 'ContributionsController@listMembersContributionAccount'); ///
        Route::get('/{id}', 'ContributionsController@show');
        Route::post('/create', 'ContributionsController@store');
        Route::post('/members_contribution', 'ContributionsController@membersContribution');
        Route::post('/interest_accrued', 'ContributionsController@interestAccrued');
        Route::post('/interestOnContributionPaid', 'ContributionsController@interestOnContributionPaid');
        Route::post('/withdrawalFromContribution', 'ContributionsController@withdrawalFromContribution');

    });


    Route::group(['prefix' => 'target_savings'], function () {
        Route::get('/all', 'TargetSavingsController@listSavings'); ///
        Route::get('/{id}', 'TargetSavingsController@show');
        Route::post('/create', 'TargetSavingsController@store');
        Route::post('/make', 'TargetSavingsController@make');
        Route::post('/withdrawAtScheduledTime', 'TargetSavingsController@withdrawAtScheduledTime');
        Route::post('/penaltiesFromUnscheduledWithdrawal', 'TargetSavingsController@penaltiesFromUnscheduledWithdrawal');


    });

    Route::group(['prefix' => 'equity'], function () {
        Route::get('/all', 'EquityController@listAccount'); ///
        Route::get('/{id}', 'EquityController@show');
        Route::post('/create', 'EquityController@store');
        Route::post('/purchaseOfShares', 'EquityController@purchaseOfShares');
        Route::post('/saleOfShares', 'EquityController@saleOfShares');
        Route::post('/saleOfSharesBetweenMembers', 'EquityController@saleOfSharesBetweenMembers');

    });

    Route::group(['prefix' => 'loan'], function () {
        Route::get('/all', 'LoanController@listMembersLoanAccount');
        Route::get('/{id}', 'LoanController@show');
        Route::post('/create', 'LoanController@store');
        Route::post('/loanDisburse', 'LoanController@membersLoan');
        Route::post('/repayment', 'LoanController@loanRepayment');
        Route::post('/penaltiesOnLoanAccrued', 'LoanController@penaltiesOnLoanAccrued');
        Route::post('/penaltiesOnLoanReceived', 'LoanController@penaltiesOnLoanReceived');
        //Route::post('/disburse', 'LoanController@disburseLoanWithInterest');
        //Route::post('/disburseWithoutInterest', 'LoanController@disburseLoanWithoutInterest');
        Route::post('/interest_accrued', 'LoanController@interestAccrued');
        Route::post('/interestOnLoanReceived', 'LoanController@interestOnLoanReceived');
    });


});




