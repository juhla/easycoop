<?php

namespace App\Modules\Api\Traits;


use App\Modules\Api\Utils\ResponseUtil;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Response;


trait ApOauth
{

    private $url = "http://137.117.211.230:9091/oauth/token";
    //private $url2 = "https://api.myeasycoop.com/oauth/token";
    private $client_id = "bmac-client";
    private $client_secret = "bmac!2019!";
    private $username = "demouser5";
    private $password = "Abc@123";
    private $isError = false;
    private $isErrorMessage = "";


    function base64url_decode($token)
    {
        return json_decode(base64_decode(str_replace('_', '/', str_replace('-', '+', explode('.', $token)[1]))));
    }

    public function init($username, $password)
    {
        $tokenContent = "grant_type=password&username={$username}&password={$password}";
        $authorization = base64_encode("$this->client_id:$this->client_secret");
        $tokenHeaders = array(
            "Authorization: Basic {$authorization}",
            "Content-Type: application/x-www-form-urlencoded"
        );
        $token = curl_init();
        curl_setopt($token, CURLOPT_URL, $this->url);
        curl_setopt($token, CURLOPT_HTTPHEADER, $tokenHeaders);
        curl_setopt($token, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($token, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($token, CURLOPT_POST, true);
        curl_setopt($token, CURLOPT_POSTFIELDS, $tokenContent);
        $response = curl_exec($token);
        curl_close($token);
        //dd($response);
        $token_array = json_decode($response, true);
        // dd($token_array);
        if (isset($token_array['error'])) {
            $this->isError = true;
            $this->isErrorMessage = empty($token_array['error_description']) ?  $token_array['message'] : $token_array['error_description'];
        } else {
            $this->isError = false;
            session()->put("ApOauth", $token_array);
            $result = $this->base64url_decode($token_array['access_token']);
            session()->put("ApOauth.decoded", $result);
        }

    }

    public function refreshToken()
    {

    }
}

