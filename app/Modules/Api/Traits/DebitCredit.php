<?php

namespace App\Modules\Api\Traits;

use App\Modules\Api\Models\AccountGroup;
use App\Modules\Api\Models\Ledger;
use App\Modules\Api\Models\Transaction;
use App\Modules\Api\Models\TransactionItem;
use App\Modules\Api\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait DebitCredit
{


    protected $saveErrors = [];
    protected $isSaved;
    protected $hasError = false;
    protected $validated;
    protected $savedID = [];

    /**
     * @return mixed
     */
    public function getSavedID()
    {
        return $this->savedID;
    }

    /**
     * @param mixed $savedID
     */
    public function setSavedID($savedID)
    {
        $this->savedID[] = $savedID;
    }


    /**
     * @return mixed
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * @param mixed $validated
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;
    }


    /**
     * @return mixed
     */
    public function getisSaved()
    {
        return $this->isSaved;
    }

    /**
     * @param mixed $isSaved
     */
    public function setIsSaved($isSaved): void
    {
        $this->isSaved = $isSaved;
    }

    /**
     * @return mixed
     */
    public function getSaveErrors()
    {
        return $this->saveErrors;
    }

    /**
     * @param mixed $saveErrors
     */
    public function setSaveErrors($saveErrors): void
    {
        $this->saveErrors[] = $saveErrors;
    }


    function getLedgerIdFromCode($code)
    {
        try {
            $ledger = Ledger::where('code', $code)->first();
            return $ledger->id;
        } catch (\Exception $exception) {
            return null;
        }
    }

    function makePosting($data)
    {

        if (!isset($data['items'])) {
            $newRequest['items'] = [$data];
        } else {
            $newRequest['items'] = $data['items'];
        }

        $allTheRequest = $newRequest['items'];
        $counter = 0;
        foreach ($allTheRequest as $eachRequest) {
            $counter++;
            $rules = [
                'transaction_date' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric',
                'description' => 'required',
                'debit_ledger' => "required|exists:tenant_conn.ca_ledgers,id",
                'credit_ledger' => "required|exists:tenant_conn.ca_ledgers,id",
            ];
            $validator = Validator::make($eachRequest, $rules);
            if ($validator->fails()) {
                $this->setSaveErrors($this->flatten($validator->errors()->all(), "; ") . ". index:: [" . $counter . "]");
            }
        }
        if (!empty($this->getSaveErrors())) {
            return $this->sendErrorWithData($this->getSaveErrors(), "There is an Error in your request", 404);
        }

        DB::connection('tenant_conn')->beginTransaction();
        try {
            foreach ($allTheRequest as $eachRequest) {
                $eachRequest['reference_no'] = 'TRA' . time() . GenerateRandomString(5, 'ALPHA');
                $transaction = new Transaction;
                $transaction->transaction_date = $eachRequest['transaction_date'];
                $transaction->amount = $eachRequest['amount'];
                $transaction->transaction_type = 'api';
                $transaction->reference_no = $eachRequest['reference_no'];
                $transaction->description = $eachRequest['description'];
                $transaction->save();


                $item1 = new TransactionItem;
                $item1->ledger_id = $eachRequest['debit_ledger'];
                $item1->amount = $eachRequest['amount'];
                $item1->item_description = $eachRequest['description'];
                $item1->dc = 'D';
                $transaction->item()->save($item1);


                $item2 = new TransactionItem;
                $item2->ledger_id = $eachRequest['credit_ledger'];
                $item2->amount = $eachRequest['amount'];
                $item2->item_description = $eachRequest['description'];
                $item2->dc = 'C';
                $transaction->item()->save($item2);
                $this->setSavedID($transaction->id);
            }

        } catch (\Exception $exception) {
            DB::connection('tenant_conn')->rollBack();
            return $this->sendError($exception->getMessage());

        }
        $this->isSaved = true;
        DB::connection('tenant_conn')->commit();
        //$models = Transaction::whereIn('id', $this->getSavedID())->with('item')->get();
        $models = Transaction::whereIn('id', $this->getSavedID())->get();
        return $this->sendResponse($models->toArray(), 'Transaction created successfully');

    }

}

