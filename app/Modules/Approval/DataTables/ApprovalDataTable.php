<?php

namespace App\Modules\Approval\DataTables;

use App\Modules\Approval\Models\FiscalYear;
use App\Modules\Approval\Models\Transaction;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class ApprovalDataTable extends DataTable
{
    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable
            ->addColumn('pcv', function ($__res) {
                return $__res->transactions->pv_receipt_no;
            })
            ->editColumn('transaction_type', function ($__res) {
                $transaction_type = implode('-', array_map('ucfirst', explode('-', $__res->transaction_type)));
                return $transaction_type;
            })
            ->editColumn('approve_status', function ($__res) {
                if ($__res->approve_status == 0) {
                    $approve_status = 'Pending';
                } elseif ($__res->approve_status == 1) {
                    $approve_status = 'Approved';
                } else {
                    $approve_status = 'Rejected';
                }
                return $approve_status;
            })
            ->editColumn('amount', function ($__res) {
                return formatNumber($__res->amount);
            })
            ->addColumn('description', function ($__res) {
                return $__res->description;
            })
            ->filterColumn('description', function ($query, $keyword) {
                // $query->whereHas('ca_transactions', function ($query) use ($keyword) {
                $sql = "description  like ? ";
                $query->whereRaw($sql, ["%{$keyword}%"]);
                // });
            })
            ->filterColumn('credit', function ($query, $keyword) {
                $query->whereHas('ledger', function ($query) use ($keyword) {
                    $sql = "name  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->filterColumn('pcv', function ($query, $keyword) {
                $query->whereHas('transactions', function ($query) use ($keyword) {
                    $sql = "pv_receipt_no  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->editColumn('date', function ($__res) {
                return date('d/m/Y', strtotime($__res->transaction_date));
            })
            ->orderColumn('date', 'ca_transactions.transaction_date $1')
            ->addColumn('action', function ($transaction) {
                return view('approval::actions.datatables_actions', compact('transaction'))->render();
            })
            ->rawColumns(['action']);

        return $dataTable;

    }

    public function query(Transaction $model)
    {

        $type = $this->getType();
        $start_date = session('start_date');
        $end_date = session('end_date');

        $condition = array(
            'approve_status' => '0',
            'flag' => 'Active',
        );
        $query = $model->with(['item_without_scope.ledger'])
            ->withoutGlobalScope('active')
            ->where($condition)->newQuery();

        if (!empty($start_date)) {
            $start = Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '>=', $start);
        }
        if (!empty($end_date)) {
            $end = Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '<=', $end);
        }

        if (empty($start_date) && empty($end_date)) {

            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $query->whereBetween('ca_transactions.transaction_date', [$fiscalYear->begins, $fiscalYear->ends]);
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('batchDT')
            ->columns($this->getColumns())
            ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['title' => 'Date', 'data' => 'date', 'footer' => 'date', 'searchable' => false],
            ['title' => 'Status', 'data' => 'approve_status', 'footer' => 'approve_status'],
            ['title' => 'Ref No', 'data' => 'reference_no', 'footer' => 'reference_no'],
            ['title' => 'Description', 'data' => 'description', 'footer' => 'description'],
            ['title' => 'Amount', 'data' => 'amount', 'footer' => 'amount'],
            ['title' => 'Transaction Type', 'data' => 'transaction_type', 'footer' => 'transaction_type'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'batch_' . date('YmdHis');
    }
}
