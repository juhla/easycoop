<?php

namespace App\Modules\Approval\Http\Controllers;

use Auth;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Approval\DataTables\ApprovalDataTable;
use App\Modules\Approval\Models\Approval;
use App\Modules\Approval\Models\Comment;
use App\Modules\Approval\Models\Transaction;
use App\Modules\Approval\Models\TransactionItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ApprovalController extends Controller
{
    public function __construct(Request $request, Approval $approval)
    {
        $this->middleware(['tenant']);
        $this->request = $request;
        $this->approval = $approval;
    }

    /**
     * method to display all batch entry
     * @return $this
     */
    public function index(ApprovalDataTable $table)
    {

        //dd($fiscalYear);
        $approval = Approval::get();
        $approval_status = $approval[0]['approval'];
        $inputs = request()->all();
        // $this->hasAccess(['add_journal_entry', 'view_journal_entry']);
        $title = 'Transaction Entry (Approval)';
        // $type = 'batch-posting';

        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');

        $table->setType($type);
        return $table->render('approval::index', compact('type', 'approval_status', 'title', 'start_date', 'end_date'));

    }

    public function setting()
    {

        try {
            $approval = Approval::get();

            $approval_status = $approval[0]['approval'];

            if ($approval_status == '1') {
                $__status = [
                    'approval' => '0',
                ];
            } else {
                $__status = [
                    'approval' => '1',
                ];
            }
            $change_status = $approval[0]->update($__status);

        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

        return response()->json([
            'data' => [
                'status' => $change_status,
            ],
        ]);
    }

    public function approve(Request $request, $id)
    {

        $__approve = [
            'approve_status' => '1',
        ];
        // Update the  row on the ca_transactions table
        $transaction = Transaction::withoutGlobalScopes()->find($id);
        $transaction->update($__approve);

        //Update the transaction on the ca_transaction_items table
        $transaction_item = TransactionItem::withoutGlobalScopes()
            ->where('transaction_id', $id);
        $transaction_item->update($__approve);

        $message = "The Transaction has been succesfully approved";
        return response()->json(compact('data', 'message'));
    }

    public function reject(Request $request)
    {

        $trnx_id = $request['trnx_id'];
        $reject_comment = $request['reject_comment'];
        $__comment = [
            'trnx_id' => $trnx_id,
            'content' => $reject_comment,
            'rejected_by' => Auth::user()->id,
        ];

        $comment = Comment::create($__comment);
        if ($comment) {

            $__reject = [
                'approve_status' => '-1',
            ];

            // Update the  row on the ca_transactions table
            $transaction = Transaction::withoutGlobalScope('active')->find($trnx_id);
            $trnx_update = $transaction->update($__reject);

            //Update the transaction on the ca_transaction_items table
            $transaction_item = TransactionItem::withoutGlobalScope('active')->where('transaction_id', $trnx_id)->update($__reject);

        }
        $message = "The Transaction has been succesfully rejected";
        return response()->json(compact('data', 'message'));
    }
}
