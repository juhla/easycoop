<?php

namespace App\Modules\Approval\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $connection = 'tenant_conn';

    protected $table = 'ca_comments';

    protected $fillable = [
        'trnx_id',
        'content',
        'rejected_by',
    ];
    //
}
