<?php

use App\Modules\Base\Traits\Acl;

?>
{{-- {{$transaction}} --}}

<div class="btn-group btn-group-sm">
    <button type="button" class="btn btn-complete dropdown-toggle"
            data-toggle="dropdown"><span class="caret"></span></button>
    <ul class="dropdown-menu"
        style="right: 0; left: auto; overflow: visible !important">
        @if(Acl::can('view_receipt'))
            <li><a href="#" class="view" data-value="{{$transaction->id}}">View</a></li>
        @endif
        @if(Acl::can('edit_receipt'))
            <li><a href="#" class="approve" data-url-approve="/approval/transaction/approve/"
                   data-value="{{$transaction->id}}">Approve</a></li>
        @endif
        {{--<li><a href="#">Split Transaction</a></li>--}}
        {{--<li><a href="#">Create Invoice payment</a></li>--}}
        {{--<li><a href="#">Move Entry</a></li>--}}
        @if(Acl::can(['delete_receipt']))
            <li class="divider"></li>
            <li><a href="javascript:;" data-url-reject="/approval/transaction/reject/" class="reject_comment"
                   data-value="{{ $transaction->id }}">Reject</a></li>
        @endif
    </ul>
</div>