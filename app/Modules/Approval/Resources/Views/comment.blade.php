<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="commentModal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog ">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Reason for <span class="semi-bold">Rejection</span></h5>
                        {{--<p class="p-b-10">Some description could go here.</p>--}}
                    </div>
                    <div class="modal-body">
                        <form id="rejection_comment" action="{{ url('/approval/transaction/reject/') }}" method="post" style="margin-bottom:40px">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <input type="hidden" id="trnx_id" name="trnx_id">
                            <textarea name="reject_comment" id="reject_comment" placeholder="   Describe reason for rejection..." style="width:100%; min-height:200px;resize: none;"></textarea>
                            <button type="submit" class="btn btn-sm btn-success pull-right" title="Reject" style="margin-top:10px"><i class="fa fa-close"></i>
                                Reject
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <!-- /.modal-dialog -->