<?php

use App\Modules\Base\Traits\Acl;

?>
@extends('layouts.main')

@section('content')
<section class="hbox stretch">
    {{--<aside class="aside-md bg-white b-r" id="subNav">
        <div class="wrapper b-b header">Submenu Header</div>
        <ul class="nav">
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at
                    ultricies</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
            </li>
        </ul>
    </aside>--}}
    <aside>
        <section class="vbox">
            <header class="header bg-white b-b clearfix">
                <div class="row m-t-sm">
                    <div class="col-sm-3 m-b-xs">
                        {{-- @if(Acl::can('delete_journal_entry')) --}}
                        {{-- <button type="button" class="btn btn-sm btn-danger bulk-del" title="Remove" disabled><i class="fa fa-trash-o"></i>
                            Delete
                        </button>
                        @endif --}}
                        {{-- @if(Acl::can('add_journal_entry'))
                        <a href="{{ url('approval/add-new') }}" class="btn btn-sm btn-primary">
                            <i class="fa fa-plus"></i> Add New
                        </a> --}}
                        <input type="checkbox" @if( $approval_status == '1' ) checked @endif data-toggle="toggle" data-onstyle="success">
                        {{-- @endif --}}
                    </div>
                    @if(Acl::can(['add_journal_entry']))
                    @include('approval::actions.search')
                    @endif

                </div>
            </header>
            <section class="scrollable wrapper w-f">
                <section class="panel panel-default">
                    <div class="table-responsive">
                        @section('css')
                        @include('layouts.datatables_css')
                        @endsection
                        {!! $dataTable->table(['width' => '100%']) !!}
                        @section('scripts_dt')
                        @include('layouts.datatables_js')
                        {!! $dataTable->scripts() !!}
                        @endsection
                    </div>
                </section>
            </section>
            <footer class="footer bg-white b-t">
                <div class="row text-center-xs">
                    {{--<div class="col-md-6 hidden-sm">
                        <p class="text-muted m-t">Showing 20-30 of 50</p>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right text-center-xs">
                        <ul class="pagination pagination-sm m-t-sm m-b-none">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>--}}
                    {{-- {!! $batchs->render() !!} --}}
                </div>
            </footer>
        </section>
    </aside>
</section>
@include('approval::view')
@include('approval::comment')
@stop

@section('scripts')
<script src="{{ asset('js/scripts/transactions.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/scripts/approval.js') }}" type="text/javascript"></script>

    {{-- <script src="{{asset('js/bootstrap-toggle/jquery/jquery.min.js')}}"></script> --}}
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    {{-- <link href="{{asset('js/bootstrap-toggle/css/bootstrap.min.css')}}" rel="stylesheet"> --}}

    <script>
        $(function() {
            var toggleClass = $('input:checkbox');
            toggleClass.bootstrapToggle();
            toggleClass.change(function() {
                var status = $(this).prop('checked');
                var _token = "{{csrf_token()}}";
                $.ajax({
                    url:  'approval/settings',
                    type: 'POST',
                    data: {  _token: _token },
                    context: this,
                    success: function (e) {
                        // $("input[name='ids']:checked").each(function () {
                        //     $(this).fadeOut('slow', function () {
                        //         $(this).closest("tr").remove();
                        //     });
                        // });
                        // $("input:checkbox").prop('checked', false);
                       
                    }
            })
        })
        })
    </script>

@endsection
@section('scripts')

<script>
    $('#clearBTN').click(function () {
        $('#start_date').val('');
        $('#end_date').val('');
    });
</script>
@stop
