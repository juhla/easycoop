<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="detailsModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Transaction <span class="semi-bold">Details</span></h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <p>Date: <span class="view_date"></span> </p>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="">GL Account</th>
                            <th width="">Debit Amount</th>
                            <th width="">Credit Amount</th>
                        </tr>
                        </thead>
                        <tbody align="center" class="view-transaction">
                        </tbody>
                    </table>
                    <p>Description: <span class="view_description"></span> </p>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->