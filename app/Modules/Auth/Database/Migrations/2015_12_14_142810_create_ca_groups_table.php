<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateCaGroupsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            NestedSet::columns($table);
            $table->tinyInteger('is_system_account')->default(0);
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_groups');
    }
}
