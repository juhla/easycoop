<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_ledgers', function (Blueprint $table) {
            $table->string('id');
            $table->string('name');
            $table->string('code');
            $table->double('opening_balance')->nullable();
            $table->string('opening_balance_type');
            $table->integer('group_id')->unsigned();
            $table->tinyInteger('is_system_account')->nullable();
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_ledgers');
    }
}
