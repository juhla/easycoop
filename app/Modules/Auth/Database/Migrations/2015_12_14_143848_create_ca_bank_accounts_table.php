<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ledger_code');
            $table->string('account_name');
            $table->string('account_number');
            $table->string('account_type');
            $table->string('bank');
            $table->string('currency_code');
            $table->tinyInteger('is_default');
            $table->date('last_reconciled_date');
            $table->double('ending_reconciled_balance');
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_bank_accounts');
    }
}
