<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_customers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('ledger_code');
            $table->string('name');
            $table->string('email');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('account_no')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('website')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('sync')->default(0);
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_customers');
    }
}
