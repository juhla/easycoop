<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no');
            $table->char('customer_id', 36);
            $table->string('invoice_type');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->double('amount_due');
            $table->double('total_amount');
            $table->double('amount_paid')->nullable();
            $table->text('memo');
            $table->integer('payment_term_id')->unsigned()->nullable()->default(0);
            $table->integer('transaction_id')->nullable();
            $table->string('currency')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_invoice');
    }
}
