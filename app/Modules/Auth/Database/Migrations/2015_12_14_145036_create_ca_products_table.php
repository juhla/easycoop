<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_code')->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->double('price');
            $table->string('tax')->nullable();
            $table->string('flag')->default('Active');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_products');
    }
}
