<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaSettingsInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_settings_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->string('template')->default('template_1');
            $table->string('company_logo');
            $table->tinyInteger('display_logo');
            $table->string('product_title');
            $table->integer('default_payment_term');
            $table->string('default_invoice_title');
            $table->string('default_invoice_footer');
            $table->string('default_estimate_title');
            $table->string('default_estimate_footer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_settings_invoice');
    }
}
