<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ca_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_no')->nullable();
            $table->double('amount');
            $table->date('transaction_date');
            $table->string('transaction_type');
            $table->string('description')->nullable();
            $table->tinyInteger('payment_type')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('cheque_number')->nullable();
            $table->string('customer_vendor')->nullable();
            $table->string('pv_receipt_no')->nullable();
            $table->integer('memo_posted')->nullable()->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('flag')->default('Active');
            $table->timestamps();
        });

        Schema::create('ca_transaction_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->string('ledger_id');
            $table->double('amount');
            $table->string('dc');
            $table->string('item_description')->nullable();
            $table->date('reconciliation_date')->nullable();
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ca_transaction_items');
        Schema::drop('ca_transactions');
    }
}
