<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('invoice_no');
            $table->string('customer');
            $table->string('description')->nullable();
            $table->double('amount');
            $table->tinyInteger('status')->default(0);
            $table->string('flag')->default('Active');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('credit_sales');
    }
}
