<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChequePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('pv_no');
            $table->string('mode_of_payment');
            $table->string('bank')->nullable();
            $table->string('cheque_no')->nullable();
            $table->string('supplier')->nullable();
            $table->string('description')->nullable();
            $table->double('amount');
            $table->tinyInteger('status')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('flag')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cheque_payment');
    }
}
