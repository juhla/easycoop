<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewWidgetUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('user_widgets', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('widget_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamps();
            });
        } catch (Exception $exception) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_widgets');
    }
}
