<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('todos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('todo');
                $table->integer('status')->comment('0=pending, 1=done');
                $table->softDeletes();
                $table->timestamps();
            });
        } catch (Exception $exception) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todo');
    }
}
