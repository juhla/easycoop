<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewWidgetDashboardUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            Schema::create('user_dashboard_widgets', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('widget_dashboard_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamps();
            });
        }catch (Exception $exception){

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_dashboard_widgets');
    }
}
