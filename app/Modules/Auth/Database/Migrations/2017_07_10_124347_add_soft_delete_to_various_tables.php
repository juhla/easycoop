<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class AddSoftDeleteToVariousTables extends Migration
{
    public function up()
    {
        $DB = DB::connection('tenant_conn')->getDatabaseName();
        $tables = DB::getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $table) {
            try {
                $hasColumn = Schema::hasColumn($table, 'flag');
                $notHaveDeletedAt = !Schema::hasColumn($table, 'deleted_at');
                if ($hasColumn && $notHaveDeletedAt) {
                    Schema::table($table, function (Blueprint $table) {
                        $table->softDeletes();
                    });
                }
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        }

    }

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function getTableColumns($table)
    {
        return Schema::getColumnListing($table);
    }

}
