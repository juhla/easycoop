<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatabaseTablePatches extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        //patch ca_customers
        try {
            if (!Schema::hasColumn('ca_customers', 'sync')) {
                Schema::table('ca_customers', function (Blueprint $table) {
                    $table->string('sync')->default(0);
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            //patch ca_vendors
            if (!Schema::hasColumn('ca_vendors', 'sync')) {
                Schema::table('ca_vendors', function (Blueprint $table) {
                    $table->string('sync')->default(0);
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('ca_transactions', 'memo_posted')) {
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->integer('memo_posted')->nullable()->default(0);
                });
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('ca_invoice', 'transaction_id')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->integer('transaction_id')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('cheque_payment')) {
                Schema::table('cheque_payment', function (Blueprint $table) {
                    $table->string('mode_of_payment');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('cheque_receipt', 'mode_of_payment')) {
                Schema::table('cheque_receipt', function (Blueprint $table) {
                    $table->string('mode_of_payment');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('cheque_receipt', 'mode_of_payment')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->integer('mode_of_payment');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('cheque_payment', 'mode_of_payment')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->string('mode_of_payment');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('audits')) {

                Schema::table('audits', function ($table) {
                    $table->string('auditable_id', 50)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('user_dashboard_widgets')) {
                Schema::create('user_dashboard_widgets', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('widget_dashboard_id')->unsigned();
                    $table->integer('user_id')->unsigned();
                    $table->timestamps();

                });
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('todos')) {
                Schema::create('todos', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('todo');
                    $table->integer('status')->comment('0=pending, 1=done');
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('user_license_packages', 'renewal_price')) {
                Schema::table('user_license_packages', function (Blueprint $table) {
                    $table->string('renewal_price');
                });
            }

        } catch (\Exception $e) {
        }

        try {
            if (!Schema::hasColumn('user_license_packages', 'type')) {
                Schema::table('user_license_packages', function (Blueprint $table) {
                    $table->string('type');
                });
            }

        } catch (\Exception $e) {
        }

        try {
            if (!Schema::hasTable('user_widgets')) {
                Schema::create('todos', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('widget_id')->unsigned();
                    $table->integer('user_id')->unsigned();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
        }

        try {
            if (!Schema::hasColumn('widgets', 'mode')) {
                Schema::table('widgets', function (Blueprint $table) {
                    $table->string('mode')->default('0');
                });
            }

        } catch (\Exception $e) {
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dashboard_widgets');
        Schema::dropIfExists('tbl_general_payment');
        Schema::dropIfExists('tbl_general_payslip');
        Schema::dropIfExists('tbl_designations');
        Schema::dropIfExists('cheque_payment');
        Schema::dropIfExists('audits');
        Schema::dropIfExists('todos');
        Schema::dropIfExists('user_widgets');

        if (Schema::hasColumn('ca_customers', 'sync')) {
            Schema::table('ca_customers', function (Blueprint $table) {
                $table->dropColumn('sync');
            });
        }

        if (Schema::hasColumn('ca_vendors', 'sync')) {
            Schema::table('ca_vendors', function (Blueprint $table) {
                $table->dropColumn('sync');
            });
        }

        if (Schema::hasColumn('ca_transactions', 'memo_posted')) {
            Schema::table('ca_transactions', function (Blueprint $table) {
                $table->dropColumn('memo_posted');
            });
        }

        if (Schema::hasColumn('ca_invoice', 'transaction_id')) {
            Schema::table('ca_invoice', function (Blueprint $table) {
                $table->dropColumn('transaction_id');
            });
        }

        if (Schema::hasColumn('cheque_receipt', 'mode_of_payment')) {
            Schema::table('cheque_receipt', function (Blueprint $table) {
                $table->dropColumn('mode_of_payment');
            });
        }
        if (Schema::hasColumn('cheque_receipt', 'mode_of_payment')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('mode_of_payment');
            });
        }

        if (Schema::hasColumn('cheque_payment', 'mode_of_payment')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('mode_of_payment');
            });
        }
    }
}
