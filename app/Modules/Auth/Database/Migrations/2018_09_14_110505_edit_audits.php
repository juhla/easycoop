<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;

class EditAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('audits')) {
                Schema::create('audits', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger(Config::get('audit.user.foreign_key', 'user_id'))->nullable();
                    $table->string('event');
                    $table->morphs('auditable');
                    $table->text('old_values')->nullable();
                    $table->text('new_values')->nullable();
                    $table->text('url')->nullable();
                    $table->ipAddress('ip_address')->nullable();
                    $table->string('user_agent')->nullable();
                    $table->timestamps();
                });
                try {
                    Schema::table('audits', function (Blueprint $table) {
                        $table->string('auditable_id', '50')->change();
                    });
                } catch (\Exception $exception) {
                    app('sentry')->captureException($exception);
                }
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
