<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditGroups extends Migration
{


    public function up()
    {
        $this->fixGroup();
        try {
            DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            //DB::statement("ALTER TABLE ca_bank_accounts MODIFY account_type ENUM('Savings Account','Chequing Account','Current Account','Cash Account') NULL ");
            DB::statement("ALTER TABLE ca_bank_accounts MODIFY ending_reconciled_balance DOUBLE NULL ");
            Schema::table('ca_bank_accounts', function ($table) {
                $table->date('last_reconciled_date')->nullable()->change();
            });

        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

    }

    function fixGroup()
    {
        try {
            if (!Schema::hasColumn('ca_groups', '_lft')) {
                DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
                Schema::table('ca_groups', function ($table) {
                    $table->unsignedInteger('_lft');
                    $table->unsignedInteger('_rgt');
                    $table->integer('parent_id')->nullable()->default(null)->change();
                });
                $this->fixTable();
            }
        } catch (Exception $exception) {
            app('sentry')->captureException($exception);
        }
    }

    function fixTable()
    {
        $obj = new \App\Modules\Base\Models\AccountGroup();
        $obj->where('parent_id', 0)->update(['parent_id' => null]);
        $obj->fixTree();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
