<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVatToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ca_invoice', 'vat')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->tinyInteger('vat');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
