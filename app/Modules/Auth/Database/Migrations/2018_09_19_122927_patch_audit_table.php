<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatchAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasColumn('audits', 'auditable_id')) {
                Schema::table('audits', function (Blueprint $table) {
                    $table->string('auditable_id', '50')->change();
                });
            }
        } catch (\Exception $e) {
//            dd($e);
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
