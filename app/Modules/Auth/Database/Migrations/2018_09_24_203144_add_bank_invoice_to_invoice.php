<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankInvoiceToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ca_invoice', 'bank_invoice')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->string('bank_invoice', '100')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_invoice', function (Blueprint $table) {
            $table->dropColumn('bank_invoice');
        });
    }
}
