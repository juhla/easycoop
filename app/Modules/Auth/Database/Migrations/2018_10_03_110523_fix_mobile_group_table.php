<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixMobileGroupTable extends Migration
{
    public function up()
    {
        $this->fixTable();
    }

    function fixTable()
    {
        try {
            $obj = new \App\Modules\Base\Models\AccountGroup();
            //$obj->where('parent_id', 0)->update(['parent_id' => null]);
            $obj->fixTree();
        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
