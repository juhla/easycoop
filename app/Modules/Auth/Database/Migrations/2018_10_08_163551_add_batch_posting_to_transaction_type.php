<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddBatchPostingToTransactionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            //DB::statement("ALTER TABLE ca_transactions MODIFY transaction_type ENUM('inflow','outflow','journal','invoice','batch-posting') NULL ");
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
