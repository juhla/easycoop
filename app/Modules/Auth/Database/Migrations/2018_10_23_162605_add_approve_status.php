<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApproveStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ca_transactions', 'approve_status')) {
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->string('approve_status')->default('1');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasColumn('ca_transaction_items', 'approve_status')) {
                Schema::table('ca_transaction_items', function (Blueprint $table) {
                    $table->string('approve_status')->default('1');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
