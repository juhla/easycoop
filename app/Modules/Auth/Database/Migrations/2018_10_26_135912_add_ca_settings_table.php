<?php

use App\Modules\Base\Models\Approval;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCaSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('ca_settings')) {
                Schema::create('ca_settings', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('approval')->default('1');
                    $table->timestamps();
                });

                Approval::create([
                    'approval' => '0',
                ]);
            }
        } catch (Exception $exception) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ca_settings');
    }
}
