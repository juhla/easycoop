<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncomeTypeToInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('ca_invoice', function (Blueprint $table) {
                $table->string('income_type');
            });
        } catch (Exception $exception) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_invoice', function (Blueprint $table) {
            $table->dropColumn('income_type');
        });
    }
}
