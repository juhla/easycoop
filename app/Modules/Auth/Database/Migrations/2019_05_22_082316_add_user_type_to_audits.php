<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTypeToAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audits', function (Blueprint $table) {
            try {
                if (!Schema::hasColumn('audits', 'user_type')) {
                    Schema::table('audits', function (Blueprint $table) {
                        $table->string('user_type', '100')->nullable();
                    });
                }
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audits', function (Blueprint $table) {
            $table->dropColumn('user_type');
        });
    }
}
