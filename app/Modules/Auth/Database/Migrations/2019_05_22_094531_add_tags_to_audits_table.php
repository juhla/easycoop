<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTagsToAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audits', function (Blueprint $table) {
            try {
                if (!Schema::hasColumn('audits', 'tags')) {
                    Schema::table('audits', function (Blueprint $table) {
                        $table->string('tags', '100')->nullable();
                    });
                }
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audits', function (Blueprint $table) {
            $table->dropColumn('tags');
        });
    }
}
