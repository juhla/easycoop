<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FixEnumTablesFromBmac extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('ca_bank_accounts')) {
                DB::statement('ALTER TABLE ca_bank_accounts MODIFY flag varchar(191)');
                DB::statement('ALTER TABLE ca_bank_accounts MODIFY account_type varchar(191)');
                Schema::table('ca_bank_accounts', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('account_type')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_fiscal_year')) {
                DB::statement('ALTER TABLE ca_fiscal_year MODIFY flag varchar(191)');
                DB::statement('ALTER TABLE ca_fiscal_year MODIFY status varchar(191)');
                Schema::table('ca_fiscal_year', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('status')->default('Open')->change();
                });
            }

            if (Schema::hasTable('ca_groups')) {
                DB::statement('ALTER TABLE ca_groups MODIFY flag varchar(191)');
                Schema::table('ca_groups', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_ledgers')) {
                DB::statement('ALTER TABLE ca_ledgers MODIFY flag varchar(191)');
                DB::statement('ALTER TABLE ca_ledgers MODIFY opening_balance_type varchar(191)');
                Schema::table('ca_ledgers', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('opening_balance_type')->change();
                });
            }

            if (Schema::hasTable('ca_transactions')) {
                DB::statement('ALTER TABLE ca_transactions MODIFY flag varchar(191)');
                DB::statement('ALTER TABLE ca_transactions MODIFY transaction_type varchar(191)');
                DB::statement('ALTER TABLE ca_transactions MODIFY approve_status varchar(191)');
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('approve_status')->default('1')->change();
                });
            }

            if (Schema::hasTable('ca_invoice')) {
                DB::statement('ALTER TABLE ca_invoice MODIFY invoice_type varchar(191)');
                DB::statement('ALTER TABLE ca_invoice MODIFY flag varchar(191)');
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('invoice_type')->change();
                });
            }

            if (Schema::hasTable('ca_invoice_details')) {
                DB::statement('ALTER TABLE ca_invoice_details MODIFY flag varchar(191)');
                Schema::table('ca_invoice_details', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_invoice_items')) {
                DB::statement('ALTER TABLE ca_invoice_items MODIFY flag varchar(191)');
                Schema::table('ca_invoice_items', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_payment_terms')) {
                DB::statement('ALTER TABLE ca_payment_terms MODIFY flag varchar(191)');
                Schema::table('ca_payment_terms', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_products')) {
                DB::statement('ALTER TABLE ca_products MODIFY flag varchar(191)');
                Schema::table('ca_products', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

//            if (Schema::hasTable('ca_settings')) {
//                DB::statement('ALTER TABLE ca_settings MODIFY flag varchar(191)');
//                Schema::table('ca_settings', function (Blueprint $table) {
//                    $table->string('flag')->default('Active')->change();
//                });
//            }

            if (Schema::hasTable('ca_tax_types')) {
                DB::statement('ALTER TABLE ca_tax_types MODIFY flag varchar(191)');
                Schema::table('ca_tax_types', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_vendors')) {
                DB::statement('ALTER TABLE ca_vendors MODIFY flag varchar(191)');
                Schema::table('ca_vendors', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('ca_settings_invoice')) {
                DB::statement('ALTER TABLE ca_settings_invoice MODIFY template varchar(191)');
                Schema::table('ca_settings_invoice', function (Blueprint $table) {
                    $table->string('template')->change();
                });
            }


            if (Schema::hasTable('ca_transaction_items')) {
                DB::statement('ALTER TABLE ca_transaction_items MODIFY flag varchar(191)');
                DB::statement('ALTER TABLE ca_transaction_items MODIFY dc varchar(191)');
                DB::statement('ALTER TABLE ca_transaction_items MODIFY approve_status varchar(191)');
                Schema::table('ca_transaction_items', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                    $table->string('approve_status')->default("1")->change();
                });
            }

            if (Schema::hasTable('ca_customers')) {
                DB::statement('ALTER TABLE ca_customers MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ca_customers MODIFY flag varchar(191)');
                Schema::table('ca_customers', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('flag')->default('Active')->change();
                });
            }

            if (Schema::hasTable('cash_receipt')) {
                DB::statement('ALTER TABLE cash_receipt MODIFY flag varchar(191)');
                Schema::table('cash_receipt', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
            if (Schema::hasTable('cheque_payment')) {
                DB::statement('ALTER TABLE cheque_payment MODIFY flag varchar(191)');
                Schema::table('cheque_payment', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
            if (Schema::hasTable('cheque_receipt')) {
                DB::statement('ALTER TABLE cheque_receipt MODIFY flag varchar(191)');
                Schema::table('cheque_receipt', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
            if (Schema::hasTable('credit_purchase')) {
                DB::statement('ALTER TABLE credit_purchase MODIFY flag varchar(191)');
                Schema::table('credit_purchase', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
            if (Schema::hasTable('credit_sales')) {
                DB::statement('ALTER TABLE credit_sales MODIFY flag varchar(191)');
                Schema::table('credit_sales', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
