<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FixEnumTablesFromIms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {

            if (Schema::hasTable('ims_branches')) {
                DB::statement('ALTER TABLE ims_branches MODIFY sync varchar(191)');
                Schema::table('ims_branches', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_adjustment_journals')) {
                DB::statement('ALTER TABLE ims_adjustment_journals MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ims_adjustment_journals MODIFY adjustment_type varchar(191)');
                Schema::table('ims_adjustment_journals', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('adjustment_type')->default("dec")->change();
                });
            }
            if (Schema::hasTable('ims_orders')) {
                DB::statement('ALTER TABLE ims_orders MODIFY sync varchar(191)');
                Schema::table('ims_orders', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_order_items')) {
                DB::statement('ALTER TABLE ims_order_items MODIFY sync varchar(191)');
                Schema::table('ims_order_items', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_issue_orders')) {
                DB::statement('ALTER TABLE ims_issue_orders MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ims_issue_orders MODIFY isRejected varchar(191)');
                Schema::table('ims_issue_orders', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('isRejected')->change();
                });
            }
            if (Schema::hasTable('ims_issue_order_details')) {
                DB::statement('ALTER TABLE ims_issue_order_details MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ims_issue_order_details MODIFY item_to_be_returned varchar(191)');
                Schema::table('ims_issue_order_details', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('item_to_be_returned')->change();
                });
            }
            if (Schema::hasTable('ims_sales')) {
                DB::statement('ALTER TABLE ims_sales MODIFY is_tax_calculated varchar(191)');
                DB::statement('ALTER TABLE ims_sales MODIFY isReversed varchar(191)');
                DB::statement('ALTER TABLE ims_sales MODIFY sync varchar(191)');
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('is_tax_calculated')->default(0)->change();
                    $table->string('isReversed')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_sales_items')) {
                DB::statement('ALTER TABLE ims_sales_items MODIFY sync varchar(191)');
                Schema::table('ims_sales_items', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_categories')) {
                DB::statement('ALTER TABLE ims_categories MODIFY sync varchar(191)');
                Schema::table('ims_categories', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_currencies')) {
                DB::statement('ALTER TABLE ims_currencies MODIFY sync varchar(191)');
                Schema::table('ims_currencies', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_customers')) {
                DB::statement('ALTER TABLE ims_customers MODIFY sync varchar(191)');
                Schema::table('ims_customers', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_delivered_goods')) {
                DB::statement('ALTER TABLE ims_delivered_goods MODIFY sync varchar(191)');
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_departments')) {
                DB::statement('ALTER TABLE ims_departments MODIFY sync varchar(191)');
                Schema::table('ims_departments', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_discounts')) {
                DB::statement('ALTER TABLE ims_discounts MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ims_discounts MODIFY default_value varchar(191)');
                Schema::table('ims_discounts', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('default_value')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_employees')) {
                DB::statement('ALTER TABLE ims_employees MODIFY sync varchar(191)');
                Schema::table('ims_employees', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_designations')) {
                DB::statement('ALTER TABLE ims_designations MODIFY sync varchar(191)');
                Schema::table('ims_designations', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
