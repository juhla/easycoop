<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BmacAddColumnDefaultValues extends Migration
{

    public function __construct()
    {

    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            if (Schema::hasColumn('cash_receipt', 'flag')) {
                Schema::table('cash_receipt', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('cash_receipt', 'status')) {
                Schema::table('cash_receipt', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_banks_accounts', 'flag')) {
                Schema::table('ca_banks_accounts', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_customers', 'sync')) {
                Schema::table('ca_customers', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_customers', 'flag')) {
                Schema::table('ca_customers', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('ca_fiscal_year', 'status')) {
                Schema::table('ca_fiscal_year', function (Blueprint $table) {
                    $table->string('status')->default('Open')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('ca_fiscal_year', 'flag')) {
                Schema::table('ca_fiscal_year', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('ca_groups', '_lft')) {
                Schema::table('ca_groups', function (Blueprint $table) {
                    $table->integer('_lft')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_groups', '_rgt')) {
                Schema::table('ca_groups', function (Blueprint $table) {
                    $table->integer('_rgt')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_groups', 'is_system_account')) {
                Schema::table('ca_groups', function (Blueprint $table) {
                    $table->integer('is_system_account')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('ca_groups', 'flag')) {
                Schema::table('ca_groups', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice', 'payment_term_id')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->integer('payment_term_id')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice', 'status')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice', 'flag')) {
                Schema::table('ca_invoice', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice_details', 'flag')) {
                Schema::table('ca_invoice_details', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice_items', 'flag')) {
                Schema::table('ca_invoice_items', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_ledgers', 'flag')) {
                Schema::table('ca_ledgers', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_invoice_items', 'flag')) {
                Schema::table('ca_invoice_items', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_payment_terms', 'flag')) {
                Schema::table('ca_payment_terms', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_products', 'flag')) {
                Schema::table('ca_products', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_settings', 'approval')) {
                Schema::table('ca_settings', function (Blueprint $table) {
                    $table->string('approval')->default('1')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_settings_invoice', 'template')) {
                Schema::table('ca_settings_invoice', function (Blueprint $table) {
                    $table->string('template')->default('template_1')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_tax_types', 'flag')) {
                Schema::table('ca_tax_types', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('petty_cash', 'status')) {
                Schema::table('petty_cash', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('petty_cash', 'flag')) {
                Schema::table('petty_cash', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_transactions', 'memo_posted')) {
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->integer('memo_posted')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_transactions', 'flag')) {
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_transactions', 'approve_status')) {
                Schema::table('ca_transactions', function (Blueprint $table) {
                    $table->string('approve_status')->default('1')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('ca_transaction_items', 'flag')) {
                Schema::table('ca_transaction_items', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_transaction_items', 'approve_status')) {
                Schema::table('ca_transaction_items', function (Blueprint $table) {
                    $table->string('approve_status')->default('1')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_vendors', 'sync')) {
                Schema::table('ca_vendors', function (Blueprint $table) {
                    $table->string('sync')->default('1')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ca_vendors', 'flag')) {
                Schema::table('ca_vendors', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('cheque_payment', 'status')) {
                Schema::table('cheque_payment', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('cheque_payment', 'flag')) {
                Schema::table('cheque_payment', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('cheque_receipt', 'status')) {
                Schema::table('cheque_receipt', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('cheque_receipt', 'flag')) {
                Schema::table('cheque_receipt', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('credit_purchase', 'status')) {
                Schema::table('credit_purchase', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('credit_purchase', 'flag')) {
                Schema::table('credit_purchase', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('credit_sales', 'status')) {
                Schema::table('credit_sales', function (Blueprint $table) {
                    $table->integer('status')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('credit_sales', 'flag')) {
                Schema::table('credit_sales', function (Blueprint $table) {
                    $table->string('flag')->default('Active')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
