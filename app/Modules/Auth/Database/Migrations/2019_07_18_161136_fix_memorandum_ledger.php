<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixMemorandumLedger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            if (Schema::hasTable('ca_ledgers')) {
                $results = DB::connection('tenant_conn')->table('ca_ledgers')->where('code', '3400')->get();
                if (count($results) > 1) {
                    \App\Modules\Base\Models\Ledger::where('code', '3400')->first()->delete();
                }
            }
        }catch (Exception $exception){

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
