<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUuidFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('ca_transaction_items', function (Blueprint $table) {
                $table->string('id')->change();
                $table->string('transaction_id')->change();
            });
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            Schema::table('ca_transactions', function (Blueprint $table) {
                $table->string('id')->change();
            });
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            Schema::table('ca_ledgers', function (Blueprint $table) {
                $table->string('id')->change();
            });
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
