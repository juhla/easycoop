<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSyncToCaLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            if (!Schema::hasColumn('ca_ledgers', 'sync')) {
                Schema::table('ca_ledgers', function (Blueprint $table) {
                    $table->string('sync', '100')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_ledgers', function (Blueprint $table) {
            //
        });
    }
}
