<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-12
 * Time: 14:39
 */

namespace App\Modules\Auth\Database\Seeds;


use Illuminate\Database\Seeder;

class AuthDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ChartOfAccountSeeder::class);
        //$this->call(RolesSeeder::class);
    }
}