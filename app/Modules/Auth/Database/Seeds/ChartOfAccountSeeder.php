<?php

namespace App\Modules\Auth\Database\Seeds;


use Illuminate\Database\Seeder;

class ChartOfAccountSeeder extends Seeder
{
    use \App\Modules\Base\Traits\NewCreates;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default chart of account groups
        \App\Modules\Base\Models\AccountGroup::create(['id' => '1', 'name' => 'Assets', 'code' => '1000', 'parent_id' => null, 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '2', 'name' => 'Liabilities', 'code' => '2000', 'parent_id' => null, 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '3', 'name' => 'Accumulated Funds', 'code' => '3000', 'parent_id' => null, 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '4', 'name' => 'Revenue', 'code' => '4000', 'parent_id' => null, 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '5', 'name' => 'Cost', 'code' => '5000', 'parent_id' => null, 'is_system_account' => '1']);

        \App\Modules\Base\Models\AccountGroup::create(['id' => '6', 'name' => 'Non-Current Assets', 'code' => '1100', 'parent_id' => '1', 'is_system_account' => '1']);

        \App\Modules\Base\Models\AccountGroup::create(['id' => '47', 'name' => "Member's Equity", 'code' => '3500', 'parent_id' => '3', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '42', 'name' => 'Quoted Investment', 'code' => '1140', 'parent_id' => '6', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '43', 'name' => 'Long Term Investment', 'code' => '1130', 'parent_id' => '6', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '7', 'name' => 'Property, Plant & Equipment', 'code' => '1110', 'parent_id' => '6', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '8', 'name' => 'Investment Property', 'code' => '1120', 'parent_id' => '6', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '45', 'name' => "Member's Loan", 'code' => '1150', 'parent_id' => '6', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '9', 'name' => 'Current Assets', 'code' => '1200', 'parent_id' => '1', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '10', 'name' => 'Inventory', 'code' => '1210', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '11', 'name' => 'Account Receivables', 'code' => '1220', 'parent_id' => '9', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '12', 'name' => 'Loan Disbursement', 'code' => '1221', 'parent_id' => '11', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '13', 'name' => 'Receivables - Staff', 'code' => '1222', 'parent_id' => '11', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '14', 'name' => 'Receivables - Inter-Company', 'code' => '1223', 'parent_id' => '11', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '15', 'name' => 'Short Term Investment', 'code' => '1230', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '16', 'name' => 'Bank Balances', 'code' => '1240', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '17', 'name' => 'Dom Account Balances', 'code' => '1250', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '18', 'name' => 'Cash', 'code' => '1260', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '19', 'name' => 'Prepayments', 'code' => '1270', 'parent_id' => '9', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '20', 'name' => 'Accumulated Depreciation', 'code' => '1300', 'parent_id' => '1', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '21', 'name' => 'Property, Plant & Equipment', 'code' => '1310', 'parent_id' => '20', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '22', 'name' => 'Investment Property', 'code' => '1320', 'parent_id' => '20', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '23', 'name' => 'Non-current Liabilities', 'code' => '2100', 'parent_id' => '2', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '24', 'name' => 'Long Term Borrowing', 'code' => '2110', 'parent_id' => '23', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '25', 'name' => 'Deferred Tax', 'code' => '2120', 'parent_id' => '23', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '26', 'name' => 'Long Term Provisions', 'code' => '2130', 'parent_id' => '23', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '46', 'name' => "Member's Target Savings", 'code' => '2150', 'parent_id' => '23', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '27', 'name' => 'Current Liabilities', 'code' => '2200', 'parent_id' => '2', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '28', 'name' => 'Payable - Trade', 'code' => '2210', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '29', 'name' => 'Payable - Staff', 'code' => '2220', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '30', 'name' => 'Payable - Inter-Company', 'code' => '2230', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '31', 'name' => 'Short Term Borrowing', 'code' => '2240', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '32', 'name' => 'Current Portion of Long Term Borrowing', 'code' => '2250', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '33', 'name' => 'Short Term Provisions', 'code' => '2270', 'parent_id' => '27', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '44', 'name' => "Member's Contribution", 'code' => '2270', 'parent_id' => '27', 'is_system_account' => '1']);


        \App\Modules\Base\Models\AccountGroup::create(['id' => '34', 'name' => 'Accruals', 'code' => '2300', 'parent_id' => '2', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '35', 'name' => 'Income', 'code' => '4100', 'parent_id' => '4', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '36', 'name' => 'Other Income', 'code' => '4200', 'parent_id' => '4', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '37', 'name' => 'Direct Cost', 'code' => '5100', 'parent_id' => '5', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '38', 'name' => 'Admin and Overhead', 'code' => '5200', 'parent_id' => '5', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '39', 'name' => 'Provision for Depreciation', 'code' => '5201', 'parent_id' => '38', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '40', 'name' => 'Marketing and Selling Expenses', 'code' => '5300', 'parent_id' => '5', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '41', 'name' => 'Financial Expenses', 'code' => '5400', 'parent_id' => '5', 'is_system_account' => '1']);
        \App\Modules\Base\Models\AccountGroup::create(['id' => '1000', 'name' => 'Payable - Tax', 'code' => '2231', 'parent_id' => '27', 'is_system_account' => '1']);


        // Payable - Tax
        \App\Modules\Base\Models\Ledger::create(['group_id' => '1000', 'name' => 'VAT Payable', 'code' => '2231.01', 'opening_balance_type' => 'D', 'is_system_account' => '1']);


        //create default ledger accounts for Property, Plant & Equipment - Non-Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Office Equipment', 'code' => '1110.1', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Furniture and Fixtures', 'code' => '1110.2', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Motor Vehicles', 'code' => '1110.3', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Building', 'code' => '1110.4', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Land', 'code' => '1110.5', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '7', 'name' => 'Computer Equipment', 'code' => '1110.6', 'opening_balance_type' => 'D', 'is_system_account' => '1']);


        //create default ledger accounts for Investment Property - Non-Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '8', 'name' => 'Building', 'code' => '1120.1', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '8', 'name' => 'Land', 'code' => '1120.2', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Inventory - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '10', 'name' => 'Raw Materials', 'code' => '1211', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '10', 'name' => 'WIP', 'code' => '1212', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '10', 'name' => 'Finished Goods', 'code' => '1213', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '10', 'name' => 'General Stock', 'code' => '1214', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Short Term Investment - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '15', 'name' => 'Investment Account - A', 'code' => '1231', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '15', 'name' => 'Investment Account - B', 'code' => '1232', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '15', 'name' => 'Investment Account - C', 'code' => '1233', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Bank Balances - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '16', 'name' => 'Naira Bank Account - A', 'code' => '1241', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '16', 'name' => 'Naira Bank Account - B', 'code' => '1242', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '16', 'name' => 'Naira Bank Account - C', 'code' => '1243', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Dom Account Balances - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '17', 'name' => 'Dom Bank Account - A', 'code' => '1251', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '17', 'name' => 'Dom Bank Account - B', 'code' => '1252', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Cash - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '18', 'name' => 'Cash in Hand (Head Office)', 'code' => '1261', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '18', 'name' => 'Cash in Hand (Branch Offices)', 'code' => '1262', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Prepayments - Current Assets
        \App\Modules\Base\Models\Ledger::create(['group_id' => '19', 'name' => 'Rent', 'code' => '1271', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '19', 'name' => 'Others', 'code' => '1272', 'opening_balance_type' => 'D', 'is_system_account' => '1']);


        //create default ledger accounts for Property, Plant & Equipment - Accumulated Depreciation
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Office Equipment', 'code' => '1310.1', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Furniture and Fixtures', 'code' => '1310.2', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Motor Vehicles', 'code' => '1310.3', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Building', 'code' => '1310.4', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Computer Equipment', 'code' => '1315', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '21', 'name' => 'Accum. Dep - Others', 'code' => '1310.5', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        //create default ledger accounts for Investment Property - Accumulated Depreciation
        \App\Modules\Base\Models\Ledger::create(['group_id' => '22', 'name' => 'Building', 'code' => '1320.1', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        //create default ledger accounts for Current Liabilities - Liabilities
        \App\Modules\Base\Models\Ledger::create(['group_id' => '27', 'name' => 'Current Tax Payables', 'code' => '2260', 'opening_balance_type' => 'C', 'is_system_account' => '1']);


        //create default ledger accounts for Non Current Liabilities - Liabilities
        \App\Modules\Base\Models\Ledger::create(['group_id' => '23', 'name' => 'Members Deposit for Properties', 'code' => '2140', 'opening_balance_type' => 'C', 'is_system_account' => '1']);


        // Revenue
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Interest on Loan to Members', 'code' => '4300', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Registration Fees', 'code' => '4400', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Administrative Fees', 'code' => '4500', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Trading Income', 'code' => '4600', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Interest Income', 'code' => '4700', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Income from Investment', 'code' => '4800', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '4', 'name' => 'Penalties Income', 'code' => '4900', 'opening_balance_type' => 'C', 'is_system_account' => '1']);


        //create default ledger accounts for Accruals
        \App\Modules\Base\Models\Ledger::create(['group_id' => '34', 'name' => 'Accruals', 'code' => '2310', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        //create default ledger accounts for Equity
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Share Capital', 'code' => '3100', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Deposit for Shares', 'code' => '3200', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Retained Earnings', 'code' => '3300', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Reserve Fund', 'code' => '3600', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Revenue Reserve', 'code' => '3700', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'General Reserve', 'code' => '3800', 'opening_balance_type' => 'C', 'is_system_account' => '1']);

        //create default ledger accounts for Memorandum Accounts - Equity
        \App\Modules\Base\Models\Ledger::create(['group_id' => '3', 'name' => 'Memorandum Account', 'code' => '3400', 'opening_balance_type' => 'C', 'is_system_account' => '1']);

        //create default ledger accounts for Income - Revenue
        \App\Modules\Base\Models\Ledger::create(['group_id' => '35', 'name' => 'Sales', 'code' => '4110', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '35', 'name' => 'Sales Returns', 'code' => '4120', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Other Income - Revenue
        \App\Modules\Base\Models\Ledger::create(['group_id' => '36', 'name' => 'Finance Income', 'code' => '4210', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '36', 'name' => 'Profit on Disposal of Fixed Assets', 'code' => '4220', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '36', 'name' => 'Profit on Exchange Rate Conversion', 'code' => '4230', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        //create default ledger accounts for Direct Cost - Expenses
        \App\Modules\Base\Models\Ledger::create(['group_id' => '37', 'name' => 'Purchases', 'code' => '5101', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '37', 'name' => 'Purchase Return', 'code' => '5102', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '37', 'name' => 'Cost of Sales', 'code' => '5103', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '37', 'name' => 'Interest on Contribution', 'code' => '5104', 'opening_balance_type' => 'D', 'is_system_account' => '1']);


        //create default ledger accounts for Provision for Depreciation - Admin and Overhead - Expenses
        \App\Modules\Base\Models\Ledger::create(['group_id' => '39', 'name' => 'Depreciation - Office Equipment', 'code' => '5201.1', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '39', 'name' => 'Depreciation - Furniture and Fixtures', 'code' => '5201.2', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '39', 'name' => 'Depreciation - Motor Vehicles', 'code' => '5201.3', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '39', 'name' => 'Depreciation - Building', 'code' => '5201.4', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '39', 'name' => 'Depreciation - Others', 'code' => '5201.5', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Admin and Overhead - Expenses
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Salaries and Wages', 'code' => '5202', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Repairs and Maintenance', 'code' => '5203', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Rent', 'code' => '5204', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Legal Expenses', 'code' => '5205', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Communication', 'code' => '5206', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Travelling', 'code' => '5207', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Training', 'code' => '5208', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Government Dues and Levies', 'code' => '5209', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Professional Dues', 'code' => '5210', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => 'Others', 'code' => '5211', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '38', 'name' => "Adjustment Journal", 'code' => '5211.1', 'opening_balance_type' => 'D', 'is_system_account' => '1']);


        //create default ledger accounts for Marketing and Selling Expenses - Expenses
        \App\Modules\Base\Models\Ledger::create(['group_id' => '40', 'name' => 'Salesmen\'s Salaries', 'code' => '5301', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '40', 'name' => 'Salesmen Communication', 'code' => '5302', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '40', 'name' => 'Advertising', 'code' => '5303', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '40', 'name' => 'Salesmen travelling expenses', 'code' => '5304', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '40', 'name' => 'Branch and Store Expenses', 'code' => '5305', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Financial Expenses - Expenses
        \App\Modules\Base\Models\Ledger::create(['group_id' => '41', 'name' => 'Bank Interest', 'code' => '5401', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '41', 'name' => 'Bank Charges', 'code' => '5402', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '41', 'name' => 'Other Financial Expenses', 'code' => '5403', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '41', 'name' => 'Bad Debt', 'code' => '5404', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        //create default ledger accounts for Cost (Expenses)
        \App\Modules\Base\Models\Ledger::create(['group_id' => '5', 'name' => 'Provision for Doubtful Debt', 'code' => '5500', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '5', 'name' => 'Loss on Disposal of Fixed Assets', 'code' => '5600', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '5', 'name' => 'Loss on Foreign Exchange Conversion', 'code' => '5700', 'opening_balance_type' => 'D', 'is_system_account' => '1']);

        //Sub categories under Staff Payable. Data are feed from payroll
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Pension', 'code' => '2220.1', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Staff Loan', 'code' => '2220.2', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Health Insurance', 'code' => '2220.3', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Housing Fund', 'code' => '2220.4', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - PAYE', 'code' => '2220.5', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Other Deduction', 'code' => '2220.6', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
        \App\Modules\Base\Models\Ledger::create(['group_id' => '29', 'name' => 'Payable - Salary', 'code' => '2220.7', 'opening_balance_type' => 'C', 'is_system_account' => '1']);

    }
}
