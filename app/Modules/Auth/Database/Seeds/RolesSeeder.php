<?php

namespace App\Modules\Auth\Database\Seeds;


use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \App\Modules\Base\Models\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'category' => 'admin',
        ]);


        \App\Modules\Base\Models\Role::create([
            'name' => 'business-owner',
            'display_name' => 'Business Owner',
            'category' => 'business-owner',
        ]);

    }
}
