<?php

namespace App\Modules\Auth\Http\Controllers;


use App\Modules\Auth\Events\BeforeLogin;
use App\Modules\Auth\Traits\Register;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\ImsLicenceSubscriber;

use App\Modules\Base\Models\Permission;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Models\UserLicense;
use App\Modules\Base\Models\UserWidget;
use App\Modules\Base\Models\Widget;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Traits\Mailer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Modules\Base\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use DB;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\MsmeSetup;
use Mail;
use GuzzleHttp\Client;
use App\Modules\Base\Traits\Audit;
use Zizaco\Entrust\Entrust;
use Zizaco\Entrust\EntrustPermission;
use Zizaco\Entrust\EntrustRole;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    use Mailer;
    use License, Register;

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;
    use Audit;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/workspace';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserLicense $userLicence)
    {
        //$this->middleware(['tenant']);
        $this->middleware('guest')->except(['showLoginForm', 'logout', 'signOut', 'doLogin', 'getCountryInfo']);
        $this->userLicence = $userLicence;
    }


    /**
     * Show the application social registration page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $title = 'Sign In';
        return view('auth::login', compact('title'));
    }

    private function afterLogin()
    {
    }

    /**
     * Method to verify user account via the email
     */
    public function verifyAccount()
    {
        $request = request()->all();
        //find user
        $user = User::where('email', $request['email'])->first();
        //if user exist, proceed
        if ($user) {
            //exit;
            if ($request['code'] === $user->confirmation_code) {
                $user->confirmed = 1;
                $user->active = 1;
                $user->save();

                $userRole = '';
                if ($user->hasRole('business-owner')) {
                    $userRole = 'business-owner';
                } else if ($user->hasRole('professional')) {
                    $userRole = 'professional';
                } else if ($user->hasRole('finance-provider')) {
                    $userRole = 'finance-provider';
                } else if ($user->hasRole('payroll-admin')) {
                    $userRole = 'payroll-admin';
                }

                //send welcome mail to user
                $input['subject'] = 'Hurray! Registration Complete';
                $input['first_name'] = $user['first_name'];
                $input['last_name'] = $user['last_name'];
                $input['body'] = 'stuff';
                $input['user_role'] = $userRole;
                $input['email'] = $user['email'];

                try {
                    $this->sendEmailFrmUserManager('emails.usermanager.on_successful_verification', $input);
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                    return response()->json([
                        'type' => 'email_fails',
                        'message' => $e->getMessage(),
                    ]);
                }

                flash()->success('Your email address has been successfully verified. Welcome on board, please login to your account');
            } else {
                flash()->error("Your email address could not be verified. Please contact the <a href='mailto:support@ikooba.com'>BMAC Admin</a> to assist you with your verification");
            }
        } else {
            flash()->error('Unable to verify account.');
        }
        //return to login page
        return redirect()->to('secure/sign-in');
    }

    var $theError;

    /**
     * @return mixed
     */
    public function getTheError()
    {
        return $this->theError;
    }

    /**
     * @param mixed $theError
     */
    public function setTheError($theError)
    {
        $this->theError = $theError;
    }


    public function doLoginWeb()
    {
        $input = request()->all();


        $validator = Validator::make(request()->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        //dd($input);
        //if validation fails, redirect with errors
        if ($validator->fails()) {
            return redirect()->to('secure/sign-in')
                ->withErrors($validator)
                ->withInput();
        }
        event(new BeforeLogin($input));
        $res = session()->get('app.login');
        if (!empty($res['error'])) {
            return redirect()->to($res['url'])
                ->withErrors($res['error'])
                ->withInput();
        }

    }

    public function doLogin()
    {

        $input = request()->all();
        //$input['username'] = $input['email'];

        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required'
        ]);
        //dd($input);

        //if validation fails, redirect with errors
        if ($validator->fails()) {
            return redirect()->to('secure/sign-in')
                ->withErrors($validator)
                ->withInput();
        }
        event(new BeforeLogin($input));
        $res = session()->get('app.login');
        //dd($res);
        if (!empty($res['error'])) {
            abort(500, 'Unauthorized action.');
            return redirect()->to($res['url'])
                ->withErrors($res['error'])
                ->withInput();
        }
        $currentDomain = url('') . "/";
        $user = auth()->user();

        //dd($res);
        return redirect()->to($res['path'] . $res['url']);

    }

    /**
     * Show the application email registration page.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function showRegistrationForm()
    {
        $title = 'Sign Up';
        $input = request()->all();
        $input['email'] = empty($input['email']) ? "" : $input['email'];
        $email = empty(old('email')) ? $input['email'] : old('email');
        $roles = Role::where('name', 'business-owner')
            ->orWhere('name', 'professional')
            ->orWhere('name', 'finance-provider')
            ->orWhere('name', 'trade-promoter')
            ->get();

        $category_widget = Widget::getAllBusinessWidget()->toArray();
        return view('auth::signup', compact('title', 'roles', 'email', 'category_widget'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected
    function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|min:5|max:255',
            'last_name' => 'required|max:255',
            'company_name' => 'required|unique:companies,company_name',
            'rc_number' => 'required|max:10',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    public
    function ajaxRegister()
    {
        $input = request()->all();
        return $this->register($input);

    }


    public function signOut()
    {
        try {
            $user = auth()->user();
            $this->handleTrail();
            if (!empty($user)) {
                $user->last_active_time = time();;
                $user->online_status = 0;
                $user->save();
                Auth::logout();
                Auth::guard()->logout();
            }
            session()->flush();
        } catch (\Exception $ex) {
            app('sentry')->captureException($ex);
        }


        return redirect()->to('secure/sign-in');
    }

    public function successPage()
    {
        return view('auth::auth.success');
    }

    private function checkRoleAccess($user)
    {
        //test if user has multiple rows
        $bmacEmployee = $user->hasRole('employee');
        $imsEmployee = $user->hasRole('employee-ims');
        $payrollEmployee = $user->hasRole('employee-payroll');
        $counter = 0;

        if ($bmacEmployee) {
            $counter++;
        }
        if ($imsEmployee) {
            $counter++;
        }
        if ($payrollEmployee) {
            $counter++;
        }
        $counter = 1;
        if ($counter > 1) {
        } else {
            if ($user->hasRole('employee-payroll')) {
                return redirect()->to('payroll/login');
            } else if ($user->hasRole('employee-ims')) {
                return redirect()->to('ims/create');
            } else if ($user->hasRole('employee')) {
                return null;
            }
        }
    }
}
