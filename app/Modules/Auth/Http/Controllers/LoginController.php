<?php

// namespace App\Modules\Base\Http\Controllers\Auth;
namespace App\Modules\Auth\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\User;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function switchDomain()
    {
        try {
            $input = request()->all();
            if (!empty($input['token']) && !empty($input['url']) && !empty($input['hash'])) {

                $client = new Client();
                $api_url = config('constants.api_url');
                $res = $client->request('GET', $api_url . "secure/verify?token=" . $input['token'], []);
                $status = $res->getStatusCode();

                if ($status == 200) {
                    $body = $res->getBody();
                    $remainingBytes = $body->getContents();
                    $remainingBytes = \GuzzleHttp\json_decode($remainingBytes);
                    $user = User::find($remainingBytes->id);
                    Session::put('userhash', $input['hash']);
                    Session::put('auth.token', $input['token']);
                    Auth::login($user);
                    return redirect()->to($input['url']);
                }

            } else {
                return redirect()->to('secure/sign-in');
            }
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
            return redirect()->to('secure/sign-in');
        }

    }
}
