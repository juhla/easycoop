<?php

// namespace App\Modules\Base\Http\Controllers\Auth;
namespace App\Modules\Auth\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\Permission;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Zizaco\Entrust\EntrustPermission;


class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/workspace';


    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['newPassword']);
    }

    public function newPassword()
    {

        $rules = [
            'password' => 'required|min:4|confirmed',
            'password_confirmation' => 'required|min:4',
        ];

        $validator = \Validator::make(request()->all(), $rules);
        //if validation fails, redirect with errors
        if ($validator->fails()) {
//            dd($validator);
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $password = request()->input('password');
        $email = request()->get('email');
        $user = User::where('email', $email)->first();
//        dd(request()->all(), $user);

        if ($user) {

            //save user info
            $user->password = bcrypt($password);
            \Session::put('userhash', session()->getId());
            $user->user_hash = session()->getId();
            $user->active = 1;
            $user->confirmed = 1;
            $user->save();
            //sign in user
            $credentials = array(
                'email' => $email,
                'password' => $password,
            );

            //dd($credentials);
            Auth::attempt($credentials);

            if (auth()->user()->hasRole(['trade-promoter', 'agent-finance-provider', 'finance-provider', 'professional'])) {
                return redirect()->to('profile/' . strtolower(auth()->user()->displayName()));

            } elseif (auth()->user()->hasRole('employee-payroll') || auth()->user()->hasRole('admin-payroll')) {

                $credentials = array(
                    'email' => request()->input('email'),
                    'password' => request()->input('password')
                );

                $this->tokenLogin($credentials);
                $user = auth()->user();
                session()->forget('company_db');
                session()->forget('company_name');

                if ($user->hasRole('employee-payroll') || $user->hasRole('admin-payroll')) {
//dd('dd');
                    return redirect()->to('payroll/create');
                }
            }
            return redirect()->to('/workspace');

        } else {
            flash()->error('User not found!');
            return redirect()->back();
        }
    }

    private function tokenLogin($credentials)
    {
        try {
            $client = new Client();
            $api_url = config('constants.api_url');
            $res = $client->request('POST', $api_url . "secure/sign-in", [
                'form_params' => $credentials
            ]);
            $status = $res->getStatusCode();
        } catch (\Exception $e) {
            $status = $e->getResponse()->getStatusCode();
        }

        if ($status == 200) {
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $remainingBytes = \GuzzleHttp\json_decode($remainingBytes);
            session()->put('auth.token', $remainingBytes->data->token);
        } else {
            Auth::logout();
            return redirect()->to('secure/sign-in')
                ->withErrors('Incorrect login details')
                ->withInput();
        }
    }

}
