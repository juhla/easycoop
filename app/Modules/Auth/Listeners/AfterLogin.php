<?php

namespace App\Modules\Auth\Listeners;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Traits\License;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Modules\Base\Models\Permission;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\Company;

class AfterLogin
{
    use License;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }


    private $isError;
    private $basePath;
    private $errorMessage;
    private $redirectURL;
    private $credentials;
    private $isRedirected;

    /**
     * @return mixed
     */
    public function getisRedirected()
    {
        return $this->isRedirected;
    }

    /**
     * @param mixed $isRedirected
     */
    public function setIsRedirected($isRedirected)
    {
        $this->isRedirected = $isRedirected;
    }


    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param mixed $basePath
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }


    /**
     * @return mixed
     */
    public function getisError()
    {
        return $this->isError;
    }

    /**
     * @param mixed $isError
     */
    public function setIsError($isError)
    {
        $this->isError = $isError;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return mixed
     */
    public function getRedirectURL()
    {
        return $this->redirectURL;
    }

    /**
     * @param mixed $redirectURL
     */
    public function setRedirectURL($redirectURL)
    {
        $this->redirectURL = $redirectURL;
    }


    public function handle(\App\Modules\Auth\Events\AfterLogin $event)
    {

        $this->credentials = $event->credentials;

        //dd($this->user . " >> ");
        $this->doCheck();
        $response = [
            'url' => $this->getRedirectURL(),
            'error' => $this->getisError() ? $this->getErrorMessage() : null,
            'path' => $this->getBasePath(),
        ];


        session()->put('app.login', $response);
        return $response;
    }

    function licenseChecks()
    {

        $user = auth()->user();
        //dd($user);
        //check if the account is active
        if (!$user->isActive()) {
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('Your Account has been deactivated. Kindly send an email to support@ikooba.com to reactivate your account');
            $this->setRedirectURL('/secure/sign-in');
            return;
        }
    }

    private function tokenLogin($credentials)
    {

        try {
            $client = new Client();
            $api_url = config('constants.api_url');
            $res = $client->request('POST', $api_url . "secure/sign-in", [
                'form_params' => $credentials
            ]);
            $status = $res->getStatusCode();
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            try {
                $body = $e->getResponse()->getBody();
                $remainingBytes = $body->getContents();
                $remainingBytes = \GuzzleHttp\json_decode($remainingBytes);
                $this->setErrorMessage($remainingBytes->error);
                $status = $e->getResponse()->getStatusCode();
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
                $status = $e->getResponse()->getStatusCode();
            }
        }

        if ($status == 200) {
            $body = $res->getBody();
            $remainingBytes = $body->getContents();
            $remainingBytes = \GuzzleHttp\json_decode($remainingBytes);
            session()->put('auth.token', $remainingBytes->data->token);
            return true;
        }
        return false;
    }

    function tokenLoginHandler()
    {
        if (!$this->tokenLogin($this->credentials)) {
            Auth::logout();
            try {
                $error = $this->getErrorMessage();
                $message = empty($error) ? 'Incorrect login details' : $error;
                $this->setIsError(true);
                $this->setRedirectURL('secure/sign-in');
                $this->setErrorMessage($message);
            } catch (\Exception $exception) {
                $this->setIsError(true);
                $this->setRedirectURL('secure/sign-in');
                $this->setErrorMessage('Incorrect login details');
            }
        }
        return;
    }

    function doCheck()
    {
        $this->licenseChecks();


        if ($this->getisError()) {
            return;
        }
        //$this->tokenLoginHandler();
        if ($this->getisError()) {
            return;
        }
        $user = auth()->user();
        $this->setBasePath(config("constants.business_path"));


        session()->forget('company_db');
        session()->forget('company_name');
        $today = Carbon::today();


        Session::put('userhash', session()->getId());
        $userNoAudit = User::find($user->id);
        $userNoAudit->user_hash = session()->getId();
        $userNoAudit->save();

        $this->setIsError(false);
        $this->setRedirectURL('workspace');
        return;
    }
}
