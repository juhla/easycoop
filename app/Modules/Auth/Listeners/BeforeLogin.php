<?php

namespace App\Modules\Auth\Listeners;
;

use App\Modules\Api\Traits\ApOauth;
use App\Modules\Auth\Events\BeforeLogin as BeforeLoginEvent;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BeforeLogin
{

    use \App\Modules\Base\Traits\AfterLogin, ApOauth;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    var $theError;
    var $redirectURL;
    private $basePath;

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param mixed $basePath
     */
    public function setBasePath($basePath)
    {
        $this->basePath = $basePath;
    }


    /**
     * @return mixed
     */
    public function getRedirectURL()
    {
        return $this->redirectURL;
    }

    /**
     * @param mixed $redirectURL
     */
    public function setRedirectURL($redirectURL)
    {
        $this->redirectURL = $redirectURL;
    }


    /**
     * @return mixed
     */
    public function getTheError()
    {
        return $this->theError;
    }

    /**
     * @param mixed $theError
     */
    public function setTheError($theError)
    {
        $this->theError = $theError;
    }

    public function businessTypeCheck()
    {
        $userID = getBusinessOwnerID();
        $company = Company::where('user_id', '=', $userID)->first();
        if ($company->business_type == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function handle(BeforeLoginEvent $event)
    {
        $input = $event->input;
        $username = $input['username'];
        $credentials = array(
            'username' => $username,
            'password' => $input['password'],
        );
        $remember = (isset($input['remember']) && $input['remember'] == 'checked') ? true : false;

        $this->init($username, $input['password']);
        if ($this->isError) {
            $this->setRedirectURL('secure/sign-in');
            $this->setTheError($this->isErrorMessage);

            $response = [
                'url' => $this->getRedirectURL(),
                'error' => $this->getTheError(),
                'path' => $this->getBasePath(),
            ];

            session()->put('app.login', $response);
            return $response;
        }


        try {
            $oauth = session()->get("ApOauth");
            //dd($oauth['decoded']->cooperative_code);
            $user = User::where('username', @$oauth['decoded']->cooperative_code)->first();

           // dd($user);
            if (!empty($user)) {
                if (Auth::loginUsingId($user->id)) {
                    session()->put("company_name", $oauth['decoded']->cooperative_name);
                    try {
                        if (empty($user->company_name)) {
                            $user->company_name = $oauth['decoded']->cooperative_name;
                            $user->save();
                        }
                    } catch (\Exception $exception) {

                    }
                    return event(new \App\Modules\Auth\Events\AfterLogin($credentials));
                }
            } else {
                $this->setRedirectURL('secure/sign-in');
                $this->setTheError('Incorrect login details');
            }
        } catch (\Exception $exception) {
           // dd($exception->getMessage());
            $this->setRedirectURL('secure/sign-in');
            $this->setTheError('Incorrect login details');
        }


        if (\auth()->check()) {
            $this->setBasePath(config("constants.business_path"));
        }


        $response = [
            'url' => $this->getRedirectURL(),
            'error' => $this->getTheError(),
            'path' => $this->getBasePath(),
        ];

        session()->put('app.login', $response);
        return $response;
    }


}
