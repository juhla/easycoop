<?php

namespace App\Modules\Auth\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{


    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            //'App\Modules\Base\Listeners\PatchDatabase',
            // 'App\Modules\Base\Listeners\AfterLogin',
        ],
        'App\Modules\Auth\Events\BeforeLogin' => [
            'App\Modules\Auth\Listeners\BeforeLogin',
        ],
        'App\Modules\Auth\Events\AfterLogin' => [
            'App\Modules\Auth\Listeners\AfterLogin',
        ]
    ];


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }


}
