<!DOCTYPE html>
<html lang="en" class="bg-primary">
<head>
    <meta charset="utf-8"/>
    <title>{{ (isset($title)) ? $title : 'BMAC' }} | Business Measurement Assurance and Collaboration </title>
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Bootstrap CSS -->

    <link href="{{asset('newui/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- bootstrap theme -->
    <link href="{{asset('newui/css/css/bootstrap-theme.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css"/>
    {{--<link rel="stylesheet" href="{{ asset('css/app.v1.css') }}" type="text/css"/>--}}
    {{--<link rel="stylesheet" href="{{ asset('css/rebrandingNov2017.css') }}" type="text/css"/>--}}
    <link href="{{ asset('newui/css/style.css')}}" rel="stylesheet">

    <?php if(config("app.env") == "production"): ?>
    <script src="{{asset('js/chat/countly.js') }}"></script>
    <?php endif; ?>
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie/html5shiv.js') }}"></script>
    <script src="{{ asset('js/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/ie/excanvas.js') }}"></script>
    <![endif]-->
</head>

<body class="link-r1">
<?php if(config("app.env") == "production"): ?>
@include('layouts._analytics')
<?php endif; ?>
<!-- <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
    <div class="container aside-xxl ">
        <div class="text-center">
            <a class="navbar-brand  " href="#"> -->
                {{-- <img src="{{ asset('images/slide-logo.png') }}"> | BMAC  --}}
                <!-- <span class="y-bmac-brand">BMAC</span>

            </a>


        </div> -->
        <!-- <p class="y-pfor-business-collab text-center">
            <small class="y-business-collaboration y-white">Business collaboration</small>
        </p> -->
        @yield('content')
    <!-- </div>
</section> -->

<!-- footer -->
{{--<footer id="footer">
    <div class="text-center text-light padder y-rebrand-container">
        <p>
            <small>Business Measurement Assurance & Collaboration
                <br>&copy; {{ date('Y') }}. Powered by
                <img src="{{ asset('images/slide-logo.png') }}" class="y-rebrand-logo">
                <span class="y-technologies">Technologies</span>
                <!-- <a href="www.ikooba.com">ikOOba</a> -->
            </small>
        </p>
    </div>
</footer>--}}
<!-- / footer -->
<!-- Bootstrap -->
<!-- App -->
<script src="{{ asset('js/app.v1.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>

<!-- javascripts -->
<!-- bootstrap -->
<script src="{{ asset('newui/js/bootstrap.min.js')}}" rel="stylesheet"></script>
<?php if(config("app.env") == "production"): ?>
<script src="{{ asset('js/chat/twak.js') }}"></script>
<?php endif; ?>

</body>
</html>