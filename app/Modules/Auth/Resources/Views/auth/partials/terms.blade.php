<!-- Modal -->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Terms and Conditions of Use </h4>
            </div>

            <div class="modal-body">
                <i class="" style="color:gray; font-size: 13px;">February 2016</i><br><br>

                <p>BMAC, along with its principal and affiliates, provides the information and services under the
                    following terms and conditions.</p>

                <ol><strong>1. ACCEPTANCE OF TERMS OF USE. </strong></ol>
                <p class="p-size">By accessing and using this site you agree to be bound by the following Terms of
                    Use and all terms and conditions contained and/or referenced herein or any additional terms and
                    conditions set forth on www.onebmac.com and all such terms shall be deemed accepted by you. If
                    you do NOT agree to all these Terms of Use, you should NOT use this site. If you do not agree to
                    any additional specific terms which apply to particular Content (as defined below) or to
                    particular transactions concluded through this site, then you should NOT use the part of the
                    site which contains such Content or through which such transactions may be concluded and you
                    should not use such Content or conclude such transactions.</p>

                <p class="p-size">These Terms of Use may be amended by BMAC at any time. Such amended Terms of Use
                    shall be effective upon posting on www.onebmac.com. Please check the Terms of Use published on
                    this Site regularly to ensure that you are aware of all terms governing your use of this site.
                    Also, specific terms and conditions may apply to specific content, products, materials,
                    services, discussion forums, or information contained on or available through this Site (the
                    "Content") or transactions concluded through this Site. Such specific terms may supplement these
                    Terms of Use or, where inconsistent with these Terms of Use, such specific terms will supersede
                    these Terms of Use only to the extent that the content or intent of such specific terms is
                    inconsistent with these Terms of Use.</p>

                <p class="p-size">BMAC reserves the right to make changes or updates with respect to or in the
                    content of the Site or the format thereof at any time without notice. BMAC reserves the right
                    to terminate or restrict access to the Site for any reason whatsoever at its sole discretion.
                </p><br>

                <ol><strong>2. Definitions.</strong></ol>
                <p class="p-size">
                    “BMAC” means an enterprise solution Plot 46 Ishawu Adewale Street, Off Modupe
                    Johnson Crescent, Surulere Lagos, Nigeria. A company organized under the laws of
                    Nigeria and an affiliate and licensee of BMAC Systems.
                </p>

                <p class="p-size">“Compatible Computer” means a Computer that conforms to the system requirements of
                    the Software as specified in the Documentation.
                </p>

                <p class="p-size">“Computer” means a virtual machine or physical personal electronic device that
                    accepts information in digital or similar form and manipulates it for a specific result based on
                    a sequence of instructions.
                </p>

                <p class="p-size">“SaaS” means Software as a Service.
                </p>

                <p class="p-size">"Server" means the location on which this website is placed.
                </p>

                <p class="p-size">"Services" means services available from BMAC.
                </p>

                <p class="p-size">"Software" - means each BMAC software program provided by BMAC or made
                    available through its Website, including software in which BMAC has sub-licensing
                    rights, in executable, machine readable, object, printed or interpreted form,
                    including any Software Protection Device and any documentation, modifications,
                    improvements or updates supplied to the Licensee under any Quotation/Order.
                </p>

                <p class="p-size">"Terms" means these General Terms.
                </p>

                <p class="p-size">“Use” means to access, install, download, copy, or otherwise benefit from using
                    the functionality of the Software.
                </p><br>


                <ol><strong>3. LAWS AND REGULATIONS. </strong></ol>

                <p class="p-size">Your access to and use of www.onebmac.com is subject to all applicable
                    international, federal, state and local laws and regulations. You agree not to use the Site in
                    any way that violates such laws or regulations.
                </p><br>

                <ol><strong>4. Copyright Notice. </strong></ol>
                <p class="p-size">Copyright and all other proprietary rights in the Content (including but not
                    limited to software, audio, video, text and photographs) rest with BMAC or its licensors. All
                    rights in the content not expressly granted herein are reserved. Except as stated herein, none
                    of the material may be copied, reproduced, modified, distributed, republished, downloaded,
                    displayed, posted or transmitted in any form or by any means, including, but not limited to,
                    electronic, mechanical, photocopying, recording, or otherwise, without the prior written
                    permission of BMAC or the copyright owner. All copyright and other proprietary notices shall
                    be retained on all permitted reproductions. Permission is granted to display, copy, distribute
                    and download the materials on this Site for personal, non-commercial use only; provided you do
                    not modify the materials and that you retain all copyright and other proprietary notices
                    contained in the materials. You also may not, without BMAC’s permission, "mirror" any material
                    contained on this Site on any other server. This permission terminates automatically if you
                    breach any of these terms or conditions. Upon termination, you must immediately destroy any
                    downloaded and/or printed materials. Any unauthorized use of any material contained on this Site
                    may violate copyright laws, trademark laws, the laws of privacy, communications, regulations and
                    statutes.
                </p>

                <p class="p-size">Any material or information sent through or in connection with this Site by you
                    ("User Materials") will be treated as non-confidential and non-proprietary, and immediately
                    become the property of BMAC, subject to the applicable Privacy Statement posted on this Site.
                    BMAC may use such User Materials as it deems fit, anywhere in the world, without obligation
                    for compensation, and free of any moral rights, intellectual property rights and/or other
                    proprietary rights in or to such User Materials.
                </p>

                <p class="p-size">This Site may contain references to specific BMAC products and services that may
                    not be (readily) available in a particular country. Any such reference does not imply or warrant
                    that any such products or services shall be available at any time in any particular country.
                    Please contact BMAC helpdesk on helpdesk@ikOOba.com for further information.
                </p><br>


                <ol><strong>5. TRADEMARKS. </strong></ol>
                <p class="p-size">Unless otherwise indicated, all trademarks, service marks and logos displayed on
                    the Site are registered and unregistered trademarks of BMAC (or its affiliates, subsidiaries
                    or divisions). These include each of BMAC’s primary product brands and service offerings and
                    its corporate logos and emblems.
                </p><br>


                <ol><strong>6. TAMPERING. </strong></ol>
                <p class="p-size">User agrees not to modify, move, add to, delete or otherwise tamper with the
                    information contained in BMAC’s Site.
                </p>

                <p class="p-size">User also agrees not to decompile, reverse engineer, disassemble or unlawfully use
                    or reproduce any of the software, copyrighted or trademarked material, trade secrets, or other
                    proprietary information contained in the Site.
                </p><br>


                <ol><strong>7. INDEMNITY. </strong></ol>
                <p class="p-size">You agree to defend, indemnify, and hold harmless BMAC, its principals,
                    affiliates, subsidiaries, and divisions, and each of their officers, directors, employees and
                    agents, from and against any claims, actions or demands, including without limitation reasonable
                    legal and accounting fees, alleging or resulting from your use of the Content (including
                    software) or your breach of the terms of this Agreement. BMAC shall provide notice to you
                    promptly of any such claim, suit, or proceeding and shall assist you, at your expense, in
                    defending any such claim, suit or proceeding.
                </p><br>


                <ol><strong>8. THIRD PARTY INFORMATION. </strong></ol>
                <p class="p-size">Although BMAC monitors the information on the Site, some of the information may
                    be supplied by independent third parties. While BMAC makes every effort to ensure the accuracy
                    of all information on the Site, BMAC makes no warranty as to the accuracy of any such
                    information.
                </p><br>


                <ol><strong>10. LINKS TO THIRD PARTY SITES. </strong></ol>
                <p class="p-size">This Site may contain links that will let you access other Web sites that are not
                    under the control of BMAC. The links are only provided as a convenience and BMAC does not
                    endorse any of these sites. BMAC assumes no responsibility or liability for any material that
                    may be accessed on other Web sites reached through this Site, nor does BMAC make any
                    representation regarding the quality of any product or service contained at any such site.
                </p><br>

                <ol><strong>11. NO WARRANTIES. </strong></ol>
                <p class="p-size">Although care has been taken to ensure the accuracy of information on
                    www.onebmac.com, BMAC assumes no liability therefore. Information and documents provided on
                    this Site are provided "AS IS" and “AS AVAILABLE”. BMAC hereby disclaims any representations
                    and warranties of any kind, either express or implied, including without limitation, warranties
                    of merchantability, fitness for a particular purpose, non-infringement, title or as to operation
                    or content. BMAC uses reasonable efforts to include accurate and up-to-date information on
                    this Site; it does not, however, make any warranties or representations as to its accuracy or
                    completeness. BMAC periodically adds, changes, improves, or updates the information and
                    documents on this Site (including but not limited to products, pricings, programs, events, and
                    offers) without notice. BMAC assumes no liability or responsibility for any errors or
                    omissions in the content of this Site.
                </p><br>


                <ol><strong>12. LIMITATION OF LIABILITY. </strong></ol>
                <p class="p-size">UNDER NO CIRCUMSTANCES SHALL BMAC OR ANY OF ITS PRINCIPALS, AFFILIATES,
                    SUBSIDIARIES, OR DIVISIONS, BE LIABLE FOR ANY DAMAGES SUFFERED BY YOU, INCLUDING ANY INCIDENTAL,
                    SPECIAL OR CONSEQUENTIAL DAMAGES (INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS OR DAMAGES FOR
                    BUSINESS INTERRUPTION, LOSS OF INFORMATION, PROGRAMS OR OTHER DATA) THAT RESULT FROM ACCESS TO,
                    USE OF, OR INABILITY TO USE THIS SITE OR DUE TO ANY BREACH OF SECURITY ASSOCIATED WITH THE
                    TRANSMISSION OF INFORMATION THROUGH THE INTERNET, EVEN IF EXACT WAS ADVISED OF THE POSSIBILITY
                    OF SUCH DAMAGES. ANY ACTION BROUGHT AGAINST EXACT PERTAINING TO OR IN CONNECTION WITH THIS SITE
                    MUST BE COMMENCED AND NOTIFIED TO BMAC IN WRITING WITHIN ONE (1) YEAR AFTER THE DATE THE CAUSE
                    FOR ACTION AROSE.
                </p><br>

                <ol><strong>13. PRIVACY. </strong></ol>
                <p class="p-size">Protecting the privacy of our clients and users of our Sites is important to
                    BMAC. The BMAC Privacy Statement, as updated and amended from time to time, describes how we
                    use and protect information you provide to us.
                </p><br>

                <ol><strong>14. SECURITY. </strong></ol>
                <p class="p-size">The security of information transmitted through the Internet can never be
                    guaranteed. BMAC DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS AS TO THE SECURITY OF THIS SITE.
                    YOU ACKNOWLEDGE ANY INFORMATION SENT MAY BE INTERCEPTED. EXACT DOES NOT WARRANT THAT THE SITE OR
                    THE SERVERS, WHICH MAKE THIS SITE AVAILABLE OR ELECTRONIC COMMUNICATIONS SENT BY BMAC ARE FREE
                    FROM VIRUSES OR ANY OTHER HARMFUL ELEMENTS. EXACT is not responsible for any interception or
                    interruption of any communications through the Internet or for changes to or losses of data. You
                    are responsible for maintaining the security of any password, user ID, or other form of
                    authentication involved in obtaining access to password protected or secure areas of BMAC
                    sites. In order to protect you and your data, BMAC may suspend your use of a client site,
                    without notice, pending an investigation, if any breach of security is suspected.
                </p><br>

                <ol><strong>15. Software License Usage. </strong></ol>
                <p class="p-size">If you obtain BMAC Software from its authorized licensees, and subject to your
                    compliance with the terms of this agreement, BMAC grants to you an exclusive license to Use
                    the Software in the manner and for the purposes described in the Documentation as follows:
                </p>
                <p class="p-size">
                    General Use: You may install and Use one copy of the Software on your Compatible
                        Computer.

                    BMAC is not one product but the terms and conditions under review relates to BMAC MSME solution
                </p>
                <p class="p-size">
                    Software Use: This agreement permits you to sign on to the platform or download any of the
                        available premium products of the BMAC enterprise solution. For information on the Use of
                        the Software and for information about BMAC, kindly contact the BMAC helpdesk
                        helpdesk@iKooba.com.

                </p>
                <p class="p-size">
                    Distribution: This license does not grant you the right to sublicense or distribute the
                        Software.

                </p>
                <br>


                <ol><strong>16. Use of BMAC Online Services. </strong></ol>
                <p class="p-size">In some cases, access to an BMAC Online Service might require a separate
                    subscription or other fee in order to access it, and/or your assent to additional terms of use
                    (premium modules). BMAC Online Services might not be available in all languages or to
                    residents of all countries and BMAC may, at any time and for any reason, modify or discontinue
                    the availability of any BMAC Online Service with notification to users. BMAC also reserves
                    the right to begin charging a fee for access to or use of an BMAC Online Service that was
                    previously offered at no charge.
                </p>
                <p class="p-size">If your Computer is connected to the Internet, the Software may, without
                    additional notice, update downloadable materials from these BMAC Online Services so as to
                    provide immediate availability of these BMAC Online Services even when you are offline. When
                    the Software connects to the Internet as a function of an BMAC Online Service, your IP
                    Address, user name, and password may be sent to BMAC’s servers and stored by BMAC in
                    accordance with the Additional Terms of Use. This information may be used by BMAC to send you
                    transactional messages to facilitate the BMAC Online Service. BMAC may display in-product
                    marketing to provide information about the Software and other BMAC products and Services,
                    including but not limited to BMAC Online Services, based on certain Software specific features
                    including but not limited to, the version of the Software, including without limitation,
                    platform version, version of the Software, and language.
                </p>
                <p class="p-size">
                    For downloaded software, when your computer is connected to the Internet, the Software
                        may, without additional notice, check for Updates that are available for automatic download
                        and installation to your Computer and let BMAC know the Software is successfully
                        installed. For Users, Updates may be automatically downloaded but not installed without
                        additional notice unless you change your preferences to accept automatic installation. Only
                        non-personally identifying information is transmitted to BMAC when this happens, except to
                        the extent that IP Addresses may be considered personally identifiable in some
                        jurisdictions.

                </p>
                <p class="p-size">For further information about in-product marketing, please see the “help” menu in
                    the Software. Whenever the Software makes an Internet connection and communicates with an BMAC
                    website, whether automatically or due to explicit user request, the BMAC Online Privacy Policy
                    shall apply. Additionally, unless you are provided with separate terms of use at that time, the
                    BMAC Terms of Use shall apply. Please note that the BMAC Privacy Policy allows tracking of
                    website visits and it addresses in detail the topic of tracking and use of cookies, web beacons,
                    and similar devices.
                </p><br>


                <ol><strong>17. TRANSMISSION OF PERSONAL DATA. </strong></ol>
                <p class="p-size">You acknowledge and agree that by providing BMAC with any data and information
                    through the Site, you consent to the transmission of such information over international borders
                    as necessary for processing in accordance with BMAC’s standard business practices and the
                    applicable Privacy Statement.
                </p><br>

                <ol><strong>18. ACCESS TO PASSWORD PROTECTED/SECURE AREAS. </strong></ol>
                <p class="p-size">Access to and use of password protected and/or secure areas of www.onebmac.com is
                    restricted to authorized users only. Unauthorized access to such areas is prohibited and may
                    lead to criminal prosecution.
                </p><br>

                <ol><strong>19. BUDGETING / ASSUMPTIONS. </strong></ol>
                <p class="p-size">The information on this Site may contain certain projections or assumptions. By
                    their nature, projections and assumptions involve risk and uncertainty because they relate to
                    events and depend on circumstances that will occur in the future. There are a number of factors
                    that could cause actual results and developments to differ materially from those expressed or
                    implied by these projections or forward-looking statements. Hence, results of the projections or
                    assumptions are entirely dependent on information produced by the user and BMAC cannot be held
                    liable for this.
                </p><br>

                <ol><strong>20. SPECIFIC SOFTWARE AVAILABLE ON THIS SITE. </strong></ol>
                <p class="p-size">Any software that may be made available to download from www.onebmac.com (the
                    "Software") is the copyrighted work of BMAC and/or a third party providing that Software.
                    Software made available for downloading from or through this Site is licensed subject to the
                    terms of the applicable license agreement, which may accompany or be included with the Software
                    ("License Agreement"). An end user may be unable to install any Software that is accompanied by
                    or includes a License Agreement, unless the end user first agrees to the License Agreement
                    terms. Except as set forth in the applicable License Agreement, the Software is made available
                    for use by end users only and any further copying, reproduction or redistribution of the
                    Software is expressly prohibited. WARRANTIES, IF ANY, WITH RESPECT TO SUCH SOFTWARE SHALL ONLY
                    APPLY AS EXPRESSLY SET FORTH IN THE APPLICABLE LICENSE AGREEMENT. BMAC HEREBY EXPRESSLY
                    DISCLAIMS ALL FURTHER REPRESENTATIONS AND WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
                    WARRANTIES OF MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE, TITLE OR NON-INFRINGEMENT
                    WITH RESPECT TO THE SOFTWARE.
                </p><br>

                <ol><strong>21. Support </strong></ol>
                <p class="p-size">BMAC will provide the user with appropriate support in the case of Defects of
                    the Software. This support will be offered at no cost. The support will be provided within
                    reasonable time from the date the defect is logged by BMAC from the user.
                </p>
                <p class="p-size">At the request of the user, BMAC may, at its sole discretion, provide support
                    not related to any Defects. The user shall pay BMAC for such support in accordance with
                    BMAC’s price list in effect from time to time.
                </p><br>

                <ol><strong>22. JURISDICTION/GOVERNING LAW. </strong></ol>
                <p class="p-size">These Terms of Use shall be governed by and construed in accordance with the laws
                    of The Federal Republic of Nigeria without regards to its principles of conflict of laws. You
                    agree that any disputes in connection with this agreement or its enforcement shall be first
                    resolved in arbitration failing which the dispute will be heard in a competent court of law in
                    The Federal Republic of Nigeria.
                </p>
                <p class="p-size">In the event that any provision of the Terms of Use are deemed by a court of
                    competent jurisdiction to be invalid or unenforceable, the invalid portion of the Terms of Use
                    shall be considered to be modified as closely as possible to the intent of BMAC and the
                    remainder of the Terms of Use shall remain in full force and effect.
                </p><br>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

        <div class="modal-footer">

        </div>

    </div>
</div>

