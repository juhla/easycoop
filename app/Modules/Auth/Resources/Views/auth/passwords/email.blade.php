@extends('auth::auth.layout')

<!-- Main Content -->
@section('content')
   {{-- <section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
            <strong>Reset Password</strong>
        </header>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="panel-body wrapper-lg" role="form" method="POST" action="{{ url('secure/password/email') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label">Enter E-Mail Address</label>
                <input type="email" name="email" class="form-control input-lg" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{ url('secure/sign-in') }}" class="btn btn-default">
                            <i class="fa fa-btn fa-arrow-left"></i> Sign In
                        </a>
                    </div>
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </section>--}}


    <section>
        <div class="container-fluid link-reset">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 lr-top">
                        <img src="{{asset('newui/images/bmac-logo.png')}}" alt="bmac-logo" class="img-responsive">
                        <h2 class="biz-col">Business Collaboration</h2>
                        
                        <form role="form" method="POST" action="{{ url('secure/password/email') }}">
                        {{ csrf_field() }}
                       
                        <div class="sign-area lr-shadow">
                            <div class="inside-x1">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <a href="{{ url('secure/sign-in') }}">
                                            <button class="btn float-right btn-skip">
                                                    Sign in
                                                    <i class="fa fa-sign-in"></i>
                                                </button>
                                        </a>
                                    </div>
                                </div>
                                <h2>Enter your E-mail Address</h2>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="email" name="email" class="form-control form-x2" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="alert-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="lr-password">
                                    <button type="submit" class="btn btn-pwrd">Send reset link</button>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="lr-bottom">
                            <h2 class="lr-x4">Business Measurement Assurance & Collaboration</h2>
                            <h2 class="lr-x5">
                                &copy; {{ date('Y') }}. Powered by 
                                <img src="{{asset('newui/images/ikOOba logo1.png')}}" alt="logo" class="lr-img">
                                 Technologies
                                </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@endsection

