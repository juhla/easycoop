@extends('auth::auth.layout')

@section('content')
   {{--<section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
            <strong>Reset Password</strong>
        </header>
        <form class="panel-body wrapper-lg" role="form" method="POST" action="{{ url('secure/password/reset') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                @if ($errors->has('email'))
                    <span class="alert-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>
                <input id="password" type="password" class="form-control" name="password">
                @if ($errors->has('password'))
                    <span class="alert-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                @if ($errors->has('password_confirmation'))
                    <span class="alert-danger">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary ">
                        <i class="fa fa-btn fa-refresh"></i> Reset Password
                    </button>
                </div>
            </div>
        </form>
    </section>--}}

     <section>
        <div class="container-fluid link-reset">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 lr-top">
                        <div class="text-center img-top">
                         <img src="{{asset('newui/images/bmac-logo.png')}}" alt="bmac-logo" class="img-responsive">
                        </div>
                        <h2 class="biz-col">Business Collaboration</h2>

                        <div class="sign-area lr-shadow rp-x1">
                            <div class="inside-x1">
                                <form class="panel-body wrapper-lg" role="form" method="POST" action="{{ url('secure/password/reset') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>E-mail Address</label>
                                    <input id="email" type="email" class="form-control form-x2" name="email" value="{{ $email or old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="alert-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Enter Password</label>
                                    <input id="password" type="password" class="form-control form-x2" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="alert-danger">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password-confirm">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control form-x2" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="alert-danger">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="lr-password">
                                    <button type="submit" class="btn btn-pwrd">Reset Password</button>
                                </div>
                                </form>

                            </div>
                        </div>
                        <div class="lr-bottom">
                            <h2 class="lr-x4">Business Measurement Assurance & Collaboration</h2>
                            <h2 class="lr-x5">
                                &copy; {{ date('Y') }}. Powered by 
                                <img src="{{asset('newui/images/ikOOba logo1.png')}}" alt="logo" class=""> 
                                Technologies</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
