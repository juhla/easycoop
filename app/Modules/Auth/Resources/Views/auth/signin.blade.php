@extends('auth::auth.layout')

@section('content')
<section class="panel panel-default bg-white m-t-lg">
    <header class="panel-heading text-center">
        <strong>Sign in</strong>
    </header>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}
            @endforeach
            <br>
        </div>
    @endif
    @include('flash::message')
    <form action="{{ route('auth.login') }}" class="panel-body wrapper-lg" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="control-label">Email</label>
            <input type="email" name="email" class="form-control input-lg" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label class="control-label">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control input-lg">
        </div>
        <div class="checkbox">
            <label> <input type="checkbox"> Keep me logged in </label>
        </div>
        <a href="{{ url('secure/password/reset') }}" class="pull-right m-t-xs"><small>Forgot password?</small></a>
        <button type="submit" class="btn btn-primary">Sign in</button>
        <div class="line line-dashed"></div>
        <p class="text-muted text-center"><small>Do not have an account?</small></p>
        <a href="{{ url('secure/sign-up') }}" class="btn btn-default btn-block">Create an account</a>
    </form>
</section>
@endsection