@extends('auth::auth.layout')
@section('content')
    <style type="text/css">
        .modal-header {
            background: #0a7a48;
            color: #ffffff;
        }

        .close {
            color: #ffffff !important;
            opacity: initial !important;
        }

        .p-size {
            font-size: 12px;
        }

        .text-red {
            color: #ff0000;
        }
    </style>
    <section class="panel panel-default m-t-lg bg-white">
        <header class="panel-heading text-center">
            <strong>Sign up</strong>
        </header>
        <div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div id="validationMessages">
        </div>

        @include('flash::message')

        <form action="{{ route('auth.ajax_register') }}" id="signupForms" class="panel-body wrapper-lg" method="post"
              data-validate="parsley">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="control-label">First Name</label>
                <input type="text" name="first_name" class="form-control " data-required="true"
                       value="{{ old('first_name') }}">
            </div>
            <div class="form-group">
                <label class="control-label">Last Name</label>
                <input type="text" name="last_name" class="form-control " data-required="true"
                       value="{{ old('last_name') }}">
            </div>
            <div class="form-group">
                <label class="container-label">Tell us who you are</label>
                <select name="role_id" id="role_id" class="form-control">
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">Company Name</label>
                <input type="text" name="company_name" class="form-control " data-required="true"
                       value="{{ old('company_name') }}">
            </div>
            <div class="form-group">
                <label class="control-label">RC Number</label>
                <input type="text" name="rc_number" class="form-control " data-required="true"
                       value="{{ old('rc_number') }}">
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="email" name="email" class="form-control " data-required="true" data-type="email"
                       value="{{ $email }}">
            </div>
            <div class="form-group">
                <label class="control-label">Phone Number</label>
                <input type="text" name="phone_no" class="form-control " data-required="true"
                       value="{{ old('phone_no') }}">
            </div>
            <div class="form-group">
                <label class="control-label">Password</label>
                <input type="password" id="password" name="password" class="form-control" data-required="true">
            </div>
            <div class="form-group">
                <label class="control-label">Confirm Password</label>
                <input type="password" name="password_confirmation" class="form-control" data-required="true"
                       data-equalto="#password">
            </div>
            <div class="checkbox">
                <label> <input type="checkbox" name="terms" data-required="true"> Agree the
                    <a data-toggle="modal" data-target="#myModal1">terms and
                        policy</a>
                </label>
            </div>

            <?php
            if(config("app.env") == "production"): ?>
            <div>
                {{--{!! Recaptcha::render() !!}--}}
            </div>

            <?php endif; ?>

            <br>
            <button type="submit" id="signupBtn" class="btn btn-primary btn-block">Sign up</button>
            {{--<div class="col-lg-offset-2 col-lg-10">--}}
            {{--<button type="button" onclick="signUp();" id="signupBtn" class="btn btn-lg btn-primary">--}}
            {{--<i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none"></i> Sign up--}}
            {{--</button>--}}
            {{--</div>--}}
            <div class="line line-dashed"></div>
            <p class="text-muted text-center">
                <small>Already have an account?</small>
            </p>
            <a href="{{ url('secure/sign-in') }}" class="btn btn-default btn-block">Sign in</a>
        </form>

    </section>


    @include('auth.partials.terms')



    <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3 top-space">
                        <div class="cssload-dots">
                            <div class="cssload-dot"></div>
                            <div class="cssload-dot"></div>
                            <div class="cssload-dot"></div>
                            <div class="cssload-dot"></div>
                            <div class="cssload-dot"></div>
                        </div>

                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <defs>
                                <filter id="goo">
                                    <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12"></feGaussianBlur>
                                    <feColorMatrix in="blur" mode="matrix"
                                                   values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7"
                                                   result="goo"></feColorMatrix>
                                    <!--<feBlend in2="goo" in="SourceGraphic" result="mix" ></feBlend>-->
                                </filter>
                            </defs>
                        </svg>

                    </div>

                    <div class="col-md-6 col-md-offset-3">
                        <center class="sign-message">
                            <p class="wait-text">Please wait while we setup your account...</p>
                        </center>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
<style>
    .modal-backdrop {
        display: none !important;
    }

    .modal {
        text-align: center;
        display: block;
    !important;
        background: rgba(47, 46, 46, 0.44);
    !important;
        position: fixed;
    !important;
    }

    @media screen and (min-width: 768px) {
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

</style>
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('css/touch.css') }}" type="text/css"/>


<?php $success = URL("secure/success-page");?>

<script>


    function addRow(message) {
        var div = document.createElement('div');
        div.className = 'alert alert-danger';
        $(div).append("<ul>");
        $.each(message, function (index, element) {
            for (var key in element)
                $(div).append("<li>" + element[key] + "</li>");
        });
        $(div).append("</ul>");
        //$("#validationMessages").appendChild(div);
        document.getElementById('validationMessages').appendChild(div);
    }

    function addMessage(message) {
        var div = document.createElement('div');
        div.className = 'alert alert-danger';
        $(div).append("<p>" + message + "</p>");
        document.getElementById('validationMessages').appendChild(div);
    }

    function removeRow() {
        $('#validationMessages').children().remove();
        // document.getElementById('validationMessages').removeChild(input.parentNode);
    }

    function hideDiv() {
        $('#myModal').modal('hide');
        $('#signupBtn').attr('disabled', false);
    }

    $(document).on("submit", "#signupForms", function (event) {
        event.preventDefault();
        removeRow();
        $('#signupBtn').attr('disabled', true);
        var url = $(this).attr('action');
        var data = $(this).serialize();
        loadingScreen();
        $('#myModal').modal('show');
        $.ajax({
            url: url,
            data: data,
            type: "GET",
            dataType: 'json',
            success: function (e) {


                if (e.type === 'validator_fails') {
                    hideDiv();
                    addRow(e.message);
                } else if (e.type === 'email_fails') {
                    hideDiv();
                    addMessage("Email sending failed");
                    //displayNotification('Email sending failed', 'Error!', 'error');
                } else if (e.type === 'fail') {
                    hideDiv();
                    addMessage(e.message);
                }
                else if (e.type === 'success') {
                    window.location.replace("<?php echo $success ?>");
                }
            },
            error: function (e) {
                hideDiv();
                console.log(e);
                addMessage("Error !, something went wrong");
            }
        });
    });


    var loadingScreen = function () {
        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;
        $(".next").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);

        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width", percent + "%")
                .html(percent + "%");
        }
    };

</script>

