<!DOCTYPE html>
<html lang="en">
<!-- <head> -->
    <meta charset="UTF-8">
    <script src="{{ asset('newui/js/jquery.min.js')}}"></script>
    <script src="{{ asset('newui/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('setup/css/touch2.css')}}">
    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <link href="{{ asset('newui/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/rebrandingNov2017.css') }}" type="text/css"/>
    <link href="{{ asset('newui/css/style.css')}}" rel="stylesheet">

    <title>Verify</title>
    <style>

        </style>
</head>
<body class="wc-color">

<section>
        <div class="container-fluid wc-body">
            <div class="container">
                <div class="wc-head">
                    <img src="{{asset('newui/images/bmac-logo.png')}}" alt="logo" class="img-responsive">
                    <h2 class="wc-col">Business Collaboration</h2>
                </div>
                <div class="wc-txt">
                    <div class="row wc-wrapper">
                        <div class="col-md-12">
                            <div class="wc-x1">
                                <div class="congrat">
                                    <img src="{{asset('newui/images/congratulation.png')}}" alt="img" class="img-responsive">
                                </div>
                                <h2 class="wc-x2">Welcome to BMAC</h2>
                                <h2 class="wc-x3">Kindly check your email for an activation link</h2>
                                <h2 class="wc-x4">Business Measurement Assurance & Collaboration</h2>
                                <h2 class="wc-x5">&copy; 2018. Powered by <img src="{{asset('newui/images/ikOOba logo1.png')}}" alt="logo" class="img-responsive"> Technologies</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        var current = 1,current_step,next_step,steps;
        steps = $("fieldset").length;
        $(".next").click(function(){
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function(){
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep){
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                    .css("width",percent+"%")
                    .html(percent+"%");
        }
    });
</script>