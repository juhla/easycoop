@extends('auth::auth.layout')

@section('content')
            <section class="panel panel-default bg-white m-t-lg">
                <header class="panel-heading text-center">
                    <strong>Account Verification</strong>
                </header>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        {{ $error }}
                        @endforeach
                        <br>
                    </div>
                @endif
                @include('flash::message')

                @if(request()->get('type') === 'not-registered')
                    <form action="{{ route('auth.password.new') }}" class="panel-body wrapper-lg" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="email" value="{{ request()->get('email') }}" />
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" name="password" class="form-control input-lg" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Confirm Password</label>
                            <input type="password" id="inputPassword" name="password_confirmation" class="form-control input-lg">
                        </div>
                        <button type="submit" class="btn btn-primary">Sign in</button>
                    </form>
                @else
                    <a href="{{ url('secure/sign-in') }}" class="btn btn-priyart btn-block">Sign In</a>
                @endif
            </section>
        @endsection