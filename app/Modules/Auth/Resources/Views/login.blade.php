<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>One Bmac, Accounting Platform</title>

    <!-- Bootstrap CSS -->

    <link href="{{ asset('newui/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- bootstrap theme -->
    <link href="{{ asset('newui/css/css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{ asset('homepage/asset/css/elegant-icons-style.css')}}" rel="stylesheet" />
    <link href="{{ asset('homepage/asset/css/font-awesome.min.css')}}" rel="stylesheet" />
        <!-- Custom styles -->
    <link rel="stylesheet" href="{{ asset('asset/css/fullcalendar.css')}}">
    <link href="{{ asset('homepage/asset/css/widgets.css')}}" rel="stylesheet">
    <link href="{{ asset('homepage/asset/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('homepage/asset/css/style-responsive.css')}}" rel="stylesheet" />
    <link href="{{ asset('homepage/asset/css/xcharts.min.css')}}" rel=" stylesheet">
    <link href="{{ asset('homepage/asset/css/jquery-ui-1.10.4.min.css')}}" rel="stylesheet">
    <link href="{{ asset('homepage/asset/css/mystyle.css')}}" rel="stylesheet">
    <link href="{{ asset('homepage/asset/css/style2.css')}}" rel="stylesheet">
    <link href="{{ asset('newui/css/style.css')}}" rel="stylesheet">


</head>
<body class="body-wrapper">

<section>
        <div class="container-fluid cf-r1">
            <div class="row">
                <div class="col-md-6 col-lg-5 col-sm-6 col-xs-5 acc-x1">
                    <img src="{{ asset('newui/images/accountant.png')}}" alt="image" class="img-responsive">
                    <div class="acc-x2">
                        <h2 class="bmac-r1">BMAC Accounting</h2>
                        <p class="bmac-txt">Collaborative accounting software for business</p>
                        <div class="btn-acc">
                            <a href="{{url('secure/sign-in')}}"><button class="btn btn-more">More</button></a></div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-7 col-sm-6 col-xs-7 mp-x1">
                    <div class="sign-up1">
                         <a href="{{url('/')}}"><img src="{{asset('newui/images/bmac-logo.png')}}" alt="bmac-logo" class="img-responsive"/></a>
                    </div>
                    <div class="sign-area">
                        <div class="inside-x1">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                            <br>
                        </div>
                        @endif
                        @include('flash::message')

                            <form action="{{ route('auth.login') }}" class="form-wrap" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <input type="text" class="form-control form-x1" id="username"
                                           placeholder="Enter username" name="username" value="{{ old('username') }}">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-x1" id="pwd" placeholder="Enter password" name="password">
                                </div>
                                <div class="row log-x1">
                                <div class="col-md-6 log">
                                    <div class="checkbox">
                                        <label class="dx-r1"><input type="checkbox">Keep me logged in</label>
                                    </div>
                                </div>
                                <div class="col-md-6 log">
                                    <a href="{{ url('secure/password/reset') }}">Forgot my password</a>
                                </div>
                            </div>
                            <div class="btn-up">
                                <button type="submit" class="btn btn-signup">Sign in</button>
                                <!-- <input type="submit" class="btn btn-signup" value="submit"/> -->
                            </div>
                            </form>
                           
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

<div class="container-fluid">
    <div class="row footer-poweredby">
        <div class="col-md-12 center-block text-center"> 
        Powered by <a href="http://ikooba.com" target="_blank" class="whitepower">Ikooba Technologies</a> &nbsp;&copy; 2018 BMAC
        </div>
    </div>
    
</div>













<!-- javascripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<!-- bootstrap -->
<script src="{{ asset('newui/js/bootstrap.min.js')}}" rel="stylesheet"></script>


<script>
    //knob
    // $(function() {
    //     $(".knob").knob({
    //         'draw' : function () {
    //             $(this.i).val(this.cv + '%')
    //         }
    //     })
    // });

    // //carousel
    // $(document).ready(function() {
    //     $("#owl-slider").owlCarousel({
    //         navigation : true,
    //         slideSpeed : 300,
    //         paginationSpeed : 400,
    //         singleItem : true

    //     });
    // });

    // //custom select box

    // $(function(){
    //     $('select.styled').customSelect();
    // });

    // /* ---------- Map ---------- */
    // $(function(){
    //     $('#map').vectorMap({
    //         map: 'world_mill_en',
    //         series: {
    //             regions: [{
    //                 values: gdpData,
    //                 scale: ['#000', '#000'],
    //                 normalizeFunction: 'polynomial'
    //             }]
    //         },
    //         backgroundColor: '#eef3f7',
    //         onLabelShow: function(e, el, code){
    //             el.html(el.html()+' (GDP - '+gdpData[code]+')');
    //         }
    //     });
    // });

    



</script>

</body>
</html>
