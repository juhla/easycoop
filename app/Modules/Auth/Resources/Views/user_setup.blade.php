<!DOCTYPE html>
<html lang="en">
<?php
//dd($countries);
?>

<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    {{--<link rel="stylesheet" href="{{ asset('setup/css/bootstrap.min.css')}}">--}}
   
    <link href="{{ asset('newui/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ asset('newui/css/bootstrap-theme.css')}}" rel="stylesheet">

    <script src="{{ asset('js/jquery.min.js')}}"></script>
    
    {{--<script src="{{ asset('js/bootstrap.min.js')}}"></script>--}}
    <script src="{{ asset('newui/js/bootstrap.min.js')}}"></script>

    <link rel="stylesheet" href="{{ asset('setup/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('setup/css/touch.css')}}">
    <link rel="stylesheet" href="{{ asset('css/validator/validationEngine.jquery.css')}}">

    <link rel="stylesheet" href="{{ asset('css/rebrandingNov2017.css') }}" type="text/css" />
    {{--
    <link rel="stylesheet" href="{{ asset('css/validator/template.css')}}">--}}
    {{--
    <link rel="stylesheet" href="{{ asset('css/validator/customMessages.css')}}">--}}
    <link href="{{ asset('homepage/asset/css/mystyle.css')}}" rel="stylesheet">
    
    <link href="{{ asset('newui/css/style.css')}}" rel="stylesheet">

    <title>BMAC</title>
</head>

<body class="wc-color">
    <div class="container">

        <div class="container">
            <div class="wc-head">
                <a href="http://www.ikooba.com"> <img src="{{asset('newui/images/bmac-logo.png')}}" alt="img" class="img-responsive"></a>
            </div>
            <div class="qs-x1">

                <form id="regiration_form" novalidate action="{{url('businessOwnerSetup')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div class="row">
                            <div class="col-md-12 skip">
                                <a href="{{url('/workspace')}}">
                                    <button class="btn float-right btn-skip">
                                    Skip
                                    <i class="fa fa-angle-right"></i>
                                </button>
                                </a>
                            </div>
                        </div>
                            <div class="row">
                        <div class="col-md-12">
                        <h2 class="qs-know">Step 1/2: Tell us more about your business</h2>
                        </div>
                    </div>


                        <div class="form-group">
                            <label for="type_of_business">Type of business <span class="needed">*</span></label>
                            <select data-errormessage="you need to select a type of business" class="validate[required] form-control form-x2"
                                id="typeofbusiness" name="typeofbusiness">
                                <option value="">Select Type of Business</option>
                                @foreach($industries as $industry)
                                <option value="{{$industry->id}}">{{$industry->name}}</option>option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="business_address">Business Address <span class="needed">*</span></label>
                            <textarea class="form-control form-x2" id="businessaddress" name="businessaddress" placeholder="Business Address"
                                required></textarea>
                        </div>


                        <div class="form-group">
                            <label for="country">Country <span class="needed">*</span></label>
                            <select data-errormessage="you need to select a country" class="validate[required] form-control form-x2"
                                id="country" name="country">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>option>
                                @endforeach
                            </select>
                            <input type="hidden" value="{{url('/getCountryInfo')}}" id="getCountryInfo">
                        </div>

                        <div class="form-group">
                            <label for="currency">Currency <span class="needed">*</span></label>
                            <select data-errormessage="you need to select a type of currency" class="validate[required] form-control form-x2"
                                id="typeofcurrency" name="typeofcurrency">
                                <option value="">Select a Country Currency</option>
                                @foreach($countryCurrencies as $industry)
                                <option value="{{$industry->id}}">{{$industry->name}}</option>option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="timezone">Time zone</label>
                            <input type="text" class="form-control form-x2" id="timezone" name="timezone" placeholder="GMT -01 Pacific">
                        </div>

                        <!-- <div class="btn-qs"> -->
                         <input type="button" id="next" name="next" class="btn btn-continue center-button" value="Next" />
                             <!-- <button id="next" name="next" class="btn btn-continue">Next</button> -->
                        <!-- </div> -->
                    </fieldset>

                    <fieldset>
                    <div class="row">
                    <div class="col-md-12 skip">
                            <a href="{{url('/workspace')}}">
                                <button class="btn float-right btn-skip">
                                        Skip
                                        <i class="fa fa-angle-right"></i>
                                    </button>
                            </a>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-md-12">
                            
                            <h2 class="qs-know">Step 2/2: How much accounting do you know</h2>
                        </div>
                     </div>
                       

                        <div class="form-group">
                            <label for="user_level">Accounting knowledge Level <span class="needed">*</span></label>
                            <select class="form-control form-x2" id="user_level" name="user_level">
                                <option value="4">Very little</option>
                                <option value="10">Quite a bit</option>
                                <option value="11">Advanced</option>
                            </select>
                        </div>

            

                        <input type="button" name="previous" class="previous btn btn-back con-x1" value="Back" />
                        <input type="submit" name="submit" class="submit btn btn-continue con-x2" value="Continue" id="submit_data" />
                    </fieldset>

                </form>
            </div>
        </div>
    </div>




    <div class="y-footer-down">
        <p class="text-center y-center">Powered by <img src="{{ asset('images/slide-logo.png') }}" class="y-rebrand-logo">
            <span class="y-technologies">Technologies</span>
        </p>
    </div>


    </div>
</body>

</html>
<script type="text/javascript" src="{{ asset('js/validator/jquery-1.7.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validator/jquery.validationEngine.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validator/jquery.validationEngine-en.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;

        $('#country').change(function () {
            var country = $('#country').val();
            if (country == 0 || country == '') {
                return;
            }
            var url = $('#getCountryInfo').val() + '/' + country;
            $.get(url, function (data) {
                $('#currency').val(data);
            });
        });

        $("#next").click(function () {
            console.log(' next: got here dude');
            current_step = $(this).parent();
            $("#regiration_form").validationEngine({ showOneMessage: true });


            var timeValidate = $("#timezone").validationEngine('validate');
            var businessValidate = $("#typeofbusiness").validationEngine('validate');
            var countryValidate = $("#country").validationEngine('validate');



            if (!businessValidate && !countryValidate) {
                next_step = $(this).parent().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
            }

        });
        $(".previous").click(function () {
            console.log('previous: got here dude');

            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width", percent + "%")
                .html(percent + "%");
        }
    });

</script>