<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', 'AuthController@showLoginForm');
Route::get('/switch', ['uses' => 'LoginController@switchDomain']);
Route::group(['middlewareGroups' => ['web'], 'prefix' => 'secure'], function () {
    Route::get('/sign-in', ['as' => 'auth.login', 'uses' => 'AuthController@showLoginForm']);
    Route::post('/sign-in', ['as' => 'auth.login', 'uses' => 'AuthController@doLogin']);
    Route::post('/api-sign-in', ['as' => 'auth.api-login', 'uses' => 'AuthController@doLoginWeb']);
    Route::get('/sign-out', ['as' => 'auth.logout', 'uses' => 'AuthController@signOut']);


    // Registration Routes...
    Route::get('/sign-up', ['as' => 'auth.register', 'uses' => 'AuthController@showRegistrationForm']);
    //Route::post('/sign-up', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@ajaxRegister']);
    Route::any('/sign_up', ['as' => 'auth.ajax_register', 'uses' => 'AuthController@ajaxRegister']);
    // Route::post('/sign-up', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@doRegister']);
    Route::get('/success-page', 'AuthController@successPage');
    // Password Reset Routes...
    Route::get('/password/reset', ['as' => 'auth.password.reset', 'uses' => 'ForgotPasswordController@showLinkRequestForm']);
    Route::get('/password/reset/{token?}', ['as' => 'auth.password.reset.token', 'uses' => 'ResetPasswordController@showResetForm']);


    Route::post('/password/email', ['as' => 'auth.password.email', 'uses' => 'ForgotPasswordController@sendResetLinkEmail']);
    Route::post('/password/reset', ['as' => 'auth.password.reset', 'uses' => 'ResetPasswordController@reset']);


    Route::post('/password/new', ['as' => 'auth.password.new', 'uses' => 'PasswordController@newPassword']);
    //Route::any('/password/test', ['as' => 'auth.password.new', 'uses' => 'Auth\PasswordController@test']);

    Route::get('account-verification', 'AuthController@verifyAccount');
});
