<?php

namespace App\Modules\Auth\Traits;

use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustFacade as Entrust;

trait AfterMigration
{


    function run()
    {
        $this->run001();
    }

    private function run001()
    {
        $results = \App\Modules\Base\Models\AccountGroup::find(1000);
        if (!$results) {
            try {
                \App\Modules\Base\Models\AccountGroup::create(['id' => '1000', 'name' => 'Payable - Tax', 'code' => '2231', 'parent_id' => '27', 'is_system_account' => '1']);
                \App\Modules\Base\Models\Ledger::create(['group_id' => '1000', 'name' => 'VAT Payable', 'code' => '1211.01', 'opening_balance_type' => 'D', 'is_system_account' => '1']);
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
        }
    }

}
