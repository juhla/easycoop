<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-12
 * Time: 14:14
 */

namespace App\Modules\Auth\Traits;


use App\Modules\Auth\Models\User;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Models\UserLicense;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait Register
{
    use AfterMigration;
    private $company;
    private $user;
    private $input;
    private $errorMessage;
    private $personalInfo;


    function register($input)
    {
        $rules = [
            'first_name' => 'required',
            'phone_no' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'role_id' => 'required',
            'terms' => 'required',
            //   'g-recaptcha-response' => 'required',
        ];
        //generate verification code
        $input['confirmation_code'] = generate_verification_code();
        //set password
        $input['password'] = bcrypt($input['password']);
        //set registration type for emails
        $input['type'] = 'new-registration';
        $role = Role::find($input['role_id']);


        switch ($role->name) {
            case 'business-owner':
                //set additional validation rules for msme
                $rules['company_name'] = 'required|unique:companies,company_name';
                //$rules['rc_number'] = 'required';
                //validate user input
                $validator = Validator::make(request()->all(), $rules);


                //if validation fails, redirect with errors
                if ($validator->fails()) {
                    $failedRules = $validator->errors();
                    return response()->json([
                        'type' => 'validator_fails',
                        'message' => $failedRules,
                    ]);
                }
                //make slug
                $input['slug'] = makeSlug($input['company_name']);

                //if captcha code is correct, proceed
                //                if($input['g-recaptcha-response']){
                try {
                    //save user info
                    $this->user = $user = User::create($input);

                    //assign user to role
                    $user->attachRole($role->id);
                    //pr('d');exit;
                    //find default user licence for business owner
                    $licence = UserLicense::defaultLicenceByRole($role->id);
                    //set user licence
                    $subscribe = new UserLicenceSubscriber();
                    $subscribe->user_id = $user->id;
                    $subscribe->count = 1;
                    $subscribe->license_id = $licence->id;
                    $subscribe->save();

                    $input['user_id'] = $user->id;

                    //save personal info
                    $this->personalInfo = PersonalInfo::create($input);
                    //save company info
                    $this->company = Company::create($input);
                    //setup msme database
                    $this->input = $input;
                    return $this->createCompany();
                    //send welcome mail to user
                } catch (\Exception $e) {
                    // pr($e->getMessage());exit;

                    app('sentry')->captureException($e);


                }
                return response()->json([
                    'type' => 'success'
                ]);
                break;

            case 'social-member':
                //validate user input
                $validator = Validator::make(request()->all(), $rules);
                //if validation fails, redirect with errors
                if ($validator->fails()) {
                    $failedRules = $validator->errors();
                    return response()->json([
                        'type' => 'validator_fails',
                        'message' => $failedRules,
                    ]);
                }
                //generate verification code
                $input['confirmation_code'] = generate_verification_code();
                //if captcha code is correct, proceed
                if ($input['g-recaptcha-response']) {
                    //save user info
                    $user = User::create($input);
                    //assign user to role
                    $user->attachRole($role->id);
                    //save personal info
                    $input['user_id'] = $user->id;
                    $this->personalInfo = PersonalInfo::create($input);
                    //send welcome mail to user
                    try {
                        $this->sendWelcomeEmail($input);
                    } catch (\Exception $e) {
                        app('sentry')->captureException($e);
                        return response()->json([
                            'type' => 'email_fails',
                            'message' => $e->getMessage(),
                        ]);
                    }
                    if ($user) {
                        return response()->json([
                            'type' => 'success'
                        ]);
                    } else {
                        return response()->json([
                            'type' => 'fail',
                            'message' => "Unable to sign you up. Please try again or contact support@ikooba.com",
                        ]);
                    }
                } else {
                    //if captcha code is wrong, returerror message

                    return response()->json([
                        'type' => 'fail',
                        'message' => "Invalid captcha Code",
                    ]);
                }
                break;
            default:
                //set additional validation rules for msme
                $rules['company_name'] = 'required';
                //$rules['rc_number'] = 'required';
                //validate user input
                $validator = Validator::make(request()->all(), $rules);
                //if validation fails, redirect with errors
                if ($validator->fails()) {
                    $failedRules = $validator->errors();
                    return response()->json([
                        'type' => 'validator_fails',
                        'message' => $failedRules,
                    ]);
                }

                //if captcha code is correct, proceed
                //                if($input['g-recaptcha-response']){
                //save user info
                $user = User::create($input);
                //assign user to role
                $user->attachRole($role->id);

                $input['user_id'] = $user->id;
                //find default user licence for business owner
                $licence = UserLicense::defaultLicenceByRole($role->id);
                //                dd( $licence);
                //set user licence
                $subscribe = new UserLicenceSubscriber();
                $subscribe->user_id = $user->id;
                $subscribe->count = 1;
                $subscribe->license_id = empty($licence->id) ? 0 : $licence->id;
                $subscribe->save();
                //save personal info
                $this->personalInfo = PersonalInfo::create($input);
                //save company info
                Company::create($input);
                //send welcome mail to user
                try {
                    $this->sendWelcomeEmail($input);
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                    return response()->json([
                        'type' => 'email_fails',
                        'message' => $e->getMessage(),
                    ]);
                }
                if ($user) {
                    return response()->json([
                        'type' => 'success'
                    ]);
                } else {
                    return response()->json([
                        'type' => 'fail',
                        'message' => "Unable to sign you up. Please try again or contact support@ikooba.com",
                    ]);
                }
        }
    }

    public function createCompany()
    {
        try {
            switch (config("app.env")) {
                case "production":
                    {
                        $cpanelusr = config("constants.live_username");
                        $cpanelpass = config("constants.live_password");
                    }
                    break;
                case "test":
                case "local":
                    {
                        $cpanelusr = config("constants.test_username");
                        $cpanelpass = config("constants.test_password");
                    }
                    break;
            }


            $databasename = $this->company->slug;
            $dbName = $cpanelusr . '_' . $databasename;
            DB::statement('CREATE DATABASE ' . $dbName);
            $this->company->database = $dbName;
            $this->company->save();
            $this->setConfig($dbName);


            Artisan::call('module:optimize');
            Artisan::call('module:migrate', [
                'slug' => 'auth',
                '--database' => 'tenant_conn',
                '--force' => true
            ]);
            Artisan::call('module:seed', [
                    'slug' => 'auth',
                    '--database' => 'tenant_conn',
                    '--force' => true
                ]
            );

            $this->run();


            try {
                $this->sendWelcomeEmail($this->input);
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
                return response()->json([
                    'type' => 'email_fails',
                    'message' => $e->getMessage(),
                ]);
            }

            $this->errorMessage = true;
            return response()->json([
                'type' => 'success'
            ]);
        } catch (\Exception $exception) {
            $this->errorMessage = false;
            try {
                $this->company->delete();
                $this->personalInfo->delete();
                UserLicenceSubscriber::where('user_id', $this->user->id)->delete();
                $this->user->delete();

            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
            app('sentry')->captureException($exception);
            return response()->json([
                'type' => 'fail',
                'message' => "Unable to sign you up. Please try again or contact support@ikooba.com",
            ]);

        }
    }


}