<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Http\Controllers\AssemblylineController;
use App\Modules\Base\Models\Assemblyline;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\User;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Base\Traits\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;

class CheckAssemblyLicense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:CheckAssemblyLicense';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Charge user if license has expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = Assemblyline::where('status',1 )->get();

        foreach ($user as $a)
        {
            $today = date("Y-m-d H:i:s");
            if (($today == $a->expiry_date)){
                $checkAssemblyLine = Assemblyline::where('payer_id', $a->payer_id)->first();
                $user_details = User::where('id',$a->payer_id)->first();
                $amount = $checkAssemblyLine->amount;
                $authorization_code = $checkAssemblyLine->authorization_code;
                $license_type = $checkAssemblyLine->licence_type;
                $email = $user_details->email;
                $token = config('services.paystack.secret');


                $result = array();
// Pass the customer's authorisation code, email and amount
                $postdata =  array( 'authorization_code' => $authorization_code,'email' => $email, 'amount' => $amount);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.paystack.co/transaction/charge_authorization"

                );
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($postdata));  //Post Fields
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $headers = [
                    'Authorization: Bearer '. $token,
                    'Content-Type: application/json',
                ];


                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $request = curl_exec ($ch);
                if(curl_error($ch)){
                    echo 'error:' . curl_error($ch);
                }

                curl_close ($ch);
                if ($request) {
                    $result = json_decode($request, true);

                    $today = date("Y-m-d H:i:s");
                    $tod=date_create("$today");
                    $date=date_add($tod,date_interval_create_from_date_string("30 days"));
                    $dates= date_format($date,"Y-m-d H:i:s");

                    DB::table('assemblylines')
                        ->where('payer_id', $a->payer_id)
                        ->update(['expiry_date' => $dates]);

                }

            }



        }





    }

    }


