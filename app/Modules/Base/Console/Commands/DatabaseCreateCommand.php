<?php

namespace App\Modules\Base\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use PDO;
use PDOException;
use App\xmlapi;

class DatabaseCreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a new database';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'db:create {database}';

    /**
     * Execute the console command.
     */
    public function fire()
    {
        $database = $this->argument('database');

        if (!$database) {
            $this->info('Skipping creation of database as database is empty');
            return;
        }

        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $host = config('database.connections.mysql.host');

        try {
            $this->info(config("app.env"));
            if (in_array(config("app.env"), ['production', 'test'])) {

                $cpanelusr = 'ikooba';
                $cpanelpass = '@@BMAC2017';
                $xmlapi2 = new xmlapi('127.0.0.1');
                $xmlapi2->set_port(2083);
                $xmlapi2->password_auth($cpanelusr, $cpanelpass);
                $xmlapi2->set_debug(1); //output actions in the error log 1 for true and 0 false

                $dbName = $cpanelusr . "_" . $database;
                print $xmlapi2->api2_query($cpanelusr, "Mysql", "adddb", array($database)); //creates the database
                echo "<hr>";
                print $xmlapi2->api2_query($cpanelusr, "Mysql", "adduserdb", array("" . $dbName . "", "" . $username . "", 'all')); //gives all privileges to the newly created user on the new db
                echo "<hr>";


            } else {

                $pdo = $this->getPDOConnection($host, config('database.connections.mysql.port'), $username, $password);

                $pdo->exec(sprintf(
                    'CREATE DATABASE IF NOT EXISTS %s',
                    $database,
                    config('database.connections.mysql.charset'),
                    config('database.connections.mysql.collation')
                ));

                $dbName = $database . ".*";
                $user = "'" . $username . "' @ '" . $host . "'";
                $pwd = "'" . $password . "'";


                $one = $pdo->exec(sprintf(
                    "GRANT ALL PRIVILEGES ON %s TO %s IDENTIFIED BY %s WITH GRANT OPTION", $dbName, $user, $pwd

                ));
                $this->info($one);

                $two = $pdo->exec(sprintf(
                    "GRANT ALL PRIVILEGES ON %s TO %s IDENTIFIED BY %s", $dbName, $user, $pwd

                ));
                $this->info($two);

                $three = $pdo->exec(sprintf(
                    "FLUSH PRIVILEGES;"

                ));
                $this->info($three);


            }


            $this->info(sprintf('Successfully created %s database', $database));


            $link = @mysqli_connect($host, $username, $password);
            if (!$link) {
                die('failed to connect to the server: ' . mysqli_connect_error());
            }

            if (!@mysqli_select_db($link, $database)) {
                die('failed to connect to the database: ' . mysqli_error($link));
            }

            $this->info(sprintf('Successfully connected %s database', $database));
        } catch (PDOException $exception) {
            app('sentry')->captureException($exception);
            $this->error(sprintf('Failed to create %s database, %s', $database, $exception->getMessage()));
        }


    }

    /**
     * @param  string $host
     * @param  integer $port
     * @param  string $username
     * @param  string $password
     * @return PDO
     */
    private function getPDOConnection($host, $port, $username, $password)
    {
        return new PDO(sprintf('mysql:host=%s;port=%d;', $host, $port), $username, $password);
    }
}
