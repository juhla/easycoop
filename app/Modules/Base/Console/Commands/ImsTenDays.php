<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Models\User;
use App\Modules\PriceBundle\Models\ImsLicenceSubscriber;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ImsTenDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ImsExpiryNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This send email notification whithin ten days of expiration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ims_users = ImsLicenceSubscriber::all();

        foreach ($ims_users as $ims_user) {



            $user_id = $ims_user->user_id;

            $ims_expiry_date = $ims_user->expiry_date;


            //get user email
            $user_e = User::where('id', $user_id)->first();
            //dd($user_e);
            if(!empty($user_e)){
                $email = $user_e->email;
                $name = @$user_e->displayName();
            }
            $today = Carbon::today();
            $theDay = $today->toDateString();

            $date1 = date_create($ims_expiry_date);
            $date2 = date_create($theDay);
            $diff = date_diff($date2, $date1);
            $ims_expiry_date = $diff->format("%R%a");

            $value = abs($ims_expiry_date);
            $inMessage = " is expiring in $value day(s) time";
            if ($ims_expiry_date < 1) {
                $inMessage = "has expired";
            }


              



            if ($ims_expiry_date == 30) {

               Mail::to($email)->send(new ImsThirtyDaysEmailNotification($user_e)); 
         

            } elseif ($ims_expiry_date == 20) {

               Mail::to($email)->send(new ImsTwentyDaysEmailNotification($user_e));

            } elseif ($ims_expiry_date == 10) {

              Mail::to($email)->send(new ImsTenDaysEmailNotification($user_e));

            }









    }




}
