<?php

namespace App\Modules\Base\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Base\Traits\Mailer;
use App\Modules\Base\Models\User;
use Carbon\Carbon;
use Mail;

class IsAccountVerified extends Command
{
    use Mailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:IsAccountVerified';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder if account has not been verified';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result_set = User::whereConfirmed(0)->get();

        foreach ($result_set as $value) {

            if ($value->hasRole(['business-owner', 'professional'])) {

                //compute carbon date difference
                $differenceInDays = $this->daysDifference($value->created_at);
                $message = "";
                $buttonText = "";
                $subject = "";

                switch (true) {
                    case $differenceInDays == 1:
                        $message = $this->getMessage($value, $differenceInDays);
                        $buttonText = $this->getButtonText($value, $differenceInDays);
                        $subject = 'Use less than one minute to verify your BMAC Account';
                        break;

                    case $differenceInDays == 3:
                        $message = $this->getMessage($value, $differenceInDays);
                        $buttonText = $this->getButtonText($value, $differenceInDays);
                        $subject = 'Verify your BMAC Account under one minute';
                        break;

                    case $differenceInDays == 7:
                        $message = $this->getMessage($value, $differenceInDays);
                        $buttonText = $this->getButtonText($value, $differenceInDays);
                        $subject = 'You can still verify your BMAC Account within a minute';
                        break;

                    default:
                        $message = "do-nothing";
                        break;
                }

                //process date and append message
                $input['subject'] = $subject;
                $input['body'] = $message;
                $input['buttonText'] = $buttonText;
                $input['first_name'] = "";
                $input['last_name'] = "";
                if ($value->personal_info) {
                    $input['first_name'] = $value->personal_info->first_name;
                    $input['last_name'] = $value->personal_info->last_name;
                }

                $input['email'] = $value->email;
                $input['type'] = 'new-registration';
                $input['confirmation_code'] = $value->confirmation_code;

                // $input['email']= 'ope.mesonrale@ssacltd.com';
                // dd($input);
                if ($message != 'do-nothing') {
                    try {
                        $this->sendEmailFrmUserManager("emails.usermanager.is_account_verified", $input);
                    } catch (\Exception $e) {
                        app('sentry')->captureException($e);
                        return response()->json([
                            'type' => 'email_fails',
                            'message' => $e->getMessage(),
                        ]);
                    }
                }

                // exit;

            }

        }

    }

    private function daysDifference($givenDate)
    {
        $creationDate = Carbon::parse($givenDate);
        $now = Carbon::now();
        $length = $creationDate->diffInDays($now);

        if ($length == 1) {
            $hoursDiff = $this->hours24Difference($givenDate);
            if ($hoursDiff == 24) {
                return $length;
            } else {
                return 0;
            }
        }

        if ($length == 3) {
            $hoursDiff = $this->hours24Difference($givenDate);
            if ($hoursDiff == 72) {
                return $length;
            } else {
                return 0;
            }
        }

        if ($length == 7) {
            $hoursDiff = $this->hours24Difference($givenDate);
            if ($hoursDiff == 168) {
                return $length;
            } else {
                return 0;
            }
        }

        return $length;
    }

    private function hours24Difference($givenDate)
    {
        $creationDate = Carbon::parse($givenDate);
        $now = Carbon::now();
        $hours = $creationDate->diffInHours($now);
        return $hours;
    }

    private function getMessage($user, $days)
    {
        $message[1] = '
            <center>
                <div mc:edit="body" style="width:100%;">
                <h4 style="color: #097a47; "">Did you forget to verify your account?</h4>
            </div>
            </centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Hi ' . ucfirst($user->personal_info->first_name) . ',<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            We noticed you showed interest in using BMAC. However, your account verification process is not complete. <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            You’re almost there! <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Please verify your account, by clicking on the link below, to get access to simple, collaborative cloud accounting. <br><br>
            </div>';


        $message[3] = '
           <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">Thank you for choosing BMAC</h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi ' . ucfirst($user->personal_info->first_name) . ',<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            We noticed you were trying to sign up for <a style="font-weight: 100; text-decoration: none;" href="https://ikooba.com/bmac/secure/sign-in"><mark>free cloud-accounting</mark></a>, but have not completed the process. We would love to help you get started. <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            To complete your registration, click here. <br><br>
            </div>';


        $message[7] = " 
            <center>
                <div mc:edit='body' style='width:100%;'>
                  <h4 style='color: #097a47; '>You are yet to verify your account!</h4>
               </div>
           </centre><br>

            <div mc:edit='body' style='text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;'>
            Hi " . ucfirst($user->personal_info->first_name) . ",<br><br>
            </div>

           <div mc:edit='body' style='text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;'>
            We noticed you haven’t verified your account.  <br><br>With this simple and free cloud-accounting solution, you can gain important insights about how your business is doing. <br><br>No installation required. Just sign up, verify your account and start making smarter business decisions.   <br><br>
            </div>";


        return $message[$days];

    }

    private function getButtonText($user, $days)
    {
        $messageButton[1] = "Complete your registration";
        $messageButton[3] = "Activate now";
        $messageButton[7] = "Complete your registration";

        return $messageButton[$days];
    }
}
