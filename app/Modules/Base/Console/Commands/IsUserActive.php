<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\User;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Base\Traits\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;


class IsUserActive extends Command
{
    use Mailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:IsUserActive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send thank you message to active users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    protected $emailSubject='';

    public function handle()
    {
        $allInactiveUsers = $this->getAllInactiveUsers();
        // dd($allInactiveUsers);
        foreach ($allInactiveUsers as $category) {
            // pr($category);
            foreach ($category as $user) {
                $message = $this->getMessage($user, $user['inactive_days']);
                $input['subject'] = $this->emailSubject;
                $input['first_name'] = $user['first_name'];
                $input['last_name'] = $user['last_name'];
                $input['body'] = $message;
                $input['email'] = $user['email'];


                // $input['email'] = 'ope.mesonrale@ssacltd.com';
                // dd($input);

               try {
                   $this->sendEmailFrmUserManager('emails.usermanager.is_user_active', $input);
               } catch (\Exception $e) {
                   app('sentry')->captureException($e);
                   return response()->json([
                       'type' => 'email_fails',
                       'message' => $e->getMessage(),
                   ]);
               }
                //exit;

            }

        }
    }

    /**
     * @return array
     */
    public function getAllInactiveUsers()
    {
        $dummy_users = [];
        $usersDB = Company::select("database", "user_id")->get()->toArray();
        // $usersDB = Company::whereUser_id(314)->get();
       
        //7,14
        $firstDayUsers = [];
        $seventhDayUsers = [];
        $fourteenthDayUsers = [];

        foreach ($usersDB as $db) {
            // echo $db['database'] = 'ikooba_jonnx';
            if ($this->checkForDbAvailability($db['database'])) {

                $username = Config::get('database.connections.mysql.username');
                $password = Config::get('database.connections.mysql.password');
                $host = Config::get('database.connections.mysql.host');
                $database = $db['database'];

                try {
                    if ($this->connectToDatabase($host, $username, $password, $database)) {
                        $noOfDays = $this->getDaysNotPosted();

                        if ($noOfDays == 0) {
                            $this->emailSubject = 'Yes! You did it, you made your first transactions';
                            $firstDayUsers[] = $this->getUser($db['user_id'], 0);
                        }
                        if ($noOfDays == 7) {
                            $this->emailSubject = 'You are 1-week old using BMAC';
                            $seventhDayUsers[] = $this->getUser($db['user_id'], 7);
                        }
                        if ($noOfDays == 14) {
                             $this->emailSubject = 'With BMAC you can do so much more';
                            $fourteenthDayUsers[] = $this->getUser($db['user_id'], 14);
                        }
                    }
                } catch (\Exception $ex) {
                    app('sentry')->captureException($ex);
                }
            } break;
        }

        $allInactiveUsers[] = $firstDayUsers;
        $allInactiveUsers[] = $seventhDayUsers;
        $allInactiveUsers[] = $fourteenthDayUsers;
        return $allInactiveUsers;
    }


    public function checkForDbAvailability($db_name)
    {
        if ($db_name == null || $db_name == '' || empty($db_name)) {
            return false;
        } else {
            return true;
        }
    }

    public function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);
        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function getDaysNotPosted()
    {
        $userLastPostDate = $this->getTransactionLatestDate();
        return $this->daysDifference($userLastPostDate);
    }

    private function getTransactionLatestDate()
    {
        $latestPostingDates = [];
        $latestPostingDates[] = $this->ChequeReceiptExists();
        $latestPostingDates[] = $this->PettyCashExists();
        $latestPostingDates[] = $this->CreditSalesExists();
        $latestPostingDates[] = $this->CreditPurchaseExists();
        $latestPostingDates[] = $this->ChequePaymentExists();
        $latestPostingDates[] = $this->CashReceiptExist();
        $latestPostingDates[] = $this->transactionExist();

        $latestDate = $this->getLatestDate($latestPostingDates);
        //pr($latestDate);
        return $latestDate;
    }


    public function ChequeReceiptExists()
    {
        try {
            $data = ChequeReceipt::orderBy('date', 'desc')->first();

            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }


    public function PettyCashExists()
    {
        try {
            $data = PettyCash::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function CreditSalesExists()
    {
        try {
            $data = CreditSales::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }


    public function CreditPurchaseExists()
    {

        try {
            $data = CreditPurchase::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function ChequePaymentExists()
    {

        try {
            $data = ChequePayment::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }

    public function CashReceiptExist()
    {
        try {
            $data = CashReceipt::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function transactionExist()
    {

        try {
            $data = Transaction::orderBy('transaction_date', 'desc')->first();
            // dd($data);
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->transaction_date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function getLatestDate($dates)
    {
        return max($dates);
    }

    public function getUser($user_id, $days)
    {
        $user = PersonalInfo::whereUser_id($user_id)->first();
        $data['user_id'] = $user_id;
        $data['first_name'] = $user['first_name'];
        $data['middle_name'] = $user['middle_name'];
        $data['last_name'] = $user['last_name'];
        $data['phone_no'] = $user['phone_no'];
        $data['inactive_days'] = $days;
        $data['email'] = $this->getUserEmail($user_id);

        return $data;
    }

    public function getUserEmail($user_id)
    {
        $user = User::find($user_id);
        return $user['email'];
    }

    private function daysDifference($givenDate)
    {
        $creationDate = Carbon::parse($givenDate);
        $now = Carbon::now();
        $length = $creationDate->diffInDays($now);
        return $length;
    }


    private function getMessage($user, $days)
    {
        $message[0] = '

              <center>
                <div mc:edit="body" style="width:100%;">
                  <h4 style="color: #097a47; ">You did it! You posted your first transaction!</h4>
                  <img src="https://ikooba.com/email_files/images/firsttransaction.png" style="width: 100px;">
                 </div>
               </centre><br>

             <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Hi '.ucfirst($user['first_name']).',<br><br>
             </div>


            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Looks like you’ve just posted your first transaction! The first of many more profitable postings...<br>
            Remember with the BMAC;
            <ul>
              <li>your bookkeeping is a lot simpler.</li>
              <li>you can tell how your business is doing from anywhere in the world at any time.</li>
              <li>you can collaborate with professional accountants</li>
              <li>finding finance is no longer a pain because your records are instantly available</li>
            </ul>
            Keep posting your business records in the BMAC and there is no limit to what your business can become.  <br><br>
            </div>

           <center><div mc:edit="body" style="width:100%;">
              <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://onebmac.com/secure/sign-in">Click here to log in </a></h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Download BMAC App to access your account on the go <br><br>
            </div>
            <!-- Stores icons -->
            <center><div mc:edit="body" style="width:100%;">
              <img src="https://onebmac.com/images/appstore.jpg" style="padding-top:-3px; padding-left:10px;">
              <img src="https://onebmac.com/images/playstore.jpg" style="padding-top:-3px; padding-left:10px;">
           </div></centre><br>';

        $message[7] = '
    
            <center>
                <div mc:edit="body" style="width:100%;">
                  <h4 style="color: #097a47; ">You’re 1-week old using BMAC</h4>
                  <img src="https://onebmac.com/images/feeding-bottle-1_.png" style="width: 160px;">
               </div>
            </centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>

           <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            It’s a week since you signed up to the BMAC. We hope you are gaining new and useful business insights from its various modules. <br><br>
            </div>


            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            For the latest news, views, useful tips and business advice from business professionals, subscribe to our blog <a style="font-weight: 900; text-decoration: underline; color: #097a47;" href="http://ikooba.com/blog/">www.ikooba.com/blog</a> <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            An organised business is a successful business. Keep using the BMAC to keep your business  organised… <br><br><br>
            </div>';  

        $message[14] = ' 

           <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">Explore the BMAC, increase business productivity…</h4>
              <img src="https://onebmac.com/images/increase-business-productivity.jpg" style="width: 80%;">
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>


            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Your account looks great! We are glad your record is up-to-date.<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Boost your business productivity with the BMAC by documenting your invoices & receipts, having your customers & suppliers’ listed in the cloud. <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Leverage on the BMAC features tailored to help you measure your business performance and profitability.<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Accounting doesn’t have to be that boring!<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             <a style="font-weight: 900; text-decoration: underline; color: #097a47;" href="https://ikooba.com/bmac/secure/sign-in">Explore the BMAC today</a>; we’d love to walk you through.<br><br>
            </div>';

        return $message[$days];
    }
}
