<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\User;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Base\Traits\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class IsUserInActive extends Command
{
    use Mailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:IsUserInActive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send follow up message to inactive users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */

    protected $emailSubject='';

    public function handle()
    {
        $allInactiveUsers = $this->getAllInactiveUsers();
        // dd($allInactiveUsers);
        foreach ($allInactiveUsers as $category) {
            foreach ($category as $user) {

                $message = $this->getMessage($user, $user['inactive_days']);
                $input['subject'] = $this->emailSubject;
                $input['first_name'] = $user['first_name'];
                $input['last_name'] = $user['last_name'];
                $input['body'] = $message;
                $input['email'] = $user['email'];


                // $input['email'] = 'ope.mesonrale@ssacltd.com';
                // dd($input);

                try {
                    $this->sendEmailFrmUserManager('emails.usermanager.is_user_inactive', $input);
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                    return response()->json([
                        'type' => 'email_fails',
                        'message' => $e->getMessage(),
                    ]);
                }
                // exit;

            }

        }
    }
    
    public function getAllInactiveUsers()
    {
        $dummy_users = [];
        $usersDB = Company::select("database", "user_id")->get()->toArray();
         // $usersDB = Company::whereUser_id(314)->get();
        //7,14,21, 28, 35
        $seventhDayUsers = [];
        $fourteenthDayUsers = [];
        $twentyfirstDayUsers = [];
        $twentyeightDayUsers = [];
        $thirtyfifithDayUsers = [];
        // dd($usersDB);
        foreach ($usersDB as $db) {
            if ($this->checkForDbAvailability($db['database'])) {

                $username = Config::get('database.connections.mysql.username');
                $password = Config::get('database.connections.mysql.password');
                $host = Config::get('database.connections.mysql.host');
                $database = $db['database'];

                try {
                    if ($this->connectToDatabase($host, $username, $password, $database)) {
                        echo '|'.$noOfDays = $this->getDaysNotPosted();
                        // $noOfDays = 35;
                        if ($noOfDays == 7) {
                            $seventhDayUsers[] = $this->getUser($db['user_id'], 7);
                            $this->emailSubject = 'How well is your Business Doing Today';
                        }
                        if ($noOfDays == 14) {
                            $fourteenthDayUsers[] = $this->getUser($db['user_id'], 14);
                             $this->emailSubject = 'You have worked Hard, start working Smart';
                        }

                        if ($noOfDays == 21) {
                            $twentyfirstDayUsers[] = $this->getUser($db['user_id'], 21);
                            $this->emailSubject = 'BMAC to the rescue';
                        }

                        if ($noOfDays == 28) {
                            $twentyeightDayUsers[] = $this->getUser($db['user_id'], 28);
                            $this->emailSubject = 'Cultivating Better Business Habits';
                        }

                        if ($noOfDays == 35) {
                            $thirtyfifithDayUsers[] = $this->getUser($db['user_id'], 35);
                            $this->emailSubject = 'Hello! We have something for you Today';
                        }
                    }
                } catch (\Exception $ex) {
                    app('sentry')->captureException($ex);
                }
            }
        }

        $allInactiveUsers[] = $seventhDayUsers;
        $allInactiveUsers[] = $fourteenthDayUsers;
        $allInactiveUsers[] = $twentyfirstDayUsers;
        $allInactiveUsers[] = $twentyeightDayUsers;
        $allInactiveUsers[] = $thirtyfifithDayUsers;
        return $allInactiveUsers;
    }


    public function checkForDbAvailability($db_name)
    {
        if ($db_name == null || $db_name == '' || empty($db_name)) {
            return false;
        } else {
            return true;
        }
    }

    public function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);
        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function getDaysNotPosted()
    {
        $userLastPostDate = $this->getTransactionLatestDate();
        return $this->daysDifference($userLastPostDate);
    }

    private function getTransactionLatestDate()
    {
        $latestPostingDates = [];
        $latestPostingDates[] = $this->ChequeReceiptExists();
        $latestPostingDates[] = $this->PettyCashExists();
        $latestPostingDates[] = $this->CreditSalesExists();
        $latestPostingDates[] = $this->CreditPurchaseExists();
        $latestPostingDates[] = $this->ChequePaymentExists();
        $latestPostingDates[] = $this->CashReceiptExist();
        $latestPostingDates[] = $this->transactionExist();

        $latestDate = $this->getLatestDate($latestPostingDates);
        //pr($latestDate);
        return $latestDate;
    }


    public function ChequeReceiptExists()
    {
        try {
            $data = ChequeReceipt::orderBy('date', 'desc')->first();

            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }


    public function PettyCashExists()
    {
        try {
            $data = PettyCash::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function CreditSalesExists()
    {
        try {
            $data = CreditSales::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }


    public function CreditPurchaseExists()
    {

        try {
            $data = CreditPurchase::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function ChequePaymentExists()
    {

        try {
            $data = ChequePayment::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }

    public function CashReceiptExist()
    {


        try {
            $data = CashReceipt::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function transactionExist()
    {

        try {
            $data = Transaction::orderBy('transaction_date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->transaction_date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function getLatestDate($dates)
    {
        return max($dates);
    }

    public function getUser($user_id, $days)
    {
        $user = PersonalInfo::whereUser_id($user_id)->get()->first();
        $data['user_id'] = $user_id;
        $data['first_name'] = $user['first_name'];
        $data['middle_name'] = $user['middle_name'];
        $data['last_name'] = $user['last_name'];
        $data['phone_no'] = $user['phone_no'];
        $data['inactive_days'] = $days;
        $data['email'] = $this->getUserEmail($user_id);
        return $data;
    }

    public function getUserEmail($user_id)
    {
        $user = User::find($user_id);
        return $user['email'];
    }

    private function daysDifference($givenDate)
    {
        $creationDate = Carbon::parse($givenDate);
        $now = Carbon::now();
        $length = $creationDate->diffInDays($now);
        return $length;
    }


    private function getMessage($user,$days)
    {
        $message[7] = ' 
            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

            <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
            <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
           </div><br>

              <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">HOW WELL IS YOUR BUSINESS DOING?</h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>

              <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Keeping daily records of your transactions makes life a lot easier for you and your business in the long run.<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            The BMAC is a simple, friendly and easy to use business tool which gives powerful insights into your business performance.<br><br>
            </div>


           <center><div mc:edit="body" style="width:100%;">
              <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">Start keeping your books today </a></h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            We noticed you have not used your account in a while. Is there anything we can do to help?<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             Do share with us if you have any troubles using the BMAC as we would love to help. 
             Call us on 0907 842 2233 or email us at business@ikooba.com<br><br>
            </div>';

        $message[14] = ' 
            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

            <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
            <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
           </div><br>

              <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">You work hard. Now start working smart…</h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Run your business from your mobile device anywhere, anytime. You can also see how your business is doing at a glance and collaborate with accountants as well as potential investors. 
            <br><br>
            </div>

           <center><div mc:edit="body" style="width:100%;">
              <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">Log in now </a></h4>
           </div></centre><br>

           <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Download BMAC app today to monitor your sales and purchases, cash inflows and outflows. <br><br>
            </div>
            <!-- Stores icons -->
            <center><div mc:edit="body" style="width:100%;">
              <img src="https://onebmac.com/images/appstore.png" style="padding-top:-3px; padding-left:10px;">
              <img src="https://onebmac.com/images/playstore.png" style="padding-top:-3px; padding-left:10px;">
           </div></centre><br>
           <!-- Stores icons -->

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             If you need anything all us on 0907 842 2233 or email us at business@ikooba.com<br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             We look forward to hearing from you.<br><br>
            </div>';

            $message[21] = ' 
                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

                <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
                <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
               </div><br>

                  <center><div mc:edit="body" style="width:100%;">
                  <h4 style="color: #097a47; ">BMAC to the rescue</h4>
               </div></centre><br>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Hi '.ucfirst($user['first_name']).',<br><br>
                </div>

                 <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                    It looks like you’ve been busy lately; why not consider our assembly line service?
                    We can help you prepare your financial statements at little or no cost. Remember keeping your daily transactions on BMAC will also help you to;
                    <ul>
                      <li>know how your business is doing at a glance,</li>
                      <li>manage your expenses,</li>
                      <li>make more informed business decisions,</li>
                      <li>easily collaborate with accounting professionals and potential investors.</li>
                    </ul>
                    </div>

                    <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                    Manage your business from any location on any device. BMAC is totally free for you and one of your business representatives.<br><br>
                    </div>

                   <center><div mc:edit="body" style="width:100%;">
                      <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">Log in now </a></h4>
                   </div></centre><br>

                    <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                     If you’re not sure about something, get in touch. Call us on 0907 842 2233 or email us at business@ikooba.com<br>
                     We look forward to hearing from you.<br><br>
                    </div>';

            $message[28] = '
                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

                <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
                <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
               </div><br>

                  <center><div mc:edit="body" style="width:100%;">
                  <h4 style="color: #097a47; ">Better Business Habits</h4>
               </div></centre><br>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Hi '.ucfirst($user['first_name']).',<br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Meet Mary. <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Mary started trading in fabrics and other similar products 3 years ago. She makes lots of money, but she feels she does not have much to show for her hard work. 
                <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Mary does not know she has the following bad business habits…Mary;
                <ul>
                  <li>does not keep records of her transactions. She feels it’s too much work</li>
                  <li>does not separate her personal expenses from business expenses. “It’s my money” she says</li>
                  <li>feels she does not need an accountant or someone to keep her business records. She says they’re too expensive.</li>
                  <li>As a result, she thinks having lots of money means her business is profitable</li>
                </ul>
                <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                Try BMAC today. It is easy to use, useful in managing your business and very affordable.
                <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                <a style="font-weight: 900; text-decoration: underline; color: #097a47;" href="https://ikooba.com/bmac/secure/sign-in">Log on now and begin managing your business better. </a>
                <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                As always, we will guide you towards Business, Measurement, Assurance and Collaboration. 
                Subscribe to our blog.
                <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                 If you need anything all us on 0907 842 2233  or email us at business@ikooba.com <br><br>
                </div>

                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                 We look forward to hearing from you.<br><br>
                </div>';

        $message[35] = ' 
            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

            <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
            <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
           </div><br>

              <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">Hello! Hello!</h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>

              <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Looks like you are really busy and unable to reply my emails. I have a better idea. Permit me to tell you about our Free Business Support Clinic where we offer a free business consultation session for business owners and managers. 
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Can we talk about it, a short call will do. Are you available this Friday?
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            You can possibly tell us when you will available. 
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            <a style="font-weight: 900; text-decoration: underline; color: #097a47;" href="https://ikooba.com/bmac/secure/sign-in">Login into your Account....</a>
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             If you need anything all us on 0907 842 2233  or email us at business@ikooba.com  <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             We look forward to hearing from you.<br><br>
            </div>';

        return $message[$days];
    }
}
