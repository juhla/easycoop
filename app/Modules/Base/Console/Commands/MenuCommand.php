<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Jobs\MenuJob;
use App\Modules\Base\Models\Company;

class MenuCommand extends UpgradeCommand
{
    protected $signature = 'upgrade:menu {--all=*} {--database=*}';
    protected $description = 'Setup new menu data structure';

    public function job(Company $customer)
    {
        return new MenuJob($customer);
    }
}