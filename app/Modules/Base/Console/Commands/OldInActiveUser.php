<?php

namespace App\Modules\Base\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\User;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Base\Traits\Mailer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class OldInActiveUser extends Command
{
    use Mailer;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:OldInActiveUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a one time message to old inactive users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     protected $emailSubject='';

    public function handle()
    {
        $allInactiveUsers = $this->getAllInactiveUsers();
        // dd($allInactiveUsers);
        foreach ($allInactiveUsers as $category) {
            foreach ($category as $user) {

                $message = $this->getMessage($user);
                $input['subject'] = $this->emailSubject;
                $input['first_name'] = $user['first_name'];
                $input['last_name'] = $user['last_name'];
                // $input['days'] = $user['inactive_days'];
                $input['body'] = $message;
                $input['email'] = $user['email'];


                // $input['email'] = 'ope.mesonrale@ikooba.com';
                // dd($input);

                try {
                    $this->sendEmailFrmUserManager('emails.usermanager.is_user_inactive', $input);
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                    return response()->json([
                        'type' => 'email_fails',
                        'message' => $e->getMessage(),
                    ]);
                }
                
            }

        }

    }
    
    public function getAllInactiveUsers()
    {
        $dummy_users = [];
        $usersDB = Company::select("database", "user_id")->get()->toArray();
         // $usersDB = Company::whereUser_id(314)->get();
        //20, 30, 45, 60, 75, 90
        $veryOldUsers = [];
        // dd($usersDB);
        foreach ($usersDB as $db) {
            if ($this->checkForDbAvailability($db['database'])) {

                $username = Config::get('database.connections.mysql.username');
                $password = Config::get('database.connections.mysql.password');
                $host = Config::get('database.connections.mysql.host');
                $database = $db['database'];

                try {
                    if ($this->connectToDatabase($host, $username, $password, $database)) {
                        echo '|'.$noOfDays = $this->getDaysNotPosted();
                       
                        if ($noOfDays > 35) {
                            $veryOldUsers[] = $this->getUser($db['user_id'], $noOfDays);
                            $this->emailSubject = 'Hello! We have something for you Today';
                        }
                    }
                } catch (\Exception $ex) {
                    app('sentry')->captureException($ex);
                }
            }
        }

        $allInactiveUsers[] = $veryOldUsers;
        return $allInactiveUsers;
    }


    public function checkForDbAvailability($db_name)
    {
        if ($db_name == null || $db_name == '' || empty($db_name)) {
            return false;
        } else {
            return true;
        }
    }

    public function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);
        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function getDaysNotPosted()
    {
        $userLastPostDate = $this->getTransactionLatestDate();
        return $this->daysDifference($userLastPostDate);
    }

    private function getTransactionLatestDate()
    {
        $latestPostingDates = [];
        $latestPostingDates[] = $this->ChequeReceiptExists();
        $latestPostingDates[] = $this->PettyCashExists();
        $latestPostingDates[] = $this->CreditSalesExists();
        $latestPostingDates[] = $this->CreditPurchaseExists();
        $latestPostingDates[] = $this->ChequePaymentExists();
        $latestPostingDates[] = $this->CashReceiptExist();
        $latestPostingDates[] = $this->transactionExist();

        $latestDate = $this->getLatestDate($latestPostingDates);
        //pr($latestDate);
        return $latestDate;
    }


    public function ChequeReceiptExists()
    {
        try {
            $data = ChequeReceipt::orderBy('date', 'desc')->first();

            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }


    public function PettyCashExists()
    {
        try {
            $data = PettyCash::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function CreditSalesExists()
    {
        try {
            $data = CreditSales::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }


    public function CreditPurchaseExists()
    {

        try {
            $data = CreditPurchase::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function ChequePaymentExists()
    {

        try {
            $data = ChequePayment::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }
    }

    public function CashReceiptExist()
    {


        try {
            $data = CashReceipt::orderBy('date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function transactionExist()
    {

        try {
            $data = Transaction::orderBy('transaction_date', 'desc')->first();
            if ($data == null) {
                return "1970-00-00";
            } else {
                return $data->transaction_date;
            }
        } catch (\Exception $ex) {
            return "1970-00-00";
        }

    }

    public function getLatestDate($dates)
    {
        return max($dates);
    }

    public function getUser($user_id, $days)
    {
        $user = PersonalInfo::whereUser_id($user_id)->get()->first();
        $data['user_id'] = $user_id;
        $data['first_name'] = $user['first_name'];
        $data['middle_name'] = $user['middle_name'];
        $data['last_name'] = $user['last_name'];
        $data['phone_no'] = $user['phone_no'];
        $data['inactive_days'] = $days;
        $data['email'] = $this->getUserEmail($user_id);
        return $data;
    }

    public function getUserEmail($user_id)
    {
        $user = User::find($user_id);
        return $user['email'];
    }

    private function daysDifference($givenDate)
    {
        $creationDate = Carbon::parse($givenDate);
        $now = Carbon::now();
        $length = $creationDate->diffInDays($now);
        return $length;
    }


    private function getMessage($user)
    {
        $message = ' 
            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

            <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
            <img src="https://onebmac.com/images/ikooba.png" style="padding-top:2px; padding-left:10px; width:33%;">
           </div><br>

              <center><div mc:edit="body" style="width:100%;">
              <h4 style="color: #097a47; ">Hello! Hello!</h4>
           </div></centre><br>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Hi '.ucfirst($user['first_name']).',<br><br>
            </div>

              <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Looks like you are really busy and unable to reply my emails. I have a better idea. Permit me to tell you about our Free Business Support Clinic where we offer a free business consultation session for business owners and managers. 
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            Can we talk about it, a short call will do. Are you available this Friday?
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            You can possibly tell us when you will available. 
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
            <a style="font-weight: 900; text-decoration: underline; color: #097a47;" href="https://ikooba.com/bmac/secure/sign-in">Login into your Account....</a>
            <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             If you need anything all us on 0907 842 2233  or email us at business@ikooba.com  <br><br>
            </div>

            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
             We look forward to hearing from you.<br><br>
            </div>';

        return $message;
    }
}
