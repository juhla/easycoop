<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class PatchDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
        protected $signature = 'run:patchDB';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migration on all database to perform patches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected $counter=0;

    public function handle()
    {
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');
        $host = Config::get('database.connections.mysql.host');

        $result = Company::get();
        foreach ($result as $company) {
            try {
                $this->counter++;
                if ($company->database != "") {
                    $database = $company->database;
//                     $database = 'ikooba_goodspeed_nigeria_ltd';
                    if ($this->connectToDatabase($host, $username, $password, $database)) {
                        $this->runMigrations();
                    } else {
                        echo 'db not found connection failed | ';
                    }

                }
//                 exit;

            } catch (\Exception $e) {
                echo 'crashed | ';
                app('sentry')->captureException($e);
            }
        }
    }


    public function connectToDatabase($host, $username, $password, $database)
    {
//        Artisan::call('config:cache');
        Artisan::call('cache:clear');
        Artisan::call('clear-compiled');
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);

        try {
            \DB::reconnect('tenant_conn');
            // echo  "got connected||";
            return true;

        } catch (\Exception $e) {
            // echo  "connection failed||";
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function runMigrations()
    {
        try {
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-bmac',
                '--force' => true
            ]);

            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-ims',
                '--force' => true
            ]);

            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-payroll',
                '--force' => true
            ]);
            echo "success | ";

        } catch (\Exception $e) {
            echo "migration crashed | ";
          //  app('sentry')->captureException($e);
        }
    }
}
