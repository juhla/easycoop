<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Company;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\MySql;
use Illuminate\Support\Facades\DB;
class RunDbBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sql:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run DB Backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function isDBExist($dbName)
    {
        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
        $db = DB::select($query, [$dbName]);
        return (!empty($db));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $companies = Company::all();
        $accounts = [];
        foreach ($companies as $each) {
            if (!empty($each->database)) {
                if ($this->isDBExist($each->database)) {
                    $accounts[] = $each->database;
                    $mysql = Config::get('database.connections.mysql');
                    Config::set("database.connections." . $each->database, $mysql);
                    Config::set("database.connections." . $each->database . ".database", $each->database);
                }

            }
        }
        Config::set("backup.backup.source.databases", $accounts);
        Artisan::call('backup:run', ['--only-db' => true]);


    }
}
