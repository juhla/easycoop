<?php

namespace App\Modules\Base\Console\Commands;

use Auth;
use App\Modules\Base\Models\Assemblyline;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Models\User;
use Mail;

class SendAssemblyLineCharge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:AssemblyLineCharge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' Send Email Notification for Assemblyline Charge. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $today = Carbon::today();
        $theDay = $today->toDateString();
        $checkAssemblyLine = Assemblyline::with(['business'])
            ->get();

        foreach ($checkAssemblyLine as $eachCheckAssemblyLine) {
            $assDate = $eachCheckAssemblyLine->expiry_date;
            $amount = $eachCheckAssemblyLine->amount;
            $email = $eachCheckAssemblyLine->business->email;
            $user = $eachCheckAssemblyLine->business->displayName();


            $date1 = date_create($assDate);
            $date2 = date_create($theDay);
            $diff = date_diff($date2, $date1);
            $assDate = $diff->format("%R%a");


            $header = "
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
                    <html>
                    <head>
                    <style rel=\"stylesheet\" >
                    * { margin: 0; padding: 0; font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; }
                    img { max-width: 100%; margin: 0 auto; display: block; }
                    body, .body-wrap { width: 100% !important; height: 100%; background: #f8f8f8; }
                    a { color: #71bc37; text-decoration: none; }
                    a:hover { text-decoration: underline; }
                    .text-center { text-align: center; }
                    .text-right { text-align: right; }
                    .text-left { text-align: left; }
                    .button { display: inline-block; color: white; background: #0d7d40; border: solid #0d7d40; border-width: 10px 20px 8px; font-weight: bold; border-radius: 4px; }
                    .button:hover { text-decoration: none; }
                    h1, h2, h3, h4, h5, h6 { margin-bottom: 20px; line-height: 1.25; }
                    h1 { font-size: 32px; }
                    h2 { font-size: 28px; }
                    h3 { font-size: 24px; }
                    h4 { font-size: 20px; }
                    h5 { font-size: 16px; }
                    p, ul, ol { font-size: 16px; font-weight: normal; margin-bottom: 20px; }
                    .container { display: block !important; clear: both !important; margin: 0 auto !important; max-width: 580px !important; }
                    .container table { width: 100% !important; border-collapse: collapse; }
                    .container .masthead { padding: 35px 0; background: #0d7d40; color: white; }
                    .container .masthead h1 { margin: 0 auto !important; max-width: 90%; text-transform: uppercase; }
                    .container .content { background: white; padding: 30px 35px; }
                    .container .content.footer { background: none; }
                    .container .content.footer p { margin-bottom: 0; color: #888; text-align: center; font-size: 14px; }
                    .container .content.footer a { color: #888; text-decoration: none; font-weight: bold; }
                    .container .content.footer a:hover { text-decoration: underline; }
                    </style>
                    </head>
                    <body style=\"width: 100% !important; height: 100%; background: #f8f8f8; margin: 0; padding: 0; font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65;\">
                    <table class=\"body-wrap\" style=\"width: 100% !important; height: 100%; background: #f8f8f8;\">
                        <tr>
                            <td class=\"container\" style=\"display: block !important; clear: both !important; margin: 0 auto !important; max-width: 580px !important;\">
                                <table style=\"width: 100% !important; border-collapse: collapse;\">
                                    <tr>
                                        <td align=\"center\" class=\"masthead\" style=\"padding: 35px 0; background: #0d7d40; color: white;\">
                                            <h1 style=\"margin: 0 auto !important; max-width: 90%; font-size: 32px; text-transform: uppercase;\">BMAC TEAM..</h1>
                                        </td>
                                    </tr>
                        ";
            $body = "<tr>
                        <td class=\"content\" style=\"background: white; padding: 30px 35px;\">
                            <h2 style=\"font-size: 28px;\">Dear Esteemed Customer,</h2>
                            <p style=\"font-size:13px; text-align:justify;font-weight: normal; margin-bottom: 20px;\">
                                Dear $user , we want to inform you that your card will be charged with a sum of $amount Naira .
                                To continue enjoying this Amazing Service offered by <a href='https://ikooba.com'>ikOOba Technologies</a>, Please Renew 
                                your Licence. 
                            </p>
                            
                            <table style=\"width: 100% !important; height: 100%; background: #f8f8f8;\">
                                <tr>
                                    <td align=\"center\">
                                        <p style=\"font-size: 16px; font-weight: normal; margin-bottom: 20px; \">
                                            <a href=\"https://ikooba.com/\" class=\"button\">See More Of Our Services</a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            <p style=\"font-size: 16px; font-weight: normal; margin-bottom: 20px; \"><em>– Kind Regards</em></p>
                            <em>ikOOba Technologies</em>
                        </td>
                       </tr>
                    </table>
                </td>
            </tr>";

            $footer = "
                <tr>
                    <td class=\"container\">
                        <!-- Message start -->
                        <table>
                            <tr>
                                <td class=\"content footer\" align=\"center\">
                                    <p>Sent by <a href=\"#\">ikOOba Technologies</a>,Plot 46 Ishawu Adewale Street, Off Modupe Johnson, Surulere, Lagos</p>
                                    <p>Phone | <a href=\"#\">08115828444 / 09078422233</a></p>
                                    <p><a href=\"mailto:info@ikooba.com\"></a>Email | <a href=\"#\">info@ikooba.com</a></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </body>
            </html>
            ";

            if ($assDate == 7) {
                $messageTxt = $header . $body . $footer;
                Mail::raw($messageTxt, function ($message) use ($email, $messageTxt) {
                    $message->subject('Account Charge For Assembly Line');
                    $message->from(config('mail.from.address'), config('mail.from.name'));
                    $message->to($email);
                    //$message->to('rylxes@gmail.com');
                    $message->setBody($messageTxt, 'text/html');
                });
                echo "mail sent";

            }
        }


    }
}
