<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Classes\MySqlDump;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use PDO;
use PDOException;

class SqlDump extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sql:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates mysql';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'sql:dump';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $publicPath = storage_path("app/backups/");
        $db = new MySqlDump();
        $db->dbhost = config('database.connections.mysql.host');
        //$db->tar_binary = '/usr/bin/tar';
        $db->mysqldump_binary = '/Applications/MAMP/Library/bin/mysqldump';
        $db->dbuser = config('database.connections.mysql.username');
        $db->dbpwd = config('database.connections.mysql.password');
        //$db->backupsToKeep = 130;
        $db->showDebug = true;
        $db->timezone = 'Africa/Lagos';
        $db->backupDir = '/Users/mac/Documents/mysql/';
        //$db->backupDir = $publicPath;
        $db->ignoreDatabases = ['test', 'ikooba_test*', 'bmac*'];
        $db->emptyTables = [];
        $db->dumpDatabases();
    }


}
