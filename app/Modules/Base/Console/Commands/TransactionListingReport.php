<?php

namespace App\Modules\Base\Console\Commands;

use Barryvdh\DomPDF\PDF;
use App\Modules\Base\Models\AccountGroup;
use App\Providers\AppServiceProvider;
use App\Modules\Base\Traits\TenantConfig;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class TransactionListingReport extends Command
{
    use TenantConfig;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:transactionList {data} {db}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends Transaction List Report to client email ';

    protected $transactionListingEmail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\App\Modules\Base\Mail\TransactionListingReport $transactionListingEmail)
    {
        parent::__construct();
        $this->transactionListingEmail = $transactionListingEmail;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        ini_set('memory_limit', '5828M');
        ini_set('max_execution_time', 10000);

        $data = $this->argument('data');
        $db = $this->argument('db');
        app()->makeWith('setDbConnection', ['db' => $db]);
        $name = GenerateRandomString(12, 'ALPHANUM');
        $fullname = storage_path('app' . DIRECTORY_SEPARATOR . 'transaction_list' . DIRECTORY_SEPARATOR);
        \PDF::loadView($data['view'], $data)
            ->save($fullname . $name . '.pdf')
           // ->setPaper('a4', 'landscape')
            ->stream('transaction-listing.pdf');
    }
}
