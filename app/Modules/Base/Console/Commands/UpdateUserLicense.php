<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;

use Illuminate\Console\Command;


class UpdateUserLicense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'license:updateUserLicense';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updated User License';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $__all = UserLicenceSubscriber::all();
        foreach ($__all as $license) {
            if (empty($license->count)) {
                $realCount = Collaborator::where('collaborators.user_id', $license->user_id)
                    ->whereIn('collaborators.status', [1, 2])
                    ->join('role_user', 'collaborators.client_id', '=', 'role_user.user_id')
                    ->whereIn('role_user.role_id', [4, 10, 11, 5, 6, 7, 8])
                    ->select('role_user.user_id')->count();
                $realCount = $realCount + 1;

                UserLicenceSubscriber::whereUser_id($license->user_id)->update(['count' => $realCount]);
            }
            if (($license->expiry_date == "0000-00-00") || empty($license->expiry_date)) {
                $user = User::find($license->user_id);
                if (!empty($user)) {
                    $created_at = $user->created_at->toDateString();
                    $effectiveDate = date('Y-m-d', strtotime("+1 years", strtotime($created_at)));
                    $license->expiry_date = $effectiveDate;
                    $license->save();
                }
            }
        }

    }

}
