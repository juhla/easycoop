<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Models\Company;
use Illuminate\Contracts\Bus\Dispatcher;
use InvalidArgumentException;
use Illuminate\Console\Command;

abstract class UpgradeCommand extends Command
{
    /**
     * Runs the command for a specific customer or for all customers.
     * For specific customer, the job is executed synchronously.
     * For all customers, the job is dispatched and must be picked up by a worker.
     *
     * @throws \InvalidArgumentException if no option is provided (customer or all)
     */
    public function handle()
    {
        if ($databaseName = $this->option('database')) {
            return $this->processCustomer($databaseName);
        }
        if ($all = $this->option('all')) {
            return $this->processEveryCustomer();
        }
        throw new InvalidArgumentException('You must provide a database name with --database or specify --all to run across the database.');
    }

    /**
     * Run the job in a single provided customer
     *
     * @param $databaseName
     */
    protected function processCustomer($databaseName)
    {
        $databaseName = collect($databaseName)->first();
        $customer = Company::where('database', $databaseName)->first();;
        if (is_null($customer)) {
            throw new InvalidArgumentException("{$databaseName} is not a valid database name.");
        }
        $class = $this->getClassWithoutNamespace();
        if ($this->confirm("Run {$class} for {$customer->name}?")) {
            dispatch($this->job($customer));
            $this->info('Done.');
        } else {
            $this->warn('Cancelled');
        }
    }

    /**
     * Load every customer from the database and dispatch a background job for it.
     */
    protected function processEveryCustomer()
    {
        $customers = Company::all();
        $this->info('Starting to dispatch jobs');
        foreach ($customers as $customer) {
            dispatch($this->job($customer));
            $this->info("Job dispatched for {$customer->name}");
        }
    }

    private function getClassWithoutNamespace()
    {
        return collect(explode('\\', get_class($this)))->pop();
    }

    /**
     * Return a new instance of the Job that should be dispatched.
     *
     * @param Company $customer
     * @return mixed
     */
    abstract public function job(Company $customer);
}