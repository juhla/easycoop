<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Mail\CheckupMail;
use App\Modules\Base\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class cronCheckupMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkupmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email after five days to checkup new users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {

        $dateRegistered = Carbon::today()->subDays(6);
        $dateExpired = Carbon::today()->subDays(5);
   

         $users = User::where('created_at','>',  $dateRegistered )
                        ->where('created_at', '<=', $dateExpired)
                        ->where('confirmed','=',1)
                        ->get();

        foreach ($users as $user) {
            Mail::to($user->email)->send(new CheckupMail($user));
            //$this->info('done');
        }


    }


}
