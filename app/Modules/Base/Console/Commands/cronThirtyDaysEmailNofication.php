<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Mail\thirtyDaysEmailNotification;
use App\Modules\Base\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class cronThirtyDaysEmailNofication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:emailThirtyDays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notification on the thirtieth day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateRegistered = Carbon::today()->subDays(31);
        $dateExpired = Carbon::today()->subDays(30);
   

         $users = User::where('created_at','>',  $dateRegistered )
                        ->where('created_at', '<=', $dateExpired)
                        ->where('confirmed','=',1)
                        ->get();

                        $this->info($users);
        // foreach ($users as $user) {
        //     Mail::to($user->email)->send(new thirtyDaysEmailNotification($user));
        //     $this->info($user->email);
        // }


    }
}
