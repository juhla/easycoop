<?php

namespace App\Modules\Base\Console\Commands;

use App\Modules\Base\Mail\twentyDaysEmailNotification;
use App\Modules\Base\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class cronTwentyDaysEmailNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:twentyDaysEmailNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email notification twenty days after signup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dateRegistered = Carbon::today()->subDays(21);
        $dateExpired = Carbon::today()->subDays(20);
   

         $users = User::where('created_at','>',  $dateRegistered )
                        ->where('created_at', '<=', $dateExpired)
                        ->where('confirmed','=',1)
                        ->get();

        foreach($users as $user)
        {
                 Mail::to($user->email)->send(new twentyDaysEmailNotification($user));
                 $this->info('$user->email');
        }

    }
}
