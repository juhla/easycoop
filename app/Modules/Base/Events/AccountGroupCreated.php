<?php

namespace App\Modules\Base\Events;


use App\Modules\Base\Models\AccountGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

class AccountGroupCreated
{
    public function __construct(AccountGroup $model)
    {
        $model->fixTree();
    }


}
