<?php

namespace App\Modules\Base\Events;


use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\TransactionItem;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

class TransactionItemCreated
{

    protected $salesCode = '4110';
    protected $salesReturnCode = '4120';
    protected $costOfSalesCode = '5103';
    protected $finishedGoodsCode = '1213';


    protected $salesID;
    protected $salesReturnID;
    protected $costOfSalesID;
    protected $finishedGoodsID;

    /**
     * @return mixed
     */
    public function getCostOfSalesID()
    {
        return getLedgerId($this->costOfSalesCode);
    }

    /**
     * @param mixed $costOfSalesID
     */
    public function setCostOfSalesID($costOfSalesID)
    {
        $this->costOfSalesID = $costOfSalesID;
    }

    /**
     * @return mixed
     */
    public function getFinishedGoodsID()
    {
        return getLedgerId($this->finishedGoodsCode);
    }

    /**
     * @param mixed $finishedGoodsID
     */
    public function setFinishedGoodsID($finishedGoodsID)
    {
        $this->finishedGoodsID = $finishedGoodsID;
    }


    /**
     * @return mixed
     */
    public function getSalesID()
    {
        return getLedgerId($this->salesCode);
    }

    /**
     * @param mixed $salesID
     */
    public function setSalesID($salesID)
    {
        $this->salesID = $salesID;
    }

    /**
     * @return mixed
     */
    public function getSalesReturnID()
    {
        return getLedgerId($this->salesReturnCode);
    }

    /**
     * @param mixed $salesReturnID
     */
    public function setSalesReturnID($salesReturnID)
    {
        $this->salesReturnID = $salesReturnID;
    }

    public function __construct(TransactionItem $model)
    {
        $salesID = $this->getSalesID();
        $salesReturnID = $this->getSalesReturnID();

        $transaction = $model->transactions();
        // Check if transaction is a sale
        if ($model->ledger_id == $salesID) {
            // Debit Cost of goods sold
            $trans_item1 = [
                'ledger_id' => $this->getCostOfSalesID(),
                'amount' => $model->amount,
                'item_description' => $transaction->description,
                'dc' => 'D'
            ];

            // Credit Finished Goods
            $trans_item2 = [
                'ledger_id' => $this->getFinishedGoodsID(),
                'amount' => $model->amount,
                'item_description' => $transaction->description,
                'dc' => 'C',
            ];


        }


        // Check if transaction is a sales return
        if ($model->ledger_id == $salesReturnID) {
            // Debit Cost of goods sold
            $trans_item1 = [
                'ledger_id' => $this->getCostOfSalesID(),
                'amount' => $model->amount,
                'item_description' => $transaction->description,
                'dc' => 'D'
            ];

            // Credit Finished Goods
            $trans_item2 = [
                'ledger_id' => $this->getFinishedGoodsID(),
                'amount' => $model->amount,
                'item_description' => $transaction->description,
                'dc' => 'C',
            ];


        }
    }


}
