<?php
/**
 * Created by PhpStorm.
 * User: baddy
 * Date: 16/01/2018
 * Time: 10:29
 */

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Tenant\Permission;
use App\Modules\Base\Models\Tenant\Role;
use App\Modules\Base\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Zizaco\Entrust\EntrustPermission;

class AccessControlController extends Controller
{

    public function __construct()
    {
        $this->middleware(['tenant', 'auth']);
    }

    public function index($id)
    {
        // Config::set('entrust.permissions_table','dfsdf');


        config()->set('entrust.permissions_table', session('company_db') . '.permissions');
        config()->set('entrust.roles_table', session('company_db') . '.roles');
        config()->set('entrust.permission_role_table', session('company_db') . '.permission_role');
        config()->set('entrust.role_user_table', session('company_db') . '.role_user');

        $name = User::with('personal_info')->find($id);
        $full_name = $name->email;
        if (isset($name->personal_info)) {
            $full_name = $name->last_name . " " . $name->first_name;
        }

        $permissions = EntrustPermission::where("module", "bmac")->get();
        $user_role_id = Role::where('name', $name->email)->pluck('id')->first();

        $assignedPermissions = DB::connection('tenant_conn')->table('permission_role')
            ->whereRaw("role_id = ?", [$user_role_id])
            ->pluck("permission_role.permission_id")->toArray();
        //  dd(in_array(37, $assignedPermissions));
        // dd($permissions[0]['category']);

        // $category = "";
        // $i = 0;
        $custom_permission = array();

        foreach ($permissions as $index => $permission) {

            if (!in_array($permission->category, $custom_permission)) {
                $custom_permission['default'] = array();
            }
            $custom_permission[$permission->category][] = $permission;
        }
        unset($custom_permission['default']);

        // dd($custom_permission);

        $data = [
            "assignedPermissions" => $assignedPermissions,
            "permissions" => $custom_permission,
            "full_name" => $full_name,
            "user_id" => $id,
        ];
        return view("acl.assign_permission_to_user", $data);
    }

    public function employeeList()
    {

        $employees = Collaborator::where('user_id', Auth::user()->id)
            ->with(['personalInfo'])
            ->where('flag', 'Active')->groupBy('user_id', 'client_id')->whereHas('client.roles', function ($q) {
                $q->whereIn('name', ["employee", 'employee-ims']);
            })->with('client')
            ->get();

        $data = [
            "employees" => $employees,
        ];
        //   dd($employees);
        return view("acl.employee_list", $data);
    }

    public function assignPermissionToEmployee()
    {

        $input = Input::all();
        $user = User::find($input['user_id']);

        if ($user->hasRole(['employee', 'employee-ims'])) {

            $checkRole = Role::where("name", $user->email)->first();

            if (!isset($checkRole)) {
                $checkRole = Role::create(["name" => $user->email]);
                $user->tenant_roles()->attach($checkRole->id);
            }
            $permissions = $input['permissions'];

            $ims = $checkRole->ims_perms->pluck('id')->toArray();
            $res = array_merge($ims, $permissions);
            $checkRole->bmac_perms()->sync($res);
            //$checkRole->attachPermissions($permissions);
        }

        return redirect()->back();
    }
}
