<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Audit;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\Country;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserDashboardWidget;
use App\Modules\Base\Models\UserWidget;
use App\Modules\Base\Models\Widget;
use App\Modules\Base\Models\WidgetDashboard;
use App\Modules\Base\Models\WidgetRole;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class AccountSettingsController extends Controller
{
    public function __construct()
    {

        $this->middleware(['tenant']);
    }

    public function index()
    {

    
        $data['title'] = 'Account Settings';

        $data['countryCurrencies'] = Country::where('currency_symbol', '!=', null)->get();
        $data['countryData'] = getBusinessOwnerAuth()->company->country_data;
        $userCategory = auth()->user()->roles;
        $userCategory = $userCategory->pluck('category')->all();


        $data['business_owner'] = Role::getBusinessOwnerUser();
        $data['has_user_widget'] = UserWidget::isUserWidget();
        $data['user_widget'] = Widget::getUserWidget(auth())->toArray();
        $data['category_widget'] = Widget::getWidgetByCategory($userCategory)->toArray();
        return view('settings.index')->with($data);
    }

    public function workspace_index()
    {
        $userCategory = auth()->user()->roles;
        $userCategory = $userCategory->pluck('category')->all();
        //$data['has_user_widget'] = UserWidget::isUserWidget();
        $data['business_owner'] = Role::getBusinessOwnerUser();
        $user_widget = Widget::getUserWidget(auth())->toArray();
        $categoryWidget = Widget::getWidgetByCategory($userCategory)->toArray();
        $theWidget = [];
        foreach ($categoryWidget as $key => $eachCategory) {
            $categoryWidget[$key]['checked'] = '';
            if (in_array($eachCategory, $user_widget)) {
                $categoryWidget[$key]['checked'] = 'checked';
            }
        }
        $data['category_widget'] = $categoryWidget;
        return response()->json($data);
        //return view('settings.workspace.index')->with($data);
    }

    public function change_widgets()
    {

        $input = request()->all();
        try {
            $widgetToDelete = new UserWidget();
            $widgetToDelete::where('user_id', auth()->user()->id)->delete();
            foreach ($input['widgets'] as $eachWidgets) {
                $userWidgetModel = new UserWidget();
                $userWidgetModel->user_id = auth()->user()->id;
                $userWidgetModel->widget_id = $eachWidgets;
                $userWidgetModel->save();
            }
            return response()->json(['message' => 'success']);
        } catch (\Exception $ex) {
            return response()->json(['message' => 'notvalid']);
        }
    }

    public function change_widget()
    {
        $input = request()->all();
        $userWidgetModel = new UserWidget();
        foreach ($input['widgets'] as $eachWidgets) {
            $userWidgetModel->user_id = auth()->user()->id;
            $userWidgetModel->widget_id = $eachWidgets;
            $userWidgetModel->save();
        }
    }

    public function change_proficiency()
    {
        $input = request()->all();
        $userID = auth()->user()->id;

        $widgetToDelete = new UserWidget();
        $widgetToDelete::where('user_id', $userID)->delete();

        $dashboardWidget = new UserDashboardWidget();
        $dashboardWidget::where('user_id', $userID)->delete();


        $userModel = User::find($userID);
        $roleModel = Role::find($input['role']);
        $userModel->detachRoles($userModel->roles);
        $userModel->attachRole($roleModel);
        return response()->json(['message' => 'success']);
    }

    public function changePassword()
    {

        $input = request()->all();
        $rules = [
            'current_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
        $validator = \Validator::make($input, $rules);

        if ($validator->fails()) {
            if (request()->ajax()) {
                return response()->json(['message' => 'validator']);
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }
        //validate password
        if (\Hash::check($input['current_password'], \Auth::user()->password)) {
            $user = auth()->user();
            $user->password = \Hash::make($input['password']);
            $user->save();
            return response()->json(['message' => 'success']);
        } else {
            return response()->json(['message' => 'notvalid']);
        }
    }

    public function editCurrency()
    {
        $user = auth()->user();
        $input = request()->all();
        $currency = $input['typeofcurrency'];
        Company::whereUser_id($user->id)->update(['currency' => $currency]);
        if (request()->ajax()) {
            return response()->json(['message' => 'success']);
        } else {
            flash()->success('Your currency has been update');
            return back();
        }
    }

    /**
     * Method to deactivate user account
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function deactivateAccount()
    {
        $user = auth()->user();

        $user->flag = 'Deactivated';
        $user->active = 0;
        $user->save();


        if (request()->ajax()) {
            return response()->json(['message' => 'success']);
        } else {
            flash()->success('Your account has been deactivated. Goodbye');
            return redirect()->to('secure/sign-out');
        }
    }

}