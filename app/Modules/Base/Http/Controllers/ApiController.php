<?php

namespace App\Modules\Base\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\Base\Http\Requests;
use App\Modules\Base\Http\Controllers\Controller;
use Response;
use App\Modules\Base\Models\Tax;
use App\Modules\Base\Models\AccountGroup;

class ApiController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware(['tenant']);
    }

    /**
     * method to get states
     * @param $country_id
     * @return mixed
     */
    public function _getStates($country_id)
    {
        $states = getStatesByCountry($country_id);
        return response()->json($states);
    }

    /**
     * api method to get GL accounts for use in select dropdown
     * @return mixed
     */
    public function getSelectAccounts()
    {
        $classes = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();
        $accounts = '';
        foreach ($classes as $class) {
            $accounts .= '<option value="">Please Select...</option><optgroup label="' . $class->name . '">';
            if ($groups = $class->groups) {
                foreach ($groups as $group) {
                    $accounts .= '<option disabled>&nbsp;' . $group->code . ' ' . $group->name . '</option>';
                    if ($nestedGroups = $group->nestedGroups($group->id)) {
                        foreach ($nestedGroups->orderBy('code', 'asc')->get() as $child) {
                            $accounts .= '<option disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $child->code . ' ' . $child->name . '</option>';
                            if ($subs = $child->nestedGroups($child->id)) {
                                foreach ($subs->orderBy('code', 'asc')->get() as $sub) {
                                    $accounts .= '<option disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $sub->code . ' ' . $sub->name . '</option>';
                                    if ($sub->ledgers->count()) {
                                        foreach ($sub->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                                            $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                                        }
                                    }
                                }
                            }
                            if ($child->ledgers->count()) {
                                foreach ($child->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                                    $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                                }
                            }
                        }
                    }
                    if ($group->ledgers->count()) {
                        foreach ($group->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                            $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                        }
                    }
                }
            }
            if ($class->ledgers->count()) {
                foreach ($class->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                    $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                }
            }
            $accounts .= '</optgroup>';
        }
        return response()->json($accounts);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Lists all taxes belonging to the specified company
     * Developed by Emmanuel Benson
     * 2nd September, 2015. 10:31 AM.
     */
    public function getAllTax()
    {
        $all_taxes = Tax::all();

        if (request()->ajax()) {
            if (count($all_taxes) > 0) {
                return response()->json([
                    'data' => $all_taxes,
                    'status' => 200
                ]);
            }

            return response()->json([
                'message' => 'No taxes found!',
                'status' => 404
            ]);
        }

        return response($all_taxes);
    }


}
