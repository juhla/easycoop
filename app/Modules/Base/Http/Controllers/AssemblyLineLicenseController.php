<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Assemblyline;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Models\UserLicense;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;
class AssemblyLineLicenseController extends Controller
{

    use License;

    public function __construct()
    {

    }

    public function checkSub (){
        //get user role
        $userLicense = new UserLicense();
        $userLicenceSubscriber = new UserLicenceSubscriber();
        UserLicenceSubscriber::updateTheCount(getBusinessOwnerID());
        $role = getUserRole(auth()->user()->id);
        $hasSubscribed = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();
         $hasAddedAssemblyLine = Assemblyline::where('payer_id', auth()->user()->id)->first();

        $today = Carbon::now();


        if (empty($hasAddedAssemblyLine)) {
//           $theLicense = $userLicense->findDefaultLicenceByRole($role->id);
//           $userLicenceSubscriber->user_id = auth()->user()->id;
//           $userLicenceSubscriber->license_id = $theLicense->id;
//           $license_id = $theLicense->id;
//           $hasAddedAssemblyLine->save();
            $expiry_date = 0;
            $license_id = $hasSubscribed->license_id;
            //return $today. "licence = " . $license_id . "has added = " . $hasAddedAssemblyLine . "date = " . $expiry_date;
            //return view('settings.assemblyline-licenses', compact( 'plan', 'subscriber'));
        } else {
            $license_id = $hasSubscribed->license_id;
            $expiry_date = $hasAddedAssemblyLine->expiry_date;
        }
        $plan = $userLicense->find($license_id);

        $subscriber = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();
        $title = 'Subscribe For You';
        //get user role
        $role = getUserRole(auth()->user()->id);
        //get plans for role
        $roleid = Role::findRoleCategory($role->id);
        $assembly_basic_license_plan = UserLicense::where('role_id', $roleid)->where('title', 'BMAC Basic Ass. Line')->get();
        $assembly_silver_license_plan = UserLicense::where('role_id', $roleid)->where('title', 'BMAC Silver Ass. Line')->get();


        $subscriber = UserLicenceSubscriber::where('user_id', auth()->user()->id)->first();


        $license_id = empty($subscriber->license_id) ? 0 : $subscriber->license_id;
        return view('settings.assemblylineplan', compact( 'subscriber', 'assembly_basic_license_plan', 'assembly_silver_license_plan', 'license_id', 'title'));
    }

    public function plans(){
        $this->createTables();
        $title = 'Change Plan';
        //get user role
        $role = getUserRole(auth()->user()->id);
        //get plans for role
        $roleid = Role::findRoleCategory($role->id);
        $bmac_license_plans = UserLicense::where('role_id', $roleid)->where('type_id', 4)->get();


        $subscriber = UserLicenceSubscriber::where('user_id', auth()->user()->id)->first();

        $license_id = empty($subscriber->license_id) ? 0 : $subscriber->license_id;


        return view('settings.plans', compact( 'subscriber', 'bmac_license_plans',  'license_id', 'title'));
    }

    public function subscribe($plan_id)
    {
        $plan = UserLicense::find($plan_id);
        $today = date("Y-m-d H:i:s");
        $tod=date_create("$today");
        $date=date_add($tod,date_interval_create_from_date_string("30 days"));
        $dates= date_format($date,"Y-m-d H:i:s");

        $license = Assemblyline::firstOrNew(array(
            'payer_id' => Auth::user()->id,
        ));
        $license->payer_id = Auth::user()->id;
        $license->amount = $plan->price;
        $license->status = 1;
        $license->license_type = $plan->title;
        $license->expiry_date = $dates;
        $license->authorization_code = request()->get('authorization_code');
        //$license->license_id = $plan_id;
        //$license->expiry_date = $this->getExpiryDate($plan_id, $license->expiry_date);
        $license->save();


        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }

    public function cancelAssemblyLine(){
       $del=  DB::table('collaborators')
            ->where('user_id', auth()->user()->id)
            ->delete();
         if ($del){
            $dolo = DB::table('assemblylines')->where('payer_id',  auth()->user()->id)->delete();
             if($dolo){
                 flash()->success('Assembly license Subscription has been cancelled');
                 return redirect('collaborators/list');
             }
         }






    }

    private function sendMails($plan)
    {
        try {
            $data['name'] = auth()->user()->company->company_name;
            $data['reference_no'] = request()->get('reference_no');
            $data['package'] = $plan->title;
            $data['amount'] = $plan->price;
            $data['email'] = auth()->user()->email;

            Mail::send('emails.receipt', $data, function ($m) use ($data) {
                $m->from('no-reply@onebmac.com', 'BMAC Team');

                $m->to($data['email'], $data['name'])->subject($data['name'] . ' thank you for your order');
            });

            flash()->success('Subscription is succesful, Fill in your Rc number and click submit to add assembly line');
            return redirect('assemblyline/add-new');
        } catch (\Exception $e) {
            flash()->error($e->getMessage());
            app('sentry')->captureException($e);
            return redirect('assemblyline/add-new');
        }


    }


}
