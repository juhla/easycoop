<?php

namespace App\Modules\Base\Http\Controllers;

use Auth;
use App\Modules\Base\Models\Assemblyline;
use App\Modules\Base\Models\Agents\AgentSme;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Traits\Functions;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Traits\Mailer;
use App\Modules\Base\Traits\TenantConfig;
use Carbon\Carbon;
use Request;
use Redirect;
use Session;
use App\Modules\Base\Models\Payment;

class AssemblylineController extends Controller
{

    public function __construct()
    {
        $this->middleware(['verify_business_owner']);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use Mailer;

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userID = getBusinessOwnerID();
        //dd($userID);

        $assemblyArrayEmail = config('constants.assemblyLineEmail');
        $assemblyLineEmail = $assemblyArrayEmail[0];
        $client = User::where('email', $assemblyLineEmail)->first();


        $checkCollaboration = Collaborator::
        where('user_id', $userID)->
        where('client_id', $client->id)->first();


        if (count($checkCollaboration) > 0) {
            flash()->error('Error! You have already added Assembly line');
            return redirect()->to('collaborators/list');
        } else {
            $hasAddedAssemblyLine = Assemblyline::where('payer_id', $userID)->first();
            if (count($hasAddedAssemblyLine) > 0) {
                $company = Company::where('id', auth()->user()->company->id)->first();
                $data['rc_number'] = $company->rc_number;
                $data['company_name'] = $company->company_name;
                //dd( $data['rc_number']);
                $data['title'] = 'Add a Collaborator';
                $data['roles'] = Role::where('name', 'employee')
                    ->orWhere('name', 'professional')
                    ->orWhere('name', 'finance-provider')
                    ->orWhere('name', 'trade-promoter')
                    ->orWhere('name', 'admin-payroll')
                    ->get();
                return view('assemblyline.add')->with($data);
            } else {
                flash()->error('Error! You need to pay before adding Assembly line');
                return redirect()->to('assemblyline/plans');
            }

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($input = request()->all()) {
            $rules = [

                'rc_number' => 'required',

            ];

            $validator = \Validator::make(request()->all(), $rules);
            //if validation fails, redirect with errors
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $assemblyArrayEmail = config('constants.assemblyLineEmail');
            $assemblyLineEmail = $assemblyArrayEmail[0];
            $client = User::where('email', $assemblyLineEmail)->first();
            $userID = getBusinessOwnerID();


            //return $client;
            //return $checkuser;
            //return $assemblyLineEmail;
            //Company::where('id', auth()->user()->company->id) ->update(['rc_number' => $input['rc_number']]);


            /*check if the incoming email from the form is same thing with the set email. If it's not return incorrect
              return incorrect email else continue to save
            */


            /*check if the user_id and assembline have already collaborated. if they have not collaborated, continue to save
                else return message that user have already collaborated with assemblyline
            */
            $checkCollaboration = Collaborator::where('user_id', $userID)->where('client_id', $client->id)->first();

            if (count($checkCollaboration) < 1) {

                $company = Company::where('user_id', auth()->user()->id)->first();
                $input['company_name'] = $company->company_name;
                //$email = '';
                // $assemblyLineEmail = $email;
                $input['email'] = $assemblyLineEmail;
                $client = User::where('email', $assemblyLineEmail)->first();
                $input['confirmation_code'] = generate_verification_code();
                $input['type'] = 'registered';

                $assemblyline = new Collaborator();
                $assemblyline->user_id = $userID;
                $assemblyline->client_id = $client->id;
                $assemblyline->company_id = auth()->user()->company->id;
                $assemblyline->permission = $input['permission'];
                $assemblyline->save();
                $input['record'] = $assemblyline->id;

                $this->sendMailAssembly('emails.invite', $input, $input['company_name'] . ' invited you to BMAC');

                flash()->success('Your collaboration request has been sent.');
                return redirect()->to('collaborators/list');
            } else {
                flash()->error('Error! You have already collaborated');
                return redirect()->to('collaborators/list');
            }


            //send invitation mail to user


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function morata()
    {
        $checkAssemblyLine = Assemblyline::where('payer_id', auth()->user()->id)->first();
        $amount = $checkAssemblyLine->amount;
        $authorization_code = $checkAssemblyLine->authorization_code;
        $license_type = $checkAssemblyLine->licence_type;
        $email = auth()->user()->email;
        $token = config('services.paystack.secret');


        $result = array();
// Pass the customer's authorisation code, email and amount
        $postdata = array('authorization_code' => $authorization_code, 'email' => $email, 'amount' => $amount);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.paystack.co/transaction/charge_authorization"

        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $headers = [
            'Authorization: Bearer ' . $token,
            'Content-Type: application/json',
        ];


        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);
        if (curl_error($ch)) {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
        if ($request) {
            $result = json_decode($request, true);

            $today = date("Y-m-d H:i:s");
            $tod = date_create("$today");
            $date = date_add($tod, date_interval_create_from_date_string("30 days"));
            $dates = date_format($date, "Y-m-d H:i:s");

            DB::table('assemblylines')
                ->where('payer_id', auth()->user()->id)
                ->update(['expiry_date' => $dates]);

        }


    }
}
