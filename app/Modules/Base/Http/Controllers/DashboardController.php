<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Listeners\UpdateLastLogin;
use App\Modules\Base\Models\Assemblyline;
use App\Modules\Base\Models\Audit;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\Vendor;
use App\Modules\Base\Models\Widget;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\COA;
use App\Modules\Base\Traits\Core\TrialBalance;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Traits\NewCreates;
use App\xmlapi;
use Carbon\Carbon;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Tax;
use App\Computations;
use App\DashboardCalculations;
use App\Modules\Base\Models\BankAC;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Http\Requests\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Models\UserLicenceSubscriber;
use Zizaco\Entrust\Entrust;
use App\Modules\Base\Events\BmacPatch;


class DashboardController extends Controller
{
    use NewCreates, TrialBalance, Balance, COA;

    public function __construct(Computations $compute, DashboardCalculations $dashboardCalculation)
    {
        $this->middleware(['tenant', 'auth']);
        $this->compute = $compute;
        $this->dcs = $dashboardCalculation;
    }



    public function workspace()
    {
        // dd(session()->get("ApOauth")['decoded']->cooperative_name);
        migrateDB();
        $data['title'] = 'My Work tools';
        $data['widgets'] = Widget::getUserWidget(auth());


        //$data['custom_widgets'] = CustomWidget::getCustomWidget(auth());
        return view('dashboard.workspace')->with($data);
    }


    public function bmacPremium()
    {
        $activePremium = array();
        $user = auth()->user();
        if ($user->hasRole('employee')) {
            $activePremium[] = 'BMAC';
        }
        if ($user->hasRole('employee-ims')) {
            $activePremium[] = 'Inventory Management';
        }
        if ($user->hasRole('employee-payroll')) {
            $activePremium[] = 'HRM';
        }

        $widgets = Widget::whereIn('name', $activePremium)->get();
        $data['title'] = 'BMAC Workspace';
        $data['widgets'] = $widgets;
        return view('dashboard.bmac_premium')->with($data);
    }


    public function index()
    {

        // pr(session('company_db'));exit;
        $data['title'] = 'Dashboard';
        //pr(auth()->user()->company);exit;
        //check if session has company_db in array
        if (!empty(session('company_db'))) {
            $fiscal_year = FiscalYear::getCurrentFiscalYear();
            $latestTransaction = Transaction::orderBy('transaction_date', 'desc')->select('transaction_date')->pluck('transaction_date')->first();

            try {
                $_date = \Carbon\Carbon::createFromFormat('Y-m-d', $latestTransaction);
                $currMonthStarts = $_date->startOfMonth()->toDateString();
                $currMonthEnds = $_date->endOfMonth()->toDateString();
                /*
                * WEIRD BUGS WITH PHP DATE... $_date->subMonth() GIVES ME A WRONG VALUE BUT
                * $_date->startOfMonth()->subMonth() WORKS FINE
                * Seems to be a PHP PROBLEM => date_sub( date_create( $latest ), date_interval_create_from_date_string( '1 month' ) )
                */
                $currentMonthYear = $_date->year;
                $_date = $_date->startOfMonth()->subMonth();
                $prevMonthEnds = $_date->endOfMonth()->toDateString();
                $prevMonthStarts = $_date->startOfMonth()->toDateString();
                $previousMonthYear = $_date->year;
            } catch (\Exception $e) {
                $currentMonthYear = null;
                $previousMonthYear = null;
                $currMonthStarts = null;
                $currMonthEnds = null;
                $prevMonthEnds = null;
                $prevMonthStarts = null;
            }

            /*
            *
            * Calculations for Financial Summary Section!!!
            * In calculating the Financial Summary as below, we use the last transaction month for the most recent fiscalYear.
            *
            * BALANCE SHEET ITEMS... CUMULATED
            * PROFIT/LOSS ITEMS... NOT CUMULATED
            *
            * For the depreciation for the currTotalAssets, we subtract the depreciation for the current period
            * as well as the depreciation for the previous periods which is y for the previous periods a YEAR
            * is provided for the calculations.
            */
            $currDepreciation = AccountGroup::calculateAccumulatedDepreciation($currMonthStarts, $currMonthEnds);
            $prevDepreciation = AccountGroup::calculateAccumulatedDepreciation(null, null, $previousMonthYear);

            /* BALANCE SHEET ITEMS */
            $data['currTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $currMonthEnds, $currentMonthYear, true);
            $data['prevTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $prevMonthEnds, $previousMonthYear, true);

            // MINUS IP and PPE DEPRECIATIONS
            $prevDepreciation = $prevDepreciation['ppeTotal'] + $prevDepreciation['ipTotal'];
            $currDepreciation = $currDepreciation['ppeTotal'] + $currDepreciation['ipTotal'];

            $data['currTotalAssets'] = $data['currTotalAssets'] - ($currDepreciation + $prevDepreciation);
            $data['prevTotalAssets'] = $data['prevTotalAssets'] - $prevDepreciation;

            $data['totalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $currMonthEnds, $currentMonthYear, true);
            $data['prevTotalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $prevMonthEnds, $previousMonthYear, true);

            $data['totalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $currMonthEnds, $currentMonthYear, true);
            $data['prevTotalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $prevMonthEnds, $previousMonthYear, true);


            // dd( [ $currMonthStarts, $currMonthEnds, $currentMonthYear ] );

            /* PROFIT/LOSS ITEMS */
            $data['totalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
            $data['prevTotalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);
            $data['totalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
            $data['prevTotalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);

            /*
            * Calculations for debtor, creditor, acidTest and currentRatio Section!!!
            */
            $data['currentDebtorPeriod'] = $this->compute->debtorCollectionPeriod();
            $data['currentCreditorPeriod'] = $this->compute->creditorPaymentPeriod();
            $data['currentRatio'] = $this->compute->currentRatio();
            $data['acidTestRatio'] = $this->compute->acidTestRatio();

            /*
            * Calculations for Bank Accounts and latest_transaction Section!!!
            */
            $bank_balances_tmp = $this->dcs->getBankBalances();
            $data['bank_accounts'] = $bank_balances_tmp['bank_accounts'];
            $data['total_account_balance'] = $bank_balances_tmp['total_account_balance'];
            $data['latest_transaction'] = TransactionItem::orderBy('created_at', 'desc')->select('dc', 'amount', 'created_at')->first();


            return view('dashboard.index')->with($data);
        } else {
            $data['title'] = 'Profile Page';
            //get personal info
            $data['personalInfo'] = PersonalInfo::where('user_id', auth()->user()->id)->first();
            //get company info
            $data['companyInfo'] = Company::where('user_id', auth()->user()->id)->first();
            //display a plan dashboard for financial providers, trade promoters and professionals
            return view('profile.influencers')->with($data);
        }
    }

    public function ratiosGraph()
    {
        $graphData = [
            'profitabilityRatio' => [
                'dataSet' => [],
                'series' => [
                    ['dataField' => 'NPM', 'displayText' => 'Net Profit Margin'],
                    ['dataField' => 'GPM', 'displayText' => 'Gross Profit Margin'],
                    ['dataField' => 'ROCE', 'displayText' => 'Return On Capital Employed']
                ]
            ],
            'stabilityRatio' => [
                'dataSet' => [],
                'series' => [
                    ['dataField' => 'LTD', 'displayText' => 'Long Term Debt'],
                    ['dataField' => 'TDER', 'displayText' => 'Total Debt Equity Ratio'],
                    ['dataField' => 'AC', 'displayText' => 'Asset Cover']
                ]
            ]
        ];

        $fiscalYearCompanyStarted = FiscalYear::min('begins'); // return the oldest begins date string value
        if (!empty(FiscalYear::getCurrentFiscalYear())) {
            $currentFiscalYearBegins = FiscalYear::getCurrentFiscalYear()->begins;
        }
        $lastDayOfPreviousFiscalYear = Carbon::createFromFormat('Y-m-d', $currentFiscalYearBegins)->subDay()->toDateString();
        $fiscalYear = date('Y', strtotime($currentFiscalYearBegins));

        $nonCurrentLiabilityGroups = AccountGroup::where('parent_id', 23)->select('id')->pluck('id')->all();
        $nonCurrentLiabilitiesBalanceToDate = $this->dcs->calculateLedgerBalanceForAccountGroups($nonCurrentLiabilityGroups, $fiscalYearCompanyStarted, $lastDayOfPreviousFiscalYear, null, false, false);

        $nonCurrentAssetGroups = AccountGroup::where('parent_id', 6)->select('id')->pluck('id')->all();
        $nonCurrentAssetsBalanceToDate = $this->dcs->calculateLedgerBalanceForAccountGroups($nonCurrentAssetGroups, $fiscalYearCompanyStarted, $lastDayOfPreviousFiscalYear, null, false, false);

        $capitalGroups = AccountGroup::where('parent_id', 3)->select('id')->pluck('id')->all();
        array_push($capitalGroups, 3);
        $capitalBalanceToDate = $this->dcs->calculateLedgerBalanceForAccountGroups($capitalGroups, $fiscalYearCompanyStarted, $lastDayOfPreviousFiscalYear, null, false, false);

        $quarters = [
            '1st Quarter' => ['starts' => $currentFiscalYearBegins, 'ends' => $fiscalYear . '-03-31'],
            '2nd Quarter' => ['starts' => $currentFiscalYearBegins, 'ends' => $fiscalYear . '-06-30'],
            '3rd Quarter' => ['starts' => $currentFiscalYearBegins, 'ends' => $fiscalYear . '-09-30'],
            '4th Quarter' => ['starts' => $currentFiscalYearBegins, 'ends' => $fiscalYear . '-12-31']
        ];
        foreach ($quarters as $title => $timelines) {
            $profitabilityRatio = $this->dcs->calculateProfitabilityRatio($timelines['starts'], $timelines['ends']);
            $profitabilityRatio['Quarter'] = $title;

            // how do we calculate value for bank overdraft
            $bankOverdraft = 0;

            $capital = $this->dcs->calculateLedgerBalanceForAccountGroups($capitalGroups, $timelines['starts'], $timelines['ends'], null, false, false);
            $nonCurrentAssets = $this->dcs->calculateLedgerBalanceForAccountGroups($nonCurrentAssetGroups, $timelines['starts'], $timelines['ends'], null, false, false);
            $nonCurrentLiabilities = $this->dcs->calculateLedgerBalanceForAccountGroups($nonCurrentLiabilityGroups, $timelines['starts'], $timelines['ends'], null, false, false);

            $capital += $capitalBalanceToDate;
            $nonCurrentAssets += $nonCurrentAssetsBalanceToDate;
            $nonCurrentLiabilities += $nonCurrentLiabilitiesBalanceToDate;

            try {
                // no reserves YET IN THE SYSTEM!!!
                // nonCurrentLiabilities/ ( capital + reserves )
                $ltd = $nonCurrentLiabilities / $capital;
            } catch (\Exception $e) {
                $ltd = 0;
            }

            try {
                // no reserves YET IN THE SYSTEM!!!
                $reserves = 0;
                $tder = ($nonCurrentLiabilities + $bankOverdraft) / ($capital + $reserves);
            } catch (\Exception $e) {
                $tder = 0;
            }

            try {
                $ac = $nonCurrentAssets / $nonCurrentLiabilities;
            } catch (\Exception $e) {
                $ac = 0;
            }


            array_push($graphData['profitabilityRatio']['dataSet'], $profitabilityRatio);

            array_push($graphData['stabilityRatio']['dataSet'], [
                'Quarter' => $title,
                'LTD' => $ltd,
                'TDER' => $tder,
                'AC' => $ac
            ]);
        }

        return response()->json($graphData);
    }


    public function getTotalBalance($class, $start_date = null)
    {
        $class = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', $class)->first();

        $total = AccountGroup::getAccountBalance($class, $start_date);

        return formatNumber($total);
    }

    /**
     * api method to get GL accounts for use in select dropdown
     * @return mixed
     */
    public function getSelectAccounts()
    {
        $classes = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();
        $accounts = '';
        foreach ($classes as $class) {
            $accounts .= '<optgroup label="' . $class->name . '">';
            if ($groups = $class->groups) {
                foreach ($groups as $group) {
                    $accounts .= '<option disabled>&nbsp;' . $group->code . ' ' . $group->name . '</option>';
                    if ($nestedGroups = $group->nestedGroups($group->id)) {
                        foreach ($nestedGroups->orderBy('code', 'asc')->get() as $child) {
                            $accounts .= '<option disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $child->code . ' ' . $child->name . '</option>';
                            if ($subs = $child->nestedGroups($child->id)) {
                                foreach ($subs->orderBy('code', 'asc')->get() as $sub) {
                                    $accounts .= '<option disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $sub->code . ' ' . $sub->name . '</option>';
                                    if ($sub->ledgers->count()) {
                                        foreach ($sub->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                                            $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                                        }
                                    }
                                }
                            }
                            if ($child->ledgers->count()) {
                                foreach ($child->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                                    $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                                }
                            }
                        }
                    }
                    if ($group->ledgers->count()) {
                        foreach ($group->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                            $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                        }
                    }
                }
            }
            if ($class->ledgers->count()) {
                foreach ($class->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                    $accounts .= '<option value="' . $ledger->id . '">&nbsp;&nbsp;&nbsp;' . $ledger->code . '  ' . $ledger->name . '</option>';
                }
            }
            $accounts .= '</optgroup>';
        }
        return response()->json($accounts);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Lists all taxes belonging to the specified company
     * Developed by Emmanuel Benson
     * 2nd September, 2015. 10:31 AM.
     */
    public function getAllTax()
    {
        $all_taxes = Tax::all();

        if (request()->ajax()) {
            if ($all_taxes->count() > 0) {
                return response()->json([
                    'data' => $all_taxes,
                    'status' => 200
                ]);
            }

            return response()->json([
                'message' => 'No taxes found!',
                'status' => 404
            ]);
        }

        return response($all_taxes);
    }


    public function logoutCompany()
    {
        try {
            session()->forget('company_name');
            session()->forget('company_db');
            return redirect()->to('profile/' . strtolower(auth()->user()->displayName()));
            //return redirect()->intended('/workspace');
        } catch (\Exception $exception) {
            return redirect()->back();
        }

    }


    //licence expiry date 
    public function licence_expiry($id)
    {

        $user = ImsLicenceSubscriber::find($id);
        return view('workspace')->with('user', $user);
    }


}