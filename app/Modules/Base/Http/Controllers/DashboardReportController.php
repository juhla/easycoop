<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Classes\PurchaseOrder\OrderItem;
use App\Modules\Base\Classes\Sales\SalesItem;
use App\Computations;
use App\DashboardCalculations;
use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Customer;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\UserDashboardWidget;
use App\Modules\Base\Models\Vendor;
use App\Modules\Base\Models\WidgetDashboard;
use Carbon\Carbon;
use App\Modules\Base\Models\Order as dbOrder;
use App\Modules\Base\Models\OrderItem as dbOrderItem;
use App\Modules\Base\Models\Product as dbProduct;
use App\Modules\Base\Models\Sale as dbSale;
use App\Modules\Base\Models\SalesItem as dbSalesItem;
use App\Modules\Base\Models\SalesDetails as dbSalesDetail;

class DashboardReportController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'tenant']);
        $this->compute = New Computations();
        $this->dcs = New DashboardCalculations();

    }

    public function change_widgets()
    {

        $input = request()->all();
        try {
            $widgetToDelete = new UserDashboardWidget();
            $widgetToDelete::where('user_id', auth()->user()->id)->delete();
            foreach ($input['widgets'] as $eachWidgets) {
                $userWidgetModel = new UserDashboardWidget();
                $userWidgetModel->user_id = auth()->user()->id;
                $userWidgetModel->widget_dashboard_id = $eachWidgets;
                $userWidgetModel->save();
            }
            return response()->json(['message' => 'success']);
        } catch (\Exception $ex) {
            return response()->json(['message' => 'notvalid']);
        }
    }


    public function stats_index()
    {
        //exit;

        $userCategory = auth()->user()->roles;
        $userCategory = $userCategory->pluck('category')->all();
        $data['has_user_widget'] = UserDashboardWidget::isUserWidget();
        $data['business_owner'] = Role::getBusinessOwnerUser();
        $user_widget = WidgetDashboard::getUserWidget(auth())->toArray();
        $categoryWidget = WidgetDashboard::getWidgetByCategory($userCategory)->toArray();
        $theWidget = [];
        foreach ($categoryWidget as $key => $eachCategory) {
            $categoryWidget[$key]['checked'] = '';
            if (in_array($eachCategory, $user_widget)) {
                $categoryWidget[$key]['checked'] = 'checked';
            }
        }
        $data['category_widget'] = $categoryWidget;
        return response()->json($data);
    }


    public function supplierByPurchases()
    {
        $graphData = ['series' => [['dataField' => 'value',
            'displayText' => 'name']]];

        $ledgerModel = new Ledger();
        $graphData['suppliers'] = $ledgerModel->topSupplierByPurchases();

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.supplier_by_purchases');
        }
    }

    public function customerBySales()
    {
        $graphData = [
            'series' => [
                [
                    'dataField' => 'value',
                    'displayText' => 'name'
                ]
            ]
        ];
        $ledgerModel = new Ledger();
        $graphData['customers'] = $ledgerModel->topCustomerBySales();
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.customers_by_sales');
        }
    }

    public function receivablesOwed()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Receivables']
            ]
        ];
        $ledgerModel = new Ledger();
        $topReceivables = $ledgerModel->topReceivables();
        $graphData['dataSet'] = $topReceivables;
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.receivables_owed');
        }

    }


    public function top_payables_accounts()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Payables']
            ]
        ];
        $ledgerModel = new Ledger();
        $topPayables = $ledgerModel->topPayables();
        $graphData['dataSet'] = $topPayables;

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.top_payables_accounts');
        }
    }

    public function debtors_collection_period()
    {
        $computations = New Computations();
        $data['currentDebtorPeriod'] = $computations->debtorCollectionPeriod();
        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard.partials.debtors_collection_period');
        }
    }

    public function index()
    {
        $bankBalances = $this->_bankBalances();
        $businessSummary = $this->_businessSummaryMonth();
        $businessSummaryYear = $this->_businessSummaryYear();
        $cashAtHand = $this->_cashAtHand();
        $incomeStatement = $this->_incomeStatement();


        $data = array_merge($bankBalances, $businessSummary);
        $data = array_merge($cashAtHand, $data);
        $data = array_merge($incomeStatement, $data);
        $data = array_merge($businessSummaryYear, $data);
        $datas['widgets'] = WidgetDashboard::getUserWidget(auth());
        $data = array_merge($datas, $data);

        //pr($datas['widgets']);exit;
        return view('dashboard.index')->with($data);
    }

    public function _cashAtHand()
    {
        $ledger = new Ledger();
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');

        $previousStart = date('Y-m-d', strtotime('first day of last month'));
        $previousEnd = date('Y-m-d', strtotime('last day of last month'));
        $thisMonth = $ledger->sumTransactionItems($first_day_this_month, $last_day_this_month, 1261);
        $lastMonth = $ledger->sumTransactionItems($previousStart, $previousEnd, 1261);

        //dd($thisMonth);
        $data['thisMonth'] = $thisMonth['Value'];
        $data['lastMonth'] = $lastMonth['Value'];

        return $data;
    }

    public function _bankBalances()
    {
        $start = date('Y-m-d', strtotime('first day of last month'));
        //dd($start);
        $end_date = date('Y-m-d', strtotime('last day of last month'));
        //dd($end_date);
        $bank_balances_tmps = $this->dcs->getBankMonthBalances(16, $start, $end_date);
        $bank_balances_tmp = $this->dcs->getBankBalances();
        //dd($bank_balances_tmps);

        $data['bank_accounts'] = $bank_balances_tmp['bank_accounts'];
        $data['bank_accounts1'] = $bank_balances_tmps['bank_accounts'];

        //dd($data['bank_accounts1']);
        $data['total_account_balance'] = $bank_balances_tmp['total_account_balance'];
        $data['latest_transaction'] = TransactionItem::orderBy('created_at', 'desc')->select('dc', 'amount', 'created_at')->first();
        return $data;
    }


    public function _businessSummaryYear()
    {
        $latestTransaction = Transaction::orderBy('transaction_date', 'desc')->select('transaction_date')->pluck('transaction_date')->first();

        try {
            $_date = \Carbon\Carbon::createFromFormat('Y-m-d', $latestTransaction);
            $currMonthStarts = $_date->startOfYear()->toDateString();
            $currMonthEnds = $_date->endOfYear()->toDateString();

            /*
            * WEIRD BUGS WITH PHP DATE... $_date->subMonth() GIVES ME A WRONG VALUE BUT
            * $_date->startOfMonth()->subMonth() WORKS FINE
            * Seems to be a PHP PROBLEM => date_sub( date_create( $latest ), date_interval_create_from_date_string( '1 month' ) )
            */
            $currentMonthYear = $_date->year;
            $_date = $_date->startOfYear()->subMonth();
            $prevMonthEnds = $_date->endOfYear()->toDateString();
            $prevMonthStarts = $_date->endOfYear()->toDateString();
            $previousMonthYear = $_date->year;
            //dd( $previousMonthYear);
        } catch (\Exception $e) {
            $currentMonthYear = null;
            $previousMonthYear = null;
            $currMonthStarts = null;
            $currMonthEnds = null;
            $prevMonthEnds = null;
            $prevMonthStarts = null;
        }

        /*
        *
        * Calculations for Financial Summary Section!!!
        * In calculating the Financial Summary as below, we use the last transaction month for the most recent fiscalYear.
        *
        * BALANCE SHEET ITEMS... CUMULATED
        * PROFIT/LOSS ITEMS... NOT CUMULATED
        *
        * For the depreciation for the currTotalAssets, we subtract the depreciation for the current period
        * as well as the depreciation for the previous periods which is y for the previous periods a YEAR
        * is provided for the calculations.
        */
        $currDepreciation = AccountGroup::calculateAccumulatedDepreciation($currMonthStarts, $currMonthEnds);
        $prevDepreciation = AccountGroup::calculateAccumulatedDepreciation(null, null, $previousMonthYear);

        /* BALANCE SHEET ITEMS */
        $data['icurrTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $currMonthEnds, $currentMonthYear, true);
        $data['iprevTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $prevMonthEnds, $previousMonthYear, true);

        // MINUS IP and PPE DEPRECIATION
        $prevDepreciation = $prevDepreciation['ppeTotal'] + $prevDepreciation['ipTotal'];
        $currDepreciation = $currDepreciation['ppeTotal'] + $currDepreciation['ipTotal'];

        $data['icurrTotalAssets'] = $data['icurrTotalAssets'] - ($currDepreciation + $prevDepreciation);
        $data['iprevTotalAssets'] = $data['iprevTotalAssets'] - $prevDepreciation;

        $data['itotalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $currMonthEnds, $currentMonthYear, true);
        $data['iprevTotalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $prevMonthEnds, $previousMonthYear, true);

        $data['itotalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $currMonthEnds, $currentMonthYear, true);
        $data['iprevTotalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $prevMonthEnds, $previousMonthYear, true);

        /* PROFIT/LOSS ITEMS */
        $data['itotalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
        $data['iprevTotalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);
        $data['itotalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
        $data['iprevTotalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);

        return $data;
    }

    public function _businessSummaryMonth()
    {
        $latestTransaction = Transaction::orderBy('transaction_date', 'desc')->select('transaction_date')->pluck('transaction_date')->first();

        try {
            $_date = \Carbon\Carbon::createFromFormat('Y-m-d', $latestTransaction);
            $currMonthStarts = $_date->startOfMonth()->toDateString();
            $currMonthEnds = $_date->endOfMonth()->toDateString();

            /*
            * WEIRD BUGS WITH PHP DATE... $_date->subMonth() GIVES ME A WRONG VALUE BUT
            * $_date->startOfMonth()->subMonth() WORKS FINE
            * Seems to be a PHP PROBLEM => date_sub( date_create( $latest ), date_interval_create_from_date_string( '1 month' ) )
            */
            $currentMonthYear = $_date->year;
            $_date = $_date->startOfMonth()->subMonth();
            $prevMonthEnds = $_date->endOfMonth()->toDateString();
            $prevMonthStarts = $_date->startOfMonth()->toDateString();
            $previousMonthYear = $_date->year;
            //dd( $previousMonthYear);
        } catch (\Exception $e) {
            $currentMonthYear = null;
            $previousMonthYear = null;
            $currMonthStarts = null;
            $currMonthEnds = null;
            $prevMonthEnds = null;
            $prevMonthStarts = null;
        }

        /*
        *
        * Calculations for Financial Summary Section!!!
        * In calculating the Financial Summary as below, we use the last transaction month for the most recent fiscalYear.
        *
        * BALANCE SHEET ITEMS... CUMULATED
        * PROFIT/LOSS ITEMS... NOT CUMULATED
        *
        * For the depreciation for the currTotalAssets, we subtract the depreciation for the current period
        * as well as the depreciation for the previous periods which is y for the previous periods a YEAR
        * is provided for the calculations.
        */
        $currDepreciation = AccountGroup::calculateAccumulatedDepreciation($currMonthStarts, $currMonthEnds);
        $prevDepreciation = AccountGroup::calculateAccumulatedDepreciation(null, null, $previousMonthYear);

        /* BALANCE SHEET ITEMS */
        $data['currTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $currMonthEnds, $currentMonthYear, true);
        $data['prevTotalAssets'] = $this->dcs->calculateAccountGroupTotal(1, null, $prevMonthEnds, $previousMonthYear, true);

        // MINUS IP and PPE DEPRECIATION
        $prevDepreciation = $prevDepreciation['ppeTotal'] + $prevDepreciation['ipTotal'];
        $currDepreciation = $currDepreciation['ppeTotal'] + $currDepreciation['ipTotal'];

        $data['currTotalAssets'] = $data['currTotalAssets'] - ($currDepreciation + $prevDepreciation);
        $data['prevTotalAssets'] = $data['prevTotalAssets'] - $prevDepreciation;

        $data['totalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $currMonthEnds, $currentMonthYear, true);
        $data['prevTotalLiabilities'] = $this->dcs->calculateAccountGroupTotal(2, null, $prevMonthEnds, $previousMonthYear, true);

        $data['totalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $currMonthEnds, $currentMonthYear, true);
        $data['prevTotalEquity'] = $this->dcs->calculateAccountGroupTotal(3, null, $prevMonthEnds, $previousMonthYear, true);

        /* PROFIT/LOSS ITEMS */
        $data['totalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
        $data['prevTotalIncome'] = $this->dcs->calculateAccountGroupTotal(4, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);
        $data['totalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $currMonthStarts, $currMonthEnds, $currentMonthYear, false);
        $data['prevTotalExpenses'] = $this->dcs->calculateAccountGroupTotal(5, $prevMonthStarts, $prevMonthEnds, $previousMonthYear, false);

        return $data;
    }


    public function stock_summary()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Stock Summary']
            ]
        ];
        $stock = $this->calculateStockMonthBalance();
        $graphData['dataSet'] = $stock['data'];
        $ends = $stock['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;
        //return view('dashboard_report.stock_summary')->with($data);

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.stock_summary');
        }
    }


    public function receivables_accounts()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Receivables History']
            ]
        ];
        $stock = $this->calculateStockMonthBalance(12);
        $graphData['dataSet'] = $stock['data'];
        $ends = $stock['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.receivables_accounts');
        }
    }


    public function revenues()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Revenue History']
            ]
        ];
        $revenue = [
            35, // income
            36, // other income
        ];
        $stock = $this->calculateStockMonthBalance($revenue);
        $graphData['dataSet'] = $stock['data'];
        $ends = $stock['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.receivables_accounts');
        }
    }


    public function revenues_month()
    {
        $input = request()->all();
        $revenue = [
            35, // income
            36, // other income
        ];
        $sales = $this->calculateStockMonthBalance($revenue);
        $sales = $sales['data'];

        $salesFull = $sales[$input['index']];
        $salesMonth = $salesFull['MonthNumber'];
        $salesYear = $salesFull['Year'];

        $begins = $salesYear . "-" . $salesMonth . "-01";
        $ends = $salesYear . "-" . $salesMonth . "-31";
        $ledger = new Ledger();
        $salesWeek = $ledger->calculateGroupWeekBalance($begins, $ends, $revenue);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $salesFull['Month'],
        ]);
    }


    public function receivables_accounts_month()
    {
        $input = request()->all();
        $sales = $this->calculateStockMonthBalance(12);
        $sales = $sales['data'];

        $salesFull = $sales[$input['index']];
        $salesMonth = $salesFull['MonthNumber'];
        $salesYear = $salesFull['Year'];

        $begins = $salesYear . "-" . $salesMonth . "-01";
        $ends = $salesYear . "-" . $salesMonth . "-31";
        $ledger = new Ledger();
        $salesWeek = $ledger->calculateGroupWeekBalance($begins, $ends, 12);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $salesFull['Month'],
        ]);
    }


    public function payables_accounts()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Payables History']
            ]
        ];
        $stock = $this->calculateStockMonthBalance(28);
        $graphData['dataSet'] = $stock['data'];
        $ends = $stock['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.payables_accounts');
        }
    }

    public function payables_accounts_month()
    {
        $input = request()->all();
        $sales = $this->calculateStockMonthBalance(28);
        $sales = $sales['data'];

        $salesFull = $sales[$input['index']];
        $salesMonth = $salesFull['MonthNumber'];
        $salesYear = $salesFull['Year'];

        $begins = $salesYear . "-" . $salesMonth . "-01";
        $ends = $salesYear . "-" . $salesMonth . "-31";
        $ledger = new Ledger();

        $salesWeek = $ledger->calculateGroupWeekBalance($begins, $ends, 28);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $salesFull['Month'],
        ]);
    }


    public function return_on_capital()
    {
        $graphData = [
            'dataSet' => [],
            'series' => [
                ['dataField' => 'ROCE', 'displayText' => 'Return on Capital Employed']
            ]
        ];
        try {
            $dashBoardCalculations = new DashboardCalculations();
            if (!empty(FiscalYear::getCurrentFiscalYear())) {
                $currentFiscalYearBegins = FiscalYear::getCurrentFiscalYear()->begins;
                $currentFiscalYearEnds = FiscalYear::getCurrentFiscalYear()->ends;
            }


            $quarters = get_quarters($currentFiscalYearBegins, $currentFiscalYearEnds);
            foreach ($quarters as $timelines) {
                $profitabilityRatio = $dashBoardCalculations->calculateProfitabilityRatio($timelines->period_start, $timelines->period_end);
                $profitabilityRatio['Quarter'] = $timelines->periodOnly;
                array_push($graphData['dataSet'], $profitabilityRatio);
            }

        } catch (\Exception $exception) {

        }
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.net_profit_margin');
        }

    }


    public function grossProfitMargin()
    {
        $graphData = [
            'dataSet' => [],
            'series' => [
                ['dataField' => 'GPM', 'displayText' => 'Gross Profit Margin']
            ]
        ];
        try {
            $dashBoardCalculations = new DashboardCalculations();
            if (!empty(FiscalYear::getCurrentFiscalYear())) {
                $currentFiscalYearBegins = FiscalYear::getCurrentFiscalYear()->begins;
                $currentFiscalYearEnds = FiscalYear::getCurrentFiscalYear()->ends;
            }

            $quarters = get_quarters($currentFiscalYearBegins, $currentFiscalYearEnds);
            foreach ($quarters as $timelines) {
                $profitabilityRatio = $dashBoardCalculations->calculateProfitabilityRatio($timelines->period_start, $timelines->period_end);
                $profitabilityRatio['Quarter'] = $timelines->periodOnly;
                array_push($graphData['dataSet'], $profitabilityRatio);
            }
        } catch (\Exception $exception) {

        }

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.gross_profit_margin');
        }

    }

    public function net_profit_margin()
    {
        $graphData = [
            'dataSet' => [],
            'series' => [
                ['dataField' => 'NPM', 'displayText' => 'Net Profit Margin']
            ]
        ];
        try {
            $dashBoardCalculations = new DashboardCalculations();
            if (!empty(FiscalYear::getCurrentFiscalYear())) {
                $currentFiscalYearBegins = FiscalYear::getCurrentFiscalYear()->begins;
                $currentFiscalYearEnds = FiscalYear::getCurrentFiscalYear()->ends;
            }

            $quarters = get_quarters($currentFiscalYearBegins, $currentFiscalYearEnds);
            foreach ($quarters as $timelines) {
                $profitabilityRatio = $dashBoardCalculations->calculateProfitabilityRatio($timelines->period_start, $timelines->period_end);
                $profitabilityRatio['Quarter'] = $timelines->periodOnly;
                array_push($graphData['dataSet'], $profitabilityRatio);
            }
        } catch (\Exception $exception) {

        }
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.net_profit_margin');
        }

    }

    public function running_expenses()
    {
        $ledger = new Ledger();
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');

        $previousStart = date('Y-m-d', strtotime('first day of last month'));
        $previousEnd = date('Y-m-d', strtotime('last day of last month'));
        $data ['previousExpenses'] = $ledger->runningExpenses($previousStart, $previousEnd);
        $data ['currentExpenses'] = $ledger->runningExpenses($first_day_this_month, $last_day_this_month);
        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard_report.running_expenses')->with($data);
        }
    }

    public function sales_history()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Sales History']
            ]
        ];
        $sales = $this->calculateSalesMonthBalance();
        $graphData['dataSet'] = $sales['data'];
        //array_push($graphData['dataSet'], $sales['data']);
        $ends = $sales['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.sales_history');
        }
    }

    public function sales_history_month()
    {
        $input = request()->all();
        $sales = $this->calculateSalesMonthBalance();
        $sales = $sales['data'];

        $salesFull = $sales[$input['index']];
        $salesMonth = $salesFull['MonthNumber'];
        $salesYear = $salesFull['Year'];

        $begins = $salesYear . "-" . $salesMonth . "-01";
        $ends = $salesYear . "-" . $salesMonth . "-31";
        $ledger = new Ledger();
        $salesWeek = $ledger->calculateLedgerWeekBalance($begins, $ends, 4110);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $salesFull['Month'],
        ]);
    }

    public function purchases_history()
    {
        $graphData = [
            //'dataSet' => [],
            'series' => [
                ['dataField' => 'Value', 'displayText' => 'Purchases History']
            ]
        ];
        $sales = $this->calculatePurchaseMonthBalance();
        $graphData['dataSet'] = $sales['data'];
        //array_push($graphData['dataSet'], $sales['data']);
        $ends = $sales['ends'];

        $fiscalYearStop = new \DateTime($ends);
        $year = $fiscalYearStop->format("Y");
        $graphData['year'] = $year;

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.purchase_history');
        }
    }

    public function purchases_history_month()
    {
        $input = request()->all();
        $sales = $this->calculatePurchaseMonthBalance();
        $sales = $sales['data'];

        $salesFull = $sales[$input['index']];
        $salesMonth = $salesFull['MonthNumber'];
        $salesYear = $salesFull['Year'];

        $begins = $salesYear . "-" . $salesMonth . "-01";
        $ends = $salesYear . "-" . $salesMonth . "-31";
        $ledger = new Ledger();
        $salesWeek = $ledger->calculateLedgerWeekBalance($begins, $ends, 5101);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $salesFull['Month'],
        ]);
    }

    private function calculateSalesMonthBalance($ledgerid = 4110)
    {
        try {
            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $begins = $fiscalYear->begins;
            $ends = $fiscalYear->ends;
            $ledger = new Ledger();
            $return['data'] = $ledger->calculateLedgerMonthBalance($begins, $ends, $ledgerid);
            $return['ends'] = $ends;
        } catch (\Exception $exception) {
            $return['data'] = [];
            $return['ends'] = "";
        }
        return $return;
    }

    private function calculatePurchaseMonthBalance($ledgerid = 5101)
    {
        try {
            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $begins = $fiscalYear->begins;
            $ends = $fiscalYear->ends;
            $ledger = new Ledger();
            $return['data'] = $ledger->calculateLedgerMonthBalance($begins, $ends, $ledgerid);
            $return['ends'] = $ends;
        } catch (\Exception $exception) {
            $return['data'] = [];
            $return['ends'] = "";
        }
        return $return;
    }

    private function calculateStockMonthBalance($group = 10)
    {
        try {
            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $begins = $fiscalYear->begins;
            $ends = $fiscalYear->ends;
            $ledger = new Ledger();
            $return['data'] = $ledger->calculateGroupMonthBalance($begins, $ends, $group);
            $return['ends'] = $ends;
        } catch (\Exception $exception) {
            $return['data'] = [];
            $return['ends'] = "";
        }
        return $return;
    }

    public function stock_summary_month()
    {
        $input = request()->all();
        $stock = $this->calculateStockMonthBalance();
        $stock = $stock['data'];
        $stockFull = $stock[$input['index']];
        $stockMonth = $stockFull['MonthNumber'];
        $stockYear = $stockFull['Year'];

        $begins = $stockYear . "-" . $stockMonth . "-01";
        $ends = $stockYear . "-" . $stockMonth . "-31";
        $ledger = new Ledger();
        $salesWeek = $ledger->calculateGroupWeekBalance($begins, $ends, 10);
        return response()->json([
            'type' => 'success',
            'data' => $salesWeek,
            'month' => $salesMonth = $stockFull['Month'],
        ]);
    }

    public function payablesAgeAnalysis()
    {
        $this->ledger = new Ledger();
        if (request()->input('from_date')) {
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));
            $ledgers = $this->ledger->where('group_id', 28)->get();
        } else {
            $ledgers = $this->ledger->where('group_id', 28)->get();
            $data['start_date'] = '';
            $data['end_date'] = '';
        }
        $current_date = strtotime(Carbon::now());

        $__analysis[0]['type'] = "Below 30";
        $__analysis[1]['type'] = "Between 30 and 60";
        $__analysis[2]['type'] = "Between 60 and 90";
        $__analysis[3]['type'] = "Between 90 and 180";
        $__analysis[4]['type'] = "Over 180";


        $__analysis[0]['amount'] = 0;
        $__analysis[1]['amount'] = 0;
        $__analysis[2]['amount'] = 0;
        $__analysis[3]['amount'] = 0;
        $__analysis[4]['amount'] = 0;

        $counter = 0;
        foreach ($ledgers as $eachLedgers) {
            if ($eachLedgers->transactionItems()->where('dc', 'D')->count()) {
                foreach ($eachLedgers->transactionItems()->where('dc', 'D')
                             ->orderBy('ca_transaction_items.amount', 'desc')
                             ->get() as $item) {
                    $trans_date = strtotime($item->transactions->transaction_date);
                    $dateDiff = (int)ceil(abs($current_date - $trans_date) / 86400);

                    if ($dateDiff < 30) {
                        $__analysis[0]['amount'] += $item->amount;
                    } else if (($dateDiff > 30) AND ($dateDiff < 60)) {
                        $__analysis[1]['amount'] += $item->amount;

                    } else if (($dateDiff > 60) AND ($dateDiff < 90)) {
                        $__analysis[2]['amount'] += $item->amount;

                    } else if (($dateDiff > 90) AND ($dateDiff < 180)) {
                        $__analysis[3]['amount'] += $item->amount;

                    } else if (($dateDiff > 180)) {
                        $__analysis[4]['amount'] += $item->amount;
                    }
                    $counter++;
                }
            }

        }
        $data = [
            'series' => [
                ['dataField' => 'amount', 'displayText' => 'Payable Age Analysis']
            ]
        ];

        $data['analysis'] = $__analysis;
        try {
            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $ends = $fiscalYear->ends;
            $fiscalYearStop = new \DateTime($ends);
            $year = $fiscalYearStop->format("Y");
            $data['year'] = $year;
        } catch (\Exception $exception) {
            $data['year'] = "";
        }

        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard_report.payables_age_analysis');
        }
    }

    public function acidTestRatio()
    {

        //where current asset id = 9
        $currentAsset = AccountGroup::find(9);
        $totalCurrentAsset = AccountGroup::getAccountBalance($currentAsset);

        //where current liabilities id = 20
        $currentLiabilities = AccountGroup::find(27);
        $totalCurrentLiabilities = AccountGroup::getAccountBalance($currentLiabilities);

        //where prepayment id = 15
        $prepayment = AccountGroup::find(19);
        $totalPrepayment = AccountGroup::getAccountBalance($prepayment);

        //where stock id = 16
        $stock = AccountGroup::find(10);
        $totalStock = AccountGroup::getAccountBalance($stock);

        $graphData['totalCurrentAsset'] = $totalCurrentAsset;
        $graphData['totalCurrentLiabilities'] = $totalCurrentLiabilities;
        $graphData['totalInventory'] = $totalPrepayment + $totalStock;
        $graphData['acidTestRatio'] = $this->compute->acidTestRatio();
//        $dataSet['totalCurrentAsset'] = $totalCurrentAsset;
//        $dataSet['totalCurrentLiabilities']= $totalCurrentLiabilities;
//        $dataSet['totalInventory']=  $totalPrepayment+$totalStock;
//        $dataSet['acidTestRatio']= $this->compute->acidTestRatio();
//        $graphData['dataSet'] =$dataSet;
//        dd($graphData);

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.acid_test_ratio');
        }
    }

    public function cashAtHand()
    {
        $ledger = new Ledger();
        $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
        $last_day_this_month = date('Y-m-t');

        $previousStart = date('Y-m-d', strtotime('first day of last month'));
        $previousEnd = date('Y-m-d', strtotime('last day of last month'));
        $thisMonth = $ledger->sumTransactionItems($first_day_this_month, $last_day_this_month, 21);
        $lastMonth = $ledger->sumTransactionItems($previousStart, $previousEnd, 21);

        //dd($thisMonth);
        $data['thisMonth'] = $thisMonth['Value'];
        $data['lastMonth'] = $lastMonth['Value'];

        //dd($data);
        return view('dashboard_report.cash_in_hand')->with($data);

    }

    public function gearRatio()
    {
        $end_date = Carbon::now();
        $start_date = Carbon::now();
        $curr_year = null;

        try {
            $fiscal_year = FiscalYear::getCurrentFiscalYear();
            $end_date = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->ends)->format('d/m/Y');
            $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->begins)->format('d/m/Y');
            $curr_year = date('Y', strtotime($fiscal_year->ends));

        } catch (\Exception $exception) {

        }
        //Long time loan
        $non_curr_liabilities = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 23)->first();

        $group = $non_curr_liabilities->groups;
        $longTermBorrowing = $group[0]->getBalance($start_date, $end_date, null, true);


        $currYrEquityTotal = 0;
        //get equity
        $equity = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();

        if ($groups = $equity->groups) {
            foreach ($groups as $group) {
                $currYrResult = $group->getBalance($start_date, $end_date, null, true);
                if ($currYrResult['dc'] === 'D') {
                    $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2);
                } elseif ($currYrResult['dc'] === 'C') {
                    $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2);
                }

                if ($nestedGroup = $group->nestedGroups($group->id)) {
                    foreach ($nestedGroup->orderBy('code', 'asc')->get() as $child) {

                        $currYrResult = $child->getBalance($start_date, $end_date, null, true);
                        if ($currYrResult['dc'] === 'D') {
                            $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2);
                        } elseif ($currYrResult['dc'] === 'C') {
                            $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2);
                        }

                        if ($subChild = $child->nestedGroups($child->id)) {
                            foreach ($subChild->orderBy('code', 'asc')->get() as $subs) {
                                $currYrResult = $subs->getBalance($start_date, $end_date, null, true);
                                if ($currYrResult['dc'] === 'D') {
                                    $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2);
                                } elseif ($currYrResult['dc'] === 'C') {
                                    $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2);
                                }
                            }
                        }
                    }
                }
            }
        }



        $currYearProfitBeforeTax = $this->dcs->calculateRetainedEarningsTemporaryFunction($curr_year);

        if ($equity->ledgers->count()) {
            foreach ($equity->ledgers()->orderBy('code', 'asc')->get() as $ledger) {
                if ($ledger->name == 'Retained Earnings') {
                    $one = $currYearProfitBeforeTax;
                    $two = $ledger->getBalance(true, null, null, $curr_year, true);
                    $currYrResult = empty($one['balance']) ? $two : $one;
                    $currYrResult['balance'] = bcadd($one['balance'], $two['balance'], 2);
                } else {
                    $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true);
                }

                if ($currYrResult['dc'] === 'D') {
                    $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2);
                } elseif ($currYrResult['dc'] === 'C') {
                    $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2);
                }
            }
        }

        //Gear ratio: long term loan/total equity
        $graphData['longTermBorrowing'] = formatNumber($longTermBorrowing['balance']);
        $graphData['currYrEquityTotal'] = formatNumber($currYrEquityTotal);
        try {
            $graphData['gearRatio'] = formatNumber($longTermBorrowing['balance'] / $currYrEquityTotal);
        } catch (\Exception $e) {
            $graphData['gearRatio'] = 0;
            $graphData['longTermBorrowing'] = 0;
            $graphData['currYrEquityTotal'] = 0;
        }


        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.gear_ratio');
        }
    }


    public function creditorPaymentPeriod()
    {
        $graphData['currentCreditorPeriod'] = $this->compute->creditorPaymentPeriod();
        //dd($data);

        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.creditor_payment_period');
        }
    }


    public function topFivePurchasesOfStock()
    {
        $date_from = '';
        $date_to = '';
        $year = '';
        try {
            $this->ledger = new Ledger();
            if (request()->input('from_date')) {
                $input = request()->all();
                $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
                $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));
                //$data['ledgers'] = $this->ledger->where('group_id', 12)->get();
                $ledgers = $this->ledger->where('group_id', 12)->get();
            } else {
                $ledgers = $this->ledger->where('group_id', 12)->get();
                $fiscalYear = FiscalYear::getCurrentFiscalYear();
                $date_from = $fiscalYear->begins;
                $date_to = $fiscalYear->ends;
            }
            $fiscalYearStop = new \DateTime($date_to);
            $year = $fiscalYearStop->format("Y");
        } catch (\Exception $exception) {

        }

//dd($date_from, $date_to);
        $itemQtyList = [];
        $itemPriceList = [];
        $itemProductList = [];
        $itemProductIdList = [];
        $orderItemTest = [];
        try {
            $orders = dbOrder::WhereRaw('issue_date >= ? and issue_date <= ?', [$date_from, $date_to])->get();
            if (!empty($orders)) {
                foreach ($orders as $c => $order) {
                    $orderItems = dbOrderItem::WhereRaw('order_id = ?', [$order->id])->get();
                    if (!empty($orderItems)) {
                        foreach ($orderItems as $sn => $item) {
                            if (empty($itemQtyList[$item->product_id])) {
                                $itemQtyList[$item->product_id] = $item->quantity;
                                $itemPriceList[$item->product_id] = $item->total;
                                $itemProductIdList[$item->product_id] = $item->product_id;
                                $itemProductList[$item->product_id] = dbProduct::find($item->product_id)->name;
                            } else {
                                $itemQtyList[$item->product_id] += $item->quantity;
                                $itemPriceList[$item->product_id] += $item->total;
                                $itemProductIdList[$item->product_id] = $item->product_id;
                                $itemProductList[$item->product_id] = dbProduct::find($item->product_id)->name;
                            }
                        }
                    }
                }
            }
            $orderItem = new OrderItem();

            arsort($itemPriceList);
            $itemPriceListAfterSlice = array_slice($itemPriceList, 0, 5);
            foreach ($itemPriceListAfterSlice as $index => $list) {
                $orderItem->setQuantity($itemQtyList[$index]);
                $orderItem->setPrice($itemPriceList[$index]);
                $orderItem->setProduct($itemProductList[$index]);
                $orderItemTest[] = $orderItem->morphToJSON();
            }
        } catch (\Exception $exception) {

        }


        $data = [
            'series' => [
                ['dataField' => 'price', 'displayText' => 'Receive Age Analysis']
            ]
        ];
        $data['analysis'] = $orderItemTest;
        $data['year'] = $year;

        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard_report.top_five_purchases_of_stock');
        }
    }

    public function bankBalances()
    {
        $start = date('Y-m-d', strtotime('first day of last month'));
        //dd($start);
        $end_date = date('Y-m-d', strtotime('last day of last month'));
        //dd($end_date);
        $bank_balances_tmps = $this->dcs->getBankMonthBalances(16, $start, $end_date);
        $bank_balances_tmp = $this->dcs->getBankBalances();
        //dd($bank_balances_tmps);

        $data['bank_accounts'] = $bank_balances_tmp['bank_accounts'];
        $data['bank_accounts1'] = $bank_balances_tmps['bank_accounts'];

        //dd($data['bank_accounts1']);
        $data['total_account_balance'] = $bank_balances_tmp['total_account_balance'];
        $data['latest_transaction'] = TransactionItem::orderBy('created_at', 'desc')->select('dc', 'amount', 'created_at')->first();
        //dd($data['latest_transaction']);

        return view('dashboard_report.bank_balances')->with($data);
    }

    public function receivableAgeAnalysis()
    {
        $this->ledger = new Ledger();
        if (request()->input('from_date')) {
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));

            //$data['ledgers'] = $this->ledger->where('group_id', 12)->get();
            $ledgers = $this->ledger->where('group_id', 12)->get();
        } else {
            $ledgers = $this->ledger->where('group_id', 12)->get();
            $data['start_date'] = '';
            $data['end_date'] = '';
        }
        $data['title'] = 'Receivables Age Analysis';
        $current_date = strtotime(Carbon::now());

        $__analysis[0]['type'] = "Below 30";
        $__analysis[1]['type'] = "Between 30 and 60";
        $__analysis[2]['type'] = "Between 60 and 90";
        $__analysis[3]['type'] = "Between 90 and 180";
        $__analysis[4]['type'] = "Over 180";


        $__analysis[0]['amount'] = 0;
        $__analysis[1]['amount'] = 0;
        $__analysis[2]['amount'] = 0;
        $__analysis[3]['amount'] = 0;
        $__analysis[4]['amount'] = 0;

        $counter = 0;
        foreach ($ledgers as $eachLedgers) {
            if ($eachLedgers->transactionItems()->where('dc', 'C')->count()) {
                foreach ($eachLedgers->transactionItems()->where('dc', 'C')
                             ->orderBy('ca_transaction_items.amount', 'desc')
                             ->get() as $item) {
                    $trans_date = strtotime($item->transactions->transaction_date);
                    $dateDiff = (int)ceil(abs($current_date - $trans_date) / 86400);

                    if ($dateDiff < 30) {
                        $__analysis[0]['amount'] += $item->amount;
                    } else if (($dateDiff > 30) AND ($dateDiff < 60)) {
                        $__analysis[1]['amount'] += $item->amount;

                    } else if (($dateDiff > 60) AND ($dateDiff < 90)) {
                        $__analysis[2]['amount'] += $item->amount;

                    } else if (($dateDiff > 90) AND ($dateDiff < 180)) {
                        $__analysis[3]['amount'] += $item->amount;

                    } else if (($dateDiff > 180)) {
                        $__analysis[4]['amount'] += $item->amount;
                    }
                    $counter++;
                }
            }

        }
        $data = [
            'series' => [
                ['dataField' => 'amount', 'displayText' => 'Receive Age Analysis']
            ]
        ];

        $data['analysis'] = $__analysis;

        try {
            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $ends = $fiscalYear->ends;
            $fiscalYearStop = new \DateTime($ends);
            $year = $fiscalYearStop->format("Y");
            $data['year'] = $year;
        } catch (\Exception $exception) {
            $data['year'] = "";
        }


        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard_report.receivable_age_analysis');
        }
    }


    public function _incomeStatement()
    {
        $data['search'] = true;
        $fiscal_year = FiscalYear::getCurrentFiscalYear();
        try {
            $data['end_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->ends)->format('d/m/Y');
            $data['start_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->begins)->format('d/m/Y');
            $data['curr_year'] = date('Y', strtotime($fiscal_year->ends));
            $data['prev_year'] = $data['curr_year'] - 1;
        } catch (\Exception $e) {
            $data['end_date'] = null;
            $data['start_date'] = null;
            $data['curr_year'] = null;
            $data['prev_year'] = null;
        }

        if ($input = request()->all()) {
            if ($input['from_date'] !== '') {
                $data['start_date'] = $input['from_date'];
            }
            if ($input['to_date'] !== '') {
                $data['end_date'] = $input['to_date'];
            }
        }


        //get income account
        $data['income'] = AccountGroup::where('id', '35')->first();
        //get cost of sales account
        $data['direct_cost'] = AccountGroup::where('id', '37')->first();
        //get other_income account
        $data['other_income'] = AccountGroup::where('id', '36')->first();
        //get expenses accounts
        $data['expenses'] = AccountGroup::with(['groups' => function ($q) {
            $q->where('id', '!=', 37)->orderBy('code', 'asc');
        }])->where('id', '5')->first();


        return $data;
    }

    public function topFiveSalesOfStock()
    {
        $date_from = '';
        $date_to = '';
        $year = '';
        try {
            $this->ledger = new Ledger();
            if (request()->input('from_date')) {
                $input = request()->all();
                $data['start_date'] = date('Y/m/d', strtotime($input['from_date']));
                $data['end_date'] = $input['to_date'] ? date('Y/m/d', strtotime($input['to_date'])) : date('Y/m/d', strtotime(\Carbon\Carbon::now()));
                $ledgers = $this->ledger->where('group_id', 12)->get();
            } else {
                $ledgers = $this->ledger->where('group_id', 12)->get();
                $fiscalYear = FiscalYear::getCurrentFiscalYear();
                $date_from = $fiscalYear->begins;
                $date_to = $fiscalYear->ends;
                $format = \DateTime::createFromFormat('Y-m-d', $date_from);
                $date_from = $format->format("Y/m/d");
                $format = \DateTime::createFromFormat('Y-m-d', $date_to);
                $date_to = $format->format("Y/m/d");
            }

            $fiscalYearStop = new \DateTime($date_to);
            $year = $fiscalYearStop->format("Y");
        } catch (\Exception $exception) {

        }

//dd($date_from, $date_to);

        $itemQtyList = [];
        $itemPriceList = [];
        $itemProductList = [];
        $itemProductIdList = [];
        $itemProductCodeList = [];
        $saleItemTest = [];

        try {
            $sales = dbSale::WhereRaw('sales_date >= ? and sales_date <= ?', [$date_from, $date_to])->get();
            if (!empty($sales)) {

                foreach ($sales as $sale) {
                    $salesItems = dbSalesItem::WhereRaw('sales_id = ?', [$sale->id])->get();
                    if (!empty($salesItems)) {
                        foreach ($salesItems as $sn => $item) {
                            if (empty($itemQtyList[$item->product_id])) {
                                $itemQtyList[$item->product_id] = $item->quantity;
                                $itemPriceList[$item->product_id] = $item->line_total;
                                $itemProductIdList[$item->product_id] = $item->product_id;
                                $itemProductList[$item->product_id] = dbProduct::find($item->product_id)->name;
                                $itemProductCodeList[$item->product_id] = dbProduct::find($item->product_id)->code;
                            } else {
                                $itemQtyList[$item->product_id] += $item->quantity;
                                $itemPriceList[$item->product_id] += $item->line_total;
                                $itemProductIdList[$item->product_id] = $item->product_id;
                                $itemProductList[$item->product_id] = dbProduct::find($item->product_id)->name;
                                $itemProductCodeList[$item->product_id] = dbProduct::find($item->product_id)->code;
                            }
                        }
                    }
                }
            }
            $saleItem = new SalesItem();
            arsort($itemPriceList);
            $itemPriceListAfterSlice = array_slice($itemPriceList, 0, 5);
            foreach ($itemPriceListAfterSlice as $index => $list) {
                $saleItem->setQuantity($itemQtyList[$index]);
                $saleItem->setPrice($itemPriceList[$index]);
                $saleItem->setProduct($itemProductList[$index]);
                $saleItemTest[] = $saleItem->morphToJSON();
            }
        } catch (\Exception $exception) {

        }
        $data = [
            'series' => [
                ['dataField' => 'price', 'displayText' => 'Receive Age Analysis']
            ]
        ];
        $data['analysis'] = $saleItemTest;
        $data['year'] = $year;
        // dd(response()->json($data));
        if (request()->ajax()) {
            return response()->json($data);
        } else {
            return view('dashboard_report.top_five_sales_of_stock');
        }
        //return $saleItem;
    }

    public function currentRatio()
    {
        //where current asset id = 9
        $currentAsset = AccountGroup::find(9);
        $totalCurrentAsset = AccountGroup::getAccountBalance($currentAsset);

        //where current liabilities id = 20
        $currentLiabilities = AccountGroup::find(27);
        $totalCurrentLiabilities = AccountGroup::getAccountBalance($currentLiabilities);


        $graphData['totalCurrentAsset'] = $totalCurrentAsset;
        $graphData['totalCurrentLiabilities'] = $totalCurrentLiabilities;
        $graphData['currentRatio'] = $this->compute->currentRatio();

//dd($graphData);
        if (request()->ajax()) {
            return response()->json($graphData);
        } else {
            return view('dashboard_report.our_ability_to_cover_trading_liablities');
        }
    }


}