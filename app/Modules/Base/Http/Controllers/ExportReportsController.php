<?php

namespace App\Modules\Base\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\Customer;
use Barryvdh\DomPDF\PDF;
use App\Modules\Base\Traits\Export;

class ExportReportsController extends Controller
{
    public function __construct(Ledger $ledger, AccountGroup $gl_group, Transaction $transaction)
    {
        $this->middleware(['tenant']);
        $this->ledger = $ledger;
        $this->gl_group = $gl_group;
        $this->transaction = $transaction;
    }

    /**
     * method to export trail balance to PDF/XLS
     * 
     * @return Response
     */
    public function exportTrialBalance()
    {
        $export_type = request()->get('type');

        $data['title']      =   'Trial Balance';
        $data['classes']    = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();
        $data['start_date'] = request()->get('start_date');
        $data['end_date'] = request()->get('end_date');
        $data['view'] = 'reports.exports.trialbalance';
        if($export_type === 'pdf'){
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('trial-balance.pdf');
        }else {
            Export::exportTo($export_type, $data);
        }
    }

    /**
     * method to export balance sheet to PDF/Excel
     *
     */
    public function exportBalanceSheet($export_type)
    {
        $data['title']  =   'Statment of Financial Position';
        //get non-current assets groups
        $data['non_current_assets'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 6)->first();
        //get current assets groups
        $data['current_assets'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 9)->first();
        //get current year accumulated depreciation
        $data['curr_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y'));
        //get previous year accumulated depreciation
        $data['prev_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y', strtotime('-1 year')));
        //get non-current liabilities groups
        $data['non_curr_liabilities'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 23)->first();
        //get current liabilities groups
        $data['curr_liabilities'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 27)->first();
        //get equity
        $data['equity'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();

        $data['view'] = 'reports.exports.balancesheet';

        Export::exportTo($export_type, $data);
    }

    public function exportFiveYearBalanceSheet($export_type)
    {
        $data['title']  =   'Five-Year Financial Summary';
        //get non-current assets groups
        $data['non_current_assets'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 6)->first();
        //get current assets groups
        $data['current_assets'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 9)->first();
        //get current year accumulated depreciation
        $data['curr_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y'));
        //get previous year 1 accumulated depreciation
        $data['prev1_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y', strtotime('-1 year')));
        //get previous year 2 accumulated depreciation
        $data['prev2_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y', strtotime('-2 year')));
        //get previous year 3 accumulated depreciation
        $data['prev3_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y', strtotime('-3 year')));
        //get previous year 4 accumulated depreciation
        $data['prev4_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, date('Y', strtotime('-4 year')));
        //get non-current liabilities groups
        $data['non_curr_liabilities'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 23)->first();
        //get current liabilities groups
        $data['curr_liabilities'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 27)->first();
        //get equity
        $data['equity'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();

        $data['view'] = 'reports.Views.exports.fiveyearbalancesheet';

        Export::exportTo($export_type, $data);
    }

    public function exportIncomeStatement( $export_type )
    {
        $inputs = request()->only(['from_date', 'to_date', 'period']);
        if(isset($inputs['period'])) {
            $data['title']  =   'Income Statement';
            $data['from_date']  = $inputs['from_date'];
            $data['to_date']  =   $inputs['to_date'];
            $data['classesr'] = AccountGroup::with(['groups' => function($q){
                $q->orderBy('code', 'asc');
            }])->where('id','4')->get();


            $data['classesea'] = AccountGroup::with(['groups' => function($q){
                $q->orderBy('code', 'asc');
            }])->where('id','5')->get();             
            
            $data['view'] = 'accounting::Reports.Views.exports.income-statement-search-result';

            Export::exportTo($export_type, $data);

        }else{

            $data['title']  =   'Income Statement';
            $data['classesr'] = AccountGroup::with(['groups' => function($q){
                $q->orderBy('code', 'asc');
            }])->where('id','4')->get();
            // }])->whereBetween('id',array(3,4))->whereNull('parent_id')->get();

            // $data['classesec'] = AccountGroup::with(['groups' => function($q){
            //     $q->orderBy('code', 'asc');
            // }])->whereBetween('code',array(5100,5103))->get();

            $data['classesea'] = AccountGroup::with(['groups' => function($q){
                $q->orderBy('code', 'asc');
            }])->where('id','5')->get();

            $data['view'] = 'reports.exports.income-statement';

            Export::exportTo($export_type, $data);
        }
    }

    public function exportGLAccountSummary( $export_type )
    {
        $data['title']  =   'GL Account Summary';
        $data['classes'] = AccountGroup::with(['groups' => function($q){
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();

        $data['view'] = 'reports.exports.gl-account-summary';

        Export::exportTo($export_type, $data);
    }

    /**
     * Transaction listing
     */
    public function exportTransactionListing( $export_type ){
        $data['title'] = 'Transaction Listing';

        $data['start_date'] = '';
        $data['end_date']   = '';
        if($input = request()->all()){
            $data['start_date'] = $input['from_date'];
            $data['end_date']   = $input['to_date'];
            $data['transactions'] = Transaction::getTransactionListing(null, $input['from_date'], $input['to_date']);
        }else {
            $data['transactions'] = Transaction::getTransactionListing();
        }

        $data['view'] = 'reports.exports.transaction_listing';

        Export::exportTo($export_type, $data);
    }

    public function exportTransactionDetails($trans_id, $export_type )
    {
        $data['title'] = 'Transaction Details';

        $data['trans_id'] = $trans_id;

        $data['transaction'] = Transaction::getTransactionDetails($trans_id);

        $data['view'] = 'reports.exports.transaction_details';

        Export::exportTo($export_type, $data);
    }

    public function vendorLedgerStatement($ledger_id, $export_type){
        $data['start_date'] = '';
        $data['end_date']   = '';
        if($ledger_id) {
            $data['ledger'] = $this->ledger->where('id', $ledger_id)->where('group_id', 28)->first();
        }elseif($input = request()->all()){
            $ledger_id  = $input['ledger_id'];
            $data['start_date'] = $input['from_date'];
            $data['end_date']   = $input['to_date'];
            $data['ledger'] = $this->ledger->where('id', $ledger_id)->where('group_id', 28)->first();
        }
        $data['title'] = 'Vendor Ledger Statement';
        // $data['ledgers'] = $this->ledger->where('group_id', 28)->get();

        $data['view'] = 'reports.exports.vendor_ledger';

        Export::exportTo($export_type, $data);
    }

    public function ledgerStatement($ledger_id, $export_type){
        
        $data['start_date'] = '';
        $data['end_date']   = '';
        if($ledger_id) {
            $data['ledger'] = $this->ledger->where('id', $ledger_id)->first();            
        }elseif($input = request()->all()){
            $ledger_id  = $input['ledger_id'];
            $data['start_date'] = $input['from_date'];
            $data['end_date']   = $input['to_date'];
            $data['ledger'] = $this->ledger->where('id', $ledger_id)->first();
        }

        $data['title'] = 'Ledger Statement';

        $data['view'] = 'reports.exports.ledgerstatement';

        Export::exportTo($export_type, $data);
    }

    public function payablesAccounts($export_type){
        $data['ledgers'] = $this->ledger->where('group_id', 28)->get();
        $data['title'] = 'Payables Accounts';

        $data['view'] = 'reports.exports.payables_accounts';

        Export::exportTo($export_type, $data);
    }

    public function payablesAgeAnalysis($ledger_id, $export_type)
    {
        //$ledger_id = base64_decode($ledger_id);

        if(request()->input('from_date')){
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));

            $data['ledger'] = $this->ledger->find($ledger_id);
        }else{
            $data['ledger'] = $this->ledger->find($ledger_id);
            $data['start_date'] = '';
            $data['end_date']   = '';
        }
        $data['title'] = 'Payables Age Analysis';

        $data['view'] = 'reports.exports.payables_age_analysis';

        Export::exportTo($export_type, $data);
    }

    public function receivable($export_type)
    {
        $data['title']  =   'Receivables Accounts';

        $data['receivables'] = Customer::all();

        $data['view'] = 'reports.exports.receivables';

        Export::exportTo($export_type, $data);
    }

    public function agedReceivables($ledger_id, $export_type)
    {
        $data['title']  =   'Receivables Age Analysis';
        
        if(request()->input('from_date')){
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));

            $data['ledger'] = $this->ledger->find($ledger_id);
        }else{
            $data['ledger'] = $this->ledger->find($ledger_id);
            $data['start_date'] = '';
            $data['end_date']   = '';
        }

        $data['view'] = 'reports.exports.receivablesage';

        Export::exportTo($export_type, $data);
    }
}
