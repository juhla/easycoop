<?php

namespace App\Modules\Base\Http\Controllers;

use Illuminate\Http\Request;

class FrontPageController extends Controller
{
    

	public function welcome()
	{
		return view('frontpagefolder.welcome');
	}

	public function bmacPricing()
	{
		return view('frontpagefolder.bmacpricing');
	}

	public function bmacAccounting()
	{


        return view('frontpagefolder.bmacaccounting');
	}



}
