<?php

namespace App\Modules\Base\Http\Controllers;
use Mail;

class HelpController extends Controller{
    public function faq(){
        $data['title'] = 'Frequently Asked Questions';
        return view('faq')->with($data);
    }

    public function sendFeedback(){
        $input = request()->all();
        //pr($input);exit;
        try {
            Mail::send('emails.feedback', $input, function ($m) use ($input) {
                $m->from(auth()->user()->email, auth()->user()->displayName());
                $m->to('support@ikooba.com', 'BMAC Support Team')->subject($input['subject']);
            });
            flash()->success('Thank you for sending us your feedback');
            return redirect()->back();
        }catch (\Exception $e){
            //flash()->error($e->getMessage());
            return $e->getMessage();
        }
    }
}