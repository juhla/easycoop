<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Ledger;
use App;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Repositories\MemoRepository;
use Illuminate\Support\Facades\Session;


class MemorandumController extends Controller
{
    /**
     * Constructor function to this controller
     */
    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function postmemo()
    {
        $input = request()->all();
        //dd($input);
        MemoRepository::postPC($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('memorandum');
        }
    }

    /**
     * method to get credit purchases to be posted
     * @return $this
     */
    public function ledger()
    {

        $input = request()->all();
        $data['min_side'] = true;
        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Memorandum Ledger';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('memorandum.postledger')->with($data);

    }


    public function index()
    {
        //pr(Transaction::getMemoID());exit;
        $data['title'] = 'Memorandum';
        $data['type'] = 'inflow';
        //dd(session()->get('company_db'));
        $data['start_date'] = '';
        $data['end_date'] = '';
        if ($input = request()->all()) {
            $data['end_date'] = empty($input['end_date']) ? Session::get('end_date') : $input['end_date'];
            $data['start_date'] = empty($input['start_date']) ? Session::get('start_date') : $input['start_date'];
            Session::put('start_date', $data['start_date']);
            Session::put('end_date', $data['end_date']);
            $data['transactions'] = Transaction::getMemorandumTransactions($data['start_date'], $data['end_date']);
        } else {
            $data['transactions'] = Transaction::getMemorandumTransactions(null, null);
        }
        return view('memorandum.index')->with($data);
    }


}