<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Messenger\Message;
use App\Modules\Base\Models\Messenger\MessageId;
use Illuminate\Http\Request;

use App\Modules\Base\Http\Requests;
use Validator;

class MessengerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function index()
    {

        $result_set = Message::all();
        $data['title'] = "Messenger";
        $data['message_list'] = $result_set;
        return view('messenger.list')->with($data);
    }


    public function create()
    {
        //get collaborators
        $userId = auth()->user()->id;
        $counter = 0;
        $users = [];
        if (auth()->user()->hasRole(['professional', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
            $result_set = Collaborator::whereClient_id($userId)->get();
            foreach ($result_set as $value) {
                $users[] = $value->personalInfo;
            }
        } else {
            $result_set = Collaborator::whereUser_id($userId)->get();
                foreach ($result_set as $value) {
                    $users[] = $value->personalInfo;
                }     
        }
        //return $users;
        $data['users'] = $users;
        $data['title'] = "Messenger";
        //return $data;
        return view('messenger.create')->with($data);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $rules = array('message' => 'required',
            'subject' => 'required',
            'recipient' => 'required');

        $messages = array('required' => 'This is required');
        $validate = Validator::make($data, $rules, $messages);

        if ($validate->passes()) {
            $message = new Message;
            $message->message = $request->input('message');;
            $message->subject = $request->input('subject');;
            $message->status = 0; //0=>unseen $ 1=>seen by recipient
            $message->save();

            $messageID = new MessageId;
            $messageID->message_id = $message->id;
            $messageID->sender_id = auth()->user()->id;
            $messageID->recipient_id = $request->input('recipient');;
            $messageID->save();

            flash('Message sent', 'success');
            return redirect('/message');
        } else {
            flash('Please fill all entry', 'warning');
            return back()->withInput()
                ->withErrors($validate);
        }


    }


    public function show($id)
    {
        //test is id  is the receiver
        $user = auth()->user();
        $result_set = Message::find($id);
        $receiver_id = $result_set->messageId->recipient_id;

        if ($user->id == $receiver_id) {
            //update record as seen
            Message::whereId($id)->update(['status' => 1]);
        }
        $result_set = Message::find($id);
        $data['message'] = $result_set;
        $data['title'] = "Messenger";
        return view('messenger.show')->with($data);

    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
