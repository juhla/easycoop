<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\OsTicket\UserOS;
use App\Modules\Base\Models\OsTicket\UserData;
use App\Modules\Base\Models\OsTicket\UserEmail;
use App\Modules\Base\Models\OsTicket\UserAccount;
use App\Modules\Base\Models\OsTicket\UserEmailAccount;
use Auth;
use Entrust;
use Illuminate\Http\Request;


class OSTicketController extends Controller
{
       
    public function create()
    {
        $this->set_OsTicket();
        $supportUrl = config('constants.support_url');
        $email = base64_encode(auth()->user()->email); 
        $password = base64_encode(auth()->user()->password); 
       
        return  redirect()->away($supportUrl.'?luser='.$email.'&lpasswd='.$password);
    }

    private function set_OsTicket()
    {
        $authUser = auth()->user();
        //test  if user already ahav osTicket data
        $result = UserEmail::whereAddress($authUser->email)->get();
    
        if(!$result->isEmpty())
        {
           //Do nothing
        }
        else
        {
            $user = new UserOS;
            $user->status = 0;
            $user->name = $authUser->personal_info->first_name.' '.$authUser->personal_info->last_name;
            $user->save();

            $userEmail = new UserEmail;
            $userEmail->user_id = $user->id;
            $userEmail->flags = 0;
            $userEmail->address = $authUser->email;
            $userEmail->save();

            $userData = new UserData;
            $userData->user_id =$user->id;
            $userData->phone = $authUser->personal_info->phone_no;
            $userData->save();

            $userAccount = new UserAccount;
            $userAccount->user_id = $user->id;
            $userAccount->status = 1; //comfirm account
            $userAccount->extra = "browser_lang : en_US";
            $userAccount->passwd =$authUser->password;
            $userAccount->timezone = 'Africa/Lagos';
            $userAccount->registered = date('Y-m-d H:i:s');
            $userAccount->save();

            UserOS::whereId($user->id)->update(['org_id' => $user->id,
                                            'default_email_id' => $userEmail->id,]);
        }
    }
}
