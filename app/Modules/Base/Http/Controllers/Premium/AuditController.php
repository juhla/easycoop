<?php

namespace App\Modules\Base\Http\Controllers\Premium;

use Illuminate\Http\Request;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\Audit;
use App\Modules\Base\Models\User;
use Carbon;

class AuditController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    //
    public function index()
    {
        $logs = Audit::with('users')->paginate(10);
        return view('audit.audit-log', compact('logs'));
    }

    public function data_log($id)
    {
        $log = Audit::find($id);
        //pr($log->old_values);exit;
        $old = $log->old_values;
        //$old = json_decode($jsonold, true);
        $arr = $log->new_values;
        //$arr = json_decode($jsonString, true);


        return view('audit.data-log', compact('arr', 'log', 'old'));
    }

    public function search_date(Request $request)
    {
        $start = $request->start_date;
        $end = $request->end_date;
        try {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start);
            $end = Carbon\Carbon::createFromFormat('d/m/Y', $end);
        } catch (\Exception $exception) {
            $start = Carbon\Carbon::now();
            $end = Carbon\Carbon::now();
        }
        $present_day = $end->format('Y-m-d');

        if ($start == $end) {
            $results = Audit::whereDate('created_at', $present_day)
                ->get();
            return view('audit.ranged_data-log', ['results' => $results]);
        } else {
            $results = Audit::whereBetween('created_at', array($start, $end))->with('users')->get();
            return view('audit.ranged_data-log', compact('results'));
        }


    }
}
