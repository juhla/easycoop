<?php namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Traits\Functions;
use App\Modules\Base\Traits\NewCreates;
use Faker\Provider\sv_SE\Person;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\Company;
use Auth;
use Illuminate\Support\Facades\Session;
use Response;
use Request;
use Redirect;
use File;
use DB;
use Image;

class ProfileController extends Controller
{

    protected $errorHandler;
    protected $isError = false;
    use Functions, NewCreates;

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('influencer');
    }

    /**
     * Display the user profile page.
     *
     * @return Response
     */
    public function index($slug = false)
    {
//dd(auth()->user()->id);
        $data = $this->profilePage();
        //pr($data);exit;
        if (auth()->user()->hasRole(['admin', 'business-owner', 'employee', 'advanced-business-owner', 'basic-business-owner'])) {
            return view('profile.individual')->with($data);
        } else {
            return view('profile.influencers2')->with($data);
        }
    }

    public function viewProfile($slug, $id)
    {
        $user_id = base64_decode($id);

        //get personal info
        $data['personalInfo'] = Profile::getPersonalInfo($user_id);
        //get contact info
        $data['contactInfo'] = Profile::getContactInfo($user_id);
        //get professional info
        $data['professionalInfo'] = Profile::getProfessionalInfo($user_id);
        //get user company info
        $data['companyInfo'] = Profile::getCompanyInfo($user_id);
        //get photos
        $data['photos'] = Profile::getUserPhotos($user_id);
        $data['title'] = $data['personalInfo']->First_Name . ' ' . $data['personalInfo']->Last_Name;
        return view('profile::view-profile')->with($data);
    }

    public function viewCompany($slug, $id)
    {
        $id = base64_decode($id);
        //get user company info
        $data['companyInfo'] = Profile::getCompanyInfo('null', $id);
        $data['title'] = $data['companyInfo']->Company_Name;
        return view('profile::view-company')->with($data);
    }

    /**
     * save personal information
     * @return mixed
     */
    public function updatePersonalInfo()
    {
        //get all form inputs
        $input = Request::all();
        try {
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['dob']);
            $date = $date->format('Y-m-d');
        } catch (\Exception $ex) {
            $date = "0000-00-00";
        }

        //pr($input);
        //set data for personal_info table
        $personalInfo = array(
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'title' => $input['title'],
            'gender' => $input['gender'],
            'marital_status' => $input['marital_status'],
            'date_of_birth' => $date,
            'state' => isset($input['state']) ? $input['state'] : "",
            'city' => $input['city'],
            'country' => $input['country'],
            'bio' => $input['bio'],
            // 'interests'     =>  $input['interests'],
            //'languages'     =>  $input['languages']
        );
        //save user info
        PersonalInfo::where('user_id', Auth::user()->id)->update($personalInfo);
        if (Request::ajax()) {
            $return_arr['status'] = 1;
            $return_arr['message'] = "Your profile information has been updated successfully.";
            return Response::json($return_arr);
        } else {
            flash()->success('Your profile information has been updated successfully');
            return Redirect::back();
        }
    }


    /**
     * handles updating and creating a new company
     * @return mixed
     */
    public function updateCompanyInfo()
    {

    }

    function changeCompanyLogo(Request $request)
    {
        $input = request()->all();
        $response = $this->uploadFile($input['company_logo']);
        Company::where('user_id', getBusinessOwnerAuth()->id)->update([
            'company_logo' => $response]);
        flash()->success('Your logo updated successfully');
        return Redirect::back();
    }

    /**
     * method to change user profile pic
     * @return mixed
     */
    public function changeProfilePic()
    {

    }


    /**
     * @return mixed
     */
    public function getErrorHandler()
    {
        return $this->errorHandler;
    }

    /**
     * @param mixed $errorHandler
     */
    public function setErrorHandler($errorHandler)
    {
        $this->errorHandler = $errorHandler;
    }

    /**
     * @param $file
     * @param null $username
     * @return string
     */
    private function uploadFile($file, $username = null)
    {
        File::exists(public_path() . '/uploads/') || File::makeDirectory(public_path() . '/uploads/', $mode = 0777, true, true);
        File::exists(public_path() . '/uploads/thumbs/') || File::makeDirectory(public_path() . '/uploads/thumbs', $mode = 0777, true, true);
        //upload path
        $destinationPath = public_path() . '/uploads/';
        //thumbnail path
        $thumbnailPath = public_path() . '/uploads/thumbs/';
        //get temp file name
        $file_name = $file->getClientOriginalName();
        //get extension
        $file_ext = File::extension($file_name);
        //rename image, give the file a random name
        $filename = sha1($file_name) . '-' . time() . '.' . $file_ext;

        try {
            //move original image
            $upload_success = $file->move($destinationPath, $filename);

            if ($upload_success && $file_ext === 'jpg' || $file_ext === 'png' || $file_ext === 'JPG' || $file_ext === 'jpeg') {
                // resizing an uploaded file
                Image::make($destinationPath . $filename)->fit(194, 200)->save($thumbnailPath . "thumb_" . $filename);
            }
            return $filename;
        } catch (\Exception $e) {
            $this->isError = true;
            $this->setErrorHandler($e->getMessage());
            return $e->getMessage();
        }
    }
}
