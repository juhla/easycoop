<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Ticket;
use App\Modules\Base\Models\Department;
use App\Modules\Base\Models\TicketType;
use App\Modules\Base\Models\ServiceTime;
use App\Modules\Base\Models\TicketResponse;
use App\Modules\Base\Models\User;
use App\Modules\Base\Classes\Helper;
use Validator;
use DB;
use File;
use Activity;
use Auth;
use Entrust;
use Illuminate\Http\Request;
use App\Modules\Base\Http\Requests\TicketRequest;

Class TicketController extends Controller
{

    protected $form = 'ticket-form';

    public function index(Ticket $ticket, Request $request)
    {

        $query = $ticket::with('assignedUser')
            ->whereHas('assignedUser', function ($query) {
                $query->where('user_id', '=', Auth::user()->id);
            });


        $query = $ticket::where('user_id', '=', Auth::user()->id);

        $filter_data = [
            'start_date' => $request->input('start_date'),
            'end_date' => $request->input('end_date'),
            'assigned' => $request->input('assigned'),
            'department_id' => $request->input('department_id'),
            'ticket_type_id' => $request->input('ticket_type_id'),
            'ticket_priority' => $request->input('ticket_priority'),
            'ticket_status' => $request->input('ticket_status'),
            'user_id' => $request->input('user_id'),
            'response_time_overdue' => $request->input('response_time_overdue'),
            'resolution_time_overdue' => $request->input('resolution_time_overdue'),
        ];

        $tickets = $query->get();

        $col_data = array();
        $col_heads = array(
            'Option',
            'Ticket #',
            'Email',
            'Subject',
            'Priority',
            'Status');

        $col_heads = Helper::putCustomHeads($this->form, $col_heads);
        $col_ids = Helper::getCustomColId($this->form);
        $values = Helper::fetchCustomValues($this->form);
        //pr($tickets);exit;

        foreach ($tickets as $ticket) {

            $cols = array(
                '<div class="btn-group btn-group-xs">' .
//                '<a href="/ticket/' . $ticket->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" title="View"> <i class="fa fa-share"></i></a> ' .
//                '<a href="/ticket/' . $ticket->id . '/edit" class="btn btn-default btn-xs" data-toggle="tooltip" title="Edit"> <i class="fa fa-edit"></i></a> ' .
                Helper::delete_form(['ticket.destroy', $ticket->id]) . '</div>',
                $ticket->ticket_no,
                $ticket->User->email,
                $ticket->ticket_subject,
                Helper::showTicketPriority($ticket->ticket_priority),
                Helper::showTicketStatus($ticket->ticket_status)
            );
            $id = $ticket->id;

            foreach ($col_ids as $col_id)
                array_push($cols, isset($values[$id][$col_id]) ? $values[$id][$col_id] : '');
            $col_data[] = $cols;
        }
        //pr($col_data);exit;

        $departments = Department::pluck('department_name', 'id')->all();
        $ticket_types = TicketType::pluck('ticket_type_name', 'id')->all();
        Helper::writeResult($col_data);

        $users = User::with('roles')
            ->whereHas('roles', function ($query) {
                $query->where('roles.name', '!=', 'user');
            })->pluck('email', 'id')->all();

        return view('ticket.list', compact(
            'col_heads',
            'col_data',
            'departments',
            'ticket_types',
            'filter_data',
            'users'
        ));
    }

    public function create()
    {
        $departments = Department::pluck('department_name', 'id')->all();
        $ticket_types = TicketType::pluck('ticket_type_name', 'id')->all();
        $priorities = config('list.priority');

        return view('ticket.add', compact('departments', 'ticket_types', 'priorities'));
    }

    public function store(TicketRequest $request, Ticket $ticket)
    {


        $data = $request->all();
        $ticket->fill($data);
        $ticket->ticket_status = 'open';
        $ticket->user_id = Auth::user()->id;

        $service_time = Helper::getServiceTime($ticket->ticket_priority);

        if ($service_time['resolution_time_type'] == 'business_hour')
            $ticket->resolution_due_time = Helper::calculateDueTime($service_time['resolution_time'], date('Y-m-d H:i'));
        else
            $ticket->resolution_due_time = date('Y-m-d H:i', (($service_time['resolution_time'] * 60) + strtotime(date('Y-m-d H:i'))));

        if ($service_time['response_time_type'] == 'business_hour')
            $ticket->response_due_time = Helper::calculateDueTime($service_time['response_time'], date('Y-m-d H:i'));
        else
            $ticket->response_due_time = date('Y-m-d H:i', (($service_time['response_time'] * 60) + strtotime(date('Y-m-d H:i'))));

        $max_ticket_no = Ticket::max('ticket_no');
        $next_ticket_no = config('config.next_ticket_no');

        if ($max_ticket_no >= $next_ticket_no)
            $ticket->ticket_no = ++$max_ticket_no;
        else
            $ticket->ticket_no = $next_ticket_no;

        $ticket->save();

        Helper::storeCustomField($this->form, $ticket->id, $data);

        // return redirect('/view-ticket/'.$ticket->id)->withSuccess(config('constants.ADDED'));
        return redirect('ticket/list')->withSuccess(config('constants.ADDED'));

    }

    public function destroy($ticket)
    {
        if ($ticket) {
            //delete bank
            Ticket::where('id', $ticket)->delete();
        }
        return redirect('ticket/list')->withSuccess(config('constants.DELETED'));
    }


}

?>