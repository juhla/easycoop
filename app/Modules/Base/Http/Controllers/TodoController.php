<?php

namespace App\Modules\Base\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Base\Models\Messenger\Todo;

use App\Modules\Base\Http\Requests;
use Mockery\CountValidator\Exception;
use Validator;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);
    }
    public function index()
    {

        try {
            $result_set = Todo::all();
           // dd();
            $data['title'] = "To Dos";
            $data['todo_list'] = $result_set;
            return view('todo.list')->with($data);
        }catch (\Exception $e){
            return redirect('/dashboard');
        }
    }

    public function store(Request $request)
    {
        $data =  $request->all();
        $rules = array('todo' => 'required');

        $messages = array('required' => 'This is required');
        $validate = Validator::make($data, $rules, $messages);

        if( $validate->passes()) {
            $message = new Todo;
            $message->todo = $request->input('todo');
            $message->status =0; //0=>pending $ 1=>done
            $message->save();

            flash('To do saved', 'success');
            return redirect('/todo');
        }
        else{
            flash('Enter a Todo item', 'warning');
            return back()->withInput()
                ->withErrors($validate);
        }
    }

    public function update(Request $request, $id)
    {
        //update record as seen
        Todo::whereId($id)->update(['status'=> 1]);
        flash('To do task completed', 'success');
        return redirect('todo');
    }


    public function delete($id)
    {
        Todo::whereId($id)->delete();
        flash('To do has been removed', 'success');
        return redirect('todo');
    }
}
