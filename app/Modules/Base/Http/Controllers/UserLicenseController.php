<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Models\Payment;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Models\UserLicense;
use App\Modules\Base\Traits\License;
use Illuminate\Support\Facades\Auth;
use Mail;

class UserLicenseController extends Controller
{

    use License;

    public function __construct()
    {
        $this->middleware(['tenant']);

    }

    public function index()
    {
        //get user role
        $userLicense = new UserLicense();
        $userLicenceSubscriber = new UserLicenceSubscriber();
        UserLicenceSubscriber::updateTheCount(getBusinessOwnerID());
        $role = getUserRole(auth()->user()->id);
        $hasSubscribed = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();

        if (empty($hasSubscribed)) {
            $theLicense = $userLicense->findDefaultLicenceByRole($role->id);
            $userLicenceSubscriber->user_id = auth()->user()->id;
            $userLicenceSubscriber->license_id = $theLicense->id;
            $license_id = $theLicense->id;
            $userLicenceSubscriber->save();
        } else {
            $license_id = $hasSubscribed->license_id;
        }
        $plan = $userLicense->find($license_id);
        // $clientss = Collaborator::where('user_id', auth()->user()->id)->where('flag', 'Active')->get();

        $userID = getBusinessOwnerID();
        //  "employee-payroll", "admin-payroll", "employee-ims"
        $clients = Collaborator::where('user_id', $userID)->where('flag', 'Active')->groupBy('user_id', 'client_id')->
            whereHas('client.roles', function ($q) {
            $q->whereNotIn('name', ["employee-payroll", "admin-payroll", "employee-ims"]);
        })->with('client')
            ->get();

        //dd($clients);

        $subscriber = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();
//pr(auth()->user()->displayName());exit;
        return view('settings.user-licenses', compact('clients', 'plan', 'subscriber'));
    }

    public function membership_plans()
    {

        $this->createTables();
        $title = 'Change Renewal Plan';

        try {
            $bmac = true;
            $bmac_subscriber = UserLicenceSubscriber::with('license')
                ->where('user_id', auth()->user()->id)
                ->first();
            if (empty($bmac_subscriber)) {
                $bmac = false;
            }

            $__bmac_expiry_date = date('l jS \of F Y', strtotime($bmac_subscriber->expiry_date));

        } catch (\Exception $ex) {
            $bmac = false;
        }

        try {
            $payroll = true;
            $payroll_subscriber = PayrollLicenceSubscriber::with('license')
                ->where('user_id', auth()->user()->id)
                ->first();
            if (empty($payroll_subscriber)) {
                $payroll = false;
            }
            $__payroll_expiry_date = date('l jS \of F Y', strtotime($payroll_subscriber->expiry_date));
        } catch (\Exception $ex) {
            $payroll = false;
        }

        try {
            $ims = true;
            $ims_subscriber = ImsLicenceSubscriber::with('license')
                ->where('user_id', auth()->user()->id)
                ->first();
            if (empty($ims_subscriber)) {
                $ims = false;
            }
            $__ims_expiry_date = date('l jS \of F Y', strtotime($ims_subscriber->expiry_date));
        } catch (\Exception $ex) {
            $ims = false;
        }

        return view('settings.membership_plans', compact('__ims_expiry_date', '__payroll_expiry_date', '__bmac_expiry_date', 'ims', 'ims_subscriber', 'bmac', 'payroll', 'bmac_subscriber', 'payroll_subscriber', 'title'));
    }

    public function plans()
    {
        $this->createTables();
        $title = 'Change Plan';
        //get user role
        $role = getUserRole(auth()->user()->id);
        //get plans for role
        $roleid = Role::findRoleCategory($role->id);
        $bmac_license_plans = UserLicense::where('role_id', $roleid)->where('type_id', 4)->get();
        $payroll_license_plans = UserLicense::where('role_id', $roleid)->where('type_id', 7)
            ->where('id', '!=', 18)
            ->orderBy('id', 'ASC')
            ->get();
        $ims_license_plans = UserLicense::where('id', '=', 12)->orWhere('id', '=', 13)
        // where('role_id', $roleid)->where('type_id', 1)
        // ->where('id', '!=', 8)
            ->get();

        // dd($ims_license_plans);
        $subscriber = UserLicenceSubscriber::where('user_id', auth()->user()->id)->first();
        $payroll_subscriber = PayrollLicenceSubscriber::where('user_id', auth()->user()->id)->first();
        $ims_subscriber = ImsLicenceSubscriber::where('user_id', auth()->user()->id)->first();
        //dd($payroll_license_plans);

        $license_id = empty($subscriber->license_id) ? 0 : $subscriber->license_id;
        $payroll_subscriber_id = empty($payroll_subscriber->license_id) ? 0 : $payroll_subscriber->license_id;
        $ims_subscriber_id = empty($ims_subscriber->license_id) ? 0 : $ims_subscriber->license_id;

        return view('settings.plans', compact('ims_subscriber_id', 'payroll_subscriber_id', 'subscriber', 'bmac_license_plans', 'ims_license_plans', 'payroll_license_plans', 'license_id', 'title'));
    }

    public function makePayment($plan_id)
    {
        return view('settings.payment');
    }

    private function sendMails($plan)
    {
        try {
            $data['name'] = auth()->user()->company->company_name;
            $data['reference_no'] = request()->get('reference_no');
            $data['package'] = $plan->title;
            $data['amount'] = $plan->price;
            $data['email'] = auth()->user()->email;

            Mail::send('emails.receipt', $data, function ($m) use ($data) {
                $m->from('no-reply@onebmac.com', 'BMAC Team');

                $m->to($data['email'], $data['name'])->subject($data['name'] . ' thank you for your order');
            });

            flash()->success('User license plan has been changed');
            return redirect('membership/user-licenses');
        } catch (\Exception $e) {
            flash()->error($e->getMessage());
            return redirect('membership/user-licenses');
        }

    }

    public function subscribe_payroll($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = PayrollLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        $license->expiry_date = $this->getExpiryDate($plan_id, $license->expiry_date);
        $license->save();

        //$this->renewTheLicense($license);

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);
    }

    public function subscribe_ims($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = ImsLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        $license->expiry_date = $this->getExpiryDate($plan_id, $license->expiry_date);
        $license->save();

        //$this->renewTheLicense($license);

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }

    public function subscribe($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = UserLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        $license->expiry_date = $this->getExpiryDate($plan_id, $license->expiry_date);
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }

    public function renewal_ims($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = ImsLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        //$license->expiry_date = date('Y-m-d', strtotime("+1 years", strtotime($license->expiry_date)));
        $license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }

    public function renewal_payroll($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = PayrollLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        $license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }

    public function renewal_bmac($plan_id)
    {
        $plan = UserLicense::find($plan_id);

        $license = UserLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        $license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->reference_no = request()->get('reference_no');
        $payment->save();

        return $this->sendMails($plan);

    }
}
