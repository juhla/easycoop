<?php

namespace App\Modules\Base\Http\Controllers;

use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\Country;
use App\Modules\Base\Models\User;
use Illuminate\Http\Request;
use Auth;
use App\Modules\Base\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Modules\Base\Traits\Audit;
use Zizaco\Entrust\EntrustFacade as Entrust;

class setupController extends Controller
{
      use Audit;
    public function __construct(Request $request)
    {
        $this->middleware(['auth']);
        $this->request = $request;
    }


    public function businessOwnerSetUp()
    {
        $this->handleTrail();
        $user = auth()->user();

        $countries = getCountry();
        $industries = DB::table('industries')->orderBy('name', 'asc')->get();
        $countryCurrencies = Country::where('currency_symbol', '!=', null)->get();

        if ($user->is_first_login == 0) {
            User::find($user->id)->update(['is_first_login' => '1']);
        }
        $data = [
            'industries' => $industries,
            'countries' => $countries,
            'countryCurrencies' => $countryCurrencies
        ];

        return view('user_setup', $data);
    }

    public function getCountryInfo($id)
    {
        $name = getCountryInfo($id);
        return $name;
    }

    public function businessOwnerSetupPost()
    {
        $this->handleTrail();
        $input = Input::all();
        //dd(auth()->user()->id);
        $user = User::find(auth()->user()->id);
        auth()->user()->detachRoles(auth()->user()->roles);
        $user->roles()->attach($input['user_level']);
        Company::where('user_id', auth()->user()->id)->update(
                    ['industry_no' => $input['typeofbusiness'],
                    'country' => $input['country'],
                    'currency' => $input['typeofcurrency'],
                    'address' => $input['businessaddress']]);
        return redirect('/workspace');
    }

    public function acctSetUp()
    {
        //dd('d');
//        $user = auth()->user();
//        if($user->is_first_login == 1){
//            return redirect('/workspace');
//        }
        return view('acct_setup');
    }

    public function acctSetupPost()
    {
        $this->handleTrail();
        User::find(auth()->user()->id)->update(['is_first_login' => '1']);
        $input = Input::all();
        Company::create(['company_name' => $input['company_name'], 'phone_no' => $input['phone_number']]);
        return redirect()->to('profile/' . strtolower(auth()->user()->displayName()));
    }

}
