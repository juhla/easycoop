<?php

namespace App\Modules\Base\Http\Middleware;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Permission;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Traits\License;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AfterLogin
{
    use License;


    private $isError;
    private $errorMessage;
    private $redirectURL;

    /**
     * @return mixed
     */
    public function getisError()
    {
        return $this->isError;
    }

    /**
     * @param mixed $isError
     */
    public function setIsError($isError)
    {
        $this->isError = $isError;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return mixed
     */
    public function getRedirectURL()
    {
        return $this->redirectURL;
    }

    /**
     * @param mixed $redirectURL
     */
    public function setRedirectURL($redirectURL)
    {
        $this->redirectURL = $redirectURL;

    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        //dd(\auth()->check());
        if (Auth::guard($guard)->check()) {
            $this->doCheck();
            return $this->doRedirect();
        }
        return $next($request);
    }


    function doRedirect()
    {
        if ($this->getisError()) {
            return redirect()->to($this->getRedirectURL())
                ->withErrors($this->getErrorMessage())
                ->withInput();
        }
        return redirect($this->getRedirectURL());
    }

    function licenseChecks()
    {

        $user = auth()->user();
        //check if the account is active
        if (!$user->isVerified()) {
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('Your Account has not been verified. Please follow the link in the mail that was sent to you');
            $this->setRedirectURL('/secure/sign-in');
            return;
        } elseif (!$user->isActive()) {
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('Your Account has been deactivated. Kindly send an email to support@ikooba.com to reactivate your account');
            $this->setRedirectURL('/secure/sign-in');
            return;

        } elseif ($user->banned === 1) {
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('You have been banned from accessing this website');
            $this->setRedirectURL('/secure/sign-in');
            return;


        } elseif ($user->hasRole('social-member')) {
            //if user is social member
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('You don\'t have access to this area.');
            $this->setRedirectURL('/secure/sign-in');
            return;


        } elseif (!$this->updateAnnual()) {
            //if user Maintenance license has expired
            //logout the user
            auth()->logout();
            $this->setIsError(true);
            $this->setErrorMessage('Your company\'s License Package has expired!');
            $this->setRedirectURL('/secure/sign-in');
            return;
        }
    }

    function doCheck()
    {

        $this->licenseChecks();
        $user = auth()->user();
        session()->forget('company_db');
        //session()->forget('company_name');
        $today = Carbon::today();

        $roleCounter = 0;
        if ($user->hasRole('employee')) {
            $roleCounter++;
        }
        if ($user->hasRole('employee-ims')) {
            $roleCounter++;
        }
        if ($user->hasRole('employee-payroll')) {
            $roleCounter++;
        }


        Session::put('userhash', session()->getId());
        $userNoAudit = User::find($user->id);
        $userNoAudit->user_hash = session()->getId();
        $userNoAudit->online_status = 1;
        $userNoAudit->save();

        if ($roleCounter > 1) {


            if (auth()->user()->hasRole('employee')) {
                $collaborator = Collaborator::where('client_id', auth()->user()->id)->first();
                $access = $collaborator->access;
                $license = UserLicenceSubscriber::where('user_id', $collaborator->user_id)->first();
                if (($license->expiry_date < $today->toDateString()) && ($access != 'Free')) {
                    //logout the user
                    auth()->logout();
                    $this->setIsError(true);
                    $this->setErrorMessage("Your company's License Package has expired!");
                    $this->setRedirectURL('/secure/sign-in');
                    return;
                }

            }

            $this->setIsError(false);
            $this->setRedirectURL('bmac/premium');
            return;
        }

        if ($user->hasRole('employee-payroll') || $user->hasRole('admin-payroll')) {

            $this->setIsError(false);
            $this->setRedirectURL('payroll/login');
            return;

        }
        if ($user->hasRole('employee-ims')) {

            $this->setIsError(false);
            $this->setRedirectURL('ims/login');
            return;
        }
        if ($user->hasRole('employee')) {

            $checkRole = Role::where("name", $user->email)->first();
            if (!isset($checkRole)) {
                $role = Role::create(["name" => $user->email]);
                $user->roles()->attach($role->id); // id only
                $permissions = Permission::where('id', '>', 36)->get();
                $role->attachPermissions($permissions);
            }


            $collaborator = Collaborator::where('client_id', auth()->user()->id)->first();
            $access = $collaborator->access;
            $license = UserLicenceSubscriber::where('user_id', $collaborator->user_id)->first();
            if (($license->expiry_date < $today->toDateString()) && ($access != 'Free')) {
                auth()->logout();
                $this->setIsError(true);
                $this->setErrorMessage("Your company's License Package has expired!");
                $this->setRedirectURL('/secure/sign-in');
                return;
            }

        }


        //if all is well, proceed

        // First time users, take them through walkthrough
        if ($user->is_first_login == '0') {
            if ($user->hasRole(['business-owner', 'basic-business-owner', 'advanced-business-owner'])) {
                $this->setIsError(false);
                $this->setRedirectURL('business-owner-setup');
                return;
            }
            if ($user->hasRole('professional')) {
                $collaborator = Collaborator::where('client_id', auth()->user()->id)->get()->toArray();
                if (!empty($collaborator)) {
                    $this->setIsError(false);
                    $this->setRedirectURL('acct-setup');
                    return;
                }
            }
        }

        if ($user->hasRole(['agent-finance-provider', 'finance-provider', 'professional', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
            $this->setIsError(false);
            $this->setRedirectURL('profile/' . strtolower($user->displayName()));
            return;
        }
        $oauth = session()->get("ApOauth");
        session()->put("company_name", $oauth['decoded']->cooperative_name);
        $this->setIsError(false);
        $this->setRedirectURL('workspace');
        return;

    }
}
