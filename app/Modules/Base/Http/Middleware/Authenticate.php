<?php

namespace App\Modules\Base\Http\Middleware;

use App\Modules\Api\Traits\Errors;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class Authenticate
{
    use Errors;

    //use Session;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson() || $request->is('api/*')) {
                return $this->sendError("Unauthorized", 401);
            } else {
                return redirect()->guest('secure/sign-in')
                    ->withErrors('You are not allowed to view that page')
                    ->withInput();
            }
        }

        $userhash = Session::get('userhash');
        $sessionId = Session::getId();
        if (!auth()->guest() && auth()->user()->user_hash != $userhash) {
            Session::getHandler()->destroy($sessionId);
            try {
                $user = auth()->user();
                if (!empty($user)) {
                    if (!($user->hasRole(['finance-provider', 'agent-finance-provider', 'professional', 'trade-promoter']))) {
                        Auth::logout();
                        Auth::guard()->logout();
                    }
                }

            } catch (\Exception $ex) {

            }
            return redirect()->refresh();
        }

        return $next($request);
    }
}
