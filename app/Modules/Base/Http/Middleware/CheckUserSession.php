<?php

namespace App\Modules\Base\Http\Middleware;

use Closure;

class CheckUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userhash   = \Session::get('userhash');
        $sessionId = \Session::getId();
        if (!auth()->guest() && auth()->user()->user_hash != $userhash) {
            \Session::getHandler()->destroy($sessionId);
            \Auth::logout();
            return redirect()->refresh();
        }

        return $next($request);
    }
}
