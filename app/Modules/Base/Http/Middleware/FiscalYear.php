<?php

namespace App\Modules\Base\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Traits\TenantConfig;

class FiscalYear
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $fiscal_year = \App\Modules\Base\Models\FiscalYear::getCurrentFiscalYear();
        if (empty($fiscal_year)) {
            flash()->error('You need to first set a fiscal year');
            return redirect('setup/fiscal-years');
        }
        return $next($request);
    }


}
