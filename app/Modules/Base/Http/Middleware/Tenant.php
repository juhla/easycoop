<?php

namespace App\Modules\Base\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use Config;
use App\Modules\Base\Traits\TenantConfig;

class Tenant
{
    use TenantConfig;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session = session('company_db');
        if (Auth::check()) {
            $user = Auth::user();
            if (empty($session)) {

                session()->put('company_db', $user->db);
                //$this->setConfig(session()->get('company_db'));
                app()->makeWith('setDbConnection', ['db' => session()->get('company_db')]);
            } else {
                //$this->setConfig(session()->get('company_db'));
                app()->makeWith('setDbConnection', ['db' => session()->get('company_db')]);
            }
        } else {
            return redirect()->back();
        }

        return $next($request);
    }


}
