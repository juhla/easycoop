<?php

namespace App\Modules\Base\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyBusinessAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user->hasRole(['business-owner', 'basic-business-owner', 'advanced-business-owner'])) {
            return redirect()->intended('/workspace');
        }
        return $next($request);
    }
}
