<?php

namespace App\Modules\Base\Http\Middleware;

use App\Modules\Base\Traits\License;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyLicense
{
    use License;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
