<?php
/**
 * Created by PhpStorm.
 * User: baddy
 * Date: 20/12/2017
 * Time: 16:19
 */

namespace App\Modules\Base\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyProfessionalAgent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user->hasRole(['assemblyline-professional'])) {
            return redirect()->intended('/workspace');
        }
        return $next($request);
    }
}