<?php

namespace App\Modules\Base\Jobs;

use Illuminate\Support\Facades\Artisan;

class MenuJob extends UpgradeJob
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function run()
    {
        $this->runMigrations();
        // Your data migration code. Have Fun!
    }

    private function runMigrations()
    {
        try {
            Artisan::call('cache:clear');
            Artisan::call('clear-compiled');
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-bmac',
                '--force' => true
            ]);
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-ims',
                '--force' => true
            ]);
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-payroll',
                '--force' => true
            ]);

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

}
