<?php

namespace App\Modules\Base\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessSales implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            ini_set('memory_limit', '-1');
            $all = \App\Modules\Base\Models\Sale::all();

            foreach ($all as $each) {
                $sales = $each->sales_date;
                if (strpos($sales, '/') !== false) {
                    try {
                        $date = Carbon::createFromFormat('Y-m-d', $sales);
                        $date = $date->format('Y-m-d');
                    } catch (\Exception $exception) {
                        $date = Carbon::createFromFormat('Y/m/d', $sales);
                        $date = $date->format('Y-m-d');
                    } catch (\Throwable $exception) {
                        $date = Carbon::createFromFormat('Y/m/d', $sales);
                        $date = $date->format('Y-m-d');
                    }
                    $each->sales_date = $date;
                    $each->save();
                }

            }
        } catch (\Exception $exception) {
            //dd($exception->getMessage());
            app('sentry')->captureException($exception);
        }
    }
}
