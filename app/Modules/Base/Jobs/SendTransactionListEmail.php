<?php

namespace App\Modules\Base\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;

class SendTransactionListEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;
    protected $db;
    public $tries = 5;
    public $timeout = 120;

    public function __construct($data, $db)
    {
        $this->data = $data;
        $this->db = $db;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app()->makeWith('setDbConnection', ['db' => $this->db]);
        Artisan::call('report:transactionList', [
            'data' => $this->data,
            'db' => $this->db,
        ]);
    }
}
