<?php

namespace App\Modules\Base\Jobs;

use App\Modules\Base\Models\Company;
use Illuminate\Support\Facades\DB;
use Throwable;


abstract class UpgradeJob
{
    /**
     * Make sure that the job can only be executed once.
     *
     * @var int
     */
    public $tries = 1;
    /**
     * @var Company
     */
    protected $customer;

    /**
     * Create a new job instance.
     *
     * @param Company $customer
     */
    public function __construct(Company $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Performs the script upgrade on a customer wrapped in a database transaction.
     * In case of error, automatically rolls back and re-throws the exception.
     *
     * @throws Throwable
     */
    public function handle()
    {
        // Establish a database connection with the provided customer (Tenant).
        // $this->customer->connect();

        DB::reconnect('tenant_conn');
        try {
            //DB::connection('tenant_conn')->beginTransaction();

            $this->run();
           // DB::connection('tenant_conn')->commit();
        } catch (Throwable $t) {

            //DB::connection('tenant_conn')->rollback();
            throw $t;
        }
    }

    abstract protected function run();
}