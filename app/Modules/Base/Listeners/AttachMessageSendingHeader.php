<?php

namespace App\Modules\Base\Listeners;

use App\Modules\Base\Events;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttachMessageSendingHeader
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $headers = $event->message->getHeaders();
        $event->message->addBcc(env('BCC_EMAIL', 'developer@ikooba.com'));
        $event->message->addBcc('signup@ikooba.com');
        $headers->addTextHeader('X-SES-CONFIGURATION-SET', 'signup');
    }
}
