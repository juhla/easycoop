<?php

namespace App\Modules\Base\Listeners;

use App\Modules\Base\Models\Company;
use App\Modules\Base\Traits\AfterLogin;
use DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;

class PatchDatabase
{
    use AfterLogin;
    public function handle($event)
    {
        //try to check if the company database session exist first
        $session = session('company_db');

        //dd($session);
        if (!empty($session)) {
            $this->setConfig($session);
            try {
                //Artisan::call('optimize');
                Artisan::call('module:optimize');
                Artisan::call('module:migrate', [
                    'slug' => 'auth',
                    '--database' => 'tenant_conn',
                    '--force' => true
                ]);
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        }
    }

}
