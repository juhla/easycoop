<?php

namespace App\Modules\Base\Mail;

use App\Modules\Base\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CheckupMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $userID;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'no-reply@bmac.com';
        $name = 'BMAC Care';
        $subject = 'Hope you are enjoying BMAC';

        return $this->view('emails.checkupmail')
            ->from($address, $name)
            ->subject($subject)
            ->with(['user' => @$this->user]);

    }
}
