<?php

namespace App\Modules\Base\Mail;

use App\Modules\Base\Models\Customer;
use App\Modules\Invoices\Models\Invoice;
use App\Modules\Invoices\Models\InvoiceItem;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvoice extends Mailable
{
    use Queueable, SerializesModels;

    protected $invoiceID;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoiceID)
    {
        $this->invoiceID = $invoiceID;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = $this->invoiceID;
        $invoice = Invoice::where('id', $id)->first();
        $invoiceDet = InvoiceItem::where('invoice_id', $id)->get();
        $customerIdentity = Customer::where('id', $invoice->customer_id)->first();


        //set page title
        $data['title'] = 'Invoice ' . $invoice->invoice_no;
        //pass invoice object to page
        $data['invoice'] = $invoice;        //pass company's invoice settings to page
        $data['invoiceDet'] = $invoiceDet;
        $data['customerIdentity'] = $customerIdentity;

        $theName = GenerateRandomString("10", "ALPHA");
        $theInvoice = public_path() . DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR;
        $thePdfFile = $theInvoice . $theName . '.pdf';

        return $this->view('pdfgenerate.invoice')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($invoice->customer->email)
            //->to('rylxes@gmail.com')
            ->attach($thePdfFile)
            ->subject('Invoice');
    }
}
