<?php

namespace App\Modules\Base\Mail;

use App\Modules\Base\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class twentyDaysEmailNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'no-reply@bmac.com';
        $name = 'BMAC Care';
        $subject = 'BMAC 30-days trial';

        return $this->view('emails.twentydays')
                    ->from($address, $name)
                    ->subject($subject)
                    ->with(['user' => $this->user]);
    
    }
}
