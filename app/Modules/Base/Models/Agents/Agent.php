<?php

namespace App\Modules\Base\Models\Agents;



// App\Modules\Base
use App\Modules\Base\Models\Model;
use App\Modules\Base\Models\User;
use DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Agent extends Model
{
   use NullingDB;
    //set db table
    protected $connection = 'mysql';
    protected $table = 'agents';

    //mass assignment fields
    protected $fillable = ['parent_id', 'child_id'];

    //parent_id == influnecer_id
    //child_id == agent_id

    public function child()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'child_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'parent_id');
    }

    public function isUserMappedtoAgent($userID, $agentID)
    {
        return Agent::where('parent_id', $userID)
            ->where('child_id', $agentID)
            ->get()
            ->toArray();

    }

    public function getAgent($parentID)
    {
        $user = User::where('agents.parent_id', $parentID)
            ->where('users.flag', 'Active')
            ->join('agents', 'agents.child_id', '=', 'users.id')
            ->join('personal_info', 'personal_info.user_id', '=', 'users.id')
            ->get();
        return $user;
    }

    public function getAgentSmes($parentID){
        $agents = Agent::whereRaw("parent_id = ?", [$parentID])
            ->join('users', 'users.id', '=', 'agents.child_id')->get();
        $agentBusinessList = [];
        foreach ($agents as $agent){
            $user = User::where('users.flag', 'Active')
                ->where('agent_smes.agent_id', '=', $agent->child_id)
                ->join('agent_smes', 'agent_smes.sme_id', '=', 'users.id')
                ->join('personal_info', 'personal_info.user_id', '=', 'users.id')
                ->join('companies', 'companies.user_id', '=', 'users.id')
                ->get();
            $user->agentEmail= $agent->email;
            $agentBusinessList[] = $user;
        }
        return $agentBusinessList;
    }


    public function getAgentList($parentID)
    {
        $user = User::where('agents.parent_id', $parentID)
            ->where('users.flag', 'Active')
            ->join('agents', 'agents.child_id', '=', 'users.id')
            ->join('personal_info', 'personal_info.user_id', '=', 'users.id')
            ->select('agents.child_id as id', DB::raw('CONCAT(personal_info.first_name," ",personal_info.last_name) as agent'))
            ->get();
        return $user;
    }
    
     public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

}
