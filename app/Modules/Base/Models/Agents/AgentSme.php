<?php

namespace App\Modules\Base\Models\Agents;

use App\Modules\Base\Models\User;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class AgentSme extends Model 
{
    use NullingDB;
    //set db table
    protected $connection = 'mysql';
    protected $table = 'agent_smes';
    //mass assignment fields
    protected $fillable = ['agent_id', 'sme_id'];

    public function agent()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'agent_id');
    }

    public function sme()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'sme_id');
    }



    public function getAgentBusiness($agentID)
    {
        //$agentID = 162;
        $user = User::where('agent_smes.agent_id', $agentID)
            ->where('users.flag', 'Active')
            ->join('agent_smes', 'agent_smes.sme_id', '=', 'users.id')
            ->join('personal_info', 'personal_info.user_id', '=', 'users.id')
            ->join('companies', 'companies.user_id', '=', 'users.id')
            ->get();
        return $user;
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
