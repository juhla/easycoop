<?php

namespace App\Modules\Base\Models;

use Auth;
use App\Modules\Base\Traits\AfterLogin;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use App\Modules\Base\Traits\Tenant;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;

class Approval extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable, AfterLogin;
    protected $connection = 'tenant_conn';
    protected $table = 'ca_settings';

    protected $fillable = [
        'approval',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        //$this->handleSession();

        //dd(session('company_db'));
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

}
