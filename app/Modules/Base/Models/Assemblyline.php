<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;



class Assemblyline extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $fillable = ['payer_id', 'amount', 'license_type', 'expiry_date', 'authorization_code', 'status'];


    public function business()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'payer_id');
    }

    public static function isExist()
    {
        try {
            Assemblyline::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
