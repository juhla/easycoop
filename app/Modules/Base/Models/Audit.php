<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Audit extends Model
{
    use NullingDB;
    //
    protected $connection = 'tenant_conn';
    protected $table = 'audits';

    protected $guarded = [];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    public function users()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'user_id');
    }

    public function getTableName()
    {
        return $this->table;
    }


    public static function isExist()
    {
        try {
            Audit::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    
}
