<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;

class BankAC extends Model implements AuditableContract
{
    use SoftDeletes;
    use NullingDB;
    use Auditable;
    protected $connection = 'tenant_conn';
    //
    protected $table = 'ca_bank_accounts';
    protected $attributes = [
        'flag' => 'Active',
    ];

    protected $fillable = ['ledger_code', 'account_name', 'account_number', 'account_type', 'bank', 'currency_code', 'is_default', 'flag'];

    public function gl_account()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'ledger_id');
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getBankBalances()
    {
        $banks = parent::all();
        $dr = 0;
        $cr = 0;
        $balance = 0;

        $bank_account[] = null;

        foreach ($banks as $bank) {
            foreach ($bank->gl_account->transactionItem as $item) {
                if ($item->dc === 'D') {
                    $dr += $item->amount;
                } else {
                    $cr += $item->amount;
                }
            }
            $balance = ($dr - $cr);
        }
    }
}
