<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\DebitCredit;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use Request;
use Response;
use Carbon;
use Illuminate\Support\Facades\DB;
use Auth;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Batch extends Model implements AuditableContract
{
    use NullingDB, DebitCredit;
    use Auditable;
    protected $connection = 'tenant_conn';

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }


    public static function getTransaction($item_id = null, $start_date = null, $end_date = null)
    {
        if ($item_id) {
            $res = Transaction::where('id', $item_id)
                ->first();
        } elseif (!is_null($start_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            if (is_null($end_date)) {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            } else {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            }
            $res = Transaction::with('item')->where('transaction_type', 'batch-posting')
                ->where('flag', 'Active')
                ->whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::with('item')->whereDate('created_at', '=', date('Y-m-d'))
                ->where('flag', 'Active')
                ->where('transaction_type', 'batch-posting')
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }


    public function updateTransaction($input)
    {
        $res = Carbon\Carbon::createFromFormat('d/m/Y', $input['transaction_date']);
        $tot_amt = 0;
        try {
            for ($i = 0; $i < count($input['ledger']); $i++) {
                $tot_amt += $input['debit'][$i];
            }
            $transactions = [
                'transaction_date' => $res->format('Y-m-d'),
                'amount' => $tot_amt,
                'pv_receipt_no' => $input['pcv'],
            ];
            $transactionToSave = new Transaction($transactions);
            $transactionToSave->id = $input['tran_id'];
            $itemArray = [];


            for ($i = 0; $i < count($input['ledger']); $i++) {
                if ($input['debit'][$i] > 0) {
                    $item = new TransactionItem;
                    $item->ledger_id = $input['ledger'][$i];
                    $item->amount = $input['debit'][$i];
                    $item->item_description = $input['item_description'][$i];
                    $item->dc = 'D';
                    $itemArray[] = $item;
                }

                if ($input['credit'][$i] > 0) {
                    $item2 = new TransactionItem;
                    $item2->ledger_id = $input['ledger'][$i];
                    $item2->amount = $input['credit'][$i];
                    $item2->item_description = $input['item_description'][$i];
                    $item2->dc = 'C';
                    $itemArray[] = $item2;
                }
            }

            
            $transactionToSave->item = $itemArray;
            $this->makePosting($transactionToSave);

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }


    public function saveTransaction($input)
    {

        //$rand = parent::random(1, 99, 1);
        $ref_no = 'TRA' . time();

        // Total amount
        $tot_amt = 0;

        try {
            for ($i = 0; $i < count($input['ledger']); $i++) {
                $tot_amt += $input['debit'][$i];
            }


            $date = Carbon\Carbon::createFromFormat('d/m/Y', $input['transaction_date']);
            $postingTransaction = [
                'transaction_date' => $date->format('Y-m-d'),
                'amount' => $tot_amt,
                'transaction_type' => 'batch-posting',
                'reference_no' => $ref_no,
                'description' => $input['item_description'][0],
                'pv_receipt_no' => $input['pcv']
            ];


            $itemArray = [];
            for ($i = 0; $i < count($input['ledger']); $i++) {
                if ($input['debit'][$i] > 0) {

                    $item = new TransactionItem;
                    $item->ledger_id = $input['ledger'][$i];
                    $item->amount = $input['debit'][$i];
                    $item->item_description = $input['item_description'][$i];
                    $item->dc = 'D';
                    $itemArray[] = $item;
                }

                if ($input['credit'][$i] > 0) {
                    $item = new TransactionItem;
                    $item->ledger_id = $input['ledger'][$i];
                    $item->amount = $input['credit'][$i];
                    $item->item_description = $input['item_description'][$i];
                    $item->dc = 'C';
                    $itemArray[] = $item;
                }
            }

            $formatted = new Transaction($postingTransaction);
            $formatted->item = $itemArray;
            $this->makePosting($formatted);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        } catch (\Throwable $exception) {
            dd($exception->getMessage());
        }
    }

    // Delete's multiple journal entries
    public function deleteTransaction($input)
    {
        try {
            DB::connection('tenant_conn')->beginTransaction();
            for ($i = 0; $i < count($input['ids']); $i++) {

                if ($trans = Transaction::find($input['ids'][$i])) {
                    $trans->delete();
                    TransactionItem::where('transaction_id', $trans->id)->delete();
                }
            }
        } catch (\Exception $e) {
            DB::connection('tenant_conn')->rollback();
            return false;
            
        }
        DB::connection('tenant_conn')->commit();
        return true;
        
    }

    //Delete's single journal entries
    public function delTransactionSingle($id)
    {
        try {
            DB::connection('tenant_conn')->beginTransaction();
            if ($trans = Transaction::find($id)) {
                $trans->delete();
                TransactionItem::where('transaction_id', $trans->id)->delete();
            }
        } catch (\Exception $e) {
            DB::connection('tenant_conn')->rollback();
            return false;
        }
        DB::connection('tenant_conn')->commit();
        return true;
    }
}
