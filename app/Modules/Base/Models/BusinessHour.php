<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class BusinessHour extends Model {
	use NullingDB;
	protected $fillable = [
							'day',
							'start',
							'end'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
    protected $table = 'business_hours';

}
