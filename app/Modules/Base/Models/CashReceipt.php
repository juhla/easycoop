<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class CashReceipt extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;

    protected $connection = 'tenant_conn';

    protected $table = 'cash_receipt';
    protected $attributes = [
        'status' => 0,
    ];

    protected $fillable = ['date', 'category_id', 'invoice_no', 'bank', 'customer', 'description', 'amount', 'created_by', 'updated_by', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function customer()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'customer_code');
    }

    public function income()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'income_code');
    }
}
