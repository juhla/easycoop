<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class ChequePayment extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'cheque_payment';
    protected $attributes = [
        'flag' => 'Active',
        'status' => '0'
    ];
    protected $fillable = ['mode_of_payment', 'category_id', 'date', 'pv_no', 'bank', 'cheque_no', 'supplier', 'description', 'amount', 'created_by', 'updated_by', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getTableName()
    {
        return $this->table;
    }
}
