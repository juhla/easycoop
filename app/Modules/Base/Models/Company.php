<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Company extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'companies';

    public function getTableName()
    {
        return $this->table;
    }

    protected $fillable = ['user_id', 'company_name', 'slug', 'rc_number'];

    public function user()
    {
        return $this->hasOne('App\Modules\Base\Models\User', 'id', 'user_id');
    }

    public function country_data()
    {
        return $this->belongsTo('App\Modules\Base\Models\Country', 'currency', 'id');
    }


}