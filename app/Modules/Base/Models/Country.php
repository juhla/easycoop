<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Country extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'countries';


    public function state()
    {
        return $this->belongsTo('App\Modules\Base\Models\Country');
    }
}