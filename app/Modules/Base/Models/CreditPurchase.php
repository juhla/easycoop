<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class CreditPurchase extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    protected $connection = 'tenant_conn';
    protected $attributes = [
        'flag' => 'Active',
        'status' => '0'
    ];
    protected $table = 'credit_purchase';

    protected $fillable = ['date', 'invoice_no', 'category_id', 'description', 'amount', 'supplier', 'created_by', 'updated_by', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
