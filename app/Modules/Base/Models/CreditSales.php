<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class CreditSales extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'credit_sales';
    protected $attributes = [
        'flag' => 'Active',
        'status' => '0'
    ];
    protected $fillable = ['date', 'category_id', 'invoice_no', 'description', 'amount', 'customer', 'created_by', 'updated_by', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
