<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class CustomField extends Model {
	use NullingDB;
	protected $fillable = [
							'form',
							'field_name',
							'field_title',
							'field_type',
							'field_values',
							'field_required'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
    protected $table = 'custom_fields';

	public function customFieldValue()
    {
        return $this->hasMany('App\customFieldValue');
    }
}
