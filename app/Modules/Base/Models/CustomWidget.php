<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class CustomWidget extends Model
{
    use NullingDB;
    //
    protected $connection = 'mysql';
    protected $table = 'custom_widgets';

    public static function isExist()
    {
        try {
            CustomWidget::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }


    public static function getCustomWidget($userID)
    {
        $userWidget = CustomWidget::where('user_id', $userID)->select('widget_id')->get();
        return $userWidget;
    }
}
