<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use App\Modules\Base\Traits\Uuids;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Customer extends Model implements AuditableContract
{
    use Uuids;
    use NullingDB;
    use SoftDeletes;
    use Auditable, NullableFields;
    //set connection name
    protected $connection = 'tenant_conn';
    //set database table
    protected $table = 'ca_customers';
    protected $attributes = [
        'sync' => '0',
        'flag' => 'Active',
    ];

    protected $fillable = ['ledger_code', 'name', 'email', 'first_name', 'last_name', 'currency_code', 'account_no', 'phone_no', 'website', 'country_id', 'address', 'sync'];
    public $incrementing = false;

    public function ledger()
    {
        return $this->hasOne('App\Modules\Base\Models\Ledger', 'code', 'ledger_code');
    }

    public function getTableName()
    {
        return $this->table;
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}