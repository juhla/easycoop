<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Department extends Model {
	use NullingDB;
	protected $fillable = [
							'department_name'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
	protected $table = 'departments';

	public function profile() {
        return $this->hasMany('App\Modules\Base\Models\Profile');
	}

	public function ticket() {
        return $this->hasMany('App\Modules\Base\Models\Ticket');
	}
}
