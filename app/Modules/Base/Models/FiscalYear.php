<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use Carbon\Carbon;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class FiscalYear extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable;
    use SoftDeletes;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'ca_fiscal_year';
    protected $attributes = [
        'status' => 'Open',
        'flag' => 'Active',
    ];
    //mass assignment fields
    protected $fillable = ['begins', 'ends', 'status'];

    /**
     * save fiscal year details
     * @param $input
     * @param $user_id
     * @return Mixed
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public static function updateFYear($input)
    {

        try {
            $begin_date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['begins']);
            $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['ends']);
        } catch (\Exception $exception) {
            $end_date = Carbon::now();
            $begin_date = Carbon::now();
        }

        //get input fields for education details
        $info = array(
            'begins' => $begin_date->format('Y-m-d'),
            'ends' => $end_date->format('Y-m-d'),
            'status' => $input['status']
        );

        if ($input['fiscal_year_id'] === '') {
            //insert new fiscal years
            $res = FiscalYear::create($info);
            $return_arr['data'] = "<tr>
                        <td>" . date('d/m/Y', strtotime($res->begins)) . "</td>
                        <td>" . date('d/m/Y', strtotime($res->ends)) . "</td>
                        <td>" . $res->status . "</td>
                        <td>
                             <div class='btn-group btn-group-xs'>
                                 <button type='button' class='btn btn-info edit' data-value='" . $res->id . "'><i class='fa fa-pencil'></i></button>
                                 <button type='button' class='btn btn-danger delete' data-value='" . $res->id . "'><i class='fa fa-trash-o'></i> </button>
                             </div>
                        </td>
                    </tr>";
            $return_arr['message'] = "New Fiscal Year has been created";
            return $return_arr;
        } else {
            FiscalYear::find($input['fiscal_year_id'])->update($info);
            //get last insert details
            $res = FiscalYear::find($input['fiscal_year_id']);
            $return_arr['data'] = "
                        <td>" . date('d/m/Y', strtotime($res->begins)) . "</td>
                        <td>" . date('d/m/Y', strtotime($res->ends)) . "</td>
                        <td>" . $res->status . "</td>
                        <td>
                             <div class='btn-group btn-group-xs'>
                                 <button type='button' class='btn btn-info edit' data-value='" . $res->id . "'><i class='fa fa-pencil'></i></button>
                                 <button type='button' class='btn btn-danger delete' data-value='" . $res->id . "'><i class='fa fa-trash-o'></i> </button>
                             </div>
                        </td>";
            $return_arr['message'] = "Fiscal year has been updated";
            return $return_arr;
        }
    }


    public function isFiscalYear($start, $end)
    {
        $f_year = self::whereDate('begins', $start)
            ->where('ends', $end)
            ->where('flag', 'Active')
            ->get();

        return $f_year->isEmpty();
    }

    public static function getCurrentFiscalYear()
    {
        try {
            $f_year = FiscalYear::where('status', 'Open')
                ->where('flag', 'Active')
                ->first();
            return $f_year;
        } catch (\Exception $exception) {
            return New FiscalYear();
        }
    }


    public static function getPreviousFiscalYear()
    {
        try {
            $f_year = FiscalYear::where('status', 'Open')
                ->where('flag', 'Active')->first();
            return $f_year;
        } catch (\Exception $exception) {
            return New FiscalYear();
        }
    }
}
