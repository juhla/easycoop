<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Holiday extends Model {
	use NullingDB;
	protected $fillable = [
							'date',
							'holiday_description'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
	protected $table = 'holidays';

}
