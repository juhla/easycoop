<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class InfluencerAgent extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $fillable = [
        'influencer_id',
        'agent_id'
    ];
}
