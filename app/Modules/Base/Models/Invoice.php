<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Vendor;
use App\Modules\Base\Traits\DebitCredit;
use App\Modules\Invoices\Traits\InvoiceTrait;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\Customer;
use App\Modules\Invoices\Models\InvoiceItem;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Ledger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Invoice extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable, NullableFields, InvoiceTrait;
    //set connection
    protected $connection = 'tenant_conn';

    protected $table = 'ca_invoice';
    protected $attributes = [
        'payment_term_id' => '0',
        'status' => '0',
        'flag' => 'Active',
        'vat' => 'None',
        'income_type' => 'None',
    ];

    protected $nullable = [
        'transaction_id',
        'currency',
        'payment_term_id',
        'amount_paid',
    ];

    protected $fillable = [
        'id',
        'payment_type',
        'transaction_id',
        'bank_invoice',
        'payment_term_id',
        'invoice_no',
        'customer_id',
        'vat',
        'invoice_type',
        'po_so_no',
        'invoice_date',
        'due_date',
        'memo',
        'amount_due',
        'total_amount',
        'currency',
        'bank_invoice',
        'transaction_id',
        'amount_paid',
        'income_type',

    ];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function saveInvoice($input)
    {
        //$invoices = Invoice::all();
        if ($input['invoice_type'] === 'sales') {
            $customer = Customer::find($input['customer_id']);
        } else {
            $customer = Vendor::find($input['customer_id']);
        }
        $customerLedger = Ledger::where('code', $customer->ledger_code)->first();
        $invoice_no = 'INV' . time();
        $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['invoice_date']);
        $due_date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['due_date']);

        if ($input['payment_type'] == 'cash') {
            $paymentType = 1;
        } else if ($input['payment_type'] == 'credit') {
            $paymentType = 2;
        } else {
            $paymentType = 3; //bank
        }

        //if reference no is set from input
        if (isset($input['reference_no'])) {
            $reference_no = $input['reference_no'];
        } else {
            $reference_no = 'TRA' . time();
        }
        if (!isset($input['bank_id'])) {
            $bank = '';
        } else {
            $bank = $input['bank_id'];
        }


        $isVat = (@in_array($input['vat'], ['on', '1']) ? true : false);
        $input['isVat'] = $isVat;
        $total = $input['total'];
        if ($isVat) {
            $total = 1.05 * $input['total'];
        }


        $invoice = [
            'customer_id' => $input['customer_id'],
            'income_type' => @$input['income_type'],
            'invoice_no' => $invoice_no,
            'invoice_type' => $input['invoice_type'],
            //'po_so_no'      =>  $input['po_so_no'],
            'invoice_date' => $date->format('Y-m-d'),
            'due_date' => $due_date->format('Y-m-d'),
            'memo' => $input['memo'],
            'amount_due' => $input['total'],
            'total_amount' => $total,
            'amount' => $input['total'],
            'vat' => (($isVat) ? 1 : 0),
            'bank_invoice' => $input['bank_invoice'],
            'currency' => $input['currency'],
            'created_by' => Auth::user()->id,
            'transaction_type' => 'invoice',
            'reference_no' => $reference_no,
            'payment_term_id' => $paymentType,
            'payment_type' => $input['payment_type'],
            'transaction_date' => date('Y-m-d'),
            'bank_id' => $bank,
        ];


        $formatted = new Invoice($invoice);
        if (!empty($input['invoice_id'])) {
            $formatted->id = $input['invoice_id'];
        }
        for ($i = 0; $i < count($input['description']); $i++) {
            $invoice_items = [
                'description' => $input['description'][$i],
                'amount' => $input['price'][$i],
                'product_id' => 0,
                'tax' => 0,
                'price' => $input['price'][$i],
                'quantity' => 1
            ];
            $InvoiceItem[] = new InvoiceItem($invoice_items);
        }

        $formatted->item = $InvoiceItem;

        //dd($formatted);
        $this->makeInvoicePosting($formatted);
    }

    private function multiInvoicePosting($input, $created = true)
    {

        $transactionItem = [];
        try {

            $reference_no = $input['reference_no'];
            $paymentType = $input['paymentType'];
            $bank = $input['bank'];
            $invoice_no = $input['invoice_no'];
            $payment_type = $input['payment_type'];
            $total = $input['total'];
            $totalAmount = $total * 1.05;
            $subTotalAmount = $total;
            $vatAmount = $total * 0.05;
            $description = $input['memo'];


            $isVat = (@$input['vat'] ? true : false);

            if ($isVat) {

                $trans_item[] = [
                    'ledger_id' => getLedgerId('1211.01'),
                    'amount' => $vatAmount,
                    'item_description' => $description,
                    'dc' => 'C'
                ]; // Credit VAT payable with VAT Amount


                $trans_item[] = [
                    'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                    'amount' => $totalAmount,
                    'item_description' => $description,
                    'dc' => 'D'
                ];
                // Debit Customer

                $trans_item[] = [
                    'ledger_id' => getLedgerId($input['income_type']),
                    'amount' => $subTotalAmount,
                    'item_description' => $description,
                    'dc' => 'C',
                ];

                // Credit Sales


                if ($input['payment_type'] != 'credit') {
                    $trans_item[] = [
                        'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                        'amount' => $totalAmount,
                        'item_description' => $description,
                        'dc' => 'C'
                    ];
                    // Credit Customer
                }

                if ($input['payment_type'] == 'cash') {

                    $trans_item[] = [
                        'ledger_id' => getLedgerId(1261),
                        'amount' => $totalAmount,
                        'item_description' => $description,
                        'dc' => 'D',
                    ];
                    // Debit Cash
                } else if ($input['payment_type'] == 'bank') {

                    $trans_item[] = [
                        'ledger_id' => $input['bank_id'],
                        'amount' => $totalAmount,
                        'item_description' => $description,
                        'dc' => 'D',
                    ];
                    // Debit Bank

                }
            } else {


                $trans_item[] = [
                    'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                    'amount' => $total,
                    'item_description' => $description,
                    'dc' => 'D'
                ];

                // Debit Customer
                $trans_item[] = [
                    'ledger_id' => getLedgerId($input['income_type']),
                    'amount' => $total,
                    'item_description' => $description,
                    'dc' => 'C',
                ];

                // Credit Sales

                if ($input['payment_type'] != 'credit') {
                    $trans_item[] = [
                        'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                        'amount' => $total,
                        'item_description' => $description,
                        'dc' => 'C'
                    ];
                    // Credit Customer
                }

                if ($input['payment_type'] == 'cash') {


                    $trans_item[] = [
                        'ledger_id' => getLedgerId(1261),
                        'amount' => $total,
                        'item_description' => $description,
                        'dc' => 'D',
                    ];
                    // Debit Cash
                } else if ($input['payment_type'] == 'bank') {

                    $trans_item[] = [
                        'ledger_id' => $input['bank_id'],
                        'amount' => $total,
                        'item_description' => $description,
                        'dc' => 'D',
                    ];
                    // Debit Bank

                }
            }

            if (!isset($input['transaction_id'])) {
                $input['transaction_id'] = '';
            }


            if (!$created) {
                $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['invoice_date']);
                if (empty($input['income_type']) || $input['income_type'] == '') {
                    $transaction = [
                        'amount' => $total,
                        'updated_by' => \Illuminate\Support\Facades\Auth::user()->id,
                        'transaction_date' => $date->format('Y-m-d'),
                        'description' => $description,
                    ];
                    Transaction::where('id', $input['transaction_id'])->update($transaction);
                } else {

                    if ($input['payment_type'] == 'cash') {
                        $paymentType = 1;
                    } else if ($input['payment_type'] == 'credit') {
                        $paymentType = 2;
                    } else {
                        $paymentType = 3; //bank
                    }

                    $transaction_id = $input['transaction_id'];
                    $transaction = [
                        'amount' => $total,
                        'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
                        'transaction_type' => 'invoice',
                        'reference_no' => $reference_no,
                        'payment_type' => $paymentType,
                        'transaction_date' => $date->format('Y-m-d'),
                        // 'bank_id' => @$bank,
                        'description' => $description,
                    ];


                    $formatted = new Transaction($transaction);
                    $formatted->id = $transaction_id;
                    foreach ($trans_item as $eachTransaction) {
                        $transactionItem[] = new TransactionItem($eachTransaction);
                    }
                    $formatted->item = $transactionItem;
                    $this->makePosting($formatted);

                }
            } else {
                $invoiceObj = $input['invoiceObj'];
                if ($input['transaction_id'] === '') {
                    $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['invoice_date']);
                    $__transaction = [
                        'reference_no' => $reference_no,
                        'amount' => $total,
                        'transaction_date' => $date->format('Y-m-d'),
                        'transaction_type' => 'invoice',
                        //'description' => $input['memo'],
                        'description' => $description,
                        'payment_type' => $paymentType,
                        'bank_id' => $bank,
                        'customer_vendor' => $input['customer_id'],
                        'pv_receipt_no' => $invoice_no,
                        'created_by' => Auth::user()->id,
                    ];


                    $formatted = new Transaction($__transaction);
                    foreach ($trans_item as $eachTransaction) {
                        $transactionItem[] = new TransactionItem($eachTransaction);
                    }
                    $formatted->item = $transactionItem;
                    $this->makePosting($formatted);

                    if (!$this->isError) {
                        Invoice::find($invoiceObj->id)->update(['transaction_id' => $this->savedTransaction->id]);
                    }

                } else {
                    $formatted = Transaction::find($input['transaction_id']);
                    foreach ($trans_item as $eachTransaction) {
                        $transactionItem[] = new TransactionItem($eachTransaction);
                    }
                    $formatted->item = $transactionItem;
                    $this->makePosting($formatted);
                }
            }
        } catch (\Exception $exception) {
            dd($exception->getMessage() . " >> " . $exception->getLine());
        }
    }

    public function items()
    {
        return $this->hasMany('App\Modules\Invoices\Models\InvoiceItem', 'invoice_id');
    }

    public function item()
    {
        return $this->hasMany('App\Modules\Invoices\Models\InvoiceItem', 'invoice_id');
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function customer()
    {
        return $this->belongsTo('App\Modules\Base\Models\Customer', 'customer_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Modules\Base\Models\Vendor', 'customer_id');
    }
}
