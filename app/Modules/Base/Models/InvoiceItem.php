<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class InvoiceItem extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';

    protected $table = 'ca_invoice_items';
    protected $attributes = [
        'flag' => 'Active',
    ];

    //public $timestamps = false;

    protected $fillable = [
        'invoice_id',
        'product_id',
        'description',
        'quantity',
        'price',
        'amount',
        'tax'
    ];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function invoice()
    {
        return $this->belongsTo('App\Modules\Invoices\Models\Invoice', 'invoice_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Modules\Base\Models\Product', 'product_id');
    }
}
