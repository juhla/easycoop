<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class TaxType extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'ca_tax_types';
    //mass assignment fields
    protected $fillable = ['template', 'company_logo', 'display_logo', 'product_title', 'default_invoice_title'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
