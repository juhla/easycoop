<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\Tree;
use App\Modules\Base\Traits\LedgerCode;
use App\Modules\Base\Traits\NewCreates;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use App\Modules\Base\Traits\Uuids;
use Carbon;
use App\Modules\Base\Models\FiscalYear;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Webpatser\Uuid\Uuid;

class Ledger extends Model implements AuditableContract
{
    use Auditable, NewCreates, NullableFields, Balance, Tree, SoftDeletes, LedgerCode, Uuids;
    public $incrementing = false;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'ca_ledgers';
    protected $attributes = [
        'flag' => 'Active',
    ];
    //mass assignment fields
    protected $fillable = [
        'name', 'code', 'group_id', 'opening_balance', 'opening_balance_type', 'is_system_account'
    ];


    protected $nullable = [
        'opening_balance',
        'is_system_account',
    ];

    protected $casts = [
        'group_id' => 'integer',
        'opening_balance' => 'double',
        'code' => 'float',
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('ca_ledgers.flag', 'Active');
        });
        static::creating(function ($model) {
            $model->id = Uuid::generate()->string;
        });
    }


    public static function getLedgerByGroup($groupID)
    {
        if (!is_array($groupID)) {
            $groupID = array($groupID);
        }
        return Ledger::whereIn('group_id', $groupID)
            ->join('ca_groups', 'ca_ledgers.group_id', '=', 'ca_groups.id')
            ->pluck('ca_ledgers.name', 'ca_ledgers.code')
            ->all();
    }

    public function group()
    {
        return $this->belongsTo('App\Modules\Base\Models\AccountGroup', 'group_id')->where('ca_groups.flag', 'Active');
    }

    public function banks()
    {
        return $this->hasMany('App\Modules\Base\Models\BankAC', 'ledger_code')->where('ca_bank_accounts.flag', 'Active');
    }

    public function transactionItems()
    {
        return $this->hasMany('App\Modules\Base\Models\TransactionItem', 'ledger_id')->where('ca_transaction_items.flag', 'Active');
    }

    /**
     * Calculate closing balance of specified ledger account for the given
     * date range
     *
     * @param2 date start date
     */

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public static function getAdjustmentID()
    {
        $ledgers = Ledger::where('name', "Adjustment Journal")->first();
        return $ledgers->id;
    }

    public static function getMemoID()
    {
        $ledgers = Ledger::where('name', "Memorandum Account")->first();
        return $ledgers->id;
    }

    public function getClosingBalance()
    {
        $op_balance = $this->op_balance; //set opening balance
        $op_balance_type = $this->op_balance_type; //set balance type (Debit/Credit)
        $dr_total = 0; //default debit total
        $cr_total = 0; //default credit total
        $dr_total_dc = 0;
        $cr_total_dc = 0;

        //get debit total
        $total_dr = TransactionItem::where('ledger_id', $this->id)
            ->where('dc', 'D')
            ->sum('amount');
        if (empty($total_dr)) {
            $dr_total = 0;
        } else {
            $dr_total = $total_dr;
        }

        //get credit total
        $total_cr = TransactionItem::where('ledger_id', $this->id)
            ->where('dc', 'C')
            ->sum('amount');
        if (empty($total_cr)) {
            $cr_total = 0;
        } else {
            $cr_total = $total_cr;
        }

        /* Add opening balance */
        if ($op_balance_type == 'D') {
            $dr_total_dc = bcadd($op_balance, $dr_total, 2);
            $cr_total_dc = $cr_total;
        } else {
            $dr_total_dc = $dr_total;
            $cr_total_dc = bcadd($op_balance, $cr_total, 2);
        }

        /* Calculate and update closing balance */
        $cl = 0;
        if ($dr_total_dc > $cr_total_dc) {
            $cl = bcsub($dr_total_dc, $cr_total_dc, 2);
            if ($cl === '0.00') {
                return '0.00';
            } else {
                return 'Dr ' . $cl;
            }
        } else if ($cr_total_dc === $dr_total_dc) {
            return '0.00';
        } else {
            $cl = bcsub($cr_total_dc, $dr_total_dc, 2);
            if ($cl === '0.00') {
                return '0.00';
            } else {
                return 'Cr ' . $cl;
            }
        }
    }

    public function getTransactionsForLedger($ledger_id, $year = null, $dateRange = null)
    {
        $transactions = TransactionItem::where('ledger_id', $ledger_id)->with(['transactions']);
        if ($year) {
            $transactions = $transactions->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereYear('ca_transactions.transaction_date', '=', $year);
        }
        if ($dateRange) {
            $transactions = $transactions->dateRange($dateRange['start_date'], $dateRange['end_date']);
        }

        //        $transactions = $transactions->get()->groupBy(function($transactionItem) {
        //           return Carbon\Carbon::parse($transactionItem->transactions->transaction_date)->format('Y-m');
        //        })->sortBy(function($v,$k) { return $k; });


        $transactions = $transactions->selectRaw('ca_transaction_items.*')
            ->get()->groupBy(function ($transactionItem) {
                return Carbon\Carbon::parse($transactionItem->transactions->transaction_date)->format('Y-m');
            })->sortBy(function ($v, $k) {
                return $k;
            });

        return $transactions;
    }

    public static function calculateLedgerBalance($ledgerID)
    {
        if (!is_array($ledgerID)) {
            $ledgerID = array($ledgerID);
        }
        $balance = 0;
        $balanceBroughtForwardType = '';
        try {

            $debit = TransactionItem::whereIn('ca_ledgers.code', $ledgerID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->sum('ca_transaction_items.amount');

            $credit = TransactionItem::whereIn('ca_ledgers.code', $ledgerID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->select('ca_ledgers.name as name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
                ->get()
                ->toArray();

            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        } catch (\Exception $ex) {
            pr($ex);
        }

        $result['Name'] = $balance;
        $result['Value'] = $balance;
        $result['Balance'] = $balanceBroughtForwardType;

        return $result;
    }

    public function runningExpenses($begins, $ends)
    {
        $transactionItems = new TransactionItem();
        $res = $transactionItems
            ->whereBetween('ca_transactions.transaction_date', [$begins, $ends])
            ->where(function ($query) {
                $query->orWhere('ca_ledgers.group_id', 38);  // Admin Overhead
                $query->orWhere('ca_ledgers.group_id', 40);  // Marketing and Selling Expenses
                $query->orWhere('ca_ledgers.group_id', 41);  // Financial Expenses
            })
            ->with(['transactions', 'ledger'])
            ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
            ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.name', 'ca_ledgers.id', 'ca_ledgers.group_id', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
            ->orderBy('ca_ledgers.name', 'desc')
            ->groupBy('ca_ledgers.name')
            ->take(10);

        $resEE = $res->get()
            ->toArray();

        return $resEE;
    }

    public function getLedgerBalance($ledgerID)
    {
        $balance = 0;
        $name = $balanceBroughtForwardType = '';
        try {
            $transactionItems = new TransactionItem();
            $debit = $transactionItems
                ->where('ca_ledgers.code', $ledgerID)
                ->where('dc', 'D')
                ->with(['transactions', 'ledger'])
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->select('ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
                ->get()
                ->first();


            $credit = $transactionItems
                ->where('ca_ledgers.code', $ledgerID)
                ->where('dc', 'C')
                ->with(['transactions', 'ledger'])
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->select('ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
                ->get()
                ->first();


            $name = (isset($credit->name) ? $credit->name : $debit->name);
            $debit = $debit->total_sales;
            $credit = $credit->total_sales;


            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        } catch (\Exception $ex) {
            //pr($ex->getMessage());
        }


        $result['Name'] = $name;
        $result['Value'] = $balance;
        $result['Balance'] = $balanceBroughtForwardType;

        return $result;
    }

    public function topCustomerBySales($maximum_amount = 5)
    {
        $debit = Ledger::where('group_id', 12)
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.name as name', DB::raw('SUM(ca_transaction_items.amount) as value'))
            ->where('dc', 'D')
            ->orderBy('value', 'desc')
            ->groupBy('ca_ledgers.name')
            ->get()
            ->take($maximum_amount)
            ->toArray();
        return $debit;
    }


    public function topSupplierByPurchases($maximum_amount = 5)
    {
        $debit = Ledger::where('group_id', 28)
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.name as name', DB::raw('SUM(ca_transaction_items.amount) as value'))
            ->where('dc', 'C')
            ->orderBy('value', 'desc')
            ->groupBy('ca_ledgers.name')
            ->get()
            ->take($maximum_amount)
            ->toArray();
        return $debit;
    }

    public function topReceivables($maximum_amount = 5)
    {
        $value = [];
        $debit = Ledger::where('group_id', 12)
            ->with(['transactionItems'])
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.id', 'ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
            ->where('dc', 'D')
            ->orderBy('ca_ledgers.name', 'desc')
            ->groupBy('ca_ledgers.id')
            ->get()
            ->toArray();


        $credit = Ledger::where('group_id', 12)
            ->with(['transactionItems'])
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.id', 'ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
            ->where('dc', 'C')
            ->orderBy('ca_ledgers.name', 'desc')
            ->groupBy('ca_ledgers.id')
            ->get()->toArray();

        //pr($credit);exit;


        foreach ($debit as $debitkey => $eachDebit) {

            $value[$debitkey]['Value'] = ($eachDebit['total_sales']);
            $value[$debitkey]['Name'] = $eachDebit['name'];

            foreach ($credit as $creditkey => $eachCredit) {
                if ($eachCredit['id'] == $eachDebit['id']) {
                    $value[$debitkey]['Value'] = ($eachCredit['total_sales'] - $eachDebit['total_sales']);
                    $value[$debitkey]['Name'] = $eachCredit['name'];
                }
            }
        }

        //pr($value);exit;

        usort($value, function ($a, $b) {
            return $a['Value'] < $b['Value'];
        });

        return array_slice($value, 0, $maximum_amount);
    }

    public function topPayables($maximum_amount = 5)
    {
        $value = [];
        $debit = Ledger::where('group_id', 28)
            ->with(['transactionItems'])
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.id', 'ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
            ->where('dc', 'D')
            ->orderBy('ca_ledgers.name', 'desc')
            ->groupBy('ca_ledgers.id')
            ->get()->toArray();


        $credit = Ledger::where('group_id', 28)
            ->with(['transactionItems'])
            ->join('ca_transaction_items', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
            ->select('ca_ledgers.id', 'ca_ledgers.name', DB::raw('SUM(ca_transaction_items.amount) as total_sales'))
            ->where('dc', 'C')
            ->orderBy('ca_ledgers.name', 'desc')
            ->groupBy('ca_ledgers.id')
            ->get()->toArray();


        foreach ($credit as $creditkey => $eachCredit) {

            $value[$creditkey]['Value'] = ($eachCredit['total_sales']);
            $value[$creditkey]['Name'] = $eachCredit['name'];
            foreach ($debit as $debitkey => $eachDebit) {
                if ($eachCredit['id'] == $eachDebit['id']) {
                    $value[$creditkey]['Value'] = ($eachCredit['total_sales'] - $eachDebit['total_sales']);
                    $value[$creditkey]['Name'] = $eachCredit['name'];
                }
            }
        }

        usort($value, function ($a, $b) {
            return $a['Value'] < $b['Value'];
        });

        return array_slice($value, 0, $maximum_amount);
    }


    public function sumTransactionItems($month_start, $month_end, $ledgerID, $dt = null)
    {
        if (!is_array($ledgerID)) {
            $ledgerID = array($ledgerID);
        }
        $balance = 0;
        $balanceBroughtForwardType = '';
        try {

            $debit = TransactionItem::whereIn('ca_ledgers.code', $ledgerID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->whereBetween('ca_transactions.transaction_date', [$month_start, $month_end])
                ->sum('ca_transaction_items.amount');

            $credit = TransactionItem::whereIn('ca_ledgers.code', $ledgerID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->whereBetween('ca_transactions.transaction_date', [$month_start, $month_end])
                ->sum('ca_transaction_items.amount');

            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        } catch (\Exception $ex) {
        }


        if (isset($dt) || !empty($dt)) {
            $result['Month'] = $dt->format("F");
            $result['Month_short'] = $dt->format("M");
            $result['Year'] = $dt->format("Y");
            $result['MonthNumber'] = $dt->format("m");
        }
        $result['Value'] = $balance;
        $result['Balance'] = $balanceBroughtForwardType;

        return $result;
    }

    public function calculateGroupMonthBalance($fiscalYearStart, $fiscalYearStop, $groupID)
    {

        $monthChart = [];
        $fiscalYearStart = new \DateTime($fiscalYearStart);
        $fiscalYearStop = new \DateTime($fiscalYearStop);


        $start = $fiscalYearStart->modify('first day of this month');
        $end = $fiscalYearStop->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);

        $counter = 0;
        foreach ($period as $dt) {
            $start = $dt->format("Y-m-01");
            $end = $dt->format("Y-m-31");
            $monthChart[$counter] = $this->sumGroupTransactionItems($start, $end, $groupID, $dt);
            $counter++;
        }

        return $monthChart;
    }

    private function sumGroupTransactionItems($month_start, $month_end, $groupID, $dt)
    {


        $balance = 0;
        $balanceBroughtForwardType = '';
        try {
            $debit = TransactionItem::where('ca_ledgers.group_id', $groupID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->whereBetween('ca_transactions.transaction_date', [$month_start, $month_end])
                ->sum('ca_transaction_items.amount');

            $credit = TransactionItem::where('ca_ledgers.group_id', $groupID)
                ->with(['transactions', 'ledger'])
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->join('ca_ledgers', 'ca_transaction_items.ledger_id', '=', 'ca_ledgers.id')
                ->whereBetween('ca_transactions.transaction_date', [$month_start, $month_end])
                ->sum('ca_transaction_items.amount');

            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        } catch (\Exception $ex) {
        }

        $result['Month'] = $dt->format("F");
        $result['Month_short'] = $dt->format("M");
        $result['Year'] = $dt->format("Y");
        $result['Value'] = $balance;
        $result['Balance'] = $balanceBroughtForwardType;
        $result['MonthNumber'] = $dt->format("m");;
        return $result;
    }


    public function calculateGroupWeekBalance($monthStart, $monthStop, $groupID)
    {

        $weekChart = [];
        $start = new \DateTime($monthStart);
        $end = new \DateTime($monthStop);
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($start, $interval, $end);

        $weekNumber = 1;
        $weeks = array();
        foreach ($dateRange as $date) {
            $weeks[$weekNumber][] = $date->format('Y-m-d');
            if ($date->format('w') == 6) {
                $weekNumber++;
            }
        }
        //pr($begins);pr($ends);exit;
        $counter = 0;
        foreach ($weeks as $eachWeek) {

            $min = min($eachWeek);
            $max = max($eachWeek);
            $weekChart[$counter] = $this->sumGroupTransactionItems($min, $max, $groupID, $date);
            $weekChart[$counter]['weeks'] = "Week " . ($counter + 1);
            $counter++;
        }
        return $weekChart;
    }


    public function calculateLedgerWeekBalance($monthStart, $monthStop, $ledgerID)
    {

        $weekChart = [];
        $start = new \DateTime($monthStart);
        $end = new \DateTime($monthStop);
        $interval = new \DateInterval('P1D');
        $dateRange = new \DatePeriod($start, $interval, $end);

        $weekNumber = 1;
        $weeks = array();
        foreach ($dateRange as $date) {
            $weeks[$weekNumber][] = $date->format('Y-m-d');
            if ($date->format('w') == 6) {
                $weekNumber++;
            }
        }
        $counter = 0;
        foreach ($weeks as $eachWeek) {
            $min = min($eachWeek);
            $max = max($eachWeek);

            $weekChart[$counter] = $this->sumTransactionItems($min, $max, $ledgerID, $date);
            $weekChart[$counter]['weeks'] = "Week " . ($counter + 1);
            $counter++;
        }
        //pr($weekChart);exit;
        return $weekChart;
    }


    public function calculateLedgerMonthBalance($fiscalYearStart, $fiscalYearStop, $ledgerID)
    {

        $monthChart = [];
        $fiscalYearStart = new \DateTime($fiscalYearStart);
        $fiscalYearStop = new \DateTime($fiscalYearStop);


        $start = $fiscalYearStart->modify('first day of this month');
        $end = $fiscalYearStop->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period = new \DatePeriod($start, $interval, $end);

        $counter = 0;
        foreach ($period as $dt) {
            $start = $dt->format("Y-m-01");
            $end = $dt->format("Y-m-31");
            $monthChart[$counter] = $this->sumTransactionItems($start, $end, $ledgerID, $dt);
            $counter++;
        }

        return $monthChart;
    }

    /**
     * Calculate balance for the year
     *
     *
     * @param year
     */
    public function calculateOpeningBalance($year)
    {
        $balance = 0;
        $balanceBroughtForwardType = '';

        if ($year) {
            $debit = TransactionItem::where('ledger_id', $this->id)
                ->with(['transactions'])
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereYear('ca_transactions.transaction_date', '<=', $year)
                ->sum('ca_transaction_items.amount');

            $credit = TransactionItem::where('ledger_id', $this->id)
                ->with(['transactions'])
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereYear('ca_transactions.transaction_date', '<=', $year)
                ->sum('ca_transaction_items.amount');

            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        }

        return [$balance, $balanceBroughtForwardType];
    }


    public function calculateFullOpeningBalance($year)
    {
        $balance = 0;
        $balanceBroughtForwardType = '';

        if ($year) {
            $debit = TransactionItem::where('ledger_id', $this->id)
                ->with(['transactions'])
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereDate('ca_transactions.transaction_date', '<', $year)
                ->sum('ca_transaction_items.amount');

            $credit = TransactionItem::where('ledger_id', $this->id)
                ->with(['transactions'])
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereDate('ca_transactions.transaction_date', '<', $year)
                ->sum('ca_transaction_items.amount');

            if ($debit > $credit) {
                $balance = $debit - $credit;
                $balanceBroughtForwardType = 'D';
            } else {
                $balance = $credit - $debit;
                $balanceBroughtForwardType = 'C';
            }
        }

        return [$balance, $balanceBroughtForwardType];
    }


    /**
     * Calculate the balance of a ledger
     * @param bool|false $compute if set to true returns dc and amount. if false returns a view
     * @param null $start_date
     * @return array
     */
    public function getBalance($compute = false, $start_date = null, $end_date = null, $year = null, $accumulate = null, $parentName = null)
    {
        $op_balance = $this->op_balance; //set opening balance
        $op_balance_type = $this->opening_balance_type; //set balance type (Debit/Credit)
        $dr_total = 0; //default debit total
        $cr_total = 0; //default credit total
        $dr_total_dc = 0;
        $cr_total_dc = 0;

        if (is_null($end_date) || $end_date === '') {
            $end_date = date('Y-m-d');
        }

        if (in_array($parentName, ['Assets', 'Liabilities', 'Equity'])) {
            $previous_year = date_format(date_sub(date_create($start_date), date_interval_create_from_date_string('1 year')), 'Y');
            $op = $this->calculateOpeningBalance($previous_year);
            $closingBalanceForPreviousYear = $op[0];
            $closingBalanceForPreviousYearType = $op[1];

            if (strtolower($closingBalanceForPreviousYearType) == 'd') {
                $dr_total += $closingBalanceForPreviousYear;
            } else {
                $cr_total += $closingBalanceForPreviousYear;
            }
        }

        //get debit total
        if (!is_null($start_date) || $start_date != '') {

            //get debit total
            $total_dr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where(function ($q) use ($accumulate, $start_date, $end_date) {
                    if ($accumulate) {
                        $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                        // $q->where('ca_transactions.transaction_date', '<=', $end_date);
                    } else {
                        $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                    }
                })
                ->sum('ca_transaction_items.amount');
        } elseif (!is_null($year)) {
            $total_dr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where(function ($q) use ($accumulate, $year) {
                    if ($accumulate) {
                        $q->where('ca_transactions.transaction_date', '<=', $year . '-12-31');
                    } else {
                        $q->whereBetween('ca_transactions.transaction_date', [$year . '-01-01', $year . '-12-31']);
                    }
                })
                ->sum('ca_transaction_items.amount');
        } else {
            $total_dr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'D')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transactions.transaction_date', '<=', $end_date)
                ->sum('ca_transaction_items.amount');
        }

        if (empty($total_dr)) {
            $dr_total += 0;
        } else {
            $dr_total += $total_dr;
        }

        //get credit total
        if (!is_null($start_date) || $start_date != '') {

            //get debit total
            $total_cr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where(function ($q) use ($accumulate, $start_date, $end_date) {
                    if ($accumulate) {
                        $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                        // $q->where('ca_transactions.transaction_date', '<=', $end_date);
                    } else {
                        $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                    }
                })
                ->sum('ca_transaction_items.amount');
        } elseif (!is_null($year)) {
            $total_cr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where(function ($q) use ($accumulate, $year) {
                    if ($accumulate) {
                        $q->where('ca_transactions.transaction_date', '<=', $year . '-12-31');
                    } else {
                        $q->whereBetween('ca_transactions.transaction_date', [$year . '-01-01', $year . '-12-31']);
                    }
                })
                ->sum('ca_transaction_items.amount');
        } else {
            $total_cr = TransactionItem::where('ledger_id', $this->id)
                ->where('dc', 'C')
                ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transactions.transaction_date', '<=', $end_date)
                ->sum('ca_transaction_items.amount');
        }

        if (empty($total_cr)) {
            $cr_total += 0;
        } else {
            $cr_total += $total_cr;
        }

        /* Add opening balance */
        if ($op_balance_type == 'D') {
            $dr_total_dc = bcadd($op_balance, $dr_total, 2);
            $cr_total_dc = $cr_total;
        } else {
            $dr_total_dc = $dr_total;
            $cr_total_dc = bcadd($op_balance, $cr_total, 2);
        }


        /* Calculate and update closing balance */
        $cl = 0;
        if ($dr_total_dc > $cr_total_dc) {
            $cl = bcsub($dr_total_dc, $cr_total_dc, 2);
            if ($compute) {
                return ['dc' => 'D', 'balance' => $cl];
            } else {
                return \View::make('accounting::reports.views.partials._report-child-mini')->with(['dc' => 'D', 'balance' => $cl]);
            }
        } else if ($cr_total_dc === $dr_total_dc) {
            if ($compute) {
                return ['dc' => '', 'balance' => ''];
            } else {
                return \View::make('accounting::reports.views.partials._report-child-mini')->with(['dc' => $op_balance_type, 'balance' => $cl]);
            }
        } else {
            $cl = bcsub($cr_total_dc, $dr_total_dc, 2);
            if ($compute) {
                return ['dc' => 'C', 'balance' => $cl];
            } else {
                return \View::make('accounting::reports.views.partials._report-child-mini')->with(['dc' => 'C', 'balance' => $cl]);
            }
        }
    }

    /**
     * Calculate opening balance of specified ledger account for the given
     * date range
     *
     * @param2 date start date
     */
    public function getOpeningBalance($start_date = null)
    {
        $op_balance = $this->op_balance; //set opening balance
        $op_balance_type = $this->op_balance_type; //set balance type (Debit/Credit)
        $dr_total = 0; //default debit total
        $cr_total = 0; //default credit total
        $dr_total_dc = 0;
        $cr_total_dc = 0;

        /* If start date is not specified then return here */
        if (is_null($start_date)) {
            return $this->op_balance;
        }

        //get debit total
        $total_dr = TransactionItem::where('ledger_id', $this->id)
            ->where('dc', 'D')
            ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
            ->where('ca_transactions.transaction_date', '<', $start_date)
            ->sum('ca_transaction_items.amount');
        if (empty($total_dr)) {
            $dr_total = 0;
        } else {
            $dr_total = $total_dr;
        }

        //get credit total
        $total_cr = TransactionItem::where('ledger_id', $this->id)
            ->where('dc', 'C')
            ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
            ->where('ca_transactions.transaction_date', '<', $start_date)
            ->sum('ca_transaction_items.amount');
        if (empty($total_cr)) {
            $cr_total = 0;
        } else {
            $cr_total = $total_cr;
        }

        /* Add opening balance */
        if ($op_balance_type == 'D') {
            $dr_total_dc = bcadd($op_balance, $dr_total, 2);
            $cr_total_dc = $cr_total;
        } else {
            $dr_total_dc = $dr_total;
            $cr_total_dc = bcadd($op_balance, $cr_total, 2);
        }

        /* Calculate and update closing balance */
        $cl = 0;
        if ($dr_total_dc > $cr_total_dc) {
            $cl = bcsub($dr_total_dc, $cr_total_dc, 2);
            return $cl;
        } else if ($cr_total_dc === $dr_total_dc) {
            return $cl;
        } else {
            $cl = bcsub($cr_total_dc, $dr_total_dc, 2);
            return $cl;
        }
    }


}
