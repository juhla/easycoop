<?php

namespace App\Modules\Base\Models\Messenger;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Message extends Model 
{
    use NullingDB;
    //use SoftDeletes;
    protected $connection = 'mysql';
    protected $table = 'message';

    protected $fillable = ['message', 'subject', 'status'];

    public function messageId()
    {
        return $this->belongsTo('App\Modules\Base\Models\Messenger\MessageId', 'id', 'message_id');
    }

    
}
