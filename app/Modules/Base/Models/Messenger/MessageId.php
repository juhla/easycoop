<?php

namespace App\Modules\Base\Models\Messenger;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class MessageId extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'message_ids';

    protected $fillable = ['sender_id', 'recipient_id'];

    public function personalInfo()
    {
        return $this->belongsTo('App\Modules\Base\Models\PersonalInfo', 'recipient_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'recipient_id');
    }

    public function personalInfo2()
    {
        return $this->belongsTo('App\Modules\Base\Models\PersonalInfo', 'sender_id', 'user_id');
    }

}