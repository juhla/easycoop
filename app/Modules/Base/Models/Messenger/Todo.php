<?php

namespace App\Modules\Base\Models\Messenger;

use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;

class Todo extends Model implements AuditableContract
{
    use NullingDB;
    // use SoftDeletes;
    //set connection
    use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'todos';

    protected $fillable = ['todo', 'status'];

    //protected $dates = ['deleted_at'];


    public static function isUserWidget()
    {
        try {
            Todo::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }


    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
