<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Database\Eloquent\Model as EloquentModel;


class Model extends EloquentModel
{

    use NullingDB;
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
        });
        static::updating(function ($model) {
        });
    }



}
