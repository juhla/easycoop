<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Order extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable;
    use Uuids;
    protected $connection = 'tenant_conn';
    protected $table = "ims_orders";
    protected $fillable = [
                              'supplier_id',                                                     
                              'order_number',                                               
                              'issue_date',                                                 
                              'receipt_date',                                               
                              'branch_id',                                                       
                              'location_id',                                                     
                              'bill_address',                                            
                              'ship_address',                                            
                              'order_note',                                                 
                              'internal_notes',                                             
                              'order_by',                                                        
                              'department_id',
                              'sync',
    ];
    public $incrementing = false;

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
