<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class OrderItem extends Model implements AuditableContract
{
    use NullingDB;
    use Uuids;
    use Auditable;
    protected $connection = 'tenant_conn';
    protected $table = "ims_order_items";
    protected $fillable = [
                'order_id',
               'product_id',
               'tax_value',
               'unit_value',
               'price',
               'quantity',
               'total',
               'sync'
    ];
    public $incrementing = false;

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
