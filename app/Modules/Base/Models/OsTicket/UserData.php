<?php

namespace App\Modules\Base\Models\OSTicket;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class UserData extends Model 
{
    use NullingDB;
	 //set connection for model
    protected $connection = 'mysqlOsTicket';

    //set db table
    protected $table = 'ost_user__cdata';

     public $timestamps = false;

    
   
}



