<?php

namespace App\Modules\Base\Models\OSTicket;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class UserOS extends Model 
{
    use NullingDB;
     protected $connection = 'mysqlOsTicket';
     //set db table
     protected $table = 'ost_user';

    const CREATED_AT = 'created';
	const UPDATED_AT = 'updated';
	
    
}



