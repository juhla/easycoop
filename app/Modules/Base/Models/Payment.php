<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Payment extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'payments';

    protected $fillable = ['user_id', 'license_id', 'amount', 'reference_no'];
}
