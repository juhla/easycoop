<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class PaymentTerm extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;

    protected $connection = 'tenant_conn';

    protected $table = 'ca_payment_terms';
    protected $attributes = [
        'flag' => 'Active',
    ];
    protected $fillable = ['name', 'description', 'payment_type', 'days'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

}