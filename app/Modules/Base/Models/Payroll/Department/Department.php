<?php

namespace App\Modules\Base\Models\Payroll\Department;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Department extends Model implements AuditableContract
{
    use NullingDB;
	use SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_department';

    protected $primaryKey = 'department_id';
    //field can not be mass assigned
    protected $dates = ['deleted_at'];
   public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
   
}
