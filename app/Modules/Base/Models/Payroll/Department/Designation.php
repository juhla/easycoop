<?php

namespace App\Modules\Base\Models\Payroll\Department;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Illuminate\Support\Facades\Schema;

use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Designation extends Model implements AuditableContract
{
    use NullingDB;
	use SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_designations';
    //set primary key
    protected $primaryKey = 'designations_id';
    //field can not be mass assigned
    protected $dates = ['deleted_at'];

    /**
     * Establish relationship between Department and Designation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department(){
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Department\Department', 'department_id');
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getTableName()
    {
        return $this->table;
    }
}
