<?php

namespace App\Modules\Base\Models\Payroll\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use App\Modules\Base\Models\Payroll\Settings\Holiday;
use App\Modules\Base\Models\Payroll\Attendance;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;

class Employee extends Model implements AuditableContract
{
    use NullingDB;
	use SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_employee';

    protected $primaryKey = 'employee_id';
    
    //field can not be mass assigned
    protected $dates = ['deleted_at'];

    /**
     * Establish relationship between Designation and Employee
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function designation(){
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Department\Designation', 'designations_id', 'designations_id');
    }

    public function bank(){
        return $this->hasOne('App\Modules\Base\Models\Payroll\Employee\EmployeeBank', 'employee_id');
    }

    public function document(){
        return $this->hasOne('App\Modules\Base\Models\Payroll\Employee\EmployeeDocument', 'employee_id');
    }

    public static function total_attendace_in_month($employee_id, $flag = NULL)
    {
        $month = date('m');
        $year = date('Y');
        $total_holiday = [];
        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = Holiday::where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->get();
            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return count($total_holiday);
            }
        } else {
            $get_total_attendance = Attendance::where('employee_id', $employee_id)->whereBetween('date', [$start_date, $end_date])->count(); // get all attendace by start date and in date
            return $get_total_attendance;
        }
    }
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
    
}
