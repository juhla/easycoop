<?php

namespace App\Modules\Base\Models\Payroll\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class EmployeeAward extends Model implements AuditableContract
{
    use NullingDB;
	use SoftDeletes;
    use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_employee_award';
    //set primary key
    protected $primaryKey = 'employee_award_id';
    //field can not be mass assigned
    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Employee\Employee', 'employee_id', 'employee_id');
    }
   public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
