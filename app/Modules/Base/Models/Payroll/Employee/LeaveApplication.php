<?php

namespace App\Modules\Base\Models\Payroll\Employee;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class LeaveApplication extends Model implements AuditableContract
{	
    use NullingDB;
    use Auditable;
	//use SoftDeletes;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_application_list';
    //set fillables
    protected $fillable = ['employee_id', 'leave_category_id', 'reason', 'start_date', 'end_date'];
   
    //field can not be mass assigned
    //protected $dates = ['deleted_at'];

    public function category(){
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Settings\LeaveCategory', 'leave_category_id', 'leave_category_id');
    }

    public function employee(){
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Employee\Employee', 'employee_id', 'employee_id');
    }
  
  public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }


}
