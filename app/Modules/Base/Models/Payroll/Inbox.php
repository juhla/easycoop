<?php

namespace App\Modules\Base\Models\Payroll;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Inbox extends Model implements AuditableContract
{
    use NullingDB;
	use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'tbl_inbox';

    protected $fillable = ['to', 'subject', 'message_body', 'attach_file', 'attach_file_path', 'attach_filename', 'from'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
 
}
