<?php

namespace App\Modules\Base\Models\Payroll;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Notice extends Model implements AuditableContract
{
    use NullingDB;
	use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'tbl_notices';

    protected $fillable = ['title', 'short_description', 'long_description', 'flag', 'employee_id'];
 
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
