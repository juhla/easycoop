<?php

namespace App\Modules\Base\Models\Payroll\PayrollManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class GeneralSalaryPaySlip extends Model implements AuditableContract
{ 
    use NullingDB;
	use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_general_payslip';


    public static function isGeneralSalaryPaySlip()
    {
        try {
            GeneralSalaryPaySlip::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
    
}
