<?php

namespace App\Modules\Base\Models\Payroll\PayrollManager;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class GeneralSalaryPayment extends Model implements AuditableContract
{
    use NullingDB;
	use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_general_payment';

     /**Establish relationship between Employee and  Salary Payment **/
    public function employee()
    {
        return $this->belongsTo('App\Modules\Base\Models\Payroll\Employee\Employee');
    }
   public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    } 
}
