<?php

namespace App\Modules\Base\Models\Payroll\PayrollManager;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class SalaryPaySlip extends Model implements AuditableContract
{
	use Auditable;
    use NullingDB;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_salary_payslip';

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
    
}
