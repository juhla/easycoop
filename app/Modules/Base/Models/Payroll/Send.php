<?php

namespace App\Modules\Base\Models\Payroll;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Send extends Model implements AuditableContract
{
    use NullingDB;
	use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'tbl_send';

    protected $fillable = ['to', 'subject', 'message_body', 'attach_file', 'attach_file_path', 'attach_filename', 'employee_id'];
    
     public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
