<?php

namespace App\Modules\Base\Models\Payroll\Settings;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class GeneralSetting extends Model implements AuditableContract
{
    use NullingDB;
	use Auditable;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_gsettings';
    //mass assignment fields
    //protected $fillable = ['begins', 'ends', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
