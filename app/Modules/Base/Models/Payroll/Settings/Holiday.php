<?php

namespace App\Modules\Base\Models\Payroll\Settings;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;

class Holiday extends Model implements AuditableContract
{	
    use NullingDB;
	use Auditable;
	use SoftDeletes;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'tbl_holiday';
   //set primary key
    protected $primaryKey = 'holiday_id';
    //field can not be mass assigned
    protected $dates = ['deleted_at'];
   public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
