<?php namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\Collaborator;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class PayrollLicenceSubscriber extends Model
{
    use NullingDB;
    protected $fillable = ['user_id', 'license_id', 'expiry_date', 'count'];
    protected $connection = 'mysql';
    protected $table = 'payroll_license_subscribers';

    public function __construct()
    {
        parent::__construct();
        //self::createTableifNonExist();
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function role()
    {
        return $this->belongsTo('App\Modules\Base\Models\Role', 'role_id');
    }

    public function license()
    {
        return $this->belongsTo('App\Modules\Base\Models\UserLicense', 'license_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'user_id');
    }

    public function findDefaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicence::where('role_id', $role)->where('is_default', 1)->first();
    }

    public static function isExist()
    {
        try {
            PayrollLicenceSubscriber::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

}
