<?php

namespace App\Modules\Base\Models;

use Zizaco\Entrust\EntrustPermission;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Permission extends EntrustPermission
{
    use NullingDB;
    protected $connection = 'mysql';
   // protected $fillable = ['category', 'name', 'display_name'];
}