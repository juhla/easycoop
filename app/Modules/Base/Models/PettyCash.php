<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\NullingDB;

class PettyCash extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    protected $connection = 'tenant_conn';

    protected $table = 'petty_cash';
    protected $attributes = [
        'flag' => 'Active',
        'status' => '0',
    ];

    protected $fillable = ['date', 'category_id', 'pvc_no', 'description', 'amount', 'supplier', 'expense_code', 'created_by', 'updated_by', 'status'];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
