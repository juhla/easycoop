<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\Uuids;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Product extends Model implements AuditableContract
{
    use NullingDB;
    use Uuids;
    use Auditable;
    protected $connection = 'tenant_conn';
    protected $table = "ims_products";
    protected $fillable = [
        'code',
        'name',
        'display_name',
        'description',
        'unit_of_measure_id',
        'size',
        'weight',
        'height',
        'width',
        'color',
        'manufacturer',
        'currency_id',
        'purpose_id',
        'category_id',
        'sub_category_id',
        'image',
        'sync'
    ];
    public $incrementing = false;

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
