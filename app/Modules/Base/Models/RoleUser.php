<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class RoleUser extends Model
{
    use NullingDB;
    //
    protected  $fillable = ['user_id', 'role_id'];
    protected $connection = 'mysql';
    protected $table = 'role_user';

}
