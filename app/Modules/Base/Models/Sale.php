<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\Uuids;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Sale extends Model implements AuditableContract
{
    use NullingDB;
    use Uuids;
    use Auditable;
    protected $connection = 'tenant_conn';
    protected $table = "ims_sales";
    protected $fillable = [
        'customer_id',
        'transaction_id',
        'department_id',
        'branch_id',
        'location_id',
        'sales_date',
        'sales_time',
        'bill_address',
        'ship_address',
        'sales_notes',
        'internal_notes',
        'order_by',
        'bill_no',
        'bill_type',
        'isReversed',
        'is_tax_calculated',
        'sync',
    ];
    public $incrementing = false;

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
