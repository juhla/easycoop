<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class ServiceTime extends Model {
	use NullingDB;
	protected $fillable = [
							'priority',
							'response_time',
							'response_unit',
							'response_time_type',
							'resolution_time',
							'resolution_unit',
							'resolution_time_type'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
    protected $table = 'service_times';

}
