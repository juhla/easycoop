<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class State extends Model
{
    protected $connection = 'mysql';
    protected $table = 'states';

    public function getTableName()
    {
        return $this->table;
    }

}