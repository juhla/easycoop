<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Tax extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Auditable;
    //set connection name
    protected $connection = 'tenant_conn';
    //set table name
    protected $table = 'ca_tax_types';
    protected $attributes = [
        'flag' => 'Active',
    ];

    protected $fillable = ['name', 'abbreviation', 'rate', 'sales_gl_code', 'purchase_gl_code'];

    /**
     * Establish relationship between tax and ledger
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sales_gl_account()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'sales_gl_code');
    }

    public function purchase_gl_account()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'purchase_gl_code');
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}