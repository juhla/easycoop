<?php

namespace App\Modules\Base\Models\Tenant;

use Zizaco\Entrust\EntrustPermission;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Permission extends EntrustPermission
{
    use NullingDB;
    protected $connection = 'tenant_conn';
    protected $table = 'permissions';
   // protected $fillable = ['category', 'name', 'display_name'];
}