<?php

namespace App\Modules\Base\Models\Tenant;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Role extends EntrustRole
{
    use NullingDB;
    //
    protected $fillable = ['display_name', 'category', 'name', 'description', 'is_admin', 'flag'];
    protected $connection = 'tenant_conn';
    protected $table = 'roles';

    public function users()
    {
        return $this->belongsToMany('App\Modules\Base\Models\User', 'role_user');
    }

    public static function getBusinessOwnerUser()
    {
        return Role::where('category', "business-owner")->get(['id', 'name', 'display_name'])->toArray();
    }

    public static function findRoleCategory($role)
    {
        $category = Role::find($role)->category;
        return Role::where('name', $category)->pluck('id')->first();
    }


    public static function findUserRoles()
    {
        return Role::groupby('category')->distinct()->get();
    }


    /**
     * Many-to-Many relations with the permission model.
     * Named "perms" for backwards compatibility. Also because "perms" is short and sweet.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function perms()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Tenant\Permission', 'permission_role');
    }


    public function bmac_perms()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Tenant\Permission', 'permission_role')->where('permissions.module', 'bmac');
    }

    public function ims_perms()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Tenant\Permission', 'permission_role')->where('permissions.module', 'ims');
    }

    /**
     * save tasks details
     * @param $input
     * @return Mixed
     */
    public static function updateRole($input)
    {

        //get input fields for education details
        $info = array(
            'name' => $input['name'],
            'display_name' => $input['display_name'],
            'description' => $input['description'],
            'is_admin' => $input['is_admin'],
            'flag' => $input['flag']
        );
        $permissionBtn = '';
        if ($input['role_id'] === '') {

            //insert new item
            $role = Role::create($info);


            $return_arr['data'] = "<tr>
                        <td>" . $role->name . "</td>
                        <td>" . $role->display_name . "</td>
                        <td>
                             <div class='btn-group'>
                                 <button type='button' class='btn btn-default edit' data-value='" . $role->id . "'><i class='fa fa-pencil'></i></button>
                                 <button type='button' class='btn btn-danger delete' data-value='" . $role->id . "'><i class='fa fa-trash-o'></i> </button>
                             </div>
                        </td>
                    </tr>";
            return $return_arr;
        } else {

            Role::where('id', $input['role_id'])
                ->update($info);
            //get last insert details
            $role = Role::find($input['role_id']);

            $return_arr['data'] = "
                         <td>" . $role->name . "</td>
                        <td>" . $role->display_name . "</td>
                        <td>
                             <div class='btn-group'>
                                 <button type='button' class='btn btn-default edit' data-value='" . $role->id . "'><i class='fa fa-pencil'></i></button>
                                 <button type='button' class='btn btn-danger delete' data-value='" . $role->id . "'><i class='fa fa-trash-o'></i> </button>
                             </div>
                        </td>";
            return $return_arr;
        }
    }
}
