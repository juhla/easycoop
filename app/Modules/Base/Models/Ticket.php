<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Ticket extends Model {
    use NullingDB;
	protected $fillable = [
							'ticket_no',
							'ticket_subject',
							'ticket_description',
							'ticket_status',
							'ticket_priority',
							'ticket_type_id',
							'department_id',
							'user_id',
							'response_due_time',
							'resolution_due_time'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
	protected $table = 'tickets';

    public function ticketType()
    {
        return $this->belongsTo('App\Modules\Base\Models\TicketType');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\Base\Models\User');
    }

	public function assignedUser()
    {
        return $this->belongsToMany('App\Modules\Base\Models\User', 'assigned_tickets', 'ticket_id', 'user_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Modules\Base\Models\Department');
    }

}
