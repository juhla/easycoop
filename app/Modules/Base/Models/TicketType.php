<?php

namespace App\Modules\Base\Models;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class TicketType extends Model {
	use NullingDB;
	protected $fillable = [
							'ticket_type_name'
						];
	protected $primaryKey = 'id';
    protected $connection = 'mysql';
	protected $table = 'ticket_types';

    public function ticket()
    {
        return $this->hasMany('App\Modules\Base\Models\Ticket');
    }
}
