<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\DebitCredit;
use App\Modules\Base\Traits\Uuids;
use Faker\Provider\cs_CZ\DateTime;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\TransactionItem;
use Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Webpatser\Uuid\Uuid;

class Transaction extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable, NullableFields, SoftDeletes, Uuids, DebitCredit;

    protected $connection = 'tenant_conn';
    protected $attributes = [
        'flag' => 'Active',
        'approve_status' => '1',
        'memo_posted' => '0',
    ];
    protected $table = 'ca_transactions';
    public $incrementing = false;
    protected $nullable = [
        'reference_no',
        'description',
        'payment_type',
        'bank_id',
        'cheque_number',
        'customer_vendor',
        'pv_receipt_no',
        'memo_posted',
    ];

    protected $casts = [
        'transaction_type' => 'string'
    ];

    protected $fillable = [
        'id',
        'reference_no',
        'transaction_type', 'transaction_date', 'description',
        'amount', 'payment_type', 'customer_vendor', 'bank_id', 'pv_receipt_no',
        'cheque_number', 'updated_by', 'created_by'
    ];


    public function ledgerByBank()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'bank_id');
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('ca_transactions.flag', 'Active');
        });
        static::creating(function ($model) {
            $model->id = Uuid::generate()->string;
        });

    }

    private static function getMemorandumLedgerIdby()
    {
        //get memorandum ledger id
        $res_ledger = Ledger::whereCode('3400')->get();

        if ($res_ledger->isEmpty()) {
            $res = Ledger::create(
                [
                    'group_id' => '3',
                    'name' => 'Memorandum Account',
                    'code' => '3400',
                    'opening_balance_type' => 'C',
                    'is_system_account' => '1'
                ]
            );

            $memorandumLedgerID = $res->id;
        } else {
            $res_ledger = $res_ledger->first();
            $memorandumLedgerID = $res_ledger->id;
        }
        return $memorandumLedgerID;
    }

    private static function doPosting($input, $transaction_type = null, $payment_type = null)
    {
        //remove commas from amount
        $input['amount'] = str_replace(',', '', $input['amount']);

        $date = Carbon\Carbon::createFromFormat('d/m/Y', $input['trans_date']);
        //get input fields
        $transaction = array(
            'transaction_date' => $date->format('Y-m-d'),
            'description' => $input['description'],
            'amount' => $input['amount'],
            'payment_type' => $payment_type,
        );


        if ($input['type'] == 1) {
            //get memorandum ledger id
            $memorandumLedgerID = self::getMemorandumLedgerIdby();
            // Payment type: 1 Cash | 2 Cheques | 3 credit
            switch (true) {
                case ($transaction_type == 'inflow' && $transaction['payment_type'] == '1'):
                    {
                        $debit_account = Ledger::getLedgerIdFromCode(1261); //cash at hand ledger
                        $credit_account = $memorandumLedgerID;
                    }
                    break;

                case ($transaction_type == 'inflow' && $transaction['payment_type'] == '2'):
                    {
                        $debit_account = $input['bank_id'];
                        $credit_account = $memorandumLedgerID;  //Memorandum ledger
                        $transaction['bank_id'] = $input['bank_id'];
                    }
                    break;


                case ($transaction_type == 'outflow' && $transaction['payment_type'] == '1'):
                    {
                        $debit_account = $memorandumLedgerID;
                        $credit_account = Ledger::getLedgerIdFromCode(1261); //cash at hand ledger

                    }
                    break;

                case ($transaction_type == 'outflow' && $transaction['payment_type'] == '2'):
                    {
                        $debit_account = $memorandumLedgerID;
                        $credit_account = $input['bank_id'];
                        $transaction['bank_id'] = $input['bank_id'];
                    }
                    break;
            }
        } elseif ($input['type'] == 2) {
            // Payment type: 1 Cash | 2 Cheques | 3 credit
            switch (true) {
                case ($transaction_type == 'inflow' && $transaction['payment_type'] == '1'):
                    {
                        $debit_account = Ledger::getLedgerIdFromCode(1261); //cash at hand ledger
                        $credit_account = Ledger::getLedgerIdFromCode(4110); // Sales
                    }
                    break;

                case ($transaction_type == 'inflow' && $transaction['payment_type'] == '2'):
                    {
                        $debit_account = $input['bank_id'];
                        $credit_account = Ledger::getLedgerIdFromCode(4110);  //Sales
                        $transaction['bank_id'] = $input['bank_id'];
                    }
                    break;

                case ($transaction_type == 'inflow' && $transaction['payment_type'] == '3'):
                    {
                        $debit_account = $input['customer_id'];
                        $credit_account = Ledger::getLedgerIdFromCode(4110);  //Sales
                        $transaction['customer_vendor'] = $input['customer_id'];
                    }
                    break;

                case ($transaction_type == 'outflow' && $transaction['payment_type'] == '1'):
                    {
                        $debit_account = Ledger::getLedgerIdFromCode(5101); // Purchases
                        $credit_account = Ledger::getLedgerIdFromCode(1261); //cash at hand ledger
                    }
                    break;

                case ($transaction_type == 'outflow' && $transaction['payment_type'] == '2'):
                    {
                        $debit_account = Ledger::getLedgerIdFromCode(5101); // Purchases
                        $credit_account = $input['bank_id'];
                        $transaction['bank_id'] = $input['bank_id'];
                    }
                    break;

                case ($transaction_type == 'outflow' && $transaction['payment_type'] == '3'):
                    {
                        $debit_account = Ledger::getLedgerIdFromCode(5101); // Purchases
                        $credit_account = $input['vendor_id'];
                        $transaction['customer_vendor'] = $input['vendor_id'];
                    }
                    break;
            }
        }


        $trans_item1 = [
            'ledger_id' => $debit_account,
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'D',
        ];

        $trans_item2 = [
            'ledger_id' => $credit_account,
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'C'
        ];

        $data['transaction'] = $transaction;
        $data['item_one'] = $trans_item1;
        $data['item_two'] = $trans_item2;
        return $data;
    }

    public function saveFullTransaction($input, $transaction_type = null, $payment_type = null)
    {

        $data = self::doPosting($input, $transaction_type, $payment_type);
        $transaction = $data['transaction'];
        $trans_item1 = $data['item_one'];
        $trans_item2 = $data['item_two'];


        $transaction['transaction_type'] = $transaction_type;

        //if reference no is set from input
        if (isset($input['reference_no'])) {
            $transaction['reference_no'] = $input['reference_no'];
        } else {
            $transaction['reference_no'] = 'TRA' . time();
        }

        $formatted = new Transaction($transaction);
        $transactionItem = new TransactionItem($trans_item1);
        $transactionItem2 = new TransactionItem($trans_item2);
        $itemArray = [$transactionItem, $transactionItem2];
        $formatted->item = $itemArray;
        $this->makePosting($formatted);
    }


    public function updateFullTransaction($input, $transaction_type = null, $payment_type = null, $transaction_id = null)
    {

        $data = self::doPosting($input, $transaction_type, $payment_type);
        $transaction = $data['transaction'];
        $trans_item1 = $data['item_one'];
        $trans_item2 = $data['item_two'];


        $formatted = new Transaction($transaction);
        $formatted->id = $transaction_id;
        $transactionItem = new TransactionItem($trans_item1);
        $transactionItem2 = new TransactionItem($trans_item2);
        $itemArray = [$transactionItem, $transactionItem2];
        $formatted->item = $itemArray;
        $this->makePosting($formatted);
    }


    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function item()
    {
        return $this->hasMany('App\Modules\Base\Models\TransactionItem', 'transaction_id');
    }

    public function item_without_scope()
    {
        return $this->hasMany('App\Modules\Base\Models\TransactionItem', 'transaction_id')->withoutGlobalScopes();
    }

    public static function getTheTransaction($transaction_id)
    {
        return Transaction::where('ca_transactions.flag', 'Active')
            ->with(['item'])
            ->where('ca_transactions.id', $transaction_id)
            ->get()->first();

    }

    public static function getMemoID()
    {
        $ledgers = Ledger::where('name', 'Memorandum Account')->first();
        return $ledgers->id;

    }

    public static function getMemorandumTransactions($start_date = null, $end_date = null)
    {

        $ledgerID = self::getMemoID();

        if (!is_null($start_date) && !is_null($end_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');

            $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');

            $res = Transaction::where('ca_transactions.flag', 'Active')
                ->join('ca_transaction_items', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transaction_items.ledger_id', $ledgerID)
                ->where('ca_transactions.memo_posted', 0)
                ->select('ca_transactions.*')
                ->whereBetween('ca_transactions.transaction_date', [$start, $end])
                ->orderBy('ca_transactions.transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::where('ca_transactions.flag', 'Active')
                ->join('ca_transaction_items', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transaction_items.ledger_id', $ledgerID)
                ->where('ca_transactions.memo_posted', 0)
                ->select('ca_transactions.*')
                ->orderBy('ca_transactions.transaction_date', 'desc')
                ->paginate(30);
        }

        //dd($res);
        return $res;

    }

    /**
     * get current company's transactions
     * @param #item_id
     * @return mixed
     */
    public static function getTransaction($item_id = null, $start_date = null, $end_date = null, $type)
    {
        if ($item_id) {
            $res = Transaction::where('id', $item_id)
                ->first();
        } elseif (!is_null($start_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            if (!is_null($end_date)) {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            } else {
                $end = Carbon\Carbon::now();
                //$end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            }
            $res = Transaction::where('flag', 'Active')
                ->where('transaction_type', $type)
                ->whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::whereDate('created_at', '=', date('Y-m-d'))
                ->where('flag', 'Active')
                ->where('transaction_type', $type)
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }


    /**
     * @param $input
     */
    public function saveTransaction($input)
    {

        //remove commas from amount
        $input['amount'] = str_replace(',', '', $input['amount']);
        $date = Carbon\Carbon::createFromFormat('d/m/Y', $input['trans_date']);
        //get input fields
        $transaction = array(
            'transaction_date' => $date->format('Y-m-d'),
            'description' => $input['description'],
            'amount' => $input['amount'],
        );

        if ($input['transaction_type'] !== 'journal') {
            $transaction['payment_type'] = $input['payment_type'];
            $transaction['customer_vendor'] = $input['customer_vendor'];
            $transaction['pv_receipt_no'] = $input['pv_receipt_no'];
            if (isset($input['bank_id'])) {
                $transaction['bank_id'] = $input['bank_id'];
                $transaction['cheque_number'] = $input['cheque_number'];
            }
        }

        $trans_item1 = [
            'ledger_id' => $input['debit_account'],
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'D',
        ];

        $trans_item2 = [
            'ledger_id' => $input['credit_account'],
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'C'
        ];


        if ($input['transaction_id'] === '') {
            //insert new inflow
            $transaction['transaction_type'] = $input['transaction_type'];
            //if reference no is set from input
            if (isset($input['reference_no'])) {
                $transaction['reference_no'] = $input['reference_no'];
            } else {
                $transaction['reference_no'] = 'TRA' . time();
            }
            $formatted = new Transaction($transaction);
            $transactionItem = new TransactionItem($trans_item1);
            $transactionItem2 = new TransactionItem($trans_item2);
            $itemArray = [$transactionItem, $transactionItem2];
            $formatted->item = $itemArray;
            $this->makePosting($formatted);
        } else {

            $transaction['id'] = $input['transaction_id'];
            $formatted = new Transaction($transaction);
            $formatted->id = $input['transaction_id'];
            $transactionItem = new TransactionItem($trans_item1);
            $transactionItem2 = new TransactionItem($trans_item2);
            $itemArray = [$transactionItem, $transactionItem2];
            $formatted->item = $itemArray;
            $this->makePosting($formatted);
        }
    }

    /**
     * Transaction listing (Report)
     * @param  [type] $item_id    [description]
     * @param  [type] $start_date [description]
     * @param  [type] $end_date   [description]
     * @return [type]             [description]
     */
    public static function getTransactionListing($item_id = null, $start_date = null, $end_date = null)
    {
        if (!is_null($start_date)) {
            $start = date('Y-m-d', strtotime($start_date));
            if (is_null($end_date)) {
                $end = date('Y-m-d');
            } else {
                $end = date('Y-m-d', strtotime($end_date));
            }
            $res = Transaction::whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }

    public static function getTransactionDetails($id)
    {
        $res = Transaction::find($id);
        return $res;
    }

    public static function verifyTransactionDate($date)
    {
        $f_year = FiscalYear::where('status', 'Open')->where('flag', 'Active')->first();

        try {
            if ($f_year) {
                $date = Carbon\Carbon::createFromFormat('d/m/Y', $date);
                $new_date = $date->format('Y-m-d');
                if ($new_date >= $f_year->begins && $new_date <= $f_year->ends) {
                    return 'yes';
                } else {
                    return 'no';
                }
            } else {
                return 'no-f-year';
            }
        } catch (\Exception $exception) {
            return 'no-f-year';
        }
    }
}

