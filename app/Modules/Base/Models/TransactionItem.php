<?php namespace App\Modules\Base\Models;

use App\Modules\Base\Traits\Uuids;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use Webpatser\Uuid\Uuid;


class TransactionItem extends Model implements AuditableContract
{

    use Auditable, NullableFields, SoftDeletes, Uuids;
    protected $connection = 'tenant_conn';

    protected $table = 'ca_transaction_items';
    public $incrementing = false;
    protected $attributes = [
        'flag' => 'Active',
        'approve_status' => '1'
    ];
    protected $fillable = ['transaction_id', 'ledger_id', 'amount', 'dc', 'reconciliation_date', 'item_description'];


    protected $nullable = [
        'item_description',
        'reconciliation_date'
    ];

    /**
     * establish relationship between transactions and transactionItems
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transactions()
    {
        return $this->belongsTo('App\Modules\Base\Models\Transaction', 'transaction_id');
    }


    public function transactions_without_scope()
    {
        return $this->belongsTo('App\Modules\Base\Models\Transaction', 'transaction_id')->withoutGlobalScopes();
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    /**
     * Establish relationship between transactionItems and Ledger
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ledger()
    {
        return $this->belongsTo('App\Modules\Base\Models\Ledger', 'ledger_id');
    }


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->whereHas('ledger', function ($q) {
                $q->where('ca_ledgers.flag', 'Active');
            })
                ->where('ca_transaction_items.flag', 'Active');
        });
        static::creating(function ($model) {
            $model->id = Uuid::generate()->string;
        });

    }

    /**
     * scope method to get transaction account type D or C
     * @param $query
     * @param $dc
     * @return mixed
     */
    public function scopeAccount($query, $dc)
    {
        return $query->where('dc', $dc);
    }

    /**
     * Scope Method to filter result by date
     * @param $query
     * @param string $start
     * @param string $end
     * @return mixed
     */
    public function scopeDateRange($query, $start = '', $end = '')
    {
        if ($start) {
            $start_date = date('Y-m-d', strtotime($start));
            if ($end === '') {
                $end_date = date('Y-m-d');
            } else {
                $end_date = date('Y-m-d', strtotime($end));
            }
            return $query->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
        }
    }

    /**
     * get transactions
     * by Paul
     * @param $lid
     * @return mixed
     */
    public static function getTranz($lid)
    {
        $get = Ledger::join('ca_transaction_items', 'ca_ledgers.id', '=', 'ca_transaction_items.ledger_id')
            ->where('ca_transaction_items.ledger_id', $lid)
            ->orderBy('ca_transaction_items.created_at', 'asc')
            ->where('ca_transaction_items.dc', 'D')
            ->select('ca_transaction_items.ledger_id', 'ca_transaction_items.created_at', 'ca_transaction_items.amount', 'ca_ledgers.name', 'ca_ledgers.code')
            ->paginate(20);
        return $get;
    }
}

