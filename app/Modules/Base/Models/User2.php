<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use App\Modules\Base\Notifications\MyOwnResetPassword as ResetPasswordNotification;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class User2 extends Authenticatable implements AuditableContract
{
    use NullingDB;
    use EntrustUserTrait, Notifiable;
    use Auditable;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'confirmation_code', 'is_first_login', 'user_hash', 'session_id'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }


    /**
     * Establish relationship between Users and PersonalInfo models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function personal_info()
    {
        return $this->hasOne('App\Modules\Base\Models\PersonalInfo', 'user_id');
    }

    /**
     * Establish relationship between Users and Company models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne('App\Modules\Base\Models\Company', 'user_id');
    }

    /**
     * Establish relationship between Users and Collaborator models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function collaborations()
    {
        return $this->hasMany('App\Modules\Base\Models\Company', 'user_id', 'client_id');
    }


    public function agents()
    {
        return $this->hasMany('App\Modules\Base\Models\Agents\Agent', 'user_id', 'agent_id');
    }


    /**
     * Get the roles a user has
     */
    public function roles()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Role', 'role_user');
    }


    /**
     * Update user last login time
     */
    public function updateOnline()
    {
        $this->last_active_time = time();
        $this->save();
    }

    /**
     * update user online status
     * @param $s
     * @return bool
     */
    public function updateStatus($s)
    {
        if (!\Auth::user()) return false;
        $this->online_status = $s;

        $this->save();
    }

    /**
     * Check if the user has activated his account
     * @return bool
     */
    public function isVerified()
    {
        if ($this->confirmed == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the account is active
     * @return bool
     */
    public function isActive()
    {
        if ($this->active == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function displayName()
    {
        try {
            if (empty($this->personal_info->first_name) && empty($this->personal_info->last_name)) {
                return $this->email;
            }
            return $this->personal_info->first_name . ' ' . $this->personal_info->last_name;
        } catch (\Exception $e) {
            return $this->email;
            //return '';
        }
    }

    /**
     * Get the name.
     *
     * Had to add this Accessor field so that we can have the ability to do something like this $userInstance->name
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->displayName();
    }

    /**
     * @param $permission
     * @return bool
     *
     */
    public function hasAccess($permission)
    {
        $permissions = @unserialize($this->permissions);
        if (!$permissions) return false;
        if (isset($permissions[$permission]) or in_array($permission, $permissions)) return true;
        return false;
    }


    public function displayAvatar()
    {
        if ($this->profile_pic) {
            return asset('uploads/thumbs/thumb_' . $this->profile_pic);
        } else {
            return asset('images/avatar_default.jpg');
        }
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token, $this->getEmailForPasswordReset()));
    }




}
