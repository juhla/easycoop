<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class UserDashboardWidget extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable;
    //set connection name
    protected $connection = 'tenant_conn';
    //set database table
    protected $table = 'user_dashboard_widgets';

    protected $fillable = ['widget_dashboard_id', 'user_id'];

    public function users()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'user_id');
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function widget()
    {
        return $this->belongsTo('App\Modules\Base\Models\WidgetDashboard', 'widget_dashboard_id');
    }

    public static function getUserWidget($userID)
    {
        $userWidget = UserDashboardWidget::where('user_id', $userID)->select('widget_dashboard_id')->get();
        return $userWidget;
    }

    public static function isUserWidget()
    {
        try {
            UserDashboardWidget::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}