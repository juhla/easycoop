<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Collaborator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\UserLicense;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class UserLicenceSubscriber extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'user_license_subscribers';

    public function getTableName()
    {
        return $this->table;
    }

    public function role()
    {
        return $this->belongsTo('App\Modules\Base\Models\Role', 'role_id');
    }

    public function license()
    {
        return $this->belongsTo('App\Modules\Base\Models\UserLicense', 'license_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\Base\Models\User', 'user_id');
    }

    public function findDefaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicense::where('role_id', $role)->where('is_default', 1)->first();
    }

    public static function updateTheCount($userID)
    {
        $realCount = Collaborator::where('user_id', $userID)
            ->whereIn('status', [1, 2])
            ->where('flag', 'Active')
            ->groupBy('user_id', 'client_id')
            ->whereHas('client.roles', function ($q) {
                $q->whereIn('roles.id', [4, 10, 11, 5, 6, 7, 8]);
            })
            ->get()
            ->count();

        $realCount = $realCount + 1;


        UserLicenceSubscriber::whereUser_id($userID)
            ->update(['count' => $realCount]);
    }

    public static function updateCount($userID)
    {
        $theCounter = UserLicenceSubscriber::where('user_id', $userID)->first();
        if (($theCounter->count == 0) || (is_null($theCounter->count))) {

            $realCount = Collaborator::where('user_id', $userID)
                ->whereIn('status', [1, 2])
                ->where('flag', 'Active')
                ->groupBy('user_id', 'client_id')
                ->whereHas('client.roles', function ($q) {
                    $q->whereIn('roles.id', [4, 10, 11, 5, 6, 7, 8]);
                })
                ->get()
                ->count();

            $realCount = $realCount + 1;


            UserLicenceSubscriber::whereUser_id($userID)
                ->update(['count' => $realCount]);
        }
    }


    public static function findBmacDefaultLicenceByRole()
    {
        return UserLicense::where('is_default', 1)->where('type_id', 4)->first();
    }


    /**
     * Function to validate no of users added
     * Only check user limit when adding a collaborator
     *  exceptions are the premium user collaborators
     * @param $user_id
     * @param $role
     * @return string
     */
    public static function validate($user_id, $role, $input)
    {

        $defaultLicense = self::findBmacDefaultLicenceByRole();


        //get package user is subscribed to
        $res = UserLicenceSubscriber::where('user_id', $user_id)->first();

        // check if its an assembly line email and the role to collaborate is a professional and passthrough
        $assemblyLineEmail = config("constants.assemblyLineEmail");
        $theCOllabEmail = $input['email'];
        if ((in_array($theCOllabEmail, $assemblyLineEmail)) && ($role->name == 'professional')) {
            return 'valid';
        }

        // if you are on a default role and you are trying to collaborate anyone except from employee
        //  throw error
        if (($res->license_id == $defaultLicense->id) && ($role->name != 'employee')) {
            return 'not-allowed';
        }


        //count number of users user has added
        $no_of_users = $res->count;

        if (!in_array($role->name, ['employee-payroll', 'admin-payroll', 'employee-ims'])) {
            if ($res->license->no_of_users > $no_of_users) {
                return 'valid';
            } elseif ($res->license->no_of_users <= $no_of_users) {
                return 'limit-exceeded';
            }
        } else {
            return 'valid';
        }
    }


    /**
     * Function to validate no of users added
     * Only check user limit when adding an employee
     * No limit for influencer and professional
     * @param $user_id
     * @param $role
     * @return string
     */
    public static function validatez($user_id, $role)
    {
        //get package user is subscribed to
        $res = UserLicenceSubscriber::where('user_id', $user_id)->first();
        //count number of users user has added
        $no_of_users = $res->count;

        //check if user is adding an employee
        if ($role === 'employee') {
            if ($res->license->no_of_users > $no_of_users) {
                return 'valid';
            } elseif ($res->license->no_of_users == $no_of_users) {
                return 'limit-exceeded';
            }
        } else {
            return 'valid';
        }
    }

    /**
     * Function to validate no of users added
     * Only check user limit when adding an employee
     * No limit for influencer and professional
     * @param $user_id
     * @param $role
     * @return string
     */
    public static function validates($user_id, $role)
    {
        //get package user is subscribed to
        $res = UserLicenceSubscriber::where('user_id', $user_id)->first();
        //count number of users user has added
        $no_of_users = $res->count;
        switch ($role) {
            case "employee":
                {
                    if ($res->license->no_of_users > $no_of_users) {
                        return 'valid';
                    } elseif ($res->license->no_of_users <= $no_of_users) {
                        return 'limit-exceeded';
                    }
                }
                break;
            case "professional":
                {
                    return 'valid';
                    if ($res->license->no_of_users > $no_of_users) {
                        return 'valid';
                    } elseif ($res->license->no_of_users <= $no_of_users) {
                        return 'limit-exceeded';
                    }
                }
                break;
            case "finance-provider":
            case "trade-promoter":
                {
                    $userLicense = new UserLicense();
                    $role = getUserRole($user_id);
                    $default = $userLicense->findDefaultLicenceByRole($role);

                    if ($default->id != $res->license_id) {
                        if ($res->license->no_of_users > $no_of_users) {
                            return 'valid';
                        } elseif ($res->license->no_of_users <= $no_of_users) {
                            return 'limit-exceeded';
                        }
                    } else {
                        return 'not-allowed';
                    }
                }
                break;
            default:
                break;
        }
    }
}
