<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class UserLicense extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'user_license_packages';

    protected $fillable = ['role_id','title','slug','no_of_users','is_default','description','price','duration','duration_type','status','annual_price'];

//    public function __construct()
//    {
//        //parent::__construct();
//    }

    public function getTableName()
    {
        return $this->table;
    }

    public function role()
    {
        return $this->belongsTo('App\Modules\Base\Models\Role', 'role_id');
    }

    public function findDefaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicense::where('role_id', $role)->where('is_default', 1)->first();
    }

    public static function defaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicense::where('role_id', $role)->where('is_default', 1)->first();
    }

}
