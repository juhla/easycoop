<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;

class UserPriceBundle extends Model
{
    protected $connection = 'mysql';
    protected $table = 'user_price_bundles';
    protected $fillable = ['*'];

}
