<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\Uuids;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Vendor extends Model implements AuditableContract
{
    use NullingDB, SoftDeletes;
    use Uuids;
    use Auditable;
    //set connection name
    protected $connection = 'tenant_conn';
    //set database table
    protected $table = 'ca_vendors';
    protected $attributes = [
        'flag' => 'Active',
        'sync' => '0'
    ];
    public $incrementing = false;
    protected $fillable = ['ledger_code', 'name', 'email', 'first_name', 'last_name', 'currency_code', 'account_no', 'phone_no', 'website', 'country_id', 'address', 'sync'];

    public function ledger()
    {
        return $this->hasOne('App\Modules\Base\Models\Ledger', 'code', 'ledger_code');
    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getTableName()
    {
        return $this->table;
    }
}