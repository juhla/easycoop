<?php

namespace App\Modules\Base\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Widget extends Model
{
    use NullingDB;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mysql';
    protected $table = 'widgets';
    protected $fillable = ['name', 'routes'];

    /**
     * Get the widget a role has
     */
    public function roles()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Role', 'widget_roles');
    }

    /**
     * Get the widget a user has
     */
    public function users()  
    {
        return $this->belongsToMany('App\Modules\Base\Models\User', 'user_widgets');
    }

    private static function getWidgetByRole($roleid)
    {
        if (!is_array($roleid)) {
            $roleid = [$roleid];
        }
        return Widget::whereHas('roles', function ($query) use ($roleid) {
            $query->whereIn('role_id', $roleid);
        })->get();
    }

    private static function getWidgetIDByRole($roleid)
    {
        if (!is_array($roleid)) {
            $roleid = [$roleid];
        }
        return Widget::whereHas('roles', function ($query) use ($roleid) {
            $query->whereIn('role_id', $roleid);
        })->get();
    }

    public static function getWidgetByCategory($categoryID)
    {
        if (!is_array($categoryID)) {
            $categoryID = [$categoryID];
        }
        return Widget::whereHas('roles', function ($query) use ($categoryID) {
            $query->whereIn('category', $categoryID);
        })->get();
    }

    //public static function getWidgetByCategory(){}

    public static function getAllBusinessWidget()
    {
        // category id business-owner
        return self::getWidgetByCategory("business-owner");
    }

    public static function getElementaryBusinessWidget()
    {
        // role id 4 == business owner elementary
        return self::getWidgetByRole(4);
    }

    public static function getBasicBusinessWidget()
    {
        // role id 4 == business owner elementary
        return self::getWidgetByRole(9);

    }

    public static function getUserWidget($auth)
    {
        $userRole = $auth->user()->roles;
        $userRole = $userRole->pluck('id')->all();

        $flatWidgets = array();
        try {
            $widgets = UserWidget::getUserWidget($auth->user()->id)->toArray();
            foreach ($widgets as $key => $eachWidgets) {
                $flatWidgets[$key] = $eachWidgets['widget_id'];
            }
        } catch (\Exception $ex) {
            $flatWidgets = array();
        }


        $roleWidget = self::getWidgetIDByRole($userRole)->toArray();
        $flatRoleWidgets = array();
        foreach ($roleWidget as $key => $eachRoleWidget) {
            $flatRoleWidgets[$key] = $eachRoleWidget['id'];
        }
        $obj_merged = empty($flatWidgets) ? $flatRoleWidgets : $flatWidgets;
        //$obj_merged = array_merge($flatRoleWidgets, $flatWidgets);
        $userWidget = Widget::whereIn('id', $obj_merged)->get();
        return $userWidget;

    }


    public static function isExist()
    {
        try {
            Widget::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    // public static function createTableifNonExist()
    // {

    //     if (!self::isExist()) {
    //         Schema::connection('mysql')->create('custom_widgets', function (Blueprint $table) {
    //             $table->increments('id');
    //         });
    //     }
    // }


}
