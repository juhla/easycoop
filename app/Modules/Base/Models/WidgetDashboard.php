<?php

namespace App\Modules\Base\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class WidgetDashboard extends Model
{
    use NullingDB;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mysql';
    protected $table = 'widget_dashboards';
    protected $fillable = ['name', 'method', 'partials', 'description'];

    /**
     * Get the widget a role has
     */
    public function roles()
    {
        return $this->belongsToMany('App\Modules\Base\Models\Role', 'widget_dashboard_roles');
    }

    /**
     * Get the widget a user has
     */
    public function users()
    {
        return $this->belongsToMany('App\Modules\Base\Models\User', 'user_dashboard_widgets');
    }

    private static function getWidgetByRole($roleid)
    {
        return WidgetDashboard::whereHas('roles', function ($query) use ($roleid) {
            $query->where('role_id', $roleid);
        })->get();
    }

    private static function getWidgetIDByRole($roleid)
    {
        return WidgetDashboard::whereHas('roles', function ($query) use ($roleid) {
            $query->where('role_id', $roleid);
        })->get();
    }

    public static function getWidgetByCategory($categoryID)
    {
        return WidgetDashboard::whereHas('roles', function ($query) use ($categoryID) {
            $query->where('category', $categoryID);
        })->get();
    }


    public static function getAllBusinessWidget()
    {
        // category id business-owner
        return self::getWidgetByCategory("business-owner");
    }

    public static function getElementaryBusinessWidget()
    {
        // role id 4 == business owner elementary
        return self::getWidgetByRole(4);
    }

    public static function getBasicBusinessWidget()
    {
        // role id 4 == business owner elementary
        return self::getWidgetByRole(9);

    }

    public static function getUserWidget($auth)
    {
        $userRole = $auth->user()->roles->first()->id;
        $flatWidgets = array();
        try {
            $widgets = UserDashboardWidget::getUserWidget($auth->user()->id)->toArray();
            foreach ($widgets as $key => $eachWidgets) {
                $flatWidgets[$key] = $eachWidgets['widget_dashboard_id'];
            }
        } catch (\Exception $ex) {
            $flatWidgets = array();
        }
        $roleWidget = self::getWidgetIDByRole($userRole)->toArray();
        $flatRoleWidgets = array();
        foreach ($roleWidget as $key => $eachRoleWidget) {
            $flatRoleWidgets[$key] = $eachRoleWidget['id'];
        }
        $obj_merged = empty($flatWidgets) ? $flatRoleWidgets : $flatWidgets;

        //dd($obj_merged);
        //$obj_merged = array_merge($flatRoleWidgets, $flatWidgets);
        $userWidget = WidgetDashboard::whereIn('id', $obj_merged)->get();
        return $userWidget;

    }


}
