<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class WidgetDashboardRole extends Model
{
    use NullingDB;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mysql';
    protected $fillable = ['widget_dashboard_id', 'role_id'];

    public function role()
    {
        return $this->belongsTo('App\Modules\Base\Models\Role', 'role_id');
    }

    public function widget_dashboard()
    {
        return $this->belongsTo('App\Modules\Base\Models\WidgetDashboard', 'widget_dashboard_id');
    }


}
