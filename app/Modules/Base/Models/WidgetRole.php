<?php

namespace App\Modules\Base\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class WidgetRole extends Model
{
    use NullingDB;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['widget_id', 'role_id'];
    protected $connection = 'mysql';

    public function role()
    {
        return $this->belongsTo('App\Modules\Base\Models\Role', 'role_id');
    }

    public function widget()
    {
        return $this->belongsTo('App\Modules\Base\Models\Widget', 'widget_id');
    }



}
