<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-10
 * Time: 14:11
 */

namespace App\Modules\Base\Observer;


class ActiveToSoftDelete
{
    public function updating($model)
    {
        //dd($model);
        if ($model->isDirty('flag') && ($model->flag == 'Active')) {
            $model->restore();
        }
        if ($model->isDirty('flag') && ($model->flag == 'Inactive')) {
            $model->delete();
        }

    }

//    public function updated($model)
//    {
//        dd($model);
//        if ($model->isDirty('flag') && ($model->flag == 'Active')) {
//            $model->restore();
//        }
//        if ($model->isDirty('flag') && ($model->flag == 'Inactive')) {
//            $model->delete();
//        }
//
//    }

}