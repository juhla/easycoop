<?php

namespace App\Modules\Base\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Modules\Base\Events\BmacPatch' => [
            'App\Modules\Base\Listeners\PatchDatabase',
        ],
        'Illuminate\Mail\Events\MessageSending' => [
            'App\Modules\Base\Listeners\AttachMessageSendingHeader',
        ],
    ];


    /**
     * Register the application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
