<?php

namespace App\Modules\Base\Repositories;

use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;

class MemoRepository
{

    /**
     * save notebooks
     * @param $input
     * @return Mixed
     */
    public static function postPC($input)
    {
        $ids = count($input["register_id"]);
        for ($i = 0; $i < $ids; $i++) {

            $trans_item1 = [
                // 'ledger_id' => $input['debit_account'][$i],
                'ledger_id' => getTransactionIdByLedger($input['debit_account'][$i]),
                'amount' => $input['amount'][$i],
                'dc' => 'D',
            ];

            $trans_item2 = [
                // 'ledger_id' => $input['credit_account'][$i],
                'ledger_id' => getTransactionIdByLedger($input['credit_account'][$i]),
                'amount' => $input['amount'][$i],
                'dc' => 'C'
            ];


            $item = Transaction::where('id', $input['register_id'][$i])->update(['memo_posted' => 1]);
            if ($item) {
                //get transaction number
                $trans_item1['transaction_id'] = $input['register_id'][$i];
                $trans_item2['transaction_id'] = $input['register_id'][$i];
                //insert transaction item
                TransactionItem::create($trans_item1);
                TransactionItem::create($trans_item2);
            }


        }
    }

}