<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{ asset('setup/css/bootstrap.min.css')}}">
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" href="{{ asset('setup/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('setup/css/touch.css')}}">
    <link rel="stylesheet" href="{{ asset('css/validator/validationEngine.jquery.css')}}">
    <title>Accountant</title>
</head>
<body>
<div class="container">

    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid ">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               {{-- <a href="http://www.ikooba.com"><img src="{{ asset('setup/images/ikooba.png')}}"
                                                     class="ikooba-logo"></a>
                                                     --}}
                <span style="font-size:1.6em;font-weight:bolder;color: #0d7d40;">BMAC</span>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class=""><a href="{{url('/workspace')}}" class="">Skip</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">


            <form id="regiration_form" novalidate action="{{url('/acctSetupPost')}}" method="post">
                {{csrf_field()}}
                <fieldset>

                    <div class="row alert alert-info" id="atitle">
                        <div class="col-md-10"><span class="step-count">Set up your business profile in a few simple steps...</span>
                        </div>
                        <div class="col-md-2"><span class="skip-btn">skip</span></div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>

                    <div class="form-group">
                        <label for="company_name">Company Name </label>
                        <input type="text" data-errormessage="company name is required"
                               class="validate[required] form-control" id="company_name" name="company_name"
                               placeholder="Your Company Name...">
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Phone Number </label>
                        <input type="text" data-errormessage="phone number is required"
                               class="validate[required] form-control" id="phone_number" name="phone_number"
                               placeholder="Your Phone Number...">
                    </div>

                    <input type="submit" id="" name="" class="next btn btn-primary btn-lg btn-block" value="Continue"/>
                </fieldset>

            </form>


        </div>
    </div>


</body>
</html>
<script type="text/javascript" src="{{ asset('js/validator/jquery-1.7.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validator/jquery.validationEngine.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/validator/jquery.validationEngine-en.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;
        $(".next").click(function () {
            $("#regiration_form").validationEngine({showOneMessage: true});


            var companyValidate = $("#company_name").validationEngine('validate');
            var phoneValidate = $("#phone_number").validationEngine('validate');


            if (!companyValidate && !phoneValidate) {
                current_step = $(this).parent();
                next_step = $(this).parent().next();
                next_step.show();
                current_step.hide();
                setProgressBar(++current);
            }

        });
        $(".previous").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                    .css("width", percent + "%")
                    .html(percent + "%");
        }
    });
</script>