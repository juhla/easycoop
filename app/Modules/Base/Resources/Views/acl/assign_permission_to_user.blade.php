@extends('layouts.main')
@section('content')
    <style>
        .dd {
            width: 100% !important;
        }
    </style>

    {{--<h2 style="color: blue"> </h2>--}}
    <link href="{{ asset('js/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm" style="font-size: 3em; text-align: center">

                           User Access Permission

 <!--/form-group-->
                    </div>
                </header>
                <section  class="scrollable wrapper w-f" style="padding-top:50px;">
                    <div class="form-group">
                        <label for="branch_id" class="col-sm-3 control-label">User</label>
                        <div class="col-sm-9">
                           {{$full_name}}
                        </div>
                    </div>

<br/>
<br/>
<br/>

                    {!! Form::open(['url' => 'access-control/assign', 'id' => "submitEmployeeForm", 'class'=>'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                    <div  class="dd dd-nodrag" id="nested_accounts">
                        <ol class="dd-list">
                            <?php $category = "";$i = 1; ?>
                            
                            @foreach($permissions as $category_name=>$permission)
                                <li class="dd-item" data-id="1">
            
                                        <div class="dd-handle dd-nodrag">
                                            <input type="checkbox" class="category{{$i}} checkAll{{$i}}" value=""/>  {{ $category_name }}
                                        </div>
                                        @foreach($permission as $key=>$value)

                                        <ol class="dd-list">
                                            <li class="dd-item" data-id="2">
                                                <div class="dd-handle dd-nodrag">
                                                    <input type="checkbox" class="category{{$i}}" value="{{ $value['id'] }}"  name="permissions[]" <?php if(in_array($value['id'], $assignedPermissions)){echo  "checked";}else echo  ""; ?>/> {{  $value['display_name'] }}
                                                </div>
                                            </li>
                                        </ol>
                                        @endforeach
                                    <?php $category = $category_name; $i++?>
                                </li>
                            @endforeach
                            <input type="hidden" value="{{$i}}" id="permission_no"/>
                            <input type="hidden" value="{{$user_id}}" name="user_id"/>

                        </ol>
                    </div>
                    <div class="btn-group col-md-3">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>
                            Submit
                        </button>
                    </div>
                    {!! Form::close() !!}
                </section>
            </section>

        </aside>
    </section>


@stop









@section('scripts')
    <script src="{{ asset('js/scripts/acl.js') }}" type="text/javascript"></script>
  <!--  <script src="{{ asset('js/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>-->
    <script type="text/javascript">
        $(function(){
            var baseurl = $('#baseurl').val();


            $('#nested_accounts').nestable().nestable('collapseAll');;
            $(".dd-nodrag").on("mousedown", function(event) { // mousedown prevent nestable click
                event.preventDefault();
                return false;
            });

        });

        var permission_no = $("#permission_no").val();
        var s = 0;
        for(var i = 1; i <= permission_no; i++){
            $('.checkAll'+i).on('change', {"id": i}, function (e) {
                var m = "category"+e.data.id;//alert(e.data.id);
                $("input."+m+":checkbox").prop('checked', $(this).prop("checked"));
            });
        }
 //   $("#permission_no").val(s)

    </script>
@stop