@extends('layouts.main')

@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Full Name</th>
                                    <th width="15%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($business) > 0)
                                    @foreach($business as $eachbusiness)
                                        <tr>
                                            <td>{{ $eachbusiness->company_name }}</td>
                                            <td>{{ $eachbusiness->title }} {{ $eachbusiness->last_name }} {{ $eachbusiness->first_name }}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">

                                                    <a title="Switch Business"
                                                       href="{{ url('dashboard/change-company?slug='.$eachbusiness->database) }}"
                                                       class="btn btn-sm btn-primary">
                                                        <i class="fa fa-plus"></i> Switch Business
                                                    </a>
                                                    @if(auth()->user()->hasRole(array('finance-provider')))
                                                        <a href="javascript:;" class="btn btn-danger delete-agent"
                                                           data-value="{{ $eachbusiness->sme_id }}" title="Delete Agent"><i
                                                                    class="fa fa-trash-o"></i> Unlink Business</a>
                                                    @endif


                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">You dont have any SMEs yet.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop

@section('scripts')
    <script>


        var baseurl = $('#baseurl').val();

        $(document).on("click", ".delete-agent", function (event) {

            var ID = $(this).attr('data-value');
            var $this = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the data once deleted!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: !1
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'GET',
                        url: baseurl + '/setup/agents/delete/' + ID + '/{{$agent_id}}',
                        context: this,
                        success: function () {
                            swal({
                                title: "Deleted!",
                                text: "item was deleted successfully",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    $this.fadeOut('slow', function () {
                                        $this.closest("tr").remove();
                                    });
                                }
                            });
                            window.location.href = baseurl + '/setup/agents/get/{{$agent_id}}';
                        }
                    })
                    ;
                }
            });
        });
    </script>
@stop
