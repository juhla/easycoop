@extends('layouts.main')

@section('content')
    <section class="panel panel-default">
        @include('flash::message')
        <header class="panel-heading font-bold">Assign an Agent to SME</header>
        <div class="panel-body">
            <form role="form" autocomplete="off" method="post"
                  action="{{url('setup/agents/save_map_agent')}}" data-validate="parsley">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="form-group form-group-default form-group-default-select2">
                    <label class="">Agent</label>
                    <select class="select2-option" name="agent" id="status" style="width: 100%">
                        @foreach($agents as $eachAgents):
                        <option value="{{$eachAgents->id}}">{{$eachAgents->agent}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group form-group-default form-group-default-select2">
                    <label class="">Business</label>
                    <select multiple="multiple" class="select2-option" name="sme[]"
                            style="width: 100%">
                        @foreach($business as $eachBusiness):
                        <option value="{{$eachBusiness->id}}">{{$eachBusiness->name}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="clearfix"></div>
                <button type="submit" class="btn btn-primary" id="saving">Save</button>

            </form>
        </div>
    </section>

@stop

