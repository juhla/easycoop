@extends('layouts.main')

@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            <button type="button" class="btn btn-sm btn-danger delete-selected" title="Remove" disabled>
                                <i class="fa fa-trash-o"></i>
                                Delete
                            </button>
                            <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#AgentSettings">
                                <i class="fa fa-plus"></i> Add New
                            </a>


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <input type="hidden" value="{{url("/setup/agentsSme/delete")}}" id="deleteUrl" >
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">

                                <thead>
                                <tr>
                                    <th>Agent Email</th>
                                    <th width="85%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($agentBusinesses) > 0)
                                    @foreach($agentBusinesses as $agent)
                                        <tr>
                                            <td>{{ $agent->agentEmail }}</td>
                                            <td>
                                                <table class="table table-striped m-b-none">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: center">Businesses</th>
                                                        <th width="15%"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($agent) > 0)
                                                        @foreach($agent as $agentBusiness)
                                                            <tr>
                                                                <td>{{ $agentBusiness->company_name }}</td>

                                                                <td>
                                                                    <div class="btn-group btn-group-xs">

                                                                        <a href="javascript:;" class="btn btn-danger delete-agent"
                                                                           data-agent="{{ $agentBusiness->agent_id }}" data-sme="{{$agentBusiness->sme_id}}" title="Delete Agent"><i
                                                                                    class="fa fa-trash-o "></i></a>
                                                                    </div>
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="2">Agent Does not have any Businesses.</td>
                                                        </tr>
                                                    @endif


                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>


                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="2">You have not added any Agent.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('agents.partials._finance-provider-agent-form')

@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script>

    $(document).ready(function(){
        $(document).on('click', '.delete-agent', function () {
            var agent = $(this).attr("data-agent");
            var sme = $(this).attr("data-sme");
            //alert(v);
            var r = confirm("Are you sure you want to un-assign this business?");
            if(r){
                var url = $("#deleteUrl").val();
                $.get(url, {'agent_id':agent,'sme_id':sme })
                    .done(function(e){
                        if(e == '200'){
                            alert('This business have been successfully been un-assigned');
                            location.reload();
                        }else{
                            alert('An Error occur while trying to un-assign this business');
                        }
                    })
                    .fail(function(){
                        alert('error');
                    });
            }

        });
    });
</script>

