@extends('layouts.main')

@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            <button type="button" class="btn btn-sm btn-danger delete-selected" title="Remove" disabled>
                                <i class="fa fa-trash-o"></i>
                                Delete
                            </button>
                            <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#AgentSettings">
                                <i class="fa fa-plus"></i> Add New
                            </a>


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th width="15%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($agents) > 0)
                                    @foreach($agents as $agent)
                                        <tr>
                                            <td>{{ $agent->name }}</td>
                                            <td>{{ $agent->email }}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{url('setup/agents/get/'.$agent->agent_id)}}"
                                                       class="btn btn-default edit-agent"
                                                       title="View Agent"><i
                                                                class="fa fa-pencil"></i></a>
                                                    <a href="javascript:;" class="btn btn-danger delete-agent"
                                                       data-value="{{ $agent->agent_id }}" title="Delete Agent"><i
                                                                class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">You have not added any Agent.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('agents.partials._finance-provider-agent-form')

@stop

