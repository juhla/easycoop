<div id="AgentSettings" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-content-widget">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5><span id="title">Add</span> <span class="semi-bold">Agent</span></h5>
            </div>
            <div class="modal-body">
                <!-- <div id="widgetdashboardBody"></div> -->

                <form action="{{ url('setup/agents/store') }}" method="post" id="AgentForm" data-validate="parsley">
                    <div class="form-group-attached">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Last Name <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="last_name" id="name" data-required required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label> First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Email <span class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email" data-validate data-type="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Password <span class="required">*</span></label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Confirm Password <span class="required">*</span></label>
                                    <input type="password" class="form-control" name="password_confirmation" id="confirm_password">
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="form-group">
                        <button type="submit" id="submitAgent" class="btn btn-lg btn-primary"><i
                                    class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none"></i> Create
                            Agent
                        </button>
                    </div>
                </form>


            </div>
        </div>

    </div>
</div>
<?php $success = URL("agents");?>

<script>


    var baseurl = $('#baseurl').val();

    $(document).on("submit", "#AgentForm", function (event) {
        event.preventDefault();
        $('#submitAgent').attr('disabled', true);
        $('.spinner').show();
        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.ajax({
            url: url,
            data: data,
            type: "GET",
            dataType: 'json',
            success: function (e) {
                if (e.type === 'validator_fails') {
                    displayNotification(e.message, 'Error', 'error');
                    $('#submitAgent').attr('disabled', false);
                    $('.spinner').hide();
                } else if (e.type === 'success') {
                    displayNotification('Agent Has been created', 'Success!', 'success');
                    $('#submitAgent').attr('disabled', false);
                    $('.spinner').hide();
                    window.location.href = baseurl + '/setup/agents';
                }
            },
            error: function (e) {
                addMessage("Error !, something went wrong");
            }
        });

    });


    $(document).on("click", ".delete-vendor", function (event) {

        var ID = $(this).attr('data-value');
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the data once deleted!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'GET',
                    url: baseurl + '/setup/vendors/delete/' + ID,
                    context: this,
                    success: function () {
                        swal({
                            title: "Deleted!",
                            text: "item was deleted successfully",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $this.fadeOut('slow', function () {
                                    $this.closest("tr").remove();
                                });
                            }
                        });
                    }
                });
            }
        });
    });
</script>


<style>
    .modal-content-widget {
        color: #000000 !important;
    }
</style>
