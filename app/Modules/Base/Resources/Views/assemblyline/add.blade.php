@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">

                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/receivables/pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/receivables/xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                <button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>--}}
                            <a href="{{ url('collaborators/list') }}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to List </a>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">

                    <div class="row">
                        <div class="col-sm-6">
                            @include('flash::message')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <section class="panel panel-default">

                                <header class="panel-heading font-bold">Company Information</header>
                                <form role="form" action="{{ url('assemblyline/add-new') }}" method="post" data-validate="parsley">

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>{{$company_name}}'s'  Rc Number</label>
                                        <input type="text" name="rc_number" class="form-control" value="{{ $rc_number }}" data-required>
                                    </div>

                                </div>

                                <header class="panel-heading font-bold">Enter User Details</header>
                                <div class="panel-body">


                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                                        <div class="form-group">
                                            <label class="control-label" for="id_perm_type">
                                                Access Type
                                            </label>
                                            <div class="controls">
                                                <ul style="list-style: none">
                                                    <li>
                                                        <label for="id_perm_type_0">
                                                            <input checked="checked" id="id_perm_type_0" name="permission" type="radio" value="view" tabindex="4"> view only
                                                        </label>
                                                        <span class="help-block">If you want someone to view — but not change — your information</span>
                                                    </li>
                                                    <li>
                                                        <label for="id_perm_type_1">
                                                            <input id="id_perm_type_1" name="permission" type="radio" value="edit" tabindex="5"> view and edit
                                                        </label>
                                                        <span class="help-block">If you want someone to be able to see <em>and</em> change your information</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- END PANEL -->
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <a href="{{ url('collaborators/list') }}" class="btn btn-default">Cancel</a>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>

                                </div>
                                </form>
                            </section>
                        </div>
                        <div class="col-md-6">
                            <!-- START PANEL -->
                            <div class="panel panel-default">
                                <blockquote>
                                    <p>You can invite a guest to see or collaborate on your financial information.
                                        Only give access to people you trust, since they will see your past transactions.
                                        Learn more in our Guest Collaborator FAQ.</p>
                                </blockquote>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>
                </section>
                <footer class="footer bg-white b-t">
                    {{--<div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>--}}
                </footer>
            </section>
        </aside>
    </section>
@stop
