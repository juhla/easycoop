@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Audit Log</h3>
            </div>
            <div class="container">
                <span>Click on Data to view more information pertaining to a log</span>
            </div>
            <div class="row" style="margin-top: 10px;">
                <form id="filter_ledger" action="{{url('data-search')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="col-sm-3 m-b-xs">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control datepicker" id="start_date"
                                   name="start_date" value="" placeholder="From:">
                            <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                        </div>
                    </div>
                    <div class="col-sm-3 m-b-xs">
                        <div class="input-group">
                            <input type="text" class="input-sm form-control datepicker" name="end_date" id="end_date"
                                   value="" placeholder="To:">
                            <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                        </div>
                    </div>
                    <div class="btn-group col-md-3">
                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Submit</button>

                    </div>
                </form>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-12">
                    <?php if(!empty($logs)): ?>

                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-responsive">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Event</th>

                                <th>Action</th>
                                <th></th>
                                <th>Model</th>
                                <th></th>
                                <th>Date</th>
                            </tr>
                            </thead>

                            <tbody id="terms">
                            @foreach($logs as $log)
                                <tr>

                                    {{--<td>{{$log->users->email}}</td>--}}
                                    <td><?php echo ($log->users) ? $log->users->email : $log->user_id   ?></td>

                                    <td>{{ $log->event}}</td>
                                    <td><a href="data-log/{{$log->id}}">Data</a></td>
                                    <td>On</td>
                                    <td>{{$log->auditable_type}}</td>
                                    <td>On</td>
                                    <td>{{ date('M j, Y h:ia', strtotime($log->created_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {{$logs->links()}}
                            <div class="text-center">
                                Showing {{$logs->currentPage()}} of {{ $logs->lastPage()}}
                            </div>
                        </div>
                    </section>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4">

                    <div class="text-center">


                    </div>
                </div>
            </div>
        </section>
    </section>
@stop

