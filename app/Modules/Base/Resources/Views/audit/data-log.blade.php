@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Data Log</h3>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-12">
                    <span>Old values</span>
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}

                        <table class="table table-responsive">
                            
                            <thead>
                                <tr>
                                    <th>Key</th>
                                    <th>Value</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="terms">
                               
                            <tr>
                                
                                @foreach($old as $key => $value)
                                <td>{{$key}}</td>
                                </td><td>{{ $value}}</td>
                                
                            </tr>

                               @endforeach 
                              
                            </tbody>
                        </table>
                        
                    </section>
                    <span>New values</span>
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-responsive">
                            
                            <thead>
                                <tr>
                                    <th>Key</th>
                                    <th>Value</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="terms">
                               
                            <tr>
                                
                                @foreach($arr as $key => $value)
                                <td>{{$key}}</td>
                                </td><td>{{ $value}}</td>
                                
                            </tr>

                               @endforeach 
                              
                            </tbody>
                        </table>
                        
                    </section>
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}

                        <table class="table table-responsive">
                            
                            <thead>
                                <tr>
                                    
                                    <th></th>
                                    
                                </tr>
                            </thead>
                            <tbody id="terms">
                               
                            <tr>
                                
                               
                                <td><strong>Url</strong></td><td>{{ $log->url}}</td>
                                
                                
                            </tr>

                               
                              
                            </tbody>
                        </table>
                        
                    </section>
                </div>
                <div class="col-sm-4">
                   
                    
                </div>
            </div>
        </section>
    </section>
@stop

