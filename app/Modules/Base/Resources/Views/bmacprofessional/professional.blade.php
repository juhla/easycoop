<!DOCTYPE html>
<html >
<head>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="http://ikooba.com/assets/images/logo.png" type="image/x-icon">
  <meta name="description" content="">
  <title>BMAC Professional</title>
  <link rel="stylesheet" href="{{asset('professional/assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/tether/tether.min.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/bootstrap/css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/bootstrap/css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/socicon/css/styles.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/animatecss/animate.min.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/theme/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('professional/assets/mobirise/css/mbr-additional.css')}}" type="text/css">
  
  
  
  
</head>
<body>
  <section class="header9 cid-r4XnaaEY0r mbr-fullscreen mbr-parallax-background" id="header9-y">

    

    

    <div class="container">
         <div class="mbr-section-btn align-left"><a  style="background: #cfbf0f" class="btn btn-md btn-info display-4" href="https://onebmac.com">Home</a></div>
        <div class="media-container-column mbr-white col-md-8">
            <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1">
                 BMAC <br>Professional Network&nbsp;</h1>
           
            <p class="mbr-text pb-3 mbr-fonts-style display-7" style="text-align: justify">Professional services such as Accounting and Bookkeeping are not a priority for most entrepreneurs. It's time consuming, boring and energy-sapping. Business owners would much rather be out there doing what they do best – providing excellent  goods and services. Finance is more attractive due to the immediate or urgent importance to businesses – hence the BMAC Professionals' Network.<br><br>


            
            The BMAC Professionals’ Network is a virtual network where;
            <ul style="color:#767676;font-size: 18px;text-align:justify">
                <li> Professionals meet businesses who need Professional Services</li>
                <li> Professionals can serve more clients anywhere, anytime</li>
                <li> Professionals can connect with other professionals</li>
                <li>Professionals can exchange ideas with other professionals</li>
            </ul>


            </p>
            <div class="mbr-section-btn align-left"><a class="btn btn-md btn-info display-4" href="https://bmacaccnetwork1.typeform.com/to/tyyKbx">Register</a></div>
        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="engine"><a href="https://mobiri.se/w">free html5 templates</a></section><section class="features9 cid-r4Xw3oyaWv" id="features9-z">

    

    
    <div class="container">
        <div class="row justify-content-center">
            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-search" style="color: rgb(9, 122, 71);"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">Vision</h4>
                        <p class="mbr-text mbr-fonts-style display-7" style="text-align: justify"> To deliver exceptional value (Profitability, Access to finance, structure and Dynamism) for the clients by leveraging on BMAC solutions which are Accounting, Inventory Management System (IMS), HR-Payroll.
                        </p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-rocket" style="color: rgb(9, 122, 71);"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-5">
                            Mission</h4>
                        <p class="mbr-text mbr-fonts-style display-7" style="text-align: justify">
                           To create a virtual network of Business Professionals who provide support to businesses up and down Nigeria using the BMAC Solutions in a spirit of transparency, integrity, accuracy, innovation and collaboration.</p>
                    </div>
                </div>
            </div>

            

            
        </div>
    </div>
</section>

<section class="features11 cid-r4XBdgb3ym" id="features11-13">

    


    <div class="container" style="margin-top: -50px">   
        <div class="col-md-12">


          <h1 class="mbr-section-title align-center mbr-bold pb-3 mbr-fonts-style display-1"> Objectives of the Professional Network</h1>
          <br><br>
            <div class="media-container-row">
                <div class="mbr-figure" style="width: 50%;">
                  
                    <img src="{{asset('professional/assets/images/background1.png')}}" alt="Mobirise">
                    <br><br>
                     <img src="{{asset('professional/assets/images/background3.png')}}" alt="Mobirise">
                </div>

        


                <div class=" align-left aside-content">
                    
                    <div class="mbr-section-text">
                        <p class="mbr-text mb-5 pt-3 mbr-light mbr-fonts-style display-5"></p>
                    </div>

                    <div class="block-content">
                        <div class="card p-3 pr-3">
                            <div class="media">
                                <div class=" align-self-center card-img pb-3">
                                    <span class="mbr-iconfont mbri-edit" style="color: rgb(9, 122, 71);"></span>
                                </div>     
                                <div class="media-body">
                                    <h4 class="card-title mbr-fonts-style display-7">&nbsp;To Professionals</h4>
                                </div>
                            </div>                

                            <div class="card-box">
                          <p class="block-text mbr-fonts-style display-7" style="text-align: justify">

                            <ol type="a" style="font-family: Rubik; font-size:18px;color:#767676;text-align: justify">
                                <li> Empower Accountants</li>
                                <li> Pros are easily connected to Businesses</li>
                                <li>Create a pool of knowledge for businesses and professionals by virtue of cataloguing enquiries and knowledge-oriented interactions</li>
                                <li>Increase ability to partner with other Accountants in other regions</li>
                            </ol>


                        </p>
                            </div>
                        </div>

                        <div class="card p-3 pr-3">
                            <div class="media">
                                <div class="align-self-center card-img pb-3">
                                    <span class="mbr-iconfont mbri-shopping-basket" style="color: rgb(9, 122, 71);"></span>
                                </div>     
                                <div class="media-body">
                                    <h4 class="card-title mbr-fonts-style display-7">To SMEs around Nigeria</h4>


                                </div>
                            </div>                

                            <div class="card-box">
                                <p class="block-text mbr-fonts-style display-7" style="text-align: justify">
                                      <ol type="a" style="font-family: Rubik; font-size:18px;color:#767676;text-align: justify">
                                        <li>
                                        Assembly Line philosophy: ability for one person to service a group of businesses at a reduced cost</li>

                                        <li>
                                          In every local, create a pool of Professionals that Businesses in those localities can contact and hire at affordable rates - anywhere they may be in the country
                                        </li>

                                      </ol>

                                  
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>          
</section>

<section once="" class="cid-r4XClUk7FS" id="footer7-14">

    

    

    <div class="container">
        <div class="media-container-row align-center mbr-white">
         
            <div class="row social-row">
               
            <div class="row row-copirayt">
                <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                  <a class="copy" href="https://onebmac.com/">  © Copyright 2018 Ikooba Technologies - All Rights Reserved </a>
                </p>
            </div>
        </div>
    </div>
</section>


  <script src="{{asset('professional/assets/web/assets/jquery/jquery.min.')}}"></script>
  <script src="{{asset('professional/assets/popper/popper.min.')}}"></script>
  <script src="{{asset('professional/assets/tether/tether.min.')}}"></script>
  <script src="{{asset('professional/assets/bootstrap/js/bootstrap.min.')}}"></script>
  <script src="{{asset('professional/assets/parallax/jarallax.min.')}}"></script>
  <script src="{{asset('professional/assets/viewportchecker/jquery.viewportchecker.')}}"></script>
  <script src="{{asset('professional/assets/smoothscroll/smooth-scroll.')}}"></script>
  <script src="{{asset('professional/assets/theme/js/script.js')}}"></script>
  
  
  <input name="animation" type="hidden">
  </body>
</html>