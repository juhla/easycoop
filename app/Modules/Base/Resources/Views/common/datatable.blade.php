{{--{!! showMessage() !!}--}}
<div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
        <tr>
            @foreach($col_heads as $col_head)
                @if($col_head == 'Option')
                    <th style="width:120px;">{!! $col_head !!}</th>
                @else
                    <th>{!! $col_head !!}</th>
                @endif
            @endforeach
        </tr>
        </thead>
        @if(isset($col_data))
            <tbody>
            @foreach($col_data as $eachdata)
                <tr>
                    @foreach($eachdata as $col_foot)
                        <th>{!! $col_foot !!}</th>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        @endif
    </table>
</div>