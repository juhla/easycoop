

<section class="panel panel-default">
    <header class="panel-heading font-bold">Bank Balances</header>
    <div class="panel-body">
        <div>
            <span class="text-muted">Total:</span>
            <span class="h3 block">₦{{ formatNumber($total_account_balance) }}</span>
        </div>
        <div class="row m-t-sm">
            <table class="table table-striped m-b-none"> <thead>
                <tr>
                    <th>Account</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($bank_accounts as $bank_account)
                    <tr>
                        <td class="text-muted block">{{ $bank_account['name'] }} </td>
                        <td>₦{{ formatNumber($bank_account['account_balance']) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>