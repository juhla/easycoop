<div class="row">
    <div class="col-md-12 m-b-10">
        <div class="widget-9 panel no-border bg-white no-margin widget-loader-bar">
            <div class="container-xs-height full-height">
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="panel-heading  top-left top-right">
                            <div class="panel-title text-black">
                                <span class="font-montserrat fs-11 all-caps">Business Activities</span>
                            </div>
                            <div class="panel-controls">
                                <ul>
                                    <li><a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-xs-height">
                    <div class="col-xs-height col-top">
                        <div class="">
                            <h3 class="no-margin p-b-5 text-white">&nbsp;</h3>
                            <a href="#" class="btn-circle-arrow text-black"><i class="pg-arrow_right"></i> </a>
                            <span class="small hint-text text-black">Last transaction was posted <span id='latest_transaction_date'> {{ $latest_transaction['created_at'] }} </span> </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
