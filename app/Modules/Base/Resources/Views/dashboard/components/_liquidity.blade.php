

<div class="col-sm-6 col-md-3 padder-v b-r b-light bg-warning" style="height: 260px">
    <div id='current_ratio' style=" position: relative; top: 0px; left: 0px;"></div>
    <input type="hidden" id="currentRatio" value="{{ $currentRatio }}" />
</div>
<div class="col-sm-6 col-md-3 padder-v b-r b-light lt bg-dark" style="height: 260px">
    <div id='acid_test' style=" position: relative; top: 0px; left: 0px;"></div>
    <input type="hidden" id="acidTestRatio" value="{{ $acidTestRatio }}" />
</div>
<div class="col-sm-6 col-md-3 padder-v b-r b-light bg-primary" style="height: 260px">
    <div id='debtor' style=" position: relative; top: 0px; left: 0px;"></div>
    <input type="hidden" id="currentDebtorPeriod" value="{{ $currentDebtorPeriod }}" />
</div>
<div class="col-sm-6 col-md-3 padder-v b-r b-light lt bg-danger" style="height: 260px">
    <div id='creditor' style=" position: relative; top: 0px; left: 0px;"></div>
    <input type="hidden" id="currentCreditorPeriod" value="{{ $currentCreditorPeriod }}" />
</div>
{{--<div class="wrapper-lg">--}}
    {{--<div class="text-center">--}}
        {{--<div class="inline m">--}}
            {{--<div id='current_ratio' style=" position: relative; top: 0px; left: 0px;"></div>--}}
            {{--<input type="hidden" id="currentRatio" value="{{ $currentRatio }}" />--}}
        {{--</div>--}}
        {{--<div class="inline m">--}}
            {{--<div id='acid_test' style=" position: relative; top: 0px; left: 0px;"></div>--}}
            {{--<input type="hidden" id="acidTestRatio" value="{{ $acidTestRatio }}" />--}}
        {{--</div>--}}
        {{--<div class="inline m">--}}
            {{--<div id='debtor' style=" position: relative; top: 0px; left: 0px;"></div>--}}
            {{--<input type="hidden" id="currentDebtorPeriod" value="{{ $currentDebtorPeriod }}" />--}}
        {{--</div>--}}
        {{--<div class="inline m">--}}
            {{--<div id='creditor' style=" position: relative; top: 0px; left: 0px;"></div>--}}
            {{--<input type="hidden" id="currentCreditorPeriod" value="{{ $currentCreditorPeriod }}" />--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}