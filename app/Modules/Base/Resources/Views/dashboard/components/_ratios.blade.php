<!-- Nav tabs -->
<ul class="nav nav-tabs nav-tabs-linetriangle">
    <li class="active">
        <a data-toggle="tab" href="#profitability-pane"><span>Profitability Ratio</span></a>
    </li>
    <li>
        <a data-toggle="tab" href="#efficiency-pane"><span>Efficiency Ratio</span></a>
    </li>
    <li>
        <a data-toggle="tab" href="#stability-pane"><span>Stability Ratio</span></a>
    </li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="profitability-pane">
        <div class="row column-seperation">
            <div class="col-md-12">
                <div id='profitability' style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="efficiency-pane">
        <div class="row">
            <div class="col-md-12">
                <div id='efficiency' style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="stability-pane">
        <div class="row">
            <div class="col-md-12">
                <div id='stability' style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
            </div>
        </div>
    </div>
</div>