@extends('layouts.main')

@section('content')
    <link href="{{ asset('js/jqwidgets/styles/jqx.base.css') }}" rel="stylesheet" type="text/css"/>
    <section class="vbox">
        <section class="scrollable">
            <div class="wrapper-lg">
                <h3 class="m-b-xs font-bold m-t-none">
                    @if(!auth()->user()->hasRole(array('social-member', 'employee','business-owner', 'admin', 'advanced-business-owner', 'basic-business-owner')))
                        {{ session()->get('company_name') }} Dashboard
                    @endif
                </h3>
                <div class="text-muted text-sm">
                    @if(count($latest_transaction) > 0)
                        <span class="small hint-text text-black">Last transaction was posted <span
                                    id='latest_transaction_date'> {{ $latest_transaction['created_at'] }} </span> </span>
                    @endif
                </div>
            </div>
            <section class="panel panel-default">
                <div class="row m-l-none m-r-none bg-light lter">
                    @include('dashboard.components._liquidity')
                </div>
            </section>
            <ul class="nav nav-tabs">
                <li class="active m-l-lg"><a href="#sales" data-toggle="tab">Profitability Ratio</a></li>
                <li><a href="#revenue" data-toggle="tab">Efficiency Ratio</a></li>
                <li><a href="#orders" data-toggle="tab">Stability Ratio</a></li>
            </ul>
            <div class="wrapper-lg bg-white b-b b-light">
                <div class="tab-content">
                    <div class="tab-pane active" id="sales">
                        <div id='profitability'
                             style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
                    </div>
                    <div class="tab-pane" id="revenue">
                        <div id='efficiency'
                             style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
                    </div>
                    <div class="tab-pane" id="orders">
                        <div id='stability'
                             style="margin-top: 10px; width: 100%; height: 400px; position: relative;"></div>
                    </div>
                </div>

            </div>
            <div class="hbox">
                <aside>
                    @include('dashboard.components._bank_balances')
                </aside>
                <aside class="">
                    @include('dashboard.components._financial_summary')
                </aside>
            </div>
        </section>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/jqwidgets/jqxcore.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jqwidgets/jqxchart.core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jqwidgets/jqxdata.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jqwidgets/jqxdraw.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jqwidgets/jqxgauge.js') }}"></script>
    <script type="text/javascript">
        var baseurl = $('#baseurl').val();

        $(document).ready(function () {

            var currentDebtorPeriod = $('#currentDebtorPeriod').val();
            var currentCreditorPeriod = $('#currentCreditorPeriod').val();
            var currentRatio = $('#currentRatio').val();
            var acidTestRatio = $('#acidTestRatio').val();
            // prepare chart data as an array

            $('#current_ratio').jqxGauge({
                ranges: [
                    {
                        startValue: 2,
                        endValue: 3,
                        style: {fill: '#4cb848', stroke: '#4cb848'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 1.5,
                        endValue: 2,
                        style: {fill: '#ff8000', stroke: '#ff8000'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 0,
                        endValue: 1.5,
                        style: {fill: '#e53d37', stroke: '#e53d37'},
                        startDistance: 0,
                        endDistance: 0
                    }],
                cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
                caption: {offset: [0, 10], value: 'Current Ratio', position: 'bottom'},
                border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
                ticksMinor: {interval: 0.05, size: '5%'},
                ticksMajor: {interval: 0.25, size: '10%'},
                labels: {position: 'outside', interval: 0.5},
                pointer: {style: {fill: '#2e79bb'}, width: 5},
                animationDuration: 1500,
                width: '100%',
                height: '100%',
                max: 3,
            });

            $('#acid_test').jqxGauge({
                ranges: [
                    {
                        startValue: 2,
                        endValue: 3,
                        style: {fill: '#4cb848', stroke: '#4cb848'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 1.5,
                        endValue: 2,
                        style: {fill: '#ff8000', stroke: '#ff8000'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 0,
                        endValue: 1.5,
                        style: {fill: '#e53d37', stroke: '#e53d37'},
                        startDistance: 0,
                        endDistance: 0
                    }],
                cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
                caption: {offset: [0, 10], value: 'Acid-Test Ratio', position: 'bottom'},
                border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
                ticksMinor: {interval: 0.05, size: '5%'},
                ticksMajor: {interval: 0.25, size: '10%'},
                labels: {position: 'outside', interval: 0.5},
                pointer: {style: {fill: '#2e79bb'}, width: 5},
                animationDuration: 1500,
                width: '100%',
                height: '100%',
                max: 3,
            });

            $('#debtor').jqxGauge({
                ranges: [
                    {
                        startValue: 0,
                        endValue: 45,
                        style: {fill: '#4cb848', stroke: '#4cb848'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 45,
                        endValue: 90,
                        style: {fill: '#ff8000', stroke: '#ff8000'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 90,
                        endValue: 140,
                        style: {fill: '#e53d37', stroke: '#e53d37'},
                        startDistance: 0,
                        endDistance: 0
                    }],
                cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
                caption: {offset: [0, 10], value: 'Debtor Collection Period <br> (in days)', position: 'bottom'},
                border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
                ticksMinor: {interval: 5, size: '5%'},
                ticksMajor: {interval: 20, size: '10%'},
                labels: {position: 'outside', interval: 20},
                pointer: {style: {fill: '#2e79bb'}, width: 5},
                animationDuration: 1500,
                width: '100%',
                height: '100%',
                max: 140,

            });

            $('#creditor').jqxGauge({
                ranges: [{
                    startValue: 0,
                    endValue: 90,
                    style: {fill: '#4cb848', stroke: '#4cb848'},
                    startDistance: 0,
                    endDistance: 0
                },
                    {
                        startValue: 90,
                        endValue: 120,
                        style: {fill: '#ff8000', stroke: '#ff8000'},
                        startDistance: 0,
                        endDistance: 0
                    },
                    {
                        startValue: 120,
                        endValue: 160,
                        style: {fill: '#e53d37', stroke: '#e53d37'},
                        startDistance: 0,
                        endDistance: 0
                    }],
                cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
                caption: {offset: [0, 10], value: 'Creditor Payment Period <br> (in days)', position: 'bottom'},
                border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
                ticksMinor: {interval: 5, size: '5%'},
                ticksMajor: {interval: 20, size: '10%'},
                labels: {position: 'outside', interval: 20},
                pointer: {style: {fill: '#2e79bb'}, width: 5},
                animationDuration: 1500,
                width: '100%',
                height: '100%',
                max: 160,

            });

            // prepare jqxChart shared settings
            var sharedSettings = {
                title: "",
                description: "",
                enableAnimations: true,
                showLegend: false,
                padding: {left: 5, top: 5, right: 5, bottom: 5},
                titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
                xAxis: {
                    dataField: 'Quarter',
                    gridLines: {visible: true}
                },
                colorScheme: 'scheme01',
                seriesGroups: [
                    {
                        type: 'column',
                        columnsGapPercent: 50,
                        seriesGapPercent: 0,
                        valueAxis: {
                            visible: true,
                            unitInterval: 10,
                            title: {text: 'Amount'}
                        },
                        series: []
                    }
                ]
            };

            // setup the charts
            $.ajax({
                type: 'GET',
                url: baseurl + '/ratios.json',
                dataType: 'json',
                success: function (res) {
                    var stabilitySettings = JSON.parse(JSON.stringify(sharedSettings));
                    var profitabilitySettings = JSON.parse(JSON.stringify(sharedSettings));

                    stabilitySettings.title = 'Stability Ratio';
                    stabilitySettings.source = res['stabilityRatio']['dataSet'];
                    stabilitySettings.seriesGroups[0].series = res['stabilityRatio']['series'];

                    profitabilitySettings.colorScheme = 'scheme04';
                    profitabilitySettings.title = 'Profitability Ratio';
                    profitabilitySettings.source = res['profitabilityRatio']['dataSet'];
                    profitabilitySettings.seriesGroups[0].series = res['profitabilityRatio']['series'];

                    $('#profitability').jqxChart(profitabilitySettings);
                    $('#stability').jqxChart(stabilitySettings);
                }
            });


            $('#efficiency').jqxChart(sharedSettings);
            $('#current_ratio').jqxGauge('value', currentRatio);
            $('#acid_test').jqxGauge('value', acidTestRatio);
            $('#debtor').jqxGauge('value', currentDebtorPeriod);
            $('#creditor').jqxGauge('value', currentCreditorPeriod);
        });
    </script>
@stop