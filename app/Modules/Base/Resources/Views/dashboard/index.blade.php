@extends('layouts.main')

@section('content')
    <link href="{{ asset('js/jqwidgets/styles/jqx.base.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" type="text/css"/>

    <section class="vbox">
        <section class="scrollable">
            <div class="wrapper-lg">
                <div class="row" style="margin-right: 100px; padding: 0px;">

                    <?php foreach ($widgets as $eachWidgets): ?>
                    <div class="col-md-5 col-md-offset-1 box">
                        <div class="inner">
                            <?php $url = "dashboard.partials." . $eachWidgets->partials ?>
                            @include($url,['data'=>$eachWidgets->name])
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </section>
    </section>
@endsection
