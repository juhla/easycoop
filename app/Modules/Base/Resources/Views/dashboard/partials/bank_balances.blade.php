<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <span class="text-muted">Total:</span>
        <span class="h3 block">₦{{ formatNumber($total_account_balance) }}</span>
        <table class="table table-striped m-b-none">
            <thead>
            <tr>
                <th>Account</th>
                <th>Previous Balance</th>
                <th>Current Balance</th>
            </tr>
            </thead>
            <tbody>
            @foreach($bank_accounts as $key => $bank_account)
                <tr>
                    <td class="text-muted block">{{ $bank_account['name'] }} </td>
                    <td>₦{{ formatNumber($bank_accounts1[$key]['account_balance']) }}</td>
                    <td>₦{{ formatNumber($bank_account['account_balance']) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>

