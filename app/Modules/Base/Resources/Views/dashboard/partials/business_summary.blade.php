<div class="list-item">
    <div>
        <section class="panel panel-default">
            <header class="panel-heading font-bold">{{$data}}</header>
            <div class="panel-body">
                <table class="table table-striped m-b-none"> <thead>
                    <tr>
                        <th></th>
                        <th>Prev Month</th>
                        <th>Current Month</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Assets</td>
                        <td>₦{{ formatNumber($prevTotalAssets) }}</td>
                        <td>₦{{ formatNumber($currTotalAssets) }}</td>
                        {!! calculatePercentage($prevTotalAssets, $currTotalAssets) !!}
                    </tr>
                    <tr>
                        <td>Liabilities</td>
                        <td>₦{{ formatNumber($prevTotalLiabilities) }}</td>
                        <td>₦{{ formatNumber($totalLiabilities) }}</td>
                        {!! calculatePercentage($prevTotalLiabilities, $totalLiabilities) !!}
                    </tr>
                    <tr>
                        <td>Equity</td>
                        <td>₦{{ formatNumber($prevTotalEquity) }}</td>
                        <td>₦{{ formatNumber($totalEquity) }}</td>
                        {!! calculatePercentage($prevTotalEquity, $totalEquity) !!}
                    </tr>
                    <tr>
                        <td>Income</td>
                        <td>₦{{ formatNumber($prevTotalIncome) }}</td>
                        <td>₦{{ formatNumber($totalIncome) }}</td>
                        {!! calculatePercentage($prevTotalIncome, $totalIncome) !!}
                    </tr>
                    <tr>
                        <td>Expense</td>
                        <td>₦{{ formatNumber($prevTotalExpenses) }}</td>
                        <td>₦{{ formatNumber($totalExpenses) }}</td>
                        {!! calculatePercentage($prevTotalExpenses, $totalExpenses) !!}
                    </tr>
                    </tbody>
                </table>
            </div>

        </section>
    </div>
</div>