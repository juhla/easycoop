<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th align="center">Previous balance</th>
                <th align="center">Current balance</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><i><strong>₦{{ number_format($thisMonth,2) }}</strong></i></td>
                <td><i><strong>₦{{ number_format($lastMonth,2) }}</strong></i></td>
            </tr>
            </tbody>
        </table>
    </div>
</section>

