<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='_creditorPaymentPeriod' style="width:400px; height:302px; padding: 2px 75px;"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();

        $('#_creditorPaymentPeriod').jqxGauge({
            ranges: [
                {
                    startValue: 0,
                    endValue: 45,
                    style: {fill: '#4cb848', stroke: '#4cb848'},
                    startDistance: 0,
                    endDistance: 0
                },
                {
                    startValue: 45,
                    endValue: 90,
                    style: {fill: '#ff8000', stroke: '#ff8000'},
                    startDistance: 0,
                    endDistance: 0
                },
                {
                    startValue: 90,
                    endValue: 140,
                    style: {fill: '#e53d37', stroke: '#e53d37'},
                    startDistance: 0,
                    endDistance: 0
                }],
            cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
            caption: {offset: [0, 10], value: 'Creditor Payment Period <br> (in days)', position: 'bottom'},
            border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
            ticksMinor: {interval: 5, size: '5%'},
            ticksMajor: {interval: 20, size: '10%'},
            labels: {position: 'outside', interval: 20},
            pointer: {style: {fill: '#2e79bb'}, width: 5},
            animationDuration: 1500,
            width: '65%',
            height: '100%',
            max: 140,

        });

        // setup the charts
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/creditor_payment_period',
            dataType: 'json',
            success: function (res) {
                $('#_creditorPaymentPeriod').jqxGauge('value', res['currentCreditorPeriod']);
            }
        });

    });

</script>
