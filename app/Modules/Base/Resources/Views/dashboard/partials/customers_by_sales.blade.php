<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='customerBySales' style="width:400px; height:280px"></div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {

        // prepare chart data
        var baseurl = $('#baseurl').val();
        // prepare jqxChart settings
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: true,
            showBorderLine: false,
            legendPosition: {left: 520, top: 0, width: 100, height: 100},
            padding: {left: 0, top: 0, right: 0, bottom: 0},
            titlePadding: {left: 0, top: 0, right: 0, bottom: 0},
            // source: dataAdapter,
            colorScheme: 'scheme02',

            seriesGroups: [
                {
                    type: 'pie',
                    valueAxis: {
                        displayValueAxis: true,
                        formatSettings: {sufix: '%'}
                    },
                    showLabels: true,
                    width: '20',
                    height: '20',
                    series: [
                        {
                            dataField: 'value',
                            displayText: 'name',
                            labelRadius: 60,
                            initialAngle: 15,
                            radius: 120,
                            centerOffset: 0,
                            labels: {
                                offset: {x: 10, y: -5},
                                padding: { bottom: -2 }
                            },
                            formatSettings: {
                                sufix: '', decimalPlaces: '2', decimalSeparator: '.',
                                thousandsSeparator: ','
                            }
                        }
                    ]
                }
            ]
        };


        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/customer_by_sales',
            dataType: 'json',
            // setup the chart
            success: function (res) {
                var settings = JSON.parse(JSON.stringify(sharedSettings));
                settings.colorScheme = 'scheme05';
                settings.title = '';
                settings.description = "";
                settings.source = res['customers'];
                //console.log(res['customers'])
                $('#customerBySales').jqxChart(settings);
                //console.log(settings.seriesGroups[0].series[0].formatSettings)
            }
        });

    });
</script>
