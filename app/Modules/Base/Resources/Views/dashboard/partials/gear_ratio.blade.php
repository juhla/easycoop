<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='gear_ratio' style="width:400px; height:302px; padding: 0px 60px;"></div>
    </div>
</section>
@include('reports.partials._gear_ratio')


<script type="text/javascript">


    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#gear_ratio");

        var acidTestRatio = $('#acidTestRatio').val();
        // prepare chart data as an array

        $('#gear_ratio').jqxGauge({
            ranges: [
                {
                    startValue: 1,
                    endValue: 2,
                    style: {fill: '#4cb848', stroke: '#4cb848'},
                    startDistance: 0,
                    endDistance: 0
                },
                {
                    startValue: 0.5,
                    endValue: 1,
                    style: {fill: '#ff8000', stroke: '#ff8000'},
                    startDistance: 0,
                    endDistance: 0
                },
                {
                    startValue: 0,
                    endValue: 0.5,
                    style: {fill: '#e53d37', stroke: '#e53d37'},
                    startDistance: 0,
                    endDistance: 0
                }],
            cap: {size: '5%', style: {fill: '#2e79bb', stroke: '#2e79bb'}},
            caption: {offset: [0, 10], value: 'Gearing Ratio', position: 'bottom'},
            border: {style: {fill: '#8e9495', stroke: '#7b8384', 'stroke-width': 1}},
            ticksMinor: {interval: 0.05, size: '5%'},
            ticksMajor: {interval: 0.25, size: '10%'},
            labels: {position: 'outside', interval: 0.5},
            pointer: {style: {fill: '#2e79bb'}, width: 5},
            animationDuration: 1500,
            width: '70%',
            height: '100%',
            max: 2,
        });

        // prepare jqxChart shared settings
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: false,
            padding: {left: 5, top: 5, right: 5, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                dataField: 'gearRatio',
                gridLines: {visible: true}
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    columnsGapPercent: 50,
                    seriesGapPercent: 0,
                    valueAxis: {
                        visible: true,
                        unitInterval: 10,
                        title: {text: 'Amount'}
                    },
                    series: []
                }
            ]
        };

        // setup the charts
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/gear_ratio',
            dataType: 'json',
            success: function (res) {
                selector.jqxGauge('value', res['gearRatio']);
                selector.on('click', function (event) {
                    $('#gearRatio').append(res['gearRatio']);
                    $('#currYrEquityTotal').append(res['currYrEquityTotal']);
                    $('#longTermBorrowing').append(res['longTermBorrowing']);
                    $('#bsItemsModal').modal('show');
                });
            }
        });


    });
</script>
