<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='grossProfitMargin' style="width:385px; height:270px;"></div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: false,
            padding: {left: 5, top: 5, right: 5, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                dataField: 'Quarter',
                gridLines: {visible: false}
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    columnsGapPercent: 50,
                    seriesGapPercent: 0,
                    valueAxis: {
                        visible: true,
                       // unitInterval: 10,
                        title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    },

                    series: []
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/gross_profit_margin',
            dataType: 'json',
            success: function (res) {

                var grossProfitMarginSettings = JSON.parse(JSON.stringify(sharedSettings));
                grossProfitMarginSettings.colorScheme = 'scheme07';
                grossProfitMarginSettings.title = ' ';
                grossProfitMarginSettings.source = res['dataSet'];
                grossProfitMarginSettings.seriesGroups[0].series = res['series'];
                $('#grossProfitMargin').jqxChart(grossProfitMarginSettings);
            }
        });


    });
</script>