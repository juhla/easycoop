<section class="panel panel-default">
 <header class="panel-heading font-bold">{{$data}}</header>
 <div class="panel-body">
  <div id='monthlyRevenue' style="width:387px; height:270px;"></div>
 </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#monthlyRevenue");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true,
                    padding: {top: 100}
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/revenues',
            dataType: 'json',
            success: function (res) {
                var revenueSettings = JSON.parse(JSON.stringify(settings));
                revenueSettings.colorScheme = 'scheme04';
//                revenueSettings.title = 'Monthly revenues history';
                revenueSettings.title = '';
//                revenueSettings.description = 'Statistics for ' + res['year'];
                revenueSettings.description = '';
                revenueSettings.source = res['dataSet'];
                revenueSettings.seriesGroups[0].series = res['series'];
                revenueSettings.xAxis.dataField = "Month_short";
                selector.jqxChart(revenueSettings);
            }
        });

        selector.on('click', function (event) {
            //addContainer();
            var result, month;
            $.ajax({
                url: baseurl + '/dashboard/report/revenues_month',
                data: {index: event.args.elementIndex},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    if (e.type === 'success') {
                        result = e.data;
                        month = e.month;

                        var series = [
                            {
                                dataField: 'Value',
                                displayText: 'Revenues History'
                            }
                        ];
                        var revenueMonthSettings = JSON.parse(JSON.stringify(settings));
                        revenueMonthSettings.colorScheme = 'scheme04';
                        revenueMonthSettings.title = 'Weekly Revenues History';
                        revenueMonthSettings.description = 'Statistics for ' + month;
                        revenueMonthSettings.source = result;
                        revenueMonthSettings.seriesGroups[0].series = series;
                        revenueMonthSettings.xAxis.dataField = "weeks";
                        selector.jqxChart(revenueMonthSettings);
                        selector.off("click");
                    }
                },
                error: function (e) {

                }
            });


        });


    });
</script>
