<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='profitability' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: false,
            padding: {left: 5, top: 5, right: 5, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                dataField: 'Quarter',
                gridLines: {visible: false}
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    columnsGapPercent: 50,
                    seriesGapPercent: 0,
                    valueAxis: {
                        visible: true,
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        },
                        title: {text: 'Margin'}
                    },
                    series: []
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/net_profit_margin',
            dataType: 'json',
            success: function (res) {

                var netProfitMargin = JSON.parse(JSON.stringify(sharedSettings));
                netProfitMargin.colorScheme = 'scheme06';
                netProfitMargin.title = ' ';
                netProfitMargin.source = res['dataSet'];
                netProfitMargin.seriesGroups[0].series = res['series'];
                $('#profitability').jqxChart(netProfitMargin);
            }
        });


    });
</script>
