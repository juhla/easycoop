<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='payablesAccount' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#payablesAccount");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };

        var payablesAccount = function () {
            $.ajax({
                type: 'GET',
                url: baseurl + '/dashboard/report/payables_accounts',
                dataType: 'json',
                success: function (res) {
                    var purchaseHistorySettings = JSON.parse(JSON.stringify(settings));
                    purchaseHistorySettings.colorScheme = 'scheme04';
                    purchaseHistorySettings.title = ' ';
                    purchaseHistorySettings.description = 'Statistics for ' + res['year'];
                    purchaseHistorySettings.source = res['dataSet'];
                    purchaseHistorySettings.seriesGroups[0].series = res['series'];
                    purchaseHistorySettings.xAxis.dataField = "Month_short";
                    selector.jqxChart(purchaseHistorySettings);
                }
            });
        };

        payablesAccount();
        selector.on('click', function (event) {
            //addContainer();
            var result, month;
            $.ajax({
                url: baseurl + '/dashboard/report/payables_accounts_month',
                data: {index: event.args.elementIndex},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    if (e.type === 'success') {
                        result = e.data;
                        month = e.month;

                        var series = [
                            {
                                dataField: 'Value',
                                displayText: 'Payables History'
                            }
                        ];
                        var purchaseHistoryMonthSettings = JSON.parse(JSON.stringify(settings));
                        purchaseHistoryMonthSettings.colorScheme = 'scheme04';
                        purchaseHistoryMonthSettings.title = ' ';
                        purchaseHistoryMonthSettings.description = 'Statistics for ' + month;
                        purchaseHistoryMonthSettings.source = result;
                        purchaseHistoryMonthSettings.seriesGroups[0].series = series;
                        purchaseHistoryMonthSettings.xAxis.dataField = "weeks";
                        selector.jqxChart(purchaseHistoryMonthSettings);
                        selector.off();
                    }
                },
                error: function (e) {

                }
            });


        });


    });
</script>
