<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='payablesAge' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#payablesAge");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/payables_age_analysis',
            dataType: 'json',
            success: function (res) {
                var payableAge = JSON.parse(JSON.stringify(settings));
                payableAge.colorScheme = 'scheme05';
                payableAge.title = ' ';
                payableAge.description = 'Statistics for ' + res['year'];
                payableAge.source = res['analysis'];
                payableAge.seriesGroups[0].series = res['series'];
                payableAge.xAxis.dataField = "type";
                selector.jqxChart(payableAge);

            }
        });


    });
</script>
