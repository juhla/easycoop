<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='receiveAgeAnalysisHistory' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();
        var selector = $("#receiveAgeAnalysisHistory");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                formatSettings: {
                    prefix: '₦',
                    decimalPlaces: 2,
                    thousandsSeparator: ','
                },
                formatFunction: function (value, itemIndex, serie, group) {
                    //console.log('hh');
                    return value.toFixed(2).replace(/./g, function (c, i, a) {
                        return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
                    });
                },
                labels: {
                    visible: true,
                    formatSettings: {
                        decimalPlaces: 2
                    }
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                    prefix: '₦',
                    decimalPlaces: 2,
                    decimalSeparator: '.',
                    negativeWithBrackets: true,
                    thousandsSeparator: ','
                },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/receivable_age_analysis',
            dataType: 'json',
            success: function (res) {
                var receivableAgeAnalysisSettings = JSON.parse(JSON.stringify(settings));
                receivableAgeAnalysisSettings.colorScheme = 'scheme02';
                receivableAgeAnalysisSettings.title = '';
                receivableAgeAnalysisSettings.description = 'Statistics for ' + res['year'];
                receivableAgeAnalysisSettings.source = res['analysis'];
                receivableAgeAnalysisSettings.seriesGroups[0].series = res['series'];
                receivableAgeAnalysisSettings.xAxis.dataField = "type";
                console.log(receivableAgeAnalysisSettings);
                selector.jqxChart(receivableAgeAnalysisSettings);
            }
        });


    });
</script>
