<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='recievablesAccount' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#recievablesAccount");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/receivables_accounts',
            dataType: 'json',
            success: function (res) {
                var purchaseHistorySettings = JSON.parse(JSON.stringify(settings));
                purchaseHistorySettings.colorScheme = 'scheme04';
//                purchaseHistorySettings.title = 'Monthly receivables history';
                purchaseHistorySettings.title = '';
//                purchaseHistorySettings.description = 'Statistics for ' + res['year'];
                purchaseHistorySettings.description = '';
                purchaseHistorySettings.source = res['dataSet'];
                purchaseHistorySettings.seriesGroups[0].series = res['series'];
                purchaseHistorySettings.xAxis.dataField = "Month_short";
                selector.jqxChart(purchaseHistorySettings);
            }
        });

        selector.on('click', function (event) {
            //addContainer();
            var result, month;
            $.ajax({
                url: baseurl + '/dashboard/report/receivables_accounts_month',
                data: {index: event.args.elementIndex},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    if (e.type === 'success') {
                        result = e.data;
                        month = e.month;

                        var series = [
                            {
                                dataField: 'Value',
                                displayText: 'Receivables History'
                            }
                        ];
                        var purchaseHistoryMonthSettings = JSON.parse(JSON.stringify(settings));
                        purchaseHistoryMonthSettings.colorScheme = 'scheme04';
                        purchaseHistoryMonthSettings.title = 'Weekly Receivables History';
                        purchaseHistoryMonthSettings.description = 'Statistics for ' + month;
                        purchaseHistoryMonthSettings.source = result;
                        purchaseHistoryMonthSettings.seriesGroups[0].series = series;
                        purchaseHistoryMonthSettings.xAxis.dataField = "weeks";
                        selector.jqxChart(purchaseHistoryMonthSettings);
                        selector.off("click");
                    }
                },
                error: function (e) {

                }
            });


        });


    });
</script>
