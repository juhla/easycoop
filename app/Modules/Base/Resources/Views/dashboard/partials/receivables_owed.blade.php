<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='topReceivables' style="width:387px; height:250px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {


        var baseurl = $('#baseurl').val();
        var selector = $("#topReceivables");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };

        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/receivables_owed',
            dataType: 'json',
            success: function (res) {
                var receivablesAccount = JSON.parse(JSON.stringify(settings));
                receivablesAccount.colorScheme = 'scheme04';
                receivablesAccount.title = 'Top 5 Receivables';
                receivablesAccount.description = 'Statistics';
                receivablesAccount.source = res['dataSet'];
                receivablesAccount.seriesGroups[0].series = res['series'];
                receivablesAccount.xAxis.dataField = "Name";
                selector.jqxChart(receivablesAccount);
            }
        });


    });
</script>
