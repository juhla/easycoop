<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='ROCE' style="width:400px; height:200px"></div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: false,
            padding: {left: 5, top: 5, right: 5, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                dataField: 'Quarter',
                gridLines: {visible: false}
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    columnsGapPercent: 50,
                    seriesGapPercent: 0,
                    valueAxis: {
                        visible: true,
                       // unitInterval: 10,
                        title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }

                    },
                    series: []
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/return_on_capital',
            dataType: 'json',
            success: function (res) {

                var netProfitMargin = JSON.parse(JSON.stringify(sharedSettings));
                netProfitMargin.colorScheme = 'scheme04';
//                netProfitMargin.title = 'Return on capital employed';
                netProfitMargin.title = '';
                netProfitMargin.source = res['dataSet'];
                netProfitMargin.seriesGroups[0].series = res['series'];
                $('#ROCE').jqxChart(netProfitMargin);
            }
        });


    });
</script>