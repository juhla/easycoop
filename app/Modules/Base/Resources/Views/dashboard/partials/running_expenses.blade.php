
    <section class="panel panel-default panelf">
        <header class="panel-heading font-bold">{{$data}}</header>
        <div class="panel-body pbody">
        <p>Current Month</p>
            <div class="row m-t-sm">
                <table class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th>Expenses</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody id="currentMonth">
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel-body pbody">
        <p>Previous Month</p>
            <div class="row m-t-sm">
                <table class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th>Expenses</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody id="previousMonth">
                    </tbody>
                </table>
            </div>
        </div>
    </section>


<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/running_expenses',
            dataType: 'json',
            success: function (res) {
                createForm(res)
            }
        });

        function generateParameterForm(element) {

            var form = $("<tr>");
            var td1 = $('<td class="text-muted block">' + element['name'] + ' </td>');
            var td2 = $('<td>' + element['total_sales'] + ' </td>');
            $(form).append(td1).append(td2).append('</tr>');
            return $(form);
        }

        function createForm(e) {
            $.each(e['previousExpenses'], function (index, element) {
                $("#previousMonth").append(generateParameterForm(element));
            });
            $.each(e['currentExpenses'], function (index, element) {
                $("#currentMonth").append(generateParameterForm(element));
            });
        }


    });
</script>
