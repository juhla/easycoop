<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='salesHistory' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        var baseurl = $('#baseurl').val();
        var selector = $("#salesHistory");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                       // title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/sales_history',
            dataType: 'json',
            success: function (res) {
                var salesSettings = JSON.parse(JSON.stringify(settings));
                salesSettings.colorScheme = 'scheme04';
//                salesSettings.title = 'Monthly Sales history';
                salesSettings.title = '';
//                salesSettings.description = 'Statistics for ' + res['year'];
                salesSettings.description = '';
                salesSettings.source = res['dataSet'];
                salesSettings.seriesGroups[0].series = res['series'];
                salesSettings.xAxis.dataField = "Month_short";
                salesSettings.xAxis.displayText = "Month";
                selector.jqxChart(salesSettings);
            }
        });

        selector.on('click', function (event) {
            //addContainer();
            var result, month;
            $.ajax({
                url: baseurl + '/dashboard/report/sales_history_month',
                data: {index: event.args.elementIndex},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    if (e.type === 'success') {
                        result = e.data;
                        month = e.month;

                        var series = [
                            {
                                dataField: 'Value',
                                displayText: 'Sales History'
                            }
                        ];
                        var salesSettings = JSON.parse(JSON.stringify(settings));
                        salesSettings.colorScheme = 'scheme04';
                        salesSettings.title = 'Weekly Sales History';
                        salesSettings.description = 'Statistics for ' + month;
                        salesSettings.source = result;
                        salesSettings.seriesGroups[0].series = series;
                        salesSettings.xAxis.dataField = "weeks";
                        selector.jqxChart(salesSettings);
                        selector.off("click");
                    }
                },
                error: function (e) {

                }
            });


        });


    });
</script>
