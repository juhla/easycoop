<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='stockSummary' style="width:387px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var baseurl = $('#baseurl').val();
        var selector = $("#stockSummary");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };

        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/stock_summary',
            dataType: 'json',
            success: function (res) {
                var stockSettings = JSON.parse(JSON.stringify(settings));
                stockSettings.colorScheme = 'scheme04';
                stockSettings.title = '';
                stockSettings.description = 'Statistics for ' + res['year'];
                stockSettings.source = res['dataSet'];
                stockSettings.seriesGroups[0].series = res['series'];
                stockSettings.xAxis.dataField = "Month_short";
                selector.jqxChart(stockSettings);
            }
        });


        selector.on('click', function (event) {
            //addContainer();
            var result, month;
            $.ajax({
                url: baseurl + '/dashboard/report/purchase_history_month',
                data: {index: event.args.elementIndex},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    if (e.type === 'success') {
                        result = e.data;
                        month = e.month;


                        var series = [
                            {
                                dataField: 'Value',
                                displayText: 'Stock Summary'
                            }
                        ];


                        var stockMonthSettings = JSON.parse(JSON.stringify(settings));
                        stockMonthSettings.colorScheme = 'scheme04';
//                        stockMonthSettings.title = 'Weekly Stock Summary';
                        stockMonthSettings.title = '';
//                        stockMonthSettings.description = 'Statistics for ' + month;
                        stockMonthSettings.description = '';
                        stockMonthSettings.source = result;
                        stockMonthSettings.seriesGroups[0].series = series;
                        stockMonthSettings.xAxis.dataField = "weeks";
                        selector.jqxChart(stockMonthSettings);
                        selector.off("click");

                    }
                },
                error: function (e) {

                }
            });


        });


    });
</script>
