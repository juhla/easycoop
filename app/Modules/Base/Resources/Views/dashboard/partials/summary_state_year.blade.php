<div class="list-item">
    <div>
        <section class="panel panel-default">
            <header class="panel-heading font-bold">{{$data}}</header>
            <div class="panel-body">
                <table class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Prev Year</th>
                        <th>Current Year</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Assets</td>
                        <td>₦{{ formatNumber($iprevTotalAssets) }}</td>
                        <td>₦{{ formatNumber($icurrTotalAssets) }}</td>
                        {!! calculatePercentage($iprevTotalAssets, $icurrTotalAssets) !!}
                    </tr>
                    <tr>
                        <td>Liabilities</td>
                        <td>₦{{ formatNumber($iprevTotalLiabilities) }}</td>
                        <td>₦{{ formatNumber($itotalLiabilities) }}</td>
                        {!! calculatePercentage($iprevTotalLiabilities, $itotalLiabilities) !!}
                    </tr>
                    <tr>
                        <td>Equity</td>
                        <td>₦{{ formatNumber($iprevTotalEquity) }}</td>
                        <td>₦{{ formatNumber($itotalEquity) }}</td>
                        {!! calculatePercentage($iprevTotalEquity, $itotalEquity) !!}
                    </tr>
                    <tr>
                        <td>Income</td>
                        <td>₦{{ formatNumber($iprevTotalIncome) }}</td>
                        <td>₦{{ formatNumber($itotalIncome) }}</td>
                        {!! calculatePercentage($iprevTotalIncome, $itotalIncome) !!}
                    </tr>
                    <tr>
                        <td>Expense</td>
                        <td>₦{{ formatNumber($iprevTotalExpenses) }}</td>
                        <td>₦{{ formatNumber($itotalExpenses) }}</td>
                        {!! calculatePercentage($iprevTotalExpenses, $itotalExpenses) !!}
                    </tr>
                    </tbody>
                </table>
            </div>

        </section>
    </div>
</div>