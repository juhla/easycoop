
<section class="panel panel-default panelf">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover table-condensed tablef" id="inflowTable">
                <thead>
                <tr>
                    <th width="40%"></th>
                    <th></th>
                    <th>Current Month <br>₦</th>
                    <th>Year to Date <br>₦</th>
                </tr>
                </thead>

                <?php  $month = new Carbon\Carbon('first day of this month') ?>
                <?php  $year = new Carbon\Carbon('first day of January '. date('Y')) ?>
                <?php  $current_month = $month->format('Y-m-d') ?>
                <?php  $start_date = $year->format('Y-m-d') ?>
                <?php  $end_date = date('Y-m-d') ?>


                <?php  $currMonthIncome = $income->getBalance($current_month, $end_date) ?>
                <?php  $incomeYearToDate = $income->getBalance($start_date, $end_date) ?>

                <?php  $currMonthDirectCost = $direct_cost->getBalance($current_month, $end_date) ?>
                <?php  $yearToDateDirectCost = $direct_cost->getBalance($start_date, $end_date) ?>

                <?php  $currMonthOIncome = $other_income->getBalance($current_month, $end_date) ?>
                <?php  $oIncomeYearToDate = $other_income->getBalance($start_date, $end_date) ?>

                <?php  $currMonthExp = 0 ?>
                <?php  $yearToDateExp = 0 ?>


                <tbody>
                <tr class="clickable-row" data-value="{{ $income->id }}">
                    <td style="font-weight: 800; font-size: 16px"> {{ $income->name }}</td>

                    <td></td>
                    <td>{{ formatNumber($currMonthIncome['balance']) }}</td>
                    <td>{{ formatNumber($incomeYearToDate['balance']) }}</td>
                </tr>
                <tr class="clickable-row" data-value="{{ $direct_cost->id }}">
                    <td style="font-weight: 800; font-size: 15px"> {{ $direct_cost->name }}</td>

                    <td></td>
                    <td>{{ formatNumber($currMonthDirectCost['balance']) }}</td>
                    <td>{{ formatNumber($yearToDateDirectCost['balance']) }}</td>
                </tr>
                </tbody>
                <thead>
                <tr style="background-color: #cce9ff">
                    <th>Gross Profit</th>
                    <th></th>
                    <?php  $grossProfitMonth = bcsub($currMonthIncome['balance'], $currMonthDirectCost['balance'], 2 )?>
                    <th>{{ formatNumber($grossProfitMonth) }}</th>
                    <?php  $grossProfitYTD = bcsub($incomeYearToDate['balance'], $yearToDateDirectCost['balance'], 2)?>
                    <th>{{ formatNumber($grossProfitYTD) }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="font-weight: 800; font-size: 15px"> {{ $other_income->name }}</td>
                    <td></td>
                    <td>{{ formatNumber($currMonthOIncome['balance']) }}</td>
                    <td>{{ formatNumber($oIncomeYearToDate['balance']) }}</td>
                </tr>
                <tr>
                    <td colspan="4" style="font-weight: 600; font-size: 15px"> {{ $expenses->name }}</td>
                </tr>
                @if($groups = $expenses->groups)
                    @foreach($groups as $group)
                        <tr class="clickable-row" data-value="{{ $group->id }}">
                            <td style="font-weight: 600">{{ $group->name }} </td>
                            <td></td>
                            <td>
                                <?php  $currMonthResult = $group->getBalance($current_month, $end_date) ?>
                                {{ formatNumber($currMonthResult['balance']) }}
                                <?php  $currMonthExp = bcadd($currMonthResult['balance'], $currMonthExp, 2) ?>
                            </td>
                            <td>
                                <?php  $yearToDateResult = $group->getBalance($start_date, $end_date) ?>
                                {{ formatNumber($yearToDateResult['balance']) }}
                                <?php  $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2) ?>
                            </td>
                        </tr>
                        @if($nestedGroup = $group->nestedGroups($group->id))
                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)

                                <?php  $currMonthResult = $child->getBalance($current_month, $end_date) ?>
                                <?php  $currMonthExp = bcadd($currMonthResult['balance'], $currMonthExp, 2) ?>

                                <?php  $yearToDateResult = $child->getBalance($start_date, $end_date) ?>
                                <?php  $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2) ?>

                                @if($subChild = $child->nestedGroups($child->id))
                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)

                                        <?php  $currMonthResult = $subs->getBalance($current_month, $end_date) ?>
                                        <?php  $currMonthExp = bcadd($currMonthResult['balance'], $currMonthExp, 2) ?>
                                        <?php  $yearToDateResult = $subs->getBalance($start_date, $end_date) ?>
                                        <?php  $yearToDateExp = bcadd($yearToDateResult['balance'], $yearToDateExp, 2) ?>

                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($expenses->ledgers->count())
                    @foreach($expenses->ledgers()->where('id', '!=', 44)->orderBy('code', 'asc')->get() as $ledger)

                        <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                        @if($result['dc'] === 'D')
                            <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                        @elseif($result['dc'] === 'C')
                            <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                        @else
                            -
                        @endif

                        <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                        @if($yeartodate['dc'] === 'D')
                            <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                        @elseif($yeartodate['dc'] === 'C')
                            <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                        @else
                            -
                        @endif

                    @endforeach
                @endif
                </tbody>
                <thead>
                <tr style="background-color: #cce9ff">
                    <th>Total Expenses</th>
                    <th></th>
                    <th>{{ formatNumber($currMonthExp) }}</th>
                    <th>{{ formatNumber($yearToDateExp) }}</th>
                </tr>
                </thead>
                <thead>
                <tr style="background-color: #cce9ff">
                    <th>Net Profit</th>
                    <th></th>
                    <th>
                        <?php  $currMonthPBT = ($grossProfitMonth + $currMonthOIncome['balance']) - $currMonthExp ?>
                        {{ formatNumber($currMonthPBT) }}
                    </th>
                    <th>
                        <?php  $yearToDatePBT = ($grossProfitYTD + $oIncomeYearToDate['balance']) - $yearToDateExp ?>
                        {{ formatNumber($yearToDatePBT) }}
                    </th>
                </tr>
                </thead>
                {{-- <tbody>
                 <tr>
                     <td>Company/Income Tax</td>
                     <td></td>
                     <td>
                         @if($currMonthPBT < 0)
                             -
                             --}}<?php  $currMonthIncomeTax = 0 ?> {{--
                         @else
                             --}}<?php  $currMonthIncomeTax = (30 / 100) * $currMonthPBT ?> {{--
                             {{ formatNumber($currMonthIncomeTax) }}
                         @endif
                     </td>
                     <td>
                         @if($yearToDatePBT < 0)
                             -
                             --}}<?php  $yearToDateIncomeTax = 0 ?> {{--
                         @else
                             --}}<?php  $yearToDateIncomeTax = (30 / 100) * $yearToDatePBT ?> {{--
                             {{ formatNumber($yearToDateIncomeTax) }}
                         @endif
                     </td>
                 </tr>
                 <tr>
                     <td>Education Tax</td>
                     <td></td>
                     <td>
                         @if($currMonthPBT < 0)
                             -
                             --}}<?php  $currMonthEduTax = 0 ?> {{--
                         @else
                             --}}<?php  $currMonthEduTax = (2 / 100) * $currMonthPBT ?> {{--
                             {{ formatNumber($currMonthEduTax) }}
                         @endif
                     </td>
                     <td>
                         @if($yearToDatePBT < 0)
                             -
                             --}}<?php  $yearToDateEduTax = 0 ?> {{--

                         @else
                             --}}<?php  $yearToDateEduTax = (2 / 100) * $yearToDatePBT ?> {{--
                             {{ formatNumber($yearToDateEduTax) }}
                         @endif
                     </td>
                 </tr>
                 </tbody>
                 <thead>
                 <tr style="background-color: #cce9ff">
                     <th>Net Profit</th>
                     <th></th>
                     --}}<?php  $currMonthTax = bcadd($currMonthIncomeTax, $currMonthEduTax, 2) ?> {{--
                     --}}<?php  $yearToDateTax = bcadd($yearToDateIncomeTax, $yearToDateEduTax, 2) ?> {{--
                     <th>{{ formatNumber(bcsub($currMonthPBT, $currMonthTax )) }}</th>
                     <th>{{ formatNumber(bcsub($yearToDatePBT, $yearToDateTax )) }}</th>
                 </tr>
                 </thead>--}}
            </table>
        </div>
    </div>
</section>

