<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='supplierByPurchases' style="width:387px; height:280px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        // prepare chart data
        var baseurl = $('#baseurl').val();
        // prepare jqxChart settings
        var sharedSettings = {
            title: "",
            description: "",
            enableAnimations: true,
            showLegend: true,
            showBorderLine: false,
            legendPosition: {left: 520, top: 140, width: 100, height: 100},
            padding: {left: 5, top: 5, right: 5, bottom: 5},
            titlePadding: {left: 0, top: 0, right: 0, bottom: 10},
            // source: dataAdapter,
            colorScheme: 'scheme02',
            seriesGroups: [
                {
                    type: 'pie',
                    showLabels: true,
                    series: [
                        {
                            dataField: 'value',
                            displayText: 'name',
                            labelRadius: 60,
                            initialAngle: 15,
                            radius: 120,
                            centerOffset: 0,
                            formatSettings: {decimalPlaces: 2},
                            formatSettings: {prefix: '₦', sufix: '', decimalPlaces: '2', decimalSeparator: '.',
                                thousandsSeparator: ','}
                        }
                    ]
                }
            ]
        };

        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/supplier_by_purchases',
            dataType: 'json',
            // setup the chart
            success: function (res) {
                var settings = JSON.parse(JSON.stringify(sharedSettings));
                settings.colorScheme = 'scheme02';
                settings.title = '';
                settings.description = "";
                settings.source = res['suppliers'];
                $('#supplierByPurchases').jqxChart(settings);
            }
        });

    });
</script>


