<section class="panel panel-default">
    <header class="panel-heading font-bold">{{$data}}</header>
    <div class="panel-body">
        <div id='topPayableS' style="width:385px; height:270px"></div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {


        var baseurl = $('#baseurl').val();
        var selector = $("#topPayableS");
        var settings = {
            showLegend: true,
            enableAnimations: true,
            padding: {left: 20, top: 5, right: 20, bottom: 5},
            titlePadding: {left: 90, top: 0, right: 0, bottom: 10},
            xAxis: {
                gridLines: {visible: false}
            },
            valueAxis: {
                flip: false,
                labels: {
                    visible: true
                }
            },
            colorScheme: 'scheme01',
            seriesGroups: [
                {
                    type: 'column',
                    orientation: 'vertical',
                    columnsGapPercent: 50,
                    toolTipFormatSettings: {
                        prefix: '₦',
                        decimalPlaces: 2,
                        decimalSeparator: '.',
                        negativeWithBrackets: true,
                        thousandsSeparator: ','
                    },
                    valueAxis: {
                        visible: true,
                        //  title: {text: 'Margin'},
                        labels: {
                            formatSettings: {
                                prefix: '₦',
                                decimalPlaces: 2,
                                thousandsSeparator: ','
                            }
                        }
                    }
                }
            ]
        };

        $.ajax({
            type: 'GET',
            url: baseurl + '/dashboard/report/top_payables_accounts',
            dataType: 'json',
            success: function (res) {
                var payablesAccount = JSON.parse(JSON.stringify(settings));
                payablesAccount.colorScheme = 'scheme05';
                payablesAccount.title = 'Top 5 Payables';
                payablesAccount.description = '';
                payablesAccount.source = res['dataSet'];
                payablesAccount.seriesGroups[0].series = res['series'];
                payablesAccount.xAxis.dataField = "Name";
                selector.jqxChart(payablesAccount);
            }
        });


    });
</script>
