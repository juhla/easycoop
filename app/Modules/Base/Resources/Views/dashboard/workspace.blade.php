@extends('layouts.main')

@section('content')
    <link href="{{ asset('js/jqwidgets/styles/jqx.base.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Calendar style-->
    <link rel="stylesheet" href="{{ asset('calendar/fullcalendar.css') }}"/>
    <!-- End Calendar style-->
    <!-- Calculator style-->
    <link rel="stylesheet" href="{{ asset('calculator/css/style.css') }}"/>
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" type="text/css"/>
    <!-- End Calculator style-->
    @include('flash::message')
    <section class="vbox dash">
        <section class="scrollable">
            <div class="wrapper-lg">
                <h3 class="m-b-xs font-bold m-t-none">
                    @if(auth()->user()->hasRole(array('social-member', 'employee','business-owner', 'admin')))
                        @if(auth()->user()->last_active_time === 0)
                            Welcome, {{ auth()->user()->displayName() }}
                        @else
                            Welcome back {{ auth()->user()->displayName() }}
                        @endif
                    @else
                        {{ session()->get('company_name') }} Work tools
                    @endif
                </h3>
            </div>

            @if(session('status'))
              <div class="alert alert-success">
                  {{session('status')}}
              </div>
                
            @endif

        <!-- Trigger the modal with a button -->


        @include('dashboard.workspace._modals')
        <!-- Launch Pad -->
            <div class="wrapper-lg-launch">
                <?php foreach ($widgets as $eachWidgets): ?>
                <?php
                $url = $eachWidgets->routes;
                if ($eachWidgets->name == "My Profile") {
                    $url = 'profile/' . strtolower(auth()->user()->displayName());
                }?>
                <div class="item" <?php echo $eachWidgets->div_items ?> >
                    <a href="{{ url($url) }}">
                        <i class="{{ $eachWidgets->class }}" aria-hidden="true"></i>
                        <span class="caption">{{ $eachWidgets->name }}</span>
                    </a>
                </div>

                <?php endforeach; ?>

            </div>
            <!-- End Launch Pad -->

        </section>
    </section>
@endsection
@section('scripts')

    <!-- Start Calender scripts-->
    <script src="{{ asset('calendar/jquery.min.js') }}"></script>
    <script src="{{ asset('calendar/moment.min.js') }}"></script>
    <script src="{{ asset('calendar/fullcalendar.js') }}"></script>
    <!-- initialize the calendar on ready -->
    <script type="application/javascript">
        $(document).ready(function () {
            $('#walkthrough').click(function () {
                var tour = new Tour();
                tour.addSteps(steps);
                tour.init();
                tour.start(true);
            });
            $('#calendar').fullCalendar({
                // put options and callbacks here
            })
        });
    </script>

    <!-- End Calender scripts-->

    <!-- Calculator -->
    <script src="{{ asset('calculator/js/index.js') }}"></script>
    <!-- End Calculator -->
@stop