<!-- Calculator Modal -->
<div class="modal fade" id="myModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calculator </h4>
            </div>

            <div class="modal-body">
                <div class="calculator">

                    <div class="calc-row">
                        <div class="screen">0123456789</div>
                    </div>

                    <div class="calc-row">
                        <div class="button">C</div><div class="button">CE</div><div class="button backspace">back</div><div class="button plus-minus">+/-</div><div class="button">%</div>
                    </div>

                    <div class="calc-row">
                        <div class="button">7</div><div class="button">8</div><div class="button">9</div><div class="button divice">/</div><div class="button root">sqrt</div>
                    </div>

                    <div class="calc-row">
                        <div class="button">4</div><div class="button">5</div><div class="button">6</div><div class="button multiply">*</div><div class="button inverse">1/x</div>
                    </div>

                    <div class="calc-row">
                        <div class="button">1</div><div class="button">2</div><div class="button">3</div><div class="button">-</div><div class="button pi">pi</div>
                    </div>

                    <div class="calc-row">
                        <div class="button zero">0</div><div class="button decimal">.</div><div class="button">+</div><div class="button equal">=</div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Calculator Modal -->

<!-- Calendar modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Calendar</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <div id='calendar' style="z-index:1151 !important;"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Calendar Modal -->

