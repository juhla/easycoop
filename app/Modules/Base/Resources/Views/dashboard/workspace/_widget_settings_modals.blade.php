<div id="widgetSettings" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content modal-content-widget">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Widget Settings</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{ url('workspace-settings/change-widgets') }}" method="post"
                      id="widgetForm" data-validate="parsley">
                    <div class="form-group">
                        <div class="col-md-12 columns">

                            <div class="checkAll">
                                <label class="checkbox-inlinee" for="checkAll">
                                    <div class="touchcheckbox">
                                        <input type="checkbox" id="checkAll" name="check1" checked/>
                                        <label for="touchcheckbox"></label>
                                    </div>
                                    <span class="all-style">Check / Uncheck all </span>
                                </label>
                            </div>

                            <div id="widgetBody">
                            </div>


                        </div>
                    </div>
                    <div class="form-group">
                        {{--<input type="button" class="btn btn-lg btn-block btn-success" name="" value="Save Changes">--}}
                        <button type="submit" id="submitWidget" class="btn btn-lg btn-primary"><i
                                    class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none"></i> Save
                            Changes
                        </button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>
<?php

// action="{{ url('workspace-settings/change-widgets') }}"
?>

<script>

    // Check all
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    // Check all


    var baseurl = $('#baseurl').val();
    $.ajax({
        url: baseurl + '/workspace/widgets',
        type: "GET",
        dataType: 'json',
        success: function (e) {
            createForm(e)
        },
        error: function (e) {

        }
    });
    $(document).on("submit", "#widgetForm", function (event) {
        event.preventDefault();
        $('#submitWidget').attr('disabled', true);
        $('.spinner').show();

        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            type: "GET",
            dataType: 'json',
            success: function (e) {
                if (e.message === 'notvalid') {
                    displayNotification('Error editing widgets', 'Error', 'error');
                    $('#submitWidget').attr('disabled', false);
                    $('.spinner').hide();
                } else if (e.message === 'success') {
                    displayNotification('Your widgets has been updated', 'Success!', 'success');
                    $('#submitWidget').attr('disabled', false);
                    $('.spinner').hide();
                    window.location.href = baseurl + '/workspace';
                } else {
                    displayNotification('Check your fields', 'Error!', 'error');
                    $('#submitWidget').attr('disabled', false);
                    $('.spinner').hide();
                }

            },
            error: function (e) {
                displayNotification('Something went wrong', 'Error!!', 'error');
                $('#submitWidget').attr('disabled', false);
                $('.spinner').hide();
            }
        });
    });


    function generateParameterForm(element) {
        var form = $("<label/>").addClass("checkbox-inline");
        form.html(
                '<div class="touchcheckbox">' +
                '<input ' + element['checked'] + ' value="' + element['id'] + '" type="checkbox" id="' + element['id'] + '" name="widgets[]"/>' +
                '<label for="touchcheckbox"></label>' +
                '</div>' +
                element['name']
        );
        return $(form);
    }
    function createForm(e) {
        var div = document.createElement('div');
        $form = $('<div></div>');
        $.each(e['category_widget'], function (index, element) {
            $form.append(generateParameterForm(element));
        });
        $("#widgetBody").append($form);

    }
</script>
<style>
    .modal-content-widget {
        color: #000000 !important;
    }
</style>
