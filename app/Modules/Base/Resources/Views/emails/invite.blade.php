<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <title>BMAC Collaboration Request</title>
    <style type="text/css">
        .ReadMsgBody {
            width: 100%;
            background-color: #ffffff;
        }

        .ExternalClass {
            width: 100%;
            background-color: #ffffff;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        html {
            width: 100%;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            margin: 0;
            padding: 0;
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
            table-layout: fixed;
            margin: 0 auto;
        }

        table table table {
            table-layout: auto;
        }

        img {
            display: block !important;
            over-flow: hidden !important;
        }

        table td {
            border-collapse: collapse;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        img:hover {
            opacity: 0.9 !important;
        }

        a {
            color: #6ec8c7;
            text-decoration: none;
        }

        .textbutton a {
            font-family: 'open sans', arial, sans-serif !important;
            color: #ffffff !important;
        }

        .preference-link a {
            color: #6ec8c7 !important;
            text-decoration: underline !important;
        }

        .button2 a {
            color: #414a51 !important;
        }

        table[class="buttonbox"] {
            width: 60% !important;
        }

        .y-brand-green {
            color: #0d7c4a;
            font-weight: bolder;
        }

        img.y-rebrand-logo {
            display: inline !important;
            width: 3.5em;
            height: auto;
            margin-bottom: -6px;
        }

        .y-center {
            text-align: center;
        }

        .textbutton {
            background-color: #0d7d40;
        }

        /*Responsive*/
        @media only screen and (max-width: 640px) {
            body {
                width: auto !important;
            }

            table[class="table-inner"] {
                width: 90% !important;
            }

            table[class="table-full"] {
                width: 100% !important;
                text-align: center !important;
                background: none !important;
            }

            table[class="buttonbox"] {
                width: 100% !important;
            }

            /* Image */
            img[class="img1"] {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            body {
                width: auto !important;
            }

            table[class="table-inner"] {
                width: 90% !important;
            }

            table[class="table-full"] {
                width: 100% !important;
                text-align: center !important;
                background: none !important;
            }

            table[class="buttonbox"] {
                width: 100% !important;
            }

            /* image */
            img[class="img1"] {
                width: 100% !important;
                height: auto !important;
            }
        }
    </style>
</head>

<body>

<?php $bmac = config('constants.bmac_url'); ?>
<table bgcolor="#414a51" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="35" align="right" valign="bottom">
                                    <table width="35" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-left-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" background="{{ asset('/images/title-bg.png') }}"
                                    style="background-image: url({{ asset('/images/title-bg.png') }}); background-repeat: repeat-x; background-size: auto; background-position: bottom;">
                                    <table style="border-radius:6px;" width="100%" border="0" align="center"
                                           cellpadding="0" cellspacing="0" bgcolor="#6ec8c7">
                                        <tr>
                                            <td class="textbutton" height="50" align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0"
                                                       class="table-inner">
                                                    <tr>
                                                        <td height="7"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center"
                                                            style="font-family: 'Open sans', Arial, sans-serif; color:#FFFFFF; font-size:16px; font-weight: bold;">
                                                            BMAC Collaboration Request.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="7"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="35" align="left" valign="bottom">
                                    <table width="35" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-right-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="30" bgcolor="#FFFFFF"></td>
                </tr>
                <!--logo-->
                <tr>
                    <td align="center" bgcolor="#FFFFFF">
                        <a href="#">
                            <span class="y-brand-green">BMAC</span>
                            {{-- <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                            src="{{ asset('images/ikooba.png') }}" alt="ikOOba" />
                            --}}
                        </a>
                    </td>
                </tr>
                <!--end logo-->
                <tr>
                    <td height="25" bgcolor="#FFFFFF"></td>
                </tr>
                <tr>
                    <td bgcolor="#f3f3f3" align="center">
                        <table align="center" class="table-inner" width="430" border="0" cellspacing="0"
                               cellpadding="0">
                            <tr>
                                <td height="40"></td>
                            </tr>
                            <!--headline-->
                            <tr>
                                <td align="center"
                                    style="font-family: 'Open Sans', Arial, sans-serif; font-size:36px;color:#414a51;font-weight: bold;letter-spacing: 2px;">
                                    You are invited!
                                </td>
                            </tr>
                            <!--end headline-->
                            <tr>
                                <td height="25"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25"></td>
                            </tr>
                            <!--content-->
                            <tr>
                                <td align="center"
                                    style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:14px; line-height: 28px;">
                                    {{ $company_name }} invited you to have access to view/manage the business book.
                                </td>
                            </tr>
                            <!--end content-->
                            <tr>
                                <td height="40"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFFFFF"
                        style="font-family: 'Open Sans', Arial, sans-serif; font-size:28px;color:#414a51;font-weight: bold;letter-spacing: 2px;">
                        Confidentiality Agreement
                    </td>
                </tr>
                <tr>
                    <td height="45" bgcolor="#FFFFFF"
                        style=" box-shadow:0px 3px 0px #ccd5dc;border-bottom-left-radius:6px;border-bottom-right-radius:6px;">
                        <p>It is understood and agreed to that the below identified discloser of confidential
                            information may provide certain information that is and must be kept confidential. To ensure
                            the protection of such information, and to preserve any confidentiality necessary under
                            patent and/or trade secret laws, it is agreed that</p>
                        <ol>
                            <li>
                                The Confidential Information to be disclosed can be described as and includes:
                                <p>This Agreement states the entire agreement between the parties concerning the
                                    disclosure of Confidential Information. Any addition or modification to this
                                    Agreement must be made in writing and signed by the parties.</p>
                            </li>
                            <li>The Recipient agrees not to disclose the confidential information obtained from the
                                discloser to anyone unless required to do so by law.
                            </li>
                            <li> If any of the provisions of this Agreement are found to be unenforceable, the remainder
                                shall be enforced as fully as possible and the unenforceable provision(s) shall be
                                deemed modified to the limited extent required to permit enforcement of the Agreement as
                                a whole.
                            </li>
                        </ol>
                        <p>WHEREFORE, the parties acknowledge that they have read and understand this Agreement and
                            voluntarily accept the duties and obligations set forth herein.</p>
                    </td>
                </tr>
                <tr>
                    <td height="35" bgcolor="#FFFFFF"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table class="table-full" bgcolor="#6ec8c7" style="border-radius:4px;" border="0"
                               cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="textbutton" height="45"
                                    style="font-family: 'Open sans', Arial, sans-serif; color:#FFFFFF; font-size:14px;padding-left: 15px;padding-right: 15px;">
                                    <a href="{{ url('collaborate/invite-response')}}?email={{ $email }}&code={{ $confirmation_code }}&response=1&type={{ $type }}&record={{$record}}">Accept
                                        Request</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15"></td>
                </tr>
            </table>
            <table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="10"></td>

                </tr>
                <tr>
                    <td>
                        <div class="text-center">
                            <p class="text-center y-center">Powered by <img src="{{$bmac}}images/ikooba.png"
                                                                            class="y-rebrand-logo">
                                <span class="y-technologies">Technologies</span>
                            </p>
                        </div>

                    </td>
                </tr>
                <!--social-->
                <tr>
                    <td align="center">
                        <table width="120" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                             src="https://onebmac.com/images/facebook.png" alt="img"/> alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                             src="https://onebmac.com/images/twitter.png" alt="img" alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                             src="https://onebmac.com/images/google.png" alt="img" alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                             src="https://onebmac.com/images/linkedin.png" alt="img" alt="img"/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="45"></td>
    </tr>
</table>
</body>

</html>