<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <title>BMAC | Business Collaboration</title>
    <style type="text/css">
        .ReadMsgBody {
            width: 100%;
            background-color: #ffffff;
        }

        .ExternalClass {
            width: 100%;
            background-color: #ffffff;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        html {
            width: 100%;
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            margin: 0;
            padding: 0;
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
            table-layout: fixed;
            margin: 0 auto;
        }

        table table table {
            table-layout: auto;
        }

        img {
            display: block !important;
            over-flow: hidden !important;
        }

        table td {
            border-collapse: collapse;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        img:hover {
            opacity: 0.9 !important;
        }

        a {
            color: #6ec8c7;
            text-decoration: none;
        }

        .textbutton a {
            font-family: 'open sans', arial, sans-serif !important;
            color: #ffffff !important;
        }

        .preference a {
            color: #6ec8c7 !important;
            text-decoration: underline !important;
        }

        .y-brand-green{
            color: #0d7c4a;
            font-weight: bolder;
        }
        img.y-rebrand-logo{
            display: inline !important;
            width: 3.5em;
            height: auto;
            margin-bottom: -6px;
        }
        .y-center{
            text-align: center;
        }


        .textbutton{
            background-color: #0d7d40;
        }


        /*Responsive*/
        @media only screen and (max-width: 640px) {
            body {
                width: auto !important;
            }

            table[class="table-inner"] {
                width: 90% !important;
            }

            table[class="table-full"] {
                width: 100% !important;
                text-align: center !important;
            }

            /* Image */
            img[class="img1"] {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            body {
                width: auto !important;
            }

            table[class="table-inner"] {
                width: 90% !important;
            }

            table[class="table-full"] {
                width: 100% !important;
                text-align: center !important;
            }

            /* image */
            img[class="img1"] {
                width: 100% !important;
                height: auto !important;
            }
        }
    </style>
</head>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#414a51">
    <?php $bmac = config('constants.bmac_url'); ?>
    <tr>
        <td align="center" height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <table bgcolor="#FFFFFF" style="border-radius:6px;" width="500" border="0" align="center" cellpadding="0"
                   cellspacing="0" class="table-inner">
                <tr>
                    <td height="40"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table class="table-inner" align="center" width="420" border="0" cellspacing="0"
                               cellpadding="0">
                            <!--Header Logo-->
                            <tr>
                                <td align="center" style="line-height: 0px;">
                                    <span class="y-brand-green">BMAC</span>
                                   {{-- <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                         src="{{ asset('images/ikooba.png') }}" alt="ikOOba"/> --}}
                                </td>
                            </tr>
                            <!--end Header Logo-->
                            <tr>
                                <td height="5"></td>
                            </tr>
                            <!--slogan-->
                            <tr>
                                <td align="center"
                                    style="font-family: 'Open Sans', Arial, sans-serif; color:#a7aeb3; font-size:12px;line-height: 28px; font-style:italic;">
                                    Business Collaboration
                                </td>
                            </tr>
                            <!--end slogan-->
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="20"></td>
                </tr>
                <!--image-->
                <tr>
                    <td align="center" style="line-height: 0px;">
                        {{--<img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1"--}}
                        {{--src="http://dummyimage.com/500x220" alt="img" width="100%" height="auto"/>--}}
                      {{--  <img style="display:block; line-height:0px; font-size:0px; border:0px;" class="img1"
                             src="{{ asset('images/ikooba_large.png') }}" alt="img" width="100%" height="auto"/>
                             --}}
                    </td>
                </tr>
                <!--end image-->
                <tr>
                    <td height="35"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table align="center" class="table-inner" width="420" border="0" cellspacing="0"
                               cellpadding="0">
                            <!--title-->
                            <tr>
                                <td align="center"
                                    style="font-family: 'Century Gothic', Arial, sans-serif; color:#414a51; font-size:28px;font-weight: bold; letter-spacing: 2px;">
                                    Failed Email delivery
                                </td>
                            </tr>
                            <!--end title-->
                            <tr>
                                <td height="15"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7"
                                                            style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--content-->
                            <tr>
                                <td align="center"
                                    style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:14px; line-height: 28px;">
                                    Hello <?php echo $first_name . ' ' . $last_name ?>,
                                    <p>{{$email}}</p>
                                  
                                </td>
                            </tr>
                            <!--end content-->
                            <tr>
                                <td height="25"></td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="text-center">
                            <p class="text-center y-center">Powered by <img src="{{$bmac}}images/ikooba.png" alt="img" />class="y-rebrand-logo">
                                <span class="y-technologies">Technologies</span>
                            </p>
                        </div>

                    </td>
                </tr>

                <tr>
                    <td height="40"></td>
                </tr>
            </table>
            <table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="25"></td>
                </tr>


                <tr>
                     <td align="center">
                        <!--social-->
                         <table width="120" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/facebook.png" alt="img" /> alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/twitter.png" alt="img"  alt="img" />
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/google.png" alt="img"  alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/linkedin.png" alt="img"  alt="img"/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <!--end social-->
                    </td>
                </tr>
                <tr>
                    <td height="55"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>