
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>{{ $name }}, thank you for your order.</title>
    <style type="text/css">
        .ReadMsgBody { width: 100%; background-color: #ffffff; }
        .ExternalClass { width: 100%; background-color: #ffffff; }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
        html { width: 100%; }
        body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
        table { border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto; }
        table table table { table-layout: auto; }
        img { display: block !important; over-flow: hidden !important; }
        table td { border-collapse: collapse; }
        .yshortcuts a { border-bottom: none !important; }
        img:hover { opacity:0.9 !important;}
        a { color: #6ec8c7; text-decoration: none; }
        .textbutton a { font-family: 'open sans', arial, sans-serif !important; color: #ffffff !important; }
        .footer-link a { color: #7f8c8d !important; }

        /*Responsive*/
        @media only screen and (max-width: 640px) {
            body { width: auto !important; }
            table[class="table-inner"] { width: 90% !important; }
            table[class="table-full"] { width: 100% !important; text-align: center !important; background: none !important; }
            /* Image */
            img[class="img1"] { width: 100% !important; height: auto !important; }
        }

        @media only screen and (max-width: 479px) {
            body { width: auto !important; }
            table[class="table-inner"] { width: 90% !important; }
            table[class="table-full"] { width: 100% !important; text-align: center !important; background: none !important; }
            /* image */
            img[class="img1"] { width: 100% !important; height: auto !important; }
        }
    </style>
</head>

<body>
<!--header-->
<?php $bmac = config('constants.bmac_url'); ?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eceff3">
    <tr>
        <td align="center">
            <table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
                <tr>
                    <td height="50"></td>
                </tr>
                <!--Header Logo-->
                <tr>
                    <td align="center" style="line-height: 0px;">
                        <span class="y-brand-green">BMAC</span>
                    </td>
                </tr>
                <!--end Header Logo-->
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
            </table>
            <table class="table-inner" width="350" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="30" align="right">
                                    <table width="30" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-left-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td rowspan="2" align="center" background="{{ asset('images/ikooba.png') }}" style="background-image: url({{ asset('images/ikooba.png') }}); background-repeat: repeat-x; background-size: auto; background-position: bottom;">
                                    <table style="border-radius:6px;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#6ec8c7">
                                        <!--title-->
                                        <tr>
                                            <td width="540" height="50" align="center" style="padding-left: 15px;padding-right: 15px; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px;color:#FFFFFF;line-height: 28px;font-weight: bold;">Your Order</td>
                                        </tr>
                                        <!--end title-->
                                    </table>
                                </td>
                                <td width="30" align="left">
                                    <table width="30" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-right-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end header-->
<!--Headline-->
<table bgcolor="#eceff3" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table class="table-inner" width="350" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td bgcolor="#FFFFFF" align="center">
                        <table align="center" class="table-inner" width="300" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="35"></td>
                            </tr>
                            <!--headline-->
                            <tr>
                                <td align="center" style="font-family: 'Open Sans', Arial, sans-serif; font-size: 22px;color:#414a51;font-weight: bold;line-height: 28px;">Thanks for your order, {{ $name }}</td>
                            </tr>
                            <!--end headline-->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td height="20">Here's your confirmation for order number {{ $reference_no }}. Review your receipt and get started using your products.</td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7" style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7" style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7" style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="15"></td>
                                            <td align="center">
                                                <table width="5" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="5" height="5" bgcolor="#6ec8c7" style="border-radius:10px;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end Headline-->
<!--list-->
<table bgcolor="#eceff3" align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="35" align="center">
            <table class="table-inner" bgcolor="#FFFFFF" width="350" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table align="center" class="table-inner" width="300" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <!--End Space-->
                                    <table class="table-full" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <!--title-->
                                        <tr>
                                            <td style="font-family: 'Open sans', Arial, sans-serif; color:#3b3b3b; font-size:16px; line-height: 20px;font-weight: bold;">Order Number: {{ $reference_no }}</td>
                                        </tr>
                                        <!--end title-->
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <!--content-->
                                        <tr>
                                            <td style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:13px; line-height: 20px;">You ordered for {{ $package }}</td>
                                        </tr>
                                        <!--end content-->
                                        <tr>
                                            <td height="10"></td>
                                        </tr>
                                        <!--price-->
                                        <tr>
                                            <td style="font-family: 'Open sans', Arial, sans-serif; color:#3b3b3b; font-size:20px; line-height: 20px;font-style: italic; font-weight: bold;">{{ number_format($amount, 2) }}</td>
                                        </tr>
                                        <!--end price-->
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" style="border-bottom:1px dotted #d4d4d4"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--end list-->


<!--Social-->
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eceff3">
    <tr>
        <td align="center">
            <table align="center" class="table-inner" width="350" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20"></td>
                </tr>
                <!--social-->
                <tr>
                    <td align="center">
                       <table width="120" border="0" align="center" cellpadding="0" cellspacing="0">
                                                  <tr>
                                                      <td align="center" style="line-height: 0px;">
                                                          <a href="#">
                                                              <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                                                  src="https://onebmac.com/images/facebook.png" alt="img" /> alt="img"/>
                                                          </a>
                                                      </td>
                                                      <td width="10"></td>
                                                      <td align="center" style="line-height: 0px;">
                                                          <a href="#">
                                                              <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                                                  src="https://onebmac.com/images/twitter.png" alt="img"  alt="img" />
                                                          </a>
                                                      </td>
                                                      <td width="10"></td>
                                                      <td align="center" style="line-height: 0px;">
                                                          <a href="#">
                                                              <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                                                  src="https://onebmac.com/images/google.png" alt="img"  alt="img"/>
                                                          </a>
                                                      </td>
                                                      <td width="10"></td>
                                                      <td align="center" style="line-height: 0px;">
                                                          <a href="#">
                                                              <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                                                  src="https://onebmac.com/images/linkedin.png" alt="img"  alt="img"/>
                                                          </a>
                                                      </td>
                                                  </tr>
                                              </table>
                    </td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <!--preference-->
                <tr>
                    <td class="footer-link" align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:12px; line-height: 28px;">
                        <a href="#">Webversion</a>
                        <span>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                        <a href="#">Unsubscribe</a>
                    </td>
                </tr>
                <!--end preference-->
            </table>
        </td>
    </tr>
    <tr>
        <td height="55"></td>
    </tr>
</table>
<!--end Social-->
</body>

</html>