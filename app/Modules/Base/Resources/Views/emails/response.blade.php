<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>BMAC Collaboration Request</title>
    <style type="text/css">
        .ReadMsgBody { width: 100%; background-color: #ffffff; }
        .ExternalClass { width: 100%; background-color: #ffffff; }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
        html { width: 100%; }
        body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
        table { border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto; }
        table table table { table-layout: auto; }
        img { display: block !important; over-flow: hidden !important; }
        table td { border-collapse: collapse; }
        .yshortcuts a { border-bottom: none !important; }
        img:hover { opacity:0.9 !important;}
        a { color: #6ec8c7; text-decoration: none; }
        .textbutton a { font-family: 'open sans', arial, sans-serif !important; color: #ffffff !important; }
        .preference-link a { color: #6ec8c7 !important; text-decoration: underline !important; }
        .button2 a { color: #414a51 !important; }
        table[class="buttonbox"] { width: 60% !important; }

        .y-brand-green{
            color: #0d7c4a;
            font-weight: bolder;
        }

        /*Responsive*/
        @media only screen and (max-width: 640px) {
            body { width: auto !important; }
            table[class="table-inner"] { width: 90% !important; }
            table[class="table-full"] { width: 100% !important; text-align: center !important; background: none !important; }
            table[class="buttonbox"] { width: 100% !important; }
            /* Image */
            img[class="img1"] { width: 100% !important; height: auto !important; }
        }

        @media only screen and (max-width: 479px) {
            body { width: auto !important; }
            table[class="table-inner"] { width: 90% !important; }
            table[class="table-full"] { width: 100% !important; text-align: center !important; background: none !important; }
            table[class="buttonbox"] { width: 100% !important; }
            /* image */
            img[class="img1"] { width: 100% !important; height: auto !important; }
        }
    </style>
</head>

<body>

    <?php $bmac = config('constants.bmac_url'); ?>
<table bgcolor="#414a51" background="{{ asset('images/background.png') }}" style="background-size:cover; background-repeat:repeat-x; background-position:top;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="50"></td>
    </tr>
    <tr>
        <td align="center">
            <table class="table-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="35" align="right" valign="bottom">
                                    <table width="35" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-left-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" background="{{ asset('images/title-bg.png') }}" style="background-image: url({{asset('images/title-bg.png') }}); background-repeat: repeat-x; background-size: auto; background-position: bottom;">
                                    <table style="border-radius:6px;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#6ec8c7">
                                        <tr>
                                            <td height="50" align="center">
                                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="table-inner">
                                                    <tr>
                                                        <td height="7"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#FFFFFF; font-size:16px; font-weight: bold;">Collaboration request response.</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="7"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="35" align="left" valign="bottom">
                                    <table width="35" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td height="25"></td>
                                        </tr>
                                        <tr>
                                            <td height="25" bgcolor="#FFFFFF" style="border-top-right-radius:6px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="30" bgcolor="#FFFFFF"></td>
                </tr>
                <!--logo-->
                <tr>
                    <td align="center" bgcolor="#FFFFFF">
                        <span class="y-brand-green">BMAC</span>
                       {{-- <a href="#">
                            <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="{{ asset('images/ikooba.png') }}" alt="logo" />
                        </a> --}}
                    </td>
                </tr>
                <!--end logo-->
                <tr>
                    <td height="25" bgcolor="#FFFFFF"></td>
                </tr>
                <tr>
                    <td bgcolor="#f3f3f3" align="center">
                        <table align="center" class="table-inner" width="430" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40"></td>
                            </tr>
                            <!--content-->
                            <tr>
                                <td align="center" style="font-family: 'Open sans', Arial, sans-serif; color:#7f8c8d; font-size:14px; line-height: 28px;">
                                    @if($response === '1')
                                        {{ $first_name.' '.$last_name }} has accepted your request to collaborate with you on BMAC.
                                    @else
                                        {{ $first_name.' '.$last_name }} has rejected your request to collaborate with you on BMAC.
                                    @endif
                                </td>
                            </tr>
                            <!--end content-->
                            <tr>
                                <td height="40"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="45" align="center" bgcolor="#FFFFFF" style=" box-shadow:0px 3px 0px #ccd5dc;border-bottom-left-radius:6px;border-bottom-right-radius:6px;"></td>
                </tr>
            </table>
            <table align="center" class="table-inner" width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="120" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/facebook.png" alt="img" /> alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/twitter.png" alt="img"  alt="img" />
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/google.png" alt="img"  alt="img"/>
                                    </a>
                                </td>
                                <td width="10"></td>
                                <td align="center" style="line-height: 0px;">
                                    <a href="#">
                                        <img style="display:block; line-height:0px; font-size:0px; border:0px;"
                                            src="https://onebmac.com/images/linkedin.png" alt="img"  alt="img"/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="45"></td>
    </tr>
</table>
</body>

</html>