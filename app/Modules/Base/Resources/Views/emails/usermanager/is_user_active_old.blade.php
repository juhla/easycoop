<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <center style="background-color:#E1E1E1;">
      <?php $bmac = config('constants.bmac_url'); ?>
      <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">

        <tr>
          <td align="center" valign="top" id="bodyCell">

            <!-- EMAIL HEADER // -->
            <!--
              The table "emailBody" is the email\'s container.
              Its width can be set to 100% for a color band
              that spans the width of the page.
            -->
            <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

              <!-- HEADER ROW // -->
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td valign="top" width="500" class="flexibleContainerCell">

                              <!-- CONTENT TABLE // -->
                              <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <!--
                                    The "invisibleIntroduction" is the text used for short preview
                                    of the email before the user opens it (50 characters max). Sometimes,
                                    you do not want to show this message depending on your design but this
                                    text is highly recommended.

                                    You do not have to worry if it is hidden, the next <td> will automatically
                                    center and apply to the width 100% and also shrink to 50% if the first <td>
                                    is visible.
                                  -->
                                  <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                      <tr>
                                        <td align="left" class="textContent">
                                          <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                            <!-- The introduction of your message preview goes here. Try to make it short. -->
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td align="right" valign="middle" class="flexibleContainerBox">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                      <tr>
                                        <td align="left" class="textContent">
                                          <!-- CONTENT // -->
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // END -->

            </table>
            <!-- // END -->

            <!-- EMAIL BODY // -->
            <!--
              The table "emailBody" is the email\'s container.
              Its width can be set to 100% for a color band
              that spans the width of the page.
            -->
            <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

              <!-- MODULE ROW // -->
              <!--
                To move or duplicate any of the design patterns
                in this email, simply move or copy the entire
                MODULE ROW section for each content block.
              -->
              <!-- MODULE ROW // -->
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td align="center" valign="top" width="500" class="flexibleContainerCell">

                              <!-- // CONTENT TABLE -->

                            </td>
                          </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->


              <!-- MODULE ROW // -->
              <!--  The "mc:hideable" is a feature for MailChimp which allows
                you to disable certain row. It works perfectly for our row structure.
                http://kb.mailchimp.com/article/template-language-creating-editable-content-areas/
              -->
              <tr mc:hideable>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td valign="top" width="500" class="flexibleContainerCell">



                            </td>
                          </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->


              <!-- MODULE ROW // -->
              <tr>
                <td align="center" valign="top" style="position: relative; bottom: 25px;">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                    <tr>
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                              <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                <tr>
                                  <td align="center" valign="top">

                                    <!-- CONTENT TABLE // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="border: 1px solid gray!important; padding:10px;">
                                      <tr>
                                        <td valign="top" class="textContent">
                                          <!--
                                            The "mc:edit" is a feature for MailChimp which allows
                                            you to edit certain row. It makes it easy for you to quickly edit row sections.
                                            http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                                          -->


                                           <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

                                            <div mc:edit="body" style="background: #097a47; width:100%; height: 60px;">
                                            <img src="https://onebmac.com/mages/ikooba.png"  style="padding-top:2px; padding-left:10px; width:33%;">
                                           </div><br>

                                  
                                            <?php echo $body; ?>
                                            
                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                             Have questions? Call Jide 0907 842 2233 or send an email to business@ikooba.com <br><br>
                                            </div>

                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">                                 
                                            <p>Keep winning and have a great day!</p>
                                            <p>Jide, </p>
                                            <p>BMAC Customer Happiness Squad</p>
                                            </div>
                                            <div>
                              
                                            </div>
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- // CONTENT TABLE -->

                                  </td>
                                </tr>

                              </table>

                            </td>
                          </tr>
                        </table>

                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                    <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="320" style="width: 240.0pt; border-collapse: collapse"><tbody><tr><td width="145" style="width: 108.75pt;">
<p class="MsoNormal"><span style="font-family: "Corbel",sans-serif">Connect with us:
</span><!-- o ignored --></p>
</td>
<td width="30" style="width: 22.5pt; padding: .75pt .75pt .75pt .75pt">
<p align="center" style="text-align: left"><a href="https://www.facebook.com/ikoobaHQ" target="_blank" rel="noreferrer"><b><span style="font-size: 10.0pt; font-family: "Corbel",sans-serif; color: white; text-decoration: none"><img border="0" width="27" height="27" style="width: .2812in; height: .2812in" id="_x0000_i1045" src="https://onebmac.com/images/facebook.png"  alt="fb"></span></b></a><span style="font-size: 10.0pt; font-family: "Times New Roman",serif"><!-- o ignored --></span></p>
</td>
<td width="35" style="width: 26.25pt; padding: .75pt .75pt .75pt .75pt">
<p align="center" style="text-align: left"><a href="https://twitter.com/ikOObaHQ" target="_blank" rel="noreferrer"><b><span style="font-size: 10.0pt; font-family: "Corbel",sans-serif; color: white; text-decoration: none"><img border="0" width="27" height="27" style="width: .2812in; height: .2812in" id="_x0000_i1044" src="https://onebmac.com/images/twitter.png" alt="tw"></span></b></a><span style="font-size: 10.0pt; font-family: "Times New Roman",serif"><!-- o ignored --></span></p>
</td>
<td width="35" style="width: 26.25pt; padding: .75pt .75pt .75pt .75pt">
<p align="center" style="text-align: left"><a href="https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg" target="_blank" rel="noreferrer"><b><span style="font-size: 10.0pt; font-family: "Corbel",sans-serif; color: white; text-decoration: none"><img border="0" width="27" height="27" style="width: .2812in; height: .2812in" id="_x0000_i1044" src="https://onebmac.com/images/youtube.png"  alt="tw"></span></b></a><span style="font-size: 10.0pt; font-family: "Times New Roman",serif"><!-- o ignored --></span></p>
</td>

</tr></tbody></table>
                  </table>

                  <!-- // CENTERING TABLE -->
                </td>
              </tr>
              <!-- // MODULE ROW -->

            </table>

            <!-- // END -->

            <!-- EMAIL FOOTER // -->
            <!--
              The table "emailBody" is the email\'s container.
              Its width can be set to 100% for a color band
              that spans the width of the page.
            -->
            <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">

              <!-- FOOTER ROW // -->
              <!--
                To move or duplicate any of the design patterns
                in this email, simply move or copy the entire
                MODULE ROW section for each content block.
              -->
              <tr>
                <td align="center" valign="top">
                  <!-- CENTERING TABLE // -->
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">
                        <!-- FLEXIBLE CONTAINER // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                          <tr>
                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                              <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                <tr>
                                  <td valign="top" bgcolor="#E1E1E1">

                                    <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                      <!--<div>
                                     
                                      </div> -->
                                    </div>

                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!-- // FLEXIBLE CONTAINER -->
                      </td>
                    </tr>
                  </table>
                  <!-- // CENTERING TABLE -->
                </td>
              </tr>

            </table>
            <!-- // END -->

          </td>
        </tr>
      </table>
    </center>