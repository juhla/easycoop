<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:mc="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
        <title>BMAC | Business Collaboration</title>
        <style type="text/css">
            .ReadMsgBody {
                width: 100%;
                background-color: #ffffff;
            }

            .ExternalClass {
                width: 100%;
                background-color: #ffffff;
            }

            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%;
            }

            html {
                width: 100%;
            }

            body {
                -webkit-text-size-adjust: none;
                -ms-text-size-adjust: none;
                margin: 0;
                padding: 0;
                color: gray;
            }

            table {
                border-spacing: 0;
                border-collapse: collapse;
                table-layout: fixed;
                margin: 0 auto;

            }

            table table table {
                table-layout: auto;
            }

            img {
                display: block !important;
                over-flow: hidden !important;
            }

            table td {
                border-collapse: collapse;
                color: gray !important;
            }

            .yshortcuts a {
                border-bottom: none !important;
            }

            img:hover {
                opacity: 0.9 !important;
            }

            a {
                color: #6ec8c7;
                text-decoration: none;
            }

            .textbutton a {
                font-family: 'open sans', arial, sans-serif !important;
                color: #ffffff !important;
            }

            .preference a {
                color: #6ec8c7 !important;
                text-decoration: underline !important;
            }
            .y-brand-green{
                color: #0d7c4a;
                font-weight: bolder;
            }
            img.y-rebrand-logo{
                display: inline !important;
                width: 3.5em;
                height: auto;
            }
            .y-center{
                text-align: center;
            }


            .textbutton{
                background-color: #0d7d40;
            }

            /*Responsive*/
            @media only screen and (max-width: 640px) {
                body {
                    width: auto !important;
                }


                table[class="table-inner"] {
                    width: 90% !important;
                }

                table[class="table-full"] {
                    width: 100% !important;
                    text-align: center !important;
                }

                /* Image */
                img[class="img1"] {
                    width: 100% !important;
                    height: auto !important;
                }
            }

            @media only screen and (max-width: 479px) {
                body {
                    width: auto !important;
                }

                table[class="table-inner"] {
                    width: 90% !important;
                }

                table[class="table-full"] {
                    width: 100% !important;
                    text-align: center !important;
                }

                /* image */
                img[class="img1"] {
                    width: 100% !important;
                    height: auto !important;
                }
            }
        </style>
    </head>
    <body>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;">
        <?php $bmac = config('constants.bmac_url'); ?>
        <tr>
            <td align="center" valign="top" id="bodyCell">

                <!-- EMAIL HEADER // -->
                <!--
                  The table "emailBody" is the email\'s container.
                  Its width can be set to 100% for a color band
                  that spans the width of the page.
                -->
                <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailHeader">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table  border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="500" class="flexibleContainerCell">

                                                    <!-- CONTENT TABLE // -->
                                                    <table  align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <!--
                                                              The "invisibleIntroduction" is the text used for short preview
                                                              of the email before the user opens it (50 characters max). Sometimes,
                                                              you do not want to show this message depending on your design but this
                                                              text is highly recommended.

                                                              You do not have to worry if it is hidden, the next <td> will automatically
                                                              center and apply to the width 100% and also shrink to 50% if the first <td>
                                                              is visible.
                                                            -->
                                                            <td align="left" valign="middle" id="invisibleIntroduction" class="flexibleContainerBox" style="display:none !important; mso-hide:all;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="left" class="textContent">
                                                                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                                <!-- The introduction of your message preview goes here. Try to make it short. -->
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="left" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <!-- EMAIL BODY // -->
                <!--
                  The table "emailBody" is the email\'s container.
                  Its width can be set to 100% for a color band
                  that spans the width of the page.
                -->
                <table bgcolor="#FFFFFF"  border="0" cellpadding="0" cellspacing="0" width="500" id="emailBody">

                    <!-- MODULE ROW // -->
                    <!--
                      To move or duplicate any of the design patterns
                      in this email, simply move or copy the entire
                      MODULE ROW section for each content block.
                    -->
                    <!-- MODULE ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">

                                                    <!-- // CONTENT TABLE -->

                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // MODULE ROW -->


                    <!-- MODULE ROW // -->
                    <!--  The "mc:hideable" is a feature for MailChimp which allows
                      you to disable certain row. It works perfectly for our row structure.
                      http://kb.mailchimp.com/article/template-language-creating-editable-content-areas/
                    -->
                    <tr mc:hideable>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="500" class="flexibleContainerCell">



                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // MODULE ROW -->


                    <!-- MODULE ROW // -->
                    <tr>
                        <td align="center" valign="top" style="position: relative; bottom: 25px;">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" valign="top">

                                                                <!-- CONTENT TABLE // -->
                                                                <table style="padding:2em;" border="0" cellpadding="0" cellspacing="0" width="100%"  style="border: 1px solid gray!important; padding:10px;">
                                                                    <tr>
                                                                        <td valign="top" class="textContent">
                                                                            <!--
                                                                              The "mc:edit" is a feature for MailChimp which allows
                                                                              you to edit certain row. It makes it easy for you to quickly edit row sections.
                                                                              http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                                                                            -->


                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">

                                                                                <div mc:edit="body" style="background: #097a47; width:100%; padding: .6em; height: 40px;" style="text-align: center">
                                                                                    <!-- <img src="https://ikooba.com/email_files/images/logo.png" style="padding-top:2px; padding-left:10px; width:33%;"> -->
                                                                                    <span style="color:#fff; font-size:1.3em;   font-weight: bolder;text-align:center;">BMAC</span>
                                                                                    <span style="color:#fff; font-size: 8px;" >Business Collaboration</span>
                                                                                </div><br>

                                                                                <center><div mc:edit="body" style="width:100%;">
                                                                                        <h4 style="color: #097a47; ">Registration completed.</h4>
                                                                                    </div></centre><br>

                                                                                    <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                        Welcome to <span style="color: #0d7c4a;font-weight: bolder"> BMAC</span>,<br><br>
                                                                                    </div>

                                                                                    @if($user_role == 'business-owner')
                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                            Thank you for signing up. It’s time to take your business to the next level. <br><br>
                                                                                        </div>

                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                            The BMAC gives you the tools you need to measure and assess your business performance, collaborate with potential investors, and keep accurate records of your daily sales and purchases.  <br><br>
                                                                                        </div>

                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                            You don’t have to deal with the stress of sorting and filing heaps of records. You can now focus on the important stuff! Like growing your business<br><br>
                                                                                        </div>

                                                                                        <center><div mc:edit="body" style="width:100%;">
                                                                                                <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">Login</a></h4>
                                                                                            </div></centre><br>

                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                Keep posting your business records in the BMAC and open your business performance to limitless growth.<br>
                                                                                                <p>Need help getting through the system? Download our user guide <a style="font-weight: 900; text-decoration: none; color: #097a47;" href="https://ikooba.com/email_files/images/ikOOba_Technologies_User_Guide.pdf" download>here.</a></p><br>
                                                                                            </div>

                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                Call us on <span style="color: green; font-size: 14px;">0907 842 2233</span> or email us at <span style="color: green; font-size: 14px;">business@ikooba.com</span><br><br><br>
                                                                                            </div>
                                                                                            @elseif($user_role == 'professional')
                                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                    Thank you for signing up. You can add even more value to your clients by using the BMAC to foster a closer and more transparent relationship with them.<br><br>
                                                                                                </div>

                                                                                                <center><div mc:edit="body" style="width:100%;">
                                                                                                        <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">SIGN IN</a></h4>
                                                                                                    </div></centre><br>

                                                                                                    <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                        Download BMAC App to access your account anywhere anytime. <br><br>
                                                                                                    </div>
                                                                                                    <!-- Stores icons -->
                                                                                                    <center><div mc:edit="body" style="width:100%;">
                                                                                                            <img src="https://sme.onebmac.com/appstore.png"
                                                                                                                 style="padding-top:-3px; padding-left:10px;">
                                                                                                            <img src="https://sme.onebmac.com/images/playstore.png"
                                                                                                                 style="padding-top:-3px; padding-left:10px;">
                                                                                                        </div></centre><br>
                                                                                                        <!-- Stores icons -->

                                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                            Have questions? Call Jide on 0907 842 2233 or send an email to business@ikooba.com<br><br>
                                                                                                        </div>
                                                                                                        @elseif($user_role == 'finance-provider')
                                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                Thank you for signing up. You can now get a detailed overview of SME business’ portfolios, thus ensuring business transparency at the highest level.  <br><br>
                                                                                                            </div>

                                                                                                            <center><div mc:edit="body" style="width:100%;">
                                                                                                                    <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">Get Started</a></h4>
                                                                                                                </div></centre><br>

                                                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                    Download BMAC App to access your account on the go <br><br>
                                                                                                                </div>
                                                                                                            <!-- Stores icons -->
                                                                                                            <center><div mc:edit="body" style="width:100%;">
                                                                                                                    <img src="https://sme.onebmac.com/images/appstore.png"
                                                                                                                         style="padding-top:-3px; padding-left:10px;">
                                                                                                                    <img src="https://sme.onebmac.com/images/playstore.png"
                                                                                                                         style="padding-top:-3px; padding-left:10px;">
                                                                                                                </div></centre><br>
                                                                                                                    <!-- Stores icons -->

                                                                                                                    <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                        Call us on 0907 842 2233 or email us at business@ikooba.com<br><br>
                                                                                                                    </div>
                                                                                                                    @else
                                                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                            Thank you for signing up.<br>
                                                                                                                            You can add even more value to your clients by using the BMAC to foster a closer and more transparent relationship with them.<br><br>
                                                                                                                        </div>

                                                                                                                        <center><div mc:edit="body" style="width:100%;">
                                                                                                                                <h4><a style="font-weight: 100; text-decoration: none; color: #ffffff; background: #097a47; padding: 9px 30px;" href="https://ikooba.com/bmac/secure/sign-in">SIGN IN</a></h4>
                                                                                                                            </div></centre><br>

                                                                                                                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                                Download BMAC App to access your account anywhere anytime. <br><br>
                                                                                                                            </div>
                                                                                                                            <!-- Stores icons -->
                                                                                                                            <center><div mc:edit="body" style="width:100%;">
                                                                                                                                    <img src="{{$bmac}}images/appstore.png"style="padding-top:-3px; padding-left:10px;">
                                                                                                                                    <img src="{{$bmac}}images/playstore.png" style="padding-top:-3px; padding-left:10px;">
                                                                                                                                </div></centre><br>
                                                                                                                                <!-- Stores icons -->

                                                                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                                    Have questions? Call Jide on 0907 842 2233 or send an email to business@ikooba.com <br><br>
                                                                                                                                </div>

                                                                                                                                @endif

                                                                                                                                <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                                                                    <p>Keep winning and have a great day!, </p>
                                                                                                                                    <p>Jide, </p>
                                                                                                                                    <p>Customer Happiness Squad</p>

                                                                                                                                </div>
                                                                                                                                <div>

                                                                                                                                </div>
                                                                                                                                <div class="text-center">
                                                                                                                                    <p class="text-center y-center">Powered by <img src="{{ asset('images/slide-logo.png') }}" class="y-rebrand-logo">
                                                                                                                                        <span class="y-technologies">Technologies</span>
                                                                                                                                    </p>
                                                                                                                                </div>


                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <!-- // CONTENT TABLE -->

                                                            </td>
                                                        </tr>

                                                    </table>

                                                </td>
                                            </tr>
                                        </table>

                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                                <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="320" style="width: 240.0pt; border-collapse: collapse"><tbody><tr><td width="145" style="width: 108.75pt;">
                                            <p class="MsoNormal"><span style="font-family: "Corbel",sans-serif">Connect with us:
                                                </span><!-- o ignored --></p>
                                        </td>
                                        <td width="30" style="width: 22.5pt; padding: .75pt .75pt .75pt .75pt">
                                            <p align="center" style="text-align: left"><a
                                                        href="https://www.facebook.com/ikoobaHQ" target="_blank"
                                                        rel="noreferrer"><b><span
                                                                style="font-size: 10.0pt; font-family: " Corbel",sans-serif;
                                                        color: white; text-decoration: none"><img border="0" width="27"
                                                                                                  height="27"
                                                                                                  style="width: .2812in; height: .2812in"
                                                                                                  id="_x0000_i1045"
                                                                                                  src="https://sme.onebmac.com/images/facebook.png"
                                                                                                  alt="fb"></span>
                                                    </b></a><span style="font-size: 10.0pt; font-family: " Times New
                                                                  Roman",serif"><!-- o ignored --></span></p>
                                        </td>
                                        <td width="35" style="width: 26.25pt; padding: .75pt .75pt .75pt .75pt">
                                            <p align="center" style="text-align: left"><a
                                                        href="https://twitter.com/ikOObaHQ" target="_blank"
                                                        rel="noreferrer"><b><span
                                                                style="font-size: 10.0pt; font-family: " Corbel",sans-serif;
                                                        color: white; text-decoration: none"><img border="0" width="27"
                                                                                                  height="27"
                                                                                                  style="width: .2812in; height: .2812in"
                                                                                                  id="_x0000_i1044"
                                                                                                  src="https://sme.onebmac.com/images/twitter.png"
                                                                                                  alt="tw"></span>
                                                    </b></a><span style="font-size: 10.0pt; font-family: " Times New
                                                                  Roman",serif"><!-- o ignored --></span></p>
                                        </td>
                                        <td width="35" style="width: 26.25pt; padding: .75pt .75pt .75pt .75pt">
                                            <p align="center" style="text-align: left"><a
                                                        href="https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg"
                                                        target="_blank" rel="noreferrer"><b><span
                                                                style="font-size: 10.0pt; font-family: " Corbel",sans-serif;
                                                        color: white; text-decoration: none"><img border="0" width="27"
                                                                                                  height="27"
                                                                                                  style="width: .2812in; height: .2812in"
                                                                                                  id="_x0000_i1044"
                                                                                                  src="https://sme.onebmac.com/images/youtube.png"
                                                                                                  alt="tw"></span>
                                                    </b></a><span style="font-size: 10.0pt; font-family: " Times New
                                                                  Roman",serif"><!-- o ignored --></span></p>
                                        </td>

                                    </tr></tbody></table>
                            </table>

                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // MODULE ROW -->

                </table>

                <!-- // END -->

                <!-- EMAIL FOOTER // -->
                <!--
                  The table "emailBody" is the email\'s container.
                  Its width can be set to 100% for a color band
                  that spans the width of the page.
                -->
                <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="500" id="emailFooter">

                    <!-- FOOTER ROW // -->
                    <!--
                      To move or duplicate any of the design patterns
                      in this email, simply move or copy the entire
                      MODULE ROW section for each content block.
                    -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td valign="top" bgcolor="#E1E1E1">

                                                                <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                                                    <!--<div>

                                                                    </div> -->
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>

                </table>
                <!-- // END -->

            </td>
        </tr>
    </table>


    </body>

</html>
