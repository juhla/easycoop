<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>BMAC Cloud Accounting</title>

    <!-- Mobile Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png"/>

    <!-- CSS
    ================================================== -->
    <!-- Fontawesome Icon font -->
    <link rel="stylesheet" href="{{asset('rebrand/css/font-awesome.min.css')}}">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{asset('rebrand/css/bootstrap.min.css')}}">
    <!-- Animate.css -->
    <!--         <link rel="stylesheet" href="css/animate.css"> -->
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('rebrand/css/owl.carousel.css')}}">
    <!-- Grid Component css -->
    <link rel="stylesheet" href="{{asset('rebrand/css/component.css')}}">
    <!-- Slit Slider css -->
    <link rel="stylesheet" href="{{asset('rebrand/css/slit-slider.css')}}">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{asset('rebrand/css/main.css')}}">
    <!-- Media Queries -->
    <link rel="stylesheet" href="{{asset('rebrand/css/media-queries.css')}}">

    <!-- styled by Festus -->
    <link rel="stylesheet" href="{{asset('css/404error.css')}}">

    <!--
    Google Font
    =========================== -->

    <!-- Oswald / Title Font -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'> -->
    <!-- Ubuntu / Body Font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('rebrand/css/style.css')}}">
    <!-- Modernizer Script for old Browsers -->
    <script src="{{asset('rebrand/js/modernizr-2.6.2.min.js')}}"></script>
    <!--    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/> -->


    <?php if(config("app.env") == "production"): ?>
    <script src="{{ asset('js/chat/countly.js') }}"></script>
    <?php endif; ?>


</head>


<body id="">
<?php if(config("app.env") == "production"): ?>
@include('layouts._analytics')
<?php endif; ?>

<!-- Fixed Navigation
		        ==================================== -->
                <div class="b1">
                    <div class="b2">
                    <a href="{{url('/')}}">
                        HOME
                    </a>
                    </div>
                  </div>
                <!--
End Fixed Navigation
==================================== -->
<div class="a">
    <div class="a1">
      <div class="a11">
        Oops!
      </div>
      <div class="a12">
        We can't seem to find the page you are looking for.
      </div>
      <div class="a14c">
        Contact us:
      </div>
      <div class="a14">
        ITSupport@myeasycoop.com
        <br>
        cfc@myeasycoop.com
      </div>
    </div>
    <div class="a2">
      <div class="a21">
      <img src="{{asset('images/5e2aeda11d337.png')}}" alt="">
      </div>
    </div>
</div>


<!-- Modal -->
@include('frontpagefolder.modal')
<!-- /.modal -->


<!-- Main jQuery -->
<script src="{{asset('rebrand/js/jquery-1.11.0.min.js')}}"></script>


<!-- Bootstrap 3.1 -->
<script src="{{asset('rebrand/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('rebrand/slick/slick.min.js')}}"></script>
<!-- Slitslider -->
<script src="{{asset('rebrand/js/jquery.slitslider.js')}}"></script>
<script src="{{asset('rebrand/js/jquery.ba-cond.min.js')}}"></script>
<!-- Parallax -->
<script src="{{asset('rebrand/js/jquery.parallax-1.1.3.js')}}"></script>
<!-- Owl Carousel -->
<script src="{{asset('rebrand/js/owl.carousel.min.js')}}"></script>
<!-- Portfolio Filtering -->
<script src="{{asset('rebrand/js/jquery.mixitup.min.js')}}"></script>
<!-- Custom Scrollbar -->
<script src="{{asset('rebrand/js/jquery.nicescroll.min.js')}}"></script>
<!-- Jappear js -->
<script src="{{asset('rebrand/js/jquery.appear.js')}}"></script>
<!-- Pie Chart -->
<script src="{{asset('rebrand/js/easyPieChart.js')}}"></script>
<!-- jQuery Easing -->
<script src="{{asset('rebrand/js/jquery.easing-1.3.pack.js')}}"></script>
<!-- tweetie.min -->
<script src="{{asset('rebrand/js/tweetie.min.js')}}"></script>
<!-- Google Map API -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!-- Highlight menu item -->
<script src="{{asset('rebrand/js/jquery.nav.js')}}"></script>
<!-- Sticky Nav -->
<script src="{{asset('rebrand/js/jquery.sticky.js')}}"></script>
<!-- Number Counter Script -->
<script src="{{asset('rebrand/js/jquery.countTo.js')}}"></script>
<!-- wow.min Script -->
<script src="{{asset('rebrand/js/wow.min.js')}}"></script>
<!-- For video responsive -->
<script src="{{asset('rebrand/js/jquery.fitvids.js')}}"></script>
<!-- Grid js -->
<script src="{{asset('rebrand/js/grid.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('rebrand/js/custom.js')}}"></script>


</body>
</html>