<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Site Maintenance</title>
    <style type="text/css">
        body {
            text-align: center;
            padding: 150px;
        }

        h1 {
            font-size: 50px;
        }

        body {
            font: 20px Helvetica, sans-serif;
            color: #333;
        }

        #article {
            display: block;
            text-align: left;
            width: 650px;
            margin: 0 auto;
        }

        a {
            color: #dc8100;
            text-decoration: none;
        }

        a:hover {
            color: #333;
            text-decoration: none;
        }
    </style>

</head>
<body>
<div id="article">
    <h1>We’ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience but we’re performing some maintenance at the moment.
            If you need to, you can always contact us on <b>0907-842-2233 or 0811-582-8444</b>,
            otherwise we’ll be back online shortly! </p>
        <p>— BMAC</p>

        <a href="javascript:;" class="list-group-item" onclick="window.history.back();">
            <i class="fa fa-chevron-right icon-muted"></i>
            <i class="fa fa-fw fa-home icon-muted"></i> Go back
        </a>
    </div>


</div>
</body>
</html>