<!DOCTYPE html><html lang="en" class="app">
<head>
    <meta charset="utf-8" /> <title>BMAC | Error</title>
    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/app.v1.css') }}" type="text/css" />
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie/html5shiv.js') }}'"></script>
    <script src="{{ asset('js/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/ie/excanvas.js') }}"></script>
    <![endif]-->
</head>
<body class="">
<section id="content">
    <div class="row m-n">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="text-center m-b-lg">
                <h1 class="h text-white animated fadeInDownBig">404</h1>
                <small>Oops, an error occurred. Page not found!</small>
            </div>
            <div class="list-group m-b-sm bg-white m-b-lg">
                <a href="javascript:;" class="list-group-item" onclick="window.history.back();">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <i class="fa fa-fw fa-home icon-muted"></i> Go back
                </a>
                <a href="#" class="list-group-item">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <i class="fa fa-fw fa-question icon-muted"></i> Send us a tip
                </a>
                <a href="#" class="list-group-item">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <span class="badge">021-215-584</span>
                    <i class="fa fa-fw fa-phone icon-muted"></i> Call us </a>
            </div>
        </div>
    </div>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder clearfix">
        <p> <small>ikooba.com | BMAC<br>&copy; {{ date('Y') }}</small> </p>
    </div>
</footer>
<!-- / footer -->
<!-- Bootstrap --> <!-- App -->
<script src="{{ asset('js/app.v1.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>
</body>
</html>