<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8"/>
    <title>BMAC | Error</title>
    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/app.v1.css') }}" type="text/css"/>
    <!--[if lt IE 9]>
    <script src="{{ asset('js/ie/html5shiv.js') }}'"></script>
    <script src="{{ asset('js/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/ie/excanvas.js') }}"></script>
    <![endif]-->
</head>
<body class="">
<section id="content">
    <div class="row m-n">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="text-center m-b-lg">
                <h1 class="h text-white animated fadeInDownBig">500</h1>
                <small>Something went wrong. Internal server error!</small>
            </div>
            <div class="list-group m-b-sm bg-white m-b-lg">
                <a href="javascript:;" class="list-group-item" onclick="window.history.back();">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <i class="fa fa-fw fa-home icon-muted"></i> Go back
                </a>
                <a href="#" class="list-group-item">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <i class="fa fa-fw fa-question icon-muted"></i> Send us a tip
                </a>
                <a href="#" class="list-group-item">
                    <i class="fa fa-chevron-right icon-muted"></i>
                    <span class="badge">021-215-584</span>
                    <i class="fa fa-fw fa-phone icon-muted"></i> Call us </a>
            </div>
            <div class="content">
                <div class="title">Something went wrong.</div>
                @if(app()->bound('sentry') && !empty(Sentry::getLastEventID()))
                    <div class="subtitle">Error ID: {{ Sentry::getLastEventID() }}</div>

                    <!-- Sentry JS SDK 2.1.+ required -->
                    <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

                    <script>
                        Raven.showReportDialog({
                            eventId: '{{ Sentry::getLastEventID() }}',
                            // use the public DSN (dont include your secret!)
                            dsn: 'http://f46a2f260f324e78bd073ebafc5b2525@173.255.241.93:9000/4',
                            user: {
                                'name': 'Jane Doe',
                                'email': 'jane.doe@example.com'
                            }
                        });
                    </script>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder clearfix">
        <p>
            <small>ikooba.com | BMAC<br>&copy; {{ date('Y') }}</small>
        </p>
    </div>
</footer>
<!-- / footer -->
<!-- Bootstrap --> <!-- App -->
<script src="{{ asset('js/app.v1.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>
</body>
</html>