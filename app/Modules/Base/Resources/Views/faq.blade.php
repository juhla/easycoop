@extends('layouts.main')

@section('content')
    <style>
        .dd {
            width: 100% !important;
        }
    </style>
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Frequently Asked Questions</h3>
            </div>
            <div class="row">

                <div class="col-sm-8">
                    <div class="panel-group m-b" id="accordion2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> What is the duration of the Trial period after I have signed up? </a>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" style="">
                                <div class="panel-body text-sm">
                                    There is no limit to how long you can use the basic services of BMAC
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                    I got a message that my session has expired. What does this message mean?
                                </a>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body text-sm"><p>
                                        When working on BMAC and the system does not detect any activity for 2 hours, it will log you off automatically.
                                        This is for your security. If you were busy processing transactions and your session expired,
                                        the system will save your transactions so that when you log back in you can restore any unprocessed transactions.
                                        Please note: In order for BMAC to save unprocessed transactions, you have to use one of the following internet browser versions:

                                    <ul>
                                        <li>Internet Explorer Version 8 or higher</li>
                                        <li>Firefox Version 3.5 or higher</li>
                                        <li>Chrome Version 4 or higher</li>
                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How do I invite my accountant as a user on BMAC?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                    On the left menu, the “Collaborators” option will allow you to invite users such as your accountant, employee & influencers. To add a user, click on Add Collaborator and the fill in the user details on the screen. Your accountant or additional user will only have access to your books base on the access type granted.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    Customizing the Chart of Account on BMAC
                                </a>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                    BMAC allows you to create and edit the chart of accounts base on your company needs.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How many companies can I create on BMAC?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                    You can create the amount of companies that you are registered for. To verify this, go to the Account Settings menu and select Companies tab to view the number of companies that you are registered for. To increase the number of companies you will need to select the option Upgrade My Account on the Companies Tab. Should you wish to decrease the number of companies you will be required to email us with your request and an agent will be in touch with you.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    Can I reverse transactions I have processed in error?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    Can I access my data even when the power is out?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    What happens to my data when there is a power cut or there is load shedding?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How do I back up my data on BMAC?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How do I change my email address?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    Can I have my data returned to me should I choose to discontinue with BAMC?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    How secure is my data when working in BMAC?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    I have reset my Password and I am still waiting for confirmation from BMAC to acknowledge this, what do I do now?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                    Can I pay online should I want to purchase BMAC premium modules?
                                </a>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body text-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </section>
    </section>
@stop