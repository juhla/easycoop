<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="feedbackform" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span id="title">Feedback</span> <span class="semi-bold">Form</span></h5>
                    <p class="p-b-10">Please leave us a feedback. Tell us about your experience and observations. </p>
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ url('help/feedback') }}" method="post" id="feedbackForm" data-validate="parsley">
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Subject <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="subject" id="subject" data-required required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Feedback</label>
                                        <textarea class="form-control" rows="10" name="feedback" id="feedback" data-required required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                            </div>
                            <div class="col-sm-4 m-t-10 sm-m-t-10">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <button type="reset" class="btn btn-default m-t-5 close-feedback-modal">Close</button>
                                <button type="submit" class="btn btn-info m-t-5">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->