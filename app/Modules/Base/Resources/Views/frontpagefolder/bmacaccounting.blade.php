<!DOCTYPE html>

    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
        <title>BMAC Cloud Accounting</title>
		
		<!-- Mobile Specific Meta
		================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="keywords" content="Accounting for Nigerian SMEs,Online accounting software,Accounting software for small-medium businesses,Business solutions in Nigeria,Cloud accounting,What is a cloud accounting software,Accounting software,Accounting software for businesses,Organization that produces accounting software,Accounting software for Nigerians,How to gain control of your business,Business development solutions,How to grow your small business,Business growth,How to choose the best accounting software,Good accounting software solutions,Affordable accounting software,Accounting technology solution,Sage,Wave,Quickbooks,Fintech industry,How to solve your business problems,How to make sales in no time,how to grow your business fast,step by step business development process,How to do business and be more profitable,sales force,How to choose the best sales representative,How to choose a sales agent,How to hire a sales rep,How to develop sales and marketing strategy that works,how can sales and marketing work together,Difference between sales and marketing,Difference between profit and income,Profit,Income,Inventory,What is an inventory software,Buy online inventory software,How to manage my inventory,Online Inventory management software,Benefits of stock taking,Stocking taking interval,How to manage my warehouse,online HR-payroll software,Cloud staff-payroll manager,are inventories assets or liabilities,Meaning of inventory,Inventory app,Benefits of cloud accounting ,Do it yourself payroll software for free,How much does payroll software cost,Best payroll software online,How HR-Payroll software works,where to buy a payroll software,How to choose the best payroll software,why a payroll software is important,Do I need a payroll software?,Financial Reporting,Financial Strategizing,General Accounting,Income Tax Planning,Internal Controls/Technology,Investment Strategies,Management Accounting,Payroll Management,Personal Financial Planning,Profit and Loss Management,Revenue Forecasting,Sales Forecasting,Tax Schedules / Remittances,Venture Capital  Financing,
		Accounting Systems and Controls,Auditing,Budgeting,Capital Acquisitions,Cash Flow Analysis,Corporate Reporting,Cost Accounting,Credit / Debt Management,Estate Planning,Financial Compliance,Financial Management,Financial Planning,Financial Presentations,Cloud accounting,Inventory accounting,Inventory audit report,inventory management,Online inventory management software,Inventory turnover,Inventory balance sheet,Inventory control">	
		
		<!-- Favicon -->
		 <link rel="shortcut icon" href="{{asset('homepage/img/favicon.ico')}}" type="image/x-icon">
		
		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{{asset('rebrand/css/font-awesome.min.css')}}">
		<!-- bootstrap.min css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/bootstrap.min.css')}}">
		<!-- Animate.css -->
<!--         <link rel="stylesheet" href="css/animate.css"> -->
		<!-- Owl Carousel -->
        <link rel="stylesheet" href="{{asset('rebrand/css/owl.carousel.css')}}">
		<!-- Grid Component css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/component.css')}}">
		<!-- Slit Slider css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/slit-slider.css')}}">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{asset('rebrand/css/main.css')}}">
		<!-- Media Queries -->
        <link rel="stylesheet" href="{{asset('rebrand/css/media-queries.css')}}">

		<!--
		Google Font
		=========================== -->                    
		
		<!-- Oswald / Title Font -->
		<!-- <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'> -->
		<!-- Ubuntu / Body Font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('rebrand/css/style.css')}}">
		<!-- Modernizer Script for old Browsers -->		
        <script src="{{asset('rebrand/js/modernizr-2.6.2.min.js')}}"></script>
     <!--    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
     <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/> -->



    <?php if(config("app.env") == "production"): ?>
	<script src="{{ asset('js/chat/countly.js') }}"></script>
    <?php endif; ?>

	
    </head>
	
    

		 <body id="">
         <?php if(config("app.env") == "production"): ?>
         @include('layouts._analytics')
         <?php endif; ?>
			  	    
		       <!-- Fixed Navigation
		        ==================================== -->
		        <header id="navigation" class="navbar navbar-inverse">
		            <div class="container">
		                <div class="navbar-header">
		                    <!-- responsive nav button -->
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                        <span class="sr-only">Toggle navigation</span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                        <span class="icon-bar"></span>
		                    </button>
							<!-- /responsive nav button -->
							
							<!-- logo -->
		                    <a class="navbar-brand" href="{{url('/')}}">
								<h1 id="logo">
									<!-- <img src="" alt="Meghna" /> -->
									BMAC
								</h1>
							</a>
							<!-- /logo -->
		                </div>

					<!-- main nav -->
			                <nav class="collapse navbar-collapse navbar-right" role="Navigation">
			                    <ul id="nav" class="nav navbar-nav">
			                    	<li><a href="{{url('bmac/professional')}}">BMAC PRO NETWORK</a></li>
			                        <li><a href="{{url('bmac/accounting')}}">ACCOUNTING</a></li>
			                        <li><a href="{{url('secure/sign-in')}}">HR</a></li>
			                        <li><a href="{{url('secure/sign-in')}}">IMS</a></li>
			                        <li><a href="{{url('bmac/pricing')}}">PRICING</a></li>
			                         <li><a href="{{url('secure/sign-in')}}">SIGN-IN</a></li>

			                         <li><a href="{{url('secure/sign-up')}}">SIGN-UP</a></li>


			                        <li><a href="#"><button class="btn btn-block btn-green nav-button" data-toggle="modal" data-target="#myModal">Request demo</button></a></li>			                        
			                    </ul>
			                </nav>
							<!-- /main nav -->
						
		            </div>
		        </header>
		        <!--
		        End Fixed Navigation
		        ==================================== -->
 <section class="bmacsoftware  placeholder">

        	<div class="container">
				
				<h1 class="text-center price-title">BMAC Accounting</h1>

			
        	</div>
        	
        </section><!-- end section -->
			
		
			
		<section id="counter"><!-- The sign up display cols -->

			<div class="prof-two">
				

				<div class="container">

					<div class="col-md-4 col-sm-4 col-xs-12 text-center">
						<h3 class="myh">Business Owner</h3>
						<p class="myp">
							Gain control of your business and seamlessly increase profitability <br><br>
						</p>

						{{-- <a href="{{url('secure/sign-up')}}" class="btn btn-success btn-green">Sign Up</a> --}}
					</div>	

					<div class="col-md-4 col-sm-4 col-xs-12 text-center">
						<h3 class="myh">Accountant</h3>
						<p class="myp">
							Serve more clients from the comfort of your remote location once you have been collaborated 
						</p>
						{{-- <a href="{{url('secure/sign-up')}}" class="btn btn-success btn-green">Sign Up</a> --}}
					</div>	

					<div class="col-md-4 col-sm-4 col-xs-12 text-center ">
						<h3 class="myh">Influencer</h3>
						<p class="myp">
							Invest in the right business after taking a sneak peek with an invite from their BMAC account
						</p>
						{{-- <a href="{{url('secure/sign-up')}}" class="btn btn-success btn-green">Sign Up</a> --}}
					</div>	
					
					
				</div>
			</div>
						
		</section><!-- end of the sign up disaplay cols -->


		
		

		

		<section id="counter" >

			<div class="row">
				
					
					<h1 class="text-center myh">Download BMAC</h1>
					<h3 class="text-center myh">Collaborative accounting for small-medium businesses</h3>

					<p class="text-center myp">
						Your business on the GO! Download the BMAC Mobile app to record & monitor your business transactions.
					</p>


				<h1>&nbsp;</h1>

				
			</div>

			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-3">
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<a href=""><img src="{{asset('rebrand/img/android-app.png')}}" class="img-responsive pull-right"></a>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-3">
					<a href=""><img src="{{asset('rebrand/img/apple.png')}}" class="img-responsive"></a>
				</div>
				<div class="col-sm-3 col-md-3 col-xs-3">
					
				</div>
			</div>


			<div class="row">
				<h1>&nbsp;</h1>
				<h1 class="text-center myh">Newsletter</h1>
				<p class="text-center myp">Stay up to date with BMAC Subscribe to our newsletter today</p>
				
			</div>


			<div class="row">

				<div class="col-md-2"><h1>&nbsp;</h1></div>

				<div class="col-md-8">

					<div class="row">

						<form>
							<div class="col-md-10 padding-0">

								<input type="email" class="form-control formletter" name="">
							
								
							</div>
							<div class="col-md-2 padding-0">
									<input type="submit" value="Subscribe" name="" class="form-control subscribe">
							</div>
						</form>
						
					</div>
					
				</div>


				<div class="col-md-2">&nbsp;</div>
				
			</div>
			<div class="container">


				<div class="row rowmargin">
					
						<div class="col-md-3 footall">
							<h4>Company</h4>
							<center class="footpara">
								<a href="">About Us</a><br>
								<a href="">Contact Us</a><br>
								<a href="">Blog</a>
							</center>

						</div>

						<div class="col-md-3 footall">
							<h4>Resources</h4>
							<center class="footpara">
								<a href="">BMAC Video</a><br>
								<a href="">Manual</a>
							</center>
						
						</div>

						<div class="col-md-3 footall">
							<h4>Support</h4>
							<center class="footpara">
								<a href="">Feedback</a><br>
								<a href="">Support@ikooba.com</a>
							</center>
						</div>

						<div class="col-md-3 footall">
							<h4>Products</h4>
							<center class="footpara">
								<a href="">BMAC</a><br>
								<a href="">IMS</a><br>
								<a href="">HR & PAYROLL</a>
								<a href="">TOWNHALL</a>
							</center>
						</div>


					</div>



				<!--Social Icon -->

				<div class="col-md-4 col-sm-4 col-xs-4">
					
				</div>

				<div class="col-md-4 col-sm-4 col-xs-4 icon">
				
					<div class="col-md-2">
						<a href="https://www.facebook.com/ikoobaHQ/" title="facebook" target="_blank"><i class="fa fa-facebook fafoot"></i></a>
					</div>

					<div class="col-md-2">
						<a href="http://https://twitter.com/ikOObaHQ" title="twitter" target="_blank"><i class="fa fa-twitter fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.linkedin.com/company/27126860/" title="linkedin" target="_blank"><i class="fa fa-linkedin fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg" title="youtube"  target="_blank"><i class="fa fa-youtube fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.instagram.com/ikooba_hq" title="instagram" target="_blank"><i class="fa fa-instagram fafoot"></i></a>
					</div>

				</div>

				<div class="col-md-4 col-sm-4 col-xs-4">
					
				</div>
			</div><!-- end container -->
		</section><!-- end section -->

<!--  -->
	 <section class="footnote">
	 	 <div class="container text-justify ">
			<p class="text-left myh" style="margin-bottom:-3px"><strong>Why you need an Accounting Software?</strong></p>
			<p>
				“The dream is real but the hustle is sold separately”. 
				Are you wondering, “Do I really need accounting software?” Yes!  “Accounting is the language of a business” it is essential for entrepreneurs who are looking to increase profitability and grow their business.
			</p>
			<p>
				As a business owner, it’s easy to get caught up in your day-to-day operations and forget about keeping track of your business records. Getting an effective accounting software makes your work easier. With BMAC (Business, Measurement, Assurance and Collaboration) accounting software; you can complete your bookkeeping in few easy steps, increasing time to focus on running your business. It also fosters a better working relationship among businesses, employers, accountants and finance providers.
			</p>
			
			<p class="text-left" style="margin-bottom:-3px"><strong>With the BMAC you can;</strong></p>
			Record day-to-day transactions with little or no accounting knowledge. Effortlessly measure business performance with dashboard/analytics and reporting. Automated calculations and compiled statements. Effortlessly manage inventory and orders. Easily manage staff and payroll processes. 
			Seamlessly manage payment terms and deductions. Protects and keeps you professional and organized during audit. Protects and keeps you professional and organized during audit. Collaborate with employees (s), professional accountant(s) and investors to work with you. Make smart decisions to achieve business goals.  Monitor business performance anywhere, anytime.
			<!-- <ul>
				<li><i class="fa fa-check-circle"></i> Record day-to-day transactions with little or no accounting knowledge.</li>
				<li><i class="fa fa-check-circle"></i> Effortlessly measure business performance with dashboard/analytics and reporting.</li>
				<li><i class="fa fa-check-circle"></i> Automated calculations and compiled statements </li>
				<li><i class="fa fa-check-circle"></i> Effortlessly manage inventory and orders.</li>
				<li><i class="fa fa-check-circle"></i> Easily manage staff and payroll processes.</li>
				<li><i class="fa fa-check-circle"></i> Seamlessly manage payment terms and deductions.</li>
				<li><i class="fa fa-check-circle"></i> Protects and keeps you professional and organized during audit.</li>
				<li><i class="fa fa-check-circle"></i> Attract the support of investors and finance providers.</li>
				<li><i class="fa fa-check-circle"></i> Collaborate with employees (s), professional accountant(s) and investors to work with you.</li>
				<li><i class="fa fa-check-circle"></i> Make smart decisions to achieve business goals.</li>
				<li><i class="fa fa-check-circle"></i> Monitor business performance anywhere, anytime.</li>
			</ul> -->

	 	 </div>
	 </section>
		
		
		
		
		
	
		<footer  class="footclose">
			<div class="container text-center">
			    Powered by <a href="http://ikooba.com" target="_blank" >Ikooba Technologies</a> &nbsp;&copy; 2018 BMAC
			</div> <!-- end container -->
		</footer> <!-- end footer -->
		
		<!-- Back to Top
		============================== -->
		<a href="javascript:;" id="scrollUp">
			<i class="fa fa-angle-up fa-2x"></i>
		</a>




<!-- Modal -->
		@include('frontpagefolder.modal')
		<!-- /.modal -->
		
		
		<!-- Main jQuery -->
		<script src="{{asset('rebrand/js/jquery-1.11.0.min.js')}}"></script>

		



		<!-- Bootstrap 3.1 -->
		<script src="{{asset('rebrand/js/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('rebrand/slick/slick.min.js')}}"></script>
		<!-- Slitslider -->
		<script src="{{asset('rebrand/js/jquery.slitslider.js')}}"></script>
		<script src="{{asset('rebrand/js/jquery.ba-cond.min.js')}}"></script>
		<!-- Parallax -->
		<script src="{{asset('rebrand/js/jquery.parallax-1.1.3.js')}}"></script>
		<!-- Owl Carousel -->
		<script src="{{asset('rebrand/js/owl.carousel.min.js')}}"></script>
		<!-- Portfolio Filtering -->
		<script src="{{asset('rebrand/js/jquery.mixitup.min.js')}}"></script>
		<!-- Custom Scrollbar -->
		<script src="{{asset('rebrand/js/jquery.nicescroll.min.js')}}"></script>
		<!-- Jappear js -->
		<script src="{{asset('rebrand/js/jquery.appear.js')}}"></script>
		<!-- Pie Chart -->
		<script src="{{asset('rebrand/js/easyPieChart.js')}}"></script>
		<!-- jQuery Easing -->
		<script src="{{asset('rebrand/js/jquery.easing-1.3.pack.js')}}"></script>
		<!-- tweetie.min -->
		<script src="{{asset('rebrand/js/tweetie.min.js')}}"></script>
		<!-- Google Map API -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Highlight menu item -->
		<script src="{{asset('rebrand/js/jquery.nav.js')}}"></script>
		<!-- Sticky Nav -->
		<script src="{{asset('rebrand/js/jquery.sticky.js')}}"></script>
		<!-- Number Counter Script -->
		<script src="{{asset('rebrand/js/jquery.countTo.js')}}"></script>
		<!-- wow.min Script -->
		<script src="{{asset('rebrand/js/wow.min.js')}}"></script>
		<!-- For video responsive -->
		<script src="{{asset('rebrand/js/jquery.fitvids.js')}}"></script>
		<!-- Grid js -->
		<script src="{{asset('rebrand/js/grid.js')}}"></script>
		<!-- Custom js -->
		<script src="{{asset('rebrand/js/custom.js')}}"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				 
		 				 $("#owl-example").owlCarousel();
		
			});
		</script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				 
		 				 $("#example").owlCarousel();
		
			});
		</script>

         <?php if(config("app.env") == "production"): ?>
         <script src="{{ asset('js/chat/twak.js') }}"></script>
         <?php endif; ?>

    </body>
</html>