<!DOCTYPE html>

    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
        <title>BMAC Cloud Accounting</title>
		
		<!-- Mobile Specific Meta
		================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="keywords" content="Accounting for Nigerian SMEs,Online accounting software,Accounting software for small-medium businesses,Business solutions in Nigeria,Cloud accounting,What is a cloud accounting software,Accounting software,Accounting software for businesses,Organization that produces accounting software,Accounting software for Nigerians,How to gain control of your business,Business development solutions,How to grow your small business,Business growth,How to choose the best accounting software,Good accounting software solutions,Affordable accounting software,Accounting technology solution,Sage,Wave,Quickbooks,Fintech industry,How to solve your business problems,How to make sales in no time,how to grow your business fast,step by step business development process,How to do business and be more profitable,sales force,How to choose the best sales representative,How to choose a sales agent,How to hire a sales rep,How to develop sales and marketing strategy that works,how can sales and marketing work together,Difference between sales and marketing,Difference between profit and income,Profit,Income,Inventory,What is an inventory software,Buy online inventory software,How to manage my inventory,Online Inventory management software,Benefits of stock taking,Stocking taking interval,How to manage my warehouse,online HR-payroll software,Cloud staff-payroll manager,are inventories assets or liabilities,Meaning of inventory,Inventory app,Benefits of cloud accounting ,Do it yourself payroll software for free,How much does payroll software cost,Best payroll software online,How HR-Payroll software works,where to buy a payroll software,How to choose the best payroll software,why a payroll software is important,Do I need a payroll software?,Financial Reporting,Financial Strategizing,General Accounting,Income Tax Planning,Internal Controls/Technology,Investment Strategies,Management Accounting,Payroll Management,Personal Financial Planning,Profit and Loss Management,Revenue Forecasting,Sales Forecasting,Tax Schedules / Remittances,Venture Capital  Financing,
		Accounting Systems and Controls,Auditing,Budgeting,Capital Acquisitions,Cash Flow Analysis,Corporate Reporting,Cost Accounting,Credit / Debt Management,Estate Planning,Financial Compliance,Financial Management,Financial Planning,Financial Presentations,Cloud accounting,Inventory accounting,Inventory audit report,inventory management,Online inventory management software,Inventory turnover,Inventory balance sheet,Inventory control">	
		
		<!-- Favicon -->

		  <link rel="shortcut icon" href="{{asset('homepage/img/favicon.ico')}}" type="image/x-icon">

		<!------ Include the above in your HEAD tag ---------->

	
		
		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{{asset('rebrand/css/font-awesome.min.css')}}">
        
		<!-- bootstrap.min css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/bootstrap.min.css')}}">
		<!-- Animate.css -->
<!--         <link rel="stylesheet" href="css/animate.css"> -->
		<!-- Owl Carousel -->
        <link rel="stylesheet" href="{{asset('rebrand/css/owl.carousel.css')}}">
		<!-- Grid Component css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/component.css')}}">
		<!-- Slit Slider css -->
        <link rel="stylesheet" href="{{asset('rebrand/css/slit-slider.css')}}">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{asset('rebrand/css/main.css')}}">
         <link rel="stylesheet" href="{{asset('rebrand/css/JiSlider.css')}}">
		<!-- Media Queries -->
        <link rel="stylesheet" href="{{asset('rebrand/css/media-queries.css')}}">

		<!--
		Google Font
		=========================== -->                    
		
		<!-- Oswald / Title Font -->
		<!-- <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'> -->
		<!-- Ubuntu / Body Font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>
			<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('rebrand/css/style.css')}}">
		<!-- Modernizer Script for old Browsers -->		
        <script src="{{asset('rebrand/js/modernizr-2.6.2.min.js')}}"></script>
     <!--    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
     <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/> -->







    <?php if(config("app.env") == "production"): ?>
	<script src="{{ asset('js/chat/countly.js') }}"></script>
    <?php endif; ?>

	
    </head>
	
    <body id="body">

    <?php if(config("app.env") == "production"): ?>
    @include('layouts._analytics')
    <?php endif; ?>

	    <!--
	    Start Preloader
	    ==================================== -->
		
      {{--  <div id="loading-mask">
        	<div class="loading-img">
        		<img alt="oneBMAC" style="margin-right:-2000000px"  />
        	</div>
        </div>  --}}
        <!--End Preloader
        ==================================== -->
		
        <!--
        Welcome Slider
        ==================================== -->
		<section id="home">	 

			        <!-- 
			        Fixed Navigation
			        ==================================== -->
			        <header id="navigation" class="navbar navbar-inverse">
			            <div class="container">
			                <div class="navbar-header">
			                    <!-- responsive nav button -->
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			                        <span class="sr-only">Toggle navigation</span>
			                        <span class="icon-bar"></span>
			                        <span class="icon-bar"></span>
			                        <span class="icon-bar"></span>
			                    </button>
								<!-- /responsive nav button -->
								
								<!-- logo -->
			                    <a class="navbar-brand" href="{{url('/')}}">
									<h1 id="logo">
										<!-- <img src="" alt="Meghna" /> -->
										BMAC
									</h1>
								</a>
								<!-- /logo -->
			                </div>

							<!-- main nav -->
			                <nav class="collapse navbar-collapse navbar-right" role="Navigation">
			                    <ul id="nav" class="nav navbar-nav">
			                  		<li><a href="{{url('bmac/professional')}}">BMAC PRO NETWORK</a></li>
			                        <li><a href="{{url('bmac/accounting')}}">ACCOUNTING</a></li>
			                        <li><a href="{{url('secure/sign-in')}}">HR</a></li>
			                        <li><a href="{{url('secure/sign-in')}}">IMS</a></li>
			                        <li><a href="{{url('bmac/pricing')}}">PRICING</a></li>
			                         <li><a href="{{url('secure/sign-in')}}">SIGN-IN</a></li>

			                         <li><a href="{{url('secure/sign-up')}}">SIGN-UP</a></li>
			                        <li><a href="#"><button class="btn btn-block btn-green nav-button" data-toggle="modal" data-target="#myModal">Request demo</button></a></li>
			                        
			                    </ul>
			                </nav>
							<!-- /main nav -->
							
			            </div>
			        </header>
			        <!--
			        End Fixed Navigation
			        ==================================== -->

		</section>
		<!--/#home section-->


		<section>

				<!-- banner slider-->
				<div class="banner-silder">
					<div id="JiSlider" class="jislider">
						<ul>
							<li>
								<div class="w3layouts-banner-top">
									<div class="bs-slider-overlay">
										<div class="container">
											<!-- Slide Text Layer -->
											<div class="w3l-slide-text text-center">
												
												<h1 class="text-uppercase pt-4 pb-3">Business, Measurement, Assurance and Collaboration</h1>
												<h4 class="heading_bottom slideh4 mb-4">“The dream is real but the hustle is sold separately”</h4>
												<div style="height:20px">&nbsp;</div>
												
												
											</div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="w3layouts-banner-top w3layouts-banner-top1">
									<div class="bs-slider-overlay">
										<div class="container">
											<!-- Slide Text Layer -->
											<div class="w3l-slide-text text-center">
											
												<h2 class="text-uppercase pt-4 pb-3 slideh4">BMAC ACCOUNTING</h2>
												<h4 class="heading_bottom slideh4  mb-4">Get real-time insights into your business performance, make smarter decisions</h4>
												<h4 class="animated fadeInUp get">Get 2-users free</h4>
												<div style="height:20px">&nbsp;</div>
												<a href="{{url('secure/sign-up')}}" class="button-style">Sign up</a>
												
											</div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="w3layouts-banner-top w3layouts-banner-top2">
									<div class="bs-slider-overlay">
										<div class="container">
											<!-- Slide Text Layer -->
											<div class="w3l-slide-text text-center">
											
												<h2 class="text-uppercase pt-4 pb-3 slideh4">BMAC IMS</h2>
												<h4 class="heading_bottom slideh4  mb-4">Accurate, up-to-date information about your inventory</h4>
												<h4 class="animated fadeInUp get">Start your 30-day free trial</h4>
												<div style="height:20px">&nbsp;</div>
												<a href="{{url('secure/sign-up')}}" class="button-style">Sign up</a>
											</div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="w3layouts-banner-top w3layouts-banner-top3">
									<div class="bs-slider-overlay">
										<div class="container">
											<!-- Slide Text Layer -->
											<div class="w3l-slide-text text-center">
												<h2 class="text-uppercase pt-4 pb-3 slideh4">BMAC HR</h2>
												<h4 class="heading_bottom slideh4  mb-4">Simplify the way you manage and pay your staff</h4>
												<h4 class="animated fadeInUp get">Start your 30-day free trial</h4>
												<div style="height:20px">&nbsp;</div>
												<a href="{{url('secure/sign-up')}}" class="button-style">Sign up</a>
											</div>
										</div>
									</div>
								</div>
							</li>
							
						</ul>
					</div>
				</div>
				<!-- //banner slider -->
	
			
		</section>
		
       

        <!-- BMAC software section -->
        <section class="bmacsoftware">

        	<div class="container">
				<h1>&nbsp;</h1>
				<h1 class="text-center myh">BMAC is an ALL-IN-ONE Business Solution</h1>

				<p class="text-center myp">
					No back and forth! BMAC is all you need to manage your business operations, track your inventory, manage your employee payroll, seamlessly collaborate with accountants, finance institutions and investors from anywhere around the world. 
				</p>


			<h1>&nbsp;</h1>
        	</div>
        	
        </section><!-- end section -->
	
		<!--

	Bmac Icon section
		==================================== -->
		
		<section id="counter">

			<div class="container">

				<div class="row">
				
					<!-- first count item -->
					<div class="col-md-4 col-sm-4 col-xs-4 text-center">
						<h1>&nbsp;</h1>
						<h1>&nbsp;</h1>
						<img src="{{asset('rebrand/img/handshake.png')}}" class="img-responsive imgone">
						 <div class="collabo"> Collaborator tool</div>
						<h1>&nbsp;</h1>
						
					</div>
					<!-- end first count item -->
				
					<!-- second count item -->
					<div class="col-md-4 col-sm-4 col-xs-4 text-center ">
						
						<img src="{{asset('rebrand/img/meeting.png')}}" class="img-responsive imgtwo center-block">
						<div class="hrdiv">Human resource management</div>
					</div>
					<!-- end second count item -->
				
					<!-- third count item -->
					<div class="col-md-4 col-sm-4 col-xs-4 text-center colnote">
						
					 
						<img src="{{asset('rebrand/img/notebook.png')}}" class="img-responsive imgthree pull-right">
						<div class="clearfix"></div>
						<div class="notebook">Notebook</div>	
						
					</div>

				</div> 		<!-- end row -->


				<div class="row">

						<div class="col-md-2 col-sm-2 col-xs-2 ">
							
							
							<img src="{{asset('rebrand/img/cloud.png')}}" class="img-responsive imgfive ">
							<div class="cloud">Cloud based</div>
							
						</div>
						<!-- end first count item -->
						
						<!-- second count item -->
						<div class="col-md-8 col-sm-8 col-xs-8 text-center">
							
							<img src="{{asset('rebrand/img/computer1.png')}}" class="img-responsive imgcomp">
						
						</div>
						<!-- end second count item -->
						
						<!-- third count item -->
						<div class="col-md-2 col-sm-2 col-xs-2 text-center">
							
						 
							<img src="{{asset('rebrand/img/group28.png')}}" class="img-responsive imgfour  pull-right">
							<div class="clearfix"></div>
							<div class="hrdiv">Pocket friendly professional assistance</div>
							
						</div>
					
				</div>
				<!-- end of row -->

				<div class="row row2">

						<div class="col-md-4 col-sm-4 col-xs-4 text-center" >
							
							
							<img src="{{asset('rebrand/img/chart.png')}}" class="img-responsive imgchart pull-left ">
							<div class="clearfix"></div>
							<div class="collabo">Analytic dashboard</div>
							
						</div>
						<!-- end first count item -->
						
						<!-- second count item -->
						<div class="col-md-4 col-sm-4 col-xs-4 text-center ">
							
							<img src="{{asset('rebrand/img/house.png')}}" class="img-responsive imghouse center-block">
							<div class="hrdiv">Inventory management system</div>
						</div>
						<!-- end second count item -->
						
						<!-- third count item -->
						<div class="col-md-4 col-sm-4 col-xs-4 text-center">
							
						 
							<img src="{{asset('rebrand/img/connect.png')}}" class="img-responsive imgconnect  pull-right">
							<div class="clearfix"></div>
							<div class="town">Townhall community</div>
								
							
						</div>
					
				</div>
			


				
			</div>
			
		</section>

	

		<section class="listbmac">
			<div class="container">
				<div class="row" style="padding-top:50px">
					<div class="col-md-3 col-sm-3 col-xs-12">
						
					</div>

					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="{{asset('rebrand/img/cloudicon.png')}}" class="img-responsive pull-left icon-margin">
						<h3 class="myh">&nbsp;Cloud</h3>
						<div class="clearfix"></div>
						<p>Control & access your information on any device anytime, anywhere. </p>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<img src="{{asset('rebrand/img/open-book.png')}}" class="img-responsive pull-left icon-margin">
						<h3 class="myh">&nbsp;Notebook</h3>
						<div class="clearfix"></div>
						<p> safe and secured online logbook for recording daily business transactions.</p>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 ">
						
					</div>
				</div>

					<h1>&nbsp;</h1>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<img src="{{asset('rebrand/img/hand-shake.png')}}" class="img-responsive pull-left icon-margin">
						<h3 class="myh">&nbsp;Collaborator</h3>
						<div class="clearfix"></div>
						<p>invite your employee (s), accountant(s) and investor(s) to work with you.</p>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<img src="{{asset('rebrand/img/dashboard.png')}}" class="img-responsive pull-left icon-margin">
						<h3 class="myh">&nbsp;Analytics</h3>
						<div class="clearfix"></div>
						<p>View your business performance at a glance.</p>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12">
					<img src="{{asset('rebrand/img/usersicon.png')}}" class="img-responsive pull-left icon-margin">
						<h3 class="myh">Townhall</h3>
						<div class="clearfix"></div>
						<p>Join our Online Community to connect with businesses, investors and professionals</p>
					</div>
					<h1>&nbsp;</h1>
					
				</div>
			</div>

		</section>

<!-- 		<section class="featured">
	

	feature row
	<div class="container">
		
		<div class="col-md-2 col-sm-3 col-xs-1"><img src="img/tribune.png" class="img-responsive"></div>
		<div class="col-md-2 col-sm-3 col-xs-1"><img src="img/techcabal-logo.png" class="img-responsive"></div>
		<div class="col-md-2 col-sm-3 col-xs-1"><img src="img/business-day-logo.jpg" class="img-responsive"></div>
		<div class="col-md-2 col-sm-3 col-xs-1"><img src="img/distrupt.png" class="img-responsive"></div>
		<div class="col-md-2 col-sm-3 col-xs-1"><h1>&nbsp;</h1></div>
	</div>
		
	end of featured row
</section> -->




		<section id="counter">
			<div class="container">
							
				<div class="row">
					
						
						<h1 class="text-center myh">Download BMAC</h1>
						<h3 class="text-center myh">Collaborative accounting for small-medium business</h3>

						<p class="text-center myp">
							Your business on the GO! Download the BMAC Mobile app to record & monitor your business transactions.
						</p>


					<h1>&nbsp;</h1>

					
				</div>

				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<a href=""><img src="{{asset('rebrand/img/android-app.png')}}" class="img-responsive pull-right"></a>
					</div>
					<div class="col-sm-3 col-md-3 col-xs-3">
						<a href=""><img src="{{asset('rebrand/img/apple.png')}}" class="img-responsive"></a>
					</div>
					<div class="col-sm-3 col-md-3 col-xs-3">
						
					</div>
				</div>


				<div class="row">
					<h1>&nbsp;</h1>
					<h1 class="text-center myh">Newsletter</h1>
					<p class="text-center myp">Stay up to date with BMAC Subscribe to our newsletter today</p>
					
				</div>


				<div class="row">

					<div class="col-md-2"><h1>&nbsp;</h1></div>

					<div class="col-md-8">

						<div class="row">

							<form>
								<div class="col-md-10 padding-0">

									<input type="email" class="form-control formletter" name="">
								
									
								</div>
								<div class="col-md-2 padding-0">
										<input type="submit" value="Subscribe" name="" class="form-control subscribe">
								</div>
							</form>
							
						</div>
						
					</div>


					<div class="col-md-2">&nbsp;</div>
					
				</div>

				<div class="row rowmargin">
					
					<div class="col-md-3 footall">
						<h4>Company</h4>
						<center class="footpara">
							<a href="">About Us</a><br>
							<a href="">Contact Us</a><br>
							<a href="">Blog</a>
						</center>

					</div>

					<div class="col-md-3 footall">
						<h4>Resources</h4>
						<center class="footpara">
							<a href="">BMAC Video</a><br>
							<a href="">Manual</a>
						</center>
					
					</div>

					<div class="col-md-3 footall">
						<h4>Support</h4>
						<center class="footpara">
							<a href="">Feedback</a><br>
							<a href="">Support@ikooba.com</a>
						</center>
					</div>

					<div class="col-md-3 footall">
						<h4>Products</h4>
						<center class="footpara">
							<a href="">BMAC</a><br>
							<a href="">IMS</a><br>
							<a href="">HR & PAYROLL</a>
							<a href="">TOWNHALL</a>
						</center>
					</div>


				</div>


				<!--Social Icon -->

				<div class="col-md-4 col-sm-4 col-xs-4">
					
				</div>

				<div class="col-md-4 col-sm-4 col-xs-4 icon">
				
					<div class="col-md-2">
						<a href="https://www.facebook.com/ikoobaHQ/" title="facebook" target="_blank"><i class="fa fa-facebook fafoot"></i></a>
					</div>

					<div class="col-md-2">
						<a href="http://https://twitter.com/ikOObaHQ" title="twitter" target="_blank"><i class="fa fa-twitter fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.linkedin.com/company/27126860/" title="linkedin" target="_blank"><i class="fa fa-linkedin fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg" title="youtube"  target="_blank"><i class="fa fa-youtube fafoot"></i></a>
					</div>
					<div class="col-md-2">
						<a href="https://www.instagram.com/ikooba_hq" title="instagram" target="_blank"><i class="fa fa-instagram fafoot"></i></a>
					</div>

				</div>

				<div class="col-md-4 col-sm-4 col-xs-4">
					
				</div>
			</div><!-- end container -->
		</section><!-- end section -->

<!--  -->
	 <section class="footnote">
	 	 <div class="container text-justify ">
			<p class="text-left myh" style="margin-bottom:-3px"><strong>Why you need an Accounting Software?</strong></p>
			<p>
				“The dream is real but the hustle is sold separately”. 
				Are you wondering, “Do I really need accounting software?” Yes!  “Accounting is the language of a business” it is essential for entrepreneurs who are looking to increase profitability and grow their business.
			</p>
			<p>
				As a business owner, it’s easy to get caught up in your day-to-day operations and forget about keeping track of your business records. Getting an effective accounting software makes your work easier. With BMAC (Business, Measurement, Assurance and Collaboration) accounting software; you can complete your bookkeeping in few easy steps, increasing time to focus on running your business. It also fosters a better working relationship among businesses, employers, accountants and finance providers.
			</p>
			
			<p class="text-left" style="margin-bottom:-3px"><strong>With the BMAC you can;</strong></p>
			Record day-to-day transactions with little or no accounting knowledge. Effortlessly measure business performance with dashboard/analytics and reporting. Automated calculations and compiled statements. Effortlessly manage inventory and orders. Easily manage staff and payroll processes. 
			Seamlessly manage payment terms and deductions. Protects and keeps you professional and organized during audit. Protects and keeps you professional and organized during audit. Collaborate with employees (s), professional accountant(s) and investors to work with you. Make smart decisions to achieve business goals.  Monitor business performance anywhere, anytime.

			

	 	 </div>
	 </section>
		
		
		
		
		
		
	
		<footer  class="footclose">
			<div class="container text-center">
			    Powered by <a href="http://ikooba.com" target="_blank" >Ikooba Technologies</a> &nbsp;&copy; 2018 BMAC
			</div> <!-- end container -->
		</footer> <!-- end footer -->
		
		<!-- Back to Top
		============================== -->
		<a href="javascript:;" id="scrollUp">
			<i class="fa fa-angle-up fa-2x"></i>
		</a>
		<!-- Modal -->
       @include('frontpagefolder.modal')
<!-- /.modal -->
		<!-- end Footer Area
		========================================== -->
		
		<!-- 
		Essential Scripts
		=====================================-->






		
		<!-- Main jQuery -->
		<script src="{{asset('rebrand/js/jquery-1.11.0.min.js')}}"></script>

		
		
	<!-- Banner Slider js script file-->
	<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
		<script src="{{asset('js/JiSlider.js')}}"></script>
		<script>
			$(window).load(function () {
				$('#JiSlider').JiSlider({
					color: '#fff',
					start: 1,
					reverse: false
				}).addClass('ff')
			})
		</script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-36251023-1']);
			_gaq.push(['_setDomainName', 'jqueryscript.net']);
			_gaq.push(['_trackPageview']);

			(function () {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>

	<!-- /Banner Slider js script file-->


		<!-- Bootstrap 3.1 -->
		<script src="{{asset('rebrand/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('rebrand/slick/slick.min.js')}}"></script>
		<!-- Slitslider -->
		<script src="{{asset('rebrand/js/jquery.slitslider.js')}}"></script>
		<script src="{{asset('rebrand/js/jquery.ba-cond.min.js')}}"></script>
		<!-- Parallax -->
		<script src="{{asset('rebrand/js/jquery.parallax-1.1.3.js')}}"></script>
		<!-- Owl Carousel -->
		<script src="{{asset('rebrand/js/owl.carousel.min.js')}}"></script>
		<!-- Portfolio Filtering -->
		<script src="{{asset('rebrand/js/jquery.mixitup.min.js')}}"></script>
		<!-- Custom Scrollbar -->
		<script src="{{asset('rebrand/js/jquery.nicescroll.min.js')}}"></script>
		<!-- Jappear js -->
		<script src="{{asset('rebrand/js/jquery.appear.js')}}"></script>
		<!-- Pie Chart -->
		<script src="{{asset('rebrand/js/easyPieChart.js')}}"></script>
		<!-- jQuery Easing -->
		<script src="{{asset('rebrand/js/jquery.easing-1.3.pack.js')}}"></script>
		<!-- tweetie.min -->
		<script src="{{asset('rebrand/js/tweetie.min.js')}}"></script>
		<!-- Google Map API -->
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		<!-- Highlight menu item -->
		<script src="{{asset('rebrand/js/jquery.nav.js')}}"></script>
		<!-- Sticky Nav -->
		<script src="{{asset('rebrand/js/jquery.sticky.js')}}"></script>
		<!-- Number Counter Script -->
		<script src="{{asset('rebrand/js/jquery.countTo.js')}}"></script>
		<!-- wow.min Script -->
		<script src="{{asset('rebrand/js/wow.min.js')}}"></script>
		<!-- For video responsive -->
		<script src="{{asset('rebrand/js/jquery.fitvids.js')}}"></script>
		<!-- Grid js -->
		<script src="{{asset('rebrand/js/grid.js')}}"></script>
		<!-- Custom js -->
		<script src="{{asset('rebrand/js/custom.js')}}"></script>

    <?php if(config("app.env") == "production"): ?>
    <script src="{{ asset('js/chat/twak.js') }}"></script>
    <?php endif; ?>

    </body>
</html>