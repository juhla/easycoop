<?php
$user = auth()->user();
//dd($user->hasRole(['business-owner', 'basic-business-owner', 'advanced-business-owner']));
if ($user->hasRole(['business-owner', 'basic-business-owner', 'advanced-business-owner'])) { ?>
@include('layouts.sidebar.business-owner')
<?php
} else if ($user->hasRole(['employee'])) {
?>
@include('layouts.sidebar.employee')
<?php
} else if ($user->hasRole(['professional', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
?>
@include('layouts.sidebar.professional')
<?php
} else if ($user->hasRole(['finance-provider'])) { ?>
@include('layouts.sidebar.finance-provider')
<?php
} else if ($user->hasRole(['trade-promoter'])) { ?>
@include('layouts.sidebar.trade-promoter')
<?php
}

?>