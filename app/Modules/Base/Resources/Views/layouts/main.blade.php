<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8"/>
    <title>BMAC | Business Collaboration</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css"/>

    @section('css')
    @show
    <link rel="stylesheet" href="{{ asset('js/datepicker/datepicker.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('js/bootstrap-sweetalert/sweet-alert.min09a2.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('js/calendar/bootstrap_calendar.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('js/jvectormap/jquery-jvectormap-1.2.2.css') }}" type="text/css"/>
    @section('stylesheetz')
    @show
    <link rel="stylesheet" href="{{ asset('js/select2/select2.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('js/select2/theme.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/app.v1.css') }}" type="text/css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-tour.min.css') }}">

    <!-- Added by Victor -->
    <link rel="stylesheet" href="{{ asset('css/touch_new.css') }}" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
    <!-- Added by Festus -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"/>

    <!-- Added by Victor -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!--[if lt IE 9]>
    <script src="{{ asset('js/ie/html5shiv.js') }}"></script>
    <script src="{{ asset('js/ie/respond.min.js') }}"></script>
    <script src="{{ asset('js/ie/excanvas.js') }}"></script>
    <![endif]-->
    <style type="text/css">
        .required, .pfaviconarsley-error-list {
            color: red;
        }

        #feedback a {
            display: block;
            position: fixed;
            top: 300px;
            right: -1px;
        }

        .sweet-overlay {
            z-index: 10 !important;
            /*display: flex;*/
            /*background-color: rgba(20, 21, 20, 0.87)!important;*/
        }

        .popover-navigation [data-role=end]:hover .sweet-overlay {
            display: none !important;
        }

        .popover-title {
            color: white !important;
            background-color: #033e04 !important;
            font-weight: bold !important;
        }

        .popover-navigation [data-role=next] {
            cursor: pointer;
            color: white !important;
            background-color: #033e04 !important;
            font-weight: bold !important;
        }

        .popover-navigation [data-role=next].disabled {
            cursor: default;
            color: #717171 !important;
            background-color: #fafafa !important;
        }

        .vbox > footer {
            z-index: 1 !important;
        }
    </style>


    <?php if (config("app.env") == "production") : ?>
<!-- Hotjar Tracking Code for sme.onebmac.com -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 1559482, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>


    <?php endif; ?>


</head>
<body class="">
@include('layouts.partials.notifications')
<?php if(config("app.env") == "production"): ?>
@include('layouts._analytics')
<?php endif; ?>

<input type="hidden" id="baseurl" value="{{ url('/') }}"/>
<section class="vbox">
    <header class="bg-primary dk header navbar navbar-fixed-top-xs">
        @include('layouts._header')
    </header>
    <section class="pos-static">
        <section class="hbox stretch">
            <!-- .aside -->
            <aside class="bg-light lter {{ (isset($min_side)) ? 'nav-xs' : 'b-r' }} aside-md hidden-print hidden-xs"
                   id="nav">
                @include('layouts._menu')
            </aside> <!-- /.aside -->
            
            <section id="content" class=".bmac main-body">
                @yield('content')
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open"
                   data-target="#nav,html"></a>
            </section>
            <aside class="bg-light lter b-l aside-md hide" id="notes">
                <div class="wrapper">Notification</div>
            </aside>
        </section>
    </section>
    <div id="feedback"><a href="#" id="open-feedback"><img src="{{ asset('images/feedback.jpg') }}"/></a></div>
    @include('feedback-form')
</section>
<!-- Bootstrap -->
<!-- Used by Tour guide-->
@section('scriptsz')
@show
<script src="{{ asset('js/jquery.min.js')}}"></script>

<script src="{{ asset('js/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/bootstrap-tour.min.js')}}"></script>

<script type="text/javascript">
    // Instance the tour
    var tour = new Tour();
    var steps = [
        {
            element: "#tour1", // string (jQuery selector) - html element next to which the step popover should be shown
            title: "Notebook for non- accountant ", // string - title of the popover
            content: "This is used to record daily transactions by a user with little or no accounting knowledge through the cash and non-cash tabs." // string - content of the popover
        },
        {
            element: "#tour2",
            title: "Step 1 - Business Setup",
            content: "As a User with accounting knowledge, the first thing is to set up your Fiscal year, Charts of Accounts, Customers &Vendors."
        },
        {
            element: "#tour3",
            title: "Step 2 - BMAC Inflow Module",
            content: "This module is used to post daily inflows of money into your Business."
        },
        {
            element: "#tour4",
            title: "Step 3 - BMAC Outflow Module",
            content: "This module is used to post daily spending of money into your Business."
        },
        {
            element: "#tour5",
            title: "Step 4 - BMAC Invoice Module",
            content: "This module is used to post daily transaction relating to credit sales indicating products, quantities and agreed price"
        },
        {
            element: "#tour6",
            title: "Step 5 - BMAC Journal Module",
            content: "This module is used to post non-cash transactions. E.g Depreciation, credit sales & purchases."
        }
    ];
    
    // Add your steps. Not too many, you don't really want to get your users sleepy
    tour.addSteps(steps);

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.start();
    const mobileBreakpoint = window.matchMedia("(min-width: 768px )");
    // const mobileBreakpointmin = window.matchMedia("(min-width: 991.1px )");

    let toggle = document.querySelector('#togglerIcon');
    let body = document.querySelector('body');
    toggle.addEventListener('click', toggleFunction);

    function toggleFunction(e) {
        if(mobileBreakpoint.matches){
            body.classList.toggle('nav-split');
        }
    }
    

</script>
<!-- App -->
<script src="{{ asset('js/app.v1.js') }}"></script>
<script src="{{ asset('js/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<script src="{{ asset('js/parsley/parsley.min.js') }}"></script>

<script src="{{ asset('js/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('js/select2/select2.min.js') }}"></script>
<script src="{{ asset('js/app.plugin.js') }}"></script>
<script src="{{ asset('js/scripts/functions.js') }}"></script>
<script src="{{ asset('js/bootbox.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxcore.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxchart.core.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxdraw.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxgauge.js') }}"></script>

{{--<script type="text/javascript" src="{{ asset('js/jqwidgets/jqxtooltip.js') }}"></script>--}}
<div class="tile" onclick="myFunction(this)"></div>
<script type="text/javascript">
    $(".popover-navigation [data-role=end]").click(function () {
        $(".sweet-overlay").css('display', 'none');
        $(".sweet-overlay").css("background", "#ffffff");
    });

    $('div.alert').delay(10000).slideUp(300);
    $('.select2').select2({});
    $('.selectpicker').selectpicker({
        size: 4
    });
    $('#open-feedback').click(function () {
        $('#feedbackform').modal('show');
    });

    $('.close-feedback-modal').click(function () {
        $('#feedbackForm')[0].reset();
        $('#feedbackform').modal('hide');
    });
</script>


<?php if(config("app.env") == "production"): ?>
<script src="{{ asset('js/chat/twak.js') }}"></script>
<?php endif; ?>

@section('scripts_dt')
@show

@section('scripts')
@show


</body>
</html>
