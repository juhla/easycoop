<section class="vbox">
    <section class="w-f scrollable">
            <!-- nav -->
            @if(session()->has('company_db') )
                <nav class="nav-primary hidden-xs">
                    <ul class="nav">
                        <li class="{{ set_active('workspace') }}">
                            <a href="{{ url('/workspace') }}">
                                <i class="fa fa-dashboard icon"><b class="bg-info"></b> </i>
                                <span>Work tools</span>
                            </a>
                        </li>

                        @if(auth()->user()->hasRole(array('business-owner','basic-business-owner','advanced-business-owner')))
                            <li class="{{ set_active('notebook-v2/*') }}">
                                <a href="{{ url('/notebook-v2') }}">
                                    <i class="fa fa-book icon"> <b class="bg-success"></b> </i>
                                    <span>Notebook</span>
                                </a>
                            </li>
                        @endif


                        <li class="{{ set_active('transactions/inflow') }}">
                            <a href="{{ url('transactions/inflow') }}">
                                <i class="fa fa-share icon"> <b class="bg-success dker"></b> </i>
                                <span id="tour3">Receipts</span>
                            </a>
                        </li>

                        <li class="{{ set_active('transactions/outflow') }}">
                            <a href="{{ url('transactions/outflow') }}">
                                <i class="fa fa-reply icon"> <b class="bg-danger dker"></b> </i>
                                <span id="tour4">Payments</span>
                            </a>
                        </li>

                        <li class="{{ set_active('invoices/*') }}">
                            <a href="{{ url('invoices/sales-invoice') }}">
                                <i class="fa fa-list-alt icon"> <b class="bg-warning dker"></b> </i>
                                <span id="tour5">Invoices</span>
                            </a>
                        </li>


                        <li class="{{ set_active('journal-entry/*') }}">
                            <a href="{{ url('journal-entry') }}">
                                <i class="fa fa-file-text-o icon"> <b class="bg-danger dker"></b> </i>
                                <span id="tour6">Journal Entry</span>
                            </a>
                        </li>


                        {{--<li class="{{ set_active('batch_posting/*') }}">--}}
                            {{--<a href="{{ url('batch_posting') }}">--}}
                                {{--<i class="fa fa-file-text-o icon"> <b class="bg-danger dker"></b> </i>--}}
                                {{--<span id="tour6">Batch Posting</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        <li class="{{ set_active('reports/*') }}">
                            <a href="{{ url('reports') }}">
                                <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                                <span>Quick View</span>
                            </a>
                        </li>


                        <li class="{{ set_active('setup/*') }} ">
                            <a href="#setup" class="{{ set_active('notebook/*') }} ">
                                <i class="fa fa-cogs icon"> <b class="bg-primary"></b> </i>
                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i>
                                            </span>
                                <span id="tour2">Business Setup</span>
                            </a>

                            <ul class="nav lt">
                                <li class="{{ set_active('setup/accounts') }} ">
                                    <a href="{{ url('setup/accounts') }}"
                                       class="{{ set_active('setup/accounts') }}">
                                        <i class="fa fa-angle-right"></i> <span>Chart of Accounts</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/fiscal-years') }} ">
                                    <a href="{{ url('setup/fiscal-years') }}"
                                       class="{{ set_active('setup/fiscal-years') }}">
                                        <i class="fa fa-angle-right"></i> <span>Fiscal Years</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/customers') }} ">
                                    <a href="{{ url('setup/customers') }}"
                                       class="{{ set_active('setup/customers') }}">
                                        <i class="fa fa-angle-right"></i> <span>Customers</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/vendors') }} ">
                                    <a href="{{ url('setup/vendors') }}" class="{{ set_active('setup/vendors') }}">
                                        <i class="fa fa-angle-right"></i> <span>Vendors</span>
                                    </a>
                                </li>
                                {{--   <li class="{{ set_active('setup/Products') }} ">
                                      <a href="{{ url('setup/products') }}"
                                         class="{{ set_active('setup/Products') }}">
                                          <i class="fa fa-angle-right"></i> <span>Products</span>
                                      </a>
                                  </li> --}}
                                <li class="{{ set_active('setup/taxes') }} ">
                                    <a href="{{ url('setup/taxes') }}" class="{{ set_active('setup/taxes') }}">
                                        <i class="fa fa-angle-right"></i> <span>Taxes</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/bank-accounts') }} ">
                                    <a href="{{ url('setup/bank-accounts') }}"
                                       class="{{ set_active('setup/bank-accounts') }}">
                                        <i class="fa fa-angle-right"></i> <span>Bank Accounts</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/payment-terms') }} ">
                                    <a href="{{ url('setup/payment-terms') }}"
                                       class="{{ set_active('setup/payment-terms') }}">
                                        <i class="fa fa-angle-right"></i> <span>Payment Terms</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>

        @endif
        <!-- / nav -->
    </section>
    <footer class="footer lt hidden-xs b-t b-light">
        <a href="javascript:void(0)" class="" id="togglerIcon">
        <span class="toggle"></span>
        <span class="toggle"></span>
        <span class="toggle"></span>
        </a>
    </footer>
</section>