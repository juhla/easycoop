<section class="vbox">
    <header class="header bg-primary lter text-center clearfix">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i
                        class="fa fa-plus"></i></button>
            <div class="btn-group hidden-nav-xs">
                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                    Switch Business
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left">
                    @if(auth()->user()->hasRole(array('agent-finance-provider')))
                    @foreach(getAgentBusinesses() as $list)
                        <li>
                            <a href="{{ url('dashboard/change-company?slug='.$list->database) }}">{{ $list->company_name }}</a>
                        </li>
                    @endforeach
                    @else
                        @foreach(getCollaboratorCompanies() as $list)
                            <li>
                                <a href="{{ url('dashboard/change-company?slug='.$list->company->database) }}">{{ $list->company->company_name }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </header>
    <section class="w-f scrollable">
            <nav class="nav-primary hidden-xs">
                <ul class="nav">

                    @if(session()->has('company_db') )

                        <li class="{{ set_active('dashboard') }}">
                            <a href="{{ url('/workspace') }}">
                                <i class="fa fa-dashboard icon"><b class="bg-info"></b> </i>
                                <span> View Work tools</span>
                            </a>
                        </li>

                        <li class="{{ set_active('reports/*') }}">
                            <a href="{{ url('reports') }}">
                                <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                                <span> View Reports</span>
                            </a>
                        </li>


                    @endif
                    @if(!auth()->user()->hasRole(array('agent-finance-provider')))
                        <li class="{{ set_active('setup/agent/*') }} ">
                            <a href="#agents" class="{{ set_active('setup/agent/*') }} ">
                                <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                                <span> Agent Management</span>
                            </a>
                            <ul class="nav lt">
                                <li class="{{ set_active('setup/agents') }} ">
                                    <a href="{{ url('setup/agents') }}"
                                       class="{{ set_active('setup/agents') }}">
                                        <i class="fa fa-angle-right"></i> <span>List Agents</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/agents/map_agents') }} ">
                                    <a href="{{ url('setup/agents/map_agents') }}"
                                       class="{{ set_active('setup/agents/map_agents') }}">
                                        <i class="fa fa-angle-right"></i> <span>Assign SMEs to Agents</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/agents/agentSmes') }} ">
                                    <a href="{{ url('setup/agents/agentSmes') }}"
                                       class="{{ set_active('setup/agents/agentSmes') }}">
                                        <i class="fa fa-angle-right"></i> <span>  Agent's SMEs</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ url('/logoutCompany') }}">
                                <i class="fa fa-home icon"><b class="bg-warning"></b> </i>
                                <span>My Home {{session()->get('company_db')}}</span>
                            </a>
                        </li>
                </ul>
            </nav>

            @endif
            {{--<nav class="nav-primary hidden-xs">--}}
            {{--<ul class="nav">--}}
            {{--<li class="{{ set_active('collaborators/*') }}">--}}
            {{--<a href="{{ url('collaborators/businesses') }}">--}}
            {{--<i class="fa fa-group icon"><b class="bg-warning"></b> </i>--}}
            {{--<span>List Businesses</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</nav>--}}
    </section>
    <footer class="footer lt hidden-xs b-t b-light">
        <a href="javascript:void(0)" class="" id="togglerIcon">
        <span class="toggle"></span>
        <span class="toggle"></span>
        <span class="toggle"></span>
        </a>
    </footer>
</section>