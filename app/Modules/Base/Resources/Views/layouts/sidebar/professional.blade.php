<section class="vbox">
    <header class="header bg-primary lter text-center clearfix">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i
                        class="fa fa-plus"></i></button>
            <div class="btn-group hidden-nav-xs">
                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                    Switch Business
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left">
                    @if(auth()->user()->hasRole(array('assemblyline-professional-agent')))
                        @foreach(getAgentBusinesses() as $list)
                            <li>
                                <a href="{{ url('dashboard/change-company?slug='.$list->database) }}">{{ $list->company_name }}</a>
                            </li>
                        @endforeach
                        @else
                        @foreach(getCollaboratorCompanies() as $list)
                            <li>
                                <a href="{{ url('dashboard/change-company?slug='.$list->company->database) }}">{{ $list->company->company_name }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </header>
    <section class="w-f scrollable">
            <!-- nav -->
            <nav class="nav-primary hidden-xs">
                <ul class="nav">
            @if(session()->has('company_db') )

                        <li class="{{ set_active('workspace') }}">
                            <a href="{{ url('/workspace') }}">
                                <i class="fa fa-dashboard icon"><b class="bg-info"></b> </i>
                                <span>Work tools</span>
                            </a>
                        </li>

                        <li class="{{ set_active('notebook/*') }} ">
                            <a href="#uikit" class="{{ set_active('notebook/*') }}">
                                <i class="fa fa-book icon"> <b class="bg-success"></b> </i>
                                <span class="pull-right"> <i class="fa fa-angle-down text"></i>
                                <i class="fa fa-angle-up text-active"></i>
                            </span>
                                <span id="tour1">Notebook</span>
                            </a>
                            <ul class="nav lt">
                                <li class="{{ set_active('notebook/cash-register/*') }}">
                                    <a href="#table" class="{{ set_active('notebook/cash-register/*') }}">
                                        <i class="fa fa-angle-down text"></i>
                                        <i class="fa fa-angle-up text-active"></i>
                                        <span>Money In</span>
                                    </a>
                                    <ul class="nav lt">
                                        <li class="{{ set_active('notebook/cash-register/cash-receipt') }}">
                                            <a href="{{ route('cash-receipt') }}"
                                               class="{{ set_active('notebook/cash-register/cash-receipt') }}">
                                                <i class="fa fa-angle-right"></i> <span>Cash</span>
                                            </a>
                                        </li>

                                        <li class="{{ set_active('notebook/bank-receipt') }}">
                                            <a href="{{ url('notebook/bank-receipt') }}"
                                               class="{{ set_active('notebook/bank-receipt') }}">
                                                <i class="fa fa-angle-right"></i> <span>Bank</span>
                                            </a>
                                        </li>


                                    </ul>
                                </li>
                                <li class="{{ set_active('notebook/non-cash-register/*') }}">
                                    <a href="#ncr">
                                        <i class="fa fa-angle-down text"></i>
                                        <i class="fa fa-angle-up text-active"></i> <span>Money Out</span>
                                    </a>
                                    <ul class="nav lt">
                                        <li class="{{ set_active('notebook/non-cash-register/cash-payment') }}">
                                            <a href="{{ route('cash-payment') }}"
                                               class="{{ set_active('notebook/non-cash-register/cash-payment') }}">
                                                <i class="fa fa-angle-right"></i> <span>Cash</span>
                                            </a>
                                        </li>
                                        <li class="{{ set_active('notebook/bank-payment') }}">
                                            <a href="{{ url('notebook/bank-payment') }}"
                                               class="{{ set_active('notebook/bank-payment') }}">
                                                <i class="fa fa-angle-right"></i> <span>Bank</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>


                                <li class="{{ set_active('notebook/non-cash-register/credit-sales') }}">
                                    <a href="{{ url('notebook/non-cash-register/credit-sales') }}"
                                       class="{{ set_active('notebook/non-cash-register/credit-sales') }}">
                                        <i class="fa fa-share icon"> <b class="bg-success dker"></b> </i>
                                        <span id="tour3">Credit sales</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('notebook/non-cash-register/credit-purchase') }}">
                                    <a href="{{ url('notebook/non-cash-register/credit-purchase') }}"
                                       class="{{ set_active('notebook/non-cash-register/credit-purchase') }}">
                                        <i class="fa fa-reply icon"> <b class="bg-danger dker"></b> </i>
                                        <span id="tour4">Credit purchases</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="{{ set_active('memorandum') }}">
                            <a href="{{ url('/memorandum') }}">
                                <i class="fa fa-book icon"><b class="bg-info"></b> </i>
                                <span>Memorandum</span>
                            </a>
                        </li>
                        <li class="{{ set_active('transactions/inflow') }}">
                            <a href="{{ url('transactions/inflow') }}">
                                <i class="fa fa-share icon"> <b class="bg-success dker"></b> </i>
                                <span id="tour3">Receipts</span>
                            </a>
                        </li>

                        <li class="{{ set_active('transactions/outflow') }}">
                            <a href="{{ url('transactions/outflow') }}">
                                <i class="fa fa-reply icon"> <b class="bg-danger dker"></b> </i>
                                <span id="tour4">Payments</span>
                            </a>
                        </li>

                        <li class="{{ set_active('invoices/*') }}">
                            <a href="{{ url('invoices/sales-invoice') }}">
                                <i class="fa fa-list-alt icon"> <b class="bg-warning dker"></b> </i>
                                <span id="tour5">Invoices</span>
                            </a>
                        </li>


                        <li class="{{ set_active('journal-entry/*') }}">
                            <a href="{{ url('journal-entry') }}">
                                <i class="fa fa-file-text-o icon"> <b class="bg-danger dker"></b> </i>
                                <span id="tour6">Journal Entry</span>
                            </a>
                        </li>

                        <li class="{{ set_active('reports/*') }}">
                            <a href="{{ url('reports') }}">
                                <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                                <span>Quick View</span>
                            </a>
                        </li>

                        <li class="{{ set_active('setup/*') }} ">
                            <a href="#setup" class="{{ set_active('notebook/*') }} ">
                                <i class="fa fa-cogs icon"> <b class="bg-primary"></b> </i>
                                <span class="pull-right">
                                                <i class="fa fa-angle-down text"></i> <i
                                            class="fa fa-angle-up text-active"></i>
                                            </span>
                                <span id="tour2">Business Setup</span>
                            </a>
                            <ul class="nav lt">
                                <li class="{{ set_active('setup/accounts') }} ">
                                    <a href="{{ url('setup/accounts') }}"
                                       class="{{ set_active('setup/accounts') }}">
                                        <i class="fa fa-angle-right"></i> <span>Chart of Accounts</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/fiscal-years') }} ">
                                    <a href="{{ url('setup/fiscal-years') }}"
                                       class="{{ set_active('setup/fiscal-years') }}">
                                        <i class="fa fa-angle-right"></i> <span>Fiscal Years</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/customers') }} ">
                                    <a href="{{ url('setup/customers') }}"
                                       class="{{ set_active('setup/customers') }}">
                                        <i class="fa fa-angle-right"></i> <span>Customers</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/vendors') }} ">
                                    <a href="{{ url('setup/vendors') }}" class="{{ set_active('setup/vendors') }}">
                                        <i class="fa fa-angle-right"></i> <span>Vendors</span>
                                    </a>
                                </li>
                                {{--      <li class="{{ set_active('setup/Products') }} ">
                                         <a href="{{ url('setup/products') }}"
                                            class="{{ set_active('setup/Products') }}">
                                             <i class="fa fa-angle-right"></i> <span>Products</span>
                                         </a>
                                     </li> --}}
                                <li class="{{ set_active('setup/taxes') }} ">
                                    <a href="{{ url('setup/taxes') }}" class="{{ set_active('setup/taxes') }}">
                                        <i class="fa fa-angle-right"></i> <span>Taxes</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/bank-accounts') }} ">
                                    <a href="{{ url('setup/bank-accounts') }}"
                                       class="{{ set_active('setup/bank-accounts') }}">
                                        <i class="fa fa-angle-right"></i> <span>Bank Accounts</span>
                                    </a>
                                </li>
                                <li class="{{ set_active('setup/payment-terms') }} ">
                                    <a href="{{ url('setup/payment-terms') }}"
                                       class="{{ set_active('setup/payment-terms') }}">
                                        <i class="fa fa-angle-right"></i> <span>Payment Terms</span>
                                    </a>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="{{ url('/logoutCompany') }}">
                                <i class="fa fa-group icon"><b class="bg-warning"></b> </i>
                                <span>Logout Business</span>
                            </a>
                        </li>

            @endif
            @if(!auth()->user()->hasRole(array('assemblyline-professional-agent', 'professional')))
                <li class="{{ set_active('setup/professional/agent/*') }} ">
                    <a href="#agents" class="{{ set_active('setup/professional/agent/*') }} ">
                        <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                        <span> Agent Management</span>
                    </a>
                    <ul class="nav lt">
                        <li class="{{ set_active('setup/professional/agents') }} ">
                            <a href="{{ url('setup/professional/agents') }}"
                               class="{{ set_active('setup/professional/agents') }}">
                                <i class="fa fa-angle-right"></i> <span>List Agents</span>
                            </a>
                        </li>
                        <li class="{{ set_active('setup/professional/agents/map_agents') }} ">
                            <a href="{{ url('setup/professional/agents/map_agents') }}"
                               class="{{ set_active('setup/professional/agents/map_agents') }}">
                                <i class="fa fa-angle-right"></i> <span>Assign SMEs to Agents</span>
                            </a>
                        </li>
                        <li class="{{ set_active('setup/professional/agents/agentSmes') }} ">
                            <a href="{{ url('setup/professional/agents/agentSmes') }}"
                               class="{{ set_active('setup/professional/agents/agentSmes') }}">
                                <i class="fa fa-angle-right"></i> <span>  Agent's SMEs</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

                </ul>
            </nav>
            {{--<nav class="nav-primary hidden-xs">--}}
        {{--<ul class="nav">--}}
        {{--<li class="{{ set_active('collaborators/*') }}">--}}
        {{--<a href="{{ url('collaborators/businesses') }}">--}}
        {{--<i class="fa fa-group icon"><b class="bg-warning"></b> </i>--}}
        {{--<span>List Businesses</span>--}}
        {{--</a>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</nav>--}}
            <!-- / nav -->
    </section>
    <footer class="footer lt hidden-xs b-t b-light">
        <a href="javascript:void(0)" class="" id="togglerIcon">
        <span class="toggle"></span>
        <span class="toggle"></span>
        <span class="toggle"></span>
        </a>
    </footer>
</section>