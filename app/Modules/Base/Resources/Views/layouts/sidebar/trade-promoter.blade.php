<section class="vbox">
    <header class="header bg-primary lter text-center clearfix">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-dark btn-icon" title="New project"><i
                        class="fa fa-plus"></i></button>
            <div class="btn-group hidden-nav-xs">
                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                    Switch Business
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left">
                    @foreach(getCollaboratorCompanies() as $list)
                        <li>
                            <a href="{{ url('dashboard/change-company?slug='.$list->company->database) }}">{{ $list->company->company_name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </header>
    <section class="w-f scrollable">
            <nav class="nav-primary hidden-xs">
                <ul class="nav">

                    @if(session()->has('company_db') )

                        <li class="{{ set_active('dashboard') }}">
                            <a href="{{ url('/workspace') }}">
                                <i class="fa fa-dashboard icon"><b class="bg-info"></b> </i>
                                <span> View Work tools</span>
                            </a>
                        </li>

                        <li class="{{ set_active('reports/*') }}">
                            <a href="{{ url('reports') }}">
                                <i class="fa fa-bar-chart-o icon"> <b class="bg-primary dker"></b> </i>
                                <span> View Reports</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logoutCompany') }}">
                                <i class="fa fa-home icon"><b class="bg-warning"></b> </i>
                                <span>My Home {{session()->get('company_db')}}</span>
                            </a>
                        </li>

                </ul>
            </nav>
            @endif
            {{--<nav class="nav-primary hidden-xs">--}}
            {{--<ul class="nav">--}}


            {{--<li class="{{ set_active('collaborators/*') }}">--}}
            {{--<a href="{{ url('collaborators/businesses') }}">--}}
            {{--<i class="fa fa-group icon"><b class="bg-warning"></b> </i>--}}
            {{--<span>List Businesses</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</nav>--}}
    </section>
    <footer class="footer lt hidden-xs b-t b-light">
        <a href="javascript:void(0)" class="" id="togglerIcon">
        <span class="toggle"></span>
        <span class="toggle"></span>
        <span class="toggle"></span>
        </a>
    </footer>
</section>