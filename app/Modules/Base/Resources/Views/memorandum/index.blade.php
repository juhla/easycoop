@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">

                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            @if(auth()->user()->hasRole('professional'))
                                @if(canEdit())
                                    <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                            disabled><i class="fa fa-save"></i>
                                        Post to GL
                                    </button>
                                @endif
                            @endif
                        </div>
                        <form id="filter_ledger" action="{{ url('memorandum') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" id="start_date"
                                           name="start_date" value="{{ $start_date }}" placeholder="From:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" name="end_date"
                                           id="end_date" value="{{ $end_date }}" placeholder="To:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="btn-group col-md-3">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Submit</button>
                                <a href="{{ url('memorandum') }}" class="btn btn-default">Clear</a>
                            </div>
                        </form>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th width="5%"><input type="checkbox" id="checkAll"/></th>
                                    <th width="13%">Date</th>
                                    <th width="18%">Description</th>
                                    <th width="13%">Amount</th>
                                    <th width="20%">Debit Account</th>
                                    <th width="20%">Credit Account</th>
                                    <th width="10%"></th>
                                </tr>
                                </thead>
                                <form action="{{ url('memorandum/ledger') }}" method="post"
                                      id="post-memo">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <tbody id="transactionTable">
                                    @if(count($transactions) > 0)
                                        @foreach($transactions as $transaction)
                                            <tr class="edit-row" data-value="{{$transaction->id}}" valign="left">
                                                <td>
                                                    <input type="checkbox" name="ids[]" id="ids"
                                                           value="{{$transaction->id}}"/>
                                                </td>
                                                <td>{{ date('d/m/Y', strtotime($transaction->transaction_date))}}</td>
                                                <td>{{ $transaction->description}}</td>
                                                <td>
                                                    <span class="{{ ($transaction->transaction_type === 'inflow') ? 'text-success' : 'text-danger' }}">{{ formatNumber($transaction->amount)}}</span>
                                                </td>
                                                <td>{{ getLedgerName('D', $transaction->id) }}</td>
                                                <td>{{ getLedgerName('C', $transaction->id) }}</td>
                                                <td>
                                                    <div class="btn-group btn-group-sm">
                                                        <button type="button" class="btn btn-complete dropdown-toggle"
                                                                data-toggle="dropdown"><span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu"
                                                            style="right: 0; left: auto; overflow: visible !important">
                                                            <li><a href="#" class="view"
                                                                   data-value="{{$transaction->id}}">View
                                                                    Transaction</a></li>
                                                        </ul>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="no-income">
                                            <td colspan="8">No record found.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {!! $transactions->render() !!}
                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('transaction::view')
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/memorandum_one.js') }}" type="text/javascript"></script>
@stop
