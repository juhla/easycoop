@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">

                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <form method="post" action="{{url('memorandum/postmemo')}}" role="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="transaction-table">
                                        <thead>
                                        <tr>
                                            <th width="10%">Date</th>
                                            <th width="10%">PVC No</th>
                                            <th width="20%">Description</th>
                                            <th width="10%">Amount</th>
                                            <th width="20%">Debit Account</th>
                                            <th width="20%">Credit Account</th>
                                        </tr>
                                        </thead>
                                        <tbody align="">
                                        @if(count($ids) > 0)
                                            @foreach($ids as $id)
                                                <?php
                                                $entry = App\Modules\Base\Models\Transaction::getTheTransaction($id);
                                                $memo = App\Modules\Base\Models\Transaction::getMemoID();
                                                foreach ($entry->item as $eachItem) {
                                                    //dd($eachItem);

                                                    if ($eachItem->ledger_id == $memo) {
                                                        if ($eachItem->dc == 'D') {
                                                            $credit = $eachItem;
                                                            $debit = new stdClass();
                                                            $debit->ledger_id = null;

                                                        }
                                                        if ($eachItem->dc == 'C') {
                                                            $debit = $eachItem;
                                                            $credit = new stdClass();
                                                            $credit->ledger_id = null;
                                                        }
                                                    }


                                                }
                                                // dd($entry);
                                                ?>
                                                <tr class="edit-row" data-value="">

                                                    <td>
                                                        {{ date('m/d/Y', strtotime($entry->transaction_date)) }}
                                                    </td>
                                                    <td>
                                                        {{ $entry->reference_no }}
                                                    </td>

                                                    <td>
                                                        {{ $entry->description}}
                                                    </td>
                                                    <td>
                                                        {{ formatNumber($entry->amount) }}
                                                    </td>
                                                    <td>
                                                        {{ getLedgerName('D', $entry->id) }}
                                                    </td>
                                                    <td>
                                                        {{ getLedgerName('C', $entry->id) }}
                                                    </td>
                                                </tr>
                                                <tr class="edit-row" data-value="">
                                                    <input type="hidden" name="register_id[]" value="{{$entry->id}}"/>
                                                    <input type="hidden" name="transaction_type[]"
                                                           value="{{$entry->transaction_type}}"/>
                                                    <td>
                                                        {{ date('m/d/Y', strtotime($entry->transaction_date)) }}
                                                        <input type="hidden" name="date[]"
                                                               value="{{ date('m/d/Y', strtotime($entry->transaction_date)) }}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->reference_no }}
                                                        <input type="hidden" name="reference_no[]"
                                                               value="{{ $entry->reference_no }}"/>
                                                    </td>

                                                    <td>
                                                        {{ $entry->description}}
                                                        <input type="hidden" name="description[]"
                                                               value="{{ $entry->description}}"/>
                                                    </td>
                                                    <td>
                                                        {{ formatNumber($entry->amount) }}
                                                        <input type="hidden" name="amount[]" class="form-control"
                                                               id="amount" value="{{ $entry->amount }}"/>
                                                    </td>
                                                    <td>

                                                        <select name="debit_account[]" class="select2 accounts"
                                                                id="debit_account" style="width: 100%">
                                                            @foreach($accounts as $account)
                                                                <optgroup label="{{$account->name}}">
                                                                    @if($account->groups)
                                                                        @foreach($account->groups as $group)
                                                                            <option value="{{$group->id}}"
                                                                                    style="font-weight: 500"
                                                                                    disabled="disabled">{{$group->code.' '.$group->name}}</option>
                                                                            @if($group->nestedGroups($group->id)->count())
                                                                                @foreach($group->nestedGroups($group->id)->get() as $child)
                                                                                    <option value="{{$child->id}}"
                                                                                            style="font-weight: 500"
                                                                                            disabled="disabled">{{$child->code.' '.$child->name}}</option>
                                                                                    @if($child->nestedGroups($child->id)->count())
                                                                                        @foreach($child->nestedGroups($child->id)->get() as $sub)
                                                                                            <option value="{{$sub->id}}"
                                                                                                    style="font-weight: 500"
                                                                                                    disabled="disabled">{{$sub->code.' '.$sub->name}}</option>
                                                                                            @if($sub->ledgers->count())
                                                                                                @foreach ($sub->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                                    <option {{($debit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                    @if($child->ledgers->count())
                                                                                        @foreach ($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                            <option {{($debit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                            @if($group->ledgers->count())
                                                                                @foreach ($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                    <option {{($debit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    @if($account->ledgers->count())
                                                                        @foreach ($account->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                            <option {{($debit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </optgroup>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="credit_account[]" class="select2 accounts"
                                                                id="credit_account" style="width: 100%">
                                                            @foreach($accounts as $account)
                                                                <optgroup label="{{$account->name}}">
                                                                    @if($account->groups)
                                                                        @foreach($account->groups as $group)
                                                                            <option value="{{$group->id}}"
                                                                                    style="font-weight: 500"
                                                                                    disabled="disabled">{{$group->code.' '.$group->name}}</option>
                                                                            @if($group->nestedGroups($group->id)->count())
                                                                                @foreach($group->nestedGroups($group->id)->get() as $child)
                                                                                    <option value="{{$child->id}}"
                                                                                            style="font-weight: 500"
                                                                                            disabled="disabled">{{$child->code.' '.$child->name}}</option>
                                                                                    @if($child->nestedGroups($child->id)->count())
                                                                                        @foreach($child->nestedGroups($child->id)->get() as $sub)
                                                                                            <option value="{{$sub->id}}"
                                                                                                    style="font-weight: 500"
                                                                                                    disabled="disabled">{{$sub->code.' '.$sub->name}}</option>
                                                                                            @if($sub->ledgers->count())
                                                                                                @foreach ($sub->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                                    <option {{($credit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                                @endforeach
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                    @if($child->ledgers->count())
                                                                                        @foreach ($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                            <option {{($credit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                            @if($group->ledgers->count())
                                                                                @foreach ($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                                    <option {{($credit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                    @if($account->ledgers->count())
                                                                        @foreach ($account->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                                            <option {{($credit->ledger_id == $ledger->id ) ? "selected": null}} value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </optgroup>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6"><hr/></td>
                                                </tr>

                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <footer class="panel-footer  align-lg-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{ url('memorandum') }}" class="btn btn-default">Cancel</a>
                            </footer>
                        </form>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                </footer>
            </section>
        </aside>
    </section>
@endsection
@section('scripts')
@stop
