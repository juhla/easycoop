@extends('layouts.main')

@section('content')
    @include('flash::message')
    <div class="container">

        <h2>Create a new message</h2>
        {!! Form::open(['route' => 'message.store','data-parsley-validate'=>'']) !!}
        <div class="col-md-6">
            <!-- Subject Form Input -->
            <div class="form-group">
                {!! Form::label('subject', 'Subject', ['class' => 'control-label']) !!}
                {!! Form::text('subject', null, ['class' => 'form-control','required'=>'']) !!}
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::label('message', 'Message', ['class' => 'control-label']) !!}
                {!! Form::textarea('message', null, ['class' => 'form-control','required'=>'']) !!}
            </div>

           <?php  $counter = count($users);
           ?>
            @if($counter > 0)
                @foreach($users as $user)
                <div class="checkbox">
                        <label title="">
                            <input type="radio" name="recipient" value="{{ $user->user_id }}" >
                            {{ $user->first_name }}
                        </label>
                @endforeach
                </div>
            @endif

                        <!-- Submit Form Input -->
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
            </div>

        </div>
        {!! Form::close() !!}
    </div>

@stop

