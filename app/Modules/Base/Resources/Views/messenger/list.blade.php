@extends('layouts.main')

@section('content')
    <?php
    // dd(\App\Modules\Base\Models\Messenger\MessageId::find(1));
    ?>
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('message/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Create
                                Message </a>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th width="20%">Subject</th>
                                    <th width="20%">Message</th>
                                    <th width="15%">Recipient</th>
                                    <th width="15%">Status</th>
                                    <th width="10%"></th>
                                </tr>
                                </thead>

                                @if(!$message_list->isEmpty())
                                    <tbody>

                                    {{ session()->get('success') }}
                                    @if(count($message_list) > 0)
                                        @foreach($message_list as $value)
                                            <?php $sender = $value->messageId->sender_id;
                                            $receiver = $value->messageId->recipient_id;
                                            $userId = auth()->user()->id;
                                            ?>
                                            @if( $userId == $sender || $userId == $receiver )
                                                <tr>
                                                    <td>{{ $value->subject}}</td>
                                                    <td>{{ str_limit($value->message, $limit = 20, $end = '...') }}</td>
                                                    <td><?php echo isset($value->messageId->personalInfo->first_name) ? $value->messageId->personalInfo->first_name : "" ?></td>
                                                    <td>
                                                        @if($value->status === 0)
                                                            <span class='label label-warning'>Unseen</span>
                                                        @else
                                                            <span class='label label-success'>Seen</span>
                                                        @endif
                                                    </td>
                                                    <td><a href="{{ url('message/'.$value->id) }}"><span
                                                                    class='label label-info'>View</span></a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6">You have not added any message.</td>
                                                </tr>
                                            @endif
                                    </tbody>
                                @else
                                    <p>No records yet</p>
                                @endif

                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
