@extends('layouts.main')

@section('content')

    <div class="container">
        <header class="header bg-white b-b clearfix">
            <div class="row m-t-sm">
                <div class="col-sm-3 m-b-xs">
                    <a href="{{ url('message/create') }}" class="btn btn-info"><i class="fa fa-plus"></i> Create Message </a>
                </div>
            </div>
        </header>
        <h3>Message</h3>

        <div class="col-md-6">
            {!! Form::textarea('message', $message->message, ['class' => 'form-control', 'readonly' => 'true']) !!}

        </div>
        <a href="{{url('message')}}"><button class="btn btn-default ">
            <i class="fa fa-mail"></i> Message List
            <span class="caret"></span>
        </button></a>


    </div>

@stop

