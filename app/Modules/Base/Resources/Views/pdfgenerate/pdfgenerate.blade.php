<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <style>
        body {
            background: #fff;
            font-family: sans-serif;
        }

        .one {
            background-color: rgb(255, 255, 255, 0.2);
            padding: 10px;
            float: left;

        }

        .two {
            background-color: rgb(255, 255, 255, 0.2);
            padding: 10px;
            float: right;
        }

        .bar {

            background: #cccccc;
            padding: 5px;
            width: 100%;
            color: #000;
            margin-top: 20px;
        }

        .bar2 {

            background: #cccccc;
            padding: 5px;
            width: 100%;
            color: #000;
        }

        .tableBody {
            background: #F2F1F1;
            width: 100%;
            padding: 5px 5px 0px 5px;
            height: auto;

        }

        .table {
            background-color: rgb(255, 255, 255, 0.2);
            width: 100%;
        }


    </style>

</head>
<body>
<?php
$bmac = config('constants.bmac_url');
?>


<div class="one">
    {{--@if(!empty(getBusinessOwnerAuth()->company->company_logo))--}}
    {{--<img src="http://onebmac.com/uploads/{{getBusinessOwnerAuth()->company->company_logo}}" width="60px"--}}
    {{--height="60px">--}}
    {{--@endif--}}
    @if(!empty(getBusinessOwnerAuth()->company->company_logo))
        <img src="{{asset("uploads/".getBusinessOwnerAuth()->company->company_logo)}}" width="60px"
             height="60px">
    @endif
</div>


<div class="two">

    <p class="">
        <span style="font-weight:bolder;font-size:14px;color:#000;"> {{ getBusinessOwnerAuth()->company_name }}</span><br>
        <a href="{{ getBusinessOwnerAuth()->company->website }}">{{ getBusinessOwnerAuth()->company->website }}</a><br>
        <span style="font-weight: bolder;color:#807676;font-size:11px"> {{ getBusinessOwnerAuth()->company->address }}</span><br>

        <span style="font-weight: bolder;color:#807676;font-size:11px"> {{ getBusinessOwnerAuth()->company->city }}
            , {{ getBusinessOwnerAuth()->company->state }}</span><br>

        {{--{{ getCountry(getBusinessOwnerAuth()->company->country) }}--}}
        <span style="font-weight: bolder;color:#000;font-size:11px"> Telephone: </span><span
                style="font-weight: bolder;color:#807676;font-size:11px">{{ getBusinessOwnerAuth()->company->phone_no }}</span>

    </p>

</div>

<div style="clear: both;margin-bottom: -20px"></div>


<div class="bar" style="margin-bottom: -9px;padding:10px">

    <table class="table">

        <tr>
            <td width="80%">
                <span style="font-weight: bolder;color:#000;font-size:11px"> Billed To: </span><br>
                <span style="font-weight:bolder;font-size:14px;color:#000;">{{ $invoice->customer->name }}</span><br>
                <span style="font-weight:bolder;font-size:14px;color:#000;">{{ $invoice->customer->address }}</span><br>
            </td>

            <td>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span style="font-weight:bolder;font-size:11px;color:#000;"> #{{ $invoice->invoice_no }}</span><br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span style="font-weight: bolder;color:#807676;font-size:11px">{{date('jS M, Y', strtotime( $invoice->invoice_date )) }} </span>


            </td>
        </tr>


    </table>


</div>


<div style="clear: both;"></div>

<div style="clear:both">&nbsp;</div>

<div class="bar">
    <table class="table">
        <tr>
            <td colspan="3" style="font-weight: bolder;color:#000">Summary</td>
        </tr>
    </table>
</div>
<div class="tableBody">

    <table class="table">
        <thead>
        <tr>
            <td style="color:#000;font-size: 11px;font-weight: bolder">Description</td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td style="color:#000;font-size: 11px;text-align: right;font-weight: bolder">Amount</td>
        </tr>
        </thead>


        @foreach( $invoiceDet as $item )
            <tr>
                <td style="font-size:11px">{{ $item->description  }}</td>
                <td>&nbsp;&nbsp;</td>
                <td style="text-align: right;font-size:11px">{{ formatNumber($item->amount )}}</td>
            </tr>
        @endforeach

        <tr style="background:hsl(0, 0%, 90%) ">


            <td>&nbsp;&nbsp;</td>
            <td style="text-align: right;font-weight: bolder;font-size:11px">Amount</td>
            <td style="text-align: right;font-size:11px">{{ formatNumber($invoice->amount_due) }} </td>

        </tr>


        {{-- display vat if value is 1 --}}

        <?php $vatshow = $invoice->amount_due; ?>
        @if( $vatshow = 1 )
            <tr style="background:hsl(0, 0%, 90%) ">
                <td>&nbsp;&nbsp;</td>
                <td style="text-align: right;font-weight: bolder;font-size:11px">VAT</td>
                <td style="text-align: right;font-size:11px">


                    {{ formatNumber($invoice->total_amount - $invoice->amount_due) }} </td>

            </tr>

        @endif

        {{-- /display vat if value is 1 --}}


    </table>

</div>

<div class="bar2">

    <table class="table">


        <tr>
            <td style="text-align: right;font-weight: bolder;font-size:11px;color:#000">&nbsp;&nbsp;</td>
            <td style="text-align: right;font-weight: bolder;font-size:11px;color:#000">&nbsp;&nbsp;</td>
            <td style="text-align: right;font-weight: bolder;font-size:11px;color:#000"></td>
            <td style="text-align: right;font-weight: bolder;font-size:11px;color:#000">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total
                ({{ $invoice->currency }} )
            </td>
            <td style="text-align: right;font-size:11px">{{ formatNumber($invoice->total_amount) }} </td>

        </tr>


    </table>


</div>

<div style="margin-top:10px">
    <span style="font-weight: bolder;color:#000;font-size:11px">Bank details: </span><br>

    <span style="font-weight: bolder;color:#000;font-size:10px">Account Name:</span>
    <span style="font-weight: bolder;color:#a9a3a3;font-size:10px"> {{ucwords($accountNumber->account_name)}}</span><br>
    <span style="font-weight: bolder;color:#000;font-size:10px">Bank:</span>
    <span style="font-weight: bolder;color:#a9a3a3;font-size:10px">{{ucwords($bankName->name)}} </span>&nbsp;<br>
    <span style="font-weight: bolder;color:#000;font-size:10px">Account No:</span>
    <span style="font-weight: bolder;color:#a9a3a3;font-size:10px"> {{$accountNumber->account_number}} </span>

</div>


</body>
</html>
   








