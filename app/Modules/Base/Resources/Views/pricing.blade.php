<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="BMAC | Business collaboration pricing">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>BMAC Pricing | Business Collaboration </title>
    <link rel="shortcut icon" href="{{ asset('images/fav.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/pricing/pricing.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

  </head>
<body>
<div class="container1 bmac">
    <div class="y-bmac-top-bar">
        <span class="y-bmac-brand">BMAC</span>
    </div>

    <h1>Pricing</h1>
    <ul class="y-nav">
        <li><a href="#bmac">BMAC</a></li>
        <li><a href="#ims">IMS</a></li>
        <li><a href="#hr">HR</a></li>
    </ul>
    <div class="bmac" id="fortablesections">
        <section class="bmac-pricing " id="bmac">
            <h1>BMAC Pricing</h1>
            <table >
                <tr>
                    <td>&nbsp;</td>
                    <td>No of Users</td>
                    <td>Licence fee</td>

                    <td>Annual maintenance fee</td>
                </tr>
                <tr>
                    <td>Basic</td>
                    <td>0 - 2</td>
                    <td>Free</td>

                    <td>14,999</td>
                </tr>
                <tr>
                    <td>Silver</td>
                    <td>3 - 5</td>
                    <td>29,999</td>

                    <td>19,999</td>
                </tr>
                <tr>
                    <td>Gold</td>
                    <td>6 - 10</td>
                    <td>39,999</td>

                    <td>25,999</td>
                </tr>
                <tr>
                    <td>Diamond</td>
                    <td>11 - 20</td>
                    <td>49,999</td>

                    <td>39,999</td>
                </tr>
                <tr>
                    <td>Platinum</td>
                    <td>Above 20</td>
                    <td>54,999</td>

                    <td>49,999</td>
                </tr>

            </table>

        </section>
        <section class="ims-pricing" id="ims">
            <h1>IMS Pricing</h1>
            <table>
                <tr>
                    <td>Sale Product Line</td>
                    <td>Initial fee <p>One-off</p></td>
                    <td>Desktop Installation <p>One-off</p></td>
                    <td>Annual maintenace fee</td>
                </tr>
                <tr>
                    <td>1 - 19</td>
                    <td>49,999</td>
                    <td>19,999</td>
                    <td>39,999</td>
                </tr>
                <tr>
                    <td>20 - 49</td>
                    <td>49,999</td>
                    <td>38,999</td>
                    <td>74,999</td>
                </tr>
                <tr>
                    <td>50 - 100</td>
                    <td>49,999</td>
                    <td>99,999</td>
                    <td>199,999</td>
                </tr>
                <tr>
                    <td>Above 100</td>
                    <td>49,999</td>
                    <td>Negotiable</td>
                    <td>399,999</td>
                </tr>
            </table>
            <div class="note">
                <p>The charges above are based on the following: </p>
                <ul>
                    <li>No of Product line</li>
                    <li>Functionality and customisation</li>
                    <li>maintenace fee and ongoing support cost</li>
                    <li>Implementation and training</li>
                </ul>
            </div>


        </section>
        <section class="hr-pricing" id="hr">
            <h1>HR Pricing</h1>
            <table>
                <tr>
                    <td>Number of current employee</td>
                    <td>Licence fee <span>Initial One-off cost</span></td>
                    <td>Ongoing Annual Product fee (N) <p>One-off</p></td>
                </tr>
                <tr>
                    <td>Up to 10 Employees</td>
                    <td>19,999</td>
                    <td>39,999</td>
                </tr>
                <tr>
                    <td>Up to 20 Employees</td>
                    <td>29,999</td>
                    <td>44,999</td>
                </tr>
                <tr>
                    <td>Up to 30 Employees</td>
                    <td>49,999</td>
                    <td>54,999</td>
                </tr>
                <tr>
                    <td>Up to 50 Employees</td>
                    <td>79,999</td>
                    <td>69,999</td>
                </tr>
                <tr>
                    <td>Up to 100 Employees</td>
                    <td>149,999</td>
                    <td>119,999</td>
                </tr>
                <tr>
                    <td>Up to 200 Employees</td>
                    <td>224,999</td>
                    <td>149,999</td>
                </tr>
                <tr>
                    <td>Up to 300 Employees</td>
                    <td>274,999</td>
                    <td>199,999</td>
                </tr>


            </table>



        </section>

    </div>


</div>







<footer>
    <div class="container">
        <p> &copy {{ date('Y') }} All Rights Reserved. ikOOba Technologies </p>
    </div>
</footer>

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/pricing/pricing.js')}}"></script>
</body>
</html>
