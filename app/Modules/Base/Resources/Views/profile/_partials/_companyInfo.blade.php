<div class="row">
    <div class="col-md-12">

        {!!Form::open(array('url' => URL("profile/updateCompanyInfo"), 'method'=>'post', 'id'=>'companyForm', 'data-validate' => 'parsley') )!!}
            <div class="form-group">
                <label class="control-label">Company Name</label>
                <input type="text" class="form-control" name="company_name" id="company_name" value="{{ $companyInfo->company_name }}" data-required data-minlength="5">
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label class="control-label">Registration Number</label>
                    <input type="text" class="form-control" name="rc_number" id="rc_number" value="{{ $companyInfo->rc_number }}" data-required data-maxlength="10" />
                </div>
                <div class="col-md-6">
                    <label class="control-label">Incorporation Date </label>
                    <input type="text" class="form-control datepicker" name="date_incorporated" id="date_incorportated" value="{{ date('d/m/Y', strtotime($companyInfo->date_incorporated)) }}" data-date-format="DD/MM/YYYY" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label">Company Description</label>
                <textarea class="form-control" name="about_company" id="about_company" rows="3">{{ $companyInfo->about_company }}</textarea>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label class="control-label">Company Address</label>
                    <input type="text" class="form-control" name="address" id="address" value="{{ $companyInfo->address }}" />
                </div>
                <div class="col-md-6">
                    <label class="control-label">Phone Number </label>
                    <input type="text" class="form-control" name="phone_no" id="phone" value="{{ $companyInfo->phone_no }}" data-maxlength="15" data-type="number" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                <label class="control-label">Industry</label>
                    <select  class="select2-option" style="width: 100%" id="industry_no" name="industry_no">
                        <option value=""></option>
                        @foreach(getIndustry() as $industry){
                            <option value="{{$industry->id}}">{{$industry->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Average No. of Staff</label>
                    <input type="text" class="form-control" name="no_of_staff" id="staff_no" value="{{ $companyInfo->no_of_staff }}" >
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label class="control-label">Country</label>
                    <select class="select2-option" style="width: 100%" id="coy_country" name="country">
                        <option value="">Select Country...</option>
                        @foreach(getCountry() as $country){
                        <option value="{{$country->id}}" {{ ($companyInfo->country === $country->id) ? 'selected' : '' }}>{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">State</label>
                    <select  class="select2-option" style="width: 100%" id="coy_state" name="state" >
                        <option value="">Select State...</option>
                    </select>
                    <input type="hidden" id="coy_state_id" value="{{ $companyInfo->state }}" />

                </div>
                <div class="col-md-4">
                    <label class="control-label">City</label>
                    <input type="text" class="form-control" name="city" id="city" value="{{ $companyInfo->city }}">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label class="control-label">Company Website</label>
                    <input type="text" class="form-control" name="website" id="website" value="{{ $companyInfo->website }}" >
                </div>
                <div class="col-md-6">
                    <label class="control-label">Company Email </label>
                    <input type="text" class="form-control" name="email" id="coy_email"  value="{{ $companyInfo->email }}" data-type="email">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <button type="submit" id="save_coyInfo" class="btn btn-primary">
                    <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none" ></i>
                    Save
                </button>
                <button type="reset" class="btn" onclick="$('#save_coyInfo').text('Save');$('#coy_id').val('');$('#coy_row_index').val('');">Clear</button>
            </div>
        {!!Form::close()!!}
    </div>
</div>
@section('scripts')
    <script>

        //auto populate contact state dropdown
        var getStates = function (country_id) {
            var currentState = $('#coy_state_id').val();
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "{{URL::to('api-v1/get-states/')}}/" + country_id,
                success: function (data) {
                    //if (data.error === 0) {
                        for (var i = 0; i < data.length; i++) {
                            var id = data[i].id;
                            var name = data[i].name;
                            $('#coy_state').append("<option value='" + id + "'>" + name + "</option>");
                        }
                    $("#coy_state").select2('val', currentState);
                }
                //timeout: 10000
            });

        }
        $('#coy_country').change(function () {
            var country_id = $(this).val();
            //empty the select with previous state
            $('#coy_state').empty();
            //populate the state field
            getStates(country_id);
        });

        var coy_country = $('#coy_country').val();

        if (coy_country > 0) {
            getStates(coy_country);
        }
    </script>
    @parent
@stop