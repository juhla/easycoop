<form id="pic-form" method="post" action="{{ url('/profile/change-company-logo') }}" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <input type="file" name="company_logo"/>
    <hr>
    <button type="submit" class="btn btn-primary">Change Company Logo</button>
</form>
@if(!empty(getBusinessOwnerAuth()->company->company_logo))
    <img src="{{asset("uploads/".getBusinessOwnerAuth()->company->company_logo)}}" width="200px"
         height="200px">
@endif