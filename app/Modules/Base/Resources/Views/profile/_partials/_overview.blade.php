    <div class="col-md-12">
        <div class="pd-20 personal-info">
            <h3 class="mgbt-xs-15 mgtp-10 font-semibold"><i class="icon-user mgr-10 profile-icon"></i> PERSONAL INFO</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">First Name:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->first_name }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Last Name:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->last_name }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Gender:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->gender }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Birthday:</label>
                        <div class="col-xs-6 controls">{{ date('M d, Y', strtotime(auth()->user()->personal_info->date_of_birth)) }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">City:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->city }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Country:</label>
                        <div class="col-xs-6 controls">{{ getCountry(auth()->user()->personal_info->country) }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Email:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->email }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Phone:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->phone_no }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Interests:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->interests }}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row mgbt-xs-0">
                        <label class="col-xs-6 control-label">Languages:</label>
                        <div class="col-xs-6 controls">{{ auth()->user()->personal_info->languages }}</div>
                    </div>
                </div>
            </div>
            @if(!auth()->user()->hasRole('employee'))
            <hr class="pd-10">
            <div class="row">
                @if(isset(auth()->user()->company))
                <div class="col-sm-12 ">
                    <h3 class="font-semibold"> COMPANY : <b>{{ auth()->user()->company->company_name }}</b></h3>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 controls">{{ auth()->user()->company->about_company }}</div>
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-12">
                            <div class="row">
                                <label class="col-xs-6 control-label">Established</label>
                                <div class="col-xs-6 controls">{{ auth()->user()->company->date_incorporated }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row ">
                                <label class="col-xs-6 control-label">Location <i class="fa fa-map-marker"></i> </label>
                                <div class="col-xs-6 controls">{{ auth()->user()->company->address }}</div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="row ">
                                <label class="col-xs-6 control-label">Website <i class="fa fa-globe"></i> </label>
                                <div class="col-xs-6 controls">{{ auth()->user()->company->website }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                                <label class="col-xs-6 control-label">Email <i class="fa fa-envelope"></i> </label>
                                <div class="col-xs-6 controls">{{ auth()->user()->company->email }}</div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row mgbt-xs-0">
                                <label class="col-xs-6 control-label">Phone <i class="fa fa-phone"></i> </label>
                                <div class="col-xs-6 controls">{{ auth()->user()->company->phone_no }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            @endif
        </div>
    </div>
