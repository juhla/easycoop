        @include('flash::message')
        {!! Form::open(array('url' => URL("profile/updatePersonalInfo"), 'method' => 'post', 'id' => 'personalForm', 'data-validate' => 'parsley') ) !!}
            <div class="form-group row">
                <div class="col-md-4">
                    <label class="control-label">Title</label>
                    <select  class="select2-option" style="width: 100%" name="title">
                        <option value="Mr" {{ ($personalInfo->title === 'Mr')? 'selected' : '' }}>Mr</option>
                        <option value="Mrs" {{ ($personalInfo->title === 'Mrs')? 'selected' : '' }}>Mrs</option>
                        <option value="Miss" {{ ($personalInfo->title === 'Miss')? 'selected' : '' }}>Miss</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">First Name</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" value="{{$personalInfo->first_name}}" data-required data-minlength="3">
                </div>
                <div class="col-md-4">
                    <label class="control-label">Last Name</label>
                    <input type="text" class="form-control" name="last_name"  value="{{$personalInfo->last_name}}" data-required data-minlength="3">
                </div>
            </div>
            <div class="form-group row">

                <div class="col-md-4">
                    <label class="control-label">Date of birth</label>
                    <input type="text" class="form-control datepicker" name="dob" id="dob" value="{{ date('d/m/Y', strtotime($personalInfo->date_of_birth)) }}" placeholder="dd/mm/yyyy">
                </div>
                <div class="col-md-4">
                    <label class="control-label">Gender</label>
                    <select  class="select2-option" style="width: 100%" name="gender">
                        <option value="Male" {{ ($personalInfo->gender === 'Male')? 'selected' : '' }}>Male</option>
                        <option value="Female" {{ ($personalInfo->gender === 'Female')? 'selected' : '' }}>Female</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">Marital Status</label>
                    <select class="select2-option" style="width: 100%" name="marital_status">
                        <option value="Married" {{ ($personalInfo->marital_status === 'Married')? 'selected' : '' }}>Married</option>
                        <option value="Unmarried" {{ ($personalInfo->marital_status === 'Unmarried')? 'selected' : '' }}>Unmarried</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <label class="control-label">Country</label>
                    <select  class="select2-option" style="width: 100%" id="psn_country" name="country">
                        <option value="">Select Country...</option>
                        @foreach(getCountry() as $country){
                        <option value="{{$country->id}}" {{ ($personalInfo->country === $country->id)? 'selected' : '' }}>{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">State</label>
                    <select  class="select2-option" style="width: 100%" id="psn_state" name="state" >
                        <option value="">Select State...</option>
                    </select>
                    <input type="hidden" id="current_state_id" value="{{ $personalInfo->state }}" />
                </div>
                <div class="col-md-4">
                    <label class="control-label">City</label>
                    <input type="text" class="form-control" name="city" id="city" value="{{ $personalInfo->city }}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">About Me</label>
                <textarea class="form-control" name="bio" id="about_me" rows="3" placeholder="Brief description about you">{{ $personalInfo->bio }}</textarea>
            </div>
            <hr>
            <div class="form-group pull-right">
                <button type="submit" id="publish-personal" class="btn btn-primary">
                    <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none" ></i>
                    Save
                </button>
            </div>
        {!! Form::close() !!}
<!-- /row-->
@section('scripts')
    <script>
        $(function() {

            //jquery datepicker plugin
            //$(".datepicker").datepicker({format: 'yyyy-mm-dd', minView: 2, autoclose: 1});
            var baseurl = $('#baseurl').val();
            var getStates = function (country_id) {
                var currentState = $('#current_state_id').val()

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: baseurl + "/api-v1/get-states/" + country_id,
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var id = data[i].id;
                            var name = data[i].name;
                            $('#psn_state').append("<option value='" + id + "'>" + name + "</option>");
                        }
                        $("#psn_state").select2('val', currentState);
                    }
                    //timeout: 10000
                });

            }


            //auto populate personal state dropdown
            $('#psn_country').change(function () {
                var country_id = $(this).val();
                //empty the select with previous state
                $('#psn_state').empty();
                //$('#psn_city').empty();

                //populate the state field
                getStates(country_id, 'psn');
            });

            var psn_country = $('#psn_country').val();

            if (psn_country > 0) {
                getStates(psn_country);
            }

        });
    </script>
    @parent
@stop