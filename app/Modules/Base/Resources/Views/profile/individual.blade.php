@extends('layouts.main')

@section('content')
    @include('flash::message')
    <section class="vbox">
        <header class="header bg-white b-b b-light"> 
            <p>Profile Overview</p> 
        </header>
        <section class="scrollable">
            <section class="hbox stretch">
                {{--<aside class="aside-lg bg-light lter b-r">--}}  
                    {{--<section class="vbox">--}}
                        {{--<section class="scrollable">--}}
                            {{--<div class="wrapper">--}}
                                {{--<div class="clearfix m-b">--}}
                                    {{--<a href="#" class="pull-left thumb m-r">--}}
                                        {{--<img src="{{ auth()->user()->displayAvatar() }}" class="img-circle">--}}
                                    {{--</a>--}}
                                    {{--<div class="clear">--}}
                                        {{--<div class="h3 m-t-xs m-b-xs">{{ auth()->user()->displayName() }}</div>--}}
                                        {{--<small class="text-muted">--}}
                                            {{--{{ getRoles(auth()->id()) }}--}}
                                        {{--</small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="panel wrapper panel-success">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-xs-4">--}}
                                            {{--<a href="#">--}}
                                                {{--<span class="m-b-xs h4 block">0</span>--}}
                                                {{--<small class="text-muted">Users</small>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-xs-4">--}}
                                            {{--<a href="#">--}}
                                                {{--<span class="m-b-xs h4 block">0</span>--}}
                                                {{--<small class="text-muted">Threads</small>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-xs-4">--}}
                                            {{--<a href="#">--}}
                                                {{--<span class="m-b-xs h4 block">0</span>--}}
                                                {{--<small class="text-muted">Reviews</small>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div>--}}
                                    {{--<small class="text-uc text-xs text-muted">about me</small>--}}
                                    {{--<p>{{ trimText($personalInfo->bio, 300) }}</p>--}}
                                    {{--<div class="line"></div>--}}
                                    {{--<small class="text-uc text-xs text-muted">connection</small>--}}
                                    {{--<p class="m-t-sm">--}}
                                        {{--<a href="#" class="btn btn-rounded btn-twitter btn-icon"><i class="fa fa-twitter"></i></a>--}}
                                        {{--<a href="#" class="btn btn-rounded btn-facebook btn-icon"><i class="fa fa-facebook"></i></a>--}}
                                        {{--<a href="#" class="btn btn-rounded btn-gplus btn-icon"><i class="fa fa-google-plus"></i></a>--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</section>--}}
                {{--</aside>--}}
                <aside class="bg-white">
                    <section class="vbox">
                        <header class="header bg-light bg-gradient">
                            <ul class="nav nav-tabs nav-white">
                                <li class="active"><a href="#businesses" data-toggle="tab">Overview</a></li>
                                <li class=""><a href="#profile" data-toggle="tab">Edit Profile</a></li>
                                @if(!auth()->user()->hasRole('employee'))
                                    <li class=""><a href="#company-info" data-toggle="tab">Company Info</a></li>
                                @endif
                                <li class=""><a href="#profile-pic" data-toggle="tab">Profile Pic</a></li>
                                <li class=""><a href="#company-logo" data-toggle="tab">Company Logo</a></li>
                            </ul>
                        </header>
                        <section class="scrollable">
                            <div class="tab-content">
                                <div class="tab-pane active" id="businesses">
                                    @include('profile._partials._overview')
                                </div>
                                <div class="tab-pane" id="profile">
                                    <div class="text-center wrapper">
                                        @include('profile._partials._personalProfile')
                                    </div>
                                </div>
                                @if(!auth()->user()->hasRole('employee'))
                                    <div class="tab-pane" id="company-info">
                                        <div class="text-center wrapper">
                                            @include('profile._partials._companyInfo')
                                        </div>
                                    </div>
                                @endif
                                <div class="tab-pane" id="profile-pic">
                                    <div class="text-center wrapper">
                                        <form id="pic-form" method="post" action="{{ url('/profile/change-profile-pic') }}" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                            <input type="file" name="profilepic" />
                                            <hr>
                                            <button type="submit" class="btn btn-primary">Change Profile Pic</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="company-logo">
                                    <div class="text-center wrapper">
                                        @include('profile._partials._companyLogo')
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </aside>
                <aside class="col-lg-3 b-l">
                    {{--<section class="vbox">--}}
                        {{--<section class="scrollable">--}}
                            {{--<div class="wrapper">--}}
                                {{--<section class="panel panel-default">--}}
                                    {{--<h4 class="font-thin padder">TownHall: Latest Threads</h4>--}}
                                    {{--<ul class="list-group">--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago</small>--}}
                                        {{--</li>--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p>Morbi nec <a href="#" class="text-info">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 1 hour ago</small>--}}
                                        {{--</li>--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p><a href="#" class="text-info">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. </p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 2 hours ago</small>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</section>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</section>--}}
                </aside>
            </section>
        </section>
    </section>

@endsection
@section('scripts')
    <script>
        var $baseurl = $('#baseurl').val();
        /////////////////////////////////////////////////////////////////
        ////////////PERSONAL INFORMATION SECTION/////////////////////////
        /////////////////////////////////////////////////////////////////
        $("#personalForm").submit(function(event) {
            event.preventDefault();
            if($('#personalForm').parsley().isValid()) {
                $('#publish-personal').attr('disabled', true);
                $('.spinner').show();
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: "POST",
                    dataType: 'json',
                    success: function (e) {
                        //$('body').loader('hide');
                        if (!e.status) {
                            displayNotification(e.message, 'Error', 'error');
                            $('#publish-personal').attr('disabled', true);
                            $('.spinner').hide();
                        } else {
                            displayNotification(e.message, 'Success!', 'success')
                        }
                        $('#publish-personal').attr('disabled', false);
                        $('.spinner').hide();

                    },
                    error: function (e) {
                        displayNotification(e.message, 'Error!!', 'error');
                        $('#publish-personal').attr('disabled', false);
                        $('.spinner').hide();
                    }
                });
            }
        });

        //submit contact form
        $("#companyForm").submit(function(event) {
            event.preventDefault();

            if($('#companyForm').parsley().isValid()) {
                $('#save_coyInfo').attr('disabled', true);
                $('.spinner').show();
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: "POST",
                    dataType: 'json',
                    success: function (e) {
                        //$('body').loader('hide');
                        if (!e.status) {
                            displayNotification(e.message, 'Error', 'error');
                            $('#save_coyInfo').attr('disabled', false);
                            $('.spinner').hide();
                        } else {
                            displayNotification(e.message, 'Success!', 'success')
                        }
                        $('#save_coyInfo').attr('disabled', false);
                        $('.spinner').hide();

                    },
                    error: function (e) {
                        displayNotification(e.message, 'Error!!', 'error');
                        $('#save_coyInfo').attr('disabled', false);
                        $('.spinner').hide();
                    }
                });
            }
        });
    </script>
@stop
