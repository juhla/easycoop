@extends('layouts.main')

@section('content')
    <section class="vbox prof-revamp">

        <!-- adding tabs -->
        <div>
            <ul class="nav nav-tabs " role="tablist">
                <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab"
                                                          data-toggle="tab">Overview</a></li>
                <li role="presentation"><a href="#businesses" aria-controls="businesses" role="tab" data-toggle="tab">Businesses</a>
                </li>
                <li role="presentation"><a href="#company-info" aria-controls="company-info" role="tab"
                                           data-toggle="tab">Company Info</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a>
                </li>
                <li role="presentation"><a href="#dprofile" aria-controls="dprofile" role="tab" data-toggle="tab">Profile</a>
                </li>
                <li role="presentation"><a href="#profile-pic" aria-controls="profile-pic" role="tab" data-toggle="tab">Profile
                        Photo</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="overview">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            <div class="item active">
                                <!--<img src="images/slide01.png" alt="professional how-to"> -->
                                <div class="welcome-to-prof">

                                    @include('flash::message')
                                    @if (!empty($error))
                                        <div class="alert alert-danger">
                                            <ul>
                                                <li>{{ $error }}</li>
                                            </ul>
                                        </div>
                                    @endif
                                    <h1 class="text-center">WELCOME TO <span class="ikooba">BMAC</span> PROFESSIONAL
                                        ACCOUNT</h1>
                                    <h3 class="text-center "><span class="label label-primary">HOW IT WORKS </span></h3>
                                </div>
                                <div class="slide-content row">
                                    <div class="col1  col-md-4 text-center">
                                        <img src="{{asset('images/signin-slide.png')}}"
                                             class="signup-icon" alt="professional sign in icon"/>
                                        <div class="desc">
                                            <p><span class="circular">1</span></p>
                                            <p>Sign in and complete your Professional account profile
                                                in the BMAC account settings.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/get-client-slide.png')}} " alt="get client icon"/>
                                        <div class="desc">
                                            <p><span class="circular">2</span></p>
                                            <p>Get your client to sign up in the BMAC. He should register
                                                as a business owner.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/get-businessowner2invite.png')}}"
                                             alt="get business owner to invite icon"/>
                                        <div class="desc">
                                            <p><span class="circular">3</span></p>
                                            <p>Get him to invite you to his BMAC Business account by using
                                                the collaborator tool. Check your email and verify.

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-caption">

                                </div>
                            </div>
                            <div class="item">
                                <!--<img src="images/slide01.png" alt="professional how-to"> -->
                                <div class="welcome-to-prof">
                                    <h1 class="text-center">WELCOME TO <span class="ikooba">BMAC</span> PROFESSIONAL
                                        ACCOUNT</h1>
                                    <h3 class="text-center"><span class="label label-primary">HOW IT WORKS </span></h3>
                                </div>
                                <div class="slide-content row">

                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/get-businessowner2invite.png')}}"
                                             alt="get business owner to invite icon"/>
                                        <div>
                                            <p><span class="circular">3</span></p>
                                            <p>Get him to invite you to his BMAC Business account by using
                                                the collaborator tool. Check your email and verify.

                                            </p>
                                        </div>
                                    </div>

                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/set-client-account.png')}}" alt="get client icon"/>
                                        <div>
                                            <p><span class="circular">4</span></p>
                                            <p>You can now set up your client Chart of Accounts,
                                                Fiscal year.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/interpret-report.png')}}"
                                             alt="get business owner to invite icon"/>
                                        <div>
                                            <p><span class="circular">5</span></p>
                                            <p>You can post transactions, view & interpret various reports as well as
                                                carry out simple analysis for your client.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-caption">

                                </div>
                            </div>
                        </div><!-- close .carousel-inner -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- #carousel-example-generic close slide -->

                    <div id="no-carousel">
                        <!-- Wrapper for slides -->
                        <div class="no-carousel-inner" role="listbox">
                            <div class="item">
                                <!--<img src="images/slide01.png" alt="professional how-to"> -->
                                <div class="welcome-to-prof">
                                    <h1 class="text-center">WELCOME TO <span class="ikooba">BMAC</span> PROFESSIONAL
                                        ACCOUNT</h1>
                                    <h3 class="text-center "><span class="label label-primary">HOW IT WORKS </span></h3>
                                </div>
                                <div class="slide-content row">
                                    <div class="col1  col-md-4 text-center">
                                        <img src="{{asset('images/signin-slide.png')}}"
                                             class="signup-icon" alt="professional sign in icon"/>
                                        <div class="desc">
                                            <p><span class="circular">1</span></p>
                                            <p>Sign in and complete your Professional account profile
                                                in the BMAC account settings.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/get-client-slide.png')}}" alt="get client icon"/>
                                        <div class="desc">
                                            <p><span class="circular">2</span></p>
                                            <p>Get your client to sign up in the BMAC. He should register
                                                as a business owner.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/get-businessowner2invite.png')}}"
                                             alt="get business owner to invite icon"/>
                                        <div class="desc">
                                            <p><span class="circular">3</span></p>
                                            <p>Get him to invite you to his BMAC Business account by using
                                                the collaborator tool. Check your email and verify.

                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/set-client-account.png')}}" alt="get client icon"/>
                                        <div>
                                            <p><span class="circular">4</span></p>
                                            <p>You can now set up your client Chart of Accounts,
                                                Fiscal year.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col1 col-md-4 text-center">
                                        <img src="{{asset('images/interpret-report.png')}}"
                                             alt="get business owner to invite icon"/>
                                        <div>
                                            <p><span class="circular">5</span></p>
                                            <p>You can post transactions, view & interpret various reports as well as
                                                carry out simple analysis for your client.
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="carousel-caption">
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- close overview -->

                <div role="tabpanel" class="tab-pane " id="businesses">

                    <div id="list-business" class="ylist-container">
                        <h4 class="ylist-container-heading">List of Businesses</h4>

                        @if($lists = getCollaboratorCompanies())

                            <ul class="ylist">
                                @foreach($lists as $list)
                                    <li class="ylist-item">
                                        <a href="{{ url('dashboard/change-company?slug='.$list->company->database) }}">
                                            {{--<img src="{{ @$list->company->user->displayAvatar() }}" alt="company logo"/>--}}
                                            <h4 class="ylist-item-heading">{{ $list->company->company_name }}</h4>
                                            <p class="ylist-item-text">
                                                <small class="pull-right">{{ @$list->company->email.' '.@$list->company->phone_no }}</small>
                                            </p>

                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                        @else

                            <div class="panel wrapper panel-success" style="margin: 20px">
                                <p>You currently do not collaborate with any businesses on the BMAC platform.</p>
                                <p>BMAC provides and environment where you can help a business manage its accounting
                                    books......</p>

                            </div>

                        @endif


                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="company-info">

                    <div class="text-center wrapper">
                        @include('profile._partials._companyInfo')
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="settings">


                    <div class="text-center wrapper">
                        @include('profile._partials._personalProfile')
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="dprofile">


                    <div class="text-center wrapper">
                        @include('profile._partials._overview')
                    </div>

                </div>
                <div class="tab-pane" id="profile-pic">
                    <div class="text-center wrapper">
                        <form id="pic-form" method="post" action="{{ url('/profile/change-profile-pic') }}"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <input type="file" name="profilepic"/>
                            <hr>
                            <button type="submit" class="btn btn-primary">Change Profile Pic</button>
                        </form>
                    </div>
                </div>

            </div><!-- close tab-content -->
        </div>
    </section>




@endsection

@section('scripts')
    <script>
        var $baseurl = $('#baseurl').val();
        /////////////////////////////////////////////////////////////////
        ////////////PERSONAL INFORMATION SECTION/////////////////////////
        /////////////////////////////////////////////////////////////////
        $("#personalForm").submit(function (event) {
            event.preventDefault();
            $('#publish-personal').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    //$('body').loader('hide');
                    if (!e.status) {
                        displayNotification(e.message, 'Error', 'error');
                        $('#publish-personal').attr('disabled', true);
                        $('.spinner').hide();
                    } else {
                        displayNotification(e.message, 'Success!', 'success')
                    }
                    $('#publish-personal').attr('disabled', false);
                    $('.spinner').hide();

                },
                error: function (e) {
                    displayNotification(e.message, 'Error!!', 'error');
                    $('#publish-personal').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });

        //submit contact form
        $("#companyForm").submit(function (event) {
            event.preventDefault()
            $('#save_coyInfo').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    //$('body').loader('hide');
                    if (!e.status) {
                        displayNotification(e.message, 'Error', 'error');
                        $('#save_coyInfo').attr('disabled', false);
                        $('.spinner').hide();
                    } else {
                        displayNotification(e.message, 'Success!', 'success')
                    }
                    $('#save_coyInfo').attr('disabled', false);
                    $('.spinner').hide();

                },
                error: function (e) {
                    displayNotification(e.message, 'Error!!', 'error');
                    $('#save_coyInfo').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });
    </script>
@stop
