<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="bsItemsModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5 id="groupName"> </h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="">Ledger Account</th>
                            <th width="">{{ $curr_year }}</th>
                            <th width="">{{ $prev_year }}</th>
                        </tr>
                        </thead>
                        <tbody align="center" id="list-items">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->