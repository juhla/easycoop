<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="bsItemsModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5 id="groupName"> </h5>
                    <p class="p-b-10">Parameters for <strong>Gear Ratio</strong></p>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th width=""></th>
                            <th width="">Amount</th>
                        </tr>
                        </thead>
                        <tbody align="">
                        <tr>
                            <td >Current Year Equity</td>
                            <td id='currYrEquityTotal'></td>
                        </tr>
                        <tr>
                            <td >Long Term Borrowing</td>
                            <td id='longTermBorrowing'></td>
                        <tr>
                        </tr>

                        </tr>
                        <td ><strong>Gear Ratio:</strong>  (Borrowing/Equity)</td>
                        <td id='gearRatio'></td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->