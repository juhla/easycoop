
<tr>
    <td style="font-weight: 800">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $group->code.' - &nbsp;'.$group->name }}</td>
    <td>Group</td>
    <td></td>
    <td></td>
    <td></td>
</tr>
@if($nestedChild = $group->nestedGroups($group->id))
    {!! displayReportItem($nestedChild->get(), 'gl-account-summary') !!}
@endif
@if($group->ledgers->count())
    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a></td>
            <td>Ledger</td>
            <td>
                <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ formatNumber($ledger->op_balance) }}</a>
            </td>
            <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->getClosingBalance() }}</a></td>
            <td>
                <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}"> <i class="fa fa-eye"></i>View</a>
            </td>
        </tr>
    @endforeach
@endif
