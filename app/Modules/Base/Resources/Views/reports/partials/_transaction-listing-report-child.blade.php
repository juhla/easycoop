<tr>
    <td colspan="6" style="font-weight: 800">{{ $group->code.' - &nbsp;'.$group->name }}</td>
</tr>
@if($nestedChild = $group->nestedGroups($group->id))
    {!! displayReportItem($nestedChild->get(), 'transaction-listing', 0, 0, $range) !!}
@endif
@if($group->ledgers->count())
    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
        <thead>
        <tr>
            <th colspan="6"><a
                        href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a>
            </th>
        </tr>
        </thead>

        <?php  $transactions = $ledger->getTransactionsForLedger( $ledger->id, null, $range ) ?>
        <?php
        $op = $ledger->calculateOpeningBalance(null);

        $openingBalance = $op[0];
        $openingBalanceType = $op[1];
       // pr($ledger->name . " >> " . $ledger->opening_balance_type);
       // pr($ledger->name . " >> " . $openingBalanceType);
        ?>
        @if($transactions)
            <tbody>
            <tr>
                <td colspan="5"><strong>Opening Balance</strong></td>
                <td><i><strong>{{ formatNumber( abs( $openingBalance ) ) }}</strong></i></td>
            </tr>

            {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType) !!}
            </tbody>
        @else

        @endif
    @endforeach
@endif