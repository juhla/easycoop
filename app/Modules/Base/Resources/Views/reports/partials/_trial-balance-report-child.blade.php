
<tr>
    <td>{{ $group->code.' '.$group->name }}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <?php  $result = $group->getBalance() ?>
    @if($result['dc'] === 'D')
        <td>{{ formatNumber($result['balance']) }}</td>
        {{-- $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
        <td> - </td>
    @elseif($result['dc'] === 'C')
        <td> - </td>
        <td>{{ formatNumber($result['balance']) }}</td>
        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
    @else
        <td> - </td>
        <td> - </td>
    @endif
</tr>
@if($nestedChild = $group->nestedGroups($group->id))
    {!! displayReportItem($nestedChild->orderBy('code', 'asc')->get(), 'trial-balance', $debitBalance, $creditBalance) !!}
@endif

