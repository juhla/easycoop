@extends('layouts.main')

@section('content')
    <style>
        .pricing_table_wdg {
            border: 1px solid #c4cbcc;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            float: left;
            margin-top: 25px;
        }

        .pricing_table_wdg ul {
            list-style: none;
            float: left;
            width: 147px;
            margin: 0;
            border: 1px solid #f2f3f3;
            padding: 5px;
            text-align: center;
            background-color: #FFF;
        }

        .pricing_table_wdg ul:hover {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            -moz-box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            -webkit-box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            cursor: pointer;
            background: #d8e9f9;
        }

        .pricing_table_wdg ul li {
            border-bottom: 1px dashed #cfd2d2;
            padding: 10px 0;
        }

        .pricing_table_wdg ul li:first-child {
            color: #FFFFFF;
            font-size: 18px;
            font-weight: bold;
            background: #2e818f;
        }

        .pricing_table_wdg ul li:nth-child(2) {
            background: #fbfbfb;
        }

        .pricing_table_wdg ul li:nth-child(3) {
            font-size: 12px;
            font-weight: bold;
        }

        .pricing_table_wdg ul li:nth-child(n+4) {
            font-size: 14px;
        }

        .pricing_table_wdg ul li:last-child a {
            color: #F0F0F0;
            text-decoration: none;
            font-weight: bold;
            display: block;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border: 1px solid #c4cbcc;
            padding: 10px;
            margin: 5px 0;
            background: #0061bb; /* Old browsers */
            background: -moz-linear-gradient(top, #0061bb 0%, #164e82 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #0061bb), color-stop(100%, #164e82)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #0061bb 0%, #164e82 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #0061bb 0%, #164e82 100%); /* Opera11.10+ */
            background: -ms-linear-gradient(top, #0061bb 0%, #164e82 100%); /* IE10+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0061bb', endColorstr='#164e82', GradientType=0); /* IE6-9 */
            background: linear-gradient(top, #0061bb 0%, #164e82 100%); /* W3C */
        }

    </style>
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-12 m-b-xs">
                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">--}}
                            {{--<i class="fa fa-caret-right text fa-lg"></i>--}}
                            {{--<i class="fa fa-caret-left text-active fa-lg"></i>--}}
                            {{--</a>--}}
                            {{--<div class="btn-group">--}}
                            {{--<a href="{{ url('reports/receivables/pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>--}}
                            {{--<a href="{{ url('reports/receivables/xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>--}}
                            {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>--}}
                            {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li><a href="#">Separated link</a></li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                            <div class="btn-group">
                                <a href="{{ url('collaborators/add-new') }}" class="btn btn-sm btn-default"><i
                                            class="fa fa-plus"></i> Add a Collaborator </a> &nbsp; &nbsp;
                                <a href="{{ url('collaborators/list-payroll') }}" class="btn btn-sm btn-default">
                                    Payroll</a>
                                <a href="{{ url('collaborators/list-ims') }}" class="btn btn-sm btn-default"> IMS</a>
                                &nbsp; &nbsp; &nbsp;
                                <a href="{{ url('assemblyline/add-new') }}" class="btn btn-sm btn-default"><i
                                            class="fa fa-plus"></i> Add Assembly Line </a>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')

                    <h1>Assembly Line Plans</h1>
                    <section class="container">

                        <div class="pricing_table_wdg">
                            @if ($license_id === 2)
                                @foreach($assembly_basic_license_plan as $plan)
                                    <ul>
                                        <li>{{ $plan->title }}</li>
                                        <li>{{ ($plan->price === '0') ? 'Free' : formatNumber($plan->price) }}</li>
                                        <li>{{ $plan->structure }} <strong>Monthly</strong></li>


                                        @if($plan->id === $license_id )
                                            <li><a href="#" class="buy_now" disabled>Current Plan</a></li>
                                            {{--@elseif($license_id > $plan->id)--}}
                                            {{--<li><a href="#" class="" disabled>Buy Now</a></li>--}}
                                        @else
                                            <li>
                                                <form action="#" method="post" id="payment-form-{{ $plan->id }}">
                                                    {{ csrf_field() }}
                                                    <script src="https://js.paystack.co/v1/inline.js"></script>
                                                    <a href="javascript:;"
                                                       onclick="payBasicWithPaystack({{ $plan->id }}, {{ $plan->price }})"
                                                       class="buy_now" data-value="{{ $plan->price }}"> Buy Now </a>
                                                </form>
                                            </li>
                                        @endif
                                    </ul>

                                @endforeach
                            @endif
                        </div>

                        <div class="pricing_table_wdg">
                            @if ($license_id === 3)
                                @foreach($assembly_silver_license_plan as $plan)
                                    <ul>
                                        <li>{{ $plan->title }}</li>
                                        <li>{{ ($plan->price === '0') ? 'Free' : formatNumber($plan->price) }}</li>
                                        <li>{{ $plan->structure }} <strong>Monthly</strong></li>


                                        @if($plan->id === $license_id )
                                            <li><a href="#" class="buy_now" disabled>Current Plan</a></li>
                                            {{--@elseif($license_id > $plan->id)--}}
                                            {{--<li><a href="#" class="" disabled>Buy Now</a></li>--}}
                                        @else
                                            <li>
                                                <form action="#" method="post" id="payment-form-{{ $plan->id }}">
                                                    {{ csrf_field() }}
                                                    <script src="https://js.paystack.co/v1/inline.js"></script>
                                                    <a href="javascript:;"
                                                       onclick="paySilverWithPaystack({{ $plan->id }}, {{ $plan->price }})"
                                                       class="buy_now" data-value="{{ $plan->price }}"> Buy Now </a>
                                                </form>
                                            </li>
                                        @endif
                                    </ul>

                                @endforeach
                            @endif
                        </div>

                    </section>


                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>

    <?php if(config("app.env") == "production"): ?>
    @include('settings.paystack.live')
    <?php else: ?>
    @include('settings.paystack.test')
    <?php endif; ?>

@stop

