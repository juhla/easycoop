@extends('layouts.main')

@section('content')
    <section class="vbox">
        <header class="header bg-white b-b b-light"> <p>Settings</p> </header>
        <section class="scrollable">
            <section class="hbox stretch">

                <aside class="bg-white">
                    <section class="vbox">
                        <header class="header bg-light bg-gradient">
                            <ul class="nav nav-tabs nav-white">
                                <li class="active"><a href="#widget-settings" data-toggle="tab">Widget Settings</a></li>
                                <li class=""><a href="#proficiency-level-settings" data-toggle="tab">Proficiency Level Settings</a></li>
                                <li class=""><a href="#notification" data-toggle="tab">Email Preference</a></li>
                                <li class=""><a href="#change-password" data-toggle="tab">Change Password</a></li>
                                <li class=""><a href="#deactivate" data-toggle="tab">Deactivate Account</a></li>
                            </ul>
                        </header>
                        <section class="scrollable">
                            <div class="tab-content">
                                <div class="tab-pane active" id="widget-settings">
                                    @include('settings.workspace.partials._widgets')
                                </div>
                                <div class="tab-pane" id="proficiency-level-settings">
                                    <div class="text-center wrapper">
                                        @include('settings.workspace.partials._proficiency')
                                    </div>
                                </div>
                                <div class="tab-pane active" id="notification">
                                    @include('settings.partials._notifications')
                                </div>
                                <div class="tab-pane" id="change-password">
                                    <div class="text-center wrapper">
                                        @include('settings.partials._change-password')
                                    </div>
                                </div>
                                <div class="tab-pane" id="deactivate">
                                    <div class="text-center wrapper">
                                        @include('settings.partials._deactivate-account')
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </aside>
                {{--<aside class="col-lg-4 b-l">--}}
                    {{--<section class="vbox">--}}
                        {{--<section class="scrollable">--}}
                            {{--<div class="wrapper">--}}
                                {{--<section class="panel panel-default">--}}
                                    {{--<h4 class="font-thin padder">TownHall: Latest Threads</h4>--}}
                                    {{--<ul class="list-group">--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago</small>--}}
                                        {{--</li>--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p>Morbi nec <a href="#" class="text-info">@Jonathan George</a> nunc condimentum ipsum dolor sit amet, consectetur</p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 1 hour ago</small>--}}
                                        {{--</li>--}}
                                        {{--<li class="list-group-item">--}}
                                            {{--<p><a href="#" class="text-info">@Josh Long</a> Vestibulum ullamcorper sodales nisi nec adipiscing elit. </p>--}}
                                            {{--<small class="block text-muted"><i class="fa fa-clock-o"></i> 2 hours ago</small>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</section>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</section>--}}
                {{--</aside>--}}
            </section>
        </section>
    </section>

@endsection
@section('scripts')
    <script>
        var $baseurl = $('#baseurl').val();
        /////////////////////////////////////////////////////////////////
        ////////////PERSONAL INFORMATION SECTION/////////////////////////
        /////////////////////////////////////////////////////////////////
        $("#passwordForm").submit(function(event) {
            event.preventDefault();

            $('#submitPassword').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'notvalid') {
                        displayNotification('Old password is incorrect', 'Error', 'error');
                        $('#submitPassword').attr('disabled', false);
                        $('.spinner').hide();
                    }else if(e.message === 'success'){
                        displayNotification('Your password has been changed', 'Success!', 'success');
                        setTimeout(function(){
                            window.location.href = $baseurl + '/secure/sign-out';
                        }, 3000);
                    }else{
                        displayNotification('Check your fields', 'Error!', 'error');
                        $('#submitPassword').attr('disabled', false);
                        $('.spinner').hide();
                    }

                },
                error: function (e){
                    displayNotification('Something went wrong', 'Error!!', 'error');
                    $('#submitPassword').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });
        $("#roleForm").submit(function (event) {
            event.preventDefault();

            $('#submitRole').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'notvalid') {
                        displayNotification('Error changing roles', 'Error', 'error');
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    } else if (e.message === 'success') {
                        displayNotification('Your role has been changed', 'Success!', 'success');
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    } else {
                        displayNotification('Check your fields', 'Error!', 'error');
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    }

                },
                error: function (e) {
                    displayNotification('Something went wrong', 'Error!!', 'error');
                    $('#submitRole').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });
        $("#widgetForm").submit(function (event) {
            event.preventDefault();

            $('#submitWidget').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'notvalid') {
                        displayNotification('Error editing widgets', 'Error', 'error');
                        $('#submitWidget').attr('disabled', false);
                        $('.spinner').hide();
                    } else if (e.message === 'success') {
                        displayNotification('Your widgets has been updated', 'Success!', 'success');
                        var url = "{{url('workspace')}}";
                            $(location).attr('href',url);
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    } else {
                        displayNotification('Check your fields', 'Error!', 'error');
                        $('#submitWidget').attr('disabled', false);
                        $('.spinner').hide();
                    }

                },
                error: function (e) {
                    displayNotification('Something went wrong', 'Error!!', 'error');
                    $('#submitWidget').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });

        $(document).on("click", "#deactivate-account", function (e) {
            e.preventDefault();
            var $this = $(this);
            swal({
                title:"Are you sure?",
                text:"Your account will be deleted permanently!",
                type:"warning",
                showCancelButton:!0,
                confirmButtonColor:"#DD6B55",
                confirmButtonText:"Yes, Deactivate account!",
                closeOnConfirm:!1
            },function(isConfirm){
                if(isConfirm){
                    $.ajax({
                        type: 'GET',
                        url: $this.attr('href'),
                        context: this,
                        success: function () {
                            swal({
                                title: "Deleted!",
                                text: "Account has been deactivated",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function(isConfirm){
                                if(isConfirm){
                                    window.location.href = $baseurl + '/secure/sign-out';
                                }
                            });
                        }
                    });
                }
            });
        });
    </script>
@stop
