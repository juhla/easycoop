<div class="col-md-12  m-t-md">

    <form class="bs-example form-horizontal" id="passwordForm" action="{{ url('account-settings/change-password') }}" method="post" data-validate="parsley">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-lg-4 control-label">Old Password</label>
            <div class="col-lg-6">
                <input type="password" name="current_password" class="form-control" data-required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-4 control-label">Password</label>
            <div class="col-lg-6">
                <input type="password" class="form-control" name="password" id="password" data-required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-4 control-label">Confirm Password</label>
            <div class="col-lg-6">
                <input type="password" class="form-control" name="password_confirmation" data-equaltto="#password" data-required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" id="submitPassword" class="btn btn-lg btn-primary">
                    <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none" ></i> Save Changes
                </button>
            </div>
        </div>
    </form>
</div>