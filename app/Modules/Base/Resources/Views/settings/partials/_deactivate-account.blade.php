<div class="row">
    <h3>Is this goodbye?</h3>
    <p>Are you sure you don't want to reconsider? How can we improve our services? <a href="#">Tell us here</a></p>
    <div class="col-xs-6 col-xs-offset-3">
        <a href="{{ url('/account-settings/deactivate-account') }}" class="btn btn-danger btn-rounded btn-lg" id="deactivate-account">Deactivate Account</a>
    </div>
</div>