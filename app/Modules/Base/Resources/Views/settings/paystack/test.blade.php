<script>
    const token = '{{ config('services.paystack.secret') }}';
    function payBasicWithPaystack(id, amount) {
        var baseurl = $('#baseurl').val();

        var handler = PaystackPop.setup({
            key: '{{ config('services.paystack.key') }}',
            email: '{{ auth()->user()->email }}',
            amount: (amount * 100),
            ref: "{{ time().GenerateRandomString(10,"ALPHA") }}",
            plan_code: "PLN_87mpfaewfqo9gh5",
            interval: 'monthly',

//                metadata: {
//                    custom_fields: [
//                        {
//                            display_name: "Mobile Number",
//                            variable_name: "mobile_number",
//                            value: "+2348012345678"
//                        }
//                    ]
//                },

            callback: function (response) {
//                    $('#payment-form-' + id).attr('action', baseurl + '/assemblyline/subscribe/' + id + '/plan?reference_no=' + response.reference);
//                    $('#payment-form-' + id).submit();
//                    $('#payment-form-' + id).attr('action', 'https://api.paystack.co/transaction/verify/'  + response.reference);
//                   $('#payment-form-' + id).submit();


//                    fetch('https://api.paystack.co/transaction/verify/'  + response.reference, {
//                        headers: {
//                            Authorization: 'Bearer '+token
//                        }
//                    }).then( res => res.json()).then(
//                        json => {
//                        console.log(json)
//                    }
//                );

                jQuery.ajax({
                    url: 'https://api.paystack.co/transaction/verify/' + response.reference,
                    type: 'GET',
                    data: {content: 'testing test'},
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    success: function (response) {
                        var autht = response.data.authorization.authorization_code
                        $('#payment-form-' + id).attr('action', baseurl + '/assemblyline/subscribe/' + id + '/plan?authorization_code=' + autht);
                        $('#payment-form-' + id).submit();
                    }
                });


            },
//                onClose: function(){
//                    alert('window closed');
//                }
        });
        handler.openIframe();
    }

    function paySilverWithPaystack(id, amount) {
        const token = '{{ config('services.paystack.secret') }}'
        var baseurl = $('#baseurl').val();

        var planD = '{{ config('services.paystack.key') }}';


        var handler = PaystackPop.setup({
            key: '{{ config('services.paystack.key') }}',
            email: '{{ auth()->user()->email }}',
            amount: (amount * 100),
            ref: "{{ time().GenerateRandomString(10,"ALPHA") }}",
            plan_code: "PLN_tr09ji1mxxenskf",
            interval: 'monthly',

//                metadata: {
//                    custom_fields: [
//                        {
//                            display_name: "Mobile Number",
//                            variable_name: "mobile_number",
//                            value: "+2348012345678"
//                        }
//                    ]
//                },
            callback: function (response) {
//                    $('#payment-form-' + id).attr('action', baseurl + '/assemblyline/subscribe/' + id + '/plan?reference_no=' + response.reference);
//                    $('#payment-form-' + id).submit();

                jQuery.ajax({
                    url: 'https://api.paystack.co/transaction/verify/' + response.reference,
                    type: 'GET',
                    data: {content: 'testing test'},
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    success: function (response) {
                        var autht = response.data.authorization.authorization_code
                        $('#payment-form-' + id).attr('action', baseurl + '/assemblyline/subscribe/' + id + '/plan?authorization_code=' + autht);
                        $('#payment-form-' + id).submit();
                    }
                });

            },
//                onClose: function(){
//                    alert('window closed');
//                }
        });
        handler.openIframe();
    }
</script>