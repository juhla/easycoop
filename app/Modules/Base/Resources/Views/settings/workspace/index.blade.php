@extends('layouts.main')

@section('content')
    <section class="vbox">
        <header class="header bg-white b-b b-light"><p>Workspace Settings</p></header>
        <section class="scrollable">
            <section class="hbox stretch">

                <aside class="bg-white">
                    <section class="vbox">
                        <header class="header bg-light bg-gradient">
                            <ul class="nav nav-tabs nav-white">
                                <li class="active"><a href="#widget-settings" data-toggle="tab">Widget Settings</a></li>
                                <li class=""><a href="#proficiency-level-settings" data-toggle="tab">Proficiency Level Settings</a></li>
                            </ul>
                        </header>
                        <section class="scrollable">
                            <div class="tab-content">
                                <div class="tab-pane active" id="widget-settings">
                                    @include('settings.workspace.partials._widgets')
                                </div>
                                <div class="tab-pane" id="proficiency-level-settings">
                                    <div class="text-center wrapper">
                                        @include('settings.workspace.partials._proficiency')
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </aside>
                <aside class="col-lg-4 b-l">
                    <section class="vbox">
                        <section class="scrollable">
                            <div class="wrapper">
                                <section class="panel panel-default">
                                    <h4 class="font-thin padder">TownHall: Latest Threads</h4>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web
                                                application template, have fun1 </p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago
                                            </small>
                                        </li>
                                        <li class="list-group-item">
                                            <p>Morbi nec <a href="#" class="text-info">@Jonathan George</a> nunc
                                                condimentum ipsum dolor sit amet, consectetur</p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 1 hour ago
                                            </small>
                                        </li>
                                        <li class="list-group-item">
                                            <p><a href="#" class="text-info">@Josh Long</a> Vestibulum ullamcorper
                                                sodales nisi nec adipiscing elit. </p>
                                            <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 hours ago
                                            </small>
                                        </li>
                                    </ul>
                                </section>
                            </div>
                        </section>
                    </section>
                </aside>
            </section>
        </section>
    </section>

@endsection
@section('scripts')
    <script>
        var $baseurl = $('#baseurl').val();
        $("#roleForm").submit(function (event) {
            event.preventDefault();

            $('#submitRole').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'notvalid') {
                        displayNotification('Error changing roles', 'Error', 'error');
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    } else if (e.message === 'success') {
                        displayNotification('Your role has been changed', 'Success!', 'success');
                    } else {
                        displayNotification('Check your fields', 'Error!', 'error');
                        $('#submitRole').attr('disabled', false);
                        $('.spinner').hide();
                    }

                },
                error: function (e) {
                    displayNotification('Something went wrong', 'Error!!', 'error');
                    $('#submitRole').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });
        $("#widgetForm").submit(function (event) {
            event.preventDefault();

            $('#submitWidget').attr('disabled', true);
            $('.spinner').show();
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                type: "POST",
                dataType: 'json',
                success: function (e) {
                    if (e.message === 'notvalid') {
                        displayNotification('Error editing widgets', 'Error', 'error');
                        $('#submitWidget').attr('disabled', false);
                        $('.spinner').hide();
                    } else if (e.message === 'success') {
                        displayNotification('Your widgets has been updgated', 'Success!', 'success');

                    } else {
                        displayNotification('Check your fields', 'Error!', 'error');
                        $('#submitWidget').attr('disabled', false);
                        $('.spinner').hide();
                    }

                },
                error: function (e) {
                    displayNotification('Something went wrong', 'Error!!', 'error');
                    $('#submitWidget').attr('disabled', false);
                    $('.spinner').hide();
                }
            });
        });
    </script>
@stop
