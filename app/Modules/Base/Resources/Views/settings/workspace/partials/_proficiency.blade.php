<div class="col-md-12  m-t-md">

    <form class="bs-example form-horizontal" id="roleForm" action="{{ url('workspace-settings/change-proficiency') }}"
          method="post" data-validate="parsley">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-lg-4 control-label">Proficiency Level</label>
            <div class="col-lg-6">
                <?php foreach ($business_owner as $eachrole): ?>
                <?php
                $user = auth()->user();
                $selected = "";
                if ($user->hasRole($eachrole['name'])) {
                    $selected = "checked";
                }
                ?>
                <input <?php echo $selected ?> class="form-control" type="radio" name="role" value="<?php echo $eachrole['id'] ?>"> <?php echo $eachrole['display_name'] ?><br>
                <?php endforeach ;?>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" id="submitRole" class="btn btn-lg btn-primary">
                        <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none"></i> Save Changes
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>