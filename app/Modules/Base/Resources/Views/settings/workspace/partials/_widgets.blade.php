<div class="col-md-12  m-t-md">

    <form class="bs-example form-horizontal" id="widgetForm" action="{{ url('workspace-settings/change-widgets') }}"
          method="post" data-validate="parsley">
        {{ csrf_field() }}
        <div class="form-group">
            <?php if($has_user_widget): ?>
            <label class="col-lg-4 control-label">Widgets</label>
            <div class="col-lg-6">
                <?php foreach ($category_widget as $eachrole): ?>
                <?php
                $selected = "";
                if (in_array($eachrole, $user_widget)) {
                    $selected = "checked";
                }
                ?>
                <div class="item">
                    <input <?php echo $selected ?> class="form-control" type="checkbox" name="widgets[]"
                           value="<?php echo $eachrole['id'] ?>"> <?php echo $eachrole['name'] ?><br>
                </div>

                <?php endforeach ;?>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" id="submitWidget" class="btn btn-lg btn-primary">
                        <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display: none"></i> Save Changes
                    </button>
                </div>
            </div>
            <?php else: ?>
            <h5><label class="col-lg-4 control-label">Widgets cannot be customized for your account </label></h5>
            <?php endif; ?>
        </div>
    </form>
</div>