<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>One Bmac, Accounting Platform</title>

    <!-- Bootstrap CSS --><link href="{{ asset('newui/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ asset('newui/css/bootstrap-theme.css')}}" rel="stylesheet">
   
    <!-- Custom styles -->
    <link rel="stylesheet" href="{{ asset('asset/css/fullcalendar.css')}}">
    <link href="{{ asset('homepage/asset/css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('homepage/asset/css/style-responsive.css')}}" rel="stylesheet"/>
    <link href="{{ asset('homepage/asset/css/mystyle.css')}}" rel="stylesheet">
    <link href="{{ asset('newui/css/style.css')}}" rel="stylesheet">


</head>
<body class="body-wrapper">

     <!-- preloader starts here -->
    
    <!-- preloader ends here -->

<!-- Signup page start here  -->
<section>
        <div class="container-fluid cf-r1">
            <div class="row">
                <div class="col-md-6 col-lg-5 acc-x1">
                    <img src="{{ asset('newui/images/accountant3.png')}}" alt="image" class="img-responsive">
                    <div class="acc-x2">
                        <h2 class="bmac-r1">BMAC Accounting</h2>
                        <p class="bmac-txt">Collaborative accounting software for business</p>
                        <div class="btn-acc">
                            <a href="{{url('secure/sign-in')}}"><button class="btn btn-more">More</button></a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-7 mp-x1">
                    <div class="sign-x1">
                    <a href="{{url('/')}}"><img src="{{asset('newui/images/bmac-logo.png')}}" alt="bmac-logo" class="img-responsive"/></a>
                    </div>
                    <div class="sign-p1">
                        <div class="inside-x1">

                    <div id="validationMessages">
                    </div>

                    <div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                    </div>

                            <form class="form-wrap" action="{{ route('auth.ajax_register') }}" id="signupForms" class="" method="post"
                          data-validate="parsley">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-x1" id="first name" placeholder="First Name" name="first_name"
                                                data-required="true"
                                                value="{{ old('first_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="text" placeholder="Last Name" name="last_name" class="form-control form-x1"
                                                data-required="true"
                                                value="{{ old('last_name') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                    <!-- <label class="container-label">Tell us who you are</label> -->
                                        <select name="role_id" id="role_id" class="form-control form-x2">
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="text" placeholder="Company Name" name="company_name" class="form-control form-x1"
                                                data-required="true"
                                                value="{{ old('company_name') }}">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="text" placeholder="RC Number" name="rc_number" class="form-control form-x1"
                                                data-required="true"
                                                value="{{ old('rc_number') }}"> 
                                     </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="email" placeholder="email" name="email" class="form-control form-x1"
                                                data-required="true"
                                                data-type="email"
                                                value="{{ $email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="text" placeholder="Phone Number" name="phone_no" class="form-control form-x1"
                                                data-required="true"
                                                value="{{ old('phone_no') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="password" placeholder="Password" id="password" name="password"
                                                class="form-control form-x1"
                                                data-required="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <input type="password" placeholder="Confirm Password" name="password_confirmation"
                                            class="form-control form-x1"
                                            data-required="true"
                                            data-equalto="#password">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                        <label> <input type="checkbox" name="terms" data-required="true"> Agree the
                                            <a data-toggle="modal" data-target="#myModal1">terms and
                                                policy</a>
                                        </label>
                                    </div>
                                    </div>
                                </div>
                                <?php
                                if(config("app.env") == "production"): ?>
                                <div>
                                    {!! Recaptcha::render() !!}
                                </div>
                                <?php endif; ?>

                                <br>
                                <div class="btn-up">
                                     <button type="submit" id="signupBtn" class="btn btn-signup">Sign up</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    
                    <div class="acct-down">
                        <span>You Already have an account?</span><a href="{{url('secure/sign-in')}}"><button class="btn btn-in">Sign in</button></a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- Sign-up page ends from here -->

<div class="container-fluid">
    <div class="row footer-poweredby">
        <div class="col-md-12 center-block text-center"> 
        Powered by <a href="http://ikooba.com" target="_blank" class="whitepower">Ikooba Technologies</a> &nbsp;&copy; 2018 BMAC
        </div>
    </div>
    
</div>

@include('auth.partials.terms')


<!-- <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false" style="margin-left:45%">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="row"> -->

                <!-- <div class="col-md-6 col-md-offset-3 top-space">
                    <div class="cssload-dots">
                        <div class="cssload-dot"></div>
                        <div class="cssload-dot"></div>
                        <div class="cssload-dot"></div>
                        <div class="cssload-dot"></div>
                        <div class="cssload-dot"></div>
                    </div>

                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <defs>
                            <filter id="goo">
                                <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12"></feGaussianBlur>
                                <feColorMatrix in="blur" mode="matrix"
                                               values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7"
                                               result="goo"></feColorMatrix> -->
                                <!--<feBlend in2="goo" in="SourceGraphic" result="mix" ></feBlend>-->
                            <!-- </filter>
                        </defs>
                    </svg>

                </div> -->

<!--              
            <div class="col-md-6 col-md-offset-3 top-space"> 
                         <div id="loading-page">
                            <div id="loading-center-page">
                                <div id="loading-center-absolute">

                                    <div class="loader">
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                    </div>

                                </div>
                            </div>

                        <div class="col-md-6 col-md-offset-3">
                            <center class="sign-message">
                                <p class="wait-text">Please wait while we setup your account...</p>
                            </center>
                        </div>
                    </div>

             </div>

            </div>
        </div>
    </div>
</div> -->

<div class="modal" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
       <div class="modal-content">            
           <div id="loading-page">
               <div id="loading-center-page">
                   <div id="loading-center-absolute">                       
                        <div class="loader">
                           <div class="box"></div>
                           <div class="box"></div>
                           <div class="box"></div>
                       </div>                 
                    </div>
               </div>
           </div>       
         </div>
   </div>

<style>
    @media screen and (min-width: 768px) {
        .modal:before {
            display: inline-block;
            vertical-align: middle;
            content: " ";
            height: 100%;
        }
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }

</style>
<!-- javascripts -->

<script src="{{ asset('newui/js/jquery-3.3.1.min.js')}}"></script>

<!-- bootstrap -->
<script src="{{ asset('newui/js/bootstrap.min.js')}}" rel="stylesheet"></script>

<!--custome script for all page-->
{{--<script src="{{ asset('js/scripts.js')}}"></script>--}}
{{--<script src="{{ asset('newui/js/main.js')}}"></script>--}}
<!-- custom script for this page-->


<script>

    //knob
    // $(function () {
    //     $(".knob").knob({
    //         'draw': function () {
    //             $(this.i).val(this.cv + '%')
    //         }
    //     })
    // });


    /* ---------- Map ---------- */
    // $(function () {
    //     $('#map').vectorMap({
    //         map: 'world_mill_en',
    //         series: {
    //             regions: [{
    //                 values: gdpData,
    //                 scale: ['#000', '#000'],
    //                 normalizeFunction: 'polynomial'
    //             }]
    //         },
    //         backgroundColor: '#eef3f7',
    //         onLabelShow: function (e, el, code) {
    //             el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
    //         }
    //     });
    // });


</script>

{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
<!-- bootstrap -->
<script src="{{ asset('newui/js/bootstrap.min.js')}}" rel="stylesheet"></script>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('css/touch.css') }}" type="text/css"/>

<script type="text/javascript">
    $('#last-image').height($(window).height());
</script>
<?php $success = URL("secure/success-page");?>

<script>


    function addRow(message) {
        var div = document.createElement('div');
        div.className = 'alert alert-danger';
        $(div).append("<ul>");
        $.each(message, function (index, element) {
            for (var key in element)
                $(div).append("<li>" + element[key] + "</li>");
        });
        $(div).append("</ul>");
        //$("#validationMessages").appendChild(div);
        document.getElementById('validationMessages').appendChild(div);
    }

    function addMessage(message) {
        var div = document.createElement('div');
        div.className = 'alert alert-danger';
        $(div).append("<p>" + message + "</p>");
        document.getElementById('validationMessages').appendChild(div);
    }

    function removeRow() {
        $('#validationMessages').children().remove();
        // document.getElementById('validationMessages').removeChild(input.parentNode);
    }

    function hideDiv() {
        $('#myModal').modal('hide');
        $('#signupBtn').attr('disabled', false);
    }

    $(document).on("submit", "#signupForms", function (event) {
        event.preventDefault();
        removeRow();
        $('#signupBtn').attr('disabled', true);
        $('#signupBtn').text('Please wait.....');
        var url = $(this).attr('action');
        var data = $(this).serialize();
        loadingScreen();
        $('#myModal').modal('show');
        $.ajax({
            url: url,
            data: data,
            type: "GET",
            dataType: 'json',
            success: function (e) {


                $('#signupBtn').text('Sign up');

                if (e.type === 'validator_fails') {
                    hideDiv();
                    addRow(e.message);
                } else if (e.type === 'email_fails') {
                    hideDiv();
                    addMessage("Email sending failed");
                    //displayNotification('Email sending failed', 'Error!', 'error');
                } else if (e.type === 'fail') {
                    hideDiv();
                    addMessage(e.message);
                }
                else if (e.type === 'success') {
                    window.location.replace("<?php echo $success ?>");
                }
            },
            error: function (e) {
                if (e.status === 200) {
                    window.location.replace("<?php echo $success ?>");
                }
                hideDiv();
                addMessage("Error !, something went wrong");
            }
        });
    });


    var loadingScreen = function () {
        var current = 1, current_step, next_step, steps;
        steps = $("fieldset").length;
        $(".next").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function () {
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);

        // Change progress bar action
        function setProgressBar(curStep) {
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width", percent + "%")
                .html(percent + "%");
        }
    };

</script>
</body>
</html>
