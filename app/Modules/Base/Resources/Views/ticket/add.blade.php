@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/receivables/pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/receivables/xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                <button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>--}}
                            <a href="{{ url('ticket/list') }}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back to List </a>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">

                    <div class="row">
                        <div class="col-sm-6">
                            @include('flash::message')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <section class="panel panel-default">
                                <header class="panel-heading font-bold">Add New Ticket </header>
                                <div class="panel-body">
                                    {{--{!! showMessage() !!}--}}
                                    {!! Form::open(['route' => 'ticket.store','role' => 'form', 'class'=>'ticket-form']) !!}
                                    @include('ticket._form')
                                    {!! Form::close() !!}
                                </div>
                            </section>
                        </div>
                        <div class="col-md-6">
                            <!-- START PANEL -->
                            <div class="panel panel-default">
                                <blockquote>
                                    <p>
                                        Tickets are issued reported by the users. It can be well managed if more details like department the issue is related,
                                        type of issue, priority of issue etc are available. This module helps to track the issues and its resolution. Staff can
                                        edit/reply to the ticket. Once a ticket is generated, estimated response time & resolution time is set as per the business hour
                                        and service time set in the configuration module. This response time & resolution time is visible to the client and admin so that
                                        it can be tracked accordingly.
                                    </p>
                                </blockquote>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>
                </section>
                <footer class="footer bg-white b-t">
                    {{--<div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>--}}
                </footer>
            </section>
        </aside>
    </section>
@stop
