@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('ticket/add-new') }}" class="btn btn-info"><i class="fa fa-plus"></i> Add a
                                Ticket </a>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="box-info">
                            @include('common.datatable',['col_heads' => $col_heads])
                        </div>
                    </section>
                </section>

            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        jQuery(document).on("click", ".alert_delete", function (e) {
            var link = jQuery(this).attr("href");
            e.preventDefault();
            bootbox.confirm("Deleting this entirty will result in deleting all linked data with it. It cannot be undone. Are you sure want to proceed? ", function (result) {
                if (result) {
                    document.location.href = link;
                }
            });
        });


        jQuery(document).on("click", "[data-submit-confirm-text]", function (e) {
            var $el = $(this);
            e.preventDefault();
            var confirmText = 'Deleting this entirty will result in deleting all linked data with it. It cannot be undone. Are you sure want to proceed? ';
            bootbox.confirm(confirmText, function (result) {
                if (result) {
                    $el.closest('form').submit();
                }
            });
        });
    </script>
@stop

