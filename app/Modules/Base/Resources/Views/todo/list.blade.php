@extends('layouts.main')

@section('content')
    <?php
    // dd(\App\Modules\Base\Models\Messenger\MessageId::find(1));
    ?>
    <section class="hbox stretch">
        <aside>
            <section class="vbox">

                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="todolist not-done">
                                    {!! Form::open(['url' => 'todo-store', 'method' => 'POST']) !!}
                                        <h1>Create Event</h1>
                                        <input type="text" name="todo" class="form-control add-todo" placeholder="Please enter an item">
                                        <br>
                                        <button id="checkAll" class="btn btn-success">Save</button>
                                    {!! Form::close() !!}
                                    <hr>
                                    <div class="todo-footer">
                                        <strong><span class="count-todos"></span></strong> To Do Lists
                                    </div>
                                    <section class="panel panel-default">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <th width="60%">To Do Lists</th>
                                                    <th width="20%">Status</th>
                                                    <th width="20%"></th>
                                                </tr>
                                                </thead>

                                                @if(!$todo_list->isEmpty())
                                                    <tbody>

                                                    @if(count($todo_list) > 0)
                                                        @foreach($todo_list as $value)
                                                            @if($value->status == 0)
                                                                <tr>
                                                                    <td>{{ str_limit($value->todo, $limit = 50, $end = '...') }}</td>
                                                                    <td> <span class='label label-warning'>Pending</span></td>
                                                                    <td>
                                                                        <a href="{{ url('todo-update/'.$value->id) }}"><span class='label label-success'>Change</span></a>
                                                                    </td>

                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6">You have not added any item.</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                @else
                                                    <p>No records yet</p>
                                                @endif

                                            </table>
                                        </div>
                                    </section>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="todolist">
                                    <h1>Done</h1>
                                    <ul id="done-items" class="list-unstyled">
                                        <li>Some item <button class="remove-item btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-remove"></span></button></li>

                                    </ul>
                                    <section class="panel panel-default">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-condensed">
                                                <thead>
                                                <tr>
                                                    <th width="2%"></th>
                                                    <th width="70%">To Do Lists</th>
                                                    <th width="28%"></th>
                                                    <th></th>
                                                </tr>
                                                </thead>

                                                @if(!$todo_list->isEmpty())
                                                    <tbody>
                                                    @if(count($todo_list) > 0)
                                                        <?php $counter=1; ?>
                                                        @foreach($todo_list as $value)
                                                            @if($value->status == 1)
                                                                <tr>
                                                                    <td>{{ $counter++ }}</td>
                                                                    <td>{{ str_limit($value->todo, $limit = 50, $end = '...') }}</td>
                                                                    <td>
                                                                        <a href="{{ url('todo-delete/'.$value->id) }}"><span class='label label-danger'>Delete</span></a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="6">You have not completed any item.</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                @else
                                                    <p>No records yet</p>
                                                @endif

                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <footer class="footer bg-white b-t">

                </footer>
            </section>
        </aside>
    </section>
@stop
