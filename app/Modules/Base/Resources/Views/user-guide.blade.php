<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>User Guide : ikOOba Technologies</title>
    <link rel="shortcut icon" href="{{ asset('images/fav.png') }}" type="image/x-icon">
    <link rel="alternate" type="application/rss+xml" title="egrappler.com" href="feed/index.html">
    <link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet"
          type="text/css">
    <link rel="stylesheet" href="css/user-guide/style.css">
    <link rel="stylesheet" href="css/user-guide/prettify.css">
</head>
<body data-post="http://www.egrappler.com/free-product-documentation-template/">
<nav>
    <div class="container">

        <?php if(auth()->user()): ?>
        <a href="{{ url('/workspace') }}" class="navbar-brand">
            <?php else: ?>
            <a href="{{ url('/') }}" class="navbar-brand">
                <?php endif; ?>
                <img src="{{ asset('images/ikooba_white.png') }}" class="m-r-sm">
            </a>

    </div>
</nav>
<header>
    <div class="container">
        <h2 class="docs-header"> User Guide</h2>
    </div>
</header>
<section>
    <div class="container">
        <ul class="docs-nav" id="menu-left">
            <li><strong>Getting Started</strong></li>
            <li><a href="#introduction" class=" ">Introduction</a></li>
            <li><a href="#access" class=" ">Accessing BMAC </a></li>
            <li><a href="#signing-up" class=" ">Sign Up Page</a></li>
            <li><a href="#sign-in" class=" ">Sign in </a></li>
            <li class="separator"></li>

            <li><strong>Accountants </strong></li>
            <li><a href="#points-to-note" class=" "> A few points to note </a></li>
            <li class="separator"></li>


            <li><strong>Business set-up </strong></li>
            <li><a href="#profile-set-up" class=" ">Profile Set-up</a></li>
            <li><a href="#fiscal_year" class=" ">Fiscal year</a></li>
            <li><a href="#chart_of_accounts" class=" "> Chart of Accounts </a></li>
            <li><a href="#vendor_setup" class=" "> Vendor’s (or your supplier) Setup </a></li>
            <li><a href="#customers_setup" class=" "> Customers Setup </a></li>
            <li><a href="#bank-Set-up" class=" "> Bank Set-up </a></li>
            <li class="separator"></li>
            <li><strong>Notebook </strong></li>
            <li><a href="#creating-a-transaction" class=" "> Creating a transaction </a></li>
            <li><a href="#modify-a-transaction" class=" "> Modify a transaction </a></li>
            <li><a href="#query-transactions" class=" "> Query transactions </a></li>
            <li class="separator"></li>
            <li><strong>Transaction management </strong></li>
            <li><a href="#tm-creating-a-transaction" class=" "> Creating a transaction </a></li>
            <li><a href="#tm-modify-a-transaction" class=" "> Modify a transaction </a></li>
            <li class="separator"></li>
            <li><strong>Workstation </strong></li>
            <li><a href="#workstation-setup" class=" "> Workstation Setup </a></li>
            <li><a href="#workstation-customisation" class=" "> Workstation Customisation </a></li>
            <li><a href="#view-workstation-items" class=" "> View workstation Items </a></li>
            <li class="separator"></li>
            <li><strong>Dashboard </strong></li>
            <li><a href="#dashboard-set-up" class=" "> Dashboard set-up </a></li>
            <li><a href="#dashboard-customisation" class=" "> Dashboard customisation </a></li>
            <li><a href="#view-dashboard-items" class=" "> View Dashboard items </a></li>
            <li class="separator"></li>
            <li><strong>Collaboration </strong></li>
            <li><a href="#sending-collaborative-invites" class=" "> Sending collaborative invites </a></li>
            <li><a href="#manage-permission-col" class=" "> Manage permission </a></li>
            <li class="separator"></li>
            <li><strong>Reports </strong></li>
            <li><a href="#generate-reports" class=" "> Generate reports </a></li>
            <li><a href="#view-reports" class=" "> View Reports </a></li>
            <li><a href="#print-report" class=" "> Print Report </a></li>
            <li><a href="#search-reports" class=" "> Search Reports </a></li>
            <li><a href="#export-reports" class=" "> Export Reports </a></li>
            <li class="separator"></li>


            <li><strong>Tutorial </strong></li>
            <li><a href="#ikOOba-youtube-channel" class=" "> ikOOba youtube Channel </a></li>
            <li class="separator"></li>
            <li><strong>Support </strong></li>
            <li><a href="#ikOOba-online-support" class=" "> ikOOba Online Support </a></li>
        </ul>
        <div class="docs-content">
            <h2> Getting Started</h2>
            <h3 id="introduction"> Introduction</h3>
            <p>
                BMAC© (“BMAC”) is a cloud accounting solution that helps businesses keep accurate and informative
                accounting records.
                It is also a platform for enterprise to collaborate by enabling direct access to Finance Providers,
                Trade promoters and Professionals. BMAC features functionalities common to most professional accounting
                solutions - such as basic accounting, book-keeping, general ledger and accounting reports to name a few.
                However, its distinguishing feature is the ability to bring various players together around business via
                its Collaborative module.
            </p>
            <p>
                The platform also enables business owners keep their books so that they can measure and control business
                performance.
                Record keeping and transaction management are problems often faced by Small and Medium Scale enterprises
                in
                Nigeria often hindering them from accessing new finance and ultimately failing to achieving their full
                potential.
                BMAC is designed to solve this problem because it is very simple to use and user-friendly.
            </p>
            <p>
                This is a simple user guide meant to provide insights into how the BMAC works and how the get the most
                out of it.
            </p>

            <h3 id="access"> Access </h3>
            <p>To access the BMAC solution, you will need to visit the website: www.ikooba.com. Click on “sign up” at
                the top
                right hand side of the website or insert your e-mail address in the field right above the button
                “Start Now” to get to sign up page.
            </p>
            <p><img src="{{ asset('images/user-guide/access.png') }}"></p>

            <h3 id="signing-up"> Sign up page </h3>
            <p>Fill in all the compulsory fields on this page ranging from full name to business name, phone number and
                password. </p>
            <p>Then Click on sign up.</p>
            <p><img src="{{ asset('images/user-guide/signup-page.png') }}"></p>
            <h4><strong>a) Verification e-mail</strong></h4>
            <ul>
                <li>After clicking the sign up button, you should get a notification email from us at ikOOba
                    technologies, containing verification instructions. This is an indication that your account has been
                    set up on BMAC.
                </li>
                <li>To verify your new account, check your inbox for the verification email. If you do not see the
                    email, please check your spam or junk folders.
                </li>
                <li>When you find the email, open it and click the link as directed by the email to activate the account
                    by login in to your mail and clicking on the “activate account” button.
                </li>
            </ul>
            <p><img src="{{ asset('images/user-guide/verify-message.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/check-mail.png') }}"></p>


            <h3 id="sign-in"> Sign in </h3>
            <p>Fill in the two available fields on the Sign In page (Email and Password) and click on the sign in button
                to get into the system.</p>
            <p><img src="{{ asset('images/user-guide/sign-in.png') }}"></p>
            <hr>


            <h2> Business set-up</h2>
            <p> Before you go into the system fully, you need to set up your business profile and account settings in
                order to properly operate the account.</p>

            <h3 id="points-to-note"> A few points to note</h3>
            <div>

                <p>1. The view you see is dependent on what you registered on, either as a Professional (Accountant)
                    that you are or a business owner that you might be too.
                    You can only use one email for each. </p>

                <p>2. The business owner work station has the full array of our bouquet including our premium packages
                    of
                    "Inventory management system and HR Payroll" </p>

                <p>3. From the business owner work station you have the collaborative tool for the business owner to
                    invite an Accountant. It is the business owner that invites the Accountant.</p>

                <p>4. The business owner records their transactions in the notebook feature (a simple tool on
                    the platform for people with little or no accounting knowledge) for the Accountant to see and
                    post into the relevant books.</p>

                <p>5. The Accountant has all the Accounting books as his/her tool to post/audit and afterwards
                    provide his clients with professional and pragmatic steps forward towards profit optimization in
                    their businesses.</p>

                <p>6. It comes at no cost to the Accountant. Would you like to partner with us and get paid?
                    Email <a href="mailto:tobusiness@ikooba.com">business@ikooba.com</a></p>

                <p>7. You can serve more clients faster, better and more professionally here on BMAC</p>


            </div>


            <h3 id="profile-set-up"> Profile set-up </h3>
            <ul>
                <li>Step 1: Business profile of the company will be set up here. The country, Currency, type of business
                    and the business address are to be set up here.
                </li>
                <li>Step 2: Here you set up the proficiency level of the user. The options are
                    <ol type="i">
                        <li>Elementary</li>
                        <li>Intermediary</li>
                        <li>Advanced</li>
                    </ol>
                </li>
            </ul>
            <p><img src="{{ asset('images/user-guide/profile-set1.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/profile-set2.png') }}"></p>


            <h3 id="fiscal_year"> Fiscal year </h3>
            <p>This is a twelve (12) month period a company chooses for accounting purposes and preparing financial
                statements. The Company accounting year needs to be set up and this may not be the same as 12 months
                calendar year (i.e. January – December).</p>
            <p><img src="{{ asset('images/user-guide/fiscal-year.png') }}"></p>


            <h3 id="chart_of_accounts"> Chart of Accounts </h3>
            <p>This is a listing of the names of the accounts that a company has identified and made available for
                recording transactions in its general ledger. BMAC has a predefined Chart of Accounts but the ledger
                accounts can be modified to suit your business type and requirements.</p>
            <p><img src="{{ asset('images/user-guide/chart-of-accounts.png') }}"></p>


            <h3 id="vendor_setup"> Vendor’s (or your supplier) Setup </h3>
            <p>Before commencing business operation, you need to set up, profile of business’s vendors. Fill up all the
                fields (or the asterisks fields) available on this page from the Vendor’s name to their Email, Country,
                Address and phone numbers.</p>
            <p><img src="{{ asset('images/user-guide/vendor-setup.png') }}"></p>

            <h3 id="customers_setup"> Customers Setup </h3>
            <p>Before you enter transactions between your company your customers into BMAC, you first need to set up the
                customer in the customer profile page. Complete the fields on this page from the Vendor’s name to their
                Email, Country, Address and phone numbers. Fields with asterisks are compulsory fields</p>
            <p><img src="{{ asset('images/user-guide/Customers-Setup.png') }}"></p>


            <h3 id="bank-Set-up"> Bank Set-up </h3>
            <p>Your bank account profiles should be setup and added on the BMAC. You can do the bank set up through
                the </p>
            <ul>
                <ol type="i">
                    <li>chart of accounts or</li>
                    <li>through this sub-module under the business setup.</li>
                </ol>
            </ul>
            <p><img src="{{ asset('images/user-guide/bank-Set-up.png') }}"></p>
            <hr>


            <h2> Notebook </h2>
            <p>BMAC considers business owners with little or no accounting knowledge. We designed the Notebook to enable
                business owners with little or no accounting knowledge record their daily transactions of their
                business. Subsequently their skilled Accounts assistant or Professional accountants can then log into
                BMAC and post your records to relevant ledger accounts.</p>

            <h3 id="creating-a-transaction"> Creating a transaction </h3>
            <p>Recording of transactions into the Notebook is easy and straight forward as no expertise of debit and
                credit is need. Enter transactions in the available fields of Date, Description, amount and click on
                save.</p>
            <p><img src="{{ asset('images/user-guide/creating-a-transaction.png') }}"></p>

            <h3 id="modify-a-transaction"> Modify a transaction </h3>
            <p>Transactions recorded through the Notebook can be edited and deleted depending on the modification. To
                edit, click on the edit icon on the recorded transaction listing and the transaction will be
                recalled.</p>

            <h3 id="query-transactions"> Query transactions </h3>
            <p>Recording of transactions into the Notebook is easy and straight forward as no expertise of debit and
                credit is need. Enter transactions in the available fields of Date, Description, amount and click on
                save.</p>
            <p><img src="{{ asset('images/user-guide/creating-a-transaction.png') }}"></p>
            <hr>


            <h2> Transaction Management </h2>
            <p> Transactions of a business can be manage on BMAC through the Core accounting modules.</p>

            <h3 id="tm-creating-a-transaction"> Creating a transaction </h3>
            <p> Transactions can be posted through the core accounting modules which instantly update the general
                ledger. Transactions are posted through the following modules; Receipts, Payments, Invoices and Journal
                entry. Any of these modules updates the general ledger through debit and credit of different relevant
                accounts.</p>
            <ul>
                <ol type="i">
                    <li>
                        <strong>Receipts</strong> - This can be used to post fund received by a business as a result of
                        its operating activities, investment activities and financing activities on BMAC i.e. Cash
                        receipt and cheque receipt from customers either for fresh transactions or settlement of
                        outstanding.
                        <p><img src="{{ asset('images/user-guide/tm-Receipts.png') }}"></p>
                    </li>
                    <li>
                        <strong>Payments</strong> - This is a module on BMAC used to post payments by a business as a
                        result of its operating activities, investment activities and financing activities i.e. Cash
                        payment and cheque payment
                        <p><img src="{{ asset('images/user-guide/tm-Payments.png') }}"></p>
                    </li>
                    <li>
                        <strong>Invoice</strong> - The Invoice is another module in BMAC used to post itemized
                        transaction between business owners and customers. It specifies the terms of the transaction,
                        and provides information on the available means of payment.
                        <p><img src="{{ asset('images/user-guide/tm-Invoice.png') }}"></p>
                    </li>
                    <li>
                        <strong>Journal Entry</strong> - The Journal in the BMAC provides an audit trail and a means of
                        analysing the effects of the transactions on financial position of your business
                        <p><img src="{{ asset('images/user-guide/tm-Journal-Entry.png') }}"></p>
                    </li>
                </ol>
            </ul>


            <h3 id="tm-modify-a-transaction"> Modify a transaction</h3>
            <p> Transactions posted through the core accounting module can be edited and deleted depending on the
                modification.</p>
            <ul>
                <li>To edit, click on the edit icon on the posted transaction listing and the transaction will be
                    recalled.
                </li>
                <li>To delete, click on the modification icon, then on delete as shown below.
                    <p><img src="{{ asset('images/user-guide/tm-Journal-Entry.png') }}"></p>
                </li>
            </ul>

            <ol type="i">
                <li>
                    <strong>Receipts</strong>
                    <p><img src="{{ asset('images/user-guide/tm-mt-Receipts.png') }}"></p>
                </li>
                <li>
                    <strong>Payments</strong>
                    <p><img src="{{ asset('images/user-guide/tm-mt-Payments.png') }}"></p>
                </li>
                <li>
                    <strong>Invoice</strong>
                    <p><img src="{{ asset('images/user-guide/tm-mt-Invoice.png') }}"></p>
                </li>
                <li>
                    <strong>Journal entry</strong>
                    <p><img src="{{ asset('images/user-guide/tm-mt-Journal-entry.png') }}"></p>
                </li>
            </ol>
            <hr>


            <h2> Workstation </h2>
            <p>This is a space on the BMAC containing access to various working tools. It is the landing page on the
                BMAC when you sign in (after registering).</p>

            <h3 id="workstation-setup"> Workstation Setup </h3>
            <p>This allow you to set up different kind of widget on the workstation. You can reduce the number of widget
                on your workstation or add more depending on your need.</p>
            <p><img src="{{ asset('images/user-guide/workstation-setup.png') }}"></p>

            <h3 id="workstation-customisation"> Workstation Customisation </h3>
            <p>This allows you to filter what tools you want to see on your workspace. This only comes on a request by
                the user of the application</p>

            <h3 id="view-workstation-items"> View workstation Items </h3>
            <p>Your workspace items can be viewed as it is the landing page on BMAC and you can also select any of the
                tools by clicking on the tool icon.</p>
            <p><img src="{{ asset('images/user-guide/view-workstation-items.png') }}"></p>
            <hr>


            <h2> Dashboard </h2>
            <p>BMAC Dashboard, which shows your business performance in simple, easy-to-understand charts and financial
                summaries to enhance real-time assessment of business performance. The Dashboard uses a combination of
                RAG Gauge, bar charts, pie charts and different tables to display the performance of a business at a
                glance.</p>

            <h3 id="dashboard-set-up"> Dashboard set-up </h3>
            <p>This allow you to set up different kind of charts, Gauge and tables on the dashboard. You can reduce the
                number of charts, gauge or tables on your dashboard or add more depending on your need.</p>
            <p><img src="{{ asset('images/user-guide/dashboard-set-up.png') }}"></p>

            <h3 id="dashboard-customisation"> Dashboard Customisation </h3>
            <p>This allows you to request for different specific information to be presented through the charts, gauge
                and tables on the dashboard. This only comes on a request by the user (business owner) of the
                application.</p>

            <h3 id="view-dashboard-items"> View Dashboard Items </h3>
            <p>Your dashboard items can be viewed at a glance to enable you make quick decisions on BMAC. You can also
                select any of the dashboard items by clicking on the icon.</p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-1.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-2.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-3.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-4.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-5.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/view-dashboard-items-6.png') }}"></p>
            <hr>


            <h2> Collaboration </h2>
            <p>The Collaborator tool on the BMAC allows business owners invite financial influencers, accountant
                (amongst others) to the platform to either </p>
            <ul>
                <li>The Collaborator tool on the BMAC allows business owners invite financial influencers, accountant
                    (amongst others) to the platform to either
                </li>
                <li>review your business performance through different mechanism.</li>
                <p><img src="{{ asset('images/user-guide/Collaboration.png') }}"></p>
            </ul>

            <h3 id="sending-collaborative-invites"> Sending collaborative invites </h3>
            <p>When a business owner sends an invite to an influencer or a professional for collaboration, the recipient
                can either accept the invite or reject the invite through the collaborator icon on the workstation.</p>
            <p><img src="{{ asset('images/user-guide/sending-collaborative-invites.png') }}"></p>

            <h3 id="manage-permission-col"> Manage permission (Read/Write) </h3>
            <p>When sending the invite, the business owner can determine the access level to assign finance providers or
                Professionals which can either be VIEW ONLY or EDIT.</p>
            <p><img src="{{ asset('images/user-guide/manage-permission-col.png') }}"></p>
            <hr>


            <h2> Reports </h2>
            <p>When it comes to the detailed review of business performance, BMAC provides user with one of the best
                reporting modules around. It provides fast, easy and accurate access to the business data.</p>

            <h3 id="generate-reports"> Generate reports </h3>
            <p>This allows users create reports by selecting what information to be included and presenting in their
                desired format.</p>
            <p><img src="{{ asset('images/user-guide/generate-reports.png') }}"></p>

            <h3 id="view-reports"> View Reports </h3>
            <p>in BMAC, viewing your business activities report is simple and easy to understand. The list of every
                report is displayed when your click on the quick view module.</p>
            <p><img src="{{ asset('images/user-guide/view-reports.png') }}"></p>

            <h3 id="print-report"> Print Report </h3>
            <p>It is also easy to print out any of these report in hardcopy document.</p>

            <h3 id="search-reports"> Search Reports </h3>
            <p>The BMAC houses a lot of data generated as a result of postings into the application, BMAC allows you to
                query any report by searching through date.</p>
            <p><img src="{{ asset('images/user-guide/search-reports.png') }}"></p>

            <h3 id="export-reports"> Export Reports </h3>
            <p>BMAC allow user to convert report files from their original format to a new one e.g. PDF, Excel</p>
            <p><img src="{{ asset('images/user-guide/export-reports.png') }}"></p>
            <hr>


            <h2> Tutorial </h2>

            <h3 id="ikOOba-youtube-channel"> ikOOba youtube Channel </h3>
            <p>Our video materials can also be found on our YouTube channel.</p>
            <p>Link:
                <a href="https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg">
                    https://www.youtube.com/channel/UCO2dygT3OoHef67FZXCmECg
                </a>
            </p>
            <p><img src="{{ asset('images/user-guide/ikOOba-youtube-channel.png') }}"></p>
            <hr>


            <h2 id="ikOOba-online-support"> ikOOba Online Support </h2>
            <p>With our online support you’ll have quick answers to your questions and timely resolution on issues
                raised.</p>

            <ul>
                <li>Live Chat – Our agents are always online to take you queries</li>
                <li>Ticketing System – With our automated ticketing system your issues are channelled to the respective
                    departments with a timely resolution on their queries.
                </li>
                <li>Knowledge Base – This documents common issues and provide quick written solutions.</li>
            </ul>
            <p>To access the support system, visit
                <a href="https://www.ikooba.com/support">
                    www.ikooba.com/support
                </a>
            </p>
            <p><img src="{{ asset('images/user-guide/ikOOba-online-support-1.png') }}"></p>
            <p><img src="{{ asset('images/user-guide/ikOOba-online-support-2.png') }}"></p>
            <hr>


        </div>
    </div>
</section>
<section class="vibrant centered">
    <div class="container">
        <h4> BMAC© is a Bouquet of Business and Collaboration Solutions that brings practical and effective solutions
            that help address day to day problems that businesses experience with accounting, performance measurement
            and control.</h4>
    </div>
</section>
<footer>
    <div class="container">
        <p> &copy {{ date('Y') }} All Rights Reserved. ikOOba Technologies </p>
    </div>
</footer>
<script src="js/user-guide/jquery.min.js"></script>

<script type="text/javascript" src="js/user-guide/prettify/prettify.js"></script>
<script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&skin=sunburst"></script>
<script src="js/user-guide/layout.js"></script>
<script src="js/user-guide/jquery.localscroll-1.2.7.js" type="text/javascript"></script>
<script src="js/user-guide/jquery.scrollTo-1.4.3.1.js" type="text/javascript"></script>
<script type="text/javascript" src="../../wp-content/themes/piha/js/top-bar-l1.js"></script>
<script type="text/javascript" src="../../wp-content/themes/piha/js/bsa-ads-l1.js"></script>
<!--Dynamically creates analytics markup-->
</body>
</html>
