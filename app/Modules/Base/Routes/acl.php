<?php
/**
 * Created by PhpStorm.
 * User: baddy
 * Date: 16/01/2018
 * Time: 10:23
 */

Route::get('/access-control', 'AccessControlController@employeeList')->middleware('verify_license');
Route::get('/access-control/assign/{id}', 'AccessControlController@index')->middleware('verify_license');
Route::post('/access-control/assign', 'AccessControlController@assignPermissionToEmployee')->middleware('verify_license');
