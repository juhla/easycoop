<?php

Route::get('/profile/{name}', 'ProfileController@index');

Route::get('/profile/view/{name}/{id}', 'ProfileController@viewProfile');

Route::get('/company/{name}/{id}', 'ProfileController@viewCompany');

//personal info routes
Route::post('/profile/updatePersonalInfo', 'ProfileController@updatePersonalInfo');
Route::post('/profile/updateContactInfo', 'ProfileController@updateContactInfo');

//professionalInfo routes
Route::get('/profile/get-professional-info/{id}', 'ProfileController@getProfessionalInfo');
Route::get('/profile/professional-info/delete/{id}', 'ProfileController@deleteProfessionalInfo');
Route::post('/profile/update-professional-info', 'ProfileController@updateProfessionalInfo');

//company info routes
Route::post('/profile/updateCompanyInfo', 'ProfileController@updateCompanyInfo');
Route::get('/profile/getCompanyInfo/{id}', 'ProfileController@getCompanyInfo');
Route::get('/profile/companyInfo/delete/{id}', 'ProfileController@deleteCompanyInfo');

//photos route
Route::any('/profile/change-profile-pic', 'ProfileController@changeProfilePic');
Route::any('/profile/change-company-logo', 'ProfileController@changeCompanyLogo');
