<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

if (config('app.name') == "BMAC") {
    Route::get('user-guide', function () {
        return view('user-guide');
    });


    Route::get('bmac-pricing', function () {
        return view('pricing');
    });

    Route::get('mailable', function () {
        $user = \App\Modules\Base\Models\User::find(423);
        return new \App\Modules\Base\Mail\CheckupMail($user);
    });

    Route::get('/bmac/pricing', 'FrontPageController@bmacPricing')->name('bmacPricing');
    Route::get('/bmac/accounting', 'FrontPageController@bmacAccounting')->name('bmacAccounting');

//Route::get('/',  ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);

//Route::get('/sign-in', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::get('/dashboard', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'DashboardReportController@index']);
    Route::get('/dashboards', ['middleware' => ['web', 'auth:web'], 'uses' => 'DashboardController@index']);
    Route::get('/business-owner-setup', ['middleware' => ['web', 'auth:web'], 'uses' => 'setupController@businessOwnerSetUp']);
    Route::get('/acct-setup', ['middleware' => ['web', 'auth:web'], 'uses' => 'setupController@acctSetUp']);


    Route::get('/workspace', ['middleware' => ['web', 'auth:web',], 'uses' => 'DashboardController@workspace']);
    Route::get('/bmac/premium', ['middleware' => ['web', 'auth:web'], 'uses' => 'DashboardController@bmacPremium']);
    Route::any('/memorandum', ['middleware' => ['web', 'auth:web'], 'uses' => 'MemorandumController@index']);
    Route::any('/memorandum/ledger', ['middleware' => ['web', 'auth:web'], 'uses' => 'MemorandumController@ledger']);
    Route::post('/memorandum/postmemo', ['middleware' => ['web', 'auth:web'], 'uses' => 'MemorandumController@postmemo']);

    Route::any('/workspace/widgets', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@workspace_index']);


    Route::get('/logoutCompany', ['middleware' => ['web', 'auth:web'], 'uses' => 'DashboardController@logoutCompany']);
//Route::get('/dashboard/change-company', ['middleware' => ['web', 'auth:web'], 'uses' => 'CollaboratorsController@changeActiveCompany']);
    Route::get('/ratios.json', 'DashboardController@ratiosGraph');
//Route::get('/test', ['uses' => 'AccountSettingsController@test']);

// Dashboard Reports
    Route::group(['middleware' => ['web', 'auth:web'], 'prefix' => 'dashboard/report'], function () {


        Route::any('/widgets', ['uses' => 'DashboardReportController@stats_index']);
        Route::any('/change-widgets', ['uses' => 'DashboardReportController@change_widgets']);


        Route::get('/', ['uses' => 'DashboardReportController@index']);

// Payables Age Analysis
        Route::any('/payables_age_analysis', 'DashboardReportController@payablesAgeAnalysis');


        Route::any('/debtors_collection_period', 'DashboardReportController@debtors_collection_period');

        // Running Expenses
        Route::any('/running_expenses', 'DashboardReportController@running_expenses');

        // Revenues
        Route::any('/revenues', 'DashboardReportController@revenues');
        Route::any('/revenues_month', 'DashboardReportController@revenues_month');

        //Top Payables
        Route::get('/top_payables_accounts', ['uses' => 'DashboardReportController@top_payables_accounts']);

        //Top Receivables
        Route::get('/receivables_owed', ['uses' => 'DashboardReportController@receivablesOwed']);

        //Monthly payables
        Route::get('/payables_accounts', ['uses' => 'DashboardReportController@payables_accounts']);
        Route::get('/payables_accounts_month', ['uses' => 'DashboardReportController@payables_accounts_month']);


        //Monthly recievables
        Route::get('/receivables_accounts', ['uses' => 'DashboardReportController@receivables_accounts']);
        Route::get('/receivables_accounts_month', ['uses' => 'DashboardReportController@receivables_accounts_month']);

        // Stock Summary
        Route::any('/stock_summary', 'DashboardReportController@stock_summary');
        Route::any('/stock_summary_month', 'DashboardReportController@stock_summary_month');

        // Net Profit Margin
        Route::any('/net_profit_margin', 'DashboardReportController@net_profit_margin');

        // Return on Capital Returned
        Route::any('/return_on_capital', 'DashboardReportController@return_on_capital');

        // Sales History
        Route::any('/sales_history', ['uses' => 'DashboardReportController@sales_history']);
        Route::any('/sales_history_month', ['uses' => 'DashboardReportController@sales_history_month']);
        Route::any('/purchase_history', ['uses' => 'DashboardReportController@purchases_history']);
        Route::any('/purchase_history_month', ['uses' => 'DashboardReportController@purchases_history_month']);

        //Ope
        //Route::get('/business_summary', 'TempController\DashboardReportOpeController@businessSummary');
        Route::get('/customer_by_sales', 'DashboardReportController@customerBySales');
        Route::get('/acid_test_ratio', 'DashboardReportController@acidTestRatio');
        Route::get('/creditor_payment_period', 'DashboardReportController@creditorPaymentPeriod');
        Route::get('/gross_profit_margin', 'DashboardReportController@grossProfitMargin');
        Route::get('/gear_ratio', 'DashboardReportController@gearRatio');

        Route::get('/cash_at_hand', 'DashboardReportController@cashAtHand');

        Route::get('/bank_balances', 'DashboardReportController@bankBalances');


        //habeeb domain
        Route::any('/receivable_age_analysis', 'DashboardReportController@receivableAgeAnalysis');
        Route::any('/top_five_purchases_of_stock', 'DashboardReportController@topFivePurchasesOfStock');
        Route::any('/top_five_sales_of_stock', 'DashboardReportController@topFiveSalesOfStock');
        Route::any('/our_ability_to_cover_trading_liabilities', 'DashboardReportController@currentRatio');
        Route::get('/supplier_by_purchases', 'DashboardReportController@supplierByPurchases');


    });


// walkthrough setup
    Route::get('/getCountryInfo/{id}', ['middleware' => ['web', 'auth:web'], 'uses' => 'setupController@getCountryInfo']);
    Route::post('/businessOwnerSetup', ['middleware' => ['web', 'auth:web'], 'uses' => 'setupController@businessOwnerSetupPost']);
    Route::post('/acctSetupPost', ['middleware' => ['web', 'auth:web'], 'uses' => 'setupController@acctSetupPost']);
//api routes
    Route::get('api-v1/get-gl-accounts-for-select', 'ApiController@getSelectAccounts');
    Route::get('api-v1/get-taxes', 'ApiController@getAllTax');
    Route::get('api-v1/get-states/{country_id}', 'ApiController@_getStates');

//account settings route
    Route::get('account-settings', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@index']);
    Route::post('account-settings/change-password', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@changePassword']);
    Route::post('account-settings/edit-currency', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@editCurrency']);
    Route::get('account-settings/deactivate-account', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@deactivateAccount']);


    Route::get('workspace-settings', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@workspace_index']);
    Route::post('workspace-settings/change-proficiency', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@change_proficiency']);
    Route::any('workspace-settings/change-widgets', ['middleware' => ['web', 'auth:web'], 'uses' => 'AccountSettingsController@change_widgets']);


    Route::get('faq', ['middleware' => ['web', 'auth:web'], 'uses' => 'HelpController@faq']);
//Route::get('membership/user-licenses', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@index']);
//Route::get('membership/plans', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@plans']);
//Route::get('membership/maintenance/plans', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@membership_plans']);
//Route::any('membership/subscribe/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@subscribe']);
//Route::any('membership/payroll/subscribe/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@subscribe_payroll']);
//Route::any('membership/ims/subscribe/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@subscribe_ims']);


    Route::any('bmac/renewal/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@renewal_bmac']);
    Route::any('ims/renewal/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@renewal_ims']);
    Route::any('payroll/renewal/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'UserLicenseController@renewal_payroll']);

    Route::get('assemblyline/plans', ['middleware' => ['web', 'auth:web'], 'uses' => 'AssemblyLineLicenseController@checkSub']);
    Route::any('assemblyline/subscribe/{id}/plan', ['middleware' => ['web', 'auth:web'], 'uses' => 'AssemblyLineLicenseController@subscribe']);


// ticketing
    Route::get('ticket/list', ['middleware' => ['web', 'auth:web'], 'uses' => 'TicketController@index']);
// Route::get('ticket/add-new', ['middleware' => ['auth:web'], 'uses' => 'TicketController@create']);
    Route::post('ticket/add-new', ['as' => 'ticket.store', 'middleware' => ['auth:web'], 'uses' => 'TicketController@store']);
    Route::any('ticket/destroy/{id}', ['as' => 'ticket.destroy', 'middleware' => ['auth:web'], 'uses' => 'TicketController@destroy']);
//help
    Route::post('help/feedback', ['middleware' => ['auth:web'], 'uses' => 'HelpController@sendFeedback']);

// osticketing
    Route::get('ticket/add-new', ['middleware' => ['auth:web'], 'uses' => 'OSTicketController@create']);

//Messenger
    Route::resource('message', 'MessengerController');
//TODO
    Route::get('todo', 'TodoController@index')->middleware('verify_license');
    Route::any('todo-store', 'TodoController@store')->middleware('verify_license');
    Route::any('todo-update/{id}', 'TodoController@update')->middleware('verify_license');
    Route::any('todo-delete/{id}', 'TodoController@delete')->middleware('verify_license');

    Route::get('audit-log', 'Premium\AuditController@index');
    Route::get('data-log/{id}', 'Premium\AuditController@data_log');
    Route::post('data-search', 'Premium\AuditController@search_date');

    require 'profile.php';
    require 'acl.php';

    Route::get('test-mail', function () {
        return view('emails.receipt');
    });


    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('assemblyline/add-new', ['middleware' => ['web', 'auth:web'], 'uses' => 'AssemblylineController@create']);
    Route::post('assemblyline/add-new', ['middleware' => ['auth:web'], 'uses' => 'AssemblylineController@store']);
    Route::get('morata', 'AssemblylineController@morata');
    Route::get('cancel/assemblyline', 'AssemblyLineLicenseController@cancelAssemblyLine');


    /* =============  Bmac Professional Route ============*/

    Route::get('bmac/professional', 'BmacProfessionalController@professional')->name('bmacprofessional');
// Route::get('/lisence', function(){
//     return 'hi';
// })->middleware('verify_license');
}
