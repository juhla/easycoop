<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 21/05/2018
 * Time: 2:28 PM
 */


namespace App\Modules\Base\Scopes;

use App\Modules\Base\Models\Message;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class ActiveToSoftDelete implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

    }

}