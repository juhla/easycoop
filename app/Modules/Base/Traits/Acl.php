<?php

namespace App\Modules\Base\Traits;

use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustFacade as Entrust;

trait Acl
{

    public function hasAccess($permissions)
    {

        if ((Auth::user()->hasRole('employee-payroll')) && in_array('payroll', $permissions)) {
            return true;
        }

        if ((Auth::user()->hasRole('employee-ims')) && in_array('ims', $permissions)) {
            return true;
        }
        //$isBusinessOwner = Auth::user()->hasRole(['business-owner', 'advanced-business-owner', 'basic-business-owner']);
        $isEmployee = Auth::user()->hasRole(['employee', 'employee-payroll', 'town-hall', 'employee-ims']);
        if (!(self::canAccess($permissions) || (!$isEmployee))) {
            if (request()->ajax()) {
                return response()->json(['message' => 'You are not allowed to access that page'], 500);
            } else {
                flash()->error('you are not permitted to view this page');
                redirect('dashboard')->send();
            }
        }
        return true;
    }

    public static function can($permissions)
    {

        if ((Auth::user()->hasRole('employee-payroll')) && in_array('payroll', $permissions)) {
            return true;
        }

        if ((Auth::user()->hasRole('employee-ims')) && in_array('ims', $permissions)) {
            return true;
        }
        $isEmployee = Auth::user()->hasRole(['employee', 'employee-payroll', 'town-hall', 'employee-ims']);
        if (!(self::canAccess($permissions) || (!$isEmployee))) {
            return false;
        }
        return true;
    }


    public static function canAccess($permission, $requireAll = false)
    {
        $roles = Auth::user()->tenant_roles;
        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = self::canAccess($permName);

                if ($hasPerm && !$requireAll) {
                    return true;
                } elseif (!$hasPerm && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($roles as $role) {
                // Validate against the Permission table
                foreach ($role->perms as $perm) {
                    if ($perm->name == $permission) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}
