<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use Illuminate\Support\Facades\Auth;

trait AfterLogin
{
    use TenantConfig;

    public function handleSession()
    {
        $session = session('company_db');
        if (Auth::check()) {
            $user = Auth::user();
            if (empty($session)) {
                session()->put('company_db', $user->db);
            }
        }
    }

}
