<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;

trait Audit
{
    use TenantConfig, NewCreates;


    public function fixLogin()
    {
        Config::set('database.connections.tenant_conn', [
            'driver' => 'mysql',
            'host' => Config::get('database.connections.mysql.host'),
            'database' => 'ikooba_db',
            'username' => Config::get('database.connections.mysql.username'),
            'password' => Config::get('database.connections.mysql.password'),
            'charset' => Config::get('database.connections.mysql.charset'),
            'collation' => Config::get('database.connections.mysql.collation'),
            'prefix' => '',
        ]);


    }


    public function handleTrail()
    {
        $session = session('company_db');

        //  $this->createProfilePic();
        if (Auth::check()) {
            $user = Auth::user();
            if (empty($session)) {
                //if member type is msme owner

                session()->put('company_db', $user->db);

            } else {
                $this->setConfig(session()->get('company_db'));
            }


        }
    }


}

