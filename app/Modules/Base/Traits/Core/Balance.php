<?php

namespace App\Modules\Base\Traits\Core;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;
use DB;

trait Balance
{
    /**
     * Assets : Debit
     * Liabilities: Credit
     * Equity: Credit
     * Revenue: Credit
     * Cost: Debit
     */


    /**
     * Assets , Liabilities and Equity are Balanced sheet items
     * Revenue and costs are income statements
     * the greater balance always takes precedent i.e if credit balance is greater than debit balance, the balance is credit balance
     */


    function bootTrait()
    {


    }

    function fixTable()
    {
        $obj = new AccountGroup();
        $obj->where('parent_id', 0)->update(['parent_id' => null]);
        $obj->fixTree();
    }

    function groupBalance($groupID)
    {
        $now = Carbon::now();
        $start = $now->copy()->startOfMonth();
        $start = $start->format('Y-m-d');


        $end = $now->copy()->endOfMonth();
        $end = $end->format('Y-m-d');

        $theAccountGroup = AccountGroup::whereDescendantOrSelf($groupID);
        $IDS = $theAccountGroup->pluck('id')->toArray();

        $debit = $this->__groupBalance($IDS, 'D', $start, $end);
        $credit = $this->__groupBalance($IDS, 'C', $start, $end);
        pr($debit);
        pr($credit);
        $res = $credit - $debit;
        pr($res);

        exit;

    }


    public function __calculateGroupBalance()
    {

    }

    /**
     * @param null $start_date
     * @param null $end_date
     * @return array
     */

    public function ___profitAndLoss($end_date = null)
    {
        $includeEquals = false;

        $revenueGroup = AccountGroup::whereDescendantOrSelf(4);
        $costGroup = AccountGroup::whereDescendantOrSelf(5);

        // Revenue - Cost  = profit (from the beginning of time)
        $revenueBalance = $this->getGroupBalance($revenueGroup->pluck('id')->toArray(), null, $end_date, $includeEquals);
        $costBalance = $this->getGroupBalance($costGroup->pluck('id')->toArray(), null, $end_date, $includeEquals);

        return $this->calculateBalanceFromDebitAndCredit($revenueBalance, $costBalance);
    }


    /**
     * @param null $start_date
     * @param null $end_date
     * @return array
     */
    public function __calculateRetainedEarnings($start_date = null, $end_date = null)
    {

        $profitOrLoss = $this->___profitAndLoss($end_date);

        // 3300 Retained Earnings Ledger Code
        $retainedEarningLedger = Ledger::where('code', 3300)->first()->id;
        $retainedEarningBalance = $this->getLedgerBalance($retainedEarningLedger, null, $end_date);

        // combine the profit to the retained earnings to get the total retained earnings
        $retainedEarnings = $this->calculateBalanceFromDebitAndCredit($retainedEarningBalance, $profitOrLoss);

        return $retainedEarnings;

    }

    function getLedgerIDForGroupID($groupID)
    {
        return Ledger::where('group_id', $groupID)->pluck('name', 'id')->toArray();
    }

    /**
     * @param $code
     * @param null $start_date
     * @param null $end_date
     */

    public function getLedgerBalanceByCode($code, $start_date = null, $end_date = null)
    {


        $dr_total = $this->__ledgerBalanceByCode($code, 'D', $start_date, $end_date);
        $cr_total = $this->__ledgerBalanceByCode($code, 'C', $start_date, $end_date);


        $debit['dc'] = 'D';
        $debit['balance'] = $dr_total;


        $credit['dc'] = 'C';
        $credit['balance'] = $cr_total;


        return $this->calculateBalanceFromDebitAndCredit($debit, $credit);

    }

    /**
     * @param $id
     * @param null $start_date
     * @param null $end_date
     * @return array
     */
    public function getLedgerBalance($id, $start_date = null, $end_date = null)
    {


        $dr_total = $this->__ledgerBalance($id, 'D', $start_date, $end_date);
        $cr_total = $this->__ledgerBalance($id, 'C', $start_date, $end_date);


        $debit['dc'] = 'D';
        $debit['balance'] = $dr_total;


        $credit['dc'] = 'C';
        $credit['balance'] = $cr_total;


        return $this->calculateBalanceFromDebitAndCredit($debit, $credit);

    }


    /**
     * @param $id
     * @param null $start_date
     * @param null $end_date
     * @param $includeEquals  used to include the equalsto checker for date filtering
     * @return array
     */
    public function getGroupBalance($id, $start_date = null, $end_date = null, $includeEquals = true)
    {


        $dr_total = $this->__groupBalance($id, 'D', $start_date, $end_date, $includeEquals);
        $cr_total = $this->__groupBalance($id, 'C', $start_date, $end_date, $includeEquals);


        $debit['dc'] = 'D';
        $debit['balance'] = $dr_total;


        $credit['dc'] = 'C';
        $credit['balance'] = $cr_total;


        return $this->calculateBalanceFromDebitAndCredit($debit, $credit);

    }


    /**
     * Gets array with index dc and balance i.e ['dc'=>'x','balance'=>'3']
     * @param array $first
     * @param array $second
     * @return array
     */
    function calculateBalanceFromDebitAndCredit($first = array(), $second = array())
    {

        if ($first['dc'] == $second['dc']) {
            return ['dc' => $first['dc'], 'balance' => bcadd($first['balance'], $second['balance'], 2)];
        } else if ($first['balance'] > $second['balance']) {
            $cl = bcsub($first['balance'], $second['balance'], 2);
            return ['dc' => $first['dc'], 'balance' => $cl];
        } else if ($second['balance'] === $first['balance']) {
            return ['dc' => '', 'balance' => 0];
        } else {
            $cl = bcsub($second['balance'], $first['balance'], 2);
            return ['dc' => $second['dc'], 'balance' => $cl];
        }
    }


    public function getFullBalance($start_date = null, $end_date = null)
    {
        $account = $this;
        $total = 0;
        if ($account->groups) {
            foreach ($account->groups as $group) {
                $result = $group->getBalance($start_date, $end_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);

                if ($group->nestedGroups($group->id)) {
                    foreach ($group->nestedGroups($group->id)->get() as $child) {
                        $result = $child->getBalance($start_date, $end_date);
                        formatNumber($result['balance']);
                        $total = bcadd($result['balance'], $total, 2);
                        if ($child->nestedGroups($child->id)) {
                            foreach ($child->nestedGroups($group->id)->get() as $sub) {
                                $result = $sub->getBalance($start_date, $end_date);
                                formatNumber($result['balance']);
                                $total = bcadd($result['balance'], $total, 2);
                            }
                        }
                    }
                }
            }
        }
        if ($account->ledgers->count()) {
            foreach ($account->ledgers()->get() as $ledger) {
                $result = $ledger->getBalance($start_date, $end_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);
            }
        }
        return $total;
    }


    function listAllGroupChildren($groupID = array())
    {
        $IDS = array();
        foreach ($groupID as $eachGroup) {
            $theAccountGroup = AccountGroup::whereDescendantOrSelf($eachGroup);
            $IDS[] = $theAccountGroup->pluck('id')->toArray();
        }
        return $IDS;
    }

    /**
     *  This method is used to get the root parent group to determine if the group is a balanced sheet item
     * @param $groupID
     * @param $start_date
     * @return null
     */
    function getParentNameFromGroupID($groupID, $start_date)
    {
        try {
            $theGroup = AccountGroup::find($groupID);
            if ($theGroup->parent_id == null) {
                $group = $theGroup->name;
            } else {
                if (empty(AccountGroup::find($groupID)->ancestors)) {
                    $group = AccountGroup::find($groupID)->where('parent_id', null)->first()->name;
                } else {
                    $group = AccountGroup::find($groupID)->ancestors->where('parent_id', null)->first()->name;
                }
            }
            if (in_array($group, ['Assets', 'Liabilities', 'Equity'])) {
                $start_date = null;
            }
        } catch (\Exception $exception) {
        }
        return $start_date;
    }


    /**
     * This method is used to get the root parent group to determine if the group is a balanced sheet item
     * @param $ledgerCode
     * @param $start_date
     * @return null
     */

    function getParentNameFromLedgerCode($ledgerCode, $start_date)
    {
        try {
            $theGroup = AccountGroup::whereHas('ledgers', function ($q) use ($ledgerCode) {
                $q->where('code', $ledgerCode);
            })->first();

            if ($theGroup->parent_id == null) {
                $group = $theGroup->name;
            } else {
                if (empty(AccountGroup::find($theGroup->id)->ancestors)) {
                    $group = AccountGroup::find($theGroup->id)->where('parent_id', null)->first()->name;
                } else {
                    $group = AccountGroup::find($theGroup->id)->ancestors->where('parent_id', null)->first()->name;
                }
            }
            if (in_array($group, ['Assets', 'Liabilities', 'Equity'])) {
                $start_date = null;
            }

        } catch (\Exception $exception) {
        } catch (\Throwable $e) {
        }
        return $start_date;
    }

    /**
     * This method is used to get the root parent group to determine if the group is a balanced sheet item
     * @param $ledgerID
     * @param $start_date
     * @return null
     */

    function getParentNameFromLedgerID($ledgerID, $start_date)
    {
        try {
            $theGroup = AccountGroup::whereHas('ledgers', function ($q) use ($ledgerID) {
                $q->where('id', $ledgerID);
            })->first();

            if ($theGroup->parent_id == null) {
                $group = $theGroup->name;
            } else {
                if (empty(AccountGroup::find($theGroup->id)->ancestors)) {
                    $group = AccountGroup::find($theGroup->id)->where('parent_id', null)->first()->name;
                } else {
                    $group = AccountGroup::find($theGroup->id)->ancestors->where('parent_id', null)->first()->name;
                }
            }
            if (in_array($group, ['Assets', 'Liabilities', 'Equity'])) {
                $start_date = null;
            }

        } catch (\Exception $exception) {
        }
        return $start_date;
    }

    /**
     * @param $groupID
     * @param null $DC
     * @param null $start_date
     * @param null $end_date
     * @param $includeEquals   used to include the equalsto checker for date filtering
     * @return mixed
     */
    function __groupBalance($groupID, $DC = null, $start_date = null, $end_date = null, $includeEquals = true)
    {
        /**
         * if the groupID being passed is an array, ... it shows that the parentID check has been done,
         * so we dont need to check it here again
         */
        $start_date = $this->getParentNameFromGroupID($groupID, $start_date);
        if (!is_array($groupID)) {
            $groupID = array($groupID);
        }

        $groupID = $this->listAllGroupChildren($groupID);
        $collection = collect($groupID);
        $flattened = $collection->flatten();
        $groupID = $flattened->all();


        //  pr($groupID);

        $res = TransactionItem::
        with(['transactions', 'ledger'])
            ->whereHas('transactions', function ($q) use ($start_date, $end_date, $includeEquals) {

                if ($includeEquals) {
                    if (!empty($start_date)) {
                        $q->whereDate('transaction_date', '>=', $start_date);
                    }
                    if (!empty($end_date)) {
                        $q->whereDate('transaction_date', '<=', $end_date);
                    }
                } else {
                    if (!empty($start_date)) {
                        $q->whereDate('transaction_date', '>', $start_date);
                    }
                    if (!empty($end_date)) {
                        $q->whereDate('transaction_date', '<', $end_date);
                    }
                }

                //$q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
            })
            ->whereHas('ledger', function ($q) use ($groupID) {
                $q
                    ->whereIn('group_id', $groupID)
                    ->where('flag', 'Active');
            })
            ->where(function ($q) use ($DC) {
                if (!empty($q)) {
                    $q->where('dc', $DC);
                }
            })
            ->where('flag', 'Active')
            //    ->toSql();
            ->sum('amount');
        return $res;
    }


    /**
     * @param $ledgerCode
     * @param $DC
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    function __ledgerBalanceByCode($ledgerCode, $DC, $start_date = null, $end_date = null)
    {
        /**
         * This method is to check if the ledger is a retained earnings ledger so we add profit/ Loss to it
         */

        $balance = 0;
        if (!is_array($ledgerCode)) {
            if ('3300' == $ledgerCode) {
                $res = $this->___profitAndLoss($start_date);
                if ($res['dc'] == $DC) {
                    $balance = $res['balance'];
                }
            }
        }


        /**
         * if the LedgerCode being passed is an array, ... it shows that the parentID check has been done,
         * so we dont need to check it here again
         */

        $start_date = $this->getParentNameFromLedgerCode($ledgerCode, $start_date);
        if (!is_array($ledgerCode)) {
            $ledgerCode = array($ledgerCode);
        }


        $amount = TransactionItem::
        with(['transactions'])
            ->whereHas('transactions', function ($q) use ($start_date, $end_date) {
                if (($start_date)) {
                    $q->whereDate('transaction_date', '>=', $start_date);
                }
                if (($end_date)) {
                    $q->whereDate('transaction_date', '<=', $end_date);
                }
                //$q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
            })
            ->whereHas('ledger', function ($q) use ($ledgerCode) {
                $q->whereIn('code', $ledgerCode)
                    ->where('flag', 'Active');
            })
            ->where('flag', 'Active')
            ->where('dc', $DC)
            ->sum('amount');


        return $amount + $balance;

    }


    /**
     * @param $ledgerID
     * @param $DC
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    function __ledgerBalance($ledgerID, $DC, $start_date = null, $end_date = null)
    {

        $start_date = $this->getParentNameFromLedgerID($ledgerID, $start_date);
        if (!is_array($ledgerID)) {
            $ledgerID = array($ledgerID);
        }


        return TransactionItem::
        with(['transactions', 'ledger'])
            ->whereHas('transactions', function ($q) use ($start_date, $end_date) {
                if (($start_date)) {
                    $q->whereDate('transaction_date', '>=', $start_date);
                }
                if (($end_date)) {
                    $q->whereDate('transaction_date', '<=', $end_date);
                }
                //$q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
            })
            ->whereHas('ledger', function ($q) use ($ledgerID) {
                $q->whereIn('id', $ledgerID)
                    ->where('flag', 'Active');
            })
            ->where('dc', $DC)
            ->where('flag', 'Active')
            ->sum('amount');

    }


}

