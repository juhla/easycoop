<?php

namespace App\Modules\Base\Traits\Core;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;
use DB;

trait COA
{
    /**
     * Assets : Debit
     * Liabilities: Credit
     * Equity: Credit
     * Revenue: Credit
     * Cost: Debit
     */

    protected $Errors;
    protected $errorBool;
    protected $AccountGroup;
    protected $inValidArray = array();

    /**
     * @return mixed
     */
    public function getErrorBool()
    {
        return $this->errorBool;
    }

    /**
     * @param mixed $errorBool
     */
    public function setErrorBool($errorBool)
    {
        $this->errorBool = $errorBool;
    }


    /**
     * @return mixed
     */
    public function getAccountGroup()
    {
        return $this->AccountGroup;
    }

    /**
     * @param mixed $AccountGroup
     */
    public function setAccountGroup($AccountGroup)
    {
        $this->AccountGroup = $AccountGroup;
    }

    /**
     * @return array
     */
    public function getInValidArray()
    {
        return $this->inValidArray;
    }

    /**
     * @param $inValidArray
     */
    public function setInValidArray($inValidArray)
    {
        $this->inValidArray[] = $inValidArray;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->Errors;
    }

    /**
     * @param mixed $Errors
     */
    public function setErrors($Errors)
    {
        $this->Errors = $Errors;
    }


    function validateGroupCOA($ledger)
    {

        $hasLedger = Ledger::where('group_id', $ledger['parent_id'])->first();
        if (!empty($hasLedger)) {
            $this->setErrors('You cannot add a sub-group to this group {the delete the ledgers under this group first}');
            return false;
        }
        return true;
    }

    /**
     * @param $groupID
     * @return bool
     */
    function getValidationFromGroupID($groupID)
    {

        try {
            $theGroup = AccountGroup::find($groupID);
            if ($theGroup->parent_id == null) {
                $group = $theGroup->name;
            } else {
                $group = AccountGroup::find($theGroup->id)->ancestors->where('parent_id', null)->first()->name;
            }
            if (in_array($group, ['Assets', 'Liabilities'])) {
                return true;
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }

    }


    function isSystemLedger($ledgerID)
    {
        return !Ledger::where('id', $ledgerID)
            ->where('is_system_account', 1)
            ->get()->isEmpty();
    }


    function validateLedgerCOA($ledger, $ledgerID)
    {

        if (!($this->isSystemLedger($ledgerID)) && ($this->getValidationFromGroupID($ledger['group_id']))) {
            $parentGroup = AccountGroup::find($ledger['group_id']);
            $bool = $parentGroup->isRoot();
            if ($bool) {
                $this->setErrors('You cannot add a ledger to this account group');
                return false;
            }
        }
        return true;
    }

    function testValidate()
    {
        $group = AccountGroup::where('parent_id', 3)->pluck('name', 'id')->toArray();
        dd(empty($group));
    }

    function checkValidLedgers()
    {
        $group = AccountGroup::where('parent_id', null)->pluck('name', 'id')->toArray();
        foreach ($group as $key => $eachGroup) {
            $this->setAccountGroup($eachGroup);
            //if (in_array($eachGroup, ['Assets', 'Liabilities', 'Equity'])) {
            if (in_array($eachGroup, ['Assets', 'Liabilities'])) {
                $this->validateGroup($key);
            }
            $this->checkIfGroupHasLedgerAndGroup($key);
        }
    }

    function checkIfGroupHasLedgerAndGroup($id)
    {
        $hasLedger = Ledger::where('group_id', $id)->where('is_system_account', '!=', '1')->first();
        //$hasLedger = Ledger::where('group_id', $id)->first();
        $hasGroup = AccountGroup::where('parent_id', $id)->first();
        if (!empty($hasLedger) && !empty($hasGroup)) {
            $this->setInValidArray($hasLedger->name . ' cannot be created under ' . $this->getAccountGroup());
            return false;
        }
        return true;
    }

    function validateGroup($id)
    {
        $group = AccountGroup::where('parent_id', $id)->pluck('name', 'id')->toArray();
        if (!empty($group)) {
            foreach ($group as $key => $eachGroup) {
                $this->setAccountGroup($eachGroup);
                $this->checkIfGroupHasLedgerAndGroup($key);
                $this->validateGroup($key);
            }
        }

    }


}

