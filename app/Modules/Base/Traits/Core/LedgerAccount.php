<?php

namespace App\Modules\Base\Traits\Core;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;
use DB;

trait LedgerAccount
{
    /**
     * Assets : Debit
     * Liabilities: Credit
     * Equity: Credit
     * Revenue: Credit
     * Cost: Debit
     */

    private $resRecPay = [];

    function getRecPayBalance($group, $end_date, $type)
    {
        $t = New TransactionItem();
        $C = $t->whereHas('transactions', function ($q) use ($end_date) {
            $q->where('transaction_date', '<=', $end_date);
        })->with('ledger')
            ->whereHas('ledger', function ($q) use ($group) {
                $q->where('group_id', $group);
            })
            ->where('dc', 'C')
            ->select('ledger_id', DB::raw('sum(amount) as quantity'))
            ->groupBy('ledger_id')
            ->get();

        $D = $t->whereHas('transactions', function ($q) use ($end_date) {
            $q->where('transaction_date', '<=', $end_date);
        })->with('ledger')
            ->whereHas('ledger', function ($q) use ($group) {
                $q->where('group_id', $group);
            })
            ->where('dc', 'D')
            ->select('ledger_id', DB::raw('sum(amount) as quantity'))
            ->groupBy('ledger_id')
            ->get();

        $acctMain = $type == 'C' ? $C : $D;
        $acctSub = $type == 'C' ? $D : $C;
        $acctMain->map(function ($value, $key) use ($acctSub) {

            $res = $this->findValueinObject($acctSub, 'ledger_id', $value->ledger_id);
            if (!empty($res)) {
                if (($value->quantity - $res->quantity != 0)) {
                    $this->resRecPay[$key]['name'] = $value->ledger->name;
                    $this->resRecPay[$key]['id'] = $value->ledger->id;
                    $this->resRecPay[$key]['due'] = $value->quantity;
                    $this->resRecPay[$key]['paid'] = $res->quantity;
                    $this->resRecPay[$key]['balance'] = $value->quantity - $res->quantity;
                }
            } else {
                $this->resRecPay[$key]['name'] = $value->ledger->name;
                $this->resRecPay[$key]['id'] = $value->ledger->id;
                $this->resRecPay[$key]['due'] = $value->quantity;
                $this->resRecPay[$key]['paid'] = 0;
                $this->resRecPay[$key]['balance'] = $value->quantity;
            }

        });
        // dd($this->resRecPay);

        return $this->resRecPay;
    }

    function findValueinObject($object, $key, $value)
    {
        return $object->filter(function ($val) use ($key, $value) {
            return $val->{$key} == $value;
        })->first();

    }

    function getBalance($dc, $id, $lowerLimit, $upperLimit, $pointer)
    {

        $upperLimitdate = null;
        $lowerLimitdate = null;
        if (!empty($lowerLimit)) {
            $lowerLimitdate = \Carbon\Carbon::today()->subDays($lowerLimit);
            $lowerLimitdate = $lowerLimitdate->toDateString();
        }
        if (!empty($upperLimit)) {
            $upperLimitdate = \Carbon\Carbon::today()->subDays($upperLimit);
            $upperLimitdate = $upperLimitdate->toDateString();
        }
        $res = TransactionItem::where(function ($q) use ($dc, $id) {
            $q
                ->where('dc', $dc)
                ->where('ledger_id', $id);

        })->whereHas('transactions', function ($q) use ($lowerLimitdate, $upperLimitdate, $pointer) {
            if ($pointer == "above") {
                if (!empty($upperLimitdate)) {
                    $q->whereDate('transaction_date', '<=', $upperLimitdate);
                }
            } else if ($pointer == "below") {
                if (!empty($lowerLimitdate)) {
                    $q->whereDate('transaction_date', '>=', $lowerLimitdate);
                }
            } else {
                if (!empty($lowerLimitdate)) {
                    $q->whereDate('transaction_date', '<=', $lowerLimitdate);
                }
                if (!empty($upperLimitdate)) {
                    $q->whereDate('transaction_date', '>=', $upperLimitdate);
                }
            }

        })
            //->toSql();
            ->sum('amount');
        // dd($res);

        return $res;
    }

    function getOffset($balance, $credit, $debit)
    {
        if ($balance == 'C') {
            $bal = $credit - $debit;
        } else if ($balance == 'D') {
            $bal = $debit - $credit;
        }
        return $bal;
    }


    function loopAgedReceivablesLedger($id)
    {

        if ($this->checkForNull($id, 'D')) {
            $data['180_above_days'] = 0;
            $data['90_180_days'] = 0;
            $data['60_90_days'] = 0;
            $data['30_60_days'] = 0;
            $data['30_days'] = 0;
            $data['total'] = 0;
        } else {
            $totalCredit = $credit = $this->getOffsetBalance($id, 'C');
            $data['180_above_days'] = $this->getBalance('D', $id, null, 180, 'above');
            $data['90_180_days'] = $this->getBalance('D', $id, 90, 180, null);
            $data['60_90_days'] = $this->getBalance('D', $id, 60, 90, null);
            $data['30_60_days'] = $this->getBalance('D', $id, 30, 60, null);
            $data['30_days'] = $this->getBalance('D', $id, 30, null, 'below');


            if ($totalCredit != 0) {
                foreach ($data as $key => $each) {
                    if ($totalCredit > $each) {
                        $data[$key] = 0;
                        $totalCredit -= $each;
                    } else if ($totalCredit <= $each) {
                        $data[$key] = $each - $totalCredit;
                        $totalCredit = 0;
                    } else if ($totalCredit == 0) {
                        break;
                    }
                }
            }
            $totalDebit = $this->getOffsetBalance($id, 'D');
            $data['total'] = $totalDebit - $credit;
        }
        return $data;
    }

    function loopAgedPayablesLedger($id)
    {

        if ($this->checkForNull($id, 'C')) {
            $data['180_above_days'] = 0;
            $data['90_180_days'] = 0;
            $data['60_90_days'] = 0;
            $data['30_60_days'] = 0;
            $data['30_days'] = 0;
            $data['total'] = 0;
        } else {
            $totalDebit = $debit = $this->getOffsetBalance($id, 'D');
            $data['180_above_days'] = $this->getBalance('C', $id, null, 180, 'above');
            $data['90_180_days'] = $this->getBalance('C', $id, 90, 180, null);
            $data['60_90_days'] = $this->getBalance('C', $id, 60, 90, null);
            $data['30_60_days'] = $this->getBalance('C', $id, 30, 60, null);
            $data['30_days'] = $this->getBalance('C', $id, 30, null, 'below');
            if ($totalDebit != 0) {
                foreach ($data as $key => $each) {
                    if ($totalDebit > $each) {
                        $data[$key] = 0;
                        $totalDebit -= $each;
                    } else if ($totalDebit <= $each) {
                        $data[$key] = $each - $totalDebit;
                        $totalDebit = 0;
                    } else if ($totalDebit == 0) {
                        break;
                    }
                }
            }
            // dd($data);

            $totalCredit = $this->getOffsetBalance($id, 'C');
            $data['total'] = $totalCredit - $debit;
        }
        return $data;
    }

    function getOffsetBalance($id, $balance)
    {
        return $this->getBalance($balance, $id, null, null, null);
    }


    function checkForNull($id, $balance)
    {
        $credit = $this->getOffsetBalance($id, 'C');
        $debit = $this->getOffsetBalance($id, 'D');

        $bal = $this->getOffset($balance, $credit, $debit);
        if ($bal <= 0) {
            return true;
        }
        return false;
    }

    function getLedgerBalanceByDate($balance, $id, $lowerLimit, $upperLimit, $pointer)
    {

        $upperLimitdate = null;
        $lowerLimitdate = null;
        if (!empty($lowerLimit)) {
            $lowerLimitdate = \Carbon\Carbon::today()->subDays($lowerLimit);
            $lowerLimitdate = $lowerLimitdate->toDateString();
        }
        if (!empty($upperLimit)) {
            $upperLimitdate = \Carbon\Carbon::today()->subDays($upperLimit);
            $upperLimitdate = $upperLimitdate->toDateString();
        }


        $credit = $this->getBalance('C', $id, null, null, null);
        $debit = $this->getBalance('D', $id, null, null, null);
        $bal = $this->getOffset($balance, $credit, $debit);

//        pr($balance);
//        pr($debit);
//        pr($credit);
//        pr($bal);
//
//        exit;
        if ($bal == 0) {
            return 0;
        }
        $credit = $this->getBalance('C', $id, $lowerLimitdate, $upperLimitdate, $pointer);
        $debit = $this->getBalance('D', $id, $lowerLimitdate, $upperLimitdate, $pointer);
        $bal = $this->getOffset($balance, $credit, $debit);

        if ($bal == 0) {
            return 0;
        }


//        pr($balance);
//        pr($debit);
//        pr($credit);
//        pr($bal);
//
//        exit;
//
//        exit;
        return $bal;

    }

}

