<?php

namespace App\Modules\Base\Traits\Core;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;
use DB;

trait Tree
{
    /**
     * Assets : Debit
     * Liabilities: Credit
     * Equity: Credit
     * Revenue: Credit
     * Cost: Debit
     */

    function hasDescendants()
    {
        return !AccountGroup::find($this->id)->descendants->isEmpty();
    }

    function hasSystemLedgers()
    {
        return !Ledger::where('group_id', $this->id)
            ->where('is_system_account', 1)
            ->get()->isEmpty();
    }


    function hasLedgers()
    {
        return !Ledger::where('group_id', $this->id)
            ->get()->isEmpty();
    }

    function directChildren()
    {
        return AccountGroup::where('parent_id', $this->id)->orderBy('code', 'asc')->get();
    }


    function equityLedgers()
    {
        return Ledger::where('group_id', 3)
            ->orderBy('code', 'asc')
            ->where('flag', 'Active')
            ->get();
    }

    function systemLedgers()
    {
        return Ledger::where('group_id', $this->id)
            ->where('is_system_account', 1)
            ->where('flag', 'Active')
            ->orderBy('code', 'asc')
            ->get();
    }


    function allLedgers()
    {
        return Ledger::where('group_id', $this->id)
            ->orderBy('code', 'asc')
            ->where('flag', 'Active')
            ->get();
    }

}

