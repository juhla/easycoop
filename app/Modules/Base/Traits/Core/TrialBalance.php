<?php

namespace App\Modules\Base\Traits\Core;

use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;
use DB;

trait TrialBalance
{


    /**
     * Assets : Debit
     * Liabilities: Credit
     * Equity: Credit
     * Revenue: Credit
     * Cost: Debit
     */


    /**
     * date Format (Y-m-d)
     * @param $DC
     * @param $start_date
     * @param $end_date
     * @return mixed
     */

    function calculateTotalBalance($DC, $start_date, $end_date)
    {


        return TransactionItem::
        with(['transactions', 'ledger'])
            ->whereHas('transactions', function ($q) use ($start_date, $end_date) {
                if (($start_date)) {
                    $q->whereDate('transaction_date', '>=', $start_date);
                }
                if (($end_date)) {
                    $q->whereDate('transaction_date', '<=', $end_date);
                }
                //$q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
            })
            ->where('dc', $DC)
            ->sum('amount');
    }


    function calculateTotalGroupBalance($DC, $start_date, $end_date)
    {


        return TransactionItem::
        with(['transactions', 'ledger'])
            ->whereHas('transactions', function ($q) use ($start_date, $end_date) {
                if (($start_date)) {
                    $q->whereDate('transaction_date', '>=', $start_date);
                }
                if (($end_date)) {
                    $q->whereDate('transaction_date', '<=', $end_date);
                }
                //$q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
            })
            ->where('dc', $DC)
            ->sum('amount');
    }


    function checkIsBalanced()
    {
        $fiscalYear = FiscalYear::getCurrentFiscalYear();

        $begins = $fiscalYear->begins;
        $ends = $fiscalYear->ends;
        $debitBalance = $this->calculateTrialBalanceTotal('D', $begins, $ends);
        $creditBalance = $this->calculateTrialBalanceTotal('C', $begins, $ends);


        $debitBalance = (int)$debitBalance;
        $creditBalance = (int)$creditBalance;
        if ($debitBalance !== $creditBalance) {
            flash()->error('Your Trial Balance is not balanced');
        }

    }

    /**
     *  This method is to check if trial Balance Balances Only
     * @param $DC
     * @param $start_date
     * @param $end_date
     * @return mixed
     */
    function calculateTrialBalanceTotal($DC, $start_date = null, $end_date = null)
    {
        $group = AccountGroup::where('parent_id', null)->get();
        $sum = [];
        //$this->bootTrait();
        foreach ($group as $eachParent) {
            $name = $eachParent->name;
            if (in_array($name, ['Assets', 'Liabilities', 'Equity'])) {
                $start_date = null;
            }
            $add = $this->__groupBalance($eachParent->id, $DC, $start_date, $end_date, true);
            $sum[$eachParent->name] = $add;
        }
        return array_sum($sum);
    }


}

