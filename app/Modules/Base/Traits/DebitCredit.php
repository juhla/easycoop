<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

trait DebitCredit
{

    protected $isTransactionError = false;
    protected $savedTransaction;
    protected $transaction;
    protected $eachTransactionItem;
    protected $transactionItem = [];
    protected $errorArray;
    protected $eachTransaction = [];
    protected $DebitCreditErrorMessage = [];


    public function flattenedError($glue = ' ')
    {
        return implode($glue, Arr::flatten($this->getErrorMessage()));
    }

    /**
     * @return mixed
     */
    public function getTransactionItem()
    {
        return $this->transactionItem;
    }

    /**
     * @param mixed $transactionItem
     */
    public function setTransactionItem($transactionItem)
    {
        $this->transactionItem[] = $transactionItem;
    }


    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    function makePosting($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        };

        $count = 0;
        foreach ($data as $eachArray) {
            $count++;

            if (!empty($eachArray->id)) {
                $this->editPosting($eachArray);
                continue;
            }
            $errorArray = ":: Index [ " . $count . " ]";
            $this->eachTransaction = $eachArray;
            $this->errorArray = $errorArray;
            $this->handleTransaction();
            if (!$this->getisError()) {
                try {

                    DB::connection('tenant_conn')->beginTransaction();


                    $this->eachTransaction->created_by = Auth::user()->id;
                    // transaction
                    $transaction = new Transaction($this->eachTransaction->toArray());
                    $transaction->save();


                    // transaction Items
                    $transactionItems = $transaction->item();
                    $transactionItems->saveMany($this->transactionItem);
                    $this->savedTransaction = $transaction;

                    DB::connection('tenant_conn')->commit();
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                } catch (\Throwable $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                }
            }
        }
    }

    function deletePosting($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        };
        $count = 0;
        foreach ($data as $eachArray) {
            $count++;
            $errorArray = ":: Index [ " . $count . " ]";

            $this->eachTransaction = $eachArray;
            $this->errorArray = $errorArray;

            $this->validateEditTransaction();
            if (!$this->getisError()) {
                try {
                    DB::connection('tenant_conn')->beginTransaction();

                    $transaction = Transaction::find($this->eachTransaction->id);

                    $transactionItems = $transaction->item();
                    $transactionItems->delete();
                    $transaction->delete();


                    DB::connection('tenant_conn')->commit();

                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                } catch (\Throwable $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                }

            }
        }
    }

    function editPosting($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        };
        $count = 0;
        foreach ($data as $eachArray) {
            $count++;
            $errorArray = ":: Index [ " . $count . " ]";

            $this->eachTransaction = $eachArray;
            $this->errorArray = $errorArray;

            $this->validateEditTransaction();
            $this->cleanEditTransactionData();
            $this->handleTransaction();


            if (!$this->getisError()) {
                try {

                    DB::connection('tenant_conn')->beginTransaction();
                    // transaction

                    $transaction = Transaction::find($this->eachTransaction->id);
                    $transaction->updated_by = Auth::user()->id;
                    //$transaction->sync = "0";
                    $transaction->reference_no = $this->eachTransaction->reference_no;
                    $transaction->amount = $this->eachTransaction->amount;
                    $transaction->transaction_date = $this->eachTransaction->transaction_date;
                    $transaction->transaction_type = $this->eachTransaction->transaction_type;
                    $transaction->description = $this->eachTransaction->description;
                    $transaction->payment_type = $this->eachTransaction->payment_type;
                    $transaction->bank_id = $this->eachTransaction->bank_id;
                    $transaction->cheque_number = $this->eachTransaction->cheque_number;
                    $transaction->customer_vendor = $this->eachTransaction->customer_vendor;
                    $transaction->pv_receipt_no = $this->eachTransaction->pv_receipt_no;
                    $transaction->memo_posted = $this->eachTransaction->memo_posted;
                    $transaction->flag = $this->eachTransaction->flag;
                    $transaction->update();


                    $transactionItems = $transaction->item();
                    $transactionItems->forceDelete();
                    $transactionItems->saveMany($this->transactionItem);
                    $this->savedTransaction = $transaction;

                    DB::connection('tenant_conn')->commit();
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                } catch (\Throwable $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isTransactionError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                }
            }
        }
    }

    function cleanEditTransactionData()
    {
        $transaction = Transaction::find($this->eachTransaction->id);


        $result = array_merge(
            $transaction->toArray(), $this->eachTransaction->toArray());


        $transactionItem = $result['item'];
        $transaction = new Transaction($result);
        $transaction->item = $transactionItem;
        if (empty($transaction->reference_no)) {
            $transaction->reference_no = 'TRA' . time();
        }
        $this->eachTransaction = $transaction;
    }

    function validateEditTransaction()
    {

        $eachArray = $this->eachTransaction;
        $errorArray = $this->errorArray;
        $rules = [
            'id' => [
                'required',
                'exists:tenant_conn.ca_transactions,id',
            ]
        ];
        $validator = Validator::make($eachArray->toArray(), $rules);
        if ($validator->fails()) {
            $this->isTransactionError = true;
            $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
            return;
        }
    }

    function cleanTransactionData()
    {
        $result = $this->eachTransaction;
        if (empty($result->reference_no)) {
            $result->reference_no = 'TRA' . time();
        }
        $this->eachTransaction = $result;
    }

    function handleTransaction()
    {

        $result = $this->eachTransaction;
        $errorArray = $this->errorArray;
        /***
         * Transaction Model
         */

        if (!($result instanceof Transaction)) {
            $this->isTransactionError = true;
            $this->setErrorMessage("The data is not an object of Transaction " . $errorArray);
            return;
        }
        $this->cleanTransactionData(); // Clean the Data For Saving
        $result = $this->eachTransaction;
        $rules = [
            'reference_no' => 'required',
            'amount' => 'required|numeric',
            'transaction_date' => 'required|date_format:Y-m-d',
            'transaction_type' => 'required',
            'description' => 'required',

        ];
        $validator = Validator::make($result->toArray(), $rules);
        //if validation fails, redirect with errors
        if ($validator->fails()) {
            $this->isTransactionError = true;
            $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
            return;
        }

        $this->eachTransaction = $result;
        /***
         * Transaction Items Model
         * index = transaction_items
         */

        //transaction_items


        // Check if Object is Empty or is not an Array
        if (!empty($result->item) && is_array($result->item)) {

            $debitLeg = $creditLeg = 0;
            foreach ($result->item as $eachItems) {
                if (!($eachItems instanceof TransactionItem)) {
                    $this->isTransactionError = true;
                    $this->setErrorMessage("The data is not an object of Transaction Item " . $errorArray);
                    return;
                }


                $rules = [
                    'ledger_id' => [
                        'required',
                        'exists:tenant_conn.ca_ledgers,id',
                    ],
                    'dc' => 'required',
                    'amount' => 'required',
                    'item_description' => 'required',

                ];
                $validator = Validator::make($eachItems->toArray(), $rules);
                //if validation fails, redirect with errors
                if ($validator->fails()) {
                    $this->isTransactionError = true;
                    $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
                    return;
                }
                //pr($eachItems->dc);

                if ($eachItems->dc == "D") {
                    $debitLeg += $eachItems->amount;

                } elseif ($eachItems->dc == "C") {
                    $creditLeg += $eachItems->amount;

                }

                //$eachItems->sync = "0";
                $this->setTransactionItem($eachItems);
            }

            // Check if Debit LEg and Credit Leg is Equal
            if ($debitLeg != $creditLeg) {
                $this->isTransactionError = true;
                $this->setErrorMessage("The Debit Leg is not Equal to the credit Leg" . " " . $errorArray);
                return;
            }


//            if ($debitLeg != $result->amount) {
//                $this->isTransactionError = true;
//                $this->setErrorMessage("The Transaction amount does not tally" . " " . $errorArray);
//                return;
//            }


            $this->isTransactionError = false;
            return;
        }

        $this->isTransactionError = true;
        $this->setErrorMessage("Transaction Item is not properly formatted in the data , .. or it doesnt Exist" . " " . $errorArray);
        return;


    }

    function testAccount()
    {

        $formatted = new Transaction;
        $formatted->id = '63870560-decf-11e9-8dd6-4fe2fb95293e';
        $formatted->transaction_date = '2019-02-10';
        $formatted->amount = 4000;
        $formatted->transaction_type = 'api';
        $formatted->description = "first transaction";


        $transactionItem = new TransactionItem;
        $transactionItem->ledger_id = 10;
        $transactionItem->dc = "D";
        $transactionItem->amount = 2000;
        $transactionItem->item_description = "DESC";


        $transactionItem2 = new TransactionItem;
        $transactionItem2->ledger_id = 11;
        $transactionItem2->dc = "C";
        $transactionItem2->amount = 2000;
        $transactionItem2->item_description = "DESC";


        $itemArray = [$transactionItem, $transactionItem2];
        $formatted->item = $itemArray;

        $this->makePosting($formatted);
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage): void
    {
        $this->DebitCreditErrorMessage[] = $errorMessage;
    }

    public function response(array $errors)
    {
        return $messages = implode(' ', Arr::flatten($errors));
    }

    /**
     * @return mixed
     */
    public function getisError()
    {
        return $this->isTransactionError;
    }

    /**
     * @param mixed $isTransactionError
     */
    public function setIsError($isError): void
    {
        $this->isTransactionError = $isError;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->DebitCreditErrorMessage;
    }
}

