<?php

namespace App\Modules\Base\Traits;

class Export
{

    public static function exportTo($type, array $data)
    {
        $title = $data['title'];
        $title = substr($title, 0, 30);
        \Excel::create($title, function ($excel) use ($data, $title) {
            $excel->sheet($title, function ($sheet) use ($data) {
                $sheet->setOrientation('landscape');
                $sheet->loadView($data['view'], $data);
            });
        })->export($type);
    }

}