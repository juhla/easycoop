<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;

trait Functions
{

    function profilePage()
    {
        //$this->createCompanyLogo();
        //get the user details
        $data['title'] = 'Profile';
        //get personal info
        $data['personalInfo'] = PersonalInfo::where('user_id', Auth::user()->id)->first();
        $data['companyInfo'] = Company::where('user_id', Auth::user()->id)->first();
        return $data;
    }


}

