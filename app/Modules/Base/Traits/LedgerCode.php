<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Ledger;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\EntrustFacade as Entrust;

trait LedgerCode
{


    /**
     * function to get the last bank account code
     * add 1 to it and return for new vendor code
     * @return string
     */
    public static function getLastBankAccountCode($operand = 1)
    {
        $code = self::generateCode(16, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand);
            return self::getLastBankAccountCode($operand);
        }
    }

    /**
     * function to get the last receivable account code
     * add 1 to it and return for new customer code
     * @return string
     */
    public static function getLastReceivableCode($operand = 0.01)
    {

        $code = self::generateCode(12, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastReceivableCode($operand);
        }
    }


    /**
     * function to get the last payable account code
     * add 1 to it and return for new vendor code
     * @return string
     */
    public static function getLastPayableCode($operand = 0.01)
    {
        $code = self::generateCode(28, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastPayableCode($operand);
        }

    }


    public static function generateCode($groupID, $operand = 0.01)
    {
        if ($groupID == 28) {
            $account = Ledger::where('group_id', 28)->orderBy('id', 'desc')->first();
            if ($account) {
                //get the code
                $code = $account->code;
                //add 1 to the code
                $newCode = bcadd($code, $operand, 2);
            } else {
                $newCode = '2210.01';
            }

            return $newCode;
        }

        if ($groupID == 12) {
            $account = Ledger::where('group_id', 12)->orderBy('id', 'desc')->first();
            if ($account) {
                //get the code
                $code = $account->code;
                //add 1 to the code
                return bcadd($code, $operand, 2);
            } else {
                return '1151.01';
            }
        }

        if ($groupID == 45) {
            $account = Ledger::where('group_id', 45)->orderBy('id', 'desc')->first();
            if ($account) {
                $code = $account->code;
                return bcadd($code,$operand, 2);
            } else {
                return '1150.01';
            }
        }

        if ($groupID == 44) {
            $account = Ledger::where('group_id', 44)->orderBy('id', 'desc')->first();
            if ($account) {
                $code = $account->code;
                return bcadd($code, $operand, 2);
            } else {
                return '2270.01';
            }
        }


        if ($groupID == 46) {
            $account = Ledger::where('group_id', 46)->orderBy('id', 'desc')->first();
            if ($account) {
                $code = $account->code;
                return bcadd($code, $operand, 2);
            } else {
                return '2150.01';
            }
        }

        if ($groupID == 47) {
            $account = Ledger::where('group_id', 47)->orderBy('id', 'desc')->first();
            if ($account) {
                $code = $account->code;
                return bcadd($code, $operand, 2);
            } else {
                return '3500.01';
            }
        }


        if ($groupID == 16) {
            //Receivable Account id is 11
            //find the last ledger under Receivable Account
            $account = Ledger::where('group_id', 16)
                // ->where('flag', 'Active')
                ->orderBy('id', 'desc')->first();
            //get the code
            $code = $account->code;
            //add 1 to the code
            $newCode = bcadd($code, $operand);
            return $newCode;
        }

    }


    public static function getLastLoanCode($operand = 0.01)
    {

        $code = self::generateCode(45, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastLoanCode($operand);
        }
    }


    public static function getLastTargetSavingsCode($operand = 0.01)
    {

        $code = self::generateCode(46, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastTargetSavingsCode($operand);
        }
    }


    public static function getLastMemberEquityCode($operand = 0.01)
    {

        $code = self::generateCode(47, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastMemberEquityCode($operand);
        }
    }


    public static function getLastContributionsCode($operand = 0.01)
    {

        $code = self::generateCode(44, $operand);
        $result = Ledger::where('code', $code)->get();
        if ($result->isEmpty()) {
            return $code;
        } else {
            $operand = bcadd($operand, $operand, 2);
            return self::getLastContributionsCode($operand);
        }
    }


}
