<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Models\UserLicense;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

trait License
{


    public $error = '';

    public function licenseError()
    {
        return $this->error;
    }

    public function isUserImsActivated()
    {
        //120
        /**
         * 1) check if the user has a ims license
         * 2) check if the user's ims license is active (not expired)
         * 3) check if the user's ims license annual isnt due
         */
        $userID = getBusinessOwnerID();
        $today = Carbon::today();
        $license = ImsLicenceSubscriber::where('user_id', $userID)->first();
        if (empty($license)) {
            $this->updateImsFree();
            return true;
            //$this->error = "You have not paid for IMS License, please make a payment to continue";
            //return false;
        }

        if (($license->expiry_date < $today->toDateString())) {
            $this->error = "Your License Has Expired";
            return false;
        }

        if ($license->expiry_date < $today->toDateString()) {
            $this->error = "Your Maintenance License Has Expired";
            return false;
        }

        return true;


    }

    public function updatePayrollFree()
    {
        $license = new PayrollLicenceSubscriber();
        $userID = getBusinessOwnerID();
        $license->user_id = $userID;
        $license->license_id = 18; //default HR Basic
        $created_at = Auth::user()->created_at->toDateString();
        //$effectiveDate = date('Y-m-d', strtotime("+1 month", strtotime($created_at)));
        $effectiveDate = date('Y-m-d', strtotime('+1 month'));
        $license->expiry_date = $effectiveDate;
        $license->save();
    }

    public function updateImsFree()
    {
        $license = new ImsLicenceSubscriber();
        $userID = getBusinessOwnerID();
        $license->user_id = $userID;
        $license->license_id = 8;
        $created_at = Auth::user()->created_at->toDateString();
        //$effectiveDate = date('Y-m-d', strtotime("+1 month", strtotime($created_at)));
        $effectiveDate = date('Y-m-d', strtotime('+1 month'));
        $license->expiry_date = $effectiveDate;
        $license->save();
    }

    public function isPayrollLimitExceeded()
    {

        $userID = getBusinessOwnerID();
        //PayrollLicenceSubscriber::createTableifNonExist();
        $license = PayrollLicenceSubscriber::where('user_id', $userID)->first();
        $payrollLicenceCount = $license->count;
        try {
            $licencePackageLimit = $license->license->no_of_users;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            PayrollLicenceSubscriber::whereUser_id($userID)
                ->update(['license_id' => 18]);

            $license = PayrollLicenceSubscriber::where('user_id', $userID)->first();
            $licencePackageLimit = $license->license->no_of_users;
        }

        if ($payrollLicenceCount >= $licencePackageLimit) {
            $this->error = "You have reach the limit for this License. Please upgrade!";
            return true;
        }

        return false;
    }

    public function isUserPayrollActivated()
    {
        //120
        /**
         * 1) check if the user has a payroll license
         * 2) check if the user's payroll license is active (not expired)
         * 3) check if the user's payroll license annual isnt due
         */
        $userID = getBusinessOwnerID();
        $today = Carbon::today();
        $license = PayrollLicenceSubscriber::where('user_id', $userID)->first();
        if (empty($license)) {
            $this->updatePayrollFree();
            return true;
            //$this->error = "You have not paid for Payroll License, please make a payment to continue";
//            return false;
        }

        if (($license->expiry_date < $today->toDateString())) {
            $this->error = "Your Payroll Annual License Has Expired";
            return false;
        }

        if ($license->expiry_date < $today->toDateString()) {
            $this->error = "Your Payroll Maintenance License Has Expired";
            return false;
        }

        return true;

    }

    public function renewLicense()
    {
        try {
            $license = $this->getUserLicense();
            $effectiveDate = date('Y-m-d', strtotime("+1 years"));
            $license->expiry_date = $effectiveDate;
            $license->save();
        } catch (\Exception $ex) {
            app('sentry')->captureException($ex);
        }

    }


    public function renewTheLicense($license)
    {
        try {
            //$license = $this->getUserLicense();
            $effectiveDate = date('Y-m-d', strtotime("+1 years"));
            $license->expiry_date = $effectiveDate;
            $license->save();
        } catch (\Exception $ex) {
            app('sentry')->captureException($ex);
        }

    }


    public function createTables()
    {

    }

    public function updateAnnual()
    {

        try {
            $user = Auth::user();
            if (!empty($user) && $user->hasRole(['professional', 'finance-provider', 'trade-promoter', 'agent-finance-provider', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
                return true;
            }
            $today = Carbon::today();
            $license = $this->getUserLicense();

            if (empty($license)) {
                if ($user->hasRole(['basic-business-owner', 'advanced-business-owner', 'business-owner'])) {
                    $license = new UserLicenceSubscriber();
                    $created_at = Auth::user()->created_at->toDateString();
                    //$effectiveDate = date('Y-m-d', strtotime("+1 years", strtotime($created_at)));
                    $effectiveDate = date('Y-m-d', strtotime("+1 months", strtotime($created_at)));
                    $license->expiry_date = $effectiveDate;
                    $license->user_id = Auth::user()->id;
                    $license->license_id = "2";
                    $license->save();
                }

            }
            //dd(($license->expiry_date == "0000-00-00") || empty($license->expiry_date));
            if (($license->expiry_date == "0000-00-00") || empty($license->expiry_date)) {
                // adding one year renewal license for bmac
                $created_at = Auth::user()->created_at->toDateString();
                //$effectiveDate = date('Y-m-d', strtotime("+1 years", strtotime($created_at)));
                $effectiveDate = date('Y-m-d', strtotime("+1 months", strtotime($created_at)));
                $license->expiry_date = $effectiveDate;
                $license->save();
            } else if ($license->expiry_date < $today->toDateString()) {
                return false;
            }
            return true;
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
            return false;
        }
    }


    public function getUserLicense()
    {
        $userID = getMainUserID();
        return UserLicenceSubscriber::where('user_id', $userID)->first();
    }

    public function getExpiryDate($plan_id, $expirydate)
    {
        $plan = UserLicense::find($plan_id);

        if (!is_numeric($plan->duration)) {
            $plan->duration = 1;
        }
        if ($plan->duration == 0) {
            // return date('Y-m-d', strtotime('+1 years', strtotime($expirydate)));
            return date('Y-m-d', strtotime('+1 years'));
        }
        $duration = $plan->duration;
        switch ($plan->duration_type) {
            case "day":
                {
                    //return date('Y-m-d', strtotime("+$duration days", strtotime($expirydate)));
                    return date('Y-m-d', strtotime("+$duration days"));
                }
                break;
            case "week":
                {
                    //return date('Y-m-d', strtotime("+$duration weeks", strtotime($expirydate)));
                    return date('Y-m-d', strtotime("+$duration weeks"));
                }
                break;
            case "month":
                {
                    //return date('Y-m-d', strtotime("+$duration months", strtotime($expirydate)));
                    return date('Y-m-d', strtotime("+$duration months"));
                }
                break;
            case "year":
                {
                    //return date('Y-m-d', strtotime("+$duration years", strtotime($expirydate)));
                    return date('Y-m-d', strtotime("+$duration years"));
                }
                break;
        }


    }

    private function licenceLimit()
    {

    }

}

