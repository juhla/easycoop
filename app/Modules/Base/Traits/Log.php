<?php

namespace App\Modules\Base\Traits;


use Illuminate\Support\Facades\Auth;

trait Log
{

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();
        self::updating(function ($model) {
            $model->created_by = auth()->user()->id;
        });
        static::creating(function ($model) {
            $model->updated_by = auth()->user()->id;
        });
    }
}

