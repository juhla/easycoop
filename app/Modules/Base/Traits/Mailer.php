<?php

namespace App\Modules\Base\Traits;

use Mail;

trait Mailer
{
    protected function notifyAdmins($params)
    {
        try {
            $this->sendMail('emails.notification', $params, 'Error Notification');
        } catch (\Exception $e) {
        }
    }

    protected function sendWelcomeEmail($params)
    {
        try {
            $this->sendMail('emails.welcome', $params, 'Welcome to  BMAC');
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            $this->notifyAdmins([
                'last_name' => 'Admin',
                'first_name' => 'BMAC',
                'body' => $e->getMessage(),
                'email' => 'info@ikooba.com'
            ]);
        }
    }

    protected function sendEmailFrmUserManager($templates, $params)
    {
        try {
            $this->sendMailManager($templates, $params);
        } catch (\Exception $e) {
            $this->notifyAdmins([
                'last_name' => 'Admin',
                'first_name' => 'BMAC',
                'body' => $e->getMessage(),
                'email' => 'info@ikooba.com'
            ]);
        }
    }


    protected function sendMail($template, $params, $subject)
    {
        $params['subject'] = $subject;
        Mail::send($template, $params, function ($m) use ($params, $template) {
            $m->from(config('mail.from.address'), config('mail.from.name'));
            $m->to($params['email'], $params['first_name'] . ' ' . $params['last_name'])->subject($params['subject']);

            try {
                if ($template == 'emails.welcome') {
                    if (!empty(config('constants.welcomeBcc'))) {
                        $welcomeEmail = config('constants.welcomeBcc');
                        foreach ($welcomeEmail as $eachOne) {
                            $m->bcc($eachOne);
                        }

                    }
                }
            } catch (\Exception $ex) {
                app('sentry')->captureException($ex);
            }

        });
    }


    protected function sendMailAssembly($template, $params, $subject)
    {
        $params['subject'] = $subject;
        Mail::send($template, $params, function ($m) use ($params, $template) {
            $m->from(config('mail.from.address'), config('mail.from.name'));
            $m->to($params['email'])->subject($params['subject']);

            try {
                if ($template == 'emails.welcome') {
                    if (!empty(config('constants.welcomeBcc'))) {
                        $welcomeEmail = config('constants.welcomeBcc');
                        foreach ($welcomeEmail as $eachOne) {
                            $m->bcc($eachOne);
                        }

                    }
                }
            } catch (\Exception $ex) {

            }

        });
    }


    protected function sendMailManager($template, $params)
    {
        Mail::send($template, $params, function ($m) use ($params) {
            $m->from(config('mail.from.address'), config('mail.from.name'));

            $m->to($params['email'], $params['first_name'] . ' ' . $params['last_name'], $params['body'])->subject($params['subject']);
        });
    }
}

