<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-10
 * Time: 13:36
 */

namespace App\Modules\Base\Traits;

use App\Modules\Base\Observer\ActiveToSoftDelete;

trait MySoftDeletes
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    public static function bootMySoftDeletes()
    {
        static::observe(new ActiveToSoftDelete);
    }


}