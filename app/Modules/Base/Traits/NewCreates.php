<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\CustomWidget;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\User;
use App\Modules\Invoices\Models\Invoice;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Modules\Base\Models\Widget;

trait NewCreates
{

    function migratePatches()
    {
        try {
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-bmac',
                '--force' => true
            ]);

            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-ims',
                '--force' => true
            ]);

            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-payroll',
                '--force' => true
            ]);
            echo "success | ";

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    function createAdjustmentLedger()
    {
    }


    function createMemoLedger()
    {
    }

    function createLedger()
    {
        $this->createAdjustment();
        $this->createMemo();
    }

    function createMemo()
    {
        try {
            if (empty(Ledger::getAdjustmentID())) {
                $this->createAdjustmentLedger();
            }
        } catch (\Exception $exception) {
            $this->createAdjustmentLedger();
        }
    }

    function createAdjustment()
    {
        try {
            if (empty(Ledger::getMemoID())) {
                $this->createMemoLedger();
            }
        } catch (\Exception $exception) {
            $this->createMemoLedger();
        }
    }


}

