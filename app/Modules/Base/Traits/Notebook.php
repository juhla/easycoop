<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use Illuminate\Support\Facades\Config;

trait Notebook
{

    use Tenant;

    function checkType($transactionID)
    {
        $trans = TransactionItem::where('transaction_id', $transactionID)
            ->pluck('ledger_id')->toArray();
        $memoID = Transaction::getMemoID();
        if (in_array($memoID, $trans)) {
            return true;
        }
        return false;
    }

    static function createInvoiceNo($calledBy)
    {

        $transactionCode = "";
        try {

            $id = 1; //first entry
            if ($calledBy == 'Cash Receipt') {
                $result = CashReceipt::all();
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CR";
            } else if ($calledBy == 'Cheque Receipt') {
                $result = ChequeReceipt::all();
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CHR";
            } else if ($calledBy == 'Credit Sales') {
                $result = CreditSales::all();
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CS";
            } else if ($calledBy == 'Credit Purchase') {
                $result = CreditPurchase::all();
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CRP";
            }

            $string_number = str_pad($id, 6, "0", STR_PAD_LEFT);
            $transactionCode = $code . $string_number;
        } catch (\Exception $ex) {
            //do nothing
        }
        return $transactionCode;
    }

    static function createPVCNo($calledBy)
    {

        $transactionCode = "";
        try {
            $id = 1; //first entry
            if ($calledBy == 'Cash Payment') {
                try {
                    $result = PettyCash::all();
                } catch (\Exception $ex) {

                }
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CP";
            } else if ($calledBy == 'Cheque Payment') {
                $result = ChequePayment::all();
                if (!$result->isEmpty()) {
                    $id = $result->last()->id;
                }
                $code = "#CHP";
            }

            $string_number = str_pad($id, 6, "0", STR_PAD_LEFT);
            $transactionCode = $code . $string_number;
        } catch (\Exception $e) {
            //do nothing
        }

        return $transactionCode;
    }
}

