<?php

namespace App\Modules\Base\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

trait NullingDB
{

    static $DBName;

    /**
     * @return mixed
     */
    public static function getDBName()
    {
        return self::$DBName;
    }

    /**
     * @param mixed $DBName
     */
    public function setDBName($DBName)
    {
        $this->DBName = $DBName;
    }

    public function setAttribute($key, $value)
    {
        if (is_scalar($value)) {
            $value = $this->emptyStringToNull(trim($value));
        }
        return parent::setAttribute($key, $value);
    }

    public function changeColumns()
    {
        foreach ($this->fillable as $eachFillable) {
            $res = self::getNullableColumns($eachFillable, $this);
            if (!empty($res)) {
                $isEmpty = (!isset($this->{$eachFillable})) && (empty($this->{$eachFillable}));
                if (($res->IS_NULLABLE == "NO") && $isEmpty) {
                    $value = "";
                    switch ($res->DATA_TYPE) {
                        case 'varchar':
                        case 'text':
                        case 'char':
                            {
                                $value = "NULL";
                            }
                            break;

                        case 'date':
                            {
                                $value = Carbon::now()->toDateString();
                            }
                            break;

                        case 'int':
                        case 'double':
                        case 'tinyint':
                        case 'float':
                            {
                                $value = 0;
                            }
                            break;
                        case 'enum':
                            {
                                $value = isset($res->COLUMN_DEFAULT) ? $res->COLUMN_DEFAULT : $this->trimEnum($res->COLUMN_TYPE);
                            }
                            break;
                    }
                    $value = empty($res->COLUMN_DEFAULT) ? $value : $res->COLUMN_DEFAULT;
                    $this->{$eachFillable} = $value;
                }
            }
        }
    }

    public function trimEnum($subject)
    {
        $search = 'enum(';
        $trimmed = str_replace($search, '', $subject);
        $search = ')';
        $trimmed = str_replace($search, '', $trimmed);
        $explode = explode(",", $trimmed);
        $value = $explode['0'];
        return $value;
    }

    /**
     * Boot the trait, add a saving observer.
     *
     * When saving the model, we iterate over its attributes and for any attribute
     * marked as nullable whose value is empty, we then set its value to null.
     */
    protected static function bootNullingDB()
    {
        static::saving(function ($model) {
            $DB = Config::get('database.connections.mysql.database');
            if ($model->connection == 'tenant_conn') {
                $DB = session('company_db') or Config::get('database.connections.tenant_conn.database');
            }
            self::$DBName = $DB;
            $model->changeColumns();

        });
//        static::creating(function ($model) {
        //        });
        //        static::updating(function ($model) {
        //        });
    }

    /**
     * return null value if the string is empty otherwise it returns what every the value is
     *
     */
    private function emptyStringToNull($string)
    {
        //trim every value
        $string = trim($string);

        if ($string === '') {
            return null;
        }

        return $string;
    }

    public static function getNullableColumns($columnName, $model)
    {
        try {
            $table = $model->table;
            $DB = self::getDBName();
            $results = DB::select("SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE
                    FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '$DB' AND TABLE_NAME = '$table' AND COLUMN_NAME = '$columnName'   ");

            return @$results[0];
        } catch (\Exception $exception) {
            return null;
        }

    }

}
