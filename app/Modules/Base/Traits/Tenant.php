<?php

namespace App\Modules\Base\Traits;

use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

trait Tenant
{
    use TenantConfig;

    public function __construct()
    {
        $session = session('company_db');

        if (Auth::check()) {
            $user = Auth::user();
            if (empty($session)) {
                session()->put('company_db', $user->db);
                $this->setConfig(session()->get('company_db'));
            } else {
                $this->setConfig(session()->get('company_db'));
            }
        }
    }

}

