<?php

namespace App\Modules\Base\Traits;

use Illuminate\Support\Facades\Config;

trait TenantConfig
{

    protected function setConfig($db)
    {
        //pr(Config::get('database.connections.mysql'));exit;
        Config::set('database.connections.tenant_conn', [
            'driver' => 'mysql',
            'host' => Config::get('database.connections.mysql.host'),
            'database' => $db,
            'username' => Config::get('database.connections.mysql.username'),
            'password' => Config::get('database.connections.mysql.password'),
            'charset' => Config::get('database.connections.mysql.charset'),
            'collation' => Config::get('database.connections.mysql.collation'),
            'prefix' => '',
        ]);
    }

}

