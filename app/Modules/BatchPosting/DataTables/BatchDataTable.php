<?php

namespace App\Modules\BatchPosting\DataTables;

use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\TransactionItem;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class BatchDataTable extends DataTable
{
    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable
            ->addColumn('debit', function ($__res) {
                return ($__res->dc == 'D') ? formatNumber($__res->amount) : "";
            })
            ->addColumn('pcv', function ($__res) {
                return $__res->transactions_without_scope->pv_receipt_no;
            })
            ->editColumn('approve_status', function ($__res) {
                if ($__res->approve_status == 0) {
                    $approve_status = 'Pending';
                } elseif ($__res->approve_status == 1) {
                    $approve_status = 'Approved';
                } else {
                    $approve_status = 'Rejected';
                }
                return $approve_status;
            })
            ->addColumn('description', function ($__res) {
                return $__res->item_description;
            })
            ->addColumn('credit', function ($__res) {
                return ($__res->dc == 'C') ? formatNumber($__res->amount) : "";
            })
            ->filterColumn('description', function ($query, $keyword) {
                $query->whereHas('transactions_without_scope', function ($query) use ($keyword) {
                    $sql = "item_description  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->filterColumn('debit', function ($query, $keyword) {
                $query->whereHas('ledger', function ($query) use ($keyword) {
                    $sql = "amount  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->addColumn('account', function ($__res) {
                return $__res->ledger->name;
            })
            ->filterColumn('credit', function ($query, $keyword) {
                $query->whereHas('ledger', function ($query) use ($keyword) {
                    $sql = "amount  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->filterColumn('pcv', function ($query, $keyword) {
                $query->whereHas('transactions_without_scope', function ($query) use ($keyword) {
                    $sql = "pv_receipt_no  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->editColumn('date', function ($__res) {
                return date('d/m/Y', strtotime($__res->transactions_without_scope->transaction_date));
            })
            ->orderColumn('date', 'ca_transaction_items.created_at $1')
            ->addColumn('action', function ($entry) {
                return view('batch_posting::actions.datatables_actions', compact('entry'))->render();
            })
            ->rawColumns(['action']);

        return $dataTable;

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TransactionItem $model)
    {

        $type = $this->getType();
        $start_date = session('start_date');
        $end_date = session('end_date');
        $approve_status = session('approve_status');

        $condition = array(
            'flag' => 'Active',
            'transaction_type' => $type,
        );

        $query = $model->whereHas('transactions_without_scope', function ($q) use ($condition) {
            $q->where($condition);
        })->withoutGlobalScope('active')->newQuery();

        if (!empty($start_date)) {
            $start = Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            $query->whereHas('transactions_without_scope', function ($q) use ($start) {
                $q->where('transaction_date', '>=', $start);
            });
        }
        if (!empty($end_date)) {
            $end = Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');
            $query->whereHas('transactions_without_scope', function ($q) use ($end) {
                $q->where('transaction_date', '<=', $end);
            });
        }

        if (empty($start_date) && empty($end_date)) {

            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $query->whereHas('transactions_without_scope', function ($q) use ($fiscalYear) {
                $q->whereBetween('transaction_date', [$fiscalYear->begins, $fiscalYear->ends]);
            });
        }

        if (!empty($approve_status)) {
            if ($approve_status == 'Pending') {
                $__status = '0';
            } else if ($approve_status == 'Approved') {
                $__status = '1';
            } else if ($approve_status == 'Rejected') {
                $__status = '-1';
            } else {
                $__status = '';
            }
            if ($__status != '') {
                $query->where('ca_transaction_items.approve_status', '=', $__status);
            }
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('batchDT')
            ->columns($this->getColumns())
            ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['title' => 'Date', 'data' => 'date', 'footer' => 'date', 'searchable' => false],
            ['title' => 'Status', 'data' => 'approve_status', 'footer' => 'approve_status'],
            ['title' => 'PCV No', 'data' => 'pcv', 'footer' => 'pcv'],
            ['title' => 'Description', 'data' => 'description', 'footer' => 'description'],
            ['title' => 'Account', 'data' => 'account', 'footer' => 'account'],
            ['title' => 'Debit', 'data' => 'debit', 'footer' => 'debit'],
            ['title' => 'Credit', 'data' => 'credit', 'footer' => 'credit'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'batch_' . date('YmdHis');
    }
}
