<?php

namespace App\Modules\BatchPosting\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\BatchPosting\DataTables\BatchDataTable;

use App\Modules\BatchPosting\Models\Transaction;
use App\Modules\BatchPosting\Models\TransactionItem;
use App\Modules\BatchPosting\Models\Batch;
use Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BatchController extends Controller
{
    public function __construct(Request $request, Batch $batch)
    {
        $this->middleware(['tenant']);
        $this->request = $request;
        $this->batch = $batch;
    }

    /**
     * method to display all batch entry
     * @return $this
     */
    public function index(BatchDataTable $table)
    {

        //dd($fiscalYear);
        $inputs = request()->all();
        $this->hasAccess(['add_journal_entry', 'view_journal_entry']);
        $title = 'Transaction Entry (Batch Posting)';
        $type = 'batch-posting';

        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');

        $table->setType($type);
        return $table->render('batch_posting::index', compact('type', 'title', 'start_date', 'end_date'));

    }

    // Display form for Transaction Entry
    public function newTransaction()
    {
        $this->hasAccess(['add_journal_entry', 'view_journal_entry']);
        $data['title'] = 'New Transaction Entry';
        return view('batch_posting::batch_form')->with($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function save()
    {

        $this->hasAccess(['add_journal_entry']);
        $input = request()->all();

        $verifyDate = Transaction::verifyTransactionDate($input['transaction_date']);
        if ($verifyDate === 'yes') {
            $result = new Batch();
            $result->saveTransaction($input);
            if ($result->getisError()) {
                return response()->json(['message' => $result->getErrorMessage(), 'statusCode' => 500]);
            }
            return response()->json(['message' => 'Transaction successfully saved.', 'statusCode' => 200]);
        } elseif ($verifyDate === 'no') {
            if (request()->ajax()) {
                return response()->json(['statusCode' => 500, 'message' => 'Transaction date must be between current Fiscal Year']);
            } else {
                flash()->error('Transaction date must be between current Fiscal Year');
                return redirect()->back();
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['statusCode' => 500, 'message' => 'Please setup a Fiscal Year before entering transactions']);
            } else {
                flash()->success('Please setup a Fiscal Year before entering transactions');
                return redirect()->back();
            }
        }

    }

    public function edit($id = '0')
    {
        $this->hasAccess(['edit_journal_entry']);

        $id = base64_decode($id);
        //pr($id);
        $batch = Transaction::where('id', $id)->with('item')->first();
        //pr($journal);exit;
        if ($batch) {
            $data['title'] = 'Edit Transaction';
            $data['batch'] = $batch;
            return view('batch_posting::edit_batch')->with($data);
        }

        return redirect('/batch-entry');

    }

    public function update()
    {

        $this->hasAccess(['edit_journal_entry']);
        $input = $this->request->all();
        $result = new Batch();
        $result->updateTransaction($input);
        if ($result->getisError()) {
            return response()->json(['message' => $result->getErrorMessage(), 'statusCode' => 500]);
        }
        return response()->json(['message' => 'Transaction saved successfully.', 'statusCode' => 200]);
    }

    public function delSingle($id)
    {
        $this->hasAccess(['delete_journal_entry']);
        if ($this->batch->delTransactionSingle($id)) {
            return response()->json(['message' => 'Transaction entry deleted successfully.', 'statusCode' => 200]);
        } else {
            return response()->json(['message' => 'Could not delete this entry.', 'statusCode' => 500]);
        }

    }

    public function delete()
    {
        $this->hasAccess(['delete_journal_entry']);
        $input = $this->request->only('ids');
        if ($this->batch->deleteTransaction($input)) {
            return response()->json(['message' => 'Transaction was successfully deleted.', 'statusCode' => 200]);
        } else {
            return response()->json(['message' => 'Could not delete transaction at this moment.', 'statusCode' => 500]);
        }

    }
}
