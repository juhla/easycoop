<?php

namespace App\Modules\BatchPosting\Models;

use Faker\Provider\cs_CZ\DateTime;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\TransactionItem;
use Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Builder;

class Transaction extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable, NullableFields, MySoftDeletes;

    protected $connection = 'tenant_conn';

    protected $table = 'ca_transactions';

    protected $nullable = [
        'reference_no',
        'description',
        'payment_type',
        'bank_id',
        'cheque_number',
        'customer_vendor',
        'pv_receipt_no',
        'memo_posted',
    ];

    protected $casts = [
        'transaction_type' => 'string'
    ];

    protected $fillable = ['reference_no',
        'transaction_type', 'transaction_date', 'description',
        'amount', 'payment_type', 'customer_vendor', 'bank_id', 'pv_receipt_no',
        'cheque_number', 'updated_by', 'created_by'
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('ca_transactions.flag', 'Active');
        });

    }

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function item()
    {
        return $this->hasMany('App\Modules\Base\Models\TransactionItem', 'transaction_id');
    }

    public static function getTheTransaction($transaction_id)
    {
        return Transaction::where('ca_transactions.flag', 'Active')
            ->with(['item'])
            ->where('ca_transactions.id', $transaction_id)
            ->get()->first();

    }

    public static function getMemoID()
    {
        $ledgers = Ledger::where('name', 'Memorandum Account')->first();
        return $ledgers->id;

    }

    public static function getMemorandumTransactions($start_date = null, $end_date = null)
    {

        $ledgerID = self::getMemoID();

        if (!is_null($start_date) && !is_null($end_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');

            $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');

            $res = Transaction::where('ca_transactions.flag', 'Active')
                ->join('ca_transaction_items', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transaction_items.ledger_id', $ledgerID)
                ->where('ca_transactions.memo_posted', 0)
                ->select('ca_transactions.*')
                ->whereBetween('ca_transactions.transaction_date', [$start, $end])
                ->orderBy('ca_transactions.transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::where('ca_transactions.flag', 'Active')
                ->join('ca_transaction_items', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                ->where('ca_transaction_items.ledger_id', $ledgerID)
                ->where('ca_transactions.memo_posted', 0)
                ->select('ca_transactions.*')
                ->orderBy('ca_transactions.transaction_date', 'desc')
                ->paginate(30);
        }

        //dd($res);
        return $res;

    }

    /**
     * get current company's transactions
     * @param #item_id
     * @return mixed
     */
    public static function getTransaction($item_id = null, $start_date = null, $end_date = null, $type)
    {
        if ($item_id) {
            $res = Transaction::where('id', $item_id)
                ->first();
        } elseif (!is_null($start_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            if (!is_null($end_date)) {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            } else {
                $end = Carbon\Carbon::now();
                //$end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            }
            $res = Transaction::where('flag', 'Active')
                ->where('transaction_type', $type)
                ->whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::whereDate('created_at', '=', date('Y-m-d'))
                ->where('flag', 'Active')
                ->where('transaction_type', $type)
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }

    /**
     * save inflow
     * @param $input
     * @return Mixed
     */
    public static function saveTransaction($input)
    {

        //remove commas from amount
        $input['amount'] = str_replace(',', '', $input['amount']);

        $date = Carbon\Carbon::createFromFormat('d/m/Y', $input['trans_date']);
        //get input fields
        $transaction = array(
            'transaction_date' => $date->format('Y-m-d'),
            'description' => $input['description'],
            'amount' => $input['amount'],
        );

        if ($input['transaction_type'] !== 'journal') {
            $transaction['payment_type'] = $input['payment_type'];
            $transaction['customer_vendor'] = $input['customer_vendor'];
            $transaction['pv_receipt_no'] = $input['pv_receipt_no'];
            if (isset($input['bank_id'])) {
                $transaction['bank_id'] = $input['bank_id'];
                $transaction['cheque_number'] = $input['cheque_number'];
            }
        }

        $trans_item1 = [
            'ledger_id' => $input['debit_account'],
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'D',
        ];

        $trans_item2 = [
            'ledger_id' => $input['credit_account'],
            'amount' => $input['amount'],
            'item_description' => $input['description'],
            'dc' => 'C'
        ];

        if ($input['transaction_id'] === '') {
            //insert new inflow
            $transaction['transaction_type'] = $input['transaction_type'];
            //if reference no is set from input
            if (isset($input['reference_no'])) {
                $transaction['reference_no'] = $input['reference_no'];
            } else {
                $transaction['reference_no'] = 'TRA' . time();
            }
            $transaction['created_by'] = \Auth::user()->id;
            $info = Transaction::create($transaction);
            //get last insert record
            // $info = Transaction::find($id);
            //get transaction number
            $trans_item1['transaction_id'] = $info->id;
            $trans_item2['transaction_id'] = $info->id;
            //insert transaction item
            TransactionItem::create($trans_item1);
            TransactionItem::create($trans_item2);
            //$notebook = Notebook::where('Notebook_No', $id)->first();

            if ($info->transaction_type === 'inflow') {
                $type = '<li><a href="#">Create Invoice payment</a></li>';
                $class_type = 'text-success';
            } elseif ($info->transaction_type === 'outflow') {
                $type = '<li><a href="#">Create bill payment</a></li>';
                $class_type = 'text-danger';
            }
            $res = '<tr><td><input type="checkbox" name="ids" value="' . $info->id . '"/></td>
                                    <td>' . date('d-m-Y', strtotime($info->transaction_date)) . '</td>
                                    <td>' . $info->description . '</td>
                                    <td><span class="' . $class_type . '">' . number_format($info->amount, 2, '.', ',') . '</span></td>
                                    <td>' . getLedgerName('D', $info->id) . '</td>
                                    <td>' . getLedgerName('C', $info->id) . '</td>
                                    <td>
                                        <div class="btn-group btn-group-sm">
                                            <button type="button" class="btn btn-complete dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu align-xs-left" role="menu"  style="right: 0; left: auto;">
                                                <li><a href="javascript:;" class="view" data-value="' . $info->id . '">View Transaction</a></li>
                                                <li><a href="javascript:;" class="edit" data-value="' . $info->id . '">Edit Transaction</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:;" class="delete-transaction" data-value="' . $info->id . '">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td></tr>';
            return $res;
        } else {
            $transaction['updated_by'] = \Auth::user()->id;
            //update transactions table
            Transaction::where('id', $input['transaction_id'])->update($transaction);
            //update transaction items table
            TransactionItem::where('transaction_id', $input['transaction_id'])->where('dc', 'D')->update($trans_item1);
            TransactionItem::where('transaction_id', $input['transaction_id'])->where('dc', 'C')->update($trans_item2);

            //get transaction
            $info = Transaction::find($input['transaction_id']);

            if ($info->transaction_type === 'inflow') {
                $type = '<li><a href="#">Create Invoice payment</a></li>';
                $class_type = 'text-success';
            } elseif ($info->transaction_type === 'outflow') {
                $type = '<li><a href="#">Create bill payment</a></li>';
                $class_type = 'text-danger';
            }
            $res = '<td><input type="checkbox" name="ids" value="' . $info->id . '"/></td>
                                    <td>' . date('d-m-Y', strtotime($info->transaction_date)) . '</td>
                                    <td>' . $info->description . '</td>
                                    <td><span class="' . $class_type . '">' . number_format($info->amount, 2, '.', ',') . '</span></td>
                                    <td>' . getLedgerName('D', $info->id) . '</td>
                                    <td>' . getLedgerName('C', $info->id) . '</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-complete dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                            <ul class="dropdown-menu align-xs-left" role="menu"  style="right: 0; left: auto;">
                                                <li><a href="javascript:;" class="view" data-value="' . $info->id . '">View Transaction</a></li>
                                                <li><a href="javascript:;" class="edit" data-value="' . $info->id . '">Edit Transaction</a></li>
                                                <li class="divider"></li>
                                                <li><a href="javascript:;" class="delete-transaction" data-value="' . $info->id . '">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>';
            return $res;
        }
    }

    /**
     * Transaction listing (Report)
     * @param  [type] $item_id    [description]
     * @param  [type] $start_date [description]
     * @param  [type] $end_date   [description]
     * @return [type]             [description]
     */
    public static function getTransactionListing($item_id = null, $start_date = null, $end_date = null)
    {
        if (!is_null($start_date)) {
            $start = date('Y-m-d', strtotime($start_date));
            if (is_null($end_date)) {
                $end = date('Y-m-d');
            } else {
                $end = date('Y-m-d', strtotime($end_date));
            }
            $res = Transaction::whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }

    public static function getTransactionDetails($id)
    {
        $res = Transaction::find($id);
        return $res;
    }

    public static function verifyTransactionDate($date)
    {
        $f_year = FiscalYear::where('status', 'Open')->where('flag', 'Active')->first();

        try {
            if ($f_year) {
                $date = Carbon\Carbon::createFromFormat('d/m/Y', $date);
                $new_date = $date->format('Y-m-d');
                if ($new_date >= $f_year->begins && $new_date <= $f_year->ends) {
                    return 'yes';
                } else {
                    return 'no';
                }
            } else {
                return 'no-f-year';
            }
        } catch (\Exception $exception) {
            return 'no-f-year';
        }
    }
}

