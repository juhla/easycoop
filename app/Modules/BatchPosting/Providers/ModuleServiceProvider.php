<?php

namespace App\Modules\BatchPosting\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'batch_posting');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'batch_posting');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'batch_posting');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
