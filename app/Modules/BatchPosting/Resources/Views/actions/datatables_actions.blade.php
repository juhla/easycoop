<?php

use App\Modules\Base\Traits\Acl;

?>

<div class="btn-group btn-group-xs">
    @if(Acl::can('edit_journal_entry'))

    <a href = "{{ url('batch_posting/edit/'.base64_encode($entry->transactions_without_scope->id)) }}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
    @endif
    @if(Acl::can('delete_journal_entry'))

    <a href="javascript:;" class="delete-journal btn btn-danger" data-value="{{ $entry->transactions_without_scope->id }}"><i class="fa fa-trash-o"></i></a>
    @endif
</div>
