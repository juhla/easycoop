@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            <a href="{{ url('batch_posting') }}" class="btn btn-sm btn-primary">
                                <i class="fa fa-arrow-left"></i> Back to Batch Posting
                            </a>


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            {{--<div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>--}}
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        @include('flash::message')
                        <div class="panel-body">
                            <form role="form" action="{{ url('batch_posting/save') }}" method="post" id="journalForm"
                                  data-validate="parsley">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label for="transaction_date">Transaction Date <span
                                                        class="required">*</span> </label>
                                            <input type="text" class="form-control datepicker" name="transaction_date"
                                                   id="transaction_date" data-required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pcv">PV/PCV/Inv. No. <span class="required">*</span> </label>
                                            <input type="text" class="form-control pcv" name="pcv" id="pcv"
                                                   data-required>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-4">
                                        <div class="form-group form-group-default">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control description" name="description" id="description">
                                        </div>
                                    </div>--}}
                                </div>
                                <div>
                                    <hr/>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th width="30%">Ledger Account</th>
                                        <th width="25%">Description</th>
                                        <th width="15%">Debit (Amount)</th>
                                        <th width="15%">Credit (Amount)</th>
                                        <th width="5%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <select class="ledger select2-option" name="ledger[]" style="width: 100%"
                                                    data-required>
                                                <option value=""></option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control item_description"
                                                   name="item_description[]" value="" data-required>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control debit"
                                                   name="debit[]" value="0.00" data-type="number" readonly
                                                   onkeypress="return numbersonly(event)">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control credit"
                                                   name="credit[]" value="0.00" data-type="number" readonly
                                                   onkeypress="return numbersonly(event)">
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-sm-8">
                                    </div>
                                    <div class="col-sm-4 m-t-10 sm-m-t-10">
                                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="entry_id" id="entry_id" value="">
                                        <button type="submit" class="btn btn-info m-t-5 pull-right" id="saveBtn"
                                                style="margin-left:10px;">Save
                                        </button>
                                        <button class="btn btn-primary m-t-5 pull-right add-new">Add New</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {{--<div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>--}}
                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">

        $(function () {


            var thousandsComma = function () {
                $('input.credit').keyup(function (e) {
                    if ((e.keyCode >= 16 && e.keyCode <= 18) || (e.keyCode >= 33 && e.keyCode <= 40) || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                        return;
                    }

                    $(this).val(function (index, value) {
                        value = value.replace(/,/g, '');
                        return numberWithCommas(value);
                    });
                });

                $('input.debit').keyup(function (e) {
                    if ((e.keyCode >= 16 && e.keyCode <= 18) || (e.keyCode >= 33 && e.keyCode <= 40) || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                        return;
                    }

                    $(this).val(function (index, value) {
                        value = value.replace(/,/g, '');
                        return numberWithCommas(value);
                    });
                });
            };

            thousandsComma();
            //set base url variable
            var baseurl = $('#baseurl').val();

            //manage readonly attr for debit and credit field
            $(document).on('focus', '.debit', function () {
                $(this).prop('readonly', false);
                $(this).closest('tr').find('.credit').prop('readonly', true);
            });

            $(document).on('focus', '.credit', function () {
                $(this).prop('readonly', false);
                $(this).closest('tr').find('.debit').prop('readonly', true);
            });


            //load general ledger accounts
            loadGLAccounts();

            // Add new entry to the form
            $('.add-new').on('click', function (e) {
                e.preventDefault();
                $('.table').append(newLineOfEntry());
                //$('.datapicker').datepicker();
                loadGLAccounts();
                thousandsComma();
            });

            $(document).on('click', '.rem_entry', function (ev) {
                ev.preventDefault();
                $(this).closest('tr').remove();
            });

            var compareDC = function () {
                var debit = 0;
                var credit = 0;

                $('input[name^=debit]').each(function () {
                    debit += +$(this).val().replace(/,/g, '');
                    $(this).val($(this).val().replace(/,/g, ''));
                });

                $('input[name^=credit]').each(function () {
                    credit += +$(this).val().replace(/,/g, '');
                    $(this).val($(this).val().replace(/,/g, ''));
                });
                if (debit === credit) {
                    return true;
                } else {
                    displayNotification('Your posting is not balanced.', 'Error!', 'error');
                    return false;
                }
            };

            //Add another line of transaction
            var newLineOfEntry = function () {
                var row = '<tr>' +
                        '<td>' +
                        '<select class="ledger select2-option" name="ledger[]" style="width: 100%" data-required> ' +
                        '<option value=""></option>' +
                        '</select>' +
                        '</td>' +
                        '<td><input type="text" class="form-control item_description" name="item_description[]" value="" data-required ></td>' +
                        '<td><input type="text" class="form-control debit" name="debit[]" value="0.00" data-type="number" readonly></td>' +
                        '<td><input type="text" class="form-control credit" name="credit[]" value="0.00" data-type="number" readonly></td>' +
                        '<td><button class="btn btn-danger btn-sm rem_entry"><i class="fa fa-minus"></i></button></td>' +
                        '</tr>';

                //validate form
                //$('#journalForm').validate();
                return row;
            };

            //submit details on click
            $(document).on('submit', '#journalForm', function (e) {
                e.preventDefault();
                if ($('input[name="debit[]"]').length === 1) {
                    displayNotification('Please add at least one more row', 'Error!', 'error');
                    return false;
                }
                if (compareDC()) {
                    var $btn = $('#saveBtn');
                    $btn.attr('disabled', true);
                    $btn.html('Saving...');
                    $.ajax({
                        type: 'POST',
                        url: $(this).attr('action'),
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (res) {
                            if (res.statusCode === 200) {
                                displayNotification(res.message, 'Success!', 'success');
                                $btn.attr('disabled', false);
                                $btn.html('Save');
                                window.location.replace(baseurl + '/batch_posting');
                            }

                            if (res.statusCode === 500) {
                                displayNotification(res.message, 'Error!', 'error');
                                $btn.attr('disabled', false);
                                $btn.html('Save');
                            }
                        },
                        error: function (res) {
                            displayNotification(res.message, 'Error!', 'error');
                            $btn.attr('disabled', false);
                            $btn.html('Save');
                        }
                    });
                } else {

                }
            });
        });

    </script>
@stop
