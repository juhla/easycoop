<?php

use App\Modules\Base\Traits\Acl;

?>
@extends('layouts.main')

@section('content')
<section class="hbox stretch">
    {{--<aside class="aside-md bg-white b-r" id="subNav">
        <div class="wrapper b-b header">Submenu Header</div>
        <ul class="nav">
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at
                    ultricies</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
            </li>
            <li class="b-b b-light">
                <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
            </li>
        </ul>
    </aside>--}}
    <aside>
        <section class="vbox">
            <header class="header bg-white b-b clearfix">
                <div class="row m-t-sm">
                    <div class="col-sm-3 m-b-xs">
                        @if(Acl::can('delete_journal_entry'))
                        <button type="button" class="btn btn-sm btn-danger bulk-del" title="Remove" disabled><i class="fa fa-trash-o"></i>
                            Delete
                        </button>
                        @endif
                        @if(Acl::can('add_journal_entry'))
                        <a href="{{ url('batch_posting/add-new') }}" class="btn btn-sm btn-primary">
                            <i class="fa fa-plus"></i> Add New
                        </a>
                        @endif
                    </div>
                    @if(Acl::can(['add_journal_entry']))
                    @include('batch_posting::actions.search')
                    @endif

                </div>
            </header>
            <section class="scrollable wrapper w-f">
                <section class="panel panel-default">
                    <div class="table-responsive">
                        @section('css')
                        @include('layouts.datatables_css')
                        @endsection
                        {!! $dataTable->table(['width' => '100%']) !!}
                        @section('scripts_dt')
                        @include('layouts.datatables_js')
                        {!! $dataTable->scripts() !!}
                        @endsection
                    </div>
                </section>
            </section>
            <footer class="footer bg-white b-t">
                <div class="row text-center-xs">
                    {{--<div class="col-md-6 hidden-sm">
                        <p class="text-muted m-t">Showing 20-30 of 50</p>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right text-center-xs">
                        <ul class="pagination pagination-sm m-t-sm m-b-none">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>--}}
                    {{-- {!! $batchs->render() !!} --}}
                </div>
            </footer>
        </section>
    </aside>
</section>
@include('batch_posting::view')
@stop
@section('scripts')
<script type="text/javascript">
    $(function () {
        //set base url variable
        var baseurl = $('#baseurl').val();
        var deleteBtn = $('.bulk-del');
        var checkboxes = $("input[type='checkbox']");
        $('#transaction_date').datepicker();

        //checkall
        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
            deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        });
        //enable/disable delete button
        $(document).on("click", checkboxes, function () {
            deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        });


        //delete single journal entry
        $(document).on("click", ".delete-journal", function (event) {

            var ID = $(this).attr('data-value');
            var _token = $('#_token').val();
            var $this = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover the data once deleted!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: !1
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'GET',
                        url: baseurl + '/batch_posting/delete/' + ID,
                        context: this,
                        success: function (res) {
                            if (res.statusCode === 200) {
                                displayNotification(res.message, 'Success', 'success');
                                $this.fadeOut('slow', function () {
                                    setTimeout(function () {
                                        window.location.replace(baseurl + '/batch_posting');
                                    }, 3000);
                                });
                            } else {
                                displayNotification(res.message, 'Error!', 'error');
                            }
                        }
                    });
                }
            });
        });

        //bulk delete journal entries
        deleteBtn.click(function () {
            var ids = [];
            var _token = "{{csrf_token()}}";
            $.each($("input[name='ids']:checked"), function () {
                ids.push($(this).val());
            });
            if (confirm('Are you sure you want to delete ' + ids.length + ' items?')) {
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/batch_posting/delete/',
                    data: { ids: ids, _token: _token },
                    context: this,
                    success: function (e) {
                        $("input[name='ids']:checked").each(function () {
                            $(this).fadeOut('slow', function () {
                                $(this).closest("tr").remove();
                            });
                        });
                        $("input:checkbox").prop('checked', false);
                        // deleteBtn.toggle();
                    },
                    error: function (e) {
                        //console.log(e)
                    }
                });

            }
            ;
        });
    });
    $('.date-field').datepicker();
</script>
<script>
    $('#clearBTN').click(function () {
        $('#start_date').val('');
        $('#end_date').val('');
    });
</script>
@stop
