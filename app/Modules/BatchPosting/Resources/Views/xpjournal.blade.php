<table class="table table-hover " id="inflowTable">
        <thead>
        <tr>
            <th width="15%">Date</th>
            <th width="18%">Description</th>
            <th width="15%" style="margin-right: 10px">Amount</th>
            <th width="18%">Debit Account</th>
            <th width="18%">Credit Account</th>
        </tr>
        </thead>
        <tbody id="journalTable">
        @if(count($journals) > 0)
            @foreach($journals as $entry_1)
                <tr class="edit-row" data-value="{{$entry_1->id}}">
                    <td>{{ date('m/d/Y', strtotime($entry_1->transaction_date)) }}</td>
                    <td>{{ $entry_1->description}}</td>
                    <td>{{ $entry_1->amount}}</td>
                    <td>
                        <?php $items = $entry_1->item()->where('dc', 'D')->where('transaction_id', $entry_1->id)->get() ?>
                        @if(count($items) > 1)
                            @foreach( $items as $item )
                                {{ getLedgerName($item->ledger_id) }}
                            @endforeach
                        @else
                            {{ getLedgerName($items->first()->ledger_id)}}
                        @endif
                    </td>
                    <td>
                        <?php $items = $entry_1->item()->where('dc', 'C')->where('transaction_id', $entry_1->id)->get() ?>
                        @if(count($items) > 1)
                            @foreach( $items as $item )
                                {{ getLedgerName($item->ledger_id).', ' }}
                            @endforeach
                        @else
                            {{ getLedgerName($items->first()->ledger_id)}}
                        @endif
                    </td>                                                  
                    
                </tr>
            @endforeach
        @endif

        </tbody>

    </table>