<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'batch_posting', 'middleware' => ['web', 'auth:web']], function () {
    Route::any('/', 'BatchController@index');
    Route::get('/add-new', 'BatchController@newTransaction');
    Route::post('/save', 'BatchController@save');

    //
    Route::get('/edit/{id}', 'BatchController@edit');
    Route::post('/update', 'BatchController@update');
    Route::any('/delete/{id}', 'BatchController@delSingle');
    Route::any('/delete', 'BatchController@delete');

    // View export test
    Route::get('/export', 'BatchController@export');

});
