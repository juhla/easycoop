<?php


namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\AccountGroup;
use App\Modules\Base\Models\BankAC;
use App\Modules\Base\Models\Customer;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Ledger;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Vendor;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\COA;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller
{

    use COA, Balance;

    /**
     * Constructor function to this controller
     */
    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function index()
    {

        //dd(session('company_db'));
//        $res = Ledger::where('group_id', '13')
//            ->orderBy('code', 'desc')
//            ->first();
//           // ->get();
//
//        dd($res);

        //dd(session('company_db'));
        $this->hasAccess(['chart_of_account']);
        $data['title'] = 'Chart of Accounts';
        $this->checkValidLedgers();

        $error = $this->getInValidArray();
        $error = implode(" <br/> ", $error);

        $data['classes'] = AccountGroup::with(['groups' => function ($q) {
            $q->where('flag', 'Active')->orderBy('code', 'asc');
        }])->where('parent_id', null)
            ->where('flag', 'Active')
            ->get();

        $viewable = view('business-setup::chart-of-accounts')->with($data);
        if (!empty($this->getInValidArray())) {
            $viewable->withErrors($error);
        }
        //pr($data['classes']);exit;
        //dd($data);
        return $viewable;
    }

    /**
     * add method
     * @return mixed
     */
    public function save()
    {
        $input = request()->all();

        $info = [
            'parent_id' => $input['parent_id'],
            'name' => $input['group_name'],
            'code' => $input['group_code'],
        ];


        $validator = Validator::make($info, [
            'name' => 'required',
            'code' => "required|unique:tenant_conn.ca_groups,code",
        ]);

        if ($validator->fails()) {
            $res = Arr::flatten($validator->errors()->toArray());
            flash()->error(implode(",", $res));
            return
                redirect()->back()
                    ->withErrors($validator)
                    ->withInput();

        }

        $status = $this->validateGroupCOA($info);
        if (!$status) {
            flash()->error($this->getErrors());
            return redirect()->back();
        }

        if ($input['group_id'] === '') {
            AccountGroup::create($info);
            flash()->success('Account Group has been created.');
        } else {
            try {
                AccountGroup::where('id', $input['group_id'])->update($info);
                flash()->success('Account Group has been update');
            } catch (\Exception $e) {
                flash()->error($e->getMessage());
                app('sentry')->captureException($e);
                return redirect()->back();
            }
        }

        return redirect()->back();
    }

    /**
     * method to get group to edit
     * @param $id
     * @return mixed
     */
    public function _get($id)
    {

        $res = AccountGroup::findOrFail($id);
        if ($res) {
            return response()->json($res);
        }
    }

    /**
     * method to delete group
     * @param $id
     * @return mixed
     */
    public function deleteGroup($id)
    {
        AccountGroup::where('id', $id)->delete();
        return redirect()->back();
    }

    /**
     * method to delete ledger
     * @param $id
     * @return mixed
     */
    public function deleteLedger($id)
    {
        $ledger = Ledger::find($id);
        Customer::where('ledger_code', $ledger->code)->delete();
        Vendor::where('ledger_code', $ledger->code)->delete();
        $ledger->delete();
        return redirect()->back();
    }

    /**
     * method to add GL Account
     * @return mixed
     */
    public function saveLedger()
    {
        $input = request()->all();

        // remove comma from number
        $opening_balance = $this->removeComma($input['opening_balance']);

        $ledger = [
            'group_id' => $input['parent_group_id'],
            'name' => $input['ledger_name'],
            'code' => $input['ledger_code'],
            'opening_balance_type' => $input['balance_type'],
            'opening_balance' => empty($input['opening_balance']) ? 0 : $opening_balance,
        ];

        $fiscalYear = FiscalYear::getCurrentFiscalYear();
        $status = $this->validateLedgerCOA($ledger, $input['ledger_id']);
        if (!$status) {
            flash()->error($this->getErrors());
            return redirect()->back();
        }

        if (!isset($fiscalYear)) {
            flash()->error('You need to first set a fiscal year');
            return redirect()->back();
        }
        if ($input['ledger_id'] === '') {
            try {
                $validator = Validator::make($input, [
                    'ledger_name' => 'required',
                    'balance_type' => 'required',
                    'ledger_code' => "required|unique:tenant_conn.ca_ledgers,code",
                ]);

                if ($validator->fails()) {

                    $res = Arr::flatten($validator->errors()->toArray());
                    flash()->error(implode(",", $res));
                    return
                        //redirect()->to('setup/accounts')
                        redirect()->back()
                            ->withErrors($validator)
                            ->withInput();

                }
                //dd($input);
                $res = Ledger::create($ledger);
                if ($input['parent_group_id'] == 16) {
                    $inputs['ledger_code'] = $input['ledger_code'];
                    $inputs['bank'] = $input['ledger_name'];
                    BankAC::create($inputs);
                }
                if ($input['parent_group_id'] == 12) {
                    $data = [
                        'name' => $input['ledger_name'],
                        'ledger_code' => $input['ledger_code'],
                        'email' => 'NULL'
                    ];
                    Customer::create($data);
                }

                if ($input['parent_group_id'] == 28) {
                    $data = [
                        'name' => $input['ledger_name'],
                        'ledger_code' => $input['ledger_code'],
                        'email' => 'NULL'
                    ];
                    Vendor::create($data);
                }
                $this->saveOpeningBalance($res->id, $fiscalYear, true);
                flash()->success('GL Account has been created');
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
        } else {
            try {
                $ledg = Ledger::where('id', $input['ledger_id'])->first();
                Ledger::where('id', $input['ledger_id'])->update($ledger);
                if ($ledg->group_id == 12) {
                    $inputs['ledger_code'] = $input['ledger_code'];
                    $inputs['name'] = $input['ledger_name'];


                    $customer = Customer::where('ledger_code', $input['ledger_code'])->first();
                    if (empty($customer)) {
                        // The ledger does not exist in the bank accounts table thus create/insert
                        $inputs['ledger_code'] = $input['ledger_code'];
                        $inputs['name'] = $input['ledger_name'];
                        Customer::create($inputs);
                    } else {
                        Customer::where('ledger_code', $input['ledger_code'])->update(['name' => $inputs['name']]);
                    }
                }
                if ($ledg->group_id == 28) {
                    $inputs['ledger_code'] = $input['ledger_code'];
                    $inputs['name'] = $input['ledger_name'];

                    $customer = Vendor::where('ledger_code', $input['ledger_code'])->first();
                    if (empty($customer)) {
                        // The ledger does not exist in the bank accounts table thus create/insert
                        $inputs['ledger_code'] = $input['ledger_code'];
                        $inputs['name'] = $input['ledger_name'];
                        Vendor::create($inputs);
                    } else {
                        Vendor::where('ledger_code', $input['ledger_code'])->update(['name' => $inputs['name']]);
                    }
                }

                if ($input['parent_group_id'] == 16) {
                    $inputs['ledger_code'] = $input['ledger_code'];
                    $inputs['bank'] = $input['ledger_name'];
                    // This checks if the ledger edited currently exists in the Bank table, creates it if doesn't exist but updates the bank if it exists.
                    $bankAcc = BankAC::where('ledger_code', $input['ledger_code'])->first();

                    // Check if  ledger exists in the bank accounts table
                    if (empty($bankAcc)) {
                        // The ledger does not exist in the bank accounts table thus create/insert
                        $inputs['ledger_code'] = $input['ledger_code'];
                        $inputs['bank'] = $input['ledger_name'];
                        BankAC::create($inputs);
                    } else {
                        BankAC::where('ledger_code', $input['ledger_code'])->update($inputs);
                    }
                }
                $this->saveOpeningBalance($input['ledger_id'], $fiscalYear, false);
                flash()->success('GL Account has been updated');
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
        }
        return redirect()->back();
    }

    public function saveOpeningBalance($ledger, $fiscalYear, $isCreated = true)
    {
        $ledger = Ledger::where('id', $ledger)->first();
        $trans_item1 = [
            'ledger_id' => $ledger->id,
            'amount' => $ledger->opening_balance,
            'item_description' => "Opening Balance",
            'dc' => $ledger->opening_balance_type,
        ];

        if ($isCreated) {
            //dd($ledger->created_at->toDateString());

            $transaction = array(
                'transaction_date' => $fiscalYear->begins,
                'description' => "Opening Balance",
                'amount' => $ledger->opening_balance,
                'transaction_type' => "journal",
                'created_by' => Auth::user()->id,
            );
            //dd($transaction);
            $info = Transaction::create($transaction);
            $trans_item1['transaction_id'] = $info->id;
            //dd($trans_item1);
            TransactionItem::create($trans_item1);
        } else {

            $transaction = array(
                'transaction_date' => $fiscalYear->begins,
                'description' => "Opening Balance",
                'amount' => $ledger->opening_balance,
                'transaction_type' => "journal",
                'updated_by' => Auth::user()->id,
            );

            $editted = TransactionItem::whereHas('transactions', function ($query) use ($fiscalYear) {
                $query->where('description', 'Opening Balance');
                $query->where('transaction_date', $fiscalYear->begins);
            })
                ->where('ledger_id', $ledger->id)
                // ->where('item_description', "Opening Balance")
                //->where('dc', $ledger->opening_balance_type)
                ->first();

            if (empty($editted)) {
                $transaction = array(
                    'transaction_date' => $fiscalYear->begins,
                    'description' => "Opening Balance",
                    'amount' => $ledger->opening_balance,
                    'transaction_type' => "journal",
                    'created_by' => Auth::user()->id,
                );

                $trans_item1 = [
                    'ledger_id' => $ledger->id,
                    'amount' => $ledger->opening_balance,
                    'item_description' => "Opening Balance",
                    'dc' => $ledger->opening_balance_type,
                ];

                //dd($transaction);
                $info = Transaction::create($transaction);
                $trans_item1['transaction_id'] = $info->id;
                TransactionItem::create($trans_item1);
            } else {
                Transaction::where('id', $editted->transaction_id)->update($transaction);
                TransactionItem::where('id', $editted->id)->update($trans_item1);
            }
        }
    }

    /**
     * method to get gl account to edit
     * @param $id
     * @return mixed
     */
    public function _getLedger($id)
    {
        $res = Ledger::findOrFail($id);
        if ($res) {
            return response()->json($res);
        }
    }

    /**
     * method to return last group account code
     * @param $group_id
     * @return string
     */
    public function getLastGroupCode($group_id)
    {
        $group = AccountGroup::find($group_id);
        if ($group) {
            //find child groups if any
            $res = AccountGroup::where('parent_id', $group_id)->orderBy('code', 'desc')->first();
            //if child group exist, return code
            if ($res) {
                return $res->code;
            } else {
                //if no child, return group code
                return $group->code;
            }
        } else {
            return 'Not Found';
        }
    }

    /**
     * method to get last ledger account code
     * @param $group_id
     * @return string
     */
    public function getLastLedgerCode($group_id)
    {
        $group = AccountGroup::find($group_id);
        if ($group) {
            //find child groups if any
            $res = Ledger::where('group_id', $group_id)
                ->orderBy('created_at', 'desc')
                ->first();
            //if child group exist, return code
            if ($res) {
                return $res->code;
            } else {
                //if no child, return group code
                return $group->code;
            }
        } else {
            return 'Not Found';
        }
    }

    public function getLedgerAccounts($id)
    {
        if (request()->get('curr_year')) {
            $curr_year = request()->get('curr_year');
        } else {
            $curr_year = null;
        }

        if (request()->get('prev_year')) {
            $prev_year = request()->get('prev_year');
        } else {
            $prev_year = null;
        }

        $ledgers = Ledger::where('group_id', $id)->get();
        $result = '';
        if ($ledgers) {
            foreach ($ledgers as $ledger) {
                $curEndDate = $curr_year . '-12-31';
                $curStartDate = $curr_year . '-01-01';


                $prevEndDate = $prev_year . '-12-31';
                $prevStartDate = $prev_year . '-01-01';


                $currYrResult = $this->getLedgerBalances($ledger->id, $curStartDate, $curEndDate);
                $prevYrResult = $this->getLedgerBalances($ledger->id, $prevStartDate, $prevEndDate);


                //$currYrResult = $ledger->getBalance(true, null, null, $curr_year);
                //$prevYrResult = $ledger->getBalance(true, null, null, $prev_year);

                $result .= '<tr>
                        <td><a href="' . url('reports/ledger-statement/' . $ledger->id) . '" target="_blank">' . $ledger->name . '</a>
                        <td>' . formatNumber($currYrResult['balance']) . '</td>
                        <td>' . formatNumber($prevYrResult['balance']) . '</td>
                        </tr>';
            }
            //            $expenses = AccountGroup::with(['groups' => function ($q) {
            //                $q->where('id', '!=', 37)->orderBy('code', 'asc');
            //            }])->where('id', '5')->first();
            //            $groups = $expenses->groups;
            //            foreach ($groups as $group) {
            //                if ($group->ledgers->count()) {
            //                    if ($nestedGroup = $group->nestedGroups($group->id)) {
            //                        foreach ($nestedGroup->orderBy('code', 'asc')->get() as $child) {
            //                            if ($child->ledgers->count()) {
            //                                foreach ($child->ledgers()->where('id', '!=', 44)->orderBy('code', 'asc')->get() as $ledger) {
            //                                    $currYrResult = $ledger->getBalance(true, null, null, $curr_year);
            //                                    $prevYrResult = $ledger->getBalance(true, null, null, $prev_year);
            //                                    $result .= '<tr>
            //                                <td><a href="' . url('reports/ledger-statement/' . $ledger->id) . '" target="_blank">' . $ledger->name . '</a>
            //                                <td>' . formatNumber($currYrResult['balance']) . '</td>
            //                                <td>' . formatNumber($prevYrResult['balance']) . '</td>
            //                                 </tr>';
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }

            return $result;
        } else {
            $result = '<tr><td colspan="3">No Ledger account for this group.</td>';
            return $result;
        }
    }


    protected function removeComma($openingBalance)
    {
        // if(is_numeric($openingBalance)){
        return $openingBalance = str_replace(',', '', $openingBalance);
    }
    // return $openingBalance;
    // }   
}
