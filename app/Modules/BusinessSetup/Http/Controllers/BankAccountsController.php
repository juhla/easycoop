<?php

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\BankAC;
use App\Modules\Base\Models\Ledger;
use Auth;

class BankAccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);

    }

    public function index()
    {
        $this->hasAccess(['Bank_Account']);
        $data['title'] = "Manage Bank Accounts";
        $data['accounts'] = BankAC::where('flag', 'Active')->get();
        return view('business-setup::banks')->with($data);
    }

    /**
     * store method to update or add new record
     * @return mixed
     */
    public function store()
    {
        $input = request()->all();

        if ($input['account_id'] === '') {
            $code = Ledger::getLastBankAccountCode();
            $input['ledger_code'] = $code;
            //leger details
            $details = [
                'name' => $input['account_name'],
                'code' => $code,
                'opening_balance_type' => 'D',
                'group_id' => 16
            ];
            //save bank account details
            $bank = BankAC::create($input);
            if ($bank) {
                //create new ledger account
                $details['name'] = $input['bank'];
                Ledger::create($details);
            }
            flash()->success('Bank account has been created.');
        } else {
            unset($input['_token']);
            $id = $input['account_id'];
            unset($input['account_id']);
            $bank = BankAC::where('id', $id)->update($input);

            //leger details
            $result = BankAC::whereId($id)->first();
            $code = $result->ledger_code;
            if ($bank) {
                //update ledger account
                Ledger::where('code', $code)->update(['name' => $input['bank']]);
            }
            flash()->success('Bank Account has been update');
        }

        return redirect()->back();
    }

    /**
     * method to get bank account to edit
     * @param $id
     * @return mixed
     */
    public function _getAccount($id)
    {
        $res = BankAC::findOrFail($id);
        if ($res) {
            return response()->json($res);
        }
    }

    public function delete($id)
    {
        if ($id) {
            $bank = BankAC::find($id);
            //delete ledger account for bank
            Ledger::where('code', $bank->ledger_code)->delete();
            //delete bank
            BankAC::where('id', $id)->delete();
        }
    }
}