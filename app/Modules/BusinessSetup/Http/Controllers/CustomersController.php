<?php

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\BusinessSetup\Traits\ExcelUpload;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\Customer;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Ledger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Session;

class CustomersController extends Controller
{
    use ExcelUpload;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function index()
    {
        $this->hasAccess(['customer']);
        $data['title'] = "Customers";
        $data['customers'] = Customer::where('flag', 'Active')->get();
        return view('business-setup::customers')->with($data);
    }


    function upload(Request $request)
    {
        $input = $request->all();
        $rules = [
            'customer_file' => 'required|file|mimes:xlsx',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            if (request()->ajax()) {
                $ajaxResponse['status'] = 'validator';
                return response()->json($ajaxResponse, 500);
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }

        $file = $input['customer_file'];
        unset($input['_token']);
        $fileName = time() . '-' . strtolower(str_replace(' ', '-', $file->getClientOriginalName()));
        $file->storeAs('uploaded', $fileName);
        $this->handleCustomerData($fileName);
        request()->session()->flash('flash_message', [
            'type' => 'success',
            'content' => 'Customer upload successful.'
        ]);
        return redirect()->back();

    }

    function download()
    {
        $template = [
            'name' => 'Name',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_no' => 'Phone Number',
            'address' => 'Address',
        ];
        Excel::create('product_template', function ($excel) use ($template) {
            $excel->sheet('create', function ($sheet) use ($template) {
                $sheet->setOrientation('landscape');
                $sheet->row(1, $template);
            });
        })->export('xlsx');
    }


    /**
     * store method to update or add new record
     * @return mixed
     */
    public function store()
    {
        $input = request()->all();
        //$customer = '';

        if ($input['customer_id'] === '') {
            $code = Ledger::getLastReceivableCode();

            $input['ledger_code'] = $code;
            //leger details
            $details = [
                'name' => $input['name'],
                'code' => $code,
                'opening_balance_type' => 'C',
                'group_id' => 12
            ];
            //save customer details

            try {
                $customer = Customer::create($input);

                if ($customer) {
                    //create new ledger account
                    Ledger::create($details);
                }
            } catch (\Exception $exception) {
                dd($exception->getMessage());
            }

            if (request()->ajax()) {
                return response()->json(['success' => 200, 'data' => $customer]);
            } else {
                flash()->success('Successfully saved customer - ' . $input['name']);
                return redirect()->back();
            }
        } else {
            unset($input['_token']);
            $id = $input['customer_id'];
            unset($input['customer_id']);
            Customer::where('id', $id)->update($input);
            $customer = Customer::where('id', $id)->first();
            Ledger::where('code', $customer->ledger_code)->update(["name" => $input['name']]);
            if (request()->ajax()) {
                return response()->json(['success' => 200, 'data' => $input['name']]);
            } else {
                flash()->success('Successfully saved customer - ' . $input['name']);
                return redirect()->to('setup/customers');
            }
        }
    }

    /**
     * method to get customer details to edit
     * @param $id
     * @return mixed
     */
    public function _getCustomer($id)
    {
        $res = Customer::findOrFail($id);
        if ($res) {
            return response()->json($res);
        }
    }

    /**
     * method to delete customer
     * @param $id
     */
    public function delete($id)
    {
        if ($id) {
            $customer = Customer::find($id);
            $ledger = Ledger::where('code', $customer->ledger_code)->first();
            //find transaction items
            try {
                DB::beginTransaction();
                $items = TransactionItem::where('ledger_id', $ledger->id)->get()->pluck("transaction_id", "id");
                $items = $items->toArray();
                Transaction::destroy($items);

                $res = TransactionItem::whereIn('transaction_id', $items)->get()->pluck('id', 'id');
                $res = $res->toArray();
                TransactionItem::destroy($res);
                //$items->delete();
                $ledger->delete();
                $customer->delete();
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();
                app('sentry')->captureException($exception);
            }
        }
    }
}
