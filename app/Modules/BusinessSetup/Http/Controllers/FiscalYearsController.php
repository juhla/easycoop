<?php

namespace App\Modules\BusinessSetup\Http\Controllers;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\FiscalYear;


class FiscalYearsController extends Controller
{


    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function index()
    {
        $this->hasAccess(['fiscal_year']);
        $data['title'] = "Fiscal Years";
        $data['fiscalyears'] = FiscalYear::where('flag', 'Active')->get();
        return view('business-setup::fiscal-year')->with($data);
    }

    /**
     * update or add a user task
     * @return mixed
     */
    public function manage()
    {
        $input = request()->all();

        $return_arr = FiscalYear::updateFYear($input);
        if (request()->ajax()) {
            $return_arr['status'] = 1;
            return response()->json($return_arr);
        } else {
            flash()->success($return_arr['message']);
            return redirect()->back();
        }
    }

    //get task to be edited
    public function getFiscalYear($item_id)
    {
        $res = FiscalYear::find($item_id);

        $data = array(
            'begins' => date('d/m/Y', strtotime($res->begins)),
            'ends' => date('d/m/Y', strtotime($res->ends)),
            'status' => $res->status
        );

        echo json_encode($data);
    }

    //make task inactive(delete)
    public function delete($id)
    {
        FiscalYear::find($id)->delete();
    }
}