<?php

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\PaymentTerm;
use Auth;
use DB;

class PaymentTermsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['tenant']);
    }


    public function index()
    {
        $this->hasAccess(['Payment-Types']);
        $data['title'] = 'Payment Terms';
        return view('business-setup::payment-terms')->with($data);
    }

    public function getAllTerms()
    {
        $paymentterms = PaymentTerm::all();
        if (request()->ajax()) {
            if (count($paymentterms) > 0) {
                return response()->json([
                    'status' => 200,
                    'data' => $paymentterms
                ]);
            }
            return response()->json([
                'status' => 404,
                'message' => 'You have not created payment terms.'
            ]);
        }

        return response($paymentterms);
    }

    public function getTerm()
    {
        $term = PaymentTerm::where('id', '=', request()->input('_term'))
            ->first();
        if (request()->ajax()) {
            if ($term) {
                return response()->json([
                    'status' => 200,
                    'data' => $term
                ]);
            }

            return response()->json([
                'status' => 404,
                'message' => 'Payment term not found.'
            ]);
        }

        if ($term) {
            return response($term);
        }
        return response('Payment term not found.');
    }

    public function postTerm()
    {
        DB::beginTransaction();
        try {
            $new_term = new PaymentTerm;
            $new_term->created_by = Auth::user()->id;
            $new_term->name = request()->input('name');
            $new_term->payment_type = request()->input('type');
            $new_term->days = request()->input('days');
            $new_term->description = request()->input('description');
            $new_term->flag = 'Active';
            $new_term->save();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'message' => 'Error while creating payment term. ' . $e->getMessage()
            ]);
        }
        DB::commit();

        return response()->json([
            'status' => 200,
            'message' => 'Payment term created. Done!'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function updateTerm()
    {
        DB::beginTransaction();
        try {
            PaymentTerm::where('id', '=', request()->input('term_id'))
                ->update([
                    'updated_by' => Auth::user()->id,
                    'name' => request()->input('name'),
                    'payment_type' => request()->input('type'),
                    'days' => request()->input('days'),
                    'description' => request()->input('description')
                ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'message' => 'Error while creating payment term. ' . $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 200,
            'message' => 'Payment term updated. Done!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroyTerm()
    {
        DB::beginTransaction();
        try {
            $model = PaymentTerm::find(request()->input('_term'));
            $model->delete();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'message' => 'Error deleting payment term. ' . $e->getMessage()
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 200,
            'message' => 'Payment term deleted.'
        ]);
    }

}