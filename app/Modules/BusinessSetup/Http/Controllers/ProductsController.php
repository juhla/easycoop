<?php

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\Product;
use DB;
use Auth;

use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function __construct()
    {

        $this->middleware(['tenant']);
    }

    public function index()
    {
        return redirect()->back();
        $data['title'] = 'Products';
        return view('business-setup::products')->with($data);
    }

    public function getAllProducts()
    {
        return redirect()->back();
        $products = Product::all();
        if (count($products) > 0) {
            return response()->json([
                'data' => $products,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'You have 0 products.',
                'status' => 404
            ]);
        }
    }

    public function getProduct($id)
    {
        return redirect()->back();
        $product = Product::where('id', '=', $id)->first();

        if ($product) {
            return response()->json([
                'data' => $product,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Product not found.',
                'status' => 404
            ]);
        }
    }


    public function postProduct(Request $request)
    {
        return redirect()->back();
        try {
            $new_product = new Product;

            $new_product->name = $request->name;
            $new_product->description = $request->description;
            $new_product->price = $request->price;
            $new_product->tax = $request->tax;

            $new_product->created_by = Auth::user()->id;
            $new_product->save();

            return response()->json([
                'message' => 'Product created.',
                'status' => 200,
                'data' => $new_product
            ]);

        } catch (\Exception $exc) {
            return response()->json([
                'message' => 'An error occurred. Unable to create product<br>' . $exc->getMessage(),
                'status' => 500
            ]);
        }
    }


    public function updateProduct(Request $request)
    {
        return redirect()->back();
        try {
            Product::where('id', '=', $request->product_id)
                ->update([
                    'name' => $request->name,
                    'description' => $request->description,
                    //'code' => $request->Product_Code,
                    'price' => $request->price,
                    'tax' => $request->tax
                ]);

            return response()->json([
                'message' => 'Product updated.',
                'status' => 200
            ]);
        } catch (\Exception $exc) {
            return response()->json([
                'message' => 'Could not update product. Make sure you have entered a correct product code.',
                'status' => 500
            ]);
        }
    }


    public function destroyProduct($id)
    {
        return redirect()->back();
        Product::where('id', '=', $id)
            ->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Product deleted.'
        ]);
    }

}