<?php
/**
 * Created by PhpStorm.
 * User: baddy
 * Date: 20/12/2017
 * Time: 14:10
 */

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\Agents\AgentSme;
use App\Modules\Base\Models\Agents\Agent;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Base\Models\Collaborator;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Traits\Mailer;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Modules\Base\Models\Payment;
use App\Modules\Base\Traits\TenantConfig;

class ProfessionalAgentController extends Controller
{
    use Mailer;
    use TenantConfig;

    public function __construct(Collaborator $collaborator)
    {
        $this->middleware(['verify_professional_agent']);
        $this->collaborator = $collaborator;

    }

    public function unlinkAgent($smeID, $agentID)
    {
        $agent = AgentSme::where('agent_id', $agentID)
            ->where('sme_id', $smeID);
        $agent->delete();
    }


    public function save_map_agent()
    {
        $input = request()->all();
        $business = $input['sme'];
        $agent = $input['agent'];
        foreach ($business as $eachBusiness) {
            $agentSme = AgentSme::firstOrNew(array(
                'sme_id' => $eachBusiness,
                'agent_id' => $agent,
            ));
            $agentSme->sme_id = $eachBusiness;
            $agentSme->agent_id = $agent;
            $agentSme->save();
        }

        flash('Sme has been assigned', 'success');
        return redirect('setup/professional/agents/map_agents');
    }

    public function getAgent()
    {
        $agent = new Agent();
        $data['title'] = "Manage Agents";
        $data['agents'] = $agent->getAgent(auth()->user()->id);
        //pr($data['agents']);exit;
        return view('agents.professional-assembly-agents')->with($data);
    }

    public function getAnAgent($id)
    {

        $inf = new Agent();
        $validate = $inf->isUserMappedtoAgent(auth()->user()->id, $id);

        if (!empty($validate)) {
            $agentSME = new AgentSme();
            $user = User::where('id', $id)->first()->displayName();
            $data['title'] = "SMEs for Agent :: " . $user;
            $data['agent_id'] = $id;
            $data['business'] = $agentSME->getAgentBusiness($id);
            return view('agents.agent-sme')->with($data);
        }
        flash('Access Denied!');
        return redirect()->back();
        //pr($data);exit;

    }

    function mapAgents()
    {
        $influencerAgent = new Agent();
        $collaborator = new Collaborator();
        $data['title'] = "Map SMEs to Agent";
        $data['agents'] = $influencerAgent->getAgentList(auth()->user()->id);
        $data['business'] = $collaborator->getSmesByClientId(auth()->user()->id);
        //pr($data['business']);exit;
        return view('agents.agent-sme-professional')->with($data);
    }


    function _getAllValidationErrors($input_arr)
    {
        foreach ($input_arr as $key) {
            foreach ($key as $key2 => $value) {
                $output_arr[] = $value;
            }
        }
        return $output_arr;
    }

    public function storeAgent()
    {

        $input = request()->all();
        $rules = array(
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email|email',
        );

        $messages = array('required' => 'This is required');
        $validate = Validator::make($input, $rules, $messages);

        if ($validate->passes()) {
            $input['active'] = 1;
            $input['confirmed'] = 1;
            $input['flag'] = 'Active';
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $input['user_id'] = $user->id;
            $input['company_name'] = Company::whereRaw("user_id = ?", [auth()->user()->id])->first()->company_name;
            PersonalInfo::create($input);
            Company::create($input);
            $input['parent_id'] = auth()->user()->id;
            $input['child_id'] = $input['user_id'];
            Agent::create($input);
            $subscribe = new UserLicenceSubscriber();
            $subscribe->user_id = $user->id;
            $subscribe->count = 1;
            $subscribe->license_id = 0;
            $subscribe->expiry_date = date('Y-m-d h:m:s', strtotime('+5 years'));
            $subscribe->save();

            //assign user to role
            $user->attachRole(18);
            $user->active = 1;
            $user->confirmed = 1;
            $user->save();
            return response()->json([
                'type' => 'success'
            ]);
        } else {
            $failedRules = $validate->errors()->toArray();
            $failedRules = $this->_getAllValidationErrors($failedRules);
            return response()->json([
                'type' => 'validator_fails',
                'message' => array_pop($failedRules),
            ]);
        }
    }

    public function agentSmes()
    {
        $agent = new Agent();
        $data['agentBusinesses'] = $agent->getAgentSmes(auth()->user()->id);
        // dd($data['agentBusinesses']);
        return view('agents.professional-agent-businesses')->with($data);
    }


    public function unAssignBusiness()
    {
        $agent_id = Input::get('agent_id');
        $sme_id = Input::get('sme_id');
        AgentSme::whereRaw("agent_id = ? and sme_id = ?", [$agent_id, $sme_id])->delete();
        return '200';
    }
}