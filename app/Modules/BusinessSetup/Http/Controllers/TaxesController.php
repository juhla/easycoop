<?php


namespace App\Modules\BusinessSetup\Http\Controllers;


use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Models\Tax;
use Auth;
use Illuminate\Http\Request;

class TaxesController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware(['tenant']);
        $this->request = $request;
    }

    public function index()
    {
        $data['title'] = "Taxes";
        return view('business-setup::taxes')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Lists all taxes belonging to the specified company
     * Developed by Emmanuel Benson
     * 2nd September, 2015. 10:31 AM.
     */
    public function getAllTax()
    {
        $all_taxes = Tax::where('flag', 'Active')->get();
        $taxes = '';
        $return_arr = [];
        if (count($all_taxes) > 0) {

            foreach ($all_taxes as $tax) {
                $sales_gl = ($tax->sales_gl_code) ? $tax->sales_gl_account->name : "-";
                $purchase_gl = ($tax->purchase_gl_code) ? $tax->purchase_gl_account->name : "-";
                if (canEdit()) {
                    $buttons = '<div class="btn-group btn-group-xs">
                                    <button type="button" class="btn btn-default" id="edit-tax" data-action="edit" data-value="' . $tax->id . '"><i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-danger" id="delete-tax" data-action="delete" data-value="' . $tax->id . '"><i class="fa fa-trash-o"></i></button>
                                </div>';
                } else {
                    $buttons = '';
                }
                $taxes .= '<tr>
                             <td class="v-align-middle">' . $tax->name . ' (' . $tax->abbreviation . ')</td>
                             <td class="v-align-middle">' . $tax->rate . '%</td>
                             <td class="v-align-middle">' . $sales_gl . '</td>
                             <td class="v-align-middle">' . $purchase_gl . '</td>
                             <td>' . $buttons . '</td>
                        </tr>';
            }
            $return_arr['taxes'] = $taxes;
            $return_arr['status'] = 200;
        }
        if ($this->request->ajax()) {
            return response()->json($return_arr);
        } else {
            //should throw an exception
            //return response($taxes);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * Retrieves a specified tax belonging to the specified company
     */
    public function getTax($id)
    {
        $tax = Tax::where('id', $id)->first();

        if ($this->request->ajax()) {
            if ($tax) {
                return response()->json([
                    'tax' => $tax,
                    'status' => 200
                ]);
            }
            return response()->json([
                'message' => 'Tax not found!',
                'status' => 404
            ]);
        } else {
            return response($tax);
        }
    }

    public function postTax()
    {
        try {
            $new_tax = new Tax;
            $new_tax->name = $this->request->name;
            $new_tax->abbreviation = $this->request->abbreviation;
            $new_tax->rate = $this->request->rate;
            $new_tax->sales_gl_code = $this->request->sales_gl_code;
            $new_tax->purchase_gl_code = $this->request->purchase_gl_code;
            $new_tax->save();

            return response()->json([
                'message' => 'Tax created successfully.'
            ]);
        } catch (\SQLiteException $sqlx) {
            return response()->json([
                'message' => 'Could not create tax. ' . $sqlx . error_get_last()
            ]);
        }
    }

    public function updateTax()
    {

        try {
            Tax::where('id', $this->request->tax_id)
                ->update([
                    'name' => $this->request->name,
                    'abbreviation' => $this->request->abbreviation,
                    'rate' => $this->request->rate,
                    'sales_gl_code' => $this->request->sales_gl_code,
                    'purchase_gl_code' => $this->request->purchase_gl_code
                ]);

            return response()->json([
                'message' => 'Tax update was successful.'
            ]);
        } catch (\Exception $exc) {
            return response()->json([
                'message' => 'Could not update tax.'
            ]);
        }

    }

    public function deleteTax($id)
    {
        if (Tax::where('id', '=', $id)
            ->delete()) {
            return response()->json([
                'message' => 'Tax deleted.',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Could not delete tax.',
                'status' => 404
            ]);
        }
    }

}
