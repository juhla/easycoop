<?php

namespace App\Modules\BusinessSetup\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\Vendor;
use App\Modules\Base\Models\Ledger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class VendorsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);

    }

    public function index()
    {
        $this->hasAccess(['vendor']);
        $data['title'] = "Manage Vendors";
        $data['vendors'] = Vendor::where('flag', 'Active')->get();
        return view('business-setup::vendors')->with($data);
    }

    /**
     * store method to update or add new record
     * @return mixed
     */
    public function store()
    {
        $input = request()->all();

        if ($input['vendor_id'] === '') {
            $code = Ledger::getLastPayableCode();
            $input['ledger_code'] = $code;
            //leger details
            $details = [
                'name' => $input['name'],
                'code' => $code,
                'opening_balance_type' => 'C',
                'group_id' => 28
            ];
            //save customer details
            $customer = Vendor::create($input);
            if ($customer) {
                //create new ledger account
                Ledger::create($details);
            }
        } else {
            unset($input['_token']);
            $id = $input['vendor_id'];
            unset($input['vendor_id']);
            Vendor::where('id', $id)->update($input);
        }
        if (request()->ajax()) {
            return response()->json(['success' => 200, 'data' => $customer]);
        } else {
            flash()->success('Successfully saved vendor - ' . $input['name']);
            return redirect()->back();
        }
    }

    /**
     * method to get vendor details to edit
     * @param $id
     * @return mixed
     */
    public function _getVendor($id)
    {
        $res = Vendor::findOrFail($id);
        if ($res) {
            return response()->json($res);
        }
    }

    /**
     * method to delete customer
     * @param $id
     */
    public function delete($id)
    {

        if ($id) {
            $vendor = Vendor::find($id);
            $ledger = Ledger::where('code', $vendor->ledger_code)->first();
            //find transaction items
            try {
                DB::beginTransaction();
                $items = TransactionItem::where('ledger_id', $ledger->id)->get()->pluck("transaction_id", "id");
                $items = $items->toArray();
                Transaction::destroy($items);


                $res = TransactionItem::whereIn('transaction_id', $items)->get()->pluck('id', 'id');
                $res = $res->toArray();
                TransactionItem::destroy($res);
                //$items->delete();
                $ledger->delete();
                $vendor->delete();
                DB::commit();
            } catch (\Exception $exception) {
                DB::rollBack();
                app('sentry')->captureException($exception);
            }
        }


    }
}