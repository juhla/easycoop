<?php

namespace App\Modules\BusinessSetup\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('business-setup', 'Resources/Lang', 'app'), 'business-setup');
        $this->loadViewsFrom(module_path('business-setup', 'Resources/Views', 'app'), 'business-setup');
        $this->loadMigrationsFrom(module_path('business-setup', 'Database/Migrations', 'app'), 'business-setup');
        $this->loadConfigsFrom(module_path('business-setup', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('business-setup', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
