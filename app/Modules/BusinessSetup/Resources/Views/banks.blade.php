@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Bank Accounts</h3>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-8">
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th width="30%">Account Name</th>
                                <th width="25%">Account No</th>
                                <th width="25%">Bank</th>
                                <th width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($accounts) > 0)
                                @foreach($accounts as $account)
                                    <tr>
                                        <td>{{ $account->account_name }}</td>
                                        <td>{{ $account->account_number }}</td>
                                        <td>{{ $account->bank }}</td>
                                        <td>
                                            @if(canEdit())
                                            <div class="btn-group btn-group-xs">
                                                <a href="javascript:;" class="btn btn-default btn-sm edit-bank" data-value="{{ $account->id }}" ><i class="fa fa-pencil"></i></a>
                                                <a href="javascript:;" class="btn btn-danger btn-sm delete-bank" data-value="{{ $account->id }}"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td colspan="4">You have not added any bank account.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </section>
                </div>
                <div class="col-sm-4">
                    @if(canEdit())
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Add/Edit Payment Term</header>
                        <div class="panel-body">
                            <form id="bankAccountForm" role="form" autocomplete="off" method="post" action="{{url('setup/bank-accounts/store')}}" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Account Name <span class="required">*</span> </label>
                                            <input class="form-control" name="account_name" id="account_name" type="text" data-required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Account Number <span class="required">*</span> </label>
                                            <input class="form-control" name="account_number" id="account_number" type="text" data-required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="">Bank <span class="required">*</span> </label>
                                            <input class="form-control" name="bank" id="bank" type="text" data-required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" id="account_id" name="account_id" value="">
                                {{--<input type="hidden" id="row_index" name="row_index" value="">--}}
                                <div class="pull-right">
                                    <button type="reset" class="btn" onclick="$('#account_id').val('');$('#row_index').val('');">Clear</button>
                                    <button type="submit" class="btn btn-info">Save</button>
                                </div>
                            </form>
                        </div>
                    </section>
                        @endif
                </div>
            </div>
        </section>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(function(){
            var baseurl = $('#baseurl').val();
            //$('#bankAccountForm').validate();


            $(document).on('click', '.edit-bank', function(e){
                e.preventDefault();
                var id = $(this).attr('data-value');

                $.ajax({
                    url: baseurl + '/setup/bank-accounts/get/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res){
                        $.each(res, function(key, value){
                            $("input[name=" + key + "]").val(value);
                        });
                        $('#account_id').val(id);
                    },
                    error: function(res){

                    }
                });
            });

            //delete task info

            $(document).on("click", ".delete-bank", function (event) {

                var ID = $(this).attr('data-value');
                var $this = $(this);
                swal({
                    title:"Are you sure?",
                    text:"You will not be able to recover the data once deleted!",
                    type:"warning",
                    showCancelButton:!0,
                    confirmButtonColor:"#DD6B55",
                    confirmButtonText:"Yes, delete it!",
                    closeOnConfirm:!1
                },function(isConfirm){
                    if(isConfirm){
                        $.ajax({
                            type: 'GET',
                            url: baseurl+'/setup/bank-accounts/delete/' + ID,
                            context: this,
                            success: function () {
                                swal({
                                    title: "Deleted!",
                                    text: "item was deleted successfully",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                }, function(isConfirm){
                                    if(isConfirm){
                                        $this.fadeOut('slow', function () {
                                            $this.closest("tr").remove();
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });


        });
    </script>
@stop
