@extends('layouts.main')

@section('content')

    <style>
        .dd {
            width: 100% !important;
        }
    </style>
    <link href="{{ asset('js/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Manage Chart of Accounts</h3>
            </div>
            <div class="row">

                <div class="col-sm-8">
                    @include('flash::message')
                    {{--@if ($errors->any())--}}
                        {{--<div class="alert alert-danger">--}}
                            {{--<ul>--}}
                                {{--@foreach ($errors->all() as $error)--}}
                                    {{--<li>{{ $error }}</li>--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                    <section class="panel panel-default">

                        <div  class="dd dd-nodrag" id="nested_accounts">
                            <ol class="dd-list">
                                @foreach($classes as $class)
                                    <li class="dd-item" data-id="1">
                                        <div class="dd-handle dd-nodrag">
                                            {{ $class->name }}
                                        </div>
                                        @if($groups = $class->groups)
                                            <ol class="dd-list">
                                                @foreach($groups as $group)
                                                    <li class="dd-item" data-id="2">
                                                        <div class="dd-handle dd-nodrag">
                                                            {!! $group->code.' - &nbsp;'.$group->name !!}
                                                            <div class="pull-right">
                                                                @if($group->is_system_account)
                                                                    System Account
                                                                @else
                                                                    @if(canEdit())
                                                                        <a href="#" class="edit-group" data-value="{{$group->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                                        &nbsp; | &nbsp;
                                                                        <a href="{{ url('setup/accounts/groups/delete/'.$group->id) }}" class="delete-group" data-value="{{$group->id}}"><i class="fa fa-trash-o"></i> Delete</a>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if($nestedGroups = $group->nestedGroups($group->id))
                                                            {!! dumpGroups($nestedGroups->where('flag', 'Active')->orderBy('code', 'asc')->get()) !!}
                                                        @endif
                                                        @if($group->ledgers->count())
                                                            <ol class="dd-list">
                                                                @foreach($group->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                                    <li class="dd-item" data-id="3">
                                                                        <div class="dd-handle dd-nodrag">
                                                                            {!! $ledger->code.' - &nbsp;'.$ledger->name !!}
                                                                            <div class="pull-right">
                                                                                @if($ledger->is_system_account)
                                                                                    @if(canEdit())
                                                                                    <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                                                    @endif
                                                                                @else
                                                                                    @if(canEdit())
                                                                                    <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                                                    &nbsp; | &nbsp;
                                                                                    <a href="{{ url('setup/accounts/ledgers/delete/'.$ledger->id) }}" class="delete-ledger" data-value="{{$ledger->id}}"><i class="fa fa-trash-o"></i> Delete</a>
                                                                                    @endif
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ol>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ol>
                                        @endif
                                        @if($class->ledgers->count())
                                            <ol class="dd-list">
                                                @foreach($class->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                    <li class="dd-item" data-id="3">
                                                        <div class="dd-handle dd-nodrag">
                                                            {!! $ledger->code.' - &nbsp;'.$ledger->name !!}
                                                            <div class="pull-right">
                                                                @if($ledger->is_system_account)
                                                                    @if(canEdit())
                                                                        <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                                    @endif
                                                                @else
                                                                    @if(canEdit())
                                                                    <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                                                    &nbsp; | &nbsp;
                                                                    <a href="{{ url('setup/accounts/ledgers/delete/'.$ledger->id) }}" class="delete-ledger" data-value="{{$ledger->id}}"><i class="fa fa-trash-o"></i> Delete</a>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ol>
                                        @endif
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    </section>
                </div>
                <div class="col-sm-4">
                    @if(canEdit())
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="groupHeader">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#groups-form" aria-expanded="true" aria-controls="groups-form" class="collapsed groupForm">
                                        Add Group
                                    </a>
                                </h4>
                            </div>
                            <div id="groups-form" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupHeader" style="height: 0px;">
                                <div class="panel-body">
                                    @include('business-setup::partials._group-form')
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="ledgerHeader">
                                <h4 class="panel-title">
                                    <a class="ledgerForm" data-toggle="collapse" data-parent="#accordion" href="#ledger-form" aria-expanded="false" aria-controls="ledger-form">
                                        Add Ledger
                                    </a>
                                </h4>
                            </div>
                            <div id="ledger-form" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="ledgerHeader" style="">
                                <div class="panel-body">
                                    @include('business-setup::partials._ledger-form')
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </section>
    </section>
@stop
@section('scripts')
    <script src="{{ asset('js/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            var baseurl = $('#baseurl').val();

            function getLastGroupCode(id){
                $.get(baseurl + '/setup/accounts/get-group-code/' + id, function(response){
                    $('#last-group-code').html(response);
                });
            }

            function getLastLedgerCode(id){
                $.get(baseurl + '/setup/accounts/get-ledger-code/' + id, function(response){
                    $('#last-ledger-code').html(response);
                });
            }

            getLastGroupCode(1);
            getLastLedgerCode(1);

            $('#nested_accounts').nestable().nestable('collapseAll');;
            $(".dd-nodrag").on("mousedown", function(event) { // mousedown prevent nestable click
                event.preventDefault();
                return false;
            });

            //get group to edit
            $('.edit-group').click(function(e){
                e.preventDefault();

                var id = $(this).attr('data-value');

                $.ajax({
                    url: baseurl + '/setup/accounts/groups/get/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res){
                        $('.groupForm').removeClass('collapsed');
                        $('#groups-form').addClass('in').removeAttr('style');
                        $('.ledgerForm').addClass('collapsed');
                        $('#ledger-form').removeClass('in').attr('style', 'height: 0px');

                        $("#parent_id").select2('val', res.parent_id);
                        $('#group_name').val(res.name).focus();
                        $('#group_code').val(res.code);
                        $('#group_id').val(res.id);
                    },
                    error: function(res){

                    }
                });
            });
            //get gl account to edit
            $('.edit-ledger').click(function(e){
                e.preventDefault();
                var id = $(this).data('value');

                $.ajax({
                    url: baseurl + '/setup/accounts/ledgers/get/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res){
                        $('.ledgerForm').removeClass('collapsed');
                        $('#ledger-form').addClass('in').removeAttr('style');
                        $('.groupForm').addClass('collapsed');
                        $('#group-form').removeClass('in').attr('style', 'height: 0px');

                        $("#parent_group_id").select2('val', res.group_id);
                        $('#ledger_name').val(res.name).focus();
                        $('#ledger_code').val(res.code);
                        $('#balance_type').select2('val', res.opening_balance_type);
                        $('#opening_balance').val(res.opening_balance);
                        $('#ledger_id').val(res.id);
                    },
                    error: function(res){

                    }
                });
            });

            $('#parent_id').on('change', function(){
                var id = $(this).val();
                getLastGroupCode(id);
            });

            $('#parent_group_id').on('change', function(){
                var id = $(this).val();
                getLastLedgerCode(id);
            });
        });


    </script>
@stop
