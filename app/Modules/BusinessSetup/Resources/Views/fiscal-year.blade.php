@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Fiscal Years</h3>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-6">
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th>Begins</th>
                                <th>Ends</th>
                                <th>Status</th>
                                <th width="20%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($fiscalyears) > 0)
                                @foreach($fiscalyears as $fyear)
                                    <tr>
                                        <td>{{ date('d/m/Y', strtotime($fyear->begins)) }}</td>
                                        <td>{{ date('d/m/Y', strtotime($fyear->ends)) }}</td>
                                        <td>{{ $fyear->status }}</td>
                                        <td>
                                            @if(canEdit())
                                            <div class="btn-group btn-group-xs">
                                                <button type="button" class="btn btn-info edit" data-value="{{$fyear->id}}"><i class="fa fa-pencil"></i></button>
                                                <button type="button" class="btn btn-danger delete" data-value="{{$fyear->id}}"><i class="fa fa-trash-o"></i> </button>
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr id="no-record"><td colspan="5">You have not added any fiscal year</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </section>
                </div>
                <div class="col-sm-6">
                    @if(canEdit())
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Add/Edit Fiscal Year</header>
                        <div class="panel-body">
                            <form id="fiscalYearForm" role="form" autocomplete="off" method="post" action="{{url('setup/fiscal-years/manage')}}" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{csrf_token()}}" >
                                <div class="form-group">
                                    <label for="begins" class="control-label">Begins <span class="required">*</span></label>
                                    <input type="text" class="form-control datepicker" name="begins" id="begins" data-required/>
                                </div>

                                <div class="form-group">
                                    <label for="ends" class="control-label">Ends <span class="required">*</span></label>
                                    <input type="text" class="form-control datepicker" name="ends" id="ends" data-required/>
                                </div>

                                <div class="form-group form-group-default form-group-default-select2">
                                    <label class="">Status</label>
                                    <select class="select2-option" name="status" id="status" style="width: 100%">
                                        <option value="Open">Open</option>
                                        <option value="Closed">Closed</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <input type="hidden" id="fiscal_year_id" name="fiscal_year_id" value="">
                                <input type="hidden" id="row_index" name="row_index" value="">
                                <button type="submit" class="btn btn-primary" id="saving">Save</button>
                                <button type="reset" class="btn btn-default" onclick="$('#fiscal_year_id').val('');$('#row_index').val('');">Clear</button>
                            </form>
                        </div>
                    </section>
                    @endif
                </div>
            </div>
        </section>
    </section>
@stop
@section('scripts')
    <script>
        $(function(){
            var baseurl = $('#baseurl').val();
            //Date Pickers
           // $('.datepicker').datepicker();

            $('#fiscalYearForm').submit(function(event){
                event.preventDefault();
                var fiscal_year_id =   $('#fiscal_year_id').val();
                var index	=	$('#row_index').val();
                $('#saving').html('Saving...');
                $('#saving').prop('disabled', true);
//                $('body').loader('show');
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: "POST",
                    dataType: 'json',
                    success: function (e) {
                        //$('body').loader('hide');
                        if (!e.status) {
                            //$.notific8(e.message,{ life:15000,horizontalEdge:"top", theme:"danger" ,heading:" ERROR!!! "});
                            return false;
                        }else{
                            if(fiscal_year_id !== ''){
//                                $.notific8(e.message,{ life:15000,horizontalEdge:"top", theme:"theme-inverse" ,heading:" SUCCESS! "});
                                $(this).closest("tr").html(e.data);
                                $('#saving').prop('disabled', false).html('Save');
                                $('.table > tbody > tr').eq(index).html(e.data);
                            }else{
//                                $.notific8(e.message,{ life:15000,horizontalEdge:"top", theme:"theme-inverse" ,heading:" SUCCESS! "});
                                $('#fiscalYearForm')[0].reset();
                                $('#status').select2('val', '');
                                $('#saving').prop('disabled', false).html('Save');
                                $('#no-record').hide();
                                $('.table').append(e.data);
                            }
                        }
                    },
                    error: function(){
                        $('#saving').html('Save');
                        $('#saving').prop('disabled', false);
                    }
                });
            });

            $(document).on('click', '.edit', function(){
                var id 		    = 	$(this).attr('data-value');

                //get the selected row index
                $('#row_index').val($(this).closest("tr").index());

                //set the id for update
                $('#fiscal_year_id').val($(this).attr('data-value'));

                //jsonOBJ = {};
                $.ajax({
                    type	:	'GET',
                    url 	:	baseurl+'/setup/get-fiscal-year/'+id,
                    cache	:	false,
                    context : 	this,
                    success	: function(res){
                        jsonOBJ	=	$.parseJSON(res);
                        for (var key in jsonOBJ) {
                            $("input[name=" + key + "]").val(jsonOBJ[key]);
                        }
                        $('#status').select2('val', jsonOBJ['status']);
                    }
                });
            });

            //delete task info

            $(document).on("click", ".delete", function (event) {

                var ID = $(this).attr('data-value');
                var $this = $(this);
                swal({
                    title:"Are you sure?",
                    text:"You will not be able to recover the data once deleted!",
                    type:"warning",
                    showCancelButton:!0,
                    confirmButtonColor:"#DD6B55",
                    confirmButtonText:"Yes, delete it!",
                    closeOnConfirm:!1
                },function(isConfirm){
                    if(isConfirm){
                        $.ajax({
                            type: 'GET',
                            url: baseurl+'/setup/delete-fiscal-year/' + ID,
                            context: this,
                            success: function () {
                                swal({
                                    title: "Deleted!",
                                    text: "item was deleted successfully",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                }, function(isConfirm){
                                    if(isConfirm){
                                        $this.fadeOut('slow', function () {
                                            $this.closest("tr").remove();
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });
        });
    </script>
@stop
