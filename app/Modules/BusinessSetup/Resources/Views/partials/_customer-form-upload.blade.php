<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="customerUploadFormModal" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                    </button>
                    <h5><span id="title">Add</span> <span class="semi-bold">Customer</span></h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ url('setup/customers/upload') }}" method="post" id="customerModalForm"
                          data-validate="parsley" enctype="multipart/form-data">
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Upload File <span class="required">*</span></label>
                                        <input class="form-control"
                                               name="customer_file"
                                               data-buttontext="Upload Customer Details"
                                               data-buttonname="btn-default" id="filestyle-4"
                                               tabindex="-1"
                                               type="file"
                                               data-required
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                            </div>
                            <div class="col-sm-4 m-t-10 sm-m-t-10">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <button type="reset" class="btn btn-default m-t-5 close-customer-upload-modal">Close
                                </button>
                                <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->

