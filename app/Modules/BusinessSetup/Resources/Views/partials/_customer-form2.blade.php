<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="customerFormModal" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span id="title">Add</span> <span class="semi-bold">Customer</span></h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ url('setup/customers/store') }}" method="post" id="customerForm" data-validate="parsley">
                          {{csrf_field()}}
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Customer/Company Name <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="name" id="name" data-required required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Contact First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Contact Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email" id="email" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label class="">Customer Country <span class="required">*</span></label>
                                        <select class="select2-option" name="country_id" id="country_id" style="width: 100%">
                                            <option value=""></option>
                                            @foreach(getCountry() as $country)
                                                <option value="{{ $country->id }}" data-title="{{ $country->currency_code }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="">Currency</label>
                                        <select class="select2-option" name="currency_code" id="currency_code" style="width: 100%">
                                            <option value=""></option>
                                            @foreach(getCountry() as $country)
                                                <option value="{{ $country->currency_code }}">{{ $country->currency_code. '('.$country->currency_symbol.')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Address</label>
                                            <input type="text" class="form-control" name="address" id="address">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" name="phone_no" id="phone_no" data-type="number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                            </div>
                            <div class="col-sm-4 m-t-10 sm-m-t-10">
                                
                                <input type="hidden" name="customer_id" id="customer_id" value="">
                                <button type="reset" class="btn btn-default m-t-5 close-customer-modal">Close</button>
                                <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->

