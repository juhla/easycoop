<form id="form-group" role="form" autocomplete="off" method="post" action="{{ url('setup/accounts/groups/add') }}"
      data-validate="parsley">
    <div class="form-group form-group-default form-group-default-select2">
        <label class="">Parent Group <span class="required">*</span></label>
        <select class="select2-option" name="parent_id" id="parent_id" style="width: 100%">
            @foreach($classes as $class)
                <option value="{{$class->id}}" class="optionGroup">{{$class->name}}</option>
                @if($class->groups->count())
                    @foreach($class->groups as $group)
                        <option value="{{$group->id}}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $group->code.'&nbsp; - &nbsp;'.$group->name !!}</option>
                        @if($group->groups->count())
                            @foreach($group->groups as $child)
                                <option value="{{$child->id}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $child->code.'&nbsp; - &nbsp;'.$child->name !!}</option>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @endforeach
        </select>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default">
                <label>Group Name <span class="required">*</span></label>
                <input type="text" class="form-control" name="group_name" id="group_name" placeholder="Enter group name"
                       data-required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default">
                <label>Group Code <span class="required">*</span></label>
                <input type="text" class="form-control" name="group_code" id="group_code"
                       placeholder="Group Code (e.g 1100)" data-required data-type="number">
                <p class="hint-text text-info">Last code for this account was <b><span id="last-group-code"></span></b>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <input type="hidden" name="group_id" id="group_id" value="">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button class="btn btn-info" type="submit">Save</button>
</form>