<form id="form-ledger" role="form" autocomplete="off" method="post" action="{{ url('setup/accounts/ledgers/add') }}"
      data-validate="parsley">
    <div class="form-group form-group-default form-group-default-select2">
        <label class="">Parent Group <span class="required">*</span> </label>
        <select class="select2-option" name="parent_group_id" id="parent_group_id" style="width: 100%">
            @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->name}}</option>
                @if($class->groups->count())
                    @foreach($class->groups as $group)
                        <option value="{{$group->id}}">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $group->code.'&nbsp; - &nbsp;'.$group->name !!}</option>
                        @if($group->groups->count())
                            @foreach($group->groups as $child)
                                <option value="{{$child->id}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $child->code.'&nbsp; - &nbsp;'.$child->name !!}</option>
                                @if($child->nestedGroups($child->id)->count())
                                    @foreach($child->nestedGroups($child->id)->get() as $sub)
                                        <option value="{{$sub->id}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $sub->code.'&nbsp; - &nbsp;'.$sub->name !!}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @endforeach
        </select>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default">
                <label>Ledger Name <span class="required">*</span></label>
                <input type="text" class="form-control" name="ledger_name" id="ledger_name"
                       placeholder="Enter Ledger Name" data-required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default">
                <label>Ledger Code <span class="required">*</span></label>
                <input type="text" onkeypress="return isNumber(event);" class="form-control" name="ledger_code"
                       id="ledger_code" placeholder="Enter Ledger Code (e.g 1100)" data-required data-type="number">
                <p class="hint-text text-info">Last code for this account was: <b><span
                                id="last-ledger-code"></span></b></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default form-group-default-select2">
                <label class="">Balance Type<span class="required">*</span></label>
                <select class="form-control" name="balance_type" id="balance_type"
                        data-placeholder="Select Balance Type" required data-init-plugin="select2">
                    <option value="">Select One</option>
                    <option value="D">Debit</option>
                    <option value="C">Credit</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-default">
                <label>Opening Balance</label>
                <input type="text" class="form-control" name="opening_balance" id="opening_balance" value="0.00">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <input type="hidden" name="ledger_id" id="ledger_id" value=""/>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button class="btn btn-info pull-right" type="submit">Save</button>
</form>

<script>
    function isNumber(e) {
        var x = e.which || e.keycode;
        if ((x >= 48 && x <= 57) || x == 8 || (x >= 35 && x <= 40) || x == 46)
            return true;
        else
            return false;
    }
</script>