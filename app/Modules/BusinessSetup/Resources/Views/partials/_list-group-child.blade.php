<ol class="dd-list">
    <li class="dd-item" data-id="3">
        <div class="dd-handle dd-nodrag">
            {!! $group->code.' - &nbsp;'.$group->name !!}
            <div class="pull-right">
                @if($group->is_system_account === 1)
                    @if(canEdit())
                        <a href="#" class="edit-group" data-value="{{$group->id}}"><i class="fa fa-edit"></i> Edit</a>
                    @endif
                @else
                    @if(canEdit())
                    <a href="#" class="edit-group" data-value="{{$group->id}}"><i class="fa fa-edit"></i> Edit</a>
                    &nbsp; | &nbsp;
                    <a href="{{ url('setup/accounts/groups/delete/'.$group->id) }}" class="delete-group" data-value="{{$group->id}}"><i class="fa fa-trash-o"></i> Delete</a>
                    @endif
                @endif
            </div>
        </div>
        @if($nestedGroups = $group->nestedGroups($group->id))
            {!! dumpGroups($nestedGroups->orderBy('code', 'asc')->get()) !!}
        @endif
        @if($group->ledgers->count())
            <ol class="dd-list">
                {{--@foreach($group->ledgers()->where('group_id', '!=', 28)->where('group_id', '!=', 12)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)--}}
                @foreach($group->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                    <li class="dd-item" data-id="4">
                        <div class="dd-handle dd-nodrag">
                            {!! $ledger->code.' - &nbsp;'.$ledger->name !!}
                            <div class="pull-right">
                                @if($ledger->is_system_account)
                                    @if(canEdit())
                                        <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                    @endif
                                @else
                                    @if(canEdit())
                                    <a href="#" class="edit-ledger" data-value="{{$ledger->id}}"><i class="fa fa-edit"></i> Edit</a>
                                    &nbsp; | &nbsp;
                                    <a href="{{ url('setup/accounts/ledgers/delete/'.$ledger->id) }}" class="delete-group" data-value="{{$ledger->id}}"><i class="fa fa-trash-o"></i> Delete</a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </li>
                @endforeach
            </ol>
        @endif
    </li>
</ol>