<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="productFormModal" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span id="title">Add</span> <span class="semi-bold">Product</span></h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <form id="productForm" role="form" autocomplete="off" action="{{ url('setup/product/save') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token" />
                        <input type="hidden" name="product_id" id="product_id" value="" />

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default required">
                                    <label>Product Name</label>
                                    <input class="form-control" name="name" id="name" type="text" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="payment-duration">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Product Description</label>
                                    <input class="form-control" id="description" name="description" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="payment-duration">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>Price</label>
                                    <input class="form-control" id="price" name="price" type="text"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default form-group-default-select2 ">
                                    <label class="">Tax</label>
                                    <select class="" style="width: 100%;" name="tax" id="tax" >
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="reset" class="btn btn-default close-product-modal" onclick="$('#product_id').val('');">Cancel</button>
                        <button type="submit" id="saveBtn" class="btn btn-info">Create</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->