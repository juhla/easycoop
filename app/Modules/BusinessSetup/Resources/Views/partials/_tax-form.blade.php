<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="taxFormModal" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span id="title">Add</span> <span class="semi-bold">Tax</span></h5>
                    {{--<p class="p-b-10">Some description could go here.</p>--}}
                </div>
                <div class="modal-body">
                    <form role="form" action="{{ url('setup/tax') }}" method="post" id="taxForm" data-validate="parsley">
                        <div class="form-group-attached">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Name <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="name" id="name" data-required required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Abbreviation (e.g VAT)</label>
                                        <input type="text" class="form-control" name="abbreviation" id="abbreviation">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Rate <span class="required">*</span></label>
                                        <input type="text" class="form-control" name="rate" id="rate" data-required data-type="number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="">Sales Gl Account</label>
                                        <select class="select2-option" style="width: 100%" name="sales_gl_code" id="sales_gl_code" >
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group-select2">
                                        <label class="">Purchase Gl Account</label>
                                        <select class="select2-option" name="purchase_gl_code" id="purchase_gl_code" style="width: 100%">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                            </div>
                            <div class="col-sm-4 m-t-10 sm-m-t-10">
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="tax_id" id="tax_id" value="">
                                <button type="reset" data-dismiss="modal" class="btn btn-default m-t-5">Close</button>
                                <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->