@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Manage Payment Terms</h3>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-8">
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Days</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="terms"></tbody>
                        </table>
                    </section>
                </div>
                <div class="col-sm-4">
                    @if(canEdit())
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Add/Edit Payment Term</header>
                        <div class="panel-body">
                            <form id="createTermForm" role="form" autocomplete="off" action="{{ url('setup/payment-terms/save') }}" method="POST" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token" />
                                <input type="hidden" name="term_id" id="term_id" value="" />

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Name <span class="required">*</span> </label>
                                            <input class="form-control" name="name" id="name" type="text" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default form-group-default-select2">
                                            <label class="">Payment Type <span class="required">*</span> </label>
                                            <select class="form-control" name="type" id="type" data-placeholder="Select Payment Type" data-init-plugin="select2" data-required>
                                                <option value="">Please Select...</option>
                                                <option value="After No. of Days">After No. of Days</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Day in following Month">Day in following Month</option>
                                                <option value="Prepayment">Prepayment</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="payment-duration" style="display: none;">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Days or Day in following month</label>
                                            <input class="form-control" id="days" name="days" type="text" placeholder="No. of Days in following month"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group-default">
                                    <label for="Description">Description</label>
                                    <textarea name="description" id="description" class="form-control" rows="8"></textarea>
                                </div>
                                <button type="reset" class="btn btn-default">Cancel</button>
                                <button type="submit" id="saveBtn" class="btn btn-info">Create</button>
                            </form>
                        </div>
                    </section>
                    @endif
                </div>
            </div>
        </section>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        (function(){

            var paymentTerm =    {
                init:   function(){
                    this.loadTerms();
                    $(document).on('click', '#saveBtn', function(evt){
                        evt.preventDefault();
                        if($('#saveBtn').text() === 'Create'){
                            paymentTerm.save();
                        }else if($('#saveBtn').text() === 'Update'){
                            paymentTerm.update();
                        }
                    });

                    $(document).on('click', 'button', function(evt){
                        evt.preventDefault();
                        var action = $(this);
                        var _term = action.closest('tr').data('id');

                        if(action.val() === 'edit'){
                            paymentTerm.edit(_term);
                        }else if(action.val() === 'view'){
                            paymentTerm.view(_term);
                        }else if(action.val() === 'destroy'){
                            paymentTerm.destroy(_term);
                        }
                    })
                },
                loadTerms:  function(){
                    var listTerms = $('#terms');
                    var terms = '';
                    $.ajax($('#baseurl').val()+"/setup/payment-terms/get-all", {
                        type        :   'GET',
                        success     :   function(data){
                            if(data.status === 200){
                                $.each(data.data, function(index, val){
                                    terms += "<tr data-id='" + val.id + "'>";
                                    terms += "<td>"+ val.name +"</td>";
                                    terms += "<td>"+ val.payment_type +"</td>";
                                    if(val.days === null){
                                        terms += "<td> - </td>";
                                    }else{
                                        terms += "<td>"+ val.days +"</td>";
                                    }
                                    terms += "<td>"+ val.description +"</td>";
                                    terms += "<td>";
                                    terms += "@if(canEdit())<div class='btn-group btn-group-xs'>";
                                    terms += "<button value='edit' class='btn btn-sm btn-default edit'><i class='fa fa-pencil'></i></button>";
//                                    terms += " <button value='view' class='btn btn-sm btn-info view'><i class='fa fa-eye'></i></button>";
                                    terms += " <button value='destroy' class='btn btn-sm btn-danger destroy'><i class='fa fa-trash-o'></i></button>";
                                    terms += "</div>@endif";
                                    terms += "</td>";
                                    terms += "</tr>";
                                });

                                listTerms.html('');
                                listTerms.append(terms);
                            }else{
                                terms += "<tr><td colspan='5'>"+ data.message +"</td></tr>";
                                listTerms.html('');
                                listTerms.append(terms);
                            }
                        }
                    });
                },
                save:   function(){

                    var url;
                    var save_btn = $('#saveBtn');
                    var data = $('#createTermForm').serialize();
                    url = $('#baseurl').val()+"/setup/payment-terms/save";

                    $.ajax(url, {
                        type        :   'POST',
                        data        :   data,
                        beforeSend  :   function(){
                            save_btn.text('Wait....');
                            save_btn.attr('disabled','disabled');
                        },
                        success     :   function(response){
                            if(response.status === 200){
                                displayNotification(response.message, 'Success!', 'success');
                                paymentTerm.loadTerms();
                                paymentTerm.clearForm();
                            }
                        },
                        error       :   function(response, errorType, errorMessage){
                            displayNotification(errorMessage, 'Error!', 'error');
                            save_btn.removeAttr('disabled');
                            save_btn.text('Create');
                        },
                        timeout     :   9000
                    });

                },
                edit:   function(_term){
                    var term_name = $('#name');
                    var term_no = $('#term_id');
                    var term_type = $('#type');
                    var duration = $('#days');
                    var description = $('#description');
                    var save_btn = $('#saveBtn');

                    $.ajax($('#baseurl').val()+"/setup/payment-terms/get", {
                        type        :   'GET',
                        data        :   {'_term':_term},
                        cache       :   false,
                        context     :   this,
                        success     :   function(response){
                            if(response.status === 200){
                                term_name.val(response.data.name);
                                term_no.val(response.data.id);
                                term_type.val(response.data.payment_type);
                                if(term_type.val() === 'After No. of Days' || term_type.val() === 'Day in following Month'){
                                    $('#payment-duration').show();
                                    $('#days').removeAttr('disabled');
                                    duration.val(response.data.days);
                                }else{
                                    $('#payment-duration').hide();
                                    $('#days').attr('disabled','disabled');
                                }
                                description.val(response.data.description);
                                save_btn.text('Update');
                            }else{
                                displayNotification(response.message, 'Error', 'error')
                            }
                        },error       :   function(response, errorType, errorMessage){
                            displayNotification(errorMessage, 'Error!', 'error');
                            save_btn.removeAttr('disabled');
                            save_btn.text('Update');
                        },
                        timeout     :   9000
                    });
                },
                view:   function(_term){

                },
                update: function(){

                    var url;
                    var save_btn = $('#saveBtn');
                    var data = $('#createTermForm').serialize();
                    url = $('#baseurl').val()+"/setup/payment-terms/update";
                    $.ajax(url, {
                        type        :   'POST',
                        data        :   data,
                        beforeSend  :   function(){
                            save_btn.text('Wait....');
                            save_btn.attr('disabled','disabled');
                        },
                        success     :   function(response){
                            if(response.status === 200){
                                displayNotification(response.message, 'Success', 'success');
                                paymentTerm.loadTerms();
                                paymentTerm.clearForm();
                            }
                        },
                        error       :   function(response, errorType, errorMessage){
                            displayNotification(errorMessage, 'Error', 'error');
                            save_btn.removeAttr('disabled');
                        },
                        timeout     :   9000
                    });
                },
                destroy: function(_term){
                    var url = $('#baseurl').val()+"/setup/payment-terms/destroy";

                    var $this = $(this);
                    swal({
                        title:"Are you sure?",
                        text:"You will not be able to recover the data once deleted!",
                        type:"warning",
                        showCancelButton:!0,
                        confirmButtonColor:"#DD6B55",
                        confirmButtonText:"Yes, delete it!",
                        closeOnConfirm:!1
                    },function(isConfirm){
                        if(isConfirm){
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data:   {'_token': $('#_token').val(), '_term': _term },
                                context: this,
                                success: function (response) {
                                    if( response.status === 200 )
                                    {
                                        displayNotification(e.message, 'Success!', 'success');
                                        paymentTerm.loadTerms();
                                    }
                                    if( response.status === 500 )
                                    {
                                        displayNotification(e.message, 'Error!', 'error');
                                    }
                                }
                            });
                        }
                    });
                },
                clearForm:  function(){
                    var save_btn = $('#saveBtn');
                    $('#name').val('');
                    $('#term_id').val('');
                    $('#type').select2('val', '');
                    $('#days').val('');
                    $('#description').val('');

                    save_btn.text('Create');
                    save_btn.removeAttr('disabled');
                }
            }

            $('#type').on('change', function(){
                if($('#type').val() === 'After No. of Days' || $('#type').val() === 'Day in following Month'){
                    $('#payment-duration').show();
                    $('#days').removeAttr('disabled');
                }else{
                    $('#payment-duration').hide()
                    $('#days').attr('disabled','disabled');
                }
            });

            paymentTerm.init();
            //$('#createTermForm').validate();
        })();
    </script>
@stop
