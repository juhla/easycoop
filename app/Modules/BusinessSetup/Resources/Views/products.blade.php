@extends('layouts.main')

@section('content')
    <section class="vbox">
        <section class="scrollable padder">
            {{--<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="index.html"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">UI kit</a></li>
                <li><a href="#">Table</a></li>
                <li class="active">Static table</li>
            </ul>--}}
            <div class="m-b-md">
                <h3 class="m-b-none">Manage Products</h3>
            </div>
            <div class="row">
                @include('flash::message')
                <div class="col-sm-8">
                    <section class="panel panel-default">
                        {{--<header class="panel-heading">
                            <span class="label bg-danger pull-right">4 left</span> Tasks
                        </header>--}}
                        <table class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="productBody"></tbody>
                        </table>
                    </section>
                </div>

                <div class="col-sm-4">
                    @if(canEdit())
                    <section class="panel panel-default">
                        <header class="panel-heading font-bold">Add/Edit Product</header>
                        <div class="panel-body">
                            <form id="productForm" role="form" autocomplete="off" action="{{ url('setup/product/save') }}" method="POST" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token" />
                                <input type="hidden" name="product_id" id="product_id" value="" />

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Product Name <span class="required">*</span></label>
                                            <input class="form-control" name="name" id="name" type="text" data-required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="payment-duration">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Product Description</label>
                                            <input class="form-control" id="description" name="description" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="payment-duration">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Price <span class="required">*</span></label>
                                            <input class="form-control" id="price" name="price" type="text" data-required data-type="number"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default form-group-default-select2 ">
                                            <label class="">Tax</label>
                                            <select class="form-control" name="tax" id="tax" data-placeholder="Select Tax Applicable" data-init-plugin="select2" >
                                                <option value="">Please Select</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="reset" class="btn btn-default" onclick="$('#product_id').val('');">Cancel</button>
                                <button type="submit" id="saveBtn" class="btn btn-info">Create</button>
                            </form>
                        </div>
                    </section>
                        @endif
                </div>
            </div>
        </section>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        (function($){

            /***** SOME GLOBAL VARIABLES****/
            var baseurl = $("#baseurl").val();
            var save_btn = $("#saveBtn");
            var clear_btn = $("#clear");
            var p_form = $("#productForm");
            var income = $('#income_gl_account');
            var expense = $("#expense_gl_account");
            var productBody = $("#productBody");
            var productMain = $("#product-main");
            var taxes = $("#tax");

            /**For table pagination**/
            /*function paginate()
             {
             $('.paging').empty();
             productMain.simplePagination({
             previousButtonClass: "btn btn-danger",
             containerClass: 'paging',
             nextButtonClass: "btn btn-danger"
             });
             }*/

            /**
             * Save Product Function
             */
            function saveProduct(url, data)
            {
                save_btn.attr('disabled', true).text('wait...');
                $.ajax(url, {
                    type    :   'POST',
                    cache   :   false,
                    context :   this,
                    data    :   data,
                    before  :   function(){

                    },
                    success :   function(e){
                        if(e.status === 200) {
                            displayNotification(e.message, 'Success', 'success');
                            clearForm();
                            loadProducts();
                        }else{
                            displayNotification(e.message,'Error', 'error');
                        }
                        save_btn.attr('disabled', false).text('Save');
                    },
                    complete    :   function(){
                        save_btn.removeAttr('disabled');
                    },
                    error       :   function(request, errorType, errorMessage){
                        displayNotification(errorMessage, 'Error', 'error');
                        save_btn.attr('disabled', false).text('Save');
                    },
                    timeout :   3000
                });
            }

            function loadTax()
            {
                var tax = "";
                $.ajax(baseurl+"/api-v1/get-taxes", {
                    type    :   'GET',
                    success  :   function(data){
                        if(data.status === 200){
                            $.each(data.data, function(index, obj){
                                tax += "<option value='"+ obj.id +"'>"+ obj.name +"</option>";
                            });
                            taxes.append(tax);
                        }else{

                        }
                    }
                });
            }

            /**
             *
             */
            function loadProducts()
            {
                var prod = "";

                $.ajax(baseurl+"/setup/get-products", {
                    type    :   'GET',
                    success :   function(data){
                        if(data.status === 200){
                            $.each(data.data, function(index, obj){
                                prod += "<tr><td>"+obj.name+"</td><td>"+ obj.price +"</td><td>";
                                prod += "@if(canEdit())<div class='btn-group btn-group-xs'>" +
                                        "<button type='button' class='btn btn-default manage' id='edit-grp' data-action='edit' data-value="+obj.id+"><i class='fa fa-pencil'></i></button>";
                                prod += "&nbsp;<button type='button' class='btn btn-danger manage' id='delete-grp' data-action='delete'' data-value="+obj.id+"><i class='fa fa-trash-o'></i> </button>" +
                                        "</div>@endif</td></tr>";

                                productBody.html("");
                                productBody.html(prod);
                            });

                            //paginate();
                        }else{
                            productBody.html("<tr><td colspan='5'>"+ data.message +"</td></tr>");
                        }
                    }
                });
            }

            function populateForm( id )
            {
                var url = baseurl+"/setup/get-product/"+id;
                clearForm();
                $.ajax(url, {
                    type    :   'GET',
                    success :   function(data){
                        if(data.status === 200)
                        {
                            for (var key in data.data) {
                                $("input[name=" + key + "]").val(data.data[key]);
                            }
                            $('#tax').val(data.data.tax);
                            $('#product_id').val(data.data.id);
                            save_btn.text('Update');
                        }else{
                            displayNotification(data.message, 'Error!', 'error');
                        }
                    }
                });
            }

            /****For clearing form fields*****/
            function clearForm()
            {
                save_btn.text('Save').data('action', 'save');
                $('#name').val("");
                $('#description').val("");
                //$('#code').val("");
                $("#price").val("");
                $('#tax').val('');
            }

            $(document).ready(function(){
                /**
                 * Load resources
                 */
                loadProducts();
                loadTax();
                /***Actions*****/
                $(document).on('submit', p_form, function(e){
                    e.preventDefault();

                    if($('#product_id').val() === ''){
                        var data = p_form.serialize();
                        var url = p_form.attr('action');
                        saveProduct(url, data);
                    }else{
                        var data = p_form.serialize();
                        var url = baseurl+"/setup/product/update";
                        saveProduct(url, data);
                    }
                });

                $(document).on('click', '.manage', function(e){
                    if($(this).data('action') === 'edit'){
                        populateForm($(this).data('value'));
                    }

                    if($(this).data('action') === 'delete'){
                        var row = $(this).closest('tr');
                        var product_id = $(this).data('value');

                        swal({
                            title:"Are you sure?",
                            text:"You will not be able to recover the data once deleted!",
                            type:"warning",
                            showCancelButton:!0,
                            confirmButtonColor:"#DD6B55",
                            confirmButtonText:"Yes, delete it!",
                            closeOnConfirm:!1
                        },function(isConfirm){
                            if(isConfirm){
                                $.ajax({
                                    type: 'GET',
                                    url: baseurl+"/setup/product/delete/" + product_id,
//                                    data:   {'_token': $('#_token').val(), 'product_id': product_id },
                                    context: this,
                                    success: function (e) {
                                        if( e.status === 200 )
                                        {
                                            displayNotification(e.message, 'Success!', 'success');
                                            row.fadeOut();
                                            setTimeout(loadProducts(), 2000);
                                        } else {
                                            displayNotification(e.message, 'Error!', 'error');
                                        }
                                    }
                                });
                            }
                        });
                    }
                });

                /***Clear form action***/
                $(document).on('click', '#clear', function(){ clearForm(); });
            });
        })(jQuery);
    </script>
@stop
