@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">
                            @if(canEdit())
                            <button type="button" class="btn btn-sm btn-danger delete-selected" title="Remove" disabled><i class="fa fa-trash-o" ></i>
                                Delete
                            </button>
                            <a href="javascript:;" class="btn btn-sm btn-primary add-new">
                                <i class="fa fa-plus"></i> Add New
                            </a>
                            @endif

                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>Phone</th>
                                    <th width="15%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($vendors) > 0)
                                    @foreach($vendors as $vendor)
                                        <tr>
                                            <td>{{ $vendor->name }}</td>
                                            <td>{{ $vendor->email }}</td>
                                            <td>{!! $vendor->address !!}</td>
                                            <td>{{ ($vendor->country_id) ? getCountry($vendor->country_id) : '-' }}</td>
                                            <td>{{ ($vendor->phone_no) ? $vendor->phone_no : '-' }}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="javascript:;" class="btn btn-default edit-vendor" data-value="{{ $vendor->id }}"  title="Edit Vendor"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:;" class="btn btn-danger delete-vendor" data-value="{{ $vendor->id }}" title="Delete Vendor"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td colspan="6">You have not added any vendor.</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {{--<div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>--}}
                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('business-setup::partials._vendor-form')
@stop
@section('scripts')
    <script type="text/javascript">
        $(function(){
            var baseurl = $('#baseurl').val();

            $('.add-new').click(function(){
                $('#vendorFormModal').modal('show');
            });

            $('.close-modal').click(function(){
                $('#vendorForm')[0].reset();
                $('#vendorFormModal').modal('hide');
                $('#country_id').select2('val', '');
                $('#currency_code').select2('val', '');
            });

            $(document).on('click', '.edit-vendor', function(e){
                e.preventDefault();
                var id = $(this).attr('data-value');

                $.ajax({
                    url: baseurl + '/setup/vendors/get/' + id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res){
                        $.each(res, function(key, value){
                            $("input[name=" + key + "]").val(value);
                        });
                        $("#country_id").select2('val', res.country_id);
                        $("#currency_code").select2('val', res.currency_code);
                        $('#vendor_id').val(res.id);
                        $('#vendorFormModal').modal('show');
                    },
                    error: function(res){

                    }
                });
            });

            //delete task info

            $(document).on("click", ".delete-vendor", function (event) {

                var ID = $(this).attr('data-value');
                var $this = $(this);
                swal({
                    title:"Are you sure?",
                    text:"You will not be able to recover the data once deleted!",
                    type:"warning",
                    showCancelButton:!0,
                    confirmButtonColor:"#DD6B55",
                    confirmButtonText:"Yes, delete it!",
                    closeOnConfirm:!1
                },function(isConfirm){
                    if(isConfirm){
                        $.ajax({
                            type: 'GET',
                            url: baseurl+'/setup/vendors/delete/' + ID,
                            context: this,
                            success: function () {
                                swal({
                                    title: "Deleted!",
                                    text: "item was deleted successfully",
                                    confirmButtonColor: "#66BB6A",
                                    type: "success"
                                }, function(isConfirm){
                                    if(isConfirm){
                                        $this.fadeOut('slow', function () {
                                            $this.closest("tr").remove();
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            });


            $('#country_id').on('change', function () {
                var currency = $('option:selected', this).data('title');
                //set currency
                $('#currency_code').select2('val', currency);
            });
        });
    </script>
@stop
