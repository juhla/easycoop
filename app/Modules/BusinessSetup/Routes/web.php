<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => 'setup', 'middleware' => ['auth:web', 'verify_license']], function () {

    Route::get('/accounts', 'AccountsController@index');
    Route::post('accounts/groups/add', 'AccountsController@save');
    Route::get('accounts/groups/get/{id}', 'AccountsController@_get');
    Route::get('accounts/groups/delete/{id}', 'AccountsController@deleteGroup');

    Route::post('accounts/ledgers/add', 'AccountsController@saveLedger');
    Route::get('accounts/ledgers/get/{id}', 'AccountsController@_getLedger');
    Route::get('accounts/ledgers/delete/{id}', 'AccountsController@deleteLedger');

    Route::get('accounts/get-group-code/{id}', 'AccountsController@getLastGroupCode');
    Route::get('accounts/get-ledger-code/{id}', 'AccountsController@getLastLedgerCode');

    Route::get('accounts/get-ledger-accounts/{id}', 'AccountsController@getLedgerAccounts');

    //////////////////////////////////////
    ////////ROUTE FOR FISCAL YEAR//////////
    //////////////////////////////////////
    Route::get('/fiscal-years', 'FiscalYearsController@index');
    Route::post('/fiscal-years/manage', 'FiscalYearsController@manage');
    Route::get('/get-fiscal-year/{id}', 'FiscalYearsController@getFiscalYear');
    Route::get('/delete-fiscal-year/{id}', 'FiscalYearsController@delete');

    //////////////////////////////////////
    ////////ROUTE FOR CUSTOMERS //////////
    //////////////////////////////////////
    Route::get('/customers', 'CustomersController@index');
    Route::get('/customers/download', 'CustomersController@download');
    Route::post('/customers/upload', 'CustomersController@upload');
    Route::post('/customers/store', 'CustomersController@store');
    Route::get('/customers/get/{id}', 'CustomersController@_getCustomer');
    Route::get('/customers/delete/{id}', 'CustomersController@delete');

    //////////////////////////////////////
    ////////ROUTE FOR VENDORS //////////
    //////////////////////////////////////
    Route::get('/vendors', 'VendorsController@index');
    Route::post('/vendors/store', 'VendorsController@store');
    Route::get('/vendors/get/{id}', 'VendorsController@_getVendor');
    Route::get('/vendors/delete/{id}', 'VendorsController@delete');


    //////////////////////////////////////
    ////////ROUTE FOR PROFESSIONAL AGENT //////////
    //////////////////////////////////////
    Route::get('/professional/agents', 'ProfessionalAgentController@getAgent');
    Route::get('/professional/agents/my_company', 'ProfessionalAgentController@getAgent');
    Route::post('/professional/agents/save_map_agent', 'ProfessionalAgentController@save_map_agent');
    Route::get('/professional/agents/map_agents', 'ProfessionalAgentController@mapAgents');
    Route::get('/professional/agents/agentSmes', 'ProfessionalAgentController@agentSmes');
    Route::any('/professional/agents/store', 'ProfessionalAgentController@storeAgent');
    Route::get('/professional/agents/get/{id}', 'ProfessionalAgentController@getAnAgent');
    Route::get('/professional/agentsSme/delete', 'ProfessionalAgentController@unAssignBusiness');
    Route::get('/professional/agents/delete/{id}', 'ProfessionalAgentController@deleteAgent');


    ///     //////////////////////////////////////
    ////////ROUTE FOR FINANCE PROVIDER AGENT //////////
    //////////////////////////////////////
    Route::get('/agents', 'InfluencerAgentsController@getAgent');
    Route::get('/agents/my_company', 'InfluencerAgentsController@getAgent');
    Route::post('/agents/save_map_agent', 'InfluencerAgentsController@save_map_agent');
    Route::get('/agents/map_agents', 'InfluencerAgentsController@mapAgents');
    Route::get('/agents/agentSmes', 'InfluencerAgentsController@agentSmes');
    Route::any('/agents/store', 'InfluencerAgentsController@storeAgent');
    Route::get('/agents/get/{id}', 'InfluencerAgentsController@getAnAgent');
    Route::get('/agentsSme/delete', 'InfluencerAgentsController@unAssignBusiness');
    Route::get('/agents/delete/{id}', 'InfluencerAgentsController@deleteAgent');

    //////////////////////////////////////
    ///ROUTE FOR TAXES  //////////
    //////////////////////////////////////
    Route::get('/taxes', 'TaxesController@index');
    Route::get('/get-taxes', 'TaxesController@getAllTax');
    Route::get('/get-tax/{id}', 'TaxesController@getTax');
    Route::post('/add-tax', 'TaxesController@postTax');
    Route::post('/update-tax', 'TaxesController@updateTax');
    Route::get('/delete-tax/{id}', 'TaxesController@deleteTax');

    //////////////////////////////////////
    ////////ROUTE FOR BANK ACCOUNTS///////
    //////////////////////////////////////
    Route::get('/bank-accounts', 'BankAccountsController@index');
    Route::post('/bank-accounts/store', 'BankAccountsController@store');
    Route::get('/bank-accounts/get/{id}', 'BankAccountsController@_getAccount');
    Route::get('/bank-accounts/delete/{id}', 'BankAccountsController@delete');

    //////////////////////////////////////
    ///ROUTE FOR PAYMENT TERMS //////////
    //////////////////////////////////////
    Route::get('/payment-terms', 'PaymentTermsController@index');
    Route::get('/payment-terms/get-all', 'PaymentTermsController@getAllTerms');     // Get all payment terms belonging to a specified company
    Route::get('/payment-terms/get', 'PaymentTermsController@getTerm');              // Get a specific payment term belonging to a specified company

    Route::post('/payment-terms/save', 'PaymentTermsController@postTerm');            // Create new payment term
    Route::post('/payment-terms/update', 'PaymentTermsController@updateTerm');   // Update a payment term
    Route::post('/payment-terms/destroy', 'PaymentTermsController@destroyTerm'); // Destroy a payment term

    //////////////////////////////////////
    ///ROUTE FOR PRODUCT SETUP  //////////
    //////////////////////////////////////
    Route::get('/products', 'ProductsController@index');
    Route::get('/get-products', 'ProductsController@getAllProducts'); // Get all products
    Route::get('/get-product/{id}', 'ProductsController@getProduct'); // Get just a specific products
    Route::post('/product/save', 'ProductsController@postProduct'); // Store new products
    Route::post('/product/update', 'ProductsController@updateProduct'); // Updates a products
    Route::get('/product/delete/{id}', 'ProductsController@destroyProduct'); // Destroys a products

});

