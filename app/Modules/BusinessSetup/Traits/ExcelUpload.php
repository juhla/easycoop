<?php

namespace App\Modules\BusinessSetup\Traits;

use App\Modules\Base\Models\Customer;
use App\Modules\Base\Models\Ledger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;
use Zizaco\Entrust\EntrustFacade as Entrust;

trait ExcelUpload
{

    protected $saveErrors = [];
    protected $isCustomer;
    protected $isLedger;

    /**
     * @return mixed
     */
    public function getSaveErrors()
    {
        return $this->saveErrors;
    }

    /**
     * @param mixed $saveErrors
     */
    public function setSaveErrors($saveErrors): void
    {
        $this->saveErrors[] = $saveErrors;
    }

    function handleCustomerData($fileName)
    {
        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        libxml_disable_entity_loader(false);
        $file = $storagePath . 'uploaded/' . $fileName;
        $collection = (new FastExcel)->importSheets($file);
        $collection->each(function ($sheet, $key) {

            foreach ($sheet as $eachResult) {
                try {


                    $rules = [
                        'Name' => 'required',
                        'Email' => 'required|email',
                    ];
                    $validator = Validator::make($eachResult, $rules);
                    if ($validator->fails()) {
                        $this->setSaveErrors($validator->getMessageBag()->all());
                    }

                    $resultCustomer = Customer::where('name', $eachResult['Name'])->first();
                    $this->isCustomer = empty($resultCustomer);

                    $resultLedger = Ledger::where('name', $eachResult['Name'])
                        ->where('group_id', 12)
                        ->first();
                    $this->isLedger = empty($resultLedger);


                    //  dd($resultLedger, $this->isLedger);

                    if (!$this->isCustomer && $this->isLedger) {

                        $code = $resultCustomer->ledger_code;
                        $details = [
                            'name' => $eachResult['Name'],
                            'code' => $code,
                            'opening_balance_type' => 'C',
                            'group_id' => 12
                        ];
                        Ledger::create($details);
                    } else if ($this->isCustomer && !$this->isLedger) {
                        $input = [
                            'name' => $eachResult['Name'],
                            'ledger_code' => $resultLedger->code,
                            'email' => $eachResult['Email'],
                            'first_name' => $eachResult['First Name'],
                            'last_name' => $eachResult['Last Name'],
                            'phone_no' => $eachResult['Phone Number'],
                            'address' => $eachResult['Address'],
                        ];
                        $customer = Customer::create($input);
                    } else if ($this->isCustomer && $this->isLedger) {
                        $code = Ledger::getLastReceivableCode();
                        $input = [
                            'name' => $eachResult['Name'],
                            'ledger_code' => $code,
                            'email' => $eachResult['Email'],
                            'first_name' => $eachResult['First Name'],
                            'last_name' => $eachResult['Last Name'],
                            'phone_no' => $eachResult['Phone Number'],
                            'address' => $eachResult['Address'],
                        ];
                        $customer = Customer::create($input);
                        if ($customer) {
                            $details = [
                                'name' => $eachResult['Name'],
                                'code' => $code,
                                'opening_balance_type' => 'C',
                                'group_id' => 12
                            ];
                            Ledger::create($details);
                        }

                    }

                    // dd('ddd');
                } catch (\Exception $exception) {

                    dd($exception->getMessage());
                    $this->setSaveErrors($exception->getMessage() . " >> " . $exception->getLine());

                }


            }
        });

    }

}
