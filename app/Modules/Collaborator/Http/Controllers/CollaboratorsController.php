<?php

namespace App\Modules\Collaborator\Http\Controllers;

use Auth;
use App\Modules\Base\Models\Agents\AgentSme;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Models\User;
use App\Modules\Base\Models\PersonalInfo;
use App\Modules\Collaborator\Models\Collaborator;
use App\Modules\Base\Models\Role;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\UserLicenceSubscriber;
use App\Modules\Base\Traits\Functions;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Traits\Mailer;
use App\Modules\Base\Traits\TenantConfig;
use Redirect;
use Session;
use App\Modules\Base\Models\Payment;
use Illuminate\Http\Request;
use App\Modules\Base\Http\Requests;
use App\Modules\Base\Http\Controllers\Controller;

class CollaboratorsController extends Controller
{


    use Mailer, Functions;
    use License, TenantConfig;

    public function __construct(Collaborator $collaborator)
    {

        $this->collaborator = $collaborator;
        $this->middleware(['full_tenant']);
    }

    /**
     * Function to change active company for influencers
     * @return mixed
     */
    public function changeActiveCompany()
    {

        //forget previous sessions
        try {
            session()->forget('company_name');
            session()->forget('company_db');
            $db = request()->get('slug');

            $coy = Company::where('database', $db)
                ->join('user_license_subscribers', 'user_license_subscribers.user_id', '=', 'companies.user_id')
                // ->join('user_license_packages', 'user_license_subscribers.license_id', '=', 'user_license_packages.id')
                ->select('user_license_subscribers.*', 'companies.*')
                ->first();
            $license = UserLicenceSubscriber::findBmacDefaultLicenceByRole();


            if (!$this->updateAnnual()) {
                session()->forget('company_name');
                session()->forget('company_db');
                $data = $this->profilePage();
                $data['error'] = "The Business Owner's License Has Expired";
                return view('profile.influencers2')->with($data);
                // return redirect()->back()->with($error);

            }


            $assemblyLineEmail = config("constants.assemblyLineEmail");
            $theCOllabEmail = auth()->user()->email;
            $checkRole = !auth()->user()->hasRole(['assemblyline-professional', 'assemblyline-professional-agent']);
            if (($license->id == $coy->license_id) && (!in_array($theCOllabEmail, $assemblyLineEmail)) && ($checkRole)) {
                session()->forget('company_name');
                session()->forget('company_db');
                $data = $this->profilePage();
                $data['error'] = 'The Business Owner needs to upgrade his/her account before you can access it';
                return view('profile.influencers2')->with($data);
                // return redirect()->back()->with($error);
            }

            if ($coy) {
                session()->put('company_name', $coy->company_name);
                session()->put('company_db', $coy->database);
                $this->setConfig(session()->get('company_db'));
                // app()->make('setDbConnection', ['db' => session()->get('company_db')]);
            }
            return redirect()->to('/workspace');
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
            return redirect()->back();
        }
    }

    public function businesses()
    {

        $user = auth()->user();
        if (auth()->user()->hasRole(['agent-finance-provider'])) {
            $agentSME = new AgentSme();
            $agent = User::where('id', $user->id)->first()->displayName();
            $data['title'] = "SMEs for Agent :: " . $agent;
            $data['agent_id'] = $user->id;
            $data['business'] = $agentSME->getAgentBusiness($user->id);
            return view('agents.agent-sme')->with($data);
        }
        $data['title'] = 'Collaborators';
        $data['clients'] = Collaborator::with('Company')->where('client_id', auth()->user()->id)->where('flag', 'Active')->get();
        return view('Collaborator::businesses')->with($data);
    }

    public function index()
    {
        $data['title'] = 'Collaborators';
        $result = Collaborator::where('user_id', auth()->user()->id)->
        where('flag', 'Active')->groupBy('user_id', 'client_id')->
        whereHas('client.roles', function ($q) {
            $q->whereNotIn('name', ["employee-payroll", "admin-payroll", "employee-ims"]);
        })->with('client')
            ->get();


        //dd($resultCollaborator);
        $data['clients'] = $result;
        $data['showPermission'] = true;
        return view('Collaborator::list')->with($data);


    }

    public function payrollList()
    {
        $data['title'] = 'Payroll';

        $resultCollaborator = Collaborator::where('user_id', auth()->user()->id)->
        where('flag', 'Active')->groupBy('user_id', 'client_id')->
        whereHas('client.roles', function ($q) {
            $q->whereIn('name', ['employee-payroll', 'admin-payroll']);
        })->with('client')
            ->get();

        $data['clients'] = $resultCollaborator;
        $data['showPermission'] = false;
        return view('Collaborator::list')->with($data);
    }

    public function imsList()
    {
        $data['title'] = 'IMS';

        $resultCollaborator = Collaborator::where('user_id', auth()->user()->id)->
        where('flag', 'Active')->groupBy('user_id', 'client_id')->
        whereHas('client.roles', function ($q) {
            $q->whereIn('name', ['employee-ims']);
        })->with('client')
            ->get();

        $data['clients'] = $resultCollaborator;
        $data['showPermission'] = false;
        return view('Collaborator::list')->with($data);
    }

    public function create()
    {

        $company = Company::where('id', auth()->user()->company->id)->first();
        $data['rc_number'] = $company->rc_number;
        $data['company_name'] = $company->company_name;
        //dd( $data['rc_number']);
        $data['title'] = 'Add a Collaborator';
        $data['roles'] = Role::where('name', 'employee')
            ->orWhere('name', 'professional')
            ->orWhere('name', 'finance-provider')
            ->orWhere('name', 'trade-promoter')
            ->orWhere('name', 'admin-payroll')
            ->get();
        return view('Collaborator::add')->with($data);
    }

    public function store(Request $request)
    {
        if ($input = request()->all()) {
            $user_status = $request->user_status;
            //$user_status = Input::get('user_status');
            if ($user_status == 1) {
                $rules = [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email',
                    'rc_number' => 'required',
                    'phone' => 'max:15'
                ];
            } else {
                $rules = [
                    'email' => 'required|email',
                    'rc_number' => 'required'
                ];
            }


            $validator = \Validator::make(request()->all(), $rules);
            //if validation fails, redirect with errors

            if ($validator->fails()) {

                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            Company::where('id', auth()->user()->company->id)
                ->update(['rc_number' => $input['rc_number']]);
            //find role
            $role = Role::find($input['role_id']);
            $input['role'] = $role->display_name;
            //check for user package and validate no of allowed users
            $isValid = UserLicenceSubscriber::validate(getBusinessOwnerID(), $role, $input);

            if ($role->name == 'admin-payroll') {
                if ($this->isUserPayrollActivated()) {
                    if (!$this->isPayrollLimitExceeded()) {
                        $isValid = 'valid';
                    } else {
                        flash()->error($this->licenseError());
                        return redirect()->back()->withInput();
                    }
                } else {
                    flash()->error($this->licenseError());
                    return redirect()->back()->withInput();
                }
            }
            switch ($isValid) {
                case 'valid':
                    //get company info for mail
                    $company = Company::find(auth()->user()->company->id);
                    $input['company_name'] = $company->company_name;

                    //get/set first free users
                    $isFree = $this->collaborator->checkFree(auth()->user()->id);

                    //generate verification code
                    $input['confirmation_code'] = generate_verification_code();
                    //check if user exist already
                    $user = User::where('email', $input['email'])->first();
                    if (count($user) > 0) {

                        if (($role->name == 'employee') && ($user->hasRole(['employee-ims', 'employee-payroll']))) {

                            $this->collaborator->user_id = auth()->user()->id;
                            $this->collaborator->client_id = $user->id;
                            $this->collaborator->company_id = auth()->user()->company->id;
                            $this->collaborator->permission = $input['permission'];
                            $this->collaborator->access = $isFree ? "Free" : "Paid";
                            $this->collaborator->save();
                            $input['record'] = $this->collaborator->id;
                            $user->attachRole($role->id);

                            try {
                                $input['type'] = 'registered';
                                $this->sendMail('emails.invite', $input, $input['company_name'] . ' invited you to BMAC');
                            } catch (\Exception $e) {
                                app('sentry')->captureException($e);
                                return $e->getMessage();
                            }
                            flash()->success('Your collaboration request has been sent.');
                            return redirect()->to('collaborators/list');

                        }

                        //if user is not business owner, send invitation
                        if ($user->hasRole(['professional', 'finance-provider', 'trade-promoter', 'assemblyline-professional'])) {
                            //save msme user info
                            $this->collaborator->user_id = auth()->user()->id;
                            $this->collaborator->client_id = $user->id;
                            $this->collaborator->company_id = auth()->user()->company->id;
                            $this->collaborator->permission = $input['permission'];
                            $this->collaborator->access = $isFree ? "Free" : "Paid";
                            $this->collaborator->save();
                            $input['record'] = $this->collaborator->id;

                            try {
                                $input['type'] = 'registered';
                                $this->sendMail('emails.invite', $input, $input['company_name'] . ' invited you to BMAC');
                            } catch (\Exception $e) {
                                app('sentry')->captureException($e);
                                return $e->getMessage();
                            }
                            flash()->success('Your collaboration request has been sent.');
                            return redirect()->to('collaborators/list');
                        } else {
                            flash()->error('You cannot invite this user!');
                            return redirect()->back()->withInput();
                        }
                    } else {
                        //save user info
                        $user = User::create($input);
                        //assign user to role
                        $user->attachRole($role->id);
                        //save personal info

                        $input['user_id'] = $user->id;
                        PersonalInfo::create($input);
                        //save company info
                        if ($role->name != 'admin-payroll') {
                            $company = new Company();
                            $company->user_id = $user->id;
                            $company->save();
                        }


                        if ($role->name == 'admin-payroll') {
                            $userId = getBusinessOwnerID();
                            PayrollLicenceSubscriber::whereUser_id($userId)->increment('count');
                        }

                        //save msme user info
                        $this->collaborator->user_id = auth()->user()->id;
                        $this->collaborator->client_id = $user->id;
                        $this->collaborator->company_id = auth()->user()->company->id;
                        $this->collaborator->permission = $input['permission'];
                        $this->collaborator->access = $isFree ? "Free" : "Paid";
                        $this->collaborator->save();


                        //send invitation mail to user
                        $input['record'] = $this->collaborator->id;
                        //dd($input);
                        try {
                            $input['type'] = 'not-registered';
                            $this->sendMail('emails.invite', $input, $input['company_name'] . ' invited you to BMAC');
                        } catch (\Exception $e) {
                            app('sentry')->captureException($e);
                            return $e->getMessage();
                        }

                        flash()->success('Your collaboration request has been sent.');
                        return redirect()->to('collaborators/list');
                    }
                    break;
                case 'not-allowed':
                    flash()->error('You must upgrade your license to add either an Influencer or a Professional');
                    return redirect()->back()->withInput();
                    break;
                case 'limit-exceeded':
                    flash()->error('You have exceed the user limit on your current plan. Please upgrade your plan to add more users');
                    return redirect()->back()->withInput();
                    break;
                default:
                    return redirect()->back()->withInput();
                    break;
            }

        }
    }

    /**
     *
     * @param $id
     */
    public function changePermission()
    {
        $action = request()->get('action');
        $client = request()->get('client');

        $message = "";
        switch ($action) {
            //case 1 - change permission to edit
            case 1:
                Collaborator::where('id', $client)->update(['permission' => 'view']);
                $message = "Permission for the user changed to edit";
                break;
            //case 2 - change permission to edit and view
            case 2:
                Collaborator::where('id', $client)->update(['permission' => 'edit']);
                $message = "Permission for the user changed to edit and view ";
                break;
            //case 3 - revoke access/deactivate account
            case 3:
                $res = Collaborator::find($client);

                //check if user is an employee
                if ($res->client->role_id === 5) {
                    //delete collaborator info
                    Collaborator::where('id', $client)->delete();
                    //delete personal information
                    PersonalInfo::where('user_id', $res->client_id)->update(['flag' => 'Inactive']);
                    //delete user info
                    User::where('id', $res->client_id)->update(['flag' => 'Deactivated', 'active' => 0]);
                } else {
                    //delete collaborator info
                    Collaborator::where('id', $client)->delete();
                }
                $theLicense = $this->getUserLicense();
                $theLicense->decrement('count', 1);
                $message = "Users access Revoked/Account Deactivated ";
                break;
        }
        flash()->success($message);
        return redirect()->to('membership/user-licenses');
    }

    public function inviteResponse()
    {
        $request = request()->all();
        //get response
        //$response = $request['response'];
        $res = Collaborator::find($request['record']);
        //check if collaboration record exist
        if (!$res) {
            flash()->error('You access has been revoked');
            return redirect()->to('secure/sign-in');
        }
        $status = 2;
        $assemblyLineEmail = config("constants.assemblyLineEmail");
        if (in_array($request['email'], $assemblyLineEmail)) {
            // Accept Response for Assembly line
            $status = 5;
        }

        //check if user exist
        $user = User::where('email', $request['email'])->first();
        //change status to based on user role
        //if employee, change status to 2
        //if influencer or professional change status to 1, pending payment


        if ((!$user->hasRole(['admin-payroll'])) && (!in_array($request['email'], $assemblyLineEmail))) {
            //update user count in user license subscriber table
            $subscriber = UserLicenceSubscriber::where('user_id', $res->user_id)->first();
            $subscriber->count = $subscriber->count + 1;
            $subscriber->save();
        }

        //        if ($user->hasRole('employee')) {
        //            //update user count in user license subscriber table
        //            $subscriber = UserLicenceSubscriber:: where('user_id', $res->user_id)->first();
        //            $subscriber->count = $subscriber->count + 1;
        //            $subscriber->save();
        //        } else {
        //            $count = Collaborator::where('client_id', $user->id)->where('status', 2)->count();
        //            if ($user->hasRole('professional')) {
        //                //if user is professional, check if user has used more than 5 and set status to 1
        //                if ($count > 5) {
        //                    $status = 1;
        //                }
        //            } else if ($user->hasRole(['finance-provider', 'trade-promoter'])) {
        //                //check if finance provider has used up 50 free access and set status to 1
        //                if ($count > 50) {
        //                    $status = 1;
        //                }
        //            }
        //        }

        if ($request['type'] === 'not-registered') {

            if ($request['code'] === $user->confirmation_code) {
                $user->confirmed = 1;
                $user->active = 1;
                $user->save();

                Collaborator::where('id', $request['record'])->update(['status' => $status, 'flag' => 'Active']);
                $request['email'] = $res->employer->email;
                $request['first_name'] = $user->personal_info->first_name;
                $request['last_name'] = $user->personal_info->last_name;

                flash()->success('Welcome ' . $user->personal_info->first_name . ', please enter a password to continue.');
                try {
                    $this->sendMail('emails.response', $request, $user->displayName() . ' accepted your request');
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);

                }
                return view('auth::auth.verify');
            } else {
                flash()->success('Unable to validate your account. Please send an email to support@ikooba.com.');
                return redirect()->to('secure/sign-in');
            }

        } else {

            Collaborator::where('id', $request['record'])->update(['status' => $status, 'flag' => 'Active']);

            $request['email'] = $res->employer->email;
            try {
                $request['first_name'] = $user->personal_info->first_name;
                $request['last_name'] = $user->personal_info->last_name;
            } catch (\Exception $ex) {
                $request['first_name'] = "";
                $request['last_name'] = "";
            }
            try {
                $this->sendMail('emails.response', $request, $user->displayName() . ' accepted your request');
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
            flash()->success('You have accepted a collaboration request. Please login to your account');
            //return to login page
            return redirect()->to('secure/sign-in');
        }
    }

    public function changeStatus()
    {
        $action = request()->get('action');
        $client = request()->get('client');
        $reference_no = request()->get('ref_no');
        $message = "";
        switch ($action) {
            case 2://accepted and maid payment
                //verify payment from paystack
                //$result = $this->verifyPaystack($reference_no);
                //if($result['data']['success'] === 'success') {
                //enter payment details
                $payment = new Payment();
                $payment->user_id = auth()->user()->id;
                //$payment->license_id = $plan->id;
                $payment->amount = 18000;
                $payment->reference_no = $reference_no;
                $payment->description = 'made payment for access';
                $payment->save();

                Collaborator::where('id', $client)->update(['status' => 2]);
                $message = "Payment was successful ";
                return response(['status' => 'success', 'message' => $message]);

                //                }else{
                //                    $message = "Payment was unsuccessful ";
                //                    return response(['status' => 'error', 'message' => $message]);
                //                }
                break;
            //case 3 - disconnect with business account
            case 3:
                $res = Collaborator::find($client);
                Collaborator::where('id', $client)->update(['status' => 3]);
                $message = "You have been disconnected with user";
                return response(['status' => 'success', 'message' => $message]);
                break;
        }
        //flash()->success($message);
    }

    public function getAgent()
    {
        //app()->make('setDbConnection', ['db' => session()->get('company_db')]);
        $data['title'] = "Manage Agents";
        $data['agents'] = User::where('flag', 'Active')->get();
        return view('setup.finance-provider-agents')->with($data);
    }

    public function storeAgent()
    {
        // app()->make('setDbConnection', ['db' => session()->get('company_db')]);
        $data['title'] = "Manage Agents";
        $data['agents'] = User::where('flag', 'Active')->get();
        return view('setup.finance-provider-agents')->with($data);
    }

    private function verifyPaystack($reference_no)
    {
        $result = array();
        //The parameter after verify/ is the transaction reference to be verified
        $url = 'https://api.paystack.co/transaction/verify/' . $reference_no;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . config('services.paystack.key')]
        );
        $request = curl_exec($ch);
        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
        }
        return $result;
    }


}
