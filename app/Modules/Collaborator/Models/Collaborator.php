<?php

namespace App\Modules\Collaborator\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\UserLicense;
use App\Modules\Base\Models\User;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Collaborator extends \App\Modules\Base\Models\Collaborator
{ }
