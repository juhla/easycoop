@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/receivables/pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/receivables/xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                <button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>--}}
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th width="30%">Company Name</th>
                                    <th width="15%">Email</th>
                                    <th width="10%">Status</th>
                                    <th width="15%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {{ session()->get('success') }}
                                @if(count($clients) > 0)
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>
                                                <?php
                                                if ($client->status === 2) {
                                                    $url = url('dashboard/change-company?slug=' . $client->company->database);
                                                    //$url = url('dashboard/change-company');
                                                } else {
                                                    $url = '#';
                                                }
                                                ?>
                                                <a href="{{ $url }}">{{ $client->company->company_name }}</a>
                                            </td>
                                            <td>{{ $client->client->email }}</td>
                                            <td>
                                                @if($client->status === 0)
                                                    <span class='label label-warning'>Pending</span>
                                                @elseif($client->status === 1)
                                                    <span class='label label-warning'>Awaiting Payment</span>
                                                @elseif($client->status ===2)
                                                    <span class='label label-success'>Accepted</span>
                                                @elseif($client->status ===3)
                                                    <span class='label label-danger'>Rejected</span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group btn-group-xs">

                                                    <div class="btn-group">
                                                        <button class="btn btn-default dropdown-toggle"
                                                                data-toggle="dropdown"><i class="fa fa-lock"></i> Action
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <script src="https://js.paystack.co/v1/inline.js"></script>
                                                            @if($client->status === 0)
                                                                <li><a href="javascript:;"
                                                                       onclick="payWithPaystack({{ $client->id }}, 2)">Accept
                                                                        and make payment</a></li>
                                                            @elseif($client->status === 1)
                                                                <li><a href="javascript:;"
                                                                       onclick="payWithPaystack({{ $client->id }}, 2)">Make
                                                                        Payment</a></li>
                                                            @elseif($client->status === 2)
                                                                <li><a href="javascript:;" class="permission"
                                                                       data-value="{{ $client->id }}" data-action="3">Disconnect</a>
                                                                </li>
                                                            @endif

                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">You have No Businesses.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        function payWithPaystack(client, action) {
            var baseurl = $('#baseurl').val();
            var handler = PaystackPop.setup({
                key: '{{ config('services.paystack.key') }}',
                email: '{{ auth()->user()->email }}',
                amount: (18000 * 100),
                ref: Date.now(),
//                metadata: {
//                    custom_fields: [
//                        {
//                            display_name: "Mobile Number",
//                            variable_name: "mobile_number",
//                            value: "+2348012345678"
//                        }
//                    ]
//                },
                callback: function (response) {
                    $.ajax({
                        type: 'GET',
                        url: baseurl + '/collaborator/change-status?action=' + action + '&client=' + client + '&ref_no=' + response.reference,
                        context: this,
                        success: function (res) {
                            if (res.status === 'success') {
                                displayNotification(res.message, 'Success!', 'success');
                                setTimeout(function () {
                                    window.location.reload();
                                }, 3000);
                            } else {
                                displayNotification(res.message, 'Error!', 'error');
                            }
                        },
                        error: function () {
                            displayNotification('Something went wrong', 'Error!', 'error');
                        }
                    });
                },
//                onClose: function(){
//                    alert('window closed');
//                }
            });
            handler.openIframe();
        }

        $('.permission').on('click', function (e) {
            var baseurl = $('#baseurl').val();
            var client = $(this).data('value');
            var action = $(this).data('action');
            $.ajax({
                type: 'GET',
                url: baseurl + '/collaborator/change-status?action=' + action + '&client=' + client,
                context: this,
                success: function (msg) {
                    displayNotification('Operation was successful', 'Success!', 'success');

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);

                },
                error: function () {
                    displayNotification('Something went wrong', 'Error!', 'error');
                }
            });
        });

    </script>
@stop
