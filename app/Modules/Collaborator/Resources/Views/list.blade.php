@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-12 m-b-xs">
                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">--}}
                            {{--<i class="fa fa-caret-right text fa-lg"></i>--}}
                            {{--<i class="fa fa-caret-left text-active fa-lg"></i>--}}
                            {{--</a>--}}
                            {{--<div class="btn-group">--}}
                            {{--<a href="{{ url('reports/receivables/pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>--}}
                            {{--<a href="{{ url('reports/receivables/xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>--}}
                            {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>--}}
                            {{--<ul class="dropdown-menu">--}}
                            {{--<li><a href="#">Action</a></li>--}}
                            {{--<li><a href="#">Another action</a></li>--}}
                            {{--<li><a href="#">Something else here</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li><a href="#">Separated link</a></li>--}}
                            {{--</ul>--}}
                            {{--</div>--}}
                            <div class="btn-group">
                                <a href="{{ url('collaborators/add-new') }}" class="btn btn-sm btn-default"><i
                                            class="fa fa-plus"></i> Add a Collaborator </a> &nbsp; &nbsp;
                                <a href="{{ url('collaborators/list-payroll') }}" class="btn btn-sm btn-default">
                                    Payroll</a>
                                <a href="{{ url('collaborators/list-ims') }}" class="btn btn-sm btn-default"> IMS</a>
                                &nbsp; &nbsp; &nbsp;
                                <a href="{{ url('assemblyline/add-new') }}" class="btn btn-sm btn-default"><i
                                            class="fa fa-plus"></i> Add Assembly Line </a>
                                <a href="{{ url('cancel/assemblyline') }}" class="btn btn-sm btn-default"><i
                                            class="fa fa-window-close"></i> Cancel Assembly Line </a>
                            </div>
                        </div>
                    </div>
                </header>
                <div>
                    <!--   <section class="scrollable wrapper w-f" style="height: 400px"  >  -->

                @include('flash::message')
                <!--   <section class="panel panel-default" > -->

                    <!--  <div class="table-responsive" > -->


                    <table class="table table-hover table-condensed" id="tableData">
                        <thead>
                        <tr>
                            <th width="20%">Name</th>
                            <th width="15%">Role(s)</th>
                            <th width="15%">Email</th>
                            <th width="15%">Permission</th>
                            <th width="10%">Status</th>
                            @if($showPermission)
                                <th width="15%"></th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        {{ session()->get('success') }}
                        @if(count($clients) > 0)
                            @foreach($clients as $client)

                                <tr>
                                    <td>{{ $client->client->displayName()}}</td>
                                    <td>{{ getAllUserRoles($client->client_id) }}</td>
                                    <td>{{ $client->client->email }}</td>
                                    <td>{{ ($client->permission === 'view')? ucfirst('View') : 'View and Edit' }}</td>
                                    <td>
                                        @if($client->status === 0)
                                            <span class='label label-warning'>Pending</span>
                                        @elseif((in_array($client->status,[1,2,5])))
                                            <span class='label label-success'>Accepted</span>
                                        @elseif($client->status === 3)
                                            <span class='label label-danger'>Disconnected</span>
                                        @endif
                                    </td>
                                    @if($showPermission)
                                        <td>
                                            <div class="btn-group btn-group-xs">
                                                <div class="btn-group">
                                                    <button class="btn btn-default dropdown-toggle"
                                                            data-toggle="dropdown" style="z-index: 30"><i
                                                                class="fa fa-lock"></i> Change Permission <span
                                                                class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        @if($client->permission === 'view')
                                                            <li><a href="#" class="permission"
                                                                   data-value="{{ $client->id }}" data-action="2">Edit
                                                                    and View</a></li>
                                                        @else
                                                            <li><a href="#" class="permission"
                                                                   data-value="{{ $client->id }}"
                                                                   data-action="1">View</a></li>
                                                        @endif
                                                        <li><a href="#" class="permission"
                                                               data-value="{{ $client->id }}" data-action="3">Revoke
                                                                Access</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">You have not added any user.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <!--      </div> -->
                    <!--    </section> -->
                    <!--     </section>  -->
                </div>

                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $('.permission').on('click', function (e) {
            var baseurl = $('#baseurl').val();
            var client = $(this).data('value');
            var action = $(this).data('action');
            $.ajax({
                type: 'GET',
                url: baseurl + '/collaborator/change-permission?action=' + action + '&client=' + client,
                context: this,
                success: function () {
                    displayNotification('Operation was successful', 'Success!', 'success');
                    if (action === 3) {
                        $(this).fadeOut('slow', function () {
                            $(this).closest("tr").remove();
                        });
                    } else {
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                },
                error: function () {
                    displayNotification('Something went wrong', 'Error!', 'error');
                }
            });
        });


        $('#tableData td').css({"font-size": "15px", "padding-right": "50px"});
        $('#tableData thead').css('font-size', '15px');


    </script>
@stop
