<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


// Route::get('/', function () {
//     dd('This is the Collaborator module index page. Build something great!');
// });


Route::get('collaborators/list', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@index']);
Route::get('collaborators/list-payroll', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@payrollList']);
Route::get('collaborators/list-ims', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@imsList']);
Route::get('collaborators/businesses', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@businesses']);
Route::get('collaborators/add-new', ['middleware' => ['auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@create']);
Route::post('collaborators/add-new', ['middleware' => ['auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@store']);
Route::any('collaborator/change-permission', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@changePermission']);
Route::any('collaborator/change-status', ['uses' => 'CollaboratorsController@changeStatus'])->middleware('verify_license');
Route::any('collaborate/invite-response', ['uses' => 'CollaboratorsController@inviteResponse']);
Route::get('dashboard/change-company', ['middleware' => ['web', 'auth:web', 'verify_license'], 'uses' => 'CollaboratorsController@changeActiveCompany']);




