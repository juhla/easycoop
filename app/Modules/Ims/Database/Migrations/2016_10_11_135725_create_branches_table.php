<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ims_branches')) {
            Schema::create('ims_branches', function (Blueprint $table) {
                $table->uuid('id');
                $table->primary('id');
                $table->string('branch_name');
                $table->string('address');
                $table->string('city');
                $table->integer('country_id');
                $table->integer('state_id');
                $table->string('phone');
                $table->string('email');
                $table->enum('sync', array(0, 1))->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_branches');
    }
}
