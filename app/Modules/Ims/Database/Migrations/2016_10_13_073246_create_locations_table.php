<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_locations', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('branch_id', 36);
            $table->string('location_name');
            $table->string('description');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_locations');
    }
}
