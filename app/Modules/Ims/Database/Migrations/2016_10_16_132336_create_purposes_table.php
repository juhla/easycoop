<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurposesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_purposes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('purpose_name');
            $table->string('purpose_code');
            $table->char('location_id', 36);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_purposes');
    }
}
