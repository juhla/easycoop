<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTableIms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            Schema::create('ims_categories', function (Blueprint $table) {
                $table->uuid('id');
                $table->primary('id');
                $table->string('name');
                $table->string('code');
                $table->string('description');
                $table->char('parent', 36);
                $table->enum('sync', array(0, 1))->default(0);
                $table->timestamps();
            });
        }catch (Exception $exception){

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_categories');
    }
}
