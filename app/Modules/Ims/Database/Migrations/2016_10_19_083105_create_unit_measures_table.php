<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_unit_measures', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('unit_measurement_code');
            $table->string('unit_measurement_name');
            $table->string('description');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_unit_measures');
    }
}
