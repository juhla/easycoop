<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_taxes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('tax_name');
            $table->string('tax_code');
            $table->string('description');
            $table->string('tax_value');
            $table->enum('default_value', ['0', '1'])->default('0');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_taxes');
    }
}
