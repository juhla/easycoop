<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_employees', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('user_id');
            $table->integer('country_id');
            $table->char('branch_id', 100);
            $table->char('designation_id', 36);
            $table->char('department_id', 36);
            $table->string('address');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_employees');
    }
}
