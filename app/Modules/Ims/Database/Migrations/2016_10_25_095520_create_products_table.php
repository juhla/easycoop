<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_products', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('image');
            $table->string('code');
            $table->string('name');
            $table->string('display_name');
            $table->string('description');
            $table->string('size');
            $table->string('weight');
            $table->string('height');
            $table->string('width');
            $table->string('color');
            $table->string('manufacturer');
            $table->char('currency_id', 36);
            $table->char('purpose_id', 36);
            $table->char('category_id', 36);
            $table->char('sub_category_id', 36);
            $table->char('unit_of_measure_id', 36);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_products');
    }
}
