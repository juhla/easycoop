<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_discounts', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('discount_name');
            $table->string('discount_code');
            $table->string('description');
            $table->string('discount_value');
            $table->enum('default_value', ['0', '1'])->default('0');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_discounts');
    }
}
