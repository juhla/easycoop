<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_product_stocks', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('supplier_id', 36);
            $table->char('product_id', 36);
            $table->integer('quantity');
            $table->string('cost');
            $table->string('price');
            $table->char('tax_id', 36);
            $table->string('production_date');
            $table->string('best_before');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_product_stocks');
    }
}
