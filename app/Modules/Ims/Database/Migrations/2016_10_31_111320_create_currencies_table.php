<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_currencies', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('country_name');
            $table->integer('country_id');
            $table->string('currency_name');
            $table->string('currency_code');
            $table->string('currency_symbol');
            $table->string('decimal_symbol');
            $table->string('currency_digits');
            $table->string('thousand_symbol');
            $table->string('description');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_currencies');
    }
}
