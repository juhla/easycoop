<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStockLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_product_stock_locations', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('location_id', 36);
            $table->char('product_id', 36);
            $table->integer('location_qty');
            $table->integer('ideal_qty');
            $table->integer('min_qty');
            $table->integer('max_qty');
            $table->integer('warning_qty');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_product_stock_locations');
    }
}
