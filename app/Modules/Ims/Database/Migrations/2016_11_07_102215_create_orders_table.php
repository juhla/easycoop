<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_orders', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('supplier_id', 36);
            $table->string('order_number');
            $table->string('issue_date');
            $table->string('receipt_date');
            $table->char('branch_id', 36);
            $table->char('location_id', 36);
            $table->text('bill_address');
            $table->text('ship_address');
            $table->text('order_note');
            $table->text('internal_notes');
            $table->integer('order_by');
            $table->char('department_id', 36);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_orders');
    }
}
