<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_order_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('order_id', 36);
            $table->char('product_id', 36);
            $table->string('tax_value');
            $table->string('total');
            $table->string('price');
            $table->integer('quantity');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_order_items');
    }
}
