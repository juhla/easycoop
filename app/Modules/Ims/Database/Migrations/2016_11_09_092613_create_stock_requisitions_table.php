<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_stock_requisitions', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('order_number');
            $table->string('issue_date');
            $table->string('receipt_date');
            $table->char('source_branch', 36);
            $table->char('source_location', 36);
            $table->char('destination_branch', 36);
            $table->char('destination_location', 36);
            $table->string('notes');
            $table->string('ordered_by');
            $table->char('ordered_by_dept', 36);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_stock_requisitions');
    }
}
