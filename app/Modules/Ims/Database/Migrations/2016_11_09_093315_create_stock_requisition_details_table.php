<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockRequisitionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_stock_requisition_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('stock_requisition_id', 36);
            $table->char('product_id', 36);
            $table->enum('item_to_be_returned', ['1', '0'])->default('0');
            $table->string('item_return_date');
            $table->integer('quantity');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_stock_requisition_details');
    }
}
