<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_order_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('order_id', 36);
            $table->string('discount');
            $table->string('payment_type');
            $table->string('payment_status');
            $table->string('total_amount');
            $table->string('grand_total');
            $table->string('status');
            $table->integer('ledger_id');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_order_details');
    }
}
