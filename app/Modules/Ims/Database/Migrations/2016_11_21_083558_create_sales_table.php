<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_sales', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('customer_id', 36);
            $table->char('department_id', 36);
            $table->char('branch_id', 36);
            $table->char('location_id', 36);
            $table->string('sales_date');
            $table->char('transaction_id', 36);
            $table->string('sales_time');
            $table->string('bill_address');
            $table->string('ship_address');
            $table->string('sales_notes');
            $table->string('internal_notes');
            $table->string('order_by');
            $table->string('bill_no');
            $table->string('bill_type');
            $table->enum('is_tax_calculated', ['0', '1']);
            $table->enum('isReversed', ['0', '1']);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_sales');
    }
}
