<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_sales_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('sales_id', 36);
            $table->char('product_id', 36);
            $table->integer('quantity');
            $table->string('line_total');
            $table->string('discount');
            $table->string('tax');
            $table->string('price');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_sales_items');
    }
}
