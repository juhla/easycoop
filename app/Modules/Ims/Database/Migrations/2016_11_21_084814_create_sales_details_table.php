<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_sales_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('sales_id', 36);
            $table->string('vat');
            $table->string('paid_amount');
            $table->string('balance');
            $table->string('change');
            $table->string('total_discount');
            $table->string('total_amount');
            $table->string('grand_total');
            $table->string('payment_type');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_sales_details');
    }
}
