<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_issue_orders', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('issue_stock_number');
            $table->string('issue_date');
            $table->char('source_branch', 36);
            $table->char('source_location', 36);
            $table->char('destination_location', 36);
            $table->char('destination_branch', 36);
            $table->string('notes');
            $table->enum('isRejected', ['0', '1']);
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_issue_orders');
    }
}
