<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ims_issue_order_details', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('issue_stock_id', 36);
            $table->char('product_id', 36);
            $table->integer('quantity');
            $table->enum('item_to_be_returned', ['1', '0'])->default('0');
            $table->string('item_return_date');
            $table->enum('sync', array(0, 1))->default(0);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_issue_order_details');
    }
}
