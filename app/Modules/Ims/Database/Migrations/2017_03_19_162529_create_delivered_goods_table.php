<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveredGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ims_delivered_goods')) {
            Schema::create('ims_delivered_goods', function (Blueprint $table) {
                $table->uuid('id');
                $table->primary('id');
                $table->char('product_id', 36);
                $table->integer('quantity');
                $table->string('price');
                $table->string('datetime');
                $table->char('order_id', 36);
                $table->enum('sync', array(0, 1))->default(0);
                $table->char('product_stock_location_id', 36);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_delivered_goods');
    }
}
