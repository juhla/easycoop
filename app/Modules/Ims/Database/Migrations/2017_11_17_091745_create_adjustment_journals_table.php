<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjustmentJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //check if table exist first
        if (!Schema::hasTable('ims_adjustment_journals')) {
            Schema::create('ims_adjustment_journals', function (Blueprint $table) {
                $table->uuid('id');
                $table->primary('id');
                $table->char('product_id', 36);
                $table->integer('quantity');
                $table->string('price');
                $table->string('total');
                $table->string('date_created');
                $table->string('description');
                $table->integer('transaction_id');
                $table->char('location_id', 36);
                $table->enum('sync', array(0, 1))->default(0);
                $table->enum('adjustment_type', array("inc", "dec"))->default("dec");

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ims_adjustment_journals');
    }
}
