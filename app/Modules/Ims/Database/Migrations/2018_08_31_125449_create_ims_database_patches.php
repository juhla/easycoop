<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateImsDatabasePatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        try {
            //patch ca_customers
            if (!Schema::hasColumn('ca_customers', 'sync')) {
                Schema::table('ca_customers', function (Blueprint $table) {
                    $table->enum('sync', array(0, 1))->default(0);
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            //patch ca_vendors
            if (!Schema::hasColumn('ca_vendors', 'sync')) {
                Schema::table('ca_vendors', function (Blueprint $table) {
                    $table->enum('sync', array(0, 1))->default(0);
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('ims_adjustment_journals')) {
                Schema::create('ims_adjustment_journals', function (Blueprint $table) {
                    $table->uuid('id');
                    $table->primary('id');
                    $table->char('product_id', 36);
                    $table->integer('quantity');
                    $table->string('price');
                    $table->string('total');
                    $table->string('date_created');
                    $table->string('description');
                    $table->integer('transaction_id');
                    $table->char('location_id', 36);
                    $table->enum('sync', array(0, 1))->default(0);
                    $table->enum('adjustment_type', array("inc", "dec"))->default("dec");
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('ims_delivered_goods')) {
                //patch ims_delivered_goods
                if (!Schema::hasColumn('ims_delivered_goods', 'product_stock_location_id')) {
                    Schema::table('ims_delivered_goods', function (Blueprint $table) {
                        $table->char('product_stock_location_id', 36);
                    });
                }

            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasTable('ims_sales')) {
                //patch ims_sales
                if (!Schema::hasColumn('ims_sales', 'transaction_id')) {
                    Schema::table('ims_sales', function (Blueprint $table) {
                        $table->integer('transaction_id', 11);
                    });
                }
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     */

    public function down()
    {
        //rollback ca_customers
        if (Schema::hasColumn('ca_customers', 'sync')) {
            Schema::table('ca_customers', function (Blueprint $table) {
                $table->dropColumn('sync');
            });
        }

        //rollback ca_vendors
        if (Schema::hasColumn('ca_vendors', 'sync')) {
            Schema::table('ca_vendors', function (Blueprint $table) {
                $table->dropColumn('sync');
            });
        }

        //rollback ims_adjustment_journals
        Schema::dropIfExists('ims_adjustment_journals');

        //rollbback ims_delivered_goods
        if (Schema::hasColumn('ims_delivered_goods', 'product_stock_location_id')) {
            Schema::table('ims_delivered_goods', function (Blueprint $table) {
                $table->dropColumn('product_stock_location_id');
            });
        }

        //rollback ims_sales
        if (Schema::hasColumn('ims_sales', 'transaction_id')) {
            Schema::table('ims_sales', function (Blueprint $table) {
                $table->dropColumn('transaction_id');
            });
        }

    }
}
