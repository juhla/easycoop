<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class EditPo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        try {
            if (Schema::hasTable('ims_order_details')) {

                if (!Schema::hasColumn('ims_order_details', 'paid_amount')) {
                    Schema::table('ims_order_details', function (Blueprint $table) {
                        $table->string('paid_amount')->nullable();
                    });
                }
                if (!Schema::hasColumn('ims_order_details', 'balance')) {
                    Schema::table('ims_order_details', function (Blueprint $table) {
                        $table->string('balance')->nullable();
                    });
                }
                if (!Schema::hasColumn('ims_order_details', 'change')) {
                    Schema::table('ims_order_details', function (Blueprint $table) {
                        $table->string('change')->nullable();
                    });
                }

            }

            if (Schema::hasTable('ims_order_items')) {
                if (!Schema::hasColumn('ims_order_items', 'tax_value')) {
                    Schema::table('ims_order_items', function (Blueprint $table) {
                        $table->string('tax_value')->nullable()->change();
                    });
                }
            }


        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
