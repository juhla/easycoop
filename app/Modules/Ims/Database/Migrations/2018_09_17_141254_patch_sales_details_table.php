<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PatchSalesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('ims_sales_details')) {
                if (!Schema::hasColumn('ims_sales_details', 'cost_price')) {
                    Schema::table('ims_sales_details', function (Blueprint $table) {
                        $table->float('cost_price');
                    });
                }
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            //patch ca_customers
            if (Schema::hasColumn('ims_sales_details', 'cost_price')) {
                Schema::table('ims_sales_details', function (Blueprint $table) {
                    $table->dropColumn('cost_price');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }
}
