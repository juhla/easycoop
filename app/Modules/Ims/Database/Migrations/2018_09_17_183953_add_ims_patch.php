<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddImsPatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        try {

            //check if the order_id columns exist first before running the patch migration
            if (Schema::hasColumn('ims_delivered_goods', 'order_id')) {
                Schema::table('ims_delivered_goods', function ($table) {
                    $table->string('order_id')->nullable()->change();
                });
            } else {
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('order_id')->nullable()->after('datetime');
                });
            }

            //check if the ims_delivered_goods columns exist first before running the patch migration
            if (Schema::hasColumn('ims_delivered_goods', 'product_stock_location_id')) {
                Schema::table('ims_delivered_goods', function ($table) {
                    $table->string('product_stock_location_id')->nullable()->change();
                });
            } else {
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('product_stock_location_id')->nullable()->after('sync');
                });
            }

        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

        try {
            Schema::table('ims_products', function ($table) {
                $table->string('image')->nullable()->change();
            });
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

        try {
            Schema::table('ims_branches', function ($table) {
                $table->integer('state_id')->nullable()->change();
            });
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

        try {
            Schema::table('ims_currencies', function ($table) {
                $table->string('country_name')->nullable()->change();
            });
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }

        try {
            if (!Schema::hasTable('ims_delivered_goods')) {
                Schema::create('ims_delivered_goods', function ($table) {
                    $table->uuid('id');
                    $table->primary('id');
                    $table->char('product_id', 36);
                    $table->integer('quantity');
                    $table->double('price');
                    $table->string('datetime');
                    $table->enum('sync', array(0, 1))->default(0);
                    $table->char('product_stock_location_id', 36);
                    $table->timestamps();
                });
            }
        } catch (Exception $exception) {
            app('sentry')->captureException($exception);
        }

//        try {
//            if (!Schema::hasTable('outlet_receives')) {
//                Schema::table('outlet_receives', function (Blueprint $table) {
//                    $table->string('waybill_no')->nullable();
//                    $table->string('number')->change();
//                    $table->string('total_amount');
//                    $table->string('grand_total');
//                    $table->string('discount');
//                });
//            }
//        } catch (Exception $exception) {
//            app('sentry')->captureException($exception);
//        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
