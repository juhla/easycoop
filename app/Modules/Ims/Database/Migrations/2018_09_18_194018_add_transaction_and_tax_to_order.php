<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionAndTaxToOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_order_details', 'transaction_id')) {
                Schema::table('ims_order_details', function (Blueprint $table) {
                    $table->string('transaction_id')->nullable();
                });
            }
            if (!Schema::hasColumn('ims_order_details', 'is_tax_calculated')) {
                Schema::table('ims_order_details', function (Blueprint $table) {
                    $table->tinyInteger('is_tax_calculated')->nullable();
                });
            }
        } catch (Exception $exception) {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
