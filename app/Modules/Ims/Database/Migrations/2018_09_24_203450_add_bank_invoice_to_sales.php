<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankInvoiceToSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_sales', 'bank_invoice')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('bank_invoice', '100')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ims_sales', function (Blueprint $table) {
            $table->dropColumn('bank_invoice');
        });
    }
}
