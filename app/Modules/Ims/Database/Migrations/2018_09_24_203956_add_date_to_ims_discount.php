<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateToImsDiscount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_discounts', 'start_date')) {
                Schema::table('ims_discounts', function (Blueprint $table) {
                    $table->string('start_date', '15')->nullable();
                    $table->string('end_date', '15')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (!Schema::hasColumn('ims_product_stocks', 'discount_id')) {
                Schema::table('ims_product_stocks', function (Blueprint $table) {
                    $table->string('discount_id', '36')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ims_discounts', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });

        Schema::table('ims_product_stocks', function (Blueprint $table) {
            $table->dropColumn('discount_id');
        });
    }
}
