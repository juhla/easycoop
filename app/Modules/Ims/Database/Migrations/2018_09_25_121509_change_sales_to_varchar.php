<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class ChangeSalesToVarchar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            if (Schema::hasColumn('ims_sales_details', 'cost_price')) {
                Schema::table('ims_sales_details', function (Blueprint $table) {
                    $table->string('total_amount')->nullable()->change();
                    $table->string('grand_total')->nullable()->change();
                    $table->string('cost_price')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ims_sales_details', function (Blueprint $table) {
            $table->double('total_amount')->change();
            $table->double('grand_total')->change();
            $table->double('cost_price')->change();

        });
    }
}
