<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullifySomeImsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            if (Schema::hasTable('ims_sales')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('bill_address')->nullable()->change();
                    $table->string('ship_address')->nullable()->change();
                    $table->string('sales_notes')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
