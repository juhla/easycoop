<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVariantToIms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('ims_variants')) {
                Schema::create('ims_variants', function (Blueprint $table) {
                    $table->integer('product_id')->nullable();
                    $table->string('price', '100')->nullable();
                    $table->string('name', '100')->nullable();
                    $table->integer('units')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ims_variants');
    }
}
