<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdsToVariants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_variants', 'id')) {
                Schema::table('ims_variants', function (Blueprint $table) {
                    $table->increments('id');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (!Schema::hasColumn('ims_variants', 'created_at')) {
                Schema::table('ims_variants', function (Blueprint $table) {
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
