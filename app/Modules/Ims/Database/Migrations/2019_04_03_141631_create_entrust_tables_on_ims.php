<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntrustTablesOnIms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::connection('tenant_conn')->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        try {
            if (!Schema::hasTable('permissions')) {
                Schema::create('permissions', function (Blueprint $table) {
                    $table->integer('id');
                    $table->string('category');
                    $table->string('name');
                    $table->string('display_name');

                    $table->timestamps();
                });
            }

            if (!Schema::hasTable('roles')) {
                Schema::create('roles', function (Blueprint $table) {
                    $table->integer('id');
                    $table->string('category');
                    $table->string('name');
                    $table->string('display_name');
                    $table->boolean('sync')->nullable();

                    $table->timestamps();
                });
            }

            if (!Schema::hasTable('role_user')) {
                Schema::create('role_user', function (Blueprint $table) {
                    $table->integer('id');
                    $table->integer('user_id');
                    $table->integer('role_id');

                    $table->timestamps();
                });
            }

            if (!Schema::hasTable('permission_role')) {
                Schema::create('permission_role', function (Blueprint $table) {
                    $table->integer('id');
                    $table->integer('permission_id');
                    $table->integer('role_id');

                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
