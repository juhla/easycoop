<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class EntrustMigrationPatchFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            if (Schema::hasTable('permissions')) {
                Schema::table('permissions', function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }
            if (Schema::hasTable('roles')) {
                Schema::table('roles', function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }
            if (Schema::hasTable('role_user')) {
                Schema::table('role_user', function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }
            if (Schema::hasTable('permission_role')) {
                Schema::table('permission_role', function (Blueprint $table) {
                    $table->increments('id')->change();
                });
            }

            // $count = DB::table('permissions')->get();
            // if(count($count) == 0){
            Artisan::call('db:seed', [
                '--class' => 'PermissionsTableSeeder',
                '--force' => true
            ]);
            // } 
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
