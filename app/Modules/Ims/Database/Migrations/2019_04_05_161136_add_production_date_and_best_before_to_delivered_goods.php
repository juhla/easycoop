<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductionDateAndBestBeforeToDeliveredGoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            if (Schema::hasTable('ims_delivered_goods')) {
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('production_date')->nullable();
                    $table->string('best_before')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
