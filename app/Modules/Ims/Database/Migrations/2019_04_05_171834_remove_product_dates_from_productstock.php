<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveProductDatesFromProductstock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ims_product_stocks', function (Blueprint $table) {
            $table->dropColumn('production_date');
            $table->dropColumn('best_before');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ims_product_stocks', function (Blueprint $table) {
            $table->text('production_date');
            $table->text('best_before');
        });
    }
}
