<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductionDateAndBestBeforeToOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            if (Schema::hasTable('ims_order_items')) {
                Schema::table('ims_order_items', function (Blueprint $table) {
                    $table->string('production_date')->nullable();
                    $table->string('best_before')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
