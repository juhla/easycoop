<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FixEnumTablesFromIms2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {

            if (Schema::hasTable('ims_branches')) {
                DB::statement('ALTER TABLE ims_branches MODIFY sync varchar(191)');
                Schema::table('ims_branches', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_orders')) {
                DB::statement('ALTER TABLE ims_orders MODIFY sync varchar(191)');
                Schema::table('ims_orders', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_order_items')) {
                DB::statement('ALTER TABLE ims_order_items MODIFY sync varchar(191)');
                Schema::table('ims_order_items', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_sales')) {
                DB::statement('ALTER TABLE ims_sales MODIFY sync varchar(191)');
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
            if (Schema::hasTable('ims_sales_items')) {
                DB::statement('ALTER TABLE ims_sales_items MODIFY sync varchar(191)');
                Schema::table('ims_sales_items', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_categories')) {
                DB::statement('ALTER TABLE ims_categories MODIFY sync varchar(191)');
                Schema::table('ims_categories', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_currencies')) {
                DB::statement('ALTER TABLE ims_currencies MODIFY sync varchar(191)');
                Schema::table('ims_currencies', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }

            if (Schema::hasTable('ims_delivered_goods')) {
                DB::statement('ALTER TABLE ims_delivered_goods MODIFY sync varchar(191)');
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
