<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixImsDiscountEnumField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('ims_discounts')) {
                DB::statement('ALTER TABLE ims_discounts MODIFY sync varchar(191)');
                DB::statement('ALTER TABLE ims_discounts MODIFY default_value varchar(191)');
                Schema::table('ims_discounts', function (Blueprint $table) {
                    $table->string('sync')->default(0)->change();
                    $table->string('default_value')->default(0)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureEception($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
