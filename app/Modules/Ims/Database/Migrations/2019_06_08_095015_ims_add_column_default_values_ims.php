<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImsAddColumnDefaultValuesIms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasColumn('ims_adjustment_journals', 'sync')) {
                Schema::table('ims_adjustment_journals', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_adjustment_journals', 'adjustment_type')) {
                Schema::table('ims_adjustment_journals', function (Blueprint $table) {
                    $table->string('adjustment_type')->default('dec')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_branches', 'sync')) {
                Schema::table('ims_branches', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_categories', 'sync')) {
                Schema::table('ims_categories', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_currencies', 'sync')) {
                Schema::table('ims_currencies', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_customers', 'sync')) {
                Schema::table('ims_customers', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_delivered_goods', 'sync')) {
                Schema::table('ims_delivered_goods', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_departments', 'sync')) {
                Schema::table('ims_departments', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_designations', 'sync')) {
                Schema::table('ims_designations', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_discounts', 'default_value')) {
                Schema::table('ims_discounts', function (Blueprint $table) {
                    $table->string('default_value')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_discounts', 'sync')) {
                Schema::table('ims_discounts', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_employees', 'sync')) {
                Schema::table('ims_employees', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_branches', 'sync')) {
                Schema::table('ims_branches', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_issue_orders', 'sync')) {
                Schema::table('ims_issue_orders', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_issue_order_details', 'item_to_be_returned')) {
                Schema::table('ims_issue_order_details', function (Blueprint $table) {
                    $table->string('item_to_be_returned')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_issue_order_details', 'sync')) {
                Schema::table('ims_issue_order_details', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_locations', 'sync')) {
                Schema::table('ims_locations', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_orders', 'sync')) {
                Schema::table('ims_orders', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_order_details', 'sync')) {
                Schema::table('ims_order_details', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_order_items', 'sync')) {
                Schema::table('ims_order_items', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_products', 'sync')) {
                Schema::table('ims_products', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_product_stocks', 'sync')) {
                Schema::table('ims_product_stocks', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_product_stock_locations', 'sync')) {
                Schema::table('ims_product_stock_locations', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_purposes', 'sync')) {
                Schema::table('ims_purposes', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_sales', 'is_tax_calculated')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('is_tax_calculated')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_sales', 'is_Reversed')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('is_Reversed')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_sales', 'sync')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_sales_details', 'sync')) {
                Schema::table('ims_sales_details', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_sales_items', 'sync')) {
                Schema::table('ims_sales_items', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_stock_requisitions', 'sync')) {
                Schema::table('ims_stock_requisitions', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_stock_requisition_details', 'item_to_be_returned')) {
                Schema::table('ims_stock_requisition_details', function (Blueprint $table) {
                    $table->string('item_to_be_returned')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_stock_requisition_details', 'sync')) {
                Schema::table('ims_stock_requisition_details', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_suppliers', 'sync')) {
                Schema::table('ims_suppliers', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_taxes', 'default_value')) {
                Schema::table('ims_taxes', function (Blueprint $table) {
                    $table->string('default_value')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_taxes', 'sync')) {
                Schema::table('ims_taxes', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_unit_measures', 'sync')) {
                Schema::table('ims_unit_measures', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('ims_variants', 'sync')) {
                Schema::table('ims_variants', function (Blueprint $table) {
                    $table->string('sync')->default('0')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
