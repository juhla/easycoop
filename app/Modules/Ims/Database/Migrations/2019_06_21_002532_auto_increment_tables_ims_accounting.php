<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class AutoIncrementTablesImsAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    private $DB;

    public function trimEnum($subject)
    {
        $search = 'enum(';
        $trimmed = str_replace($search, '', $subject);
        $search = ')';
        $trimmed = str_replace($search, '', $trimmed);
        $explode = explode(",", $trimmed);
        $value = $explode['0'];
        return $value;
    }

    public function up()
    {

        $DB = DB::connection('tenant_conn')->getDatabaseName();
        $this->setDB($DB);
        Log::debug("DB :: " . $DB);
        $nonColumns = ['permission_role', 'role_user'];
        $tables = DB::getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $table) {
            if (!in_array($table, $nonColumns)) {
                try {
                    $columns = $this->getTableColumns($table);
                    foreach ($columns as $eachColumns) {
                        $res = $this->getNullableColumns($eachColumns, $table);
                        switch ($res->DATA_TYPE) {
                            case 'enum':
                                {
                                    Log::debug('DB :: ' . $DB . " table:: " . $table . " column:: " . $eachColumns);
                                    $tail = "";
                                    if (!empty($res->COLUMN_DEFAULT)) {
                                        $tail = "default '$res->COLUMN_DEFAULT'";
                                    }
                                    DB::statement(" ALTER TABLE  $table  MODIFY  $eachColumns varchar(191) NULL $tail");
                                }
                                break;
                        }
                    }
                    //$isAutoColumn = $this->getAutoIncrementColumns('id', $table);
                    $hasColumn = Schema::hasColumn($table, 'id');
                    if ($hasColumn) {
                        $col = DB::getSchemaBuilder()->getColumnType($table, 'id');
                        $isColumnType = ($col == 'integer') ? true : false;
                        if ($isColumnType) {
                            Schema::table($table, function (Blueprint $table) {
                                $table->bigIncrements('id')->change();
                            });
                        }
                    }
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                }
            }

        }
    }

    public function getTableColumns($table)
    {
        return Schema::getColumnListing($table);
    }

    public function getNullableColumns($columnName, $model)
    {
        $DB = $this->getDB();
        try {
            $table = $model;
            $results = DB::select("SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE
                    FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '$DB' AND TABLE_NAME = '$table' AND COLUMN_NAME = '$columnName'   ");

            return @$results[0];
        } catch (\Exception $exception) {
            return null;
        }

    }

    /**
     * @return mixed
     */
    public function getDB()
    {
        return $this->DB;
    }

    /**
     * @param mixed $DB
     */
    public function setDB($DB)
    {
        $this->DB = $DB;
    }

    public function getAutoIncrementColumns($columnName, $model)
    {
        $DB = $this->getDB();
        try {
            $table = $model;
            $results = DB::select("SELECT *
                    FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '$DB' 
                AND TABLE_NAME = '$table' 
                AND DATA_TYPE = 'int' 
                AND COLUMN_DEFAULT IS NULL
                AND IS_NULLABLE = 'NO'
                AND EXTRA like '%auto_increment%'
                AND COLUMN_NAME = '$columnName'   ");
            return !empty(@$results[0]);
        } catch (\Exception $exception) {
            return false;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
