<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditItemReturnDateImsIssueOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('ims_issue_order_details', function (Blueprint $table) {
                $table->string('item_return_date')->nullable()->change();
            });
            Schema::table('ims_adjustment_journals', function (Blueprint $table) {
                $table->string('transaction_id')->nullable()->change();
            });
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
