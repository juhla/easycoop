<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToTheSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_sales', 'recalculated')) {
                Schema::table('ims_sales', function (Blueprint $table) {
                    $table->string('recalculated', '100')->nullable()->default('0');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

}
