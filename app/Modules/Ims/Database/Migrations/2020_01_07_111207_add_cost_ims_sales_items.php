<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostImsSalesItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('ims_sales_items', 'cost')) {
                Schema::table('ims_sales_items', function (Blueprint $table) {
                    $table->string('cost', '100')->nullable()->default('0');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

}
