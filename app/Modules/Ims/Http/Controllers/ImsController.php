<?php

namespace App\Modules\Ims\Http\Controllers;

use Auth;
use App\Modules\Base\Http\Controllers\Controller;
use App\ImsSetup;
use App\Modules\Base\Models\ImsLicenceSubscriber;
use App\Modules\Base\Traits\License;
use App\Modules\Base\Traits\NewCreates;
use Redirect;
use Session;

class ImsController extends Controller
{

    use License, NewCreates;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function setup()
    {
        //dd('k');
        if (config("app.env") == "production") {
            try {
                exec('chmod 777 storage/ -R');
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
        }
        // ** checking if the business owner has clicked IMS
        $businessOwnerId = getBusinessOwnerID(); //18
        $user = auth()->user();
        //checking the record that would have been created if the business owner has clicked IMS
        $licenceSubscriber = ImsLicenceSubscriber::where('user_id', $businessOwnerId)->get();
        $businessOwnerRole = $user->hasRole(['business-owner', 'advanced-business-owner', 'basic-business-owner']);
        if (!$businessOwnerRole && $licenceSubscriber->isEmpty()) {
            return back()->withErrors('status', 'Please call your business owner to activate IMS for you');
        }
        // ** end checking if the business owner has clicked IMS

        $this->hasAccess(['ims']);
        if (!$this->isUserImsActivated()) {
            flash()->error($this->licenseError());
            return redirect('membership/plans');
        }

        $ims_url = config('constants.ims_url');
        $ims = $ims_url . "secure/sign-in?token=" . session()->get('auth.token');
          //dd($ims);
        event(new \App\Modules\Ims\Events\ImsSetup());
        // dd('dd');
        return Redirect::to($ims);
    }

    public function login()
    {

        //dd('k');
        if (config("app.env") == "production") {
            try {
                exec('chmod 777 storage/ -R');
            } catch (\Exception $exception) {
                app('sentry')->captureException($exception);
            }
        }
        $this->hasAccess(['ims']);
        //used only by ims employee
        $ims_url = config('constants.ims_url');
        $ims = $ims_url . "secure/sign-in?token=" . session()->get('auth.token');
        event(new \App\Modules\Ims\Events\ImsSetup());
        //check if user has an active licence
        if (!$this->isUserImsActivated()) {
            //no valid or have expired license
            flash()->error($this->licenseError());
            return redirect('secure/sign-in');
        }
        //dd($ims);
        return Redirect::to($ims);
    }
}
