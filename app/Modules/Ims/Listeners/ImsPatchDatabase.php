<?php

namespace App\Modules\Ims\Listeners;

use App\Modules\Base\Models\Company;
use App\Modules\Base\Traits\AfterLogin;
use DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;

class ImsPatchDatabase
{
    use AfterLogin;

    /**
     * Create the event listener.
     *
     * @return void
     */

    public function handle($event)
    {
        try {

            $company = Company::whereUser_id(getBusinessOwnerID())->get()->first();
            $database = $company->database;
            $username = Config::get('database.connections.mysql.username');
            $password = Config::get('database.connections.mysql.password');
            $host = Config::get('database.connections.mysql.host');
            $this->connectToDatabase($host, $username, $password, $database);
            $this->runPatchMigrations();
        } catch (\Exception $exception) {
            // dd($exception->getMessage());
            app('sentry')->captureException($exception);
        }
    }

    private function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);

        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function runPatchMigrations()
    {
        try {
            Artisan::call('migrate', [
                '--database' => 'tenant_conn',
                '--path' => 'database/migrations/patch-ims',
                '--force' => true,
            ]);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

}
