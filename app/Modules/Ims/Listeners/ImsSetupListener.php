<?php

namespace App\Modules\Ims\Listeners;

use App\Modules\Ims\Events\ImsSetup;
use App\Modules\Base\Traits\AfterLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use App\Modules\Base\Models\Company;
use DB;

class ImsSetupListener
{
    use AfterLogin;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ImsSetup $event
     * @return void
     */
    public function handle(ImsSetup $event)
    {
        $company = Company::whereUser_id(getBusinessOwnerID())->get()->first();
        $database = $company->database;
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');
        $host = Config::get('database.connections.mysql.host');
        $this->connectToDatabase($host, $username, $password, $database);
        $this->runPatchMigrations();
    }

    private function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);

        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function runPatchMigrations()
    {
        try {
            Artisan::call('module:migrate', [
                'slug' => 'ims',
                '--database' => 'tenant_conn',
                '--force' => true
            ]);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


    }
}
