<?php

namespace App\Modules\Ims\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadTranslationsFrom(module_path('ims', 'Resources/Lang', 'app'), 'ims');
        $this->loadViewsFrom(module_path('ims', 'Resources/Views', 'app'), 'ims');
        $this->loadMigrationsFrom(module_path('ims', 'Database/Migrations', 'app'), 'ims');
        //$this->loadConfigsFrom(module_path('ims', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('ims', 'Database/Factories', 'app'));

    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
    }
}
