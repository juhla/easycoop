<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'ims', 'middleware' => ['web', 'verify_license']], function () {
    Route::get('/create', ['uses' => 'ImsController@setup']);
    Route::get('/login', ['uses' => 'ImsController@login']);

});
