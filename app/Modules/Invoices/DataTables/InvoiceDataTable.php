<?php

namespace App\Modules\Invoices\DataTables;

use App\Modules\Base\Models\FiscalYear;
use App\Modules\Invoices\Models\Invoice;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable
            ->addColumn('customer', function ($__res) {
                return $__res->customer->name;
            })
            ->editColumn('amount_due', function ($__res) {
                return formatNumber($__res->amount_due);
            })
            ->editColumn('total_amount', function ($__res) {
                return formatNumber($__res->total_amount);
            })
            ->filterColumn('customer', function ($query, $keyword) {
                $query->whereHas('customer', function ($query) use ($keyword) {
                    $sql = "name  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->filterColumn('number', function ($query, $keyword) {
                $query->whereHas('invoice', function ($query) use ($keyword) {
                    $sql = "invoice_no  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->filterColumn('amount', function ($query, $keyword) {
                $query->whereHas('ca_invoice', function ($query) use ($keyword) {
                    $sql = "amount_due  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->editColumn('date', function ($__res) {
                return date('d/m/Y', strtotime($__res->invoice_date));
            })
            ->orderColumn('date', 'ca_invoice.due_date $1')
            ->addColumn('action', function ($invoice) {
                return view('invoice::actions.datatables_actions', compact('invoice'))->render();
            })
            ->rawColumns(['action']);

        return $dataTable;

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Invoice $model)
    {

        $type = $this->getType();
        $start_date = session('start_date');
        $end_date = session('end_date');
        $query = $model->with(['customer'])->where('invoice_type', 'sales')->newQuery();
        if (!empty($start_date)) {
            $start = Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            $query->whereDate('ca_invoice.invoice_date', '>=', $start);
        }
        if (!empty($end_date)) {
            $end = Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');
            $query->whereDate('ca_invoice.invoice_date', '<=', $end);
        }

        if (empty($start_date) && empty($end_date)) {

            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $query->whereBetween('ca_invoice.invoice_date', [$fiscalYear->begins, $fiscalYear->ends]);
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('invoiceDT')
            ->columns($this->getColumns())
            ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['title' => 'Date', 'data' => 'date', 'footer' => 'date', 'searchable' => false],
            ['title' => 'Number', 'data' => 'invoice_no', 'footer' => 'number'],
            ['title' => 'Customer', 'data' => 'customer', 'footer' => 'customer'],
            ['title' => 'Amount', 'data' => 'amount_due', 'footer' => 'amount'],
            ['title' => 'Total', 'data' => 'total_amount', 'footer' => 'total'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'invoice_' . date('YmdHis');
    }
}
