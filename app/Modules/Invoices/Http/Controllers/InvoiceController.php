<?php namespace App\Modules\Invoices\Http\Controllers;

use App\Modules\Invoices\DataTables\InvoiceDataTable;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Invoices\Models\BankAC;
use App\Modules\Invoices\Models\Customer;
use App\Modules\Invoices\Models\Ledger;
use App\Modules\Invoices\Models\Transaction;
use App\Modules\Invoices\Models\TransactionItem;
use App\Modules\Invoices\Models\Vendor;
use App\Modules\Invoices\Models\Invoice;
use App\Modules\Invoices\Models\InvoiceItem;
use App\Modules\NotebookAdv\Models\FiscalYear;
use App\Modules\Base\Traits\NewCreates;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Session;
use Mail;
use PDF;
use Redirect;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    use NewCreates;

    public function __construct()
    {
        $this->middleware(['tenant', 'fiscal_year']);
    }

    /**
     * @param InvoiceDataTable $table
     * @return mixed
     */
    public function salesInvoice(InvoiceDataTable $table)
    {
        $inputs = request()->all();
        $this->hasAccess(['view_invoice']);

        $title = 'Sales Invoice';
        $type = 'invoice';

        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');
        return $table->render('invoices::sales-invoice', compact('type', 'title', 'start_date', 'end_date'));

    }

    public function download(Request $request)
    {
        $id = $request->invoice_id;

        //find the invoice
        $invoice = Invoice::where('id', $id)->first();
        $invoiceDet = InvoiceItem::where('invoice_id', $id)->get();
        $customerIdentity = Customer::where('id', $invoice->customer_id)->first();

        if (!$invoice) {
            return Redirect::back();
        }
        //get the bank account
        @$bankName = Ledger::where('id', $invoice->bank_invoice)->first();
        if (!empty($bankName)) {
            @$accountNumber = BankAC::where('ledger_code', $bankName->code)->first();
        }
        $data['title'] = 'Invoice ' . $invoice->invoice_no;
        $data['invoice'] = $invoice;
        $data['invoiceDet'] = $invoiceDet;
        $data['customerIdentity'] = $customerIdentity;
        $data['bankName'] = $bankName;
        $data['accountNumber'] = $accountNumber;


        $theName = GenerateRandomString("10", "ALPHA");
        $theInvoice = public_path() . DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR;
        $thePdfFile = $theInvoice . $theName . '.pdf';
        try {
            $pdf = PDF::loadView('pdfgenerate.pdfgenerate', $data);
            return $pdf->download($customerIdentity->first_name . '_' . $customerIdentity->last_name . '_' . 'invoice.pdf');
        } catch (\Exception $e) {
            flash()->error('Cannot download pdf at this moment!');
            return redirect()->back();
        }

    }

    /**
     * list all sales invoices for current company
     *
     * @return Response
     */
    // public function salesInvoice(InvoiceDataTable $table)
    // {
    //     $this->hasAccess(['view_invoice']);
    //     //set page title
    //     $data['title'] = 'Sales Invoice';
    //     $data['type'] = 'Inflow';
    //     //get all invoices for company
    //     $data['invoices'] = Invoice::where('invoice_type', 'sales')
    //         ->latest('id')
    //         ->paginate(10);
    //     //set view page
    //     //dd($data);
    //     return view('invoices::sales-invoice')->with($data);

    // }

    /**
     * list all purchase invoices for current company
     *
     * @return Response
     */
    public function purchaseInvoice()
    {

        $this->hasAccess(['add_invoice']);

        //set page title
        $data['title'] = 'Purchase Invoice';
        $data['type'] = 'Outflow';
        //get all invoices for company
        $data['invoices'] = Invoice::where('invoice_type', 'purchases')->paginate(30);
        //set view page
        return view('invoices::purchase-invoice')->with($data);

    }

    /**
     * display sales invoice form
     * @return view
     */
    public function displaySalesInvoiceForm()
    {
        //set page title
        $this->hasAccess(['add_invoice']);

        $data['title'] = 'Add sales invoice';
        //get all customers and pass to page
        $data['customers'] = Customer::where('flag', 'Active')->get();
        //get Bank

        // $banks = Ledger::whereRaw('group_id = ?', [16])->get();

        $banks = Ledger::where('group_id', 16)->where('flag', 'Active')
            ->orderBy('id', 'desc')
            ->get();

        $income = Ledger::where('group_id', 35)
            ->where('flag', 'Active')
            ->where('code', '!=', '4120')
            ->orderBy('id', 'desc')
            ->get();

        $data['banks'] = $banks;
        $data['income_stream'] = $income;
        //set view page
        return view('invoices::sales-invoice-form')->with($data);

    }

    /**
     * display purchase invoice form
     * @return view
     */
    public function displayPurchaseInvoiceForm()
    {
        $this->hasAccess(['add_invoice', 'view_invoice']);
        //set page title
        $data['title'] = 'Add purchase invoice';
        //get all customers and pass to page
        $data['vendors'] = Vendor::all();
        //set view page
        return view('invoices::purchase-invoice-form')->with($data);

    }

    /**
     * method to save invoice
     * @return mixed
     */
    public function store()
    {

        $this->hasAccess(['add_invoice']);
        //get all form element
        $input = request()->all();
        //model method to save or update invoice


        $verifyDate = Transaction::verifyTransactionDate($input['invoice_date']);

        //dd($input['invoice_date']);

        switch ($verifyDate) {
            case "no":
                {
                    flash()->error('Transaction date must be between current Fiscal Year');
                    return redirect()->to('invoices/sales-invoice');
                }
                break;
            case "no-f-year":
                {
                    flash()->error('Please setup a Fiscal Year before entering transactions');
                    return redirect()->to('invoices/sales-invoice');
                }
                break;
        }

        $invoice = new Invoice;
        $invoice->saveInvoice($input);
        if ($invoice->getisError()) {
            flash()->error($invoice->flattenedError());
            return redirect()->to('invoices/sales-invoice');
        }


        //return to view page on success
        flash()->success('Invoice was created successfully!');
        return redirect()->to('invoices/sales-invoice');

    }

    /**
     * method to view an invoice
     * @param $id
     * @return $this
     */
    public function view($id)
    {
        //dd(getBusinessOwnerID());
        $this->hasAccess(['view_invoice']);
        // $this->createCompanyLogo();
        // dd(public_path() . DIRECTORY_SEPARATOR . "pdfFIles" . DIRECTORY_SEPARATOR);
        //find the invoice
        $invoice = Invoice::where('id', $id)->first();
        $invoiceDet = InvoiceItem::where('invoice_id', $id)->get();

        if (!$invoice) {
            return Redirect::back();
        }

        $accountNumber = '';
        //get the bank account
        @$bankName = Ledger::where('id', $invoice->bank_invoice)->first();
        if (!empty($bankName)) {
            @$accountNumber = BankAC::where('ledger_code', $bankName->code)->first();
        }


        //set page title
        $data['title'] = 'Invoice ' . $invoice->invoice_no;
        //pass invoice object to page
        $data['invoice'] = $invoice;        //pass company's invoice settings to page
        $data['invoiceDet'] = $invoiceDet;        //pass company's invoice settings to page
        $data['bankName'] = $bankName;
        $data['accountNumber'] = $accountNumber;
        //$data['settings'] = $this->settings;
        //set view page


        return view('invoices::templates.template_4')->with($data);

    }

    public function edit($id)
    {

        $this->hasAccess(['edit_invoice']);
        //find the invoice
        $invoice = Invoice::where('id', $id)->first();
        $transaction = Transaction::where('id', $invoice->transaction_id)->first();

        if (!$invoice) {
            return Redirect::back();
        }
        //       dd($transaction->transaction_id);
        $data['transaction'] = $transaction;
        //set page title
        $data['title'] = 'Invoice ' . $invoice->invoice_no;
        //get customers/vendor and pass to page
        if ($invoice->invoice_type === 'sales') {
            $data['customers'] = Customer::all();
        } else {
            $data['customers'] = Vendor::all();
        }


        //get all products
        // $data['products'] = Product::all();
        //  dd($data);
        //pass invoice object to page
        $data['invoice'] = $invoice;
        $data['invoiceDetails'] = InvoiceItem::where('invoice_id', $invoice->id)->get();
        $banks = Ledger::whereRaw('group_id = ?', [16])->get();
        $data['banks'] = $banks;
        //set view page
        return view('invoices::edit-form')->with($data);

    }

    /**/

    public function sendPdfMail($id)
    {
        //find the invoice
        $invoice = Invoice::where('id', $id)->first();
        $invoiceDet = InvoiceItem::where('invoice_id', $id)->get();
        $customerIdentity = Customer::where('id', $invoice->customer_id)->first();

        if (!$invoice) {
            return Redirect::back();
        }
        //get the bank account
        @$bankName = Ledger::where('id', $invoice->bank_invoice)->first();
        if (!empty($bankName)) {
            @$accountNumber = BankAC::where('ledger_code', $bankName->code)->first();
        }


        //set page title
        $data['title'] = 'Invoice ' . $invoice->invoice_no;
        //pass invoice object to page
        $data['invoice'] = $invoice; //pass company's invoice settings to page
        $data['invoiceDet'] = $invoiceDet;
        $data['customerIdentity'] = $customerIdentity;
        $data['bankName'] = $bankName;
        $data['accountNumber'] = $accountNumber;

        // dd($data);

        // $pdf = PDF::loadView('pdfgenerate.pdfgenerate', $data);

        // $theName = GenerateRandomString("10", "ALPHA");
        $theName = $customerIdentity->first_name . '_' . $customerIdentity->last_name;
        $theInvoice = storage_path('app') . DIRECTORY_SEPARATOR . "files" . DIRECTORY_SEPARATOR;
        $thePdfFile = $theInvoice . $theName . '.pdf';
        //dd($thePdfFile);
        PDF::loadView('pdfgenerate.pdfgenerate', $data)->save($thePdfFile);
        try {
            Mail::send('pdfgenerate.invoice', $data, function ($message) use ($thePdfFile, $invoice) {
                $message
                    ->from(config('mail.from.address'), getBusinessOwnerAuth()->company->company_name)
                    ->to($invoice->customer->email)
                    //->to('rylxes@gmail.com')
                    ->attach($thePdfFile)
                    ->subject('Invoice');

            });
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
            flash()->success('Error Occured !');
            return redirect()->back();
        }
        unlink($thePdfFile);
        flash()->success('Invoice Successfully sent');
        return redirect()->back();
    }

    public function delete($id)
    {

        $this->hasAccess(['delete_invoice']);
        DB::beginTransaction();
        try {
            InvoiceItem::where('invoice_id', $id)->delete();
            $inv = Invoice::where('id', $id)->first();
            if (!empty($inv->transaction_id)) {
                // dd($inv->transaction_id);
                TransactionItem::where('transaction_id', $inv->transaction_id)->delete();
                Transaction::find($inv->transaction_id)->delete();
            }
            Invoice::find($id)->delete();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'message' => $e->getMessage(),
            ]);
        }
        DB::commit();
        return response()->json([
            'status' => 200,
            'message' => 'Invoice deleted.',
        ]);

    }

}
