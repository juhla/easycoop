<?php

namespace App\Modules\Invoices\Models;

use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use App\Modules\Base\Traits\Uuids;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class Customer extends \App\Modules\Base\Models\Customer
{

}