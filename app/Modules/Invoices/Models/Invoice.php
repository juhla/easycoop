<?php

namespace App\Modules\Invoices\Models;

use App\Modules\Base\Models\Vendor;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\Customer;
use App\Modules\Invoices\Models\InvoiceItem;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Models\Ledger;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class Invoice extends \App\Modules\Base\Models\Invoice
{

}
