<?php

namespace App\Modules\Invoices\Models;

use Faker\Provider\cs_CZ\DateTime;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\TransactionItem;
use Carbon;
use DB;
use Auth;
use Illuminate\Support\Facades\Schema;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Transaction extends \App\Modules\Base\Models\Transaction
{

}

