<?php

namespace App\Modules\Invoices\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\Uuids;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;

class Vendor extends \App\Modules\Base\Models\Vendor
{

}