<?php

namespace App\Modules\Invoices\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'invoices');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'invoices');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'invoices');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
