<div id="invoice-form-panel" class="modal fade md-slideUp bg-theme-inverse" tabindex="-1" data-width="1100">
    <div class="modal-header bd-theme-inverse-darken">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"><i class="fa fa-book"></i> Invoice</h4>
    </div>
    <!-- //modal-header-->
    <div class="modal-body" style="padding:0">
        <div class="col-lg-12 panel">
            <form id="invoice-form" name="invoice-form" action="{{ URL::route('new_invoice') }}" method="POST" class="panel-body">
                <input type="hidden" value="{{ csrf_token() }}" name="_token"/>
                <input type="hidden" name="_invoice" value=""/>
                <div class="col-md-12">
                    <h3><strong>Customer </strong>Info <span class="pull-right">Invoice Date: <small id="inv_date">{{ date('M d, Y', strtotime(\Carbon\Carbon::now())) }}</small></span></h3>
                    <hr/>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Select Customer</label>
                            <select class="form-control selectpicker" data-live-search="true" name="Customer" id="Customer">
                                <option value="">Select Customer</option>
                                <option value="1">ABX World</option>
                                <option value="2">Zenith Limited</option>
                                <option value="3">Cowmilky Plc</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-8">
                        <h4>My customer info</h4>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="DueDate">Due Date</label>
                            <input class="form-control" type="date" id="DueDate" name="DueDate" />
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Select payment term</label>
                            <select class="form-control PTerm" name="PTerm" id="PTerm">
                                <option value="">Payment term</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12"><hr/>
                    <h3><strong>Invoice</strong> Items</h3>
                    <hr/>
                    <div class="table-responsive">
                        <table cellpadding="0" cellspacing="0" border="0" class="table">
                            <thead>
                            <th style="text-align: left">Product</th>
                            <th style="text-align: left">Price</th>
                            <th style="text-align: left">Quantity</th>
                            <th style="text-align: left">Discount</th>
                            <th style="text-align: left">Description</th>
                            </thead>
                            <tbody id="item-container">
                            <tr>
                                <td width="22%">
                                    <select class="form-control product" name="Product[]" id="Product_1">
                                        <option value="">Select Product/Service</option>
                                    </select>
                                </td>
                                <td>
                                    <span class="badge price-label">NGN 0.00</span>
                                    <input type="hidden" class="Price" name="Price[]" value=""/>
                                </td>
                                <td width="5%">
                                    <input class="form-control Qty" id="Qty" name="Qty[]" placeholder="1" value="" type="text"/>
                                </td>
                                <td width="5%">
                                    <input class="form-control Discount" id="Discount" value="" name="Discount[]" placeholder="%0" type="text"/>
                                </td>
                                <td>
                                    <textarea class="form-control Description" maxlength="60" name="Description[]" id="Description" cols="20" rows="1"></textarea>
                                </td>
                                <td width="2%"><span class="badge"><i class="add-item fa fa-plus-circle"></i></span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" id="submit-invoice" class="btn btn-theme-inverse btn-sm" data-value="new">Save Invoice</button>
                    </div>
                </div>

                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>