<?php

use App\Modules\Base\Traits\Acl;

?>

<div class="btn-group btn-group-sm tooltip-area">
    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span>
    </button>
    <ul class="dropdown-menu align-xs-left" role="menu" style="right: 0; left: auto;">
        <li><a href="{{ url('invoices/view/'.$invoice->id) }}">View</a></li>
        @if(Acl::can('edit_invoice'))
        <li><a href="{{ url('invoices/edit/'.$invoice->id) }}">Edit</a></li>
        @endif

        @if($invoice->transactions_without_scope->approve_status == '-1') 
                    <li><a href="#" class="viewReason" data-value="{{$invoice->transactions_without_scope->id}}">View Rejection Reason</a></li>
                @endif
        {{--<li><a href="#">Print</a></li>
        <li class="divider"></li>
        @if(canEdit())
        <li><a href="#">Add a payment</a></li>
        @endif
        <li class="divider"></li>
        <li><a href="#">Send</a></li>
        <li><a href="#">Export as PDF</a></li>--}}
        @if(Acl::can('delete_invoice'))
        <li class="divider"></li>
        <li><a href="javascript:;" class="delete" data-value="{{ $invoice->id }}">Delete</a></li>
        @endif
    </ul>
</div>
