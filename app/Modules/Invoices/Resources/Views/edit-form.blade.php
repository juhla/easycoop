@extends('layouts.main')

@section('content')
    <style>
        .bankhide {
            display: none;
        }
    </style>
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            <a href="{{ url('invoices/sales-invoice') }}" class="btn btn-sm btn-primary">
                                <i class="fa fa-arrow-left"></i> Back to Invoices
                            </a>


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            {{--<div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>--}}
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="panel-body">
                            <form class="horizontal-form" role="form" method="post" action="{{ url('invoices/store') }}"
                                  id="editInvoiceForm" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="invoice_id" value="{{ $invoice->id }}"/>
                                <input type="hidden" name="reference_no" value="{{ $invoice->reference_no }}"/>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Customer <span class="text-danger">*</span></label>
                                            <select class="select2-option customers" style="width: 100%"
                                                    name="customer_id" id="customer_id" data-required>
                                                <option value=""></option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id}}" {{ ($invoice->customer_id === $customer->id) ? 'selected' : ''}}>{{ $customer->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Date <span class="text-danger">*</span></label>
                                            <input type="text" name="invoice_date" id="invoice_date"
                                                   class="form-control datepicker" autocomplete="off"
                                                   value="{{ date('d/m/Y', strtotime($invoice->invoice_date)) }}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Due Date</label>
                                            <input type="text" name="due_date" id="due_date"
                                                   class="form-control datepicker"
                                                   value="{{ date('d/m/Y', strtotime($invoice->due_date)) }}" required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Currency</label>
                                            <select class="select2-option" name="currency" id="currency"
                                                    style="width: 100%">
                                                <option value=""></option>
                                                @foreach(getCountry() as $country)
                                                    <option value="{{ $country->currency_code }}" {{($invoice->currency === $country->currency_code) ? 'selected' : ''}}>{{ $country->currency_code. '('.$country->currency_symbol.')' }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="">Payment<span class="text-danger">*</span> </label>
                                            <select class="select2-option" name="payment_type" id="paymentType"
                                                    style="width: 100%" data-required>
                                                <option value="">Select Payment Type</option>
                                                <option value="cash" {{$invoice->payment_term_id == 1 ? 'selected' : ''}}>
                                                    Cash
                                                </option>
                                                <option value="credit" {{$invoice->payment_term_id == 2 ? 'selected' : ''}}>
                                                    Credit
                                                </option>
                                                <option value="bank"
                                                        {{$invoice->payment_term_id == 3 ? 'selected' : ''}} Bank
                                                </option>
                                            </select>
                                        </div>

                                    </div>
                                    <!--/span-->
                                    {{--<div class="col-md-4">--}}
                                    {{--<div class="form-group">--}}
                                    {{--<label class="control-label">P.O/S.O</label>--}}
                                    {{--<input type="text" name="po_so_no" id="po_so_no" class="form-control" value="{{ $invoice-> }}">--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Description of transaction <span
                                                        class="text-danger">*</span></label>
                                            <textarea name="memo" id="memo" class="form-control" rows="5" cols="5"
                                                      required>{{ $invoice->memo }}</textarea>
                                        </div>


                                    </div>
                                    <!--/span-->
                                </div>

                                <div class="row">
                                    <div class="col-md-4 ">


                                        <div class="form-group">
                                            <label for="bank_invoice" class="">Select account displayed on
                                                invoice</label>
                                            <select class="select2-option" name="bank_invoice" id="bank_invoice"
                                                    style="width: 100%" data-required>
                                                <option value="">Select account displayed on invoice</option>
                                                @foreach($banks as $bank)
                                                    <option value="{{ $bank->id}}">{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>
                                    <!--/span-->

                                    <div class="col-md-4  ledger    {{$invoice->payment_type != 'bank' ? 'bankhide' : ''}}">
                                        <div class="form-group">
                                            <label class="">Bank <span class="text-danger">*</span></label>
                                            <select class="select2-option banks" style="width: 100%" name="bank_id"
                                                    id="bank_id"
                                                    class="bank_id" {{$invoice->payment_type != 'bank' ? ' data-required' : ''}}
                                            " >
                                            <option value="">Select Banks</option>
                                            @foreach($banks as $bank)
                                                <option value="{{ $bank->id}}" {{(isset($transaction->bank_id) && ($transaction->bank_id == $bank->id)) ? 'selected' : ''}}>{{ $bank->name }} </option>
                                                @endforeach
                                                </select>
                                        </div>
                                    </div>

                                    <!--/span-->
                                </div>
                                <br>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped inventory">
                                        <thead>
                                        <tr>
                                            <th width="80%" class="text-left">Particulars</th>
                                            <th width="10%" class="text-center">Amount</th>
                                            <th width="5%" class="text-center"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($invoiceDetails as $i=>$item)
                                            <tr>

                                                <td><input type="text" class="form-control" name="description[]"
                                                           id="description" value="{{ $item->description }}"></td>
                                                <td><input type="number" data-required step="0.01"
                                                           class="form-control inputs" name="price[]" id="price"
                                                           value="{{ $item->amount }}"/></td>
                                                @if($i > 0)
                                                    <td><a href="javascript:;" class="cut" style="color: red;"><i
                                                                    class="fa fa-times"></i> </a>
                                                @endif

                                            </tr>
                                            <input type="hidden" name="item_id[]" value="{{ $item->id }}"/>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="align-lg-left">
                                            <a href="javascript:;" id="add-row"><i class="fa fa-plus-circle"></i> Add
                                                more</a>
                                        </div>
                                        <br/>
                                        <div class="form-group inline-block ">
                                            <input id="vat" name="vat" {{($invoice->vat? 'checked':'')}}
                                            type="checkbox"><label style="font-weight:bolder"
                                                                   for="vat"><p>Apply
                                                    Vat(5%)</p></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="pull-right">
                                            <table>
                                                <tr>
                                                    <td><h3>Sub - Total amount: &nbsp;&nbsp;</h3></td>
                                                    <td><h3><span id="currencyCode">{{$invoice->currency}}</span><span
                                                                    class="sub-total">0.00</span></h3></td>
                                                </tr>
                                                <tr>
                                                    {{--<li> VAT: <strong>0.00</strong> </li>--}}
                                                    <td><h3>Grand Total: </h3></td>
                                                    <td><h3><span id="currencyCode2">{{$invoice->currency}}</span><span
                                                                    class="grand-total">0.00</span></h3></td>
                                                </tr>
                                                <input type="hidden" id="total" name="total" value="0"/>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <input type="hidden" name="invoice_type" id="invoice_type" value="sales">
                                    <a href="{{ url('invoices/sales-invoice') }}" class="btn btn-danger hidden-print">Cancel </a>
                                    <button type="submit" id="editBTN" class="btn btn-info hidden-print"> EDIT</button>
                                </div>
                            </form>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/invoice.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


        $(function () {

            var total = $('total').val();
            $("#editInvoiceForm").submit(function (e) {
                //prevent Default functionality
                e.preventDefault();
                document.getElementById("editInvoiceForm").submit();
            });


            $('.close-customer-modal').click(function () {
                $('#customerForm')[0].reset();
                $('#customerFormModal').modal('hide');
                $("#country_id").select2('val', '');
                $("#currency_code").select2('val', '');
            });
            $('.close-product-modal').click(function () {
                $('#productForm')[0].reset();
                $('#productFormModal').modal('hide');
                $("#tax").select2('val', '');
            });
            $('#country_id').on('change', function () {
                var currency = $('option:selected', this).data('title');
                //set currency
                $('#currency_code').select2('val', currency);
            });

            updateInvoice();

            $('#paymentType').change(function () {
                var payment_type = $(this).val();
                if (payment_type != 'cash' && payment_type != '' && payment_type != 'credit') {
                    $('.ledger').removeClass('bankhide');
                    $("#bank_id ").removeAttr('disabled');
                    //$("#bank_id ").attr('enabled','enabled')
                    $('.ledger').show();
                } else {
                    $('.ledger').hide();
                    $("#bank_id ").attr('disabled', 'disabled')
                }
            });
        });

        $('#currency').change(function () {
            var currency = $('#currency').val();
            $('#currencyCode').html(currency);
            $('#currencyCode2').html(currency);
        });


    </script>
@stop
