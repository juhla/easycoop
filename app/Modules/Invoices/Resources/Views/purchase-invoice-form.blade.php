@extends('app.layouts.app')

@section('content')
    <link href="{{ asset('app/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li><p>Accounting</p></li>
                        <li><a href="#">Outflow</a></li>
                        <li><a href="#" class="active">Purchase Invoice</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg m-t-20">
            @include('flash::message')

            <div class="col-lg-12 bg-white">
                <div class="row">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                            <div class="panel-title">Add New Purchase Invoice</div>
                        </div>
                        <div class="panel-body">
                            <form class="horizontal-form" role="form" method="post" action="{{ url('invoice/store') }}" id="invoiceForm">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="invoice_id" value="" />
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Vendor <span class="text-danger">*</span></label>
                                            <select class="full-width customers" name="customer_id" id="customer_id" data-placeholder="Select Vendor/Supplier" data-init-plugin="select2" required>
                                                <option value=""></option>
                                                @foreach($vendors as $vendor)
                                                    <option value="{{ $vendor->id}}">{{ $vendor->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Date <span class="text-danger">*</span></label>
                                            <input type="text" name="invoice_date" id="invoice_date" class="form-control datepicker" value="{{ date('Y-m-d') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Due Date</label>
                                            <input type="text" name="due_date" id="due_date" class="form-control datepicker" value="{{date('Y-m-d')}}" required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Currency</label>
                                            <select class="full-width" name="currency" id="currency" data-placeholder="Select invoice currency" data-init-plugin="select2">
                                                <option value=""></option>
                                                @foreach(getCountry() as $country)
                                                    <option value="{{ $country->currency_code }}">{{ $country->currency_code. '('.$country->currency_symbol.')' }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">P.O/S.O</label>
                                            <input type="text" name="po_so_no" id="po_so_no" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Memo <span class="text-danger">*</span></label>
                                            <textarea name="memo" id="memo" class="form-control" row="5" col="5" required></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <br>
                                <br>
                                <table class="table table-striped inventory">
                                    <thead>
                                    <tr>
                                        <th width="25%" class="text-left">Product</th>
                                        <th width="25%" class="text-left">Description</th>
                                        <th width="10%" class="text-left">Quantity</th>
                                        <th width="10%" class="text-left">Price</th>
                                        <th width="18%" class="text-left">Tax</th>
                                        <th width="10%" class="text-center">Amount</th>
                                        <th width="5%" class="text-center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <select class="full-width products product_1" name="product[]" id="product_1" data-init-plugin="select2">
                                                <option value="">-----------</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" name="description[]" id="description" value=""></td>
                                        <td><input type="text" class="form-control inputs" name="quantity[]" id="quantity" value="1" /></td>
                                        <td><input type="text" class="form-control inputs" name="price[]" id="price" value="0" /></td>
                                        <td><input type="text" class="form-control taxes" name="tax[]" id="tax" value="" /></td>
                                        <td><span class="rowTotal">0.00</span> </td>
                                        <td><input type="hidden" name="item_amount[]" id="item_amount" value="" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <br><br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="align-lg-left">
                                            <a href="javascript:;" id="add-row"><i class="fa fa-plus-circle"></i> Add more</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="pull-right">
                                            <table>
                                                <tr>
                                                    <td> <h3>Sub - Total amount: &nbsp;&nbsp;</h3> </td><td><h3>₦<span class="sub-total">0.00</span></h3> </td>
                                                </tr>
                                                <tr>
                                                {{--<li> VAT: <strong>0.00</strong> </li>--}}
                                                    <td> <h3>Grand Total: </h3></td><td><h3>₦<span class="grand-total">0.00</span></h3> </td>
                                                </tr>
                                                <input type="hidden" id="total" name="total" value="0" />
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pull-right">
                                    <input type="hidden" name="invoice_type" id="invoice_type" value="purchases">
                                    <button type="reset" class="btn btn-info hidden-print">Cancel </button>
                                    <button type="submit" class="btn btn-complete hidden-print"> SAVE </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('accounting::Vendors.Views.form')
    @include('app.partials.product-form')
@stop
@section('scripts')
    <script src="{{ asset('app/js/invoice.js') }}" type="text/javascript"></script>
@stop