@extends('app.layouts.app')

@section('content')

    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li><p>Accounting</p></li>
                        <li><a href="#">Outflow</a></li>
                        <li><a href="#" class="active">Purchase Invoices</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg m-t-20">
            @include('flash::message')

            <div class="col-lg-12 bg-white">
                <div class="row">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                            <div class="panel-title">Purchase Invoices
                            </div>
                            <a href="{{ url('purchase-invoice/add') }}" class="btn btn-info pull-right"><i class="fa fa-plus-circle"></i> Create new invoice</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover transaction-table" id="transaction-table">
                                    <thead>
                                    <tr valign="center">
                                        <th>Date</th>
                                        <th>Number</th>
                                        <th>Vendor</th>
                                        <th>Amount due</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @if(count($invoices) > 0)
                                        @foreach($invoices as $invoice)
                                            <tr class="">
                                                <td>{{ $invoice->invoice_date }}</td>
                                                <td>{{ $invoice->invoice_no }}</td>
                                                <td>{{ $invoice->vendor->name }}</td>
                                                <td>{{ formatNumber($invoice->amount_due) }}</td>
                                                <td>{{ formatNumber($invoice->total_amount) }}</td>
                                                <td></td>
                                                <td>
                                                    <div class="btn-group btn-group-sm tooltip-area">
                                                        <button type="button" class="btn btn-complete dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> </button>
                                                        <ul class="dropdown-menu align-xs-left" role="menu"  style="right: 0; left: auto;">
                                                            <li><a href="{{ url('invoice/view/'.$invoice->id) }}">View</a></li>
                                                            <li><a href="{{ url('invoice/edit/'.$invoice->id) }}">Edit</a></li>
                                                            <li><a href="#">Print</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#">Add a payment</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#">Send</a></li>
                                                            <li><a href="#">Export as PDF</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="javascript:;" class="delete" data-value="{{ $invoice->id }}">Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="no-income"><td colspan="8">You have not created any invoice yet.</td> </tr>
                                    @endif
                                    </tbody>
                                </table>
                                {!! $invoices->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script type="text/javascript">
        $(function(){
            $(document).on('click', '.delete', function(e){
                var id = $(this).data('value');
                var  btn=$(this),
                        panelBody=btn.closest(".panel").find(".panel-body"),
                        overlay=$('<div class="load-overlay"><div><div class="c1"></div><div class="c2"></div><div class="c3"></div><div class="c4"></div></div><span>Loading...</span></div>');
                panelBody.append(overlay);
                overlay.css('opacity',1).fadeIn();
                if(confirm('Are you sure you want to delete this invoice?')) {
                    $.ajax({
                        url: $('#baseurl').val() + '/invoice/delete/' + id,
                        type: 'GET',
                        success: function (res) {
                            displayNotification('Invoice deleted', 'success');
                            $(this).closest('ul').closest.closest('tr').remove();
                            setTimeout(function () {
                                panelBody.find(overlay).fadeOut("slow", function () {
                                    $(this).remove()
                                });
                            }, 300);
                        },
                        error: function (err) {
                            setTimeout(function () {
                                panelBody.find(overlay).fadeOut("slow", function () {
                                    $(this).remove()
                                });
                            }, 300);
                            displayNotification(err, 'error');
                        }
                    });
                }
            });
        });
    </script>
@stop
