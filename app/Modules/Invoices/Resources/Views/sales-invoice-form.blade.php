@extends('layouts.main')

@section('content')
    <style>
        .bankhide {
            display: none;
        }
    </style>
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            <a href="{{ url('invoices/sales-invoice') }}" class="btn btn-sm btn-primary">
                                <i class="fa fa-arrow-left"></i> Back to Invoice
                            </a>
                            &nbsp;
                            <a href="javascript:;" class="btn btn-sm btn-primary add-new">
                                <i class="fa fa-plus"> </i>&nbsp;New Customer
                            </a>

                        </div>
                        <div class="col-sm-4 m-b-xs">
                            {{--<div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>--}}
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        @include('flash::message')
                        <div class="panel-body">
                            <form class="horizontal-form" role="form" method="post" action="{{ url('invoices/store') }}"
                                  id="invoiceForm" data-validate="parsley">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="invoice_id" value=""/>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Customer <span class="text-danger">*</span></label>
                                            <select class="select2-option customers" style="width: 100%"
                                                    name="customer_id" id="customer_id" data-required>
                                                <option value=""></option>
                                                @foreach($customers as $customer)
                                                    <option value="{{ $customer->id}}">{{ $customer->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Date <span class="text-danger">*</span></label>
                                            <input type="text" name="invoice_date" id="invoice_date"
                                                   class="form-control datepicker"
                                                   value="{{ date('d/m/Y', strtotime(date('Y-m-d'))) }}" autocomplete="off" data-required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Due Date</label>
                                            <input type="text" data-parsley-type="number" name="due_date" id="due_date"
                                                   class="form-control datepicker"
                                                   value="{{ date('d/m/Y', strtotime(date('Y-m-d'))) }}" autocomplete="off" data-required>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="">Currency</label>
                                            <select class="select2-option" name="currency" id="currency"
                                                    style="width: 100%">
                                                <option value=""></option>
                                                @foreach(getCountry() as $country)
                                                    <option value="{{ $country->currency_code }}">{{ $country->currency_code. '('.$country->currency_symbol.')' }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">P.O/S.O</label>
                                            <input type="text" name="po_so_no" id="po_so_no" class="form-control"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Description of Transaction <span
                                                        class="text-danger">*</span></label>
                                            <textarea name="memo" id="memo" class="form-control" rows="5"
                                                      cols="5"></textarea>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label class="">Income </label>
                                            <select class="select2-option" name="income_type" id="incomeType"
                                                    style="width: 100%" data-required>
                                                @foreach($income_stream as $each)
                                                    <option value="{{ $each->code}}">{{ $each->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                        <div class="form-group">
                                            <label for="bank_invoice" class="">Select Account Displayed on
                                                Invoice</label>
                                            <select class="select2-option" name="bank_invoice" id="bank_invoice"
                                                    style="width: 100%" data-required>
                                                <option value="">Select account displayed on invoice</option>
                                                @foreach($banks as $bank)
                                                    <option value="{{ $bank->id}}">{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>


                                    </div>

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label class="">Payment </label>
                                            <select class="select2-option" name="payment_type" id="paymentType"
                                                    style="width: 100%" data-required>
                                                <option value="">Select Payment Type</option>
                                                <option value="cash">Cash</option>
                                                <option value="credit">Credit</option>
                                                <option value="bank">Bank</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-4 bankhide ledger">
                                        <div class="form-group">
                                            <label class="">Bank <span class="text-danger">*</span></label>
                                            <select class="select2-option banks" style="width: 100%" name="bank_id"
                                                    id="bank_id" class="bank_id">
                                                <option value="">Select Banks</option>
                                                @foreach($banks as $bank)
                                                    <option value="{{ $bank->id}}">{{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <!--/span-->
                                </div>
                                <br>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-striped inventory">
                                        <thead>
                                        <tr>
                                            <th width="75%" class="text-left">Particulars</th>
                                            <th width="10%" class="text-left">Amount</th>
                                            <th width="5%" class="text-center"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><input type="text" class="form-control" name="description[]"
                                                       id="description" value=""></td>
                                            <td><input type="number" step="0.01" data-required
                                                       class="form-control inputs"
                                                       name="price[]" id="price" value="0"/></td>
                                            {{--<td><input type="text" class="form-control taxes" name="tax[]" id="tax" value="" /></td>--}}
                                            <td><input type="hidden" name="item_amount[]" id="item_amount" value=""/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="align-lg-left">
                                            <a href="javascript:;" id="add-row"><i class="fa fa-plus-circle"></i> Add
                                                more</a>
                                        </div>
                                        <br/>
                                        <div class="form-group inline-block ">
                                            <input id="vat" name="vat" type="checkbox"><label style="font-weight:bolder"
                                                                                              for="vat"><p>Apply
                                                    Vat(5%)</p></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="pull-right">
                                            <table>
                                                <tr>
                                                    <td><h3>Sub - Total amount: &nbsp;&nbsp;</h3></td>
                                                    <td><h3><span id="currencySymbol">₦ </span><span class="sub-total">0.00</span>
                                                        </h3></td>
                                                </tr>
                                                <tr>
                                                    <td><h3>VAT: &nbsp;&nbsp;</h3></td>
                                                    <td><h3><span id="currencyvat">₦ </span><span
                                                                    class="vat-total">0.00</span>
                                                        </h3></td>
                                                </tr>
                                                <tr>
                                                    {{--<li> VAT: <strong>0.00</strong> </li>--}}
                                                    <td><h3>Grand Total: </h3></td>
                                                    <td><h3><span id="grandTotal">₦ </span><span class="grand-total">0.00</span>
                                                        </h3></td>
                                                </tr>
                                                <input type="hidden" id="total" name="total" value="0"/>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <input type="hidden" name="invoice_type" id="invoice_type" value="sales">
                                    <a href="{{ url('invoices/sales-invoice') }}" class="btn btn-danger hidden-print">Cancel </a>
                                    <button type="submit" class="btn btn-info hidden-print"> SAVE</button>
                                </div>
                            </form>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                    </div>
                </footer>
            </section>
        </aside>
    </section>

    @include('business-setup::partials._customer-form')
    @include('business-setup::partials._product-form')
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/invoice.js') }}" type="text/javascript"></script>
    <script type="text/javascript">


        $(function () {


            $('.add-new').click(function () {
                $('#customerFormModal').modal('show');
            });

            $('.close-customer-modal').click(function () {
                $('#customerForm')[0].reset();
                $('#customerFormModal').modal('hide');
                $("#country_id").select2('val', '');
                $("#currency_code").select2('val', '');
            });
            $('.close-product-modal').click(function () {
                $('#productForm')[0].reset();
                $('#productFormModal').modal('hide');
                $("#tax").select2('val', '');
            });
            $('#country_id').on('change', function () {
                var currency = $('option:selected', this).data('title');
                //set currency
                $('#currency_code').select2('val', currency);
            });

            $('#paymentType').change(function () {
                var payment_type = $(this).val();
                if (payment_type != 'cash' && payment_type != '' && payment_type != 'credit') {
                    $('.ledger').removeClass('bankhide');
                    $("#bank_id ").removeAttr('disabled');
                    //$("#bank_id ").attr('enabled','enabled')
                    $('.ledger').show();
                } else {
                    $('.ledger').hide();
                    $("#bank_id ").attr('disabled', 'disabled')
                }
            });
        });


        $('#currency').change(function () {
            var currency = $('#currency').val();
            $('#currencySymbol').html(currency + " ");
            $('#currencyvat').html(currency + " ");
            $('#grandTotal').html(currency + " ");
        });


        $('.add-new').click(function () {
            $('#customerFormModal').modal('show');
        });


    </script>
@stop
