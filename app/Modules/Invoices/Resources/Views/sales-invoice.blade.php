<?php

use App\Modules\Base\Traits\Acl;

?>
@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at
                        ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-4 m-b-xs">
                            @if(Acl::can(['delete_invoice']))
                                <button type="button" class="btn btn-sm btn-danger bulk-del" title="Remove" disabled><i
                                            class="fa fa-trash-o"></i>
                                    Delete
                                </button>
                            @endif
                            @if(Acl::can(['add_invoice']))
                                <a href="{{  url('invoices/sales-invoice/add') }}" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i> Create New Invoice
                                </a>
                            @endif
                        </div>
                        <div>
                            @if(Acl::can(['view_invoice']) && Request::is('invoices/sales-invoice'))
                                @include('invoice::actions.search')
                            @endif
                        </div>
                    </div>
                </header>

                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            @section('css')
                                @include('layouts.datatables_css')
                            @endsection
                            {!! $dataTable->table(['width' => '100%']) !!}
                            @section('scripts_dt')
                                @include('layouts.datatables_js')
                                {!! $dataTable->scripts() !!}
                            @endsection
                        </div>
                    </section>
                </section>
            </section>
            <footer class="footer bg-white b-t">
                <div class="row text-center-xs">
                    {{--<div class="col-md-6 hidden-sm">
                        <p class="text-muted m-t">Showing 20-30 of 50</p>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right text-center-xs">
                        <ul class="pagination pagination-sm m-t-sm m-b-none">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>--}}
                    {{-- {!! $invoices->links() !!} --}}
                </div>
            </footer>
    </section>
    </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $(document).on('click', '.delete', function (e) {
                var id = $(this).data('value');
                var btn = $(this),
                    panelBody = btn.closest(".panel").find(".panel-body"),
                    overlay = $('<div class="load-overlay"><div><div class="c1"></div><div class="c2"></div><div class="c3"></div><div class="c4"></div></div><span>Loading...</span></div>');
                panelBody.append(overlay);
                overlay.css('opacity', 1).fadeIn();
                if (confirm('Are you sure you want to delete this invoice?')) {
                    $.ajax({
                        url: $('#baseurl').val() + '/invoices/delete/' + id,
                        type: 'GET',
                        success: function (res) {
                            console.log(res);//return
                            displayNotification('Invoice deleted', 'Success', 'success');
                            //$(this).closest('ul').closest('tr').remove();
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        },
                        error: function (err) {
                            displayNotification(err, 'Error!', 'error');
                        }
                    });
                }
            });
        });
    </script>
    <script>
        $('#clearBTN').click(function () {
            $('#start_date').val('');
            $('#end_date').val('');
        });
    </script>
@stop
