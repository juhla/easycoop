@extends('app.layouts.app')

@section('content')
        <!-- START PAGE CONTENT -->
<div class="content">
    <nav class="navbar navbar-default bg-master-lighter sm-padding-10" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sub-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="sub-nav">
                <div class="row">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <ul class="nav navbar-nav navbar-center">
                            <li>
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Export to PDF"><i class="fa fa-file-pdf-o"></i></a>
                            </li>
                            <li>
                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="Print"><i class="fa fa-print"></i></a>
                            </li>
                            <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Download"><i class="fa fa-download"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <br>
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START PANEL -->
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                    <div>
                        <div class="pull-left">
                            {{--<img width="235" height="47" alt="" class="invoice-logo" data-src-retina="assets/img/invoice/squarespace2x.png" data-src="assets/img/invoice/squarespace.png" src="assets/img/invoice/squarespace2x.png">--}}
                            <h3>Company Logo</h3>
                            <address class="m-t-10">
                                Company address
                                <br>(877) 412-7753.
                                <br>
                            </address>
                        </div>
                        <div class="pull-right sm-m-t-20">
                            <h2 class="font-montserrat all-caps hint-text">Invoice</h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    <br>
                    <div class="container-sm-height">
                        <div class="row-sm-height">
                            <div class="col-md-9 col-sm-height sm-no-padding">
                                <p class="small no-margin">Invoice to</p>
                                @if($invoice->invoice_type === 'sales')
                                    <h5 class="semi-bold">{{ $invoice->customer->name }}</h5>
                                    <address>
                                        {{ $invoice->customer->address }}<br>
                                        {{ ($invoice->customer->country_id) ? getCountry($invoice->customer->country_id) : '' }}
                                    </address>
                                @else
                                    <h5 class="semi-bold m-t-0">{{ $invoice->vendor->name }}</h5>
                                    <address>
                                        {{ $invoice->vendor->address }}<br>
                                        {{ ($invoice->vendor->country_id) ? getCountry($invoice->vendor->country_id) : '' }}
                                    </address>
                                @endif

                            </div>
                            <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                                <br>
                                <div>
                                    <div class="pull-left font-montserrat bold all-caps">Invoice No :</div>
                                    <div class="pull-right">{{ $invoice->invoice_no }}</div>
                                    <div class="clearfix"></div>
                                </div>
                                <div>
                                    <div class="pull-left font-montserrat bold all-caps">Invoice date :</div>
                                    <div class="pull-right">{{ date('M d, Y', strtotime( $invoice->invoice_date )) }}</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered m-t-50">
                            <thead>
                            <tr>
                                <th width="25%" class="text-left">Product</th>
                                <th width="10%" class="text-left">Quantity</th>
                                <th width="10%" class="text-left">Price</th>
                                <th width="10%" class="text-left">Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $invoice->items as $item )
                                <tr>
                                    <td>{{ $item->product->name }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ formatNumber($item->price )}}</td>
                                    <td>{{ formatNumber($item->amount) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="container-sm-height">
                        <div class="row row-sm-height b-a b-grey">
                            <div class="col-sm-3 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                                <h5 class="font-montserrat all-caps small no-margin hint-text bold">Amount Due</h5>
                                <h3 class="no-margin">{{ formatNumber($invoice->amount_due) }}</h3>
                            </div>
                            <div class="col-sm-3 col-sm-height col-middle clearfix sm-p-b-15">
                                <h5 class="font-montserrat all-caps small no-margin hint-text bold">Amount Paid</h5>
                                <h3 class="no-margin">{{ formatNumber($invoice->amount_paid) }}</h3>
                            </div>
                            <div class="col-sm-6 text-right bg-info col-sm-height padding-15">
                                <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
                                <h1 class="no-margin text-white">{{ formatNumber(bcsub($invoice->amount_due, $invoice->amount_paid, 2)) }}</h1>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <p class="small hint-text">Services will be invoiced in accordance with the Service Description. You must pay all undisputed invoices in full within 30 days of the invoice date, unless otherwise specified under the Special Terms and Conditions. All payments must reference the invoice number. Unless otherwise specified, all invoices shall be paid in the currency of the invoice</p>
                    <p class="small hint-text">Insight retains the right to decline to extend credit and to require that the applicable purchase price be paid prior to performance of Services based on changes in insight's credit policies or your financial condition and/or payment record. Insight reserves the right to charge interest of 1.5% per month or the maximum allowable by applicable law, whichever is less, for any undisputed past due invoices. You are responsible for all costs of collection, including reasonable attorneys' fees, for any payment default on undisputed invoices. In addition, Insight may terminate all further work if payment is not received in a timely manner.</p>
                    <br>
                    <hr>
                    <div>
                        <span class="text-black m-r-20 sm-pull-left">generated with</span>
                        <img src="{{ asset('app/img/logo.png') }}" alt="logo" data-src="{{ asset('app/img/logo.png') }}" data-src-retina="{{ asset('app/img/logo.png') }}" width="78" height="22">
                        <span class="m-l-70 text-black sm-pull-right">+34 346 4546 445</span>
                        <span class="m-l-40 text-black sm-pull-right">support@ikooba.com</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->
</div>
<!-- END PAGE CONTENT -->
@stop