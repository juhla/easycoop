<?php

use App\Modules\Base\Traits\Acl;

?>
@extends('layouts.main')

@section('content')
    @include('flash::message')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container" style="padding-right:50px;background:#CCCCCC">
        <header class="header b-b b-light hidden-print">
            <div class="col-md-10">
                <h4 style="font-weight: bolder">Invoice</h4>
            </div>
            <div class="col-md-6">
                @if(Acl::can(['print_invoice']))
                    <button href="#" id="print_btn" class="btn btn-sm btn-info ">Print</button>

                <form action="{{ url('invoices/download')}}" method="post">
                    <input type="hidden" name="csrf" value="{{csrf_token()}}"> 
                    <input type="hidden" name="invoice_id" value="{{$invoice->id}}"> 
                    <input type="submit" name="submit" value="Download as PDF" class="btn btn-sm btn-info">
                </form>
                @endif

                @if(Acl::can(['send_email']))
                    <a href="{{ url('invoices/send/pdf/mail/'.$invoice->id ) }}" class="btn btn-sm btn-default">
                        <i
                                class="fa fa-plus"></i> Send as Mail </a>
                @endif
                @if(empty(getBusinessOwnerAuth()->company->company_logo))
                    <a href="{{ url('profile/'.strtolower(auth()->user()->displayName()).' #company-logo') }}"
                       class="btn btn-sm btn-default"><i
                                class="fa fa-plus"></i> upload company logo </a>
                @endif
            </div>

        </header>
    </div>


    <div style="background:#ccc;padding:200px;padding-top:100px">
        <div style="padding:50px;background:#fff" id="printTable">

            <div class="row">

                <div>

                </div>

                <div class="col-md-2" style="display:flex;justify-content:;align-items:center">
                    @if(!empty(getBusinessOwnerAuth()->company->company_logo))
                        <img src="{{asset("uploads/".getBusinessOwnerAuth()->company->company_logo)}}" width="60px"
                             height="60px">
                    @endif
                </div>
                <div class="col-md-10"
                     style="display:flex;flex-direction:row;justify-content:flex-end;align-content:flex-end">

                    <p class="">
                        <span style="font-weight:bolder;font-size:20px"> {{ getBusinessOwnerAuth()->company->company_name }}</span>
                        <br>
                        <a href="{{ getBusinessOwnerAuth()->company->website }}">{{ getBusinessOwnerAuth()->company->website }}</a>
                        <br>
                        {{ getBusinessOwnerAuth()->company->address }}
                        <br>
                        {{ getBusinessOwnerAuth()->company->city }}
                        , {{ getBusinessOwnerAuth()->company->country_data->name }}
                        <br>
                        {{--{{ getCountry(getBusinessOwnerAuth()->company->country) }}--}}
                        <span style="font-weight: bolder"> Telephone: {{ getBusinessOwnerAuth()->company->phone_no }}</span>

                    </p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr style="border:0.5px solid #ccc">
                    </div>
                </div>
            </div>


            <div class="row">


                <div class="col-md-6">
                    <address>
                        <strong>Billed To:</strong><br>
                        <h4 style="font-weight: bolder">{{ $invoice->customer->name }}</h4>
                        <address>{{ $invoice->customer->address }}</address>
                        <br>
                    </address>

                </div>
                <div class="col-md-6"
                     style="display:flex;flex-direction:row;justify-content:flex-end;align-content:flex-end">
                    <address style="font-family:Open Sans">
                        <span style="font-weight:bolder;font-size:18px">  #{{ $invoice->invoice_no }}</span>
                        <h5>{{ date('jS M, Y', strtotime( $invoice->invoice_date )) }}</h5>
                    </address>
                </div>
            </div>

            <div class="row">

                <table border="0" width="100%">
                    <tr style="background:#CCCCCC;">
                        <td style="padding-left:15px;font-family:Open Sans;font-weight:bolder;font-size:18px">Summary
                        </td>
                    </tr>
                </table>

                <table border="0" width="100%">
                    <tr style="background:hsl(0, 0%, 90%) ">
                        <td style="font-weight: bolder"> &nbsp;&nbsp;Description</td>
                        <td>&nbsp; &nbsp;</td>
                        <td style="font-weight: bolder;text-align:right;">Amount &nbsp;&nbsp;</td>
                    </tr>

                    @foreach( $invoiceDet as $item )
                        <tr style="background:hsl(0, 0%, 90%) ">
                            <td>&nbsp;&nbsp; {{ $item->description  }}</td>
                            <td>&nbsp;&nbsp;</td>
                            <td style="text-align: right">{{ formatNumber($item->amount )}} &nbsp;&nbsp;</td>

                        </tr>
                    @endforeach

                    <tr style="background:hsl(0, 0%, 90%) ">
                        <td>&nbsp;&nbsp;</td>
                        <td style="text-align: right;font-weight: bolder">Amount</td>
                        <td style="text-align: right">{{ formatNumber($invoice->amount_due) }} &nbsp;&nbsp;</td>

                    </tr>


                    {{-- display vat if value is 1 --}}

                    <?php $vatshow = $invoice->vat; ?>
                    @if(  $invoice->vat == 1 )
                        <tr style="background:hsl(0, 0%, 90%) ">
                            <td>&nbsp;&nbsp;</td>
                            <td style="text-align: right;font-weight: bolder">VAT</td>
                            <td style="text-align: right">


                                {{ formatNumber($invoice->total_amount - $invoice->amount_due) }}

                                &nbsp;&nbsp;
                            </td>

                        </tr>

                    @endif

                    {{-- /display vat if value is 1 --}}


                    <tr style="background:#CCCCCC;">
                        <td>&nbsp;&nbsp;</td>
                        <td style="text-align: right;font-weight: bolder"><br/>Total({{ $invoice->currency }} )</td>
                        <td style="text-align: right"><br/>{{ formatNumber($invoice->total_amount) }} &nbsp;&nbsp;</td>

                    </tr>

                    <tr style="background:#CCCCCC;">
                        <td colspan="3">
                            <hr style="border:1px #ccc solid">
                        </td>
                    </tr>

                    <tr style="">
                        <td colspan="3">
                            <hr style="border:1px #ccc solid">
                            <ul style="list-style: none;margin-left:-25px">
                                <li>
                                    <span style="color:#373636;font-weight: bolder">Account name:&nbsp;</span>{{@$accountNumber->account_name}}
                                </li>
                                <li>
                                    <span style="color:#373636;font-weight: bolder">Account no:&nbsp;</span>{{@$accountNumber->account_number}}
                                </li>
                                <li>
                                    <span style="color:#373636;font-weight: bolder">Bank name:&nbsp;</span>{{@$accountNumber->bank}}
                                </li>
                            </ul>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>











    <!-- the js function to print the invoice table -->
    <script type="text/javascript">


        function printData() {
            var divToPrint = document.getElementById("printTable");
            newWin = window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        }

        $('#print_btn').on('click', function () {
            printData();
        })
    </script>





    <!-- /the js function to print the invoice table -->




    </div>

@stop








