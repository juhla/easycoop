<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

Route::group(['prefix' => 'invoices', 'middleware' => ['web', 'auth:web']], function () {
    Route::any('/sales-invoice', 'InvoiceController@salesInvoice');
    Route::get('/purchase-invoice', 'InvoiceController@purchaseInvoice');
    Route::get('/sales-invoice/add', 'InvoiceController@displaySalesInvoiceForm');
    Route::get('/purchase-invoice/add', 'InvoiceController@displayPurchaseInvoiceForm');
    Route::post('/store', 'InvoiceController@store');
    Route::get('/view/{id}', 'InvoiceController@view');
    Route::get('/edit/{id}', 'InvoiceController@edit');
    Route::get('/delete/{id}', 'InvoiceController@delete');
    Route::get('/send/pdf/mail/{id}', 'InvoiceController@sendPdfMail');
    Route::post('/download', 'InvoiceController@download');

    Route::get('/mailable', function () {
        return new App\Modules\Base\Mail\SendInvoice(10);
    });
});
