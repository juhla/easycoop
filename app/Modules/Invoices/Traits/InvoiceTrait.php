<?php

namespace App\Modules\Invoices\Traits;


use App\Modules\Base\Models\Invoice;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Traits\DebitCredit;
use App\Modules\Invoices\Models\InvoiceItem;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

trait InvoiceTrait
{
    use DebitCredit;

    protected $isInvoiceError = false;
    protected $errorArray;
    protected $InvoiceErrorMessage = [];
    protected $eachInvoice;
    protected $eachInvoiceItem;
    protected $savedInvoice;
    protected $InvoiceItem = [];
    public $payment_type = ["cash", "credit", 'bank'];

    /**
     * @return mixed
     */
    public function getInvoiceItem()
    {
        return $this->InvoiceItem;
    }

    /**
     * @param mixed $transactionItem
     */
    public function setInvoiceItem($transactionItem)
    {
        $this->InvoiceItem[] = $transactionItem;
    }


    public function flattenedError($glue = ' ')
    {
        return implode($glue, Arr::flatten($this->getErrorMessage()));
    }


    function InvoicePosting($transactionID = null)
    {

        $input = $this->eachInvoice->toArray();
        //dd($input);
        $reference_no = $input['reference_no'];
        $paymentType = $input['paymentType'];
        $bank = $input['bank_invoice'];
        $invoice_no = $input['invoice_no'];
        $payment_type = $input['payment_type'];
        $total = $input['total_amount'];
        $totalAmount = $total * 1.05;
        $subTotalAmount = $total;
        $vatAmount = $total * 0.05;
        $description = $input['memo'];
        //dd($input['invoice_date']);
        //$date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['invoice_date']);
        $date = $input['invoice_date'];
        $isVat = (@$input['vat'] ? true : false);


        // dd($input);
        if ($isVat) {

            $trans_item[] = [
                'ledger_id' => getLedgerId('1211.01'),
                'amount' => $vatAmount,
                'item_description' => $description,
                'dc' => 'C'
            ]; // Credit VAT payable with VAT Amount


            $trans_item[] = [
                'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                'amount' => $totalAmount,
                'item_description' => $description,
                'dc' => 'D'
            ];
            // Debit Customer

            $trans_item[] = [
                'ledger_id' => getLedgerId($input['income_type']),
                'amount' => $subTotalAmount,
                'item_description' => $description,
                'dc' => 'C',
            ];

            // Credit Sales


            if ($input['payment_type'] != 'credit') {
                $trans_item[] = [
                    'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                    'amount' => $totalAmount,
                    'item_description' => $description,
                    'dc' => 'C'
                ];
                // Credit Customer
            }

            if ($input['payment_type'] == 'cash') {

                $trans_item[] = [
                    'ledger_id' => getLedgerId(1261),
                    'amount' => $totalAmount,
                    'item_description' => $description,
                    'dc' => 'D',
                ];
                // Debit Cash
            } else if ($input['payment_type'] == 'bank') {

                $trans_item[] = [
                    'ledger_id' => $input['bank_id'],
                    'amount' => $totalAmount,
                    'item_description' => $description,
                    'dc' => 'D',
                ];
                // Debit Bank

            }
        } else {


            $trans_item[] = [
                'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                'amount' => $total,
                'item_description' => $description,
                'dc' => 'D'
            ];

            // Debit Customer
            $trans_item[] = [
                'ledger_id' => getLedgerId($input['income_type']),
                'amount' => $total,
                'item_description' => $description,
                'dc' => 'C',
            ];

            // Credit Sales

            if ($input['payment_type'] != 'credit') {
                $trans_item[] = [
                    'ledger_id' => getLedgerIdForCustomer($input['customer_id']),
                    'amount' => $total,
                    'item_description' => $description,
                    'dc' => 'C'
                ];
                // Credit Customer
            }

            if ($input['payment_type'] == 'cash') {


                $trans_item[] = [
                    'ledger_id' => getLedgerId(1261),
                    'amount' => $total,
                    'item_description' => $description,
                    'dc' => 'D',
                ];
                // Debit Cash
            } else if ($input['payment_type'] == 'bank') {

                $trans_item[] = [
                    'ledger_id' => $input['bank_id'],
                    'amount' => $total,
                    'item_description' => $description,
                    'dc' => 'D',
                ];
                // Debit Bank

            }
        }

        if ($input['payment_type'] == 'cash') {
            $paymentType = 1;
        } else if ($input['payment_type'] == 'credit') {
            $paymentType = 2;
        } else {
            $paymentType = 3; //bank
        }


        $transaction = [
            'amount' => $total,
            'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
            'transaction_type' => 'invoice',
            'reference_no' => $reference_no,
            'payment_type' => $paymentType,
            'transaction_date' => $date,
            'bank_id' => $bank,
            'description' => $description,
        ];


        $formatted = new Transaction($transaction);
        if (!empty($transactionID)) {
            $formatted->id = $transactionID;
            $formatted->updated_by = Auth::user()->id;
        }
        foreach ($trans_item as $eachTransaction) {
            $transactionItem[] = new TransactionItem($eachTransaction);
        }
        $formatted->item = $transactionItem;

        //dd($formatted);
        $this->makePosting($formatted);

    }

    function makeInvoicePosting($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        };
        $count = 0;
        foreach ($data as $eachArray) {
            $count++;
            if (!empty($eachArray->id)) {
                $this->editInvoicePosting($eachArray);
                continue;
            }
            $errorArray = ":: Index [ " . $count . " ]";
            $this->eachInvoice = $eachArray;
            $this->errorArray = $errorArray;
            $this->handleInvoice();
            if (!$this->getisError()) {
                try {

                    DB::connection('tenant_conn')->beginTransaction();
                    $this->eachInvoice->created_by = Auth::user()->id;
                    // Invoice


                    $invoice = $this->eachInvoice->toArray();
                    unset($invoice['payment_type']);
                    $Invoice = new Invoice($invoice);
                    $Invoice->save();


                    // Invoice Items
                    $InvoiceItems = $Invoice->item();
                    $InvoiceItems->saveMany($this->InvoiceItem);
                    $this->savedInvoice = $Invoice;


                    $this->InvoicePosting();
                    if ($this->isError) {
                        $this->setIsError($this->isError);
                        $this->setErrorMessage($this->DebitCreditErrorMessage);
                        return;
                    }
                    $Invoice->transaction_id = $this->savedTransaction->id;
                    $Invoice->save();
                    $this->isInvoiceError = false;
                    DB::connection('tenant_conn')->commit();
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isInvoiceError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                } catch (\Throwable $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isInvoiceError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                }
            }
        }
        // dd($this->getisError(), $this->InvoiceErrorMessage);
    }

    function editInvoicePosting($data)
    {
        if (!is_array($data)) {
            $data = [$data];
        };

        $count = 0;
        foreach ($data as $eachArray) {
            $count++;
            $errorArray = ":: Index [ " . $count . " ]";


            $this->eachInvoice = $eachArray;
            $this->errorArray = $errorArray;
            $this->validateEditInvoice();
            $this->cleanEditInvoiceData();
            $this->handleInvoice();


            if (!$this->getisError()) {
                try {

                    DB::connection('tenant_conn')->beginTransaction();
                    // Invoice

                    $Invoice = Invoice::find($this->eachInvoice->id);
                    $Invoice->updated_by = Auth::user()->id;

                    $Invoice->invoice_no = $this->eachInvoice->invoice_no;
                    $Invoice->customer_id = $this->eachInvoice->customer_id;
                    $Invoice->invoice_type = $this->eachInvoice->invoice_type;
                    $Invoice->invoice_date = $this->eachInvoice->invoice_date;
                    $Invoice->due_date = $this->eachInvoice->due_date;
                    $Invoice->amount_due = $this->eachInvoice->amount_due;
                    $Invoice->total_amount = $this->eachInvoice->total_amount;
                    $Invoice->amount_paid = $this->eachInvoice->amount_paid;
                    $Invoice->memo = $this->eachInvoice->memo;
                    $Invoice->payment_term_id = $this->eachInvoice->payment_term_id;
                    $Invoice->currency = $this->eachInvoice->currency;
                    $Invoice->status = $this->eachInvoice->status;
                    $Invoice->flag = $this->eachInvoice->flag;
                    $Invoice->vat = $this->eachInvoice->vat;
                    $Invoice->bank_invoice = $this->eachInvoice->bank_invoice;
                    $Invoice->income_type = $this->eachInvoice->income_type;
                    $Invoice->update();


                    $InvoiceItems = $Invoice->item();
                    $InvoiceItems->forceDelete();
                    $InvoiceItems->saveMany($this->InvoiceItem);
                    $this->savedInvoice = $Invoice;

                    $this->InvoicePosting($Invoice->transaction_id);

                    if ($this->isError) {
                        $this->setIsError($this->isError);
                        $this->setErrorMessage($this->DebitCreditErrorMessage);
                        return;
                    }
                    //$Invoice->transaction_id = $this->savedTransaction->id;
                    $Invoice->save();
                    $this->isInvoiceError = false;
                    DB::connection('tenant_conn')->commit();
                } catch (\Exception $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isInvoiceError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                } catch (\Throwable $exception) {
                    DB::connection('tenant_conn')->rollBack();
                    $this->isInvoiceError = true;
                    $this->setErrorMessage($exception->getMessage() . " " . $errorArray);
                }
            }
        }
    }

    function cleanEditInvoiceData()
    {
        $Invoice = Invoice::find($this->eachInvoice->id);
        $result = array_merge(
            $Invoice->toArray(), $this->eachInvoice->toArray());


        $InvoiceItem = $result['item'];
        $Invoice = new Invoice($result);
        $Invoice->item = $InvoiceItem;
        if (empty($Invoice->invoice_no)) {
            $Invoice->invoice_no = 'TRA' . time();
        }

        $this->eachInvoice = $Invoice;

    }

    function validateEditInvoice()
    {
        $eachArray = $this->eachInvoice;
        $errorArray = $this->errorArray;
        $rules = [
            'id' => [
                'required',
                'exists:tenant_conn.ca_invoice,id',
            ]
        ];
        $validator = Validator::make($eachArray->toArray(), $rules);
        if ($validator->fails()) {
            $this->isInvoiceError = true;
            $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
            return;
        }
    }

    function cleanInvoiceData()
    {
        $result = $this->eachInvoice;

        if (empty($result->invoice_no)) {
            $result->invoice_no = 'TRA' . time();
        }
        if ($result->payment_type == 'cash') {
            $result->payment_term_id = 1;
        } else if ($result->payment_type == 'credit') {
            $result->payment_term_id = 2;
        } else {
            $result->payment_term_id = 3; //bank
        }

        if (empty($result->amount_due)) {
            $result->amount_due = '0';
        }

        if (empty($result->invoice_type)) {
            $result->invoice_type = 'sales';
        }

        if (empty($result->total_amount)) {
            $result->total_amount = '0';
        }
        if (empty($result->amount_paid)) {
            $result->amount_paid = '0';
        }

        if (empty($result->currency)) {
            $result->currency = '';
        }
        if (empty($result->status)) {
            $result->status = '0';
        }
        if (empty($result->vat)) {
            $result->vat = '0';
        }
        $this->eachInvoice = $result;
    }

    function cleanInvoiceItemData()
    {
        $result = $this->eachInvoiceItem;
        if (empty($result->product_id)) {
            $result->product_id = '0';
        }
        if (empty($result->tax)) {
            $result->tax = '0';
        }
        if (empty($result->quantity)) {
            $result->quantity = '0';
        }
        if (empty($result->price)) {
            $result->price = '0';
        }
        $this->eachInvoiceItem = $result;
    }

    function handleInvoice()
    {

        $result = $this->eachInvoice;

        $errorArray = $this->errorArray;
        /***
         * Invoice Model
         */

        if (!($result instanceof Invoice)) {
            $this->isInvoiceError = true;
            $this->setErrorMessage("The data is not an object of Invoice " . $errorArray);
            return;
        }
        $this->cleanInvoiceData(); // Clean the Data For Saving
        $result = $this->eachInvoice;


        $bank_invoice = $result->toArray()['bank_invoice'];

        //dd($result->toArray());
        $rules = [
            'invoice_no' => 'required',
            'payment_type' => 'required|in:' . implode(',', $this->payment_type),
            'bank_invoice' => [
                'required',
                'exists:tenant_conn.ca_ledgers,id',
                Rule::exists('tenant_conn.ca_ledgers', 'id')
                    ->where(function ($query) use ($bank_invoice) {
                        $query->where('id', $bank_invoice)
                            ->where('group_id', '16');
                    }),
            ],
            'income_type' => [
                'required',
                'exists:tenant_conn.ca_ledgers,code'
            ],
            'customer_id' => [
                'required',
                'exists:tenant_conn.ca_customers,id',
            ],

            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'amount_due' => 'required|numeric',
            'total_amount' => 'required|numeric',
            'amount_paid' => 'required|numeric',
            'memo' => 'required',
            'payment_term_id' => 'required',

        ];
        $validator = Validator::make($result->toArray(), $rules);
        //if validation fails, redirect with errors
        if ($validator->fails()) {
            $this->isInvoiceError = true;
            $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
            return;
        }

        $this->eachInvoice = $result;

        /***
         * Invoice Items Model
         * index = Invoice_items
         */

        //Invoice_items


        // Check if Object is Empty or is not an Array
        if (!empty($result->item) && is_array($result->item)) {

            $debitLeg = $creditLeg = 0;
            foreach ($result->item as $eachItems) {
                //pr($eachItems);
                if (!($eachItems instanceof InvoiceItem)) {
                    $this->isInvoiceError = true;
                    $this->setErrorMessage("The data is not an object of Invoice Item " . $errorArray);
                    return;
                }

                $rules = [
                    'product_id' => 'required',
                    'description' => 'required',
                    'quantity' => 'required',
                    'amount' => 'required|numeric',
                    'tax' => 'required|numeric',
                    'price' => 'required|numeric'

                ];


                $this->eachInvoiceItem = $eachItems;
                $this->cleanInvoiceItemData(); // Clean the Data For Saving
                $eachItems = $this->eachInvoiceItem;


                $validator = Validator::make($eachItems->toArray(), $rules);
                //if validation fails, redirect with errors
                if ($validator->fails()) {
                    $this->isInvoiceError = true;
                    $this->setErrorMessage($this->response($validator->errors()->all()) . " " . $errorArray);
                    return;
                }

                //$eachItems->sync = "0";
                $this->setInvoiceItem($eachItems);
            }
            $this->isInvoiceError = false;
            return;
        }

        $this->isInvoiceError = true;
        $this->setErrorMessage("Invoice Item is not properly formatted in the data , .. or it doesnt Exist" . " " . $errorArray);
        return;


    }

    function testAccount()
    {

        $formatted = new Invoice;
        $formatted->id = '74';
        $formatted->invoice_no = 'INV1568987633';
        $formatted->customer_id = '5c769ca0-9e5e-11e9-8bd0-dd95eff1e82e';
        $formatted->invoice_type = 'sales';
        $formatted->invoice_date = '2019-09-20';
        $formatted->due_date = "2019-09-20";
        $formatted->amount_due = "2000";
        $formatted->total_amount = "2000";
        $formatted->amount_paid = "2000";
        $formatted->memo = "SALES";
        $formatted->payment_term_id = "1";
        $formatted->currency = "";
        $formatted->status = "0";
        $formatted->vat = "0";
        $formatted->bank_invoice = "18";
        $formatted->income_type = "4110";
        $formatted->payment_type = "cash";
        // $formatted->Invoice_id = "first Invoice";
        // $formatted->created_by = "first Invoice";
        // $formatted->updated_by = "first Invoice";


        $InvoiceItem = new InvoiceItem;
        $InvoiceItem->product_id = "";
        $InvoiceItem->description = "D";
        $InvoiceItem->amount = "2000";

        $formatted->item = [$InvoiceItem];

        $this->makeInvoicePosting($formatted);
        dd($this->getisError(), $this->InvoiceErrorMessage);
    }

    /**
     * @param mixed $errorMessage
     */
    public function setErrorMessage($errorMessage): void
    {
        $this->InvoiceErrorMessage[] = $errorMessage;
    }

    public function response(array $errors)
    {
        return $messages = implode(' ', Arr::flatten($errors));
    }

    /**
     * @return mixed
     */
    public function getisError()
    {
        return $this->isInvoiceError;
    }

    /**
     * @param mixed $isError
     */
    public function setIsError($isError): void
    {
        $this->isInvoiceError = $isError;
    }

    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->InvoiceErrorMessage;
    }
}

