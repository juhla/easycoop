<?php

namespace App\Modules\Journal\Http\Controllers;

use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\TrialBalance;
use App\Modules\Journal\DataTables\JournalDataTable;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Journal\Models\Transaction;
use App\Modules\Journal\Models\TransactionItem;
use App\Modules\Journal\Models\Journal;
use Carbon;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class JournalController
 * @package Ikooba\Modules\Accounting\Modules\Journal\Controllers
 */
class JournalController extends Controller
{
    use TrialBalance, Balance;

    /**
     * JournalController constructor.
     * @param Request $request
     * @param Journal $journal
     */
    public function __construct(Request $request, Journal $journal)
    {
        $this->middleware(['tenant']);

        $this->request = $request;
        $this->journal = $journal;
    }

    /**
     * method to display all journal entry
     * @return $this
     */

    public function index(JournalDataTable $table)
    {

        //dd($fiscalYear);
        $inputs = request()->all();
        $this->checkIsBalanced();
        $this->hasAccess(['view_journal_entry', 'add_journal_entry']);
        $title = 'Journal Entry';
        $type = 'journal';

        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');

        $table->setType($type);
        //$table->setType($type);
        return $table->render('journal::index', compact('type', 'title', 'start_date', 'end_date'));

    }

    // Display form for Journal Entry
    public function newJournal()
    {
        $this->hasAccess(['add_journal_entry', 'view_journal_entry']);
        $data['title'] = 'New Journal Entry';
        return view('journal::journal_form')->with($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function save()
    {

        $this->hasAccess(['add_journal_entry']);
        $input = request()->all();

        $verifyDate = Transaction::verifyTransactionDate($input['transaction_date']);
        if ($verifyDate === 'yes') {

            $result = new Journal();
            $result->saveTransaction($input);
            if ($result->getisError()) {
                return response()->json(['message' => $result->getErrorMessage(), 'statusCode' => 500]);
            }
            return response()->json(['message' => 'Journal successfully saved.', 'statusCode' => 200]);
        } elseif ($verifyDate === 'no') {
            if (request()->ajax()) {
                return response()->json(['statusCode' => 500, 'message' => 'Transaction date must be between current Fiscal Year']);
            } else {
                flash()->error('Transaction date must be between current Fiscal Year');
                return redirect()->back();
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['statusCode' => 500, 'message' => 'Please setup a Fiscal Year before entering transactions']);
            } else {
                flash()->success('Please setup a Fiscal Year before entering transactions');
                return redirect()->back();
            }
        }

    }

    public function edit($id = '0')
    {
        $this->hasAccess(['edit_journal_entry']);

        $id = base64_decode($id);
        //pr($id);
        $journal = Transaction::where('id', $id)->with('item')->first();
        //dd($journal);exit;
        if ($journal) {
            $data['title'] = 'Edit Journal';
            $data['journal'] = $journal;
            return view('journal::edit_journal')->with($data);
        }

        return redirect('/journal-entry');

    }



    public function update()
    {

        $this->hasAccess(['edit_journal_entry']);
        $input = $this->request->all();
        $result = new Journal();
        $result->updateTransaction($input);
        if ($result->getisError()) {
            return response()->json(['message' => $result->getErrorMessage(), 'statusCode' => 500]);
        }
        return response()->json(['message' => 'Journal saved successfully.', 'statusCode' => 200]);

    }

    public function delSingle($id)
    {
        $this->hasAccess(['delete_journal_entry']);
        if ($this->journal->delJournalSingle($id)) {
            return response()->json(['message' => 'Journal entry deleted successfully.', 'statusCode' => 200]);
        } else {
            return response()->json(['message' => 'Could not delete this entry.', 'statusCode' => 500]);
        }

    }

    public function delete()
    {
        $this->hasAccess(['delete_journal_entry']);
        $input = $this->request->only('ids');
        if ($this->journal->deleteJournal($input)) {
            return response()->json(['message' => 'Journal was successfully deleted.', 'statusCode' => 200]);
        } else {
            return response()->json(['message' => 'Could not delete journal at this moment.', 'statusCode' => 500]);
        }

    }

}
