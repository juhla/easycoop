<?php

namespace App\Modules\Journal\Models;

use Auth;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Journal extends Model implements AuditableContract
{
    use NullingDB;
    use Auditable;
    protected $connection = 'tenant_conn';

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public static function getTransaction($item_id = null, $start_date = null, $end_date = null)
    {
        if ($item_id) {
            $res = Transaction::where('id', $item_id)
                ->first();
        } elseif (!is_null($start_date)) {
            $start = Carbon\Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            if (is_null($end_date)) {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            } else {
                $end = Carbon\Carbon::createFromFormat('d/m/Y', $end_date);
                $end = $end->format('Y-m-d');
            }
            $res = Transaction::with('item')->where('transaction_type', 'journal')
                ->where('flag', 'Active')
                ->whereBetween('transaction_date', [$start, $end])
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        } else {
            $res = Transaction::with('item')->whereDate('created_at', '=', date('Y-m-d'))
                ->where('flag', 'Active')
                ->where('transaction_type', 'journal')
                ->orderBy('transaction_date', 'desc')
                ->paginate(30);
        }
        return $res;
    }

    /**
     * save journal
     * @param $input
     * @return Mixed
     */
    public static function saveTransaction($input)
    {

        //$rand = parent::random(1, 99, 1);
        $ref_no = 'RFJ' . time();

        // Total amount
        $tot_amt = 0;

        try {
            for ($i = 0; $i < count($input['ledger']); $i++) {
                $tot_amt += $input['debit'][$i];
            }

            $trans_info = Transaction::create([
                'transaction_date' => Carbon\Carbon::createFromFormat('d/m/Y', $input['transaction_date']),
                'description' => $input['item_description'][0],
                'amount' => $tot_amt,
                'transaction_type' => 'journal',
                'reference_no' => $ref_no,
                //'description'               => $input['description'],
                'pv_receipt_no' => $input['pcv'],
            ]);

            for ($i = 0; $i < count($input['ledger']); $i++) {
                if ($input['debit'][$i] > 0) {

                    $item = new TransactionItem;

                    $item->transaction_id = $trans_info->id;
                    $item->ledger_id = $input['ledger'][$i];
                    $item->amount = $input['debit'][$i];
                    $item->item_description = $input['item_description'][$i];
                    $item->dc = 'D';
                    $item->save();
                }

                if ($input['credit'][$i] > 0) {
                    $item = new TransactionItem;

                    $item->transaction_id = $trans_info->id;
                    $item->ledger_id = $input['ledger'][$i];
                    $item->amount = $input['credit'][$i];
                    $item->item_description = $input['item_description'][$i];
                    $item->dc = 'C';
                    $item->save();
                }
            }

        } catch (\Exception $e) {
            return $e;
        }
        return 200;
    }

    // Delete's multiple journal entries
    public function deleteJournal($input)
    {
        try {
            DB::beginTransaction();
            for ($i = 0; $i < count($input['ids']); $i++) {

                if ($trans = Transaction::find($input['ids'][$i])) {
                    $trans->delete();
                    TransactionItem::where('transaction_id', $trans->id)->delete();
                }
            }
        } catch (\Exception $e) {
            return false;
            DB::rollback();
        }
        return true;
        DB::commit();
    }

    //Delete's single journal entries
    public function delJournalSingle($id)
    {
        try {
            DB::beginTransaction();
            if ($trans = Transaction::find($id)) {
                $trans->delete();
                TransactionItem::where('transaction_id', $trans->id)->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
        DB::commit();
        return true;
    }

    // public static function random($min, $max, $length){
    //     $gen_num = '';
    //     $numbers = range($min, $max);
    //     shuffle($numbers);
    //     $sliced = array_slice($numbers, 0, $length );

    //     foreach($sliced as $s){
    //         $gen_num .= $s;
    //     }

    //     return $gen_num;
    // }
}
