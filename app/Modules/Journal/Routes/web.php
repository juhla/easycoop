<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'journal-entry', 'middleware' => ['web', 'auth:web', 'verify_license']], function() {

    Route::any('/', 'JournalController@index');
    Route::get('/add-new', 'JournalController@newJournal');
    Route::post('/save', 'JournalController@save');

    //
    Route::get('/edit/{id}', 'JournalController@edit');
    Route::post('/update', 'JournalController@update');
    Route::any('/delete/{id}', 'JournalController@delSingle');
    Route::any('/delete', 'JournalController@delete');

    // View export test
    Route::get('/export', 'JournalController@export');

});


