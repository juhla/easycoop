<?php

namespace App\Modules\Notebook\Http\Controllers;


use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Notebook\Models\AccountGroup;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use App\Modules\Notebook\Repositories\CashReceiptRepository;
use App\Modules\Notebook\Repositories\ChequePaymentRepository;
use App\Modules\Notebook\Repositories\ChequeReceiptRepository;
use App\Modules\Notebook\Repositories\CreditPurchaseRepository;
use App\Modules\Notebook\Repositories\CreditSalesRepository;
use App\Modules\Notebook\Repositories\PettyCashRepository;
use Illuminate\Support\Facades\Schema;

class NotebookController extends Controller
{

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    /**
     * display the index page of Non-cash Registers
     */
    public function indexOfNCR()
    {
        return view('notebook::ncr.index');
    }

    /**
     * -------------------------------------------------
     * --------- BEGIN CREDIT PURCHASE METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function creditPurchase()
    {
        //createInvoiceNo();
        $data['title'] = 'Credit Purchase';
        //check form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = CreditPurchase::whereInvoice_no($input['invoice_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['invoice_no'] = createInvoiceNo($data['title']);
            }

            $result = CreditPurchaseRepository::saveCP($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }

        //get all credit purchases
        $data['entries'] = CreditPurchase::where('status', 0)->get();

        return view('notebook::ncr.credit-purchase')->with($data);
    }


    /**
     * retrive credit purchase entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCP($id)
    {
        $entry = CreditPurchase::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete credit purchases
     * @param null $id
     */
    public function deleteCP($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                CreditPurchase::where('id', $id)
                    ->delete();
            }
        } else {
            CreditPurchase::where('id', $id)
                ->delete();
        }
    }

    /**
     * -------------------------------------------------
     * --------- BEGIN PETTY CASH METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function pettyCash()
    {

        $data['title'] = 'Cash Payment';
        //check form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = PettyCash::wherePvc_no($input['pvc_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['pvc_no'] = createPVCNo($data['title']);
            }

            $result = PettyCashRepository::savePC($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }
        //get all credit purchases
        $data['entries'] = PettyCash::where('status', 0)->get();

        return view('notebook::cr.petty-cash')->with($data);
    }

    /**
     * retrieve petty cash entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPC($id)
    {
        $entry = PettyCash::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete petty Cash
     * @param null $id
     */
    public function deletePC($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                PettyCash::where('id', $id)
                    ->delete();
            }
        } else {
            PettyCash::where('id', $id)
                ->delete();
        }
    }

    /**
     * END CREDIT PURCHASE METHODS
     */

    /**
     * -------------------------------------------------
     * --------- BEGIN CREDIT SALES FUNCTIONS -------
     * -------------------------------------------------
     * @return $this
     */
    public function creditSales()
    {
        $data['title'] = 'Credit Sales';
        //check if form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = CreditSales::whereInvoice_no($input['invoice_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['invoice_no'] = createInvoiceNo($data['title']);
            }

            $result = CreditSalesRepository::saveCS($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Entry has been saved.');
                return redirect()->back();
            }
        }
        //get all credit purchases
        $data['entries'] = CreditSales::where('status', 0)->get();

        return view('notebook::ncr.credit-sales')->with($data);
    }


    /**
     * retrieve credit sales entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCS($id)
    {
        $entry = CreditSales::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete credit sales
     * @param null $id
     */
    public function deleteCS($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                CreditSales::where('id', $id)
                    ->delete();
            }
        } else {
            CreditSales::where('id', $id)
                ->delete();
        }
    }


    /**
     * -------------------------------------------------
     * --------- BEGIN CHEQUE PAYMENT METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function chequePayment()
    {
        $data['title'] = 'Cheque Payment';
        //check if form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = ChequePayment::wherePv_no($input['pv_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['pv_no'] = createPVCNo($data['title']);
            }

            $result = ChequePaymentRepository::saveChqP($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }

        //get all credit purchases
        $data['entries'] = ChequePayment::where('status', 0)->get();

        return view('notebook::cr.cheque-payment')->with($data);
    }

    /**
     * retrieve cheque payment entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChqP($id)
    {
        $entry = ChequePayment::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete cheque payment
     * @param null $id
     */
    public function deleteChqP($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                ChequePayment::where('id', $id)
                    ->delete();
            }
        } else {
            ChequePayment::where('id', $id)
                ->delete();
        }
    }

    /**
     * -------------------------------------------------
     * --------- BEGIN CHEQUE RECEIPT METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function chequeReceipt()
    {
        $data['title'] = 'Cheque Receipt';
        //check if form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = ChequeReceipt::whereInvoice_no($input['invoice_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['invoice_no'] = createInvoiceNo($data['title']);
            }

            $result = ChequeReceiptRepository::saveChqR($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }

        //get all credit purchases
        $data['entries'] = ChequeReceipt::where('status', 0)->get();

        return view('notebook::cr.cheque-receipt')->with($data);
    }


    /**
     * retrieve cheque receipt entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChqR($id)
    {
        $entry = ChequeReceipt::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete petty Cash
     * @param null $id
     */
    public function deleteChqR($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                ChequeReceipt::where('id', $id)
                    ->delete();
            }
        } else {
            ChequeReceipt::where('id', $id)
                ->delete();
        }
    }

    /**
     * -------------------------------------------------
     * --------- BEGIN CASH RECEIPT METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function cashReceipt()
    {
        $data['title'] = 'Cash Receipt';
        //if form is submitted
        if ($input = request()->all()) {

            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = CashReceipt::whereInvoice_no($input['invoice_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['invoice_no'] = createInvoiceNo($data['title']);
            }

            $result = CashReceiptRepository::saveCR($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }
        //get all credit purchases
        $data['entries'] = CashReceipt::where('status', 0)->get();

        return view('notebook::cr.cash-receipt')->with($data);
    }


    /**
     * retrieve cheque receipt entry by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCR($id)
    {
        $entry = CashReceipt::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->date));
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    /**
     * method to delete petty Cash
     * @param null $id
     */
    public function deleteCR($id = null)
    {
        if (request()->input('ids')) {
            $ids = request()->input('ids');
            foreach ($ids as $id) {
                CashReceipt::where('id', $id)
                    ->delete();
            }
        } else {
            CashReceipt::where('id', $id)
                ->delete();
        }
    }
    /**
     * -------------------------------------------------
     * --------- BEGIN Bank RECEIPT METHODS ---------
     * -------------------------------------------------
     * @return $this
     */
    public function bankReceipt()
    {
        //$obj = new ChequeReceipt();
        $data['title'] = 'Bank Receipt';

        //check if form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = ChequeReceipt::whereInvoice_no($input['invoice_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['invoice_no'] = createInvoiceNo($data['title']);
            }

            $result = ChequeReceiptRepository::saveChqR($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }
        //get all credit purchases
        $data['entries'] = ChequeReceipt::where('status', 0)->get();
        return view('notebook::cr.money-in-bank-receipt')->with($data);
    }


    /* -------------------------------------------------
    * --------- BEGIN BANK PAYMENT METHODS ---------
    * -------------------------------------------------
    * @return $this
    */
    public function bankPayment()
    {
       // $obj = new ChequePayment();
        $data['title'] = 'Bank Payment';

        //check if form is submitted
        if ($input = request()->all()) {
            //remove commas from amount
            $input['amount'] = str_replace(',', '', $input['amount']);
            //change date format to mysql format
            $date = \Carbon\Carbon::createFromFormat('d/m/Y', $input['date']);
            $input['date'] = $date->format('Y-m-d');

            //test if transaction number is unique
            $result_set = ChequePayment::wherePv_no($input['pv_no'])->get();
            if (!$result_set->isEmpty()) {
                $input['pv_no'] = createPVCNo($data['title']);
                dd($input['pv_no']);
            }

            $result = ChequePaymentRepository::saveChqP($input);
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        }

        //get all credit purchases
        $data['entries'] = ChequePayment::where('status', 0)->get();
        return view('notebook::cr.money-out-bank-payment')->with($data);
    }


}