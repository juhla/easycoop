<?php

namespace App\Modules\Notebook\Models;

use App\Modules\Base\Events\AccountGroupCreated;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\Tree;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use Auth;
use Illuminate\Database\Eloquent\Builder;

class AccountGroup extends \App\Modules\Base\Models\AccountGroup
{ }
