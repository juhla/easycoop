<?php

namespace App\Modules\Notebook\Repositories;

use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Base\Models\Transaction;
use App\Modules\Base\Models\TransactionItem;

class CreditPurchaseRepository
{
    /**
     * save inflow
     * @param $input
     * @return Mixed
     */
    public static function saveCP($input){


        if($input['item_id'] === ''){

            $input['created_by'] = \Auth::user()->id;
            $entry = CreditPurchase::create($input);

            $res = '<tr><td><input type="checkbox" name="ids[]" value="'.$entry->id.'"/></td>
                                    <td>'.$entry->date.'</td>
                                    <td>'.$entry->invoice_no.'</td>
                                    <td>'.$entry->supplier.'</td>
                                    <td>'.$entry->description.'</td>
                                    <td>'.$entry->amount.'</td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <button type="button" class="btn btn-default edit" data-value="'.$entry->id.'"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger delete" data-value="'.$entry->id.'"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </td></tr>';
            return $res;
        }else{
            $id = $input['item_id'];
            unset($input['item_id']);
            unset($input['_token']);
            CreditPurchase::where('id', $id)->update($input);
            $entry = CreditPurchase::find($id);

            $res = '<td><input type="checkbox" name="ids[]" value="'.$entry->id.'"/></td>
                                    <td>'.$entry->date.'</td>
                                    <td>'.$entry->invoice_no.'</td>
                                    <td>'.$entry->supplier.'</td>
                                    <td>'.$entry->description.'</td>
                                    <td>'.$entry->amount.'</td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <button type="button" class="btn btn-default edit" data-value="'.$entry->id.'"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger delete" data-value="'.$entry->id.'"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </td>';
            return $res;
        }
    }

    /**
     * post credit purchase to gl
     * @param $input
     * @return Mixed
     */
    public static function postCP($input)
    {
        $ids = count($input["register_id"]);
        for($i=0;$i<$ids;$i++) {

            //get input fields
            $transaction = array(
                'transaction_date' => date('Y-m-d', strtotime($input['date'][$i])),
                'description' => $input['description'][$i],
                'amount' => $input['amount'][$i],
                'reference_no' => $input['reference_no'][$i]
            );

            $trans_item1 = [
                // 'ledger_id' => $input['debit_account'][$i],
                'ledger_id' => getTransactionIdByLedger($input['debit_account'][$i]),
                'amount' => $input['amount'][$i],
                'dc' => 'D',
            ];

            $trans_item2 = [
                // 'ledger_id' => $input['credit_account'][$i],
                'ledger_id' => getTransactionIdByLedger($input['credit_account'][$i]),
                'amount' => $input['amount'][$i],
                'dc' => 'C'
            ];

            $transaction['transaction_type'] = $input['transaction_type'][$i];
            $transaction['created_by'] = \Auth::user()->id;
            $item = Transaction::create($transaction);
            if($item){
                //update notebook status, set to posted.
                CreditPurchase::where('id', $input['register_id'][$i])->update(['status' => 1]);
            }

            //get transaction number
            $trans_item1['transaction_id'] = $item->id;
            $trans_item2['transaction_id'] = $item->id;
            //insert transaction item
            TransactionItem::create($trans_item1);
            TransactionItem::create($trans_item2);
        }
    }

}