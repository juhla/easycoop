@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">
                            {{--<a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">--}}
                            {{--<i class="fa fa-caret-right text fa-lg"></i>--}}
                            {{--<i class="fa fa-caret-left text-active fa-lg"></i>--}}
                            {{--</a>--}}
                            {{--<div class="btn-group">
                                <button type="button" class="btn btn-sm btn-default" title="Refresh"><i class="fa fa-refresh"></i></button>
                                <button type="button" class="btn btn-sm btn-default" title="Remove"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>--}}


                            @if(auth()->user()->hasRole(['assemblyline-professional', 'assemblyline-professional-agent']))
                                <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                        disabled><i class="fa fa-save"></i>
                                    Post to GL
                                </button>
                            @endif


                            @if(auth()->user()->hasRole(['professional']))
                                @if(canEdit())
                                    <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                            disabled><i class="fa fa-save"></i>
                                        Post to GL
                                    </button>
                                @endif
                            @endif
                            @if(!auth()->user()->hasRole(['professional']))
                                @if(canEdit())

                                    <a href="javascript:;" class="btn btn-sm btn-primary add-new">
                                        <i class="fa fa-plus"></i> Add New
                                    </a>
                                @endif
                            @endif

                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('notebook::cr.partials._cash-receipt')

                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr align="center">
                                    <th width="1%"><input type="checkbox" id="checkAll"/></th>
                                    <th width="10%">Date</th>
                                    <th width="10%">Invoice No</th>
                                    <th width="15%">Customer</th>
                                    <th width="19%">Description</th>
                                    <th width="20%">Bank Lodged</th>
                                    <th width="15%">Amount</th>
                                    <th width="10%"></th>
                                </tr>
                                </thead>
                                <form action="{{ url('transactions/registers/cash-receipt') }}" method="post"
                                      id="post-register">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <tbody id="entries">
                                    @if(count($entries) > 0)
                                        @foreach($entries as $entry)
                                            <tr class="edit-row" data-value="{{$entry->id}}">
                                                <td>
                                                    @if($entry->status === 0)
                                                        <input type="checkbox" name="ids[]" id="ids"
                                                               value="{{$entry->id}}"/>
                                                    @endif
                                                </td>
                                                <td>{{ $entry->date}}</td>
                                                <td>{{ $entry->invoice_no}}</td>
                                                <td>{{ $entry->customer }}</td>
                                                <td>{{ $entry->description }}</td>
                                                <td>{{ $entry->bank }}</td>
                                                <td>{{ formatNumber($entry->amount) }}</td>
                                                <td>
                                                    @if(canEdit())
                                                        <div class="btn-group btn-group-xs">
                                                            <button type="button" class="btn btn-default edit"
                                                                    data-value="{{ $entry->id }}"><i
                                                                        class="fa fa-edit"></i></button>
                                                            <button type="button" class="btn btn-danger delete"
                                                                    data-value="{{ $entry->id }}"><i
                                                                        class="fa fa-trash-o"></i></button>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="no-income">
                                            <td colspan="9">No cash receipt has been registered.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {{--<div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>--}}
                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/cash-receipt.js') }}" type="text/javascript"></script>
@stop
