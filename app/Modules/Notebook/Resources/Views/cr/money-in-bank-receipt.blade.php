@extends('layouts.main')

@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">

                            @if(auth()->user()->hasRole(['assemblyline-professional', 'assemblyline-professional-agent']))
                                <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                        disabled><i class="fa fa-save"></i>
                                    Post to GL
                                </button>
                            @endif


                            @if(auth()->user()->hasRole(['professional']))
                                @if(canEdit())
                                    <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                            disabled><i class="fa fa-save"></i>
                                        Post to GL
                                    </button>
                                @endif
                            @endif
                            @if(!auth()->user()->hasRole(['professional']))
                                @if(canEdit())

                                    <a href="javascript:;" class="btn btn-sm btn-primary add-new">
                                        <i class="fa fa-plus"></i> Add New
                                    </a>
                                @endif
                            @endif

                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('notebook::cr.partials._bank-receipt')

                    <section class="panel panel-default">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr align="center">
                                    <th><input type="checkbox" id="checkAll"/></th>
                                    <th>Date</th>
                                    <th>Invoice No</th>
                                    <th>Bank Lodged</th>
                                    <th>Customer</th>
                                    <th>Issue Bank</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <form action="{{ url('transactions/registers/cheque-receipt') }}" method="post"
                                      id="post-register">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <tbody id="entries">
                                    @if(count($entries) > 0)
                                        @foreach($entries as $entry)
                                            <tr class="edit-row" data-value="{{$entry->id}}">
                                                <td>
                                                    @if($entry->status === 0)
                                                        <input type="checkbox" name="ids[]" id="ids"
                                                               value="{{$entry->id}}"/>
                                                    @endif
                                                </td>
                                                <td>{{ $entry->date}}</td>
                                                <td>{{ $entry->invoice_no}}</td>
                                                <td>{{ $entry->bank_lodged }}</td>
                                                <td>{{ $entry->customer }}</td>
                                                <td>{{ $entry->issue_bank }}</td>
                                                <td>{{ $entry->description }}</td>
                                                <td>{{ formatNumber($entry->amount) }}</td>
                                                <td>
                                                    @if(canEdit())
                                                        <div class="btn-group btn-group-xs">
                                                            <button type="button" class="btn btn-default edit"
                                                                    data-value="{{ $entry->id }}"><i
                                                                        class="fa fa-edit"></i></button>
                                                            <button type="button" class="btn btn-danger delete"
                                                                    data-value="{{ $entry->id }}"><i
                                                                        class="fa fa-trash-o"></i></button>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="no-income">
                                            <td colspan="10">No cheque receipt has been registered.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/cheque-receipt.js') }}" type="text/javascript"></script>
@stop
