<div class="row">
    <div class="col-sm-12" id="displayForm" style="display: none">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter New Cheque Payment </header>
            <form role="form" action="{{ route('cheque-payment')  }}" method="post" id="chequePaymentForm" data-validate="parsley">
                <section class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" data-required>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>PV No</label>
                                <input type="text" class="form-control"  value="{{createPVCNo('Cheque Payment') }}" readonly name="pv_no" id="pv_no" data-required>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-default">
                                <label>Cheque No</label>
                                <input type="text" class="form-control" name="cheque_no" id="cheque_no" data-required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Bank</label>
                                <input type="text" class="form-control" name="bank" id="bank" data-required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="">Payee</label>
                                <input type="text" class="form-control" name="supplier" id="supplier" data-required>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="description" id="description">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-type="number" data-required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" id="row_index" value="">
                            <button type="reset" class="btn btn-default m-t-5 close-modal">Close</button>
                            <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>

