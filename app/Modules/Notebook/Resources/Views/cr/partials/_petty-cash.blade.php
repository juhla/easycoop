<div class="row">
    <div class="col-sm-12" id="displayForm" style="display: none">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter Cash Payment Details </header>
            <form role="form" action="{{ route('cash-payment') }}" method="post" id="pettyCashForm" data-validate="parsley">
                <section class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" data-required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>PVC No</label>
                                <input type="text" class="form-control"  value="{{createPVCNo('Cash Payment') }}" readonly name="pvc_no" id="pvc_no" data-required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label class="">Payee</label>
                                <input type="text" class="form-control" name="supplier" id="supplier" data-required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-type="number" data-required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" id="row_index" value="">
                            <button type="reset" class="btn btn-default m-t-5 close-form">Close</button>
                            <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>

