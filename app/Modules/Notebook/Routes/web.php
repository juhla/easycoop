<?php
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/app/notebook/get-suppliers', ['middleware' => 'verify:access-notebook', 'verify_license'], 'NotebookController@getSuppliers');
Route::get('/app/notebook/get-expenses', ['middleware' => 'verify:access-notebook', 'verify_license'], 'NotebookController@getExpenses');

//Non-Cash Registers
Route::group(['prefix' => 'notebook/non-cash-register', 'middleware' => ['web', 'auth:web', 'verify_license']], function () {

    //credit purchase
    Route::get('/', ['as' => 'credit-purchase', 'uses' => 'NotebookController@creditPurchase']);
    Route::any('/credit-purchase', ['as' => 'credit-purchase', 'uses' => 'NotebookController@creditPurchase']);
    Route::get('/get-cp/{id}', 'NotebookController@getCP');
    Route::any('/delete-cp/{id}', 'NotebookController@deleteCP');
    //credit sales
    Route::any('/credit-sales', ['as' => 'credit-sales', 'uses' => 'NotebookController@creditSales']);
    Route::get('/get-cs/{id}', 'NotebookController@getCS');
    Route::any('/delete-cs/{id}', 'NotebookController@deleteCS');

});
//Cash Register Routes
Route::group(['prefix' => 'notebook/cash-register', 'middleware' => ['web', 'auth:web', 'verify_license']], function () {
    Route::get('/', 'NotebookController@pettyCash');

    //cash payment
    Route::any('/cash-payment', ['as' => 'cash-payment', 'uses' => 'NotebookController@pettyCash']);
    Route::get('/get-pc/{id}', 'NotebookController@getPC');
    Route::any('/delete-pc/{id}', 'NotebookController@deletePC');

    //Cheque Payment
    Route::any('/cheque-payment', ['as' => 'cheque-payment', 'uses' => 'NotebookController@chequePayment']);
    Route::get('/get-chqp/{id}', 'NotebookController@getChqP');
    Route::any('/delete-chqp/{id}', 'NotebookController@deleteChqP');

    //Cheque receipt
    Route::any('/cheque-receipt', ['as' => 'cheque-receipt', 'uses' => 'NotebookController@chequeReceipt']);
    Route::get('/get-chqr/{id}', 'NotebookController@getChqR');
    Route::any('/delete-chqr/{id}', 'NotebookController@deleteChqR');

    //Cash receipt
    Route::any('/cash-receipt', ['as' => 'cash-receipt', 'uses' => 'NotebookController@cashReceipt']);
    Route::get('/get-cr/{id}', 'NotebookController@getCR');
    Route::any('/delete-cr/{id}', 'NotebookController@deleteCR');


});


Route::any('/notebook', ['middleware' => ['web', 'auth:web'], function () {
    $user = Auth::user();
    if ($user->hasRole(['business-owner', 'advanced-business-owner', 'basic-business-owner'])) {
        return redirect()->to('notebook-v2');
    } else {
        return redirect()->to('notebook/cash-register/cash-receipt');
    }
}]);


//bank receipt
Route::any('notebook/bank-receipt', ['middleware' => ['web', 'auth:web'], 'as' => 'bank-receipt', 'uses' => 'NotebookController@bankReceipt']);
Route::any('notebook/bank-payment', ['middleware' => ['web', 'auth:web'], 'as' => 'bank-payment', 'uses' => 'NotebookController@bankPayment']);

