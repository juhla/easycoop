<?php

namespace App\Modules\NotebookAdv\DataTables;

use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class NotebookAdvDataTable extends DataTable
{
    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable
            ->addColumn('debit', function ($__res) {
                return getLedgerName('D', $__res->id);
            })
            // ->filterColumn('reference_no', function ($query, $keyword) {
            //     $query->whereHas('item.ledger', function ($query) use ($keyword) {
            //         $sql = "reference_no  like ? ";
            //         $query->whereRaw($sql, ["%{$keyword}%"]);
            //     });
            // })
            ->addColumn('credit', function ($__res) {
                return getLedgerName('C', $__res->id);
            })
            // ->filterColumn('credit', function ($query, $keyword) {
            //     $query->whereHas('item.ledger', function ($query) use ($keyword) {
            //         $sql = "name  like ? ";
            //         $query->whereRaw($sql, ["%{$keyword}%"]);
            //     });
            // })
            ->editColumn('date', function ($__res) {
                return date('d/m/Y', strtotime($__res->transaction_date));
            })
            ->orderColumn('date', 'ca_transactions.transaction_date $1')
            ->addColumn('action', function ($entry) {
                return view('notebookadv::actions.datatables_actions', compact('entry'))->render();
            })
            ->rawColumns(['action']);

        return $dataTable;

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaction $model)
    {

        $type = $this->getType();
        $start_date = session('start_date');
        $end_date = session('end_date');
        $payment_type = ['1', '2', '3'];
        $transaction_type = ['inflow', 'outflow'];
        $query = $model->with(['item.ledger'])->whereIn('transaction_type', $transaction_type)->whereIn('payment_type', $payment_type)->newQuery();

        if (!empty($start_date)) {
            $start = Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '>=', $start);
        }
        if (!empty($end_date)) {
            $end = Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '<=', $end);
        }

        if (empty($start_date) && empty($end_date)) {

            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $query->whereBetween('ca_transactions.transaction_date', [$fiscalYear->begins, $fiscalYear->ends]);
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('inflowDT')
            ->columns($this->getColumns())
            ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['title' => 'Date', 'data' => 'date', 'footer' => 'date', 'searchable' => false],
            ['title' => 'Invoice No', 'data' => 'reference_no', 'footer' => 'reference_no'],
            ['title' => 'Description', 'data' => 'description', 'footer' => 'description'],
            ['title' => 'Amount', 'data' => 'amount', 'footer' => 'amount'],
            ['title' => 'Cash', 'data' => 'amount', 'footer' => 'amount'],
            ['title' => 'Bank', 'data' => 'amount', 'footer' => 'amount'],
            ['title' => 'Credit', 'data' => 'amount', 'footer' => 'amount'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'notebookadv' . date('YmdHis');
    }
}
