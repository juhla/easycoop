<?php

namespace App\Modules\NotebookAdv\Http\Controllers;

use App\Modules\Base\Traits\Notebook;
use Illuminate\Http\Request;
use App\Modules\NotebookAdv\Models\Transaction;
use App\Modules\NotebookAdv\Models\TransactionItem;
use App\Modules\Base\Http\Controllers\Controller;


class CashReceiptController extends Controller
{
    use Notebook;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function cashReceipt(Request $request)
    {

        $data['title'] = 'Cash Receipt';
        $input = $request->only('date', 'amount', 'description', 'item_id', 'type');

        $verifyDate = Transaction::verifyTransactionDate($input['date']);
        switch ($verifyDate) {
            case 'yes':
                {

                    // Payment type: 1 Cash | 2 Cheques
                    $input['trans_date'] = $input['date'];
                    if ($input['item_id'] != "") {

                        $result = new Transaction();
                        $result->updateFullTransaction($input, 'inflow', 1, $input['item_id']);
                    } else {
                        $result = new Transaction();
                        $result->saveFullTransaction($input, 'inflow', 1);
                    }

                    if ($result->getisError()) {
                        if (request()->ajax()) {
                            return response()->json(['message' => $result->getErrorMessage()], 500);
                        } else {
                            flash()->error($result->getErrorMessage());
                            return redirect()->back();
                        }
                    }
                    if (request()->ajax()) {
                        return response()->json(['message' => 'Transaction saved successfully.']);
                    } else {
                        flash()->success('Transaction saved successfully.');
                        return redirect()->back();
                    }


                }
                break;
            case 'no':
                {
                    $data = "";
                    $message = "Transaction date must be between current Fiscal Year";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
            default:
                {
                    $data = "";
                    $message = "Please setup a Fiscal Year before entering transactions";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
        }
    }

    public function getCR($id)
    {
        $entry = Transaction::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->transaction_date));
            $entry['type'] = 2;
            if ($this->checkType($entry->id)) {
                $entry['type'] = 1;
            }
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function deleteCR($id = null)
    {
        if (request()->input('ids')) {

            $ids = request()->input('ids');
            $result = TransactionItem::whereIn('transaction_id', $ids)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::destroy($ids);


        } else {


            $result = TransactionItem::where('transaction_id', $id)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::find($id)->update();
        }
    }
}
