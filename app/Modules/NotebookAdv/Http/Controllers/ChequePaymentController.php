<?php

namespace App\Modules\NotebookAdv\Http\Controllers;

use App\Modules\Base\Traits\Notebook;
use Illuminate\Http\Request;
use App\Modules\NotebookAdv\Models\Ledger;
use App\Modules\NotebookAdv\Models\Transaction;
use App\Modules\NotebookAdv\Models\TransactionItem;
use App\Modules\Base\Http\Controllers\Controller;

class ChequePaymentController extends Controller
{
    use Notebook;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function chequePayment(Request $request)
    {
        $data['title'] = 'Cheque Payment';
        $input = $request->only('date', 'amount', 'description', 'item_id', 'bank_lodged', 'type');

        $verifyDate = Transaction::verifyTransactionDate($input['date']);
        switch ($verifyDate) {
            case 'yes':
                {

                    // Payment type: 1 Cash | 2 Cheques
                    $input['trans_date'] = $input['date'];
                    $input['bank_id'] = $input['bank_lodged'];

                    if ($input['item_id'] != "") {
                        $result = new Transaction();
                        $result->updateFullTransaction($input, 'outflow', 2, $input['item_id']);
                    } else {
                        //Create
                        $result = new Transaction();
                        $result->saveFullTransaction($input, 'outflow', 2);
                    }

                    if ($result->getisError()) {
                        if (request()->ajax()) {
                            return response()->json(['message' => $result->getErrorMessage()], 500);
                        } else {
                            flash()->error($result->getErrorMessage());
                            return redirect()->back();
                        }
                    }
                    if (request()->ajax()) {
                        return response()->json(['message' => 'Transaction saved successfully.']);
                    } else {
                        flash()->success('Transaction saved successfully.');
                        return redirect()->back();
                    }
                }
                break;
            case 'no':
                {
                    $data = "";
                    $message = "Transaction date must be between current Fiscal Year";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
            default:
                {
                    $data = "";
                    $message = "Please setup a Fiscal Year before entering transactions";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
        }

    }

    public function getChqP($id)
    {
        $entry = Transaction::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->transaction_date));
            $entry['bank_lodged'] = $entry->bank_id;
//            $entry['bank_name'] =  Ledger::getLedgerByID($entry->bank_id)->name;
            $entry['type'] = 2;
            if ($this->checkType($entry->id)) {
                $entry['type'] = 1;
            }
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function deleteChqP($id = null)
    {
        if (request()->input('ids')) {

            $ids = request()->input('ids');
            $result = TransactionItem::whereIn('transaction_id', $ids)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::destroy($ids);


        } else {


            $result = TransactionItem::where('transaction_id', $id)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::find($id)->update();
        }
    }

}