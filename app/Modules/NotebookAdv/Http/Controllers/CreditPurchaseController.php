<?php

namespace App\Modules\NotebookAdv\Http\Controllers;

use App\Modules\NotebookAdv\Models\Ledger;
use Illuminate\Http\Request;
use App\Modules\NotebookAdv\Models\Transaction;
use App\Modules\NotebookAdv\Models\TransactionItem;
use App\Modules\Base\Http\Controllers\Controller;


class CreditPurchaseController extends Controller
{

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function creditPurchase(Request $request)
    {

        $data['title'] = 'Credit Purchase';
        $input = $request->only('date', 'amount', 'description', 'item_id', 'vendor');

        // Always Purchases
        $input['type'] = 2;
        $verifyDate = Transaction::verifyTransactionDate($input['date']);
        switch ($verifyDate) {
            case 'yes':
                {

                    // Payment type: 1 Cash | 2 Cheques
                    $input['trans_date'] = $input['date'];
                    $input['vendor_id'] = Ledger::getLedgerIdFromVendorId($input['vendor']);

                    if ($input['item_id'] != "") {
                        //update
                        $result = new Transaction();
                        $result->updateFullTransaction($input, 'outflow', 3, $input['item_id']);
                    } else {
                        //Create
                        $result = new Transaction();
                        $result->saveFullTransaction($input, 'outflow', 3);
                    }

                    if ($result->getisError()) {
                        if (request()->ajax()) {
                            return response()->json(['message' => $result->getErrorMessage()], 500);
                        } else {
                            flash()->error($result->getErrorMessage());
                            return redirect()->back();
                        }
                    }
                    if (request()->ajax()) {
                        return response()->json(['message' => 'Transaction saved successfully.']);
                    } else {
                        flash()->success('Transaction saved successfully.');
                        return redirect()->back();
                    }
                }
                break;
            case 'no':
                {
                    $data = "";
                    $message = "Transaction date must be between current Fiscal Year";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
            default:
                {
                    $data = "";
                    $message = "Please setup a Fiscal Year before entering transactions";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
        }
    }


    public function getCP($id)
    {
        $entry = Transaction::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->transaction_date));
            $entry['type'] = 2;
            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function deleteCP($id = null)
    {
        if (request()->input('ids')) {

            $ids = request()->input('ids');
            $result = TransactionItem::whereIn('transaction_id', $ids)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::destroy($ids);


        } else {


            $result = TransactionItem::where('transaction_id', $id)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::find($id)->update();
        }
    }


    /*public function __construct()
    {
        parent::__construct();
        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware('jwt.auth');
    }

    public function total()
    {
        // Payment type: 1 Cash | 2 Cheques | 3 Credit
        $data = Transaction::where(['transaction_type' => 'outflow', 'payment_type' => '3', 'flag' => 'Active'])
            ->sum('amount');
        $message = "";
        return response()->json(compact('data', 'message'));
    }

    public function index()
    {
        $data = Transaction::where(['transaction_type' => 'outflow', 'payment_type' => '3', 'flag' => 'Active'])
            ->get();
        $message = "";
        return response()->json(compact('data', 'message'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $input = request()->all();

        $verifyDate = Transaction::verifyTransactionDate($input['trans_date']);

        switch ($verifyDate) {
            case 'yes': {
                $data = $request->only('trans_date', 'amount', 'description');
                $validator = Validator::make($data, [
                    'trans_date' => 'required',
                    'description' => 'required',
                    'amount' => 'required|integer',
                ]);

                if ($validator->fails()) {
                    $data = $validator->errors()->all();
                    $message = "There was an error in your form";
                    return response()->json(compact('data', 'message'), 406);
                }

                // Payment type: 1 Cash | 2 Cheques | 3 Credit
                $result = Transaction::saveFullTransaction($input, 'outflow', 3);
                return $result;
            }
                break;
            case 'no': {
                $data = "";
                $message = "Transaction date must be between current Fiscal Year";
                return response()->json(compact('data', 'message'), 406);
            }
                break;
            default: {
                $data = "";
                $message = "Please setup a Fiscal Year before entering transactions";
                return response()->json(compact('data', 'message'), 406);
            }
                break;
        }
    }

    public function show($id)
    {
        $data = CreditPurchase::where(['id' => $id])->select('id', 'date as trans_date', 'description', 'amount', 'customer_vendor')->first();
        $message = "";
        return response()->json(compact('data', 'message'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $input = request()->all();

        $verifyDate = Transaction::verifyTransactionDate($input['trans_date']);
        switch ($verifyDate) {
            case 'yes': {
                $data = $request->only('trans_date', 'amount', 'description');
                $validator = Validator::make($data, [
                    'trans_date' => 'required',
                    'description' => 'required',
                    'amount' => 'required|integer',
                ]);

                if ($validator->fails()) {
                    $data = $validator->errors()->all();
                    $message = "There was an error in your form";
                    return response()->json(compact('data', 'message'), 406);
                }

                // Payment type: 1 Cash | 2 Cheques | 3 Credit
                $result = Transaction::updateFullTransaction($input, 'outflow', 3, $id);
                return $result;
            }
                break;
            case 'no': {
                $data = "";
                $message = "Transaction date must be between current Fiscal Year";
                return response()->json(compact('data', 'message'), 406);
            }
                break;
            default: {
                $data = "";
                $message = "Please setup a Fiscal Year before entering transactions";
                return response()->json(compact('data', 'message'), 406);
            }
                break;
        }
    }

    public function destroy($id)
    {
        $message = "Failed to delete Credit Purchase Record";
        if (Transaction::where('id', $id)
            ->delete()
        ) {
            $message = "The Credit Purchase Record has been Deleted";
        }
        return response()->json(compact('message'));
    }*/


}
