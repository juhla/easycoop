<?php

namespace App\Modules\NotebookAdv\Http\Controllers;


use App\Modules\NotebookAdv\Models\Ledger;
use Illuminate\Http\Request;
use App\Modules\NotebookAdv\Models\Transaction;
use App\Modules\NotebookAdv\Models\TransactionItem;
use App\Modules\Base\Http\Controllers\Controller;

class CreditSalesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function creditSales(Request $request)
    {
        $data['title'] = 'Credit Sales';
        $input = $request->only('date', 'amount', 'description', 'item_id', 'customer');
// Always Sales
        $input['type'] = 2;
        $verifyDate = Transaction::verifyTransactionDate($input['date']);
        switch ($verifyDate) {
            case 'yes':
                {

                    // Payment type: 1 Cash | 2 Cheques | 3 Credit
                    $input['trans_date'] = $input['date'];
                    $input['customer_id'] = Ledger::getLedgerIdFromCustomerId($input['customer']);

                    if ($input['item_id'] != "") {
                        //update
                        $result = new Transaction();
                        $result->updateFullTransaction($input, 'inflow', 3, $input['item_id']);
                    } else {
                        //Create
                        $result = new Transaction();
                        $result->saveFullTransaction($input, 'inflow', 3);
                    }

                    if ($result->getisError()) {
                        if (request()->ajax()) {
                            return response()->json(['message' => $result->getErrorMessage()], 500);
                        } else {
                            flash()->error($result->getErrorMessage());
                            return redirect()->back();
                        }
                    }
                    if (request()->ajax()) {
                        return response()->json(['message' => 'Transaction saved successfully.']);
                    } else {
                        flash()->success('Transaction saved successfully.');
                        return redirect()->back();
                    }
                }
                break;
            case 'no':
                {
                    $data = "";
                    $message = "Transaction date must be between current Fiscal Year";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
            default:
                {
                    $data = "";
                    $message = "Please setup a Fiscal Year before entering transactions";
                    return response()->json(compact('data', 'message'), 406);
                }
                break;
        }

    }

    public function getCS($id)
    {
        $entry = Transaction::findOrFail($id);

        if ($entry) {
            $entry['date'] = date('d/m/Y', strtotime($entry->transaction_date));
            $entry['customer_id'] = $entry->customer_vendor;
            $entry['type'] = 2;
//            $entry['bank_name'] = getLedgerByID()->name;

            return response()->json($entry);
        } else {
            return response()->json(['status' => 404]);
        }
    }

    public function deleteCS($id = null)
    {
        if (request()->input('ids')) {

            $ids = request()->input('ids');
            $result = TransactionItem::whereIn('transaction_id', $ids)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::destroy($ids);


        } else {


            $result = TransactionItem::where('transaction_id', $id)->get()->pluck('id', 'id');
            $result = $result->toArray();
            TransactionItem::destroy($result);
            Transaction::find($id)->update();
        }
    }

}
