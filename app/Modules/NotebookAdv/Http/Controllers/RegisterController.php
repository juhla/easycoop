<?php

namespace App\Modules\NotebookAdv\Http\Controllers;

use App\Modules\NotebookAdv\DataTables\NotebookAdvDataTable;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\NotebookAdv\Models\Transaction;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{

    private $_registerData = [];

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    private function _creditSales($whereArray = [])
    {
        // Payment type: 1 Cash | 2 Cheques | 3 Credit
        $creditSales = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'inflow', 'payment_type' => '3', 'flag' => 'Active']);
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($creditSales as $each) {
            $__['view'] = "/notebook-adv/non-cash-register/get-cs/";
            $__['delete'] = "/notebook-adv/non-cash-register/delete-cs/";
            $__['trans_type'] = "1";
            $__['trans_mode'] = "3";
            $__['date'] = $each->transaction_date;
            $__['id'] = $each->id;
            $__['numb'] = $each->reference_no;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            $__['flag'] = $each->flag;
            array_push($this->_registerData, $__);
        }
    }

    private function _creditPurchases($whereArray = [])
    {
        $creditPurchase = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'outflow', 'payment_type' => '3', 'flag' => 'Active']);
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($creditPurchase as $each) {
            $__['view'] = "/notebook-adv/non-cash-register/get-cs/";
            $__['delete'] = "/notebook-adv/non-cash-register/delete-cs/";
            $__['trans_type'] = "2";
            $__['trans_mode'] = "3";
            $__['date'] = $each->transaction_date;
            $__['id'] = $each->id;
            $__['numb'] = $each->reference_no;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            $__['flag'] = $each->flag;
            array_push($this->_registerData, $__);
        }
    }

    private function _chequePayment($whereArray = [])
    {
        $chequePayment = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'outflow', 'payment_type' => '2', 'flag' => 'Active'])
                ->with(['ledgerByBank'])->get();
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($chequePayment as $each) {
            $__['trans_type'] = "2";
            $__['trans_mode'] = "2";
            $__['view'] = "/notebook-adv/cash-register/get-chqp/";
            $__['delete'] = "/notebook-adv/cash-register/delete-chqp/";

            $__['date'] = $each->date;
            $__['numb'] = $each->pv_no;

            $__['date'] = $each->transaction_date;

            $__['id'] = $each->id;
            $__['numb'] = $each->reference_no;
            $__['flag'] = $each->flag;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _cashPayment($whereArray = [])
    {
        $cashPayment = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'outflow', 'payment_type' => '1', 'flag' => 'Active']);
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        // Transaction::where(['transaction_type' => 'outflow', 'payment_type' => '1', 'flag' => 'Active'])
        //     ->get();

        foreach ($cashPayment as $each) {
            $__['trans_type'] = "2";
            $__['trans_mode'] = "1";
            $__['view'] = "/notebook-adv/cash-register/get-cp/";
            $__['delete'] = "/notebook-adv/cash-register/delete-cp/";
            $__['date'] = $each->transaction_date;
            $__['numb'] = $each->reference_no;
            $__['flag'] = $each->flag;
            $__['id'] = $each->id;
            // $__['numb'] = $each->pvc_no;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _cashReceipt($whereArray = [])
    {
        $cashReceipt = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'inflow', 'payment_type' => '1', 'flag' => 'Active']);
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($cashReceipt as $each) {
            $__['trans_type'] = "1";
            $__['trans_mode'] = "1";
            $__['view'] = "/notebook-adv/cash-register/get-cr/";
            $__['delete'] = "/notebook-adv/cash-register/delete-cr/";
            $__['date'] = $each->transaction_date;
            $__['numb'] = $each->reference_no;
            $__['flag'] = $each->flag;
            $__['id'] = $each->id;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _chequeReceipt($whereArray = [])
    {
        $chequeReceipt = Transaction::where(function ($query) use ($whereArray) {
            $query->where(['transaction_type' => 'inflow', 'payment_type' => '2', 'flag' => 'Active'])
                ->with(['ledgerByBank'])->get();
            if (!empty($whereArray)) {
                $query->whereBetween('transaction_date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($chequeReceipt as $each) {
            $__['trans_type'] = "1";
            $__['trans_mode'] = "2";
            $__['view'] = "/notebook-adv/cash-register/get-chqr/";
            $__['delete'] = "/notebook-adv/cash-register/delete-chqr/";
            $__['date'] = $each->transaction_date;
            $__['id'] = $each->id;
            $__['numb'] = $each->reference_no;
            $__['flag'] = $each->flag;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function allRegister()
    {
        $this->_creditSales();
        $this->_creditPurchases();
        $this->_chequePayment();
        $this->_chequeReceipt();
        $this->_cashPayment();
        $this->_cashReceipt();
        return $this->_registerData;
    }

    public function index(NotebookAdvDataTable $table)
    {
        $data['title'] = 'Notebook Register';
        $inputs = request()->all();

        $this->allRegister();
        // $data['start_date'] = "";
        // $data['end_date'] = "";
        // $data['search'] = "0";
        // if ($input = request()->all()) {
        //     $transactionType = $input['transaction_type'];
        //     $transactionMode = $input['transaction_mode'];
        //     $data['start_date'] = $input['from_date'];
        //     $data['end_date'] = $input['to_date'];

        if (request()->isMethod('post')) {
            // dd($inputs['to_date']);

            Session::put('start_date', $inputs['from_date']);
            Session::put('end_date', $inputs['to_date']);
            Session::put('transaction_type', $inputs['transaction_type']);
            Session::put('transaction_mode', $inputs['transaction_mode']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');
        $transaction_type = session('transaction_type');
        $transaction_mode = session('transaction_mode');

        $entry = $this->_registerData;
        // dd($entries);
        // return view('notebook-adv::register.book')->with($data);

        $table->setType(@$type);
        return $table->render('notebook-adv::register.book', compact('entry', 'title', 'start_date', 'end_date', 'transaction_type', 'transaction_mode'));
    }

    public function filter()
    { }
}
