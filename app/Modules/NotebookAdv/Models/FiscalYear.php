<?php

namespace App\Modules\NotebookAdv\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\NullingDB;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;


class FiscalYear extends \App\Modules\Base\Models\FiscalYear
{
    use NullingDB;
    use SoftDeletes;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'ca_fiscal_year';
    protected $attributes = [
        'status' => 'Open',
        'flag' => 'Active',
    ];
    //mass assignment fields
    protected $fillable = ['begins', 'ends', 'status'];

    public static function getCurrentFiscalYear()
    {
        $f_year = FiscalYear::where('status', 'Open')
            ->where('flag', 'Active')->first();
        return $f_year;
    }
}
