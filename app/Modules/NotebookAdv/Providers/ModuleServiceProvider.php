<?php

namespace App\Modules\NotebookAdv\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'notebook-adv');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'notebook-adv');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'notebook-adv');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
