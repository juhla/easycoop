<?php

use App\Modules\Base\Traits\Acl;

?>



    @if(canEdit())
    {{-- {{$entry}} --}}
    {{-- {{$entry->payment_type}} --}}
    @if($entry->payment_type == "3" && $entry->transaction_type == 'inflow')
    @php 
    $edit_url="/notebook-adv/non-cash-register/get-cs/";
    $delete_url = "/notebook-adv/non-cash-register/delete-cs/";
     @endphp
     @elseif($entry->payment_type == "3" && $entry->transaction_type == 'outflow')
     @php 
     $edit_url="/notebook-adv/non-cash-register/get-cs/";
     $delete_url = "/notebook-adv/non-cash-register/delete-cs/";
      @endphp
      @elseif($entry->payment_type == "2" && $entry->transaction_type == 'outflow')
      @php 
     $edit_url="/notebook-adv/cash-register/get-chqp/";
     $delete_url = "/notebook-adv/cash-register/delete-chqp/";
      @endphp
      @elseif($entry->payment_type == "1" && $entry->transaction_type == 'outflow')
      @php 
     $edit_url="/notebook-adv/cash-register/get-cp/";
     $delete_url = "/notebook-adv/cash-register/delete-cp/";
      @endphp
      @elseif($entry->payment_type == "1" && $entry->transaction_type == 'inflow')
      @php 
     $edit_url="/notebook-adv/cash-register/get-cr/";
     $delete_url = "/notebook-adv/cash-register/delete-cr/";
      @endphp
      @elseif($entry->payment_type == "2" && $entry->transaction_type == 'inflow')
      @php 
     $edit_url="/notebook-adv/cash-register/get-chqr/";
     $delete_url = "/notebook-adv/cash-register/delete-chqr/";
      @endphp
    @endif
    <div class="btn-group btn-group-xs">
        <button type="button" class="btn btn-default edit"
                href="#section2"
                data-trans-type="{{ $entry->transaction_type }}"
                data-trans-mode="{{ $entry->trans_mode }}"
                data-url-edit="{{ $edit_url }}"
                data-url-delete="{{ $delete_url }}"
                data-value="{{ $entry->id }}"><i
                    class="fa fa-edit"></i></button>
                    {{-- <p>{{ $entry->delete}}</p> --}}
        <button type="button" class="btn btn-danger delete"
                data-url-edit="{{ $edit_url }}"
                data-url-delete="{{ $delete_url }}"
                data-value="{{ $entry->id }}"><i
                    class="fa fa-trash-o"></i></button>
    </div>
    @endif
