<div class="row" id="bankPayment">
    <div class="col-sm-12">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Filter Results</header>
            <form role="form" action="{{ url('notebook-adv')  }}" method="post"
                  data-validate="parsley">
                <section class="panel-body">
                    <div class="col-md-12 col-lg-12 m-t-md">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <div class="form-group col-md-3">
                            <select name="transaction_type" class="select2-option"
                                    style="width: 100%" required>
                                <?php $transaction_type = config('constants.transaction_type'); ?>
                                <option value="0">Select A Type</option>
                                @foreach($transaction_type as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <select name="transaction_mode" class="select2-option"
                                    style="width: 100%" required>
                                <?php $transaction_mode = config('constants.transaction_mode'); ?>
                                <option value="0">Select A Mode</option>
                                @foreach($transaction_mode as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <div class="input-group date form_datetime"
                                 data-picker-position="bottom-left">
                                <input type="text" class="form-control datepicker" name="from_date"
                                       id="from_date"
                                       value="{{ $start_date }}"
                                       placeholder="From:">
                                <span class="input-group-btn">
                         <button class="btn btn-default" type="button"><i
                                     class="fa fa-calendar"></i></button>
                     </span>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <div class="input-group date form_datetime"
                                 data-picker-position="bottom-left">
                                <input type="text" class="form-control datepicker" name="to_date"
                                       id="to_date"
                                       value="{{ $end_date }}"
                                       placeholder="To:">
                                <span class="input-group-btn">
                         <button class="btn btn-default" type="button"><i
                                     class="fa fa-calendar"></i></button>
                     </span>
                            </div>
                        </div>
                        <div class="btn-group col-md-3">
                            <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>
                                Submit
                            </button>
                            <a href="{{ url('notebook-adv') }}" class="btn btn-default">Clear</a>
                        </div>


                    </div>
                </section>
            </form>
        </section>
    </div>
</div>