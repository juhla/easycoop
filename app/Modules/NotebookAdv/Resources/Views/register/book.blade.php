@extends('layouts.main')
<style>
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">


                        <div class="col-sm-8 m-b-xs">

                            @if(auth()->user()->hasRole('professional'))
                                @if(canEdit())
                                    <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                            disabled><i class="fa fa-save"></i>
                                        Post to GL
                                    </button>
                                @endif
                            @endif
                            @if(!auth()->user()->hasRole('professional'))
                                @if(canEdit())

                                    {{--<a href="javascript:;" class="btn btn-sm btn-primary add-new">--}}
                                    {{--<i class="fa fa-plus"></i> Add New--}}
                                    {{--</a>--}}
                                @endif
                            @endif


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">

                    <div class="col-sm-8 m-b-xs">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Transaction Type</label>
                                <select name="transaction_type" id="transaction_type" class="select2-option"
                                        style="width: 100%" required>
                                    <?php $transaction_type = config('constants.transaction_type'); ?>
                                    @foreach($transaction_type as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Transaction Mode</label>
                                <select name="transaction_mode" id="transaction_mode" class="select2-option"
                                        style="width: 100%" required>
                                    <?php $transaction_mode = config('constants.transaction_mode'); ?>
                                    @foreach($transaction_mode as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="section2"></div>
                    @include('notebook-adv::register.partials._all')
                    <div class="loader" id="loaderDIV" style="display: none"></div>

                            @include('notebookadv::actions.search')


                    <section class="panel panel-default">
                        <div class="table-responsive">
                            @section('css')
                            @include('layouts.datatables_css')
                            @endsection
                            {!! $dataTable->table(['width' => '100%']) !!}
                            @section('scripts_dt')
                            @include('layouts.datatables_js')
                            {!! $dataTable->scripts() !!}
                            @endsection
                        </div>

                       
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
    <script src="{{ asset('js/scripts/notebook-register_adv.js')}}" type="text/javascript"></script>
@stop

