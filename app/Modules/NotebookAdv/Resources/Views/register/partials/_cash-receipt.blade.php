<div class="row" id="cashReceipt" style="display: none">
    <div class="col-sm-12">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter New Cash Receipt</header>
            <form role="form" action="{{ route('cash-receipt-adv') }}" method="post" id="notebookRegisterForm" data-validate="parsley">
                <section class="panel-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" readonly data-required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Entry Type</label>
                                <select name="type"  class="select2-option" style="width: 100%" required>
                               
                                        <option value="2">Sales</option>
                                        <option value="1">Others</option>
                               
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{-- <label>Invoice No</label>
                                <input type="text" class="form-control" value="{{createInvoiceNo('Cash Receipt') }}" readonly name="invoice_no" id="invoice_no" data-required> --}}

                                <label>Description</label>
                                <input type="text" class="form-control" name="description" id="description">

                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group form-group-default">
                                {{-- <label class="">Customer</label>
                                <input type="text" class="form-control" name="customer" id="customer" data-required> --}}
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-type="number" data-required>
                        </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="description" id="description">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group ">
                                <label class="">Bank Lodged</label>
                                <input type="text" class="form-control" name="bank" id="bank">

                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-type="number" data-required>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">

                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" id="row_index" value="">
                            <button type="reset" class="btn btn-default m-t-5 close-modal">Close</button>

                            <button type="submit" class="btn btn-info m-t-5" id="saveBtnCashR">Save</button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>

