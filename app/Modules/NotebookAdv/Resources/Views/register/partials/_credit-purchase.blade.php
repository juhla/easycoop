<div class="row" id="creditPurchase" style="display: none">
    <div class="col-sm-12">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter New Credit Purchase </header>
            <form role="form" action="{{ route('credit-purchase-adv')  }}" method="post" id="notebookRegisterForm" data-validate="parsley">
                <section class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" readonly data-required>
                            </div>
                        </div>

                       {{--  <div class="col-sm-4">
                            <div class="form-group">
                                <label>Invoice No</label>
                                <input type="text" class="form-control" value="{{createInvoiceNo('Credit Purchase') }}" readonly name="invoice_no" id="invoice_no" data-required>
                            </div>
                        </div> --}}

                        <div class="col-sm-4" >
                            <div class="form-group" id="vendorReload">
                                <label class="">Vendor</label> &nbsp;&nbsp;&nbsp;<span id="successMessage"></span>
                               {{--  <input type="text" class="form-control" name="supplier" id="supplier" data-required> --}}
                                <select name="vendor" id="vendor"   class="select2-option"
                                        style="width: 100%" required>
                                    <?php $vendors = App\Modules\Base\Models\Vendor::where('flag', 'Active')->get(); ?>
                                    @foreach($vendors->all() as $vendor)
                                        <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <br>
                            <button class="btn btn-info" id="addVendor">+ Add Vendor</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="description" id="description">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-type="number" data-required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" id="row_index" value="">
                            <button type="reset" class="btn btn-default m-t-5 close-modal">Close</button>
                            <button type="submit" class="btn btn-info m-t-5" id="saveBtnCP">Save</button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>
 @include('business-setup::partials._vendor-form')
<script type="text/javascript">



      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });


  $('#addVendor').click(function(){
          $('#vendorFormModal').modal('show');
      });

      $('#vendorForm').on('submit',function(e){
        e.preventDefault();
       

    
      
       var url = $(this).attr('action');
       var post = $(this).attr('method');
        var data = $(this).serialize();
        $.ajax({
            type: post,
            url: url,
            data: data,

            success: function(data){

             var message = "<span style='color:#189730;font-weight:bolder;background:#B0E2AC; padding:5; border-radius:2px'>Vendor was added successfully</span>";
             var fade = $('#successMessage').html(message).toggle(50000);

                               if(fade)
                               {
                                     $('#successMessage').fadeOut(50000);
                               }

              $('#vendorReload').load(location.href + ' #vendorReload');
                $('#vendorFormModal').modal('hide');
               
            }

          

        })

          

    });

    

        $('.close-modal').click(function(){
            $('#vendorForm')[0].reset();
            $('#vendorFormModal').modal('hide');
            $('#country_id').select2('val', '');
            $('#currency_code').select2('val', '');
        });


    
    

</script>

