<div class="row" id="creditSales" style="display: none">
    <div class="col-sm-12">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter New Credit Sales </header>
            <form role="form" action="{{ route('credit-sales-adv')  }}" method="post" id="notebookRegisterForm" data-validate="parsley">
                <section class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" readonly data-required>
                            </div>
                        </div>

                        {{-- <div class="col-sm-4">
                            <div class="form-group">
                                <label>Invoice No</label>
                                <input type="text" class="form-control" value="{{createInvoiceNo('Credit Sales') }}" readonly name="invoice_no" id="invoice_no" data-required>
                            </div>
                        </div> --}}

                        <div class="col-sm-4">
                            <div class="form-group" id="customerReload">
                                <label class="">Customer</label> &nbsp;&nbsp;<span id="successMessage"></span>
                               {{--  <input type="text" class="form-control" name="customer" id="customer" data-required> --}}
                                 <select name="customer" id="customer"  class="select2-option"
                                        style="width: 100%" required>
                                     <?php $customers = App\Modules\Base\Models\Customer::where('flag', 'Active')->get(); ?>
                                      <span id="customerReload">
                                    @foreach($customers->all() as $customer)
                                     <option  value="{{$customer->id}}">{{$customer->name}}</option> 
                                    @endforeach
                                    </span>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                           <br/>
                            <button class="btn btn-info" id="addCustomer">+ Add Customer</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" name="description" id="description">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="0.00" data-required="true" data-type="number">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" id="row_index" value="">
                            <button type="reset" class="btn btn-default m-t-5 close-modal">Close</button>
                            <button type="submit" class="btn btn-info m-t-5" id="saveBtnCS">Save</button>
                        </div>
                    </div>
                </section>
            </form>
        </section>
    </div>
</div>
 @include('business-setup::partials._customer-form2')
<script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#addCustomer').click(function(){
            $('#customerFormModal').modal('show');
        });

        $('#customerForm').on('submit',function(e){
          e.preventDefault();
         

   
        
         var url = $(this).attr('action');
         var post = $(this).attr('method');
          var data = $(this).serialize();
          $.ajax({
              type: post,
              url: url,
              data: data,

              success: function(data){

                  var message = "<span style='color:#189730;font-weight:bolder;background:#B0E2AC; padding:5; border-radius:2px'>Customer was added successfully</span>";
                  var fade = $('#successMessage').html(message).toggle(50000);

                                    if(fade)
                                    {
                                          $('#successMessage').fadeOut(50000);
                                    }

                   $('#customerReload').load(location.href + ' #customerReload');
                     $('#customerFormModal').modal('hide');
                 
              }

            

          })

            

      });


   

        $('.close-customer-modal').click(function(){
            $('#customerForm')[0].reset();
            $('#customerFormModal').modal('hide');
            $("#country_id").select2('val', '');
            $("#currency_code").select2('val','');
        });


    
    


</script>
