<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'notebook-adv', 'middleware' => ['web', 'auth:web', 'verify_license']], function () {
    Route::any('/', ['as' => 'notebook-register', 'uses' => 'RegisterController@index']);
    //Route::post('/filter', ['as' => 'notebook-filter', 'uses' => 'RegisterController@filter']);
});
//Route::any('/notebook', ['middleware' => ['web', 'auth:web'], 'uses' => 'RegisterController@index']);

//Route::get('/app/notebook-adv/get-suppliers', ['middleware' => 'verify:access-notebook'], 'NotebookController@getSuppliers');
//Route::get('/app/notebook-adv/get-expenses', ['middleware' => 'verify:access-notebook'], 'NotebookController@getExpenses');

//Non-Cash Registers
Route::group(['prefix' => 'notebook-adv/non-cash-register', 'middleware' => ['web', 'auth:web', 'verify_license']], function () {
//
//    //credit purchase
    Route::any('/credit-purchase', ['as' => 'credit-purchase-adv', 'uses' => 'CreditPurchaseController@creditPurchase']);
    Route::get('/get-crp/{id}', 'CreditPurchaseController@getCRP');
    Route::any('/delete-crp/{id}', 'CreditPurchaseController@deleteCRP');

    //credit sales
    Route::any('/credit-sales', ['as' => 'credit-sales-adv', 'uses' => 'CreditSalesController@creditSales']);
    Route::get('/get-cs/{id}', 'CreditSalesController@getCS');
    Route::any('/delete-cs/{id}', 'CreditSalesController@deleteCS');

});

//////Cash Register Routes
Route::group(['prefix' => 'notebook-adv/cash-register', 'middleware' => ['web', 'auth:web', 'verify_license']], function () {
//    Route::get('/', 'NotebookController@pettyCash');

  //cash payment
    Route::any('/cash-payment', ['as' => 'cash-payment-adv', 'uses' => 'CashPaymentController@cashPayment']);
    Route::get('/get-cp/{id}', 'CashPaymentController@getCP');
    Route::any('/delete-cp/{id}', 'CashPaymentController@deleteCP');

 //Cheque Payment
    Route::any('/cheque-payment', ['as' => 'cheque-payment-adv', 'uses' => 'ChequePaymentController@chequePayment']);
    Route::get('/get-chqp/{id}', 'ChequePaymentController@getChqP');
    Route::any('/delete-chqp/{id}', 'ChequePaymentController@deleteChqP');

//    //Cheque receipt
    Route::any('/cheque-receipt', ['as' => 'cheque-receipt-adv', 'uses' => 'ChequeReceiptController@chequeReceipt']);
    Route::get('/get-chqr/{id}', 'ChequeReceiptController@getChqR');
    Route::any('/delete-chqr/{id}', 'ChequeReceiptController@deleteChqR');

//    //Cash receipt
    Route::any('/cash-receipt', ['as' => 'cash-receipt-adv', 'uses' => 'CashReceiptController@cashReceipt']);
    Route::get('/get-cr/{id}', 'CashReceiptController@getCR');
    Route::any('/delete-cr/{id}', 'CashReceiptController@deleteCR');

});