<?php

namespace App\Modules\NotebookV2\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Notebook\Models\CashReceipt;
use App\Modules\Notebook\Models\ChequePayment;
use App\Modules\Notebook\Models\ChequeReceipt;
use App\Modules\Notebook\Models\CreditPurchase;
use App\Modules\Notebook\Models\CreditSales;
use App\Modules\Notebook\Models\PettyCash;
use Carbon\Carbon;

class RegisterController extends Controller
{
    private $_registerData = [];

    public function __construct()
    {
        $this->middleware(['tenant']);

    }

    private function _creditSales($whereArray = [])
    {
        $creditSales = CreditSales::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();
        foreach ($creditSales as $each) {
            $__['view'] = "/notebook/non-cash-register/get-cs/";
            $__['delete'] = "/notebook/non-cash-register/delete-cs/";
            $__['trans_type'] = "1";
            $__['trans_mode'] = "3";
            $__['date'] = $each->date;
            $__['numb'] = $each->invoice_no;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            $__['id'] = $each->id;
            $__['status'] = $each->status;
            array_push($this->_registerData, $__);
        }
    }

    private function _creditPurchases($whereArray = [])
    {
        $creditPurchase = CreditPurchase::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();
        foreach ($creditPurchase as $each) {
            $__['trans_type'] = "2";
            $__['trans_mode'] = "3";
            $__['view'] = "/notebook/non-cash-register/get-cp/";
            $__['delete'] = "/notebook/non-cash-register/delete-cp/";
            $__['date'] = $each->date;
            $__['status'] = $each->status;
            $__['id'] = $each->id;
            $__['numb'] = $each->invoice_no;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _chequePayment($whereArray = [])
    {
        $chequePayment = ChequePayment::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();
        foreach ($chequePayment as $each) {
            $__['trans_type'] = "2";
            $__['trans_mode'] = "2";
            $__['view'] = "/notebook/cash-register/get-chqp/";
            $__['delete'] = "/notebook/cash-register/delete-chqp/";
            $__['date'] = $each->date;
            $__['numb'] = $each->pv_no;
            $__['id'] = $each->id;
            $__['status'] = $each->status;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _cashPayment($whereArray = [])
    {
        $cashPayment = PettyCash::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();
        foreach ($cashPayment as $each) {
            $__['trans_type'] = "2";
            $__['trans_mode'] = "1";
            $__['view'] = "/notebook/cash-register/get-pc/";
            $__['delete'] = "/notebook/cash-register/delete-pc/";
            $__['date'] = $each->date;
            $__['id'] = $each->id;
            $__['numb'] = $each->pvc_no;
            $__['status'] = $each->status;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _cashReceipt($whereArray = [])
    {
        $cashReceipt = CashReceipt::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();
        foreach ($cashReceipt as $each) {
            $__['trans_type'] = "1";
            $__['trans_mode'] = "1";
            $__['view'] = "/notebook/cash-register/get-cr/";
            $__['delete'] = "/notebook/cash-register/delete-cr/";
            $__['date'] = $each->date;
            $__['numb'] = $each->invoice_no;
            $__['status'] = $each->status;
            $__['id'] = $each->id;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function _chequeReceipt($whereArray = [])
    {
        $chequeReceipt = ChequeReceipt::where(function ($query) use ($whereArray) {
            $query->where('status', 0);
            if (!empty($whereArray)) {
                $query->whereBetween('date', [$whereArray['from_date'], $whereArray['to_date']]);
            }
        })->get();

        foreach ($chequeReceipt as $each) {
            $__['trans_type'] = "1";
            $__['trans_mode'] = "2";
            $__['view'] = "/notebook/cash-register/get-chqr/";
            $__['delete'] = "/notebook/cash-register/delete-chqr/";
            $__['date'] = $each->date;
            $__['id'] = $each->id;
            $__['numb'] = $each->invoice_no;
            $__['status'] = $each->status;
            $__['description'] = $each->description;
            $__['amount'] = $each->amount;
            array_push($this->_registerData, $__);
        }
    }

    private function allRegister()
    {
        $this->_creditSales();
        $this->_creditPurchases();
        $this->_chequePayment();
        $this->_chequeReceipt();
        $this->_cashPayment();
        $this->_cashReceipt();
        return $this->_registerData;
    }

    public function index()
    {
        $data['title'] = 'Notebook Register';
        $this->allRegister();
        $data['start_date'] = "";
        $data['end_date'] = "";
        $data['search'] = "0";
        if ($input = request()->all()) {
            $transactionType = $input['transaction_type'];
            $transactionMode = $input['transaction_mode'];

            $data['start_date'] = $input['from_date'];
            $data['end_date'] = $input['to_date'];
            $data['search'] = "1";
            if (!empty($input['from_date'])) {
                $input['from_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['from_date'])->format('Y-m-d');

            } else {
                $input['from_date'] = \Carbon\Carbon::now()->toDateString();

            }
            if (!empty($input['to_date'])) {
                $input['to_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['to_date'])->format('Y-m-d');

            } else {
                $input['to_date'] = \Carbon\Carbon::now()->toDateString();
            }

            switch (true) {
                case (($transactionType == 1) && ($transactionMode == 1)): {
                        $this->_registerData = [];
                        $this->_cashReceipt($input);
                    }
                    break;

                case (($transactionType == 1) && ($transactionMode == 2)): {
                        $this->_registerData = [];
                        $this->_chequeReceipt($input);
                    }
                    break;

                case (($transactionType == 1) && ($transactionMode == 3)): {
                        $this->_registerData = [];
                        $this->_creditSales($input);

                    }
                    break;

                case (($transactionType == 2) && ($transactionMode == 1)): {
                        $this->_registerData = [];
                        $this->_cashPayment($input);

                    }
                    break;

                case (($transactionType == 2) && ($transactionMode == 2)): {
                        $this->_registerData = [];
                        $this->_chequePayment($input);

                    }
                    break;

                case (($transactionType == 2) && ($transactionMode == 3)): {
                        $this->_registerData = [];
                        $this->_creditPurchases($input);
                    }
                    break;
            }

        }
        $data['entries'] = $this->_registerData;
        return view('notebook-v2::register.book')->with($data);
    }

    public function filter()
    {

    }

}
