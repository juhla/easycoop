<?php

namespace App\Modules\NotebookV2\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'notebook-v2');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'notebook-v2');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'notebook-v2');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
