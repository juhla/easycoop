@extends('layouts.main')
<style>
    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
@section('content')
    <section class="hbox stretch">

        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">


                        <div class="col-sm-8 m-b-xs">

                            @if(auth()->user()->hasRole('professional'))
                                @if(canEdit())
                                    <button type="button" class="btn btn-sm btn-info post-entries" title="Remove"
                                            disabled><i class="fa fa-save"></i>
                                        Post to GL
                                    </button>
                                @endif
                            @endif
                            @if(!auth()->user()->hasRole('professional'))
                                @if(canEdit())

                                    {{--<a href="javascript:;" class="btn btn-sm btn-primary add-new">--}}
                                    {{--<i class="fa fa-plus"></i> Add New--}}
                                    {{--</a>--}}
                                @endif
                            @endif


                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="search" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">

                    <div class="col-sm-8 m-b-xs">
                        <div class="col-sm-3">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Transaction Type</label>
                                <select name="transaction_type" id="transaction_type" class="select2-option"
                                        style="width: 100%" required>
                                    <?php $transaction_type = config('constants.transaction_type'); ?>
                                    @foreach($transaction_type as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Transaction Mode</label>
                                <select name="transaction_mode" id="transaction_mode" class="select2-option"
                                        style="width: 100%" required>
                                    <?php $transaction_mode = config('constants.transaction_mode'); ?>
                                    @foreach($transaction_mode as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="section2"></div>
                    @include('notebook-v2::register.partials._all')
                    <div class="loader" id="loaderDIV" style="display: none"></div>

                    <div class="row" id="bankPayment">
                        <div class="col-sm-12">
                            <section class="panel panel-info portlet-item">
                                <header class="panel-heading"> Filter Results</header>
                                <form role="form" action="{{ url('notebook-v2')  }}" method="post"
                                      data-validate="parsley">
                                    <section class="panel-body">
                                        <div class="col-md-12 col-lg-12 m-t-md">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                                            <div class="form-group col-md-3">
                                                <select name="transaction_type" class="select2-option"
                                                        style="width: 100%" required>
                                                    <?php $transaction_type = config('constants.transaction_type'); ?>
                                                    <option value="0">Select A Type</option>
                                                    @foreach($transaction_type as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <select name="transaction_mode" class="select2-option"
                                                        style="width: 100%" required>
                                                    <?php $transaction_mode = config('constants.transaction_mode'); ?>
                                                    <option value="0">Select A Mode</option>
                                                    @foreach($transaction_mode as $key => $value)
                                                        <option value="{{$key}}">{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-md-3">
                                                <div class="input-group date form_datetime"
                                                     data-picker-position="bottom-left">
                                                    <input type="text" class="form-control datepicker" name="from_date"
                                                           id="from_date"
                                                           value="{{ $start_date }}"
                                                           placeholder="From:">
                                                    <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i
                                                         class="fa fa-calendar"></i></button>
                                         </span>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <div class="input-group date form_datetime"
                                                     data-picker-position="bottom-left">
                                                    <input type="text" class="form-control datepicker" name="to_date"
                                                           id="to_date"
                                                           value="{{ $end_date }}"
                                                           placeholder="To:">
                                                    <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i
                                                         class="fa fa-calendar"></i></button>
                                         </span>
                                                </div>
                                            </div>
                                            <div class="btn-group col-md-3">
                                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i>
                                                    Submit
                                                </button>
                                                <a href="{{ url('notebook-v2') }}" class="btn btn-default">Clear</a>
                                            </div>


                                        </div>
                                    </section>
                                </form>
                            </section>
                        </div>
                    </div>


                    <section class="panel panel-default">


                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr align="center">
                                    <th><input type="checkbox" id="checkAll"/></th>
                                    <th>Date</th>
                                    <th>Invoice No</th>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Cash</th>
                                    <th>Bank</th>
                                    <th>Credit</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <form action="{{ url('transactions/registers/cash-receipt') }}" method="post"
                                      id="post-register">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <tbody id="entries">
                                    @if(count($entries) > 0)
                                        <?php $total = $totalCash = $totalBank = $totalCredit = 0; ?>
                                        @foreach($entries as $entry)

                                            @if($entry['trans_type'] == "1")
                                                <?php $total += $entry['amount'] ?>
                                            @else
                                                <?php $total -= $entry['amount'] ?>
                                            @endif


                                            <tr class="edit-row" data-value="{{$entry['id']}}">
                                                <td>
                                                    @if($entry['status'] === 0)
                                                        <input type="checkbox" name="ids[]" id="ids"
                                                               value="{{$entry['id']}}"/>
                                                    @endif
                                                </td>
                                                <td>{{ $entry['date']}}</td>
                                                <td>{{ $entry['numb']}}</td>
                                                <td>{{ $entry['description'] }}</td>
                                                <td>{{ formatNumber($entry['amount']) }}</td>
                                                <td>
                                                    @if($entry['trans_mode'] == "1")
                                                        @if($entry['trans_type'] == "1")
                                                            <?php $totalCash += $entry['amount'] ?>
                                                        @else
                                                            <?php $totalCash -= $entry['amount'] ?>
                                                        @endif
                                                        {{ formatNumber($entry['amount']) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($entry['trans_mode'] == "2")
                                                        @if($entry['trans_type'] == "1")
                                                            <?php $totalBank += $entry['amount'] ?>
                                                        @else
                                                            <?php $totalBank -= $entry['amount'] ?>
                                                        @endif
                                                        {{ formatNumber($entry['amount']) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($entry['trans_mode'] == "3")
                                                        @if($entry['trans_type'] == "1")
                                                            <?php $totalCredit += $entry['amount'] ?>
                                                        @else
                                                            <?php $totalCredit -= $entry['amount'] ?>
                                                        @endif
                                                        {{ formatNumber($entry['amount']) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(canEdit())
                                                        <div class="btn-group btn-group-xs">
                                                            <button type="button" class="btn btn-default edit"
                                                                    href="#section2"
                                                                    data-trans-type="{{ $entry['trans_type'] }}"
                                                                    data-trans-mode="{{ $entry['trans_mode'] }}"
                                                                    data-url-edit="{{ $entry['view'] }}"
                                                                    data-url-delete="{{ $entry['delete'] }}"
                                                                    data-value="{{ $entry['id'] }}"><i
                                                                        class="fa fa-edit"></i></button>
                                                            <button type="button" class="btn btn-danger delete"
                                                                    data-url-edit="{{ $entry['view'] }}"
                                                                    data-url-delete="{{ $entry['delete'] }}"
                                                                    data-value="{{ $entry['id'] }}"><i
                                                                        class="fa fa-trash-o"></i></button>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr class="edit-row">
                                            <td></td>
                                            <td></td>
                                            <td colspan="2">Total Amount</td>
                                            <td>{{formatNumber($total)}}</td>
                                            <td>{{formatNumber($totalCash)}}</td>
                                            <td>{{formatNumber($totalBank)}}</td>
                                            <td>
                                                @if($search == "1")
                                                    {{formatNumber($totalCredit)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @else
                                        <tr class="no-income">
                                            <td colspan="9">No notebook item has been registered.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">

                    </div>
                </footer>
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js"></script>
    <script src="{{ asset('js/scripts/notebook-register.js') }}" type="text/javascript"></script>
@stop

