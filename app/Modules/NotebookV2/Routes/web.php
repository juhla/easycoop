<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'notebook-v2', 'middleware' => ['web','verify_license', 'auth:web']], function () {
    Route::any('/', ['as' => 'notebook-register', 'uses' => 'RegisterController@index']);
    //Route::post('/filter', ['as' => 'notebook-filter', 'uses' => 'RegisterController@filter']);
});
//Route::any('/notebook', ['middleware' => ['web', 'auth:web'], 'uses' => 'RegisterController@index']);
