<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_gsettings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_gsettings');
            $table->string('company_name', 150);
            $table->string('logo', 150);
            $table->string('full_path', 225);
            $table->string('email', 100);
            $table->text('address');
            $table->string('city', 200);
            $table->integer('country_id');
            $table->string('active_language', 200);
            $table->string('phone', 200);
            $table->string('mobile', 100);
            $table->string('hotline', 100);
            $table->string('fax', 100);
            $table->string('website', 100);
            $table->string('currency', 200);
            $table->timestamps();
        });

        Schema::create('tbl_working_days', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('working_days_id');
            $table->integer('day_id');
            $table->tinyInteger('flag');
            $table->timestamps();
        });

        Schema::create('tbl_days', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('day_id');
            $table->string('day', 100);
            $table->timestamps();

        });

        Schema::create('tbl_holiday', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('holiday_id');
            $table->string('event_name', 200);
            $table->text('description');
            $table->date('start_date');
            $table->date('end_date');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_leave_category', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('leave_category_id');
            $table->string('category', 100);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('tbl_event', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('event_id');
            $table->integer('employee_id');
            $table->string('event_name', 255);
            $table->date('start_date');
            $table->date('end_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_gsettings');
        Schema::drop('tbl_working_days');
        Schema::drop('tbl_days');
        Schema::drop('tbl_holiday');
        Schema::drop('tbl_leave_category');
        Schema::drop('tbl_event');
    }
}
