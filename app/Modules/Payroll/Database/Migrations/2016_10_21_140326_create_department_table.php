<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            if (!Schema::hasTable('tbl_department')) {
                Schema::create('tbl_department', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('department_id');
                    $table->string('department_name', 100);
                    $table->softDeletes();
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_designations')) {
                Schema::create('tbl_designations', function ($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('designations_id');
                    $table->integer('department_id');
                    $table->string('designations', 100);
                    $table->string('basic_salary', 100);
                    $table->string('house_rent_allowance', 100);
                    $table->string('transportation_allowance', 100);
                    $table->string('dressing_allowance', 100);
                    $table->softDeletes();
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_department');
        Schema::drop('tbl_designations');
    }
}
