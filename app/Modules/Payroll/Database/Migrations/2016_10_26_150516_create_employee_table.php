<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('tbl_employee')) {
                Schema::create('tbl_employee', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('employee_id');
                    $table->integer('user_id');
                    $table->string('employment_id', 200)->nullable();
                    $table->integer('company_id');
                    $table->string('first_name', 100);
                    $table->string('last_name', 100);
                    $table->string('email', 225)->unique();
                    $table->date('date_of_birth')->nullable();
                    $table->string('gender', 50)->nullable();
                    $table->string('maratial_status', 20)->nullable();
                    $table->string('father_name', 100)->nullable();
                    $table->string('nationality', 100)->nullable();
                    $table->string('passport_number', 100)->nullable();
                    $table->string('photo', 150)->nullable();
                    $table->string('photo_a_path', 150)->nullable();
                    $table->text('present_address')->nullable();
                    $table->string('city', 100)->nullable();
                    $table->integer('country_id')->nullable();
                    $table->string('mobile', 100)->nullable();
                    $table->string('phone', 100)->nullable();
                    $table->integer('designations_id')->nullable();
                    $table->date('joining_date')->nullable();
                    $table->tinyInteger('status')->nullable()->comment = '1=active 2=blocked'; //default 1
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
        try {
            if (!Schema::hasTable('tbl_employee_bank')) {
                Schema::create('tbl_employee_bank', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('employee_bank_id');
                    $table->integer('employee_id');
                    $table->string('bank_name', 300);
                    $table->string('branch_name', 300);
                    $table->string('account_name', 300);
                    $table->string('account_number', 300);
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_days')) {
                Schema::create('tbl_days', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('day_id');
                    $table->string('day');
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_leave_category')) {
                Schema::create('tbl_leave_category', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('leave_category_id');
                    $table->string('category');
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_working_days')) {
                Schema::create('tbl_working_days', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('working_days_id', 255);
                    $table->integer('day_id', 255);
                    $table->integer('flag');
                    $table->softDeletes();
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_employee_document')) {
                Schema::create('tbl_employee_document', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('document_id');
                    $table->integer('employee_id');
                    $table->string('resume', 300);
                    $table->string('resume_path', 300);
                    $table->string('resume_filename', 300);
                    $table->string('offer_letter', 300);
                    $table->string('offer_letter_filename', 300);
                    $table->string('offer_letter_path', 300);
                    $table->string('joining_letter', 300);
                    $table->string('joining_letter_filename', 300);
                    $table->string('joining_letter_path', 300);
                    $table->string('contract_paper', 300);
                    $table->string('contract_paper_filename', 300);
                    $table->string('contract_paper_path', 300);
                    $table->string('id_proff', 300);
                    $table->string('id_proff_filename', 300);
                    $table->string('id_proff_path', 300);
                    $table->string('other_document', 300);
                    $table->string('other_document_filename', 300);
                    $table->string('other_document_path', 300);
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_employee_award')) {
                Schema::create('tbl_employee_award', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('employee_award_id');
                    $table->string('award_name', 100);
                    $table->integer('employee_id');
                    $table->string('gift_item', 300);
                    $table->string('award_date', 10);
                    $table->integer('award_amount');
                    $table->tinyInteger('view_status')->comment = '1=Read 2=Unread'; //COMMENT
                    $table->tinyInteger('notify_me')->comment = '1=on 0=off'; //COMMENT ,
                    $table->softDeletes();
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_employee');
        Schema::drop('tbl_employee_document');
        Schema::drop('tbl_employee_bank');
        Schema::drop('tbl_employee_award');
        Schema::drop('tbl_working_days');
        Schema::drop('tbl_leave_category');
    }
}
