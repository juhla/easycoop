<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tbl_employee_payroll', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('payroll_id');
            $table->integer('employee_id');
            $table->integer('basic_salary')->nullable();
            $table->string('house_rent_allowance', 200)->nullable();
            $table->string('medical_allowance', 200)->nullable();
            $table->string('special_allowance', 200)->nullable();
            $table->string('fuel_allowance', 200)->nullable();
            $table->string('transportation_allowance', 200)->nullable();
            $table->string('dressing_allowance', 200)->nullable();
            $table->string('phone_bill_allowance', 200)->nullable();
            $table->string('staff_loan', 200)->nullable();
            $table->string('health_insurance', 200)->nullable();
            $table->string('housing_fund', 200)->nullable();
            $table->string('other_allowance', 200)->nullable();
            $table->string('tax_deduction', 200)->nullable();
            $table->string('provident_fund', 200)->nullable();
            $table->string('other_deduction', 200)->nullable();
            $table->tinyInteger('employment_type')->comment('1=provision, 2=permanent')->nullable(); //COMMENT '1=provision, 2=permanent',
            $table->timestamps();
        });

        Schema::create('tbl_salary_payment', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('salary_payment_id');
            $table->integer('employee_id');
            $table->integer('basic_salary')->nullable();
            $table->string('house_rent_allowance', 200)->nullable();
            $table->string('medical_allowance', 200)->nullable();
            $table->string('special_allowance', 200)->nullable();
            $table->string('fuel_allowance', 200)->nullable();
            $table->string('phone_bill_allowance', 200)->nullable();
            $table->string('transportation_allowance', 200)->nullable();
            $table->string('dressing_allowance', 200)->nullable();
            $table->string('other_allowance', 200)->nullable();
            $table->string('tax_deduction', 200)->nullable();
            $table->string('provident_fund', 200)->nullable();
            $table->string('staff_loan', 200)->nullable();
            $table->string('health_insurance', 200)->nullable();
            $table->string('housing_fund', 200)->nullable();
            $table->string('other_deduction', 200)->nullable();
            $table->string('payment_for_month', 100)->nullable();
            $table->integer('award_amount')->nullable();
            $table->string('fine_deduction', 200)->nullable();
            $table->string('payment_type', 20)->nullable();
            $table->text('comments')->nullable();
            $table->timeStamp('payment_date'); //COMMENT '1=provision, 2=permanent',
            $table->timestamps();
        });

        Schema::create('tbl_salary_payslip', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('payslip_id');
            $table->string('payslip_number', 100);
            $table->integer('salary_payment_id');
            $table->timestamp('payslip_generate_date');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_employee_payroll');
        Schema::drop('tbl_salary_payment');
        Schema::drop('tbl_salary_payslip');
    }
}
