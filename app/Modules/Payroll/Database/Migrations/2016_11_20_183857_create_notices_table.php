<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_notices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('title');
            $table->text('short_description');
            $table->text('long_description');
            $table->tinyInteger('view_status');
            $table->tinyInteger('notify_me');
            $table->tinyInteger('flag');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notices');
    }
}
