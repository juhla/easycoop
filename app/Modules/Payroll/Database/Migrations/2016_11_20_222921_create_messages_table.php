<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_inbox')) {
            Schema::create('tbl_inbox', function (Blueprint $table) {
                $table->increments('id');
                $table->string('to');
                $table->string('from');
                $table->string('subject');
                $table->text('message_body');
                $table->string('attach_file');
                $table->string('attach_file_path');
                $table->string('attach_filename');
                $table->tinyInteger('view_status');
                $table->tinyInteger('notify_me');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        if (!Schema::hasTable('tbl_send')) {
            Schema::create('tbl_send', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->string('to');
                $table->string('subject');
                $table->text('message_body');
                $table->string('attach_file');
                $table->string('attach_file_path');
                $table->string('attach_filename');
                $table->timestamps();
                $table->softDeletes();
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inbox');
        Schema::drop('send');
    }
}
