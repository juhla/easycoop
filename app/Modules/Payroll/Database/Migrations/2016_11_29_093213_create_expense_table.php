<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_expense')) {
            Schema::create('tbl_expense', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('expense_id');
                $table->integer('employee_id');
                $table->string('item_name', 300);
                $table->string('purchase_from', 300);
                $table->date('purchase_date', 300);
                $table->double('amount');
                $table->string('bill_copy', 300);
                $table->text('bill_copy_filename');
                $table->string('bill_copy_path', 300);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_expense');
    }
}
