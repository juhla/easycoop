<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_attendance', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('attendance_id');
            $table->integer('employee_id');
            $table->integer('leave_category_id')->nullable();;
            $table->date('date');
            $table->tinyInteger('attendance_status')->comment = 'status 1=present 0=absent and 3= onleave'; //COMMENT ,
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_attendance');
    }
}
