<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_tax_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->double('consolidated_relief');
            $table->integer('consolidated_relief_cap');
            $table->double('gross_income_relief');
            $table->double('tax_rate1');
            $table->double('tax_rate2');
            $table->double('tax_rate3');
            $table->double('tax_rate4');
            $table->double('tax_rate5');
            $table->double('tax_rate6');
            $table->integer('tax_value1');
            $table->integer('tax_value2');
            $table->integer('tax_value3');
            $table->integer('tax_value4');
            $table->integer('tax_value5');
            $table->integer('tax_value6');
            $table->double('pension');
            $table->double('housing_fund');
            $table->double('health_insurance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_tax_settings');
    }
}
