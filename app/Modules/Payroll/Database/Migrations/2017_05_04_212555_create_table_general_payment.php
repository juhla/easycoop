<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGeneralPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_general_payment')) {
            Schema::create('tbl_general_payment', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('general_payment_id');
                $table->integer('general_payslip_id');
                $table->integer('employee_id');
                $table->string('gross');
                $table->string('net_salary', 200);
                $table->string('pension', 200)->nullable();
                $table->string('staff_loan', 200);
                $table->string('health_insurance', 200);
                $table->string('tax_deduction', 200);
                $table->string('housing_fund', 200);
                $table->string('other_deduction', 200);
                $table->integer('status')->comment = '1=paid 0=pending';
                $table->string('payment_date', 100);
                $table->timestamps();
            });
        }


        Schema::create('tbl_general_payslip', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('general_payslip_id');
            $table->string('generate_payslip_for', 100);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_general_payment');
        Schema::drop('tbl_general_payslip');
    }
}
