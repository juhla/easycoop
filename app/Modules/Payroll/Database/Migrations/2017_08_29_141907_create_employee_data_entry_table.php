<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeDataEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tbl_employee_data_entry')) {
            Schema::create('tbl_employee_data_entry', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('employee_id')->unsigned();;
                $table->enum('personal', ['0', '1'])->default('0');
                $table->enum('contact', ['0', '1'])->default('0');
                $table->enum('bank', ['0', '1'])->default('0');
                $table->enum('official', ['0', '1'])->default('0');
                $table->enum('document', ['0', '1'])->default('0');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_employee_data_entry');
    }
}
