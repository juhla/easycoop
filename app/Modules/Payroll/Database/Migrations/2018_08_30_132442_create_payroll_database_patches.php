<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollDatabasePatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        try {
            if (!Schema::hasTable('tbl_employee_data_entry')) {
                Schema::create('tbl_employee_data_entry', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('employee_id')->unsigned()->unique();
                    $table->enum('personal', ['0', '1'])->default('0');
                    $table->enum('contact', ['0', '1'])->default('0');
                    $table->enum('bank', ['0', '1'])->default('0');
                    $table->enum('official', ['0', '1'])->default('0');
                    $table->enum('document', ['0', '1'])->default('0');
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_general_payment')) {
                Schema::create('tbl_general_payment', function (Blueprint $table) {
                    $table->increments('general_payment_id');
                    $table->integer('general_payslip_id');
                    $table->integer('employee_id');
                    $table->string('gross');
                    $table->string('net_salary', 200);
                    $table->string('pension', 200);
                    $table->string('staff_loan', 200);
                    $table->string('health_insurance', 200);
                    $table->string('tax_deduction', 200);
                    $table->string('housing_fund', 200);
                    $table->string('other_deduction', 200);
                    $table->integer('status')->comment = '1=paid 0=pending';
                    $table->string('payment_date', 100);
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_general_payslip')) {
                Schema::create('tbl_general_payslip', function (Blueprint $table) {
                    $table->increments('general_payslip_id');
                    $table->string('generate_payslip_for', 100);
                    $table->timestamps();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn("tbl_employee", 'date_of_birth')) {
                Schema::table('tbl_employee', function (Blueprint $table) {
                    $table->string('date_of_birth')->nullable()->change();
                    $table->string('employment_id')->nullable()->change();
                    $table->string('gender')->nullable()->change();
                    $table->string('maratial_status')->nullable()->change();
                    $table->string('father_name')->nullable()->change();
                    $table->string('nationality')->nullable()->change();
                    $table->string('passport_number')->nullable()->change();
                    $table->string('photo')->nullable()->change();
                    $table->string('photo_a_path')->nullable()->change();
                    $table->string('present_address')->nullable()->change();
                    $table->string('city')->nullable()->change();
                    $table->string('country_id')->nullable()->change();
                    $table->string('mobile')->nullable()->change();
                    $table->string('phone')->nullable()->change();
                    $table->string('designations_id')->nullable()->change();
                    $table->string('joining_date')->nullable()->change();
                    $table->string('status')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn("tbl_application_list", 'leave_category_id')) {
                Schema::table('tbl_application_list', function (Blueprint $table) {
                    $table->string('leave_category_id')->nullable()->change();
                    $table->string('reason')->nullable()->change();
                    $table->string('start_date')->nullable()->change();
                    $table->string('end_date')->nullable()->change();
                    $table->string('status')->nullable()->change();
                    $table->string('view_status')->nullable()->change();
                    $table->string('notify_me')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {

            if (Schema::hasColumn("tbl_notices", 'view_status')) {
                Schema::table('tbl_notices', function (Blueprint $table) {
                    $table->string('title')->nullable()->change();
                    $table->string('short_description')->nullable()->change();
                    $table->string('long_description')->nullable()->change();
                    $table->string('view_status')->nullable()->change();
                    $table->string('notify_me')->nullable()->change();
                    $table->string('flag')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('tbl_gsettings', 'prorate_salary')) {
                DB::connection('tenant_conn')->statement("ALTER TABLE tbl_gsettings MODIFY prorate_salary ENUM('1', '0') NULL default '1'");
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('tbl_salary_payment', 'salary_adjustment')) {
                Schema::table('tbl_salary_payment', function (Blueprint $table) {
                    $table->string('salary_adjustment')->default(0)->change();
                });
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasColumn('tbl_salary_payment', 'basic_salary')) {
                Schema::table('tbl_salary_payment', function (Blueprint $table) {
                    $table->string('basic_salary')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }


    public function down()
    {
        Schema::dropIfExists('tbl_employee_data_entry');
        Schema::dropIfExists('tbl_general_payment');
        Schema::dropIfExists('tbl_general_payslip');
    }
}
