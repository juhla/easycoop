<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditReasonFromApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasColumn('tbl_application_list', 'reason')) {
                Schema::table('tbl_application_list', function (Blueprint $table) {
                    $table->text('reason')->nullable()->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_application_list', function (Blueprint $table) {
            $table->string('reason')->nullable()->change();
        });
    }
}
