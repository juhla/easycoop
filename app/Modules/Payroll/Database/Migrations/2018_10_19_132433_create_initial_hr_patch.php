<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitialHrPatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasTable('tbl_designations')) {
                Schema::create('tbl_designations', function ($table) {
                    $table->engine = 'InnoDB';
                    $table->increments('designations_id');
                    $table->integer('department_id');
                    $table->string('designations', 100);
                    $table->string('basic_salary', 100);
                    $table->string('house_rent_allowance', 100);
                    $table->string('transportation_allowance', 100);
                    $table->string('dressing_allowance', 100);
                    $table->softDeletes();
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_general_payment')) {
                Schema::create('tbl_general_payment', function (Blueprint $table) {
                    $table->increments('general_payment_id');
                    $table->integer('general_payslip_id');
                    $table->integer('employee_id');
                    $table->string('gross');
                    $table->string('net_salary', 200);
                    $table->string('pension', 200);
                    $table->string('staff_loan', 200);
                    $table->string('health_insurance', 200);
                    $table->string('tax_deduction', 200);
                    $table->string('housing_fund', 200);
                    $table->string('other_deduction', 200);
                    $table->integer('status')->comment = '1=paid 0=pending';
                    $table->string('payment_date', 100);
                    $table->timestamps();

                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (!Schema::hasTable('tbl_general_payslip')) {
                Schema::create('tbl_general_payslip', function (Blueprint $table) {
                    $table->increments('general_payslip_id');
                    $table->string('generate_payslip_for', 100);
                    $table->timestamps();
                });
            }

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
