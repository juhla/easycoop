<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('tbl_application_types', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name', 150);
                $table->timestamps();
            });
        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }

        try {

            Schema::create('tbl_application_processes', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('application_type_id');
                $table->string('name', 150);
                $table->timestamps();
            });

        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }


        try {
            if (!Schema::hasTable('tbl_assign_application_processes')) {
                Schema::create('tbl_assign_application_processes', function (Blueprint $table) {
                    $table->engine = 'InnoDB';
                    $table->increments('id');
                    $table->integer('employee_id');
                    $table->integer('application_process_id');
                    $table->timestamps();
                });
            }


        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }


        try {

            Schema::create('tbl_application_responses', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('employee_id');
                $table->integer('assign_application_processes_id');
                $table->integer('application_id');
                $table->integer('application_process_id');
                $table->string('response', 150);
                $table->text('comment');
                $table->timestamps();
            });

        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }


        try {
            if (!Schema::hasColumn('tbl_application_list', 'application_type_id')) {
                Schema::table('tbl_application_list', function (Blueprint $table) {
                    $table->string('application_type_id')->nullable();
                    $table->dropColumn('leave_category_id');
                });
            }

        } catch (Exception $ex) {
            app('sentry')->captureException($ex);
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_application_types');
        Schema::drop('tbl_assign_application_processes');
        Schema::drop('tbl_application_responses');
        Schema::drop('tbl_application_processes');
        Schema::table('tbl_application_list', function (Blueprint $table) {
            $table->dropColumn('application_type_id');
            $table->dropColumn('department_id');
            $table->string('leave_category_id')->nullable();
        });
    }
}
