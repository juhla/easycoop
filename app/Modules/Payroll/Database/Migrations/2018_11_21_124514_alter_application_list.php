<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterApplicationList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (!Schema::hasColumn('tbl_application_list', 'application_type_id')) {
                Schema::table('tbl_application_list', function (Blueprint $table) {
                    $table->string('application_type_id')->nullable();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasColumn('tbl_application_list', 'leave_category_id')) {
                Schema::table('tbl_application_list', function (Blueprint $table) {
                    $table->dropColumn('leave_category_id');
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_application_list', function (Blueprint $table) {
            $table->dropColumn('application_type_id');
            $table->string('leave_category_id');
        });
    }
}
