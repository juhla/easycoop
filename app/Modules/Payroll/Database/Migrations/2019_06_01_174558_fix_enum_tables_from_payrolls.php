<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FixEnumTablesFromPayrolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('tbl_employee_data_entry')) {
                DB::statement('ALTER TABLE tbl_employee_data_entry MODIFY personal varchar(191)');
                DB::statement('ALTER TABLE tbl_employee_data_entry MODIFY contact varchar(191)');
                DB::statement('ALTER TABLE tbl_employee_data_entry MODIFY bank varchar(191)');
                DB::statement('ALTER TABLE tbl_employee_data_entry MODIFY official varchar(191)');
                DB::statement('ALTER TABLE tbl_employee_data_entry MODIFY document varchar(191)');
                Schema::table('tbl_employee_data_entry', function (Blueprint $table) {
                    $table->string('personal')->default(0)->change();
                    $table->string('contact')->default(0)->change();
                    $table->string('bank')->default(0)->change();
                    $table->string('official')->default(0)->change();
                    $table->string('document')->default(0)->change();
                });
            }

            if (Schema::hasTable('tbl_gsettings')) {
                DB::statement('ALTER TABLE tbl_gsettings MODIFY prorate_salary varchar(191)');
                Schema::table('tbl_gsettings', function (Blueprint $table) {
                    $table->string('prorate_salary')->default(1)->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
