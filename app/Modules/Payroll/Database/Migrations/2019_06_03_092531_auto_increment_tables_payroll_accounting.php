<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AutoIncrementTablesPayrollAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $nonColumns = ['permission_role', 'role_user'];
        $tables = DB::getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $table) {
            if (!in_array($table, $nonColumns)) {
                try {
                    $hasColumn = Schema::hasColumn($table, 'id');
                    if ($hasColumn) {
                        //DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
                        $col = DB::getSchemaBuilder()->getColumnType($table, 'id');
                        $isColumnType = ($col == 'integer') ? true : false;
                        if ($isColumnType) {
                            Schema::table($table, function (Blueprint $table) {
                                $table->bigIncrements('id')->change();
                            });
                        }
                    }
                } catch (\Exception $e) {
                    $data = [
                        'extra' => [
                            'table' => $table,
                        ]
                    ];
                    app('sentry')->captureException($e, $data);
                }
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
