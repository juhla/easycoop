<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixIncrementTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            if (Schema::hasTable('tbl_attendance')) {
                Schema::table('tbl_attendance', function (Blueprint $table) {
                    $table->bigIncrements('attendance_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_days')) {
                Schema::table('tbl_days', function (Blueprint $table) {
                    $table->bigIncrements('day_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_department')) {
                Schema::table('tbl_department', function (Blueprint $table) {
                    $table->bigIncrements('department_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_employee')) {
                Schema::table('tbl_employee', function (Blueprint $table) {
                    $table->bigIncrements('employee_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_employee_award')) {
                Schema::table('tbl_employee_award', function (Blueprint $table) {
                    $table->bigIncrements('employee_award_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_employee_bank')) {
                Schema::table('tbl_employee_bank', function (Blueprint $table) {
                    $table->bigIncrements('employee_bank_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_employee_document')) {
                Schema::table('tbl_employee_document', function (Blueprint $table) {
                    $table->bigIncrements('document_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_employee_payroll')) {
                Schema::table('tbl_employee_payroll', function (Blueprint $table) {
                    $table->bigIncrements('payroll_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_event')) {
                Schema::table('tbl_event', function (Blueprint $table) {
                    $table->bigIncrements('event_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_expense')) {
                Schema::table('tbl_expense', function (Blueprint $table) {
                    $table->bigIncrements('expense_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_general_payment')) {
                Schema::table('tbl_general_payment', function (Blueprint $table) {
                    $table->bigIncrements('general_payment_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_general_payslip')) {
                Schema::table('tbl_general_payslip', function (Blueprint $table) {
                    $table->bigIncrements('general_payslip_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_gsettings')) {
                Schema::table('tbl_gsettings', function (Blueprint $table) {
                    $table->bigIncrements('id_gsettings')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_holidays')) {
                Schema::table('tbl_holidays', function (Blueprint $table) {
                    $table->bigIncrements('holiday_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_leave_category')) {
                Schema::table('tbl_leave_category', function (Blueprint $table) {
                    $table->bigIncrements('leave_category_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_leave_category')) {
                Schema::table('tbl_leave_category', function (Blueprint $table) {
                    $table->bigIncrements('leave_category_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_salary_payment')) {
                Schema::table('tbl_salary_payment', function (Blueprint $table) {
                    $table->bigIncrements('salary_payment_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_salary_payslip')) {
                Schema::table('tbl_salary_payslip', function (Blueprint $table) {
                    $table->bigIncrements('payslip_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            if (Schema::hasTable('tbl_working_days')) {
                Schema::table('tbl_working_days', function (Blueprint $table) {
                    $table->bigIncrements('working_days_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }


        try {
            if (Schema::hasTable('tbl_designations')) {
                Schema::table('tbl_designations', function (Blueprint $table) {
                    $table->bigIncrements('designations_id')->change();
                });
            }
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
