<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixSomePayrollTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tbl_expense')) {
            try {
                Schema::table('tbl_expense', function (Blueprint $table) {
                    $table->string('bill_copy')->nullable()->change();
                    $table->string('bill_copy_filename')->nullable()->change();
                    $table->string('bill_copy_path')->nullable()->change();
                });
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
