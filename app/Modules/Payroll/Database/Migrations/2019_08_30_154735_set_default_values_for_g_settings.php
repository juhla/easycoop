<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class SetDefaultValuesForGSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tbl_gsettings')) {
            try {
                $data = DB::connection('tenant_conn')->table('tbl_gsettings')->get();
                if (count($data) < 1) {
                    DB::connection('tenant_conn')->table('tbl_gsettings')->insert([
                        'company_name' => "Default",
                        'logo' => "Default",
                        'full_path' => "Default",
                        'email' => "Default",
                        'address' => "Default",
                        'city' => "Default",
                        'country_id' => "0",
                        'active_language' => "Default",
                        'phone' => "Default",
                        'mobile' => "Default",
                        'hotline' => "Default",
                        'website' => "Default",
                        'fax' => "Default",
                        'currency' => "Default",
                        'id' => "1"
                    ]);
                }
            } catch (\Exception $e) {
                app('sentry')->captureException($e);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
