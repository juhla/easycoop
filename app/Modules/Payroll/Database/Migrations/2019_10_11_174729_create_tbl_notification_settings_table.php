<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTblNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::connection('tenant_conn')->beginTransaction();
            Schema::create('tbl_notification_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('email')->default(0)->nullable();
                $table->integer('notice')->default(0)->nullable();
                $table->integer('leave_application')->default(0)->nullable();
                $table->timestamps();
            });
            DB::connection('tenant_conn')->table('tbl_notification_settings')->insert([
                'email' => 0,
                'notice' => 0,
                'leave_application' => 0,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            app('sentry')->captureException($e);
        }
        DB::connection('tenant_conn')->commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_notification_settings');
    }
}
