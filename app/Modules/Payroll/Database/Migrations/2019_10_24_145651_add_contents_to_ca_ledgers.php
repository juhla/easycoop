<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Modules\Base\Models\Ledger;

class AddContentsToCaLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        try {
            //check if user has Payable ledger
            $res = Ledger::whereCode('2220.1')->get();
            if ($res->isEmpty()) {
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Pension', 'code' => '2220.1', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Staff Loan', 'code' => '2220.2', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Health Insurance', 'code' => '2220.3', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Housing Fund', 'code' => '2220.4', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - PAYE', 'code' => '2220.5', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Other Deduction', 'code' => '2220.6', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
                Ledger::create(['group_id' => '29', 'name' => 'Payable - Salary', 'code' => '2220.7', 'opening_balance_type' => 'C', 'is_system_account' => '1']);
            }
        } catch (Exception $exception) {

        } catch (Throwable $exception) {

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ca_ledgers', function (Blueprint $table) {
            //
        });
    }
}
