<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 2019-07-12
 * Time: 13:42
 */

namespace App\Modules\Payroll\Database\Seeds;

use Illuminate\Database\Seeder;

class PayrollDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PayrollGeneralSettingSeeder::class);
    }
}