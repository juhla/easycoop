<?php

namespace App\Modules\Payroll\Http\Controllers;

use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Models\Collaborator;
use App\PayrollSetup;
use App\Modules\Base\Models\PayrollLicenceSubscriber;
use App\Modules\Base\Traits\License;
use Illuminate\Database\QueryException;
use Request;
use Auth;
use Redirect;
use Session;
use DB;
use Schema;

class PayrollController extends Controller
{
    use License;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function setup()
    {
        // ** checking if the business owner has clicked HRM
        $user = auth()->user();
        $businessOwnerId = getBusinessOwnerID(); //18
        //checking the record that would have been created if the business owner has clicked HRM
        $licenceSubscriber = PayrollLicenceSubscriber::where('user_id', $businessOwnerId)->get();
        $businessOwnerRole = $user->hasRole(['business-owner', 'advanced-business-owner', 'basic-business-owner']);
        if (!$businessOwnerRole && $licenceSubscriber->isEmpty()) {
            return back()->with('status', 'Please call your business owner to activate Payroll for you');
        }
        // ** end checking if the business owner has clicked HRM
        $this->hasAccess(['payroll']);
        //called only by business owner
        //to setup/login


        $payroll_url = config('constants.payroll_url');
        $payroll = $payroll_url . "secure/sign-in?token=" . session()->get('auth.token');
        //dd($payroll);
        if (!$this->isUserPayrollActivated()) {
            //no valid or have expired license
            flash()->error($this->licenseError());
            return redirect('membership/plans');
        }
        //test if user have already activated payroll
        $this->ensurePayrollTablesAreAvailable();
        return Redirect::to($payroll);
    }

    public function login()
    {

        $user = auth()->user();
        if (!$user->hasRole(['employee-payroll', 'admin-payroll'])) {
            $this->hasAccess(['payroll']);
        }

        //used only by employee & admin-payroll
        $payroll_url = config('constants.payroll_url');


        //check if user has an active licence
        if (!$this->isUserPayrollActivated()) {
            //no valid or have expired license
            flash()->error($this->licenseError());
            return redirect('secure/sign-in');
        }

        $payroll = $payroll_url . "secure/sign-in?token=" . session()->get('auth.token');

        //test if user have already activated payroll
        $this->ensurePayrollTablesAreAvailable();
        return Redirect::to($payroll);
    }


    private function ensurePayrollTablesAreAvailable()
    {
        event(new \App\Modules\Payroll\Events\PayrollSetup());
    }
}
