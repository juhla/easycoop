<?php

namespace App\Modules\Payroll\Listeners;

use App\Modules\Payroll\Events\PayrollSetup;
use App\Modules\Base\Models\Company;
use App\Modules\Base\Traits\AfterLogin;
use DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;

class PayrollSetupListener
{

    use AfterLogin;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PayrollSetup $event
     * @return void
     */
    public function handle(PayrollSetup $event)
    {
        $company = Company::whereUser_id(getBusinessOwnerID())->get()->first();
        $database = $company->database;
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');
        $host = Config::get('database.connections.mysql.host');
        $this->connectToDatabase($host, $username, $password, $database);
        $this->runPatchMigrations();
    }

    private function connectToDatabase($host, $username, $password, $database)
    {
        Config::set('database.connections.tenant_conn.host', $host);
        Config::set('database.connections.tenant_conn.username', $username);
        Config::set('database.connections.tenant_conn.password', $password);
        Config::set('database.connections.tenant_conn.database', $database);

        try {
            DB::reconnect('tenant_conn');
            return true;

        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            return false;
        }
    }

    private function runPatchMigrations()
    {
        try {
            Artisan::call('module:migrate', [
                'slug' => 'payroll',
                '--database' => 'tenant_conn',
                '--force' => true,
            ]);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
        }

        try {
            $res = \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::where('day_id', '1')->first();
            if (empty($res)) {
                Artisan::call('module:seed', [
                        'slug' => 'payroll',
                        '--force' => true,
                    ]
                );
            }
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }


    }
}
