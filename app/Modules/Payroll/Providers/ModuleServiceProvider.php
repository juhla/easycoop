<?php

namespace App\Modules\Payroll\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('payroll', 'Resources/Lang', 'app'), 'payroll');
        $this->loadViewsFrom(module_path('payroll', 'Resources/Views', 'app'), 'payroll');
        $this->loadMigrationsFrom(module_path('payroll', 'Database/Migrations', 'app'), 'payroll');
        //$this->loadConfigsFrom(module_path('payroll', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('payroll', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
    }
}
