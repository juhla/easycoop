<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'payroll', 'middleware' => ['web', 'verify_license'] ], function () {
    Route::get('/create', ['uses' => 'PayrollController@setup']);
    Route::get('/login', ['uses' => 'PayrollController@login']);
});