<?php

namespace App\Modules\PriceBundle\Http\Controllers;

use App\Modules\PriceBundle\Models\Collaborator;
use App\Modules\PriceBundle\Models\ImsLicenceSubscriber;
use App\Modules\PriceBundle\Models\Payment;
use App\Modules\PriceBundle\Models\PayrollLicenceSubscriber;
use App\Modules\PriceBundle\Models\Role;
use App\Modules\PriceBundle\Models\UserLicenceSubscriber;
use App\Modules\PriceBundle\Models\UserLicensePackage;
use App\Modules\PriceBundle\Models\UserPriceBundle;
use App\Modules\Base\Traits\License;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Modules\Base\Http\Requests;
use App\Modules\Base\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mail;

class PriceBundleController extends Controller
{
    use License;

    public function index(){
        //get user role
        $userLicense = new UserLicensePackage();
        $userLicenceSubscriber = new UserLicenceSubscriber();

        UserLicenceSubscriber::updateTheCount(getBusinessOwnerID());
        $role = getUserRole(auth()->user()->id);
        $hasSubscribed = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();
        if (empty($hasSubscribed)) {
            $theLicense = $userLicense->findDefaultLicenceByRole($role->id);
            $userLicenceSubscriber->user_id = auth()->user()->id;
            $userLicenceSubscriber->license_id = $theLicense->id;
            $license_id = $theLicense->id;
            $userLicenceSubscriber->save();
        } else {
            $license_id = $hasSubscribed->license_id;
        }
        $plan = $userLicense->find($license_id);

        $userID = getBusinessOwnerID();
        //  "employee-payroll", "admin-payroll", "employee-ims"
        $clients = Collaborator::where('user_id', $userID)->where('flag', 'Active')->groupBy('user_id', 'client_id')->
        whereHas('client.roles', function ($q) {
            $q->whereNotIn('name', ["employee-payroll", "admin-payroll", "employee-ims"]);
        })->with('client')
            ->get();


        $subscriber = $userLicenceSubscriber->where('user_id', auth()->user()->id)->first();
        return view('price-bundle::user-licenses', compact('clients', 'plan', 'subscriber'));
    }

    public function plans()
    {
        $title = 'Change Plan';
        $userId = auth()->user()->id;
        $price_bundle = UserPriceBundle::whereType_id("1")->get();

        foreach ($price_bundle as $each_price_bundle) {
            if($each_price_bundle->name == "Basic"){ $bundles['basic'][] = $each_price_bundle;}
            else if($each_price_bundle->name == "Silver"){ $bundles['sliver'][] = $each_price_bundle;}
            else if($each_price_bundle->name == "Gold"){ $bundles['gold'][] = $each_price_bundle;}
            else if($each_price_bundle->name == "Diamond"){ $bundles['diamond'][] = $each_price_bundle;}
            else if($each_price_bundle->name == "Platinum"){ $bundles['platinum'][]  = $each_price_bundle;}
        }

        $license_ids = $this->getUserLicenseIDs($userId);
        if(sizeof($license_ids) == 0 ){
            $license_ids[] =0;
        }

        $data['user_current_bundle'] = $this->getUserCurrentBundle($license_ids);
        $data['title'] = $title;
        $data['bmac_bundles'] = $bundles;        
        return view('price-bundle::plans')->with($data);

    }

    public function membership_plans()
    {
        $title = 'Change Renewal Plan';
        $userId = auth()->user()->id;
        $bmac_expiry_date = "";

        try {
            $bmac = true;
            $bmac_subscriber = UserLicenceSubscriber::with('license')
                ->where('user_id', auth()->user()->id)
                ->first();
            if (empty($bmac_subscriber)) {
                $bmac = false;
            }

            $bmac_expiry_date = date('l jS \of F Y', strtotime($bmac_subscriber->expiry_date));

        } catch (\Exception $ex) {
            $bmac = false;
        }

        $license_ids = $this->getUserLicenseIDs($userId);
        if(sizeof($license_ids) == 0 ){
            $license_ids[] =0;
        }

        $price_bundle = UserPriceBundle::whereType_id("1")->get();
        $user_current_bundle = $this->getUserCurrentBundle($license_ids);


        foreach ($price_bundle as $each_price_bundle) {
            if($each_price_bundle->name == ucfirst($user_current_bundle)){
                $bundles[] = $each_price_bundle;
                $renewal_price = $each_price_bundle->annual_price;
            }
        }

        $data['bmac_bundles'] = $bundles;
        $data['renewal_price'] = $renewal_price;
        $data['bmac_expiry_date'] = $bmac_expiry_date;
        $data['bmac_subscriber'] = $bmac_subscriber;
        $data['has_package'] = $bmac;
        $data['title'] = $title;

        return view('price-bundle::membership_plans')->with($data);
    }


    public function subscribe ($reference_no){

        $plan_ids = request()->all();

        foreach ($plan_ids as $plan_id){
            if($plan_id ==  25){
                $this->subscribeBmac($plan_id, $reference_no);
            }else if($plan_id ==  26){
                //ims web
                $this->subscribeIms($plan_id, $reference_no);
            }else if($plan_id ==  27){
                //ims desktop $ web
                $this->subscribeIms(26, $reference_no);
                $this->subscribeIms($plan_id, $reference_no);
            }else if($plan_id ==  28){
                $this->subscribePayroll($plan_id, $reference_no);
            }
        }

        flash()->success('User license plan has been changed');
        return redirect('membership/user-licenses');
    }

    public function renewal ($reference_no){

        $plan_ids = request()->all();
        $license = ImsLicenceSubscriber::firstOrNew(array('user_id' => Auth::user()->id));
        $ims_expires_on = $license->expiry_date;
        foreach ($plan_ids as $plan_id){
            if($plan_id ==  25){
                $this->subscribeBmac($plan_id, $reference_no, 'renewal');
            }
            else if($plan_id ==  26){
                //ims web
                $this->subscribeIms($plan_id, $reference_no, 'renewal',$ims_expires_on);
            }else if($plan_id ==  27){
                //ims desktop $ web
                $this->subscribeIms(26, $reference_no, 'renewal', $ims_expires_on);
                $this->subscribeIms($plan_id, $reference_no, 'renewal',$ims_expires_on);
            }else if($plan_id ==  28){
                $this->subscribePayroll($plan_id, $reference_no, 'renewal');
            }
        }

        flash()->success('User license plan has been renewed');
        return redirect('membership/user-licenses');
    }

    private function subscribePayroll($plan_id, $reference_no, $type = null)
    {
        $plan = UserLicensePackage::find($plan_id);

        $license = PayrollLicenceSubscriber::firstOrNew(array('user_id' => Auth::user()->id,));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        if(is_null($type)){$license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        }else{$license->expiry_date = $this->setNewExpireDate($license->expiry_date);}
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->description ="made payment for HR-Payroll access";
        $payment->reference_no = $reference_no;
        $payment->save();

        $this->sendMails($plan, $reference_no);
    }

    private function subscribeIms($plan_id, $reference_no, $type = null, $expiry_date = null )
    {
        $plan = UserLicensePackage::find($plan_id);

        $license = ImsLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
            'license_id'=> $plan_id
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        if(is_null($type)){$license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        }else{$license->expiry_date = $this->setNewExpireDate($expiry_date);}
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->description ="made payment for IMS access";
        $payment->reference_no = $reference_no;
        $payment->save();

        $this->sendMails($plan, $reference_no);

    }

    private function subscribeBmac($plan_id,$reference_no, $type = null)
    {
        $plan = UserLicensePackage::find($plan_id);

        $license = UserLicenceSubscriber::firstOrNew(array(
            'user_id' => Auth::user()->id,
        ));
        $license->user_id = Auth::user()->id;
        $license->license_id = $plan_id;
        if(is_null($type)){$license->expiry_date = date('Y-m-d', strtotime("+1 years"));
        }else{$license->expiry_date = $this->setNewExpireDate($license->expiry_date);}
        $license->save();

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->license_id = $plan->id;
        $payment->amount = $plan->price;
        $payment->description ="made payment for BMAC access";
        $payment->reference_no = $reference_no;
        $payment->save();
        $this->sendMails($plan, $reference_no);

    }

    private function setNewExpireDate($expireDate){
        try{
            $new_date = Carbon::createFromFormat('Y-m-d H:i:s', $expireDate)->addYear(1);
        }catch (\Exception $ex){
            $new_date = Carbon::createFromFormat('Y-m-d', $expireDate)->addYear(1);
        }
      return $new_date;

    }

    private function sendMails($plan, $reference_no)
    {
        try {
            $data['name'] = auth()->user()->company->company_name;
            $data['reference_no'] = $reference_no;
            $data['package'] = $plan->title;
            $data['amount'] = $plan->price;
            $data['email'] = auth()->user()->email;

            Mail::send('emails.receipt', $data, function ($m) use ($data) {
                $m->from('no-reply@onebmac.com', 'BMAC Team');

                $m->to($data['email'], $data['name'])->subject($data['name'] . ' thank you for your order');
            });

        } catch (\Exception $e) {
            flash()->error($e->getMessage());
        }

    }

    private function getUserCurrentBundle($userPackages){

        $bundleType = null;
        if(count(array_intersect($userPackages,[25,26, 27, 28] )) == 4){
            $bundleType =  "platinum";
        } else if(count(array_intersect($userPackages,[25,26, 27] )) == 3){
            $bundleType =  "gold";
        }
        else if(count(array_intersect($userPackages,[25,26] )) == 2){
            $bundleType =  "silver";
        }
        else if(count(array_intersect($userPackages,[25,28] )) == 2){
            $bundleType =  "diamond";
        }else if(count(array_intersect($userPackages,[25] )) == 1){
            $bundleType = 'basic';
        }

        return $bundleType;
    }

    private function getUserLicenseIDs($userId){

        $subscriber = UserLicenceSubscriber::whereUser_id($userId)->first();
        if(!empty($subscriber->license_id)){$license_id[] = $subscriber->license_id;}

        $subscriberPayroll = PayrollLicenceSubscriber::whereUser_id($userId)->first();
        if(!empty($subscriberPayroll->license_id)){$license_id[] = $subscriberPayroll->license_id;}

        $subscriberIMS = ImsLicenceSubscriber::whereUser_id($userId)->get();
        if(!$subscriberIMS->isEmpty()){
            foreach ($subscriberIMS as $ims){
                $license_id[] = $ims->license_id;
            }
        }

        return $license_id;
    }
}
