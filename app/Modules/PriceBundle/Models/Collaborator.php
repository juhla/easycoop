<?php

namespace App\Modules\PriceBundle\Models;

use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Model;

class Collaborator extends Model
{
    use NullingDB;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';
    protected $table = 'collaborators';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'company_id', 'client_id', 'access', 'permission'];


//    public function company()
//    {
//        return $this->belongsTo('App\Modules\PriceBundle\Models\Company', 'company_id');
//    }

    public function client()
    {
        return $this->belongsTo('App\Modules\PriceBundle\Models\User', 'client_id');
    }

    //user by business owner
//    public function personalInfo()
//    {
//        return $this->belongsTo('App\Modules\PriceBundle\Models\PersonalInfo', 'client_id', 'user_id');
//    }

    //user by profession
//    public function personalInfo2()
//    {
//        return $this->belongsTo('App\Modules\PriceBundle\Models\PersonalInfo', 'user_id', 'user_id');
//    }


//    public function employer()
//    {
//        return $this->belongsTo('App\Modules\PriceBundle\Models\User', 'user_id');
//    }

    private function addAccess()
    {
        $clients = $this->where('access', NULL)->get();
        foreach ($clients as $eachOne) {
            $isFree = $this->check($eachOne->user_id);
            $access = $isFree ? "Free" : "Paid";
            Collaborator::find($eachOne->id)->update(['access' => $access]);
        }
    }

    public function getSmesByClientId($clientID)
    {
        //$clientID = 26;
        $user = Company::where('collaborators.client_id', $clientID)
            ->where('collaborators.flag', 'Active')
            ->join('collaborators', 'companies.user_id', '=', 'collaborators.user_id')
            ->select('collaborators.user_id as id', 'companies.company_name as name')
            ->get();
        return $user;
    }

    public function check($_users)
    {
        $role = getUserRole($_users);
        $userLicence = new UserLicensePackage();
        $defaultLicence = $userLicence->findDefaultLicenceByRole($role->id);
        $noOfUsers = $defaultLicence->no_of_users;
        $theUser = $this->where('user_id', $_users)->get()->count();
        return ($theUser < $noOfUsers);
    }

    public function checkFree($_users)
    {
        //$this->addAccess();
        return $this->check($_users);

    }
}
