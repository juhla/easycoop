<?php

namespace App\Modules\PriceBundle\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;


class Payment extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'payments';

    protected $fillable = ['user_id', 'license_id', 'amount', 'reference_no','description'];
}
