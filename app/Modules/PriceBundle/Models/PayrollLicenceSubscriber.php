<?php

namespace App\Modules\PriceBundle\Models;

use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Model;

class PayrollLicenceSubscriber extends Model
{
    use NullingDB;
    protected $fillable = ['user_id', 'license_id', 'expiry_date', 'count'];
    protected $connection = 'mysql';
    protected $table = 'payroll_license_subscribers';

    public function __construct()
    {
        parent::__construct();
        //self::createTableifNonExist();
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function role()
    {
        return $this->belongsTo('App\Modules\PriceBundle\Models\Role', 'role_id');
    }

    public function license()
    {
        return $this->belongsTo('App\Modules\PriceBundle\Models\UserLicensePackage', 'license_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Modules\PriceBundle\Models\User', 'user_id');
    }

    public function findDefaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicensePackage::where('role_id', $role)->where('is_default', 1)->first();
    }

    public static function isExist()
    {
        try {
            PayrollLicenceSubscriber::get()->first();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
