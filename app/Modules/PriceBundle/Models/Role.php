<?php

namespace App\Modules\PriceBundle\Models;

use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use NullingDB;
    //
    protected $fillable = ['display_name', 'category', 'name', 'description', 'is_admin', 'flag'];
    protected $connection = 'mysql';

    public function users()
    {
        return $this->belongsToMany('App\Modules\PriceBundle\Models\User', 'role_user');
    }

    public static function getBusinessOwnerUser()
    {
        return Role::where('category', "business-owner")->get(['id', 'name', 'display_name'])->toArray();
    }

    public static function findRoleCategory($role)
    {
        $category = Role::find($role)->category;
        return Role::where('name', $category)->pluck('id')->first();
    }


    public static function findUserRoles()
    {
        return Role::groupby('category')->distinct()->get();
    }
}
