<?php

namespace App\Modules\PriceBundle\Models;

use Illuminate\Support\Facades\Auth;
use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model
{
    use NullingDB;
    use EntrustUserTrait;
    //use Auditable;

    protected $connection = 'mysql';
    protected $table = 'users';

    public function getTableName()
    {
        return $this->table;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'active', 'confirmed', 'online_status', 'confirmation_code', 'mobile_confirmation_code', 'user_hash', 'last_active_time', 'last_login', 'last_login_ip', 'flag', 'remember_token', 'provider' , 'session_id', 'is_first_login', 'profile_pic'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    public static function resolveId()
//    {
//        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
//    }


    /**
     * Establish relationship between Users and PersonalInfo models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
//    public function personal_info()
//    {
//        return $this->hasOne('App\Modules\Models\PersonalInfo', 'user_id');
//    }

    /**
     * Establish relationship between Users and Company models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
//    public function company()
//    {
//        return $this->hasOne('App\Modules\PriceBundle\Models\Company', 'user_id');
//    }

    /**
     * Establish relationship between Users and Collaborator models
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function collaborations()
    {
        return $this->hasMany('App\Modules\PriceBundle\Models\Company', 'user_id', 'client_id');
    }


    /**
     * Get the roles a user has
     */
    public function roles()
    {
        return $this->belongsToMany('App\Modules\PriceBundle\Models\Role', 'role_user');
    }


    /**
     * Update user last login time
     */
    public function updateOnline()
    {
        $this->last_active_time = time();
        $this->save();
    }

    /**
     * update user online status
     * @param $s
     * @return bool
     */
    public function updateStatus($s)
    {
        if (!Auth::user()) return false;
        $this->online_status = $s;

        $this->save();
    }

    /**
     * Check if the user has activated his account
     * @return bool
     */
    public function isVerified()
    {
        if ($this->confirmed == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the account is active
     * @return bool
     */
    public function isActive()
    {
        if ($this->active == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function displayName()
    {
        try {
            if (empty($this->personal_info->first_name) && empty($this->personal_info->last_name)) {
                return $this->email;
            }
            return $this->personal_info->first_name . ' ' . $this->personal_info->last_name;
        } catch (\Exception $e) {
            return $this->email;
            //return '';
        }
    }

    /**
     * Get the name.
     *
     * Had to add this Accessor field so that we can have the ability to do something like this $userInstance->name
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->displayName();
    }

    /**
     * @param $permission
     * @return bool
     *
     */
    public function hasAccess($permission)
    {
        $permissions = @unserialize($this->permissions);
        if (!$permissions) return false;
        if (isset($permissions[$permission]) or in_array($permission, $permissions)) return true;
        return false;
    }


    public function displayAvatar()
    {
        if ($this->profile_pic) {
            return asset('uploads/thumbs/thumb_' . $this->profile_pic);
        } else {
            return asset('images/avatar_default.jpg');
        }
    }

}
