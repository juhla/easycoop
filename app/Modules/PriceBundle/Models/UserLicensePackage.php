<?php

namespace App\Modules\PriceBundle\Models;

use App\Modules\Base\Traits\MySoftDeletes;
use App\Modules\Base\Traits\NullingDB;
use Illuminate\Database\Eloquent\Model;

class UserLicensePackage extends Model
{
    use NullingDB;
    protected $connection = 'mysql';
    protected $table = 'user_license_packages';

    protected $fillable = ['role_id','title','slug','no_of_users','is_default','description','price','duration','duration_type','status','annual_price'];

    public function getTableName()
    {
        return $this->table;
    }

//    public function role()
//    {
//        return $this->belongsTo('App\Modules\Models\Role', 'role_id');
//    }

    public function findDefaultLicenceByRole($role_id)
    {
        $role = Role::findRoleCategory($role_id);
        return UserLicensePackage::where('role_id', $role)->where('is_default', 1)->first();
    }
}
