<?php

namespace App\Modules\PriceBundle\Models;

use Illuminate\Database\Eloquent\Model;

class UserPriceBundle extends Model
{
    protected $connection = 'mysql';
    protected $table = 'user_price_bundles';
    protected $fillable = ['*'];

    public function userLicensePackage()
    {
        return $this->belongsTo('App\Modules\PriceBundle\Models\UserLicensePackage', 'user_license_package_id', 'id');
    }
}
