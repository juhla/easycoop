<?php

namespace App\Modules\PriceBundle\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'price-bundle');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'price-bundle');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'price-bundle');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
