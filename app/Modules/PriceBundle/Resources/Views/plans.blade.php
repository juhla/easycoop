<?php
/**
 * Created by PhpStorm.
 * User: Seunope
 * Date: 11/15/2018
 * Time: 02:45 AM
 */
?>
@extends('layouts.main')

@section('content')
    <style>
        .pricing_table_wdg {
            border: 1px solid #c4cbcc;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            float: left;
            margin-top: 25px;
        }

        .pricing_table_wdg ul {
            list-style: none;
            float: left;
            width: 147px;
            margin: 0;
            border: 1px solid #f2f3f3;
            padding: 5px;
            text-align: center;
            background-color: #FFF;
        }

        .pricing_table_wdg ul:hover {
            -webkit-transform: scale(1.1);
            -moz-transform: scale(1.1);
            -o-transform: scale(1.1);
            -moz-box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            -webkit-box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            box-shadow: 3px 5px 7px rgba(0, 0, 0, .7);
            cursor: pointer;
            background: #d8e9f9;
        }

        .pricing_table_wdg ul li {
            border-bottom: 1px dashed #cfd2d2;
            padding: 10px 0;
        }

        .pricing_table_wdg ul li:first-child {
            color: #FFFFFF;
            font-size: 18px;
            font-weight: bold;
            background: #2e818f;
        }

        .pricing_table_wdg ul li:nth-child(2) {
            background: #fbfbfb;
        }

        .pricing_table_wdg ul li:nth-child(3) {
            font-size: 12px;
            /*font-weight: bold;*/
        }

        .pricing_table_wdg ul li:nth-child(n+4) {
            font-size: 14px;
        }

        .pricing_table_wdg ul li:last-child a {
            color: #F0F0F0;
            text-decoration: none;
            font-weight: bold;
            display: block;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border: 1px solid #c4cbcc;
            padding: 10px;
            margin: 5px 0;
            background: #0061bb; /* Old browsers */
            background: -moz-linear-gradient(top, #0061bb 0%, #164e82 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #0061bb), color-stop(100%, #164e82)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #0061bb 0%, #164e82 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #0061bb 0%, #164e82 100%); /* Opera11.10+ */
            background: -ms-linear-gradient(top, #0061bb 0%, #164e82 100%); /* IE10+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0061bb', endColorstr='#164e82', GradientType=0); /* IE6-9 */
            background: linear-gradient(top, #0061bb 0%, #164e82 100%); /* W3C */
        }

    </style>
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('membership/plans') }}" class="btn btn-info"><i class="fa fa-plus"></i>
                                Change Plan </a>


                        </div>
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('membership/maintenance/plans') }}" class="btn btn-info"><i
                                        class="fa fa-plus"></i>
                                Pay for Maintenance </a>
                        </div>

                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')

                    <h1>Bmac Starter</h1>
                    <section class="container">

                        <div class="pricing_table_wdg">
                            @foreach($bmac_bundles as $bundle_name => $value)
                                <?php $packageTotal=0; $license_plan_id = null; $planPriceQuery=null;?>
                                    <ul>
                                        @foreach($bmac_bundles[$bundle_name] as $bundle_key => $bundles)
                                            <?php $plan = $bundles->userLicensePackage; $packageTotal=$packageTotal+$plan->price; ?>
                                            @if($bundle_key == 0)
                                              <li >{{ $bundles->name }}</li>
                                            @endif
                                            <li style="background: #fbfbfb">{{ $plan->title }}</li>
                                            <li>{{ ($plan->price === '0') ? 'Free' : formatNumber($plan->price) }}</li>
                                            <li> <strong>Users: </strong> {{$plan->no_of_users}}</li>
                                            <?php $license_plan_id[]= $plan->id; ?>
                                        @endforeach
                                            <?php $planPriceQuery =  http_build_query($license_plan_id) ?>
                                            <li> <strong>Total:  {{formatNumber($packageTotal)}}</strong></li>

                                        @if($user_current_bundle == null )
                                            <li>
                                                <form action="#" method="post" id="payment-form-{{ $plan->id }}">
                                                    {{ csrf_field() }}
                                                    <script src="https://js.paystack.co/v1/inline.js"></script>
                                                    <a href="javascript:;"
                                                       onclick="payWithPaystack({{ $plan->id }}, {{ $packageTotal }},{{json_encode($planPriceQuery)}})"
                                                       class="buy_now" data-value="{{ $packageTotal }}"> Buy Now </a>
                                                </form>
                                            </li>
                                        @else
                                            @if($user_current_bundle == $bundle_name )
                                                <li><a href="#" style="background: red" class="buy_now" disabled>Current Plan</a></li>
                                            @else
                                                <li>
                                                    <form action="#" method="post" id="payment-form-{{ $plan->id }}">
                                                        {{ csrf_field() }}
                                                        <a href="#" style="background: grey" data-value="{{ $packageTotal}}"> Buy Now </a>
                                                    </form>
                                                </li>
                                            @endif
                                        @endif

                                    </ul>


                            @endforeach
                        </div>
                    </section>
                </section>
            </section>
        </aside>
    </section>
    <script>
        function payWithPaystack(id, amount, packagesID) {
            var baseurl = $('#baseurl').val();
            var handler = PaystackPop.setup({
                key: '{{ config('services.paystack.key') }}',
                email: '{{ auth()->user()->email }}',
                amount: (amount * 100),
                ref: "{{ time().GenerateRandomString(10,"ALPHA") }}",
//                metadata: {
//                    custom_fields: [
//                        {
//                            display_name: "Mobile Number",
//                            variable_name: "mobile_number",
//                            value: "+2348012345678"
//                        }
//                    ]
//                },
                callback: function (response) {
                    $('#payment-form-' + id).attr('action', baseurl + '/membership/subscribe/' + response.reference + '/?'+packagesID );
                    $('#payment-form-' + id).submit();
                },
//                onClose: function(){
//                    alert('window closed');
//                }
            });
            handler.openIframe();
        }


    </script>
@stop
