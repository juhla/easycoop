<?php
/**
 * Created by PhpStorm.
 * User: Seunope
 * Date: 11/21/2018
 * Time: 4:11 PM
 */?>
@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('membership/plans') }}" class="btn btn-info"><i class="fa fa-plus"></i>
                                Change Plan </a>


                        </div>
                        <div class="col-sm-3 m-b-xs">
                            <a href="{{ url('membership/maintenance/plans') }}" class="btn btn-info"><i
                                        class="fa fa-plus"></i>
                                Pay for Maintenance </a>
                        </div>

                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">

                        <div class="table-responsive">
                            <h2>Users on this plan ({{ $subscriber->count.' / '.$plan->no_of_users }})</h2>
                            <p>The number of users (including you) with any level of access to your account.</p>
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th width="20%">Name</th>
                                    <th width="15%">Role(s)</th>
                                    <th width="15%">Access Type</th>
                                    <th width="15%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {{ session()->get('success') }}
                                @if(count($clients) > 0)
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>
                                                <?php
                                                if ($client->client) {
                                                    echo $client->client->displayName();
                                                }
                                                ?>

                                            </td>
                                            <td>{{ getAllUserRoles($client->client_id) }}</td>
                                            <td>{{ ($client->permission === 'view')? ucfirst('View') : 'View and Edit' }}</td>
                                            <td>
                                                <a href="{{ url('collaborator/change-permission?action=3&client='.$client->id) }}"
                                                   class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i>
                                                    Remove</a></td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td>{{ auth()->user()->displayName()}}</td>
                                    <td>{{ getUserRole(auth()->user()->id)->display_name }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop


