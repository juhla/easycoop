<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'membership', 'middleware' => ['web', 'auth:web']], function () {
    Route::get('/', function () {
        dd('This is the PriceBundle module index page. Build something great!');
    });

    Route::get('/user-licenses', 'PriceBundleController@index');
    Route::get('/plans', 'PriceBundleController@plans');
    Route::any('/subscribe/{ref_no}', 'PriceBundleController@subscribe');
    Route::get('/maintenance/plans', 'PriceBundleController@membership_plans');
    Route::any('/renewal/{ref_no}', 'PriceBundleController@renewal');

});
