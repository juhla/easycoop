<?php

namespace App\Modules\Reports\Http\Controllers;

use App\Computations;
use App\DashboardCalculations;
use App\Modules\Base\Http\Controllers\Controller;
use App\Modules\Base\Jobs\SendTransactionListEmail;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Reports\Models\AccountGroup;
use App\Modules\Reports\Models\Customer;
use App\Modules\Reports\Models\FiscalYear;
use App\Modules\Reports\Models\Ledger;
use App\Modules\Reports\Models\Transaction;
use App\Modules\Reports\Models\Vendor;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\LedgerAccount;
use App\Modules\Base\Traits\Core\TrialBalance;
use App\Modules\Base\Traits\Export;
use Carbon\Carbon;
use PDF;

class ReportsController extends Controller
{

    use TrialBalance, Balance, LedgerAccount;

    public function __construct(Ledger $ledger, AccountGroup $gl_group, Transaction $transaction, Computations $compute, DashboardCalculations $dashboardCalculation)
    {
        $this->middleware(['tenant', 'fiscal_year']);
        $this->ledger = $ledger;
        $this->compute = $compute;
        $this->gl_group = $gl_group;
        $this->transaction = $transaction;
        $this->dashboardCalculation = $dashboardCalculation;
    }

    public function index()
    {
        $this->checkIsBalanced();
        $this->hasAccess(['view-quickview']);
        $data['title'] = 'Reports';
        return view('reports::index')->with($data);
    }

    protected function parseDate($dt, $from = 'd/m/Y', $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::createFromFormat($from, $dt)->format($format);
        } catch (\Exception $e) {
            return null;
        }
    }

    protected function getPreviousYear($dt, $format = 'Y')
    {
        try {
            return date_format(date_sub(date_create($this->parseDate($dt)), date_interval_create_from_date_string('1 year')), $format);
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * method to display transactions
     * for a general ledger account
     *
     * @param $ledger_id
     * @return mixed
     */
    public function ledgerStatement($ledger_id = 0)
    {

        //dd($this->getLedgerBalance('86','2017-01-01','2017-12-31'));
        //dd($this->getGroupBalance('12', '2017-01-01', '2017-12-31'));
        $previous_year = null;
        $data['end_date'] = '';
        $data['start_date'] = '';
        $data['openingBalance'] = 0;
        $data['profitLossLedger'] = false;
        $data['balanceSheetLedger'] = false;

        if ($ledger_id !== 0) {
            $data['ledger'] = $this->ledger
                ->where('id', $ledger_id)
                ->with(['group.parent'])->first();
            $data['transactions'] = $data['ledger']->getTransactionsForLedger($ledger_id, null, null);
        } elseif ($input = request()->all()) {
            $range = [];
            $ledger_id = $input['ledger_id'];

            try {
                if ($input['from_date'] !== '') {
                    $data['start_date'] = $input['from_date'];
                    $range['start_date'] = $this->parseDate($data['start_date']);
                    $previous_year = $this->getPreviousYear($input['from_date']);
                }
                if ($input['to_date'] !== '') {
                    $data['end_date'] = $input['to_date'];
                    $range['end_date'] = $this->parseDate($data['end_date']);
                }
            } catch (\Exception $exception) {

            }

            $data['ledger'] = $this->ledger->where('id', $ledger_id)
                ->with(['group.parent'])->first();
            $data['transactions'] = $data['ledger']->getTransactionsForLedger($ledger_id, null, $range);
        } else {
            $data['ledger'] = '';
            $data['transactions'] = '';
        }

        if ($data['ledger'] != '') {
            $parent = $data['ledger']->group->parent;
            if (!$parent) {
                $parent = $data['ledger']->group;
            }
            $pid = $parent->parent_id;
            while ($pid != 0) {
                $parent = $parent->parent;
                $pid = $parent->parent_id;
            }
            if (in_array($parent->name, ['Assets', 'Liabilities', 'Equity'])) {
                $data['balanceSheetLedger'] = true;
                $type = 'balanceSheetLedger';
            }
            if (in_array($parent->name, ['Revenue', 'Cost'])) {
                $data['profitLossLedger'] = true;
                $type = 'profitLossLedger';
            }
            $ledger = $data['ledger'];

            $calculateOpeningBalance = false;
            if ($type == 'profitLossLedger') {
                if (!isset($range['start_date'])) {
                    $filterDate = $range['start_date'];
                    $calculateOpeningBalance = true;
                } else {
                    $fiscalYear = new FiscalYear();
                    $isFiscal = $fiscalYear->isFiscalYear($range['start_date'], $range['end_date']);
                    if ($isFiscal) {
                        $calculateOpeningBalance = false;
                    }
                }

            } else {
                $calculateOpeningBalance = true;
                if (isset($range['start_date'])) {
                    $filterDate = $range['start_date'];
                } else {
                    $filterDate = "";
                }
            }

            $data['openingBalance'] = 0;
            $data['openingBalanceType'] = "C";
            if ($calculateOpeningBalance) {
                $op = $ledger->calculateFullOpeningBalance($filterDate);
                $data['openingBalance'] = $op[0];
                $data['openingBalanceType'] = $op[1];
            }

        }

        $data['title'] = 'Ledger Statement';
        $data['ledgers'] = $this->ledger
            ->where('flag', 'Active')
            ->get();
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.ledgerstatement';
        $data['currency'] = getAccountCurrency();
        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('ledger-statement.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::ledger-statement')->with($data);
    }

    /**
     * method to display transactions
     * for a an account group
     *
     * @param $group_id
     * @return mixed
     */
    public function accountStatement($group, $group_id)
    {
        $id = base64_decode($group_id);
        $data['title'] = 'Ledger Statement';
        $data['group'] = AccountGroup::find($id);
        return view('reports::account-statement')->with($data);
    }

    /**
     * method to display trail balance
     *
     * @return Response
     */
    public function trialBalance()
    {
        $data['search'] = true;
        $data['title'] = 'Trial Balance';
        $data['report_date'] = date('dS F, Y');

        $fiscal_year = FiscalYear::getCurrentFiscalYear();
        $input = request()->all();

        if ($input) {
            if ($input['from_date'] !== '') {
                $data['start_date'] = $input['from_date'];
            }
            if ($input['to_date'] !== '') {
                $data['end_date'] = $input['to_date'];
            }
            $data['report_date'] = $this->parseDate($input['to_date'], 'd/m/Y', 'dS F, Y');
        } else {
            try {
                $input = [
                    'from_date' => $this->parseDate($fiscal_year->begins, 'Y-m-d', 'd/m/Y'),
                    'to_date' => $this->parseDate($fiscal_year->ends, 'Y-m-d', 'd/m/Y'),
                ];
            } catch (\Exception $e) {
                $input = ['from_date' => null, 'to_date' => null];
            }
            $data['start_date'] = $input['from_date'];
            $data['end_date'] = $input['to_date'];
            $data['report_date'] = date('dS F, Y');
        }

        if ($input && $input['from_date']) {
            $previousYear = \Carbon\Carbon::createFromFormat('d/m/Y', $input['from_date'])->year - 1;
            $data['profitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($previousYear);
        } else {
            $data['profitBeforeTax'] = ['balance' => 0, 'dc' => 'C'];
        }

        //get assets groups
        $data['asset'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 1)->first();
        //get liability
        $data['liability'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 2)->first();
        //get equity
        $data['equity'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();
        //get income
        $data['revenue'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 4)->first();
        //get cost
        $data['cost'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 5)->first();

        $data['all_group'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();

        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.trialbalance';
        $data['currency'] = getAccountCurrency();

        $this->checkIsBalanced();
        if ($export_type === 'pdf') {
            //dd($data);
            $pdf = PDF::loadView($data['view'], $data);
            return $pdf->download('trial-balance.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::trial-balance')->with($data);
    }

    /**
     * Display a gl account summary
     *
     * @return Response
     */
    public function glAccountSummary()
    {
        $data['title'] = 'GL Account Summary';
        $data['classes'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.gl-account-summary';
        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('gl-account-summary.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::gl-account-summary')->with($data);
    }

    /**
     * method to display balance sheet
     *
     * @return Response
     */
    public function balanceSheet()
    {
        //exit;
        $data['search'] = false;
        $data['title'] = 'Statement of Financial Position';

        $fiscal_year = FiscalYear::getCurrentFiscalYear();

        if ($input = request()->all()) {
            if ($input['from_date'] !== '') {
                $data['start_date'] = $input['from_date'];
            }
            if ($input['to_date'] !== '') {
                $data['end_date'] = $input['to_date'];
            }
            $data['search'] = true;
            $data['curr_year'] = \Carbon\Carbon::now()->year;
            $data['prev_year'] = $data['curr_year'] - 1;
            try {
                $data['curr_year'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['to_date'])->year;
                $data['prev_year'] = $data['curr_year'] - 1;
            } catch (\Exception $exception) {

            }

            //get current year accumulated depreciation
            $data['curr_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(\Carbon\Carbon::createFromFormat('d/m/Y', $input['from_date'])->format('Y-m-d'), \Carbon\Carbon::createFromFormat('d/m/Y', $input['to_date'])->format('Y-m-d'));
            //get previous year accumulated depreciation
            $data['prev_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['prev_year']);
        } else {
            try {
                /*
                 * In sitations when the company is just setup and no info, default to nulls and zeros
                 */
                $data['curr_year'] = date('Y', strtotime($fiscal_year->ends));
                $data['prev_year'] = $data['curr_year'] - 1;
                $data['end_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->ends)->format('d/m/Y');
                $data['start_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->begins)->format('d/m/Y');
                $data['curr_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['curr_year']);
                $data['prev_accum_depreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['prev_year']);
            } catch (\Exception $e) {

                $data['curr_year'] = null;
                $data['prev_year'] = null;
                $data['end_date'] = null;
                $data['start_date'] = null;
                $data['curr_accum_depreciation'] = 0;
                $data['prev_accum_depreciation'] = 0;
            }
        }

        $data['accruals'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 34)->first();

        if ($data['prev_year'] && $data['curr_year']) {
            $data['prevYearEndDate'] = '31/12/' . $data['prev_year'];
            $data['prevYearStartDate'] = '01/01/' . $data['prev_year'];
            $data['prevYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['prev_year'], null, null);
            $data['currYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['curr_year'], null, null);

            $data['accrualsForYears'] = [
                'currentYearAccruals' => $this->compute->getAccruals($data['accruals'], $data['curr_year']),
                'previousYearAccruals' => $this->compute->getAccruals($data['accruals'], $data['prev_year']),
            ];
        } else {
            $data['prevYearEndDate'] = '';
            $data['prevYearStartDate'] = '';
            $data['prevYearProfitBeforeTax'] = 0;
            $data['currYearProfitBeforeTax'] = 0;
            $data['accrualsForYears'] = [
                'currentYearAccruals' => 0,
                'previousYearAccruals' => 0,
            ];
        }

        //get non-current assets groups
        $data['non_current_assets'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 6)->first();
        //get current assets groups
        $data['current_assets'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 9)->first();
        //get non-current liabilities groups
        $data['non_curr_liabilities'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 23)->first();
        //get current liabilities groups
        $data['curr_liabilities'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 27)->first();

        //get equity
        $data['equity'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();

        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.balancesheet';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('statement-of-financial-position.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }

        return view('reports::balance-sheet')->with($data);
    }

    protected function calculateYear($year, $diff)
    {
        try {
            return $year - $diff;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function balanceSheetExtended()
    {
        $data['title'] = '5-year Statment of Financial Position';

        $fiscal_year = FiscalYear::getCurrentFiscalYear();
        //get non-current assets groups
        $data['non_current_assets'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 6)->first();
        //get current assets groups
        $data['current_assets'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 9)->first();

        $data['non_curr_liabilities'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 23)->first();
        //get current liabilities groups
        $data['curr_liabilities'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 27)->first();
        //get equity
        $data['equity'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 3)->first();

        //get income account
        $data['income'] = AccountGroup::where('id', '35')->first();
        //get cost of sales account
        $data['direct_cost'] = AccountGroup::where('id', '37')->first();
        //get other_income account
        $data['other_income'] = AccountGroup::where('id', '36')->first();
        //get expenses accounts
        $data['expenses'] = AccountGroup::with(['groups' => function ($q) {
            $q->where('id', '!=', 37)->orderBy('code', 'asc');
        }])->where('id', '5')->first();

        $accruals = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('id', 34)->first();

        try {
            $oldestFiscalYear = $this->parseDate(FiscalYear::orderBy('ends', 'desc')->select('begins', 'ends')->first()->begins, 'Y-m-d', 'Y');
        } catch (\Exception $e) {
            $oldestFiscalYear = '';
        }

        /*
         * In sitations when the company is just setup and no info, oldestFiscalYear would throw a Trying to get property of non-object error.
         * Rescue from error and default to nulls and zeros
         */
        if ($oldestFiscalYear != '') {
            $secondYear = $this->calculateYear($oldestFiscalYear, 1);
            $thirdYear = $this->calculateYear($oldestFiscalYear, 2);
            $fourthYear = $this->calculateYear($oldestFiscalYear, 3);
            $fifthYear = $this->calculateYear($oldestFiscalYear, 4);
            $data['firstYear'] = ['year' => intval($oldestFiscalYear), 'starts' => $oldestFiscalYear . '-01-01', 'ends' => $oldestFiscalYear . '-12-31'];
            $data['secondYear'] = ['year' => $secondYear, 'starts' => $secondYear . '-01-01', 'ends' => $secondYear . '-12-31'];
            $data['thirdYear'] = ['year' => $thirdYear, 'starts' => $thirdYear . '-01-01', 'ends' => $thirdYear . '-12-31'];
            $data['fourthYear'] = ['year' => $fourthYear, 'starts' => $fourthYear . '-01-01', 'ends' => $fourthYear . '-12-31'];
            $data['fifthYear'] = ['year' => $fifthYear, 'starts' => $fifthYear . '-01-01', 'ends' => $fifthYear . '-12-31'];

            $data['firstYearDepreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['firstYear']['year']);

            $data['secondYearDepreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['secondYear']['year']);

            $data['thirdYearDepreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['thirdYear']['year']);

            $data['fourthYearDepreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['fourthYear']['year']);

            $data['fifthYearDepreciation'] = AccountGroup::calculateAccumulatedDepreciation(null, null, $data['fifthYear']['year']);

            $data['firstYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['firstYear']['year']);

            $data['secondYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['secondYear']['year']);

            $data['thirdYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['thirdYear']['year']);

            $data['fourthYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['fourthYear']['year']);

            $data['fifthYearProfitBeforeTax'] = $this->dashboardCalculation->calculateRetainedEarningsTemporaryFunction($data['fifthYear']['year']);

            $data['firstYearAccruals'] = $this->compute->getAccruals($accruals, $data['firstYear']['year']);

            $data['secondYearAccruals'] = $this->compute->getAccruals($accruals, $data['secondYear']['year']);

            $data['thirdYearAccruals'] = $this->compute->getAccruals($accruals, $data['thirdYear']['year']);

            $data['fourthYearAccruals'] = $this->compute->getAccruals($accruals, $data['fourthYear']['year']);

            $data['fifthYearAccruals'] = $this->compute->getAccruals($accruals, $data['fifthYear']['year']);
            $data['curr_year'] = date('Y', strtotime($fiscal_year->ends));
            $data['prev_year'] = $data['curr_year'] - 1;
        } else {
            $data['curr_year'] = null;
            $data['prev_year'] = null;
            $secondYear = '';
            $thirdYear = '';
            $fourthYear = '';
            $fifthYear = '';
            $data['firstYear'] = ['year' => '', 'starts' => '', 'ends' => ''];
            $data['secondYear'] = ['year' => '', 'starts' => '', 'ends' => ''];
            $data['thirdYear'] = ['year' => '', 'starts' => '', 'ends' => ''];
            $data['fourthYear'] = ['year' => '', 'starts' => '', 'ends' => ''];
            $data['fifthYear'] = ['year' => '', 'starts' => '', 'ends' => ''];

            $data['firstYearDepreciation'] = 0;
            $data['secondYearDepreciation'] = 0;
            $data['thirdYearDepreciation'] = 0;
            $data['fourthYearDepreciation'] = 0;
            $data['fifthYearDepreciation'] = 0;
            $data['firstYearProfitBeforeTax'] = 0;
            $data['secondYearProfitBeforeTax'] = 0;
            $data['thirdYearProfitBeforeTax'] = 0;
            $data['fourthYearProfitBeforeTax'] = 0;
            $data['fifthYearProfitBeforeTax'] = 0;
            $data['firstYearAccruals'] = 0;
            $data['secondYearAccruals'] = 0;
            $data['thirdYearAccruals'] = 0;
            $data['fourthYearAccruals'] = 0;
            $data['fifthYearAccruals'] = 0;
        }

        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.fiveyearbalancesheet';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {

            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('5-year-financial-summary.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        $equity = $data['equity'];
        $group = $equity->groups;

        return view('reports::balance-sheet-extended')->with($data);
    }

    public function incomeStatement()
    {
        $data['search'] = true;
        $data['title'] = 'Income Statement';
        $fiscal_year = FiscalYear::getCurrentFiscalYear();
        try {
            $data['end_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->ends)->format('d/m/Y');
            $data['start_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->begins)->format('d/m/Y');
            $data['curr_year'] = date('Y', strtotime($fiscal_year->ends));
            $data['prev_year'] = $data['curr_year'] - 1;
        } catch (\Exception $e) {
            $data['end_date'] = null;
            $data['start_date'] = null;
            $data['curr_year'] = null;
            $data['prev_year'] = null;
        }

        if ($input = request()->all()) {
            if ($input['from_date'] !== '') {
                $data['start_date'] = $input['from_date'];
            }
            if ($input['to_date'] !== '') {
                $data['end_date'] = $input['to_date'];
            }
        }

        //get income account
        $data['income'] = AccountGroup::where('id', '35')->first();
        //get cost of sales account
        $data['direct_cost'] = AccountGroup::where('id', '37')->first();
        //get other_income account
        $data['other_income'] = AccountGroup::where('id', '36')->first();
        //get expenses accounts
        $data['expenses'] = AccountGroup::with(['groups' => function ($q) {
            $q->where('id', '!=', 37)->orderBy('code', 'asc');
        }])->where('id', '5')->first();

        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.income-statement';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('income-statement.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::income-statement')->with($data);
    }

    /**
     * @return $this
     */
    public function detailedIncomeStatement()
    {
        $data['search'] = true;
        $data['title'] = 'Detailed Income Statement';
        $fiscal_year = FiscalYear::getCurrentFiscalYear();
        try {
            $data['end_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->ends)->format('d/m/Y');
            $data['start_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $fiscal_year->begins)->format('d/m/Y');
            $data['curr_year'] = date('Y', strtotime($fiscal_year->ends));
            $data['prev_year'] = $data['curr_year'] - 1;
        } catch (\Exception $e) {
            $data['end_date'] = null;
            $data['start_date'] = null;
            $data['curr_year'] = null;
            $data['prev_year'] = null;
        }

        if ($input = request()->all()) {
            if ($input['from_date'] !== '') {
                $data['start_date'] = $input['from_date'];
            }
            if ($input['to_date'] !== '') {
                $data['end_date'] = $input['to_date'];
            }
        }

        //get income account
        $data['income'] = AccountGroup::where('id', '35')->first();
        //get cost of sales account
        $data['direct_cost'] = AccountGroup::where('id', '37')->first();
        //get other_income account
        $data['other_income'] = AccountGroup::where('id', '36')->first();

        //get expenses accounts
        $data['expenses'] = AccountGroup::with(['groups' => function ($q) {
            $q->where('id', '!=', 37)->orderBy('code', 'asc');
        }])->where('id', '5')->first();
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.detailed-income-statement';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('detailed-income-statement.pdf');
        } elseif ($export_type === 'xls') {
            $data['view'] = 'reports::exports.detailed-income-statement-xls';
            Export::exportTo('xlsx', $data);
        }
        return view('reports::income-statement-detailed')->with($data);

    }

    /**
     * Function to list all vendors
     * @return $this
     */
    public function vendorListing()
    {
        $data['title'] = 'Vendor Listing';

        if ($input = request()->all()) {
            $data['search'] = $input['search'];
            $data['vendors'] = Vendor::where('name', 'LIKE', '%' . $input['search'] . '%')->get();
        } else {
            $data['search'] = '';
            $data['vendors'] = Vendor::where('flag', 'Active')->orderBy('name', 'asc')->get();
        }
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.vendor-listing';
        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('vendor-listing.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::vendor-listing')->with($data);
    }

    /**
     * Function to list all customers
     * @return $this
     */
    public function customerListing()
    {
        $data['title'] = 'Customer Listing';
        if ($input = request()->all()) {
            $data['search'] = $input['search'];
            $data['customers'] = Customer::where('name', 'LIKE', '%' . $input['search'] . '%')->get();
        } else {
            $data['search'] = '';
            $data['customers'] = Customer::where('flag', 'Active')->orderBy('name', 'asc')->get();
        }
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.customer-listing';
        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('customer-listing.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::customer-listing')->with($data);
    }

    public function payablesAccounts()
    {
        $data['report_date'] = date('dS F, Y');
        $input = request()->all();
        if ($input) {
            if (isset($input['type'])) {
                $inputs = session('payable_input');
                $data = session('payable_data');
                $inputs['type'] = $input['type'];
                $input = $inputs;

            } else {
                if ($input['from_date'] !== '') {
                    $data['start_date'] = $input['from_date'];
                }
                if ($input['to_date'] !== '') {
                    $data['end_date'] = $input['to_date'];
                }
                $data['report_date'] = $this->parseDate($input['to_date'], 'd/m/Y', 'dS F, Y');

                session()->put('payable_data', $data);
                session()->put('payable_input', $input);
            }

        }
        if ($input && $input['to_date'] != '') {
            $end_date = $this->parseDate($data['end_date']);
            $data['ledgers'] = $this->getRecPayBalance(28, $end_date, 'C');
        } else {
            $data['report_date'] = date('dS F, Y');
            $end_date = $this->parseDate($data['report_date'], 'dS F, Y');
            $data['ledgers'] = $this->getRecPayBalance(28, $end_date, 'C');
        }

        $data['title'] = 'Payables Accounts';
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.payables_accounts';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('payables-accounts.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::payables_accounts')->with($data);
    }

    public function payablesAgeAnalysis()
    {

        if (request()->input('from_date')) {
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));

            $data['ledgers'] = $this->ledger->where('group_id', 28)->get();
        } else {
            $data['ledgers'] = $this->ledger->where('group_id', 28)->get();
            $data['start_date'] = '';
            $data['end_date'] = '';
        }
        $data['title'] = 'Payables Age Analysis';
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.payables_age_analysis';
        $data['currency'] = getAccountCurrency();
        foreach ($data['ledgers'] as $key => $eachPayables) {
            $data['rec'][$key] = $this->loopAgedPayablesLedger($eachPayables->id);
            $data['rec'][$key]['supplier'] = $eachPayables->name;
        }


        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('payables-age-analysis.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::payables_age_analysis')->with($data);
    }

    /**
     * method to display receivables
     * by Paul
     * @return $this
     */
    public function receivable()
    {
        $data['report_date'] = date('dS F, Y');
        $input = request()->all();
        if ($input) {

            if (isset($input['type'])) {
                $inputs = session('receivables_input');
                $data = session('receivables_data');
                $inputs['type'] = $input['type'];
                $input = $inputs;

            } else {
                if ($input['from_date'] !== '') {
                    $data['start_date'] = $input['from_date'];
                }
                if ($input['to_date'] !== '') {
                    $data['end_date'] = $input['to_date'];
                }
                $data['report_date'] = $this->parseDate($input['to_date'], 'd/m/Y', 'dS F, Y');

                session()->put('receivables_data', $data);
                session()->put('receivables_input', $input);
            }

        }

        if ($input && $input['to_date'] != '') {
            $end_date = $this->parseDate($data['end_date']);
            $data['ledgers'] = $this->getRecPayBalance(12, $end_date, 'D');
        } else {
            $data['report_date'] = date('dS F, Y');
            $end_date = $this->parseDate($data['report_date'], 'dS F, Y');
            $data['ledgers'] = $this->getRecPayBalance(12, $end_date, 'D');
        }


//        $collection = collect($data['ledgers']);
//
//        $res = $collection->sum('balance');
//
//        dd($res);

        $data['title'] = 'Receivables Accounts';
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.receivables';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('receivables.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::receivables')->with($data);
    }

    /**
     * method to display aged receivables
     * by Paul
     * @return $this
     */
    public function agedReceivables()
    {
        $data['title'] = 'Receivables Age Analysis';
        if (request()->input('from_date')) {
            $input = request()->all();
            $data['start_date'] = date('Y-m-d', strtotime($input['from_date']));
            $data['end_date'] = $input['to_date'] ? date('Y-m-d', strtotime($input['to_date'])) : date('Y-m-d', strtotime(\Carbon\Carbon::now()));

            $data['ledgers'] = $this->ledger->where('group_id', 12)->get();
        } else {
            $data['ledgers'] = $this->ledger->where('group_id', 12)->get();
            $data['start_date'] = '';
            $data['end_date'] = '';
        }
        //initiate export process
        $data['rec'] = [];
        foreach ($data['ledgers'] as $key => $eachReceivables) {
            $data['rec'][$key] = $this->loopAgedReceivablesLedger($eachReceivables->id);
            $data['rec'][$key]['customer'] = $eachReceivables->name;
        }

        //dd($data['rec'] );
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.receivablesage';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {
            $pdf = \PDF::loadView($data['view'], $data);
            return $pdf->download('aged-receivables.pdf');
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }
        return view('reports::receivablesage')->with($data);
    }

    /**
     * Transaction listing
     */
    public function transactionListing()
    {

        $data['title'] = 'Transaction Listing';

        $data['start_date'] = '';
        $data['end_date'] = '';
        if (request()->has('from_date') and request()->has('to_date')) {
            $input = request()->all();
            $data['start_date'] = $input['from_date'];
            $data['end_date'] = $input['to_date'];
        }
        $data['classes'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->where('parent_id', null)->get();
        //initiate export process
        $export_type = request()->get('type');
        $data['view'] = 'reports::exports.transaction_listing';
        $data['currency'] = getAccountCurrency();

        if ($export_type === 'pdf') {

            SendTransactionListEmail::dispatch($data, session()->get('company_db'));

            return redirect()->back();
        } elseif ($export_type === 'xls') {
            Export::exportTo('xlsx', $data);
        }

        return view('reports::transaction_listing')->with($data);
    }

    public function transactionDetails($trans_id)
    {
        $data['title'] = 'Transaction Details';

        $data['trans_id'] = $trans_id;

        $data['transaction'] = Transaction::getTransactionDetails($trans_id);

        return view('reports::transaction_details')->with($data);
    }
}
