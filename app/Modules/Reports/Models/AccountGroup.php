<?php

namespace App\Modules\Reports\Models;

use Auth;
use App\Modules\Base\Events\AccountGroupCreated;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\Tree;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class AccountGroup extends \App\Modules\Base\Models\AccountGroup
{

}
