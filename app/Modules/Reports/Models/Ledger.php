<?php

namespace App\Modules\Reports\Models;

use Auth;
use App\Modules\Base\Models\TransactionItem;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\Tree;
use App\Modules\Base\Traits\NewCreates;
use Carbon;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Traits\MySoftDeletes as SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Ledger extends \App\Modules\Base\Models\Ledger
{

}
