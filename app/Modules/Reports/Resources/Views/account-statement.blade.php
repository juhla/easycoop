@extends('app.layouts.app')

@section('content')
    <link href="{{ asset('app/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li><p>Accounting</p></li>
                        <li><a href="#">Transactions</a></li>
                        <li><a href="#" class="active">Inflow</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                    
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg m-t-20">
            <div class="col-lg-10 bg-white">
                <div class="row">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                            <h3><strong>Account</strong> Statement for {{ $group->name }}</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            @if($ledger !== '')

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th align="left">Date</th>
                                        <th>Ref #</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @if($ledger->transactionItems->count())

                                        <input type="hidden" name="ledger" id="ledger" value="{{ $ledger->id }}">
                                        @if($start_date !== '')
                                            <tr>
                                                <td align="left"><i><strong>Opening Balance</strong></i></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><i><strong>{{ formatNumber($ledger->getOpeningBalance($start_date)) }}</strong></i></td>
                                            </tr>
                                            <?php  $new_balance = $ledger->getOpeningBalance($start_date) ?>
                                            @foreach($ledger->transactionItems()->dateRange($start_date, $end_date)->get() as $tran)
                                                <tr>

                                                    <td align="left">{{ date('m/d/Y', strtotime($tran->transactions->transaction_date)) }}</td>
                                                    <td>{{ $tran->transactions->reference_no }}</td>
                                                    <td>{{ ucfirst($tran->transactions->transaction_type) }}</td>
                                                    <td>{{ $tran->transactions->description }}</td>
                                                    @if($tran->dc === 'D')
                                                        <td> {{ $tran->amount }} </td>
                                                        <td> - </td>
                                                        <td>{{ $new_balance += $tran->amount }}</td>

                                                    @else
                                                        <td> - </td>
                                                        <td> {{ $tran->amount }} </td>
                                                        <td>{{ $new_balance -= $tran->amount }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td align="left"><i><strong>Opening Balance</strong></i></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><i><strong>{{ formatNumber($ledger->getOpeningBalance()) }}</strong></i></td>
                                            </tr>
                                            <?php  $new_balance = $ledger->getOpeningBalance() ?>
                                            @foreach($ledger->transactionItems()->get() as $tran)
                                                <tr>

                                                    <td align="left">{{ date('m/d/Y', strtotime($tran->transactions->transaction_date)) }}</td>
                                                    <td>{{ $tran->transactions->reference_no }}</td>
                                                    <td>{{ ucfirst($tran->transactions->transaction_type) }}</td>
                                                    <td>{{ $tran->transactions->description }}</td>
                                                    @if($tran->dc === 'D')
                                                        <td> {{ $tran->amount }} </td>
                                                        <td> - </td>
                                                        <td>{{ $new_balance += $tran->amount }}</td>

                                                    @else
                                                        <td> - </td>
                                                        <td> {{ $tran->amount }} </td>
                                                        <td>{{ $new_balance -= $tran->amount }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @endif

                                        <tr>
                                            <td align="left"><em><strong>Current Closing Balance:</strong></em></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            @if( $new_balance > 0)
                                                <td>{{ 'Dr ' . $new_balance }}</td>
                                            @else
                                                <td><strong><u>{{ 'Cr ' . abs($new_balance) }}</u></strong></td>
                                            @endif
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="7">
                                                <div class="alert alert-warning">
                                                    No transactions found for the selected account.
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            @else
                                <div class="row">
                                    <div class="alert alert-info">
                                        Please select a GL Account above and submit to view all transaction for the selected account.
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <img src="{{ asset('app/img/advertise-bannar.jpg') }}" alt="Ads">
            </div>
        </div>
    </div>

@stop
@section('scripts')
    <script type="text/javascript">
        $(function(){
            $('#from_date').datepicker();
            $('#to_date').datepicker();
        });
    </script>
@stop