@extends('layouts.main')

@section('content')
    <style>
        tr.clickable-row {
            cursor: pointer;
        }

        tr.view-ledger {
            cursor: pointer;
        }
    </style>
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/statement-of-financial-position/export?type=pdf&from_date='. $start_date.'&to_date='.$end_date) }}"
                                   class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/statement-of-financial-position/export?type=xls&from_date='. $start_date.'&to_date='.$end_date) }}"
                                   class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                        <form id="filter_ledger" action="{{ url('reports/statement-of-financial-position') }}"
                              method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off"
                                           id="from_date"
                                           name="from_date" value="{{ $start_date }}" placeholder="From:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off"
                                           name="to_date"
                                           id="to_date" value="{{ $end_date }}" placeholder="To:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="btn-group col-md-3">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Filter</button>
                                <a href="{{ url('reports/statement-of-financial-position') }}" class="btn btn-default">Clear</a>
                            </div>
                        </form>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>{{getBusinessOwnerAuth()->company_name }} - Statement of Financial
                                    Position</h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="inflowTable">
                                    <thead>
                                    <tr>
                                        <th width="40%">Account Name</th>
                                        <th></th>
                                        <th>{{ $curr_year }}</th>
                                        <th>{{ $prev_year }}</th>
                                    </tr>
                                    </thead>
                                    <?php  $currYrNonCurrAssetsTotal = 0 ?>
                                    <?php  $prevYrNonCurrAssetsTotal = 0 ?>

                                    <?php  $currYrCurrAssetsTotal = 0 ?>
                                    <?php  $prevYrCurrAssetsTotal = 0 ?>

                                    <?php  $currYrNonCurrliabilitiesTotal = 0 ?>
                                    <?php  $prevYrNonCurrliabilitiesTotal = 0 ?>

                                    <?php  $currYrCurrliabilitiesTotal = 0 ?>
                                    <?php  $prevYrCurrliabilitiesTotal = 0 ?>

                                    <?php  $currYrEquityTotal = 0 ?>
                                    <?php  $prevYrEquityTotal = 0 ?>


                                    @if(!empty($end_date)):
                                    <?php  $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d') ?>
                                    @else
                                        <?php  $end_date = "" ?>
                                    @endif


                                    @if(!empty($start_date)):
                                    <?php  $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
                                    @else
                                        <?php  $start_date = "" ?>
                                    @endif


                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $non_current_assets->name }}</td>
                                    </tr>
                                    <?php  $ppe = $non_current_assets->groups()->find(7) ?>

                                    <tr class="clickable-row" data-value="{{ $ppe->id }}">
                                        <td>{{ $ppe->name }}</td>

                                        <td></td>
                                        <td>
                                            <?php  $currYrResult = $ppe->getBalance($start_date, $end_date, null, true) ?>
                                            <?php  $currPpeTotal = bcsub($currYrResult['balance'], $curr_accum_depreciation['ppeTotal'], 2) ?>
                                            {{ $currency."".formatNumber($currPpeTotal) }}
                                            <?php  $currYrNonCurrAssetsTotal = bcadd($currPpeTotal, $currYrNonCurrAssetsTotal, 2) ?>
                                        </td>
                                        <td>
                                            <?php  $prevYrResult = $ppe->getBalance(null, null, $prev_year, true) ?>
                                            <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $prev_accum_depreciation['ppeTotal'], 2) ?>
                                            {{ $currency."".formatNumber($prevPpeTotal) }}
                                            <?php  $prevYrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                                        </td>
                                    </tr>
                                    <?php  $ip = $non_current_assets->groups()->find(8) ?>
                                    <tr class="clickable-row" data-value="{{ $ip->id }}">
                                        <td>{{ $ip->name }}</td>
                                        <td></td>
                                        <td>
                                            <?php  $currYrResult = $ip->getBalance($start_date, $end_date, null, true) ?>
                                            <?php  $currIpTotal = bcsub($currYrResult['balance'], $curr_accum_depreciation['ipTotal'], 2) ?>
                                            {{ $currency."".formatNumber($currIpTotal) }}
                                            <?php  $currYrNonCurrAssetsTotal = bcadd($currIpTotal, $currYrNonCurrAssetsTotal, 2) ?>
                                        </td>
                                        <td>
                                            <?php  $prevYrResult = $ip->getBalance(null, null, $prev_year, true) ?>
                                            <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $prev_accum_depreciation['ipTotal'], 2) ?>
                                            {{ $currency."".formatNumber($prevIpTotal) }}
                                            <?php  $prevYrNonCurrAssetsTotal = bcadd($prevIpTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                                        </td>
                                    </tr>
                                    @if($non_current_assets->groups)
                                        <?php  $groups = $non_current_assets->groups()->where('id', '!=', 7)->where('id', '!=', 8)->get() ?>
                                        @foreach($groups as $group)
                                            <tr class="clickable-row" data-value="{{ $group->id }}">
                                                <td style="font-weight: 800">{{ $group->name }}</td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr class="clickable-row" data-value="{{ $child->id }}">
                                                        <td><span class="account-group-child">{{ $child->name }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                            @if($currYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                            @elseif($currYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                            @if($prevYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                            @elseif($prevYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                                <td>
                                                                    <span class="account-group-child">{{ $subs->name }}</span>
                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                                    @if($currYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                                    @elseif($currYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                                    @if($prevYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                                    @elseif($prevYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Non-Current Assets</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber($currYrNonCurrAssetsTotal) }}</th>
                                        <th>{{ $currency."".formatNumber($prevYrNonCurrAssetsTotal) }}</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $current_assets->name }}
                                        </td>
                                    </tr>
                                    @if($groups = $current_assets->groups)
                                        @foreach($groups as $group)
                                            <tr class="clickable-row" data-value="{{ $group->id }}">
                                                <td style="font-weight: 800">{{ $group->name }}

                                                </td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $group->getBalancePlus($start_date, $end_date, null, true, 'D') ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $group->getBalancePlus(null, null, $prev_year, true, 'D') ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr class="clickable-row" data-value="{{ $child->id }}">
                                                        <td>
                                                            <span class="account-group-child">{{ $child->name }}  </span>

                                                        </td>
                                                        <td></td>
                                                        <td>

                                                            <?php  $currYrResult = $child->getBalancePlus($start_date, $end_date, null, true, 'D') ?>

                                                            @if($currYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                            @elseif($currYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $prevYrResult = $child->getBalancePlus(null, null, $prev_year, true, 'D') ?>
                                                            @if($prevYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                            @elseif($prevYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                                <td>
                                                                    <span class="account-group-child">{{ $subs->name }}</span>

                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                                    @if($currYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                                    @elseif($currYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                                    @if($prevYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                                    @elseif($prevYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($current_assets->ledgers->count())
                                        @foreach($current_assets->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true) ?>


                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrAssetsTotal = bcsub($currYrCurrAssetsTotal, $currYrResult['balance'], 2) ?>
                                                    @else
                                                        -
                                                    @endif

                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $ledger->getBalance(true, null, null, $prev_year, true) ?>

                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrAssetsTotal = bcsub($prevYrCurrAssetsTotal, $prevYrResult['balance'], 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Current Assets</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber($currYrCurrAssetsTotal) }}</th>
                                        <th>{{ $currency."".formatNumber($prevYrCurrAssetsTotal) }}</th>
                                    </tr>
                                    <tr style="background-color: #cccccc">
                                        <th>Total Assets</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber(bcadd($currYrCurrAssetsTotal, $currYrNonCurrAssetsTotal, 2)) }}</th>
                                        <th>{{ $currency."".formatNumber(bcadd($prevYrCurrAssetsTotal, $prevYrNonCurrAssetsTotal, 2)) }}</th>
                                    </tr>
                                    </thead>
                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>

                                    <tbody>
                                    <tr>
                                        <td colspan="4"><h4> EQUITY and LIABILITIES</h4></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $non_curr_liabilities->name }}</td>
                                    </tr>
                                    @if($groups = $non_curr_liabilities->groups)
                                        @foreach($groups as $group)
                                            <tr class="clickable-row" data-value="{{ $group->id }}">
                                                <td style="font-weight: 800">{{ $group->name }}</td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr class="clickable-row" data-value="{{ $child->id }}">
                                                        <td><span class="account-group-child">{{ $child->name }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                            @if($currYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                            @elseif($currYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                            @if($prevYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                            @elseif($prevYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                                <td>
                                                                    <span class="account-group-child">{{ $subs->name }}</span>
                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                                    @if($currYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                                    @elseif($currYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                                    @if($prevYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                                    @elseif($prevYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Non-Current Liabilities</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber($currYrNonCurrliabilitiesTotal) }}</th>
                                        <th>{{ $currency."".formatNumber($prevYrNonCurrliabilitiesTotal) }}</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $curr_liabilities->name }}</td>
                                    </tr>
                                    @if($groups = $curr_liabilities->groups)
                                        @foreach($groups as $group)
                                            <tr class="clickable-row" data-value="{{ $group->id }}">
                                                <td style="font-weight: 800">{{ $group->name }}</td>
                                                <td></td>
                                                <td>

                                                    <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr class="clickable-row" data-value="{{ $child->id }}">
                                                        <td><span class="account-group-child">{{ $child->name }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td>

                                                            <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                            @if($currYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                            @elseif($currYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                            @if($prevYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                            @elseif($prevYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                                <td>
                                                                    <span class="account-group-child">{{ $subs->name }}</span>
                                                                </td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                                    @if($currYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                                    @elseif($currYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif

                                                                </td>
                                                                <td>
                                                                    <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                                    @if($prevYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                                    @elseif($prevYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($curr_liabilities->ledgers->count())
                                        @foreach($curr_liabilities->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true) ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif

                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $ledger->getBalance(true, null, null, $prev_year, true) ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Current Liabilities</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber($currYrCurrliabilitiesTotal) }}</th>
                                        <th>{{ $currency."".formatNumber($prevYrCurrliabilitiesTotal) }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <?php  $currYrCurrliabilitiesTotal = bcadd($currYrCurrliabilitiesTotal, $accrualsForYears['currentYearAccruals'], 2) ?>
                                        <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrCurrliabilitiesTotal, $accrualsForYears['previousYearAccruals'], 2) ?>
                                        <td style="font-weight: 800; font-size: 16px"> {{ $accruals->name }}</td>
                                        <td></td>
                                        <td>  {{ $accrualsForYears['currentYearAccruals'] }} </td>
                                        <td>  {{ $accrualsForYears['previousYearAccruals'] }} </td>
                                    </tr>
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cccccc">
                                        <th>Total Liabilities</th>
                                        <th></th>
                                        <?php  $currTotalLiability = bcadd($currYrCurrliabilitiesTotal, $currYrNonCurrliabilitiesTotal, 2) ?>
                                        <?php  $prevTotalLiability = bcadd($prevYrCurrliabilitiesTotal, $prevYrNonCurrliabilitiesTotal, 2) ?>
                                        <th>{{ $currency."".formatNumber($currTotalLiability) }}</th>
                                        <th>{{ $currency."".formatNumber($prevTotalLiability) }}</th>
                                    </tr>
                                    </thead>

                                    <tr>
                                        <td colspan="4"></td>
                                    </tr>

                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $equity->name }}</td>
                                    </tr>
                                    @if($groups = $equity->groups)
                                        @foreach($groups as $group)
                                            <tr class="clickable-row" data-value="{{ $group->id }}">
                                                <td style="font-weight: 800">{{ $group->name }}</td>
                                                <td></td>
                                                <td>
                                                    <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>

                                            </tr>
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr class="clickable-row" data-value="{{ $child->id }}">
                                                        <td><span class="account-group-child">{{ $child->name }}</span>
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                            @if($currYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                            @elseif($currYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                            @if($prevYrResult['dc'] === 'D')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                            @elseif($prevYrResult['dc'] === 'C')
                                                                {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{  $subs->id }}">
                                                                <td>
                                                                    <span class="account-group-child">{{ $subs->name }}</span>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                                    @if($currYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                                    @elseif($currYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                                        <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                                    @if($prevYrResult['dc'] === 'D')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                                    @elseif($prevYrResult['dc'] === 'C')
                                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                                        <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($equity->ledgers->count())
                                        @foreach($equity->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    @if( $ledger->name == 'Retained Earnings' )
                                                        <?php  $one = $currYearProfitBeforeTax ?>
                                                        <?php  //$two = $ledger->getBalance(true, $start_date, $end_date, null, true) ?>
                                                        <?php  $two = $ledger->getBalance(true, null, null, $curr_year, true) ?>

                                                        <?php  $twos = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                                                        <?php
                                                        $retainedEarning = ($two['dc'] == "D") ? -$two['balance'] : $two['balance'];

                                                        $prevYr = ($one['balance']);
                                                        $currYrResult['balance'] = bcadd($retainedEarning, $prevYr, 2);
                                                        $currYrResult['dc'] = ($currYrResult['balance'] < 0) ? "D" : "C";
                                                        $retainedEarning = $currYrResult;
                                                        ?>

                                                    @else
                                                        <?php  $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true) ?>
                                                    @endif



                                                    <?php //pr($currYrResult); ?>


                                                    @if($currYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                        <?php  $currYrEquityTotalzz = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                    @elseif($currYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                        <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                        <?php  $currYrEquityTotalzz = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                    @else
                                                        -
                                                    @endif


                                                </td>
                                                <td>
                                                    @if( $ledger->name == 'Retained Earnings' )
                                                        <?php
                                                        $theRes = $prevYearProfitBeforeTax;
                                                        $retainedEarnings = $ledger->getBalance(true, null, null, $prev_year, true);

                                                        $balance = bcadd($theRes['balance'], $retainedEarnings['balance'], 2);
                                                        $dc = ($balance < 0) ? "D" : "C";
                                                        $prevYrResult['balance'] = $balance;
                                                        $prevYrResult['dc'] = $dc;
                                                        ?>
                                                    @else
                                                        <?php  $prevYrResult = $ledger->getBalance(true, null, null, $prev_year, true) ?>
                                                    @endif
                                                    <?php

                                                    //pr($prevYrResult);
                                                    ?>


                                                    @if($prevYrResult['dc'] === 'D')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrEquityTotalz = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                        <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>

                                                    @elseif($prevYrResult['dc'] === 'C')
                                                        {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                        <?php  $prevYrEquityTotalz = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                        <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>

                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Equity</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber($currYrEquityTotal) }}</th>
                                        <th>{{ $currency."".formatNumber($prevYrEquityTotal) }}</th>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr style="background-color: #ccc">
                                        <th>Total Equity and Liabilities</th>
                                        <th></th>
                                        <th>{{ $currency."".formatNumber(bcadd($currYrEquityTotal, $currTotalLiability, 2)) }}</th>
                                        <th>{{ $currency."".formatNumber(bcadd($prevYrEquityTotal, $prevTotalLiability, 2)) }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
    <input type="hidden" id="curr_year" value="{{ $curr_year }}"/>
    <input type="hidden" id="prev_year" value="{{ $prev_year }}"/>
    @include('reports::partials._bs-items')
@stop
@section('scripts')
    <script>
        $(document).ready(function ($) {
            var $baseurl = $('#baseurl').val();
            var $curr_year = $('#curr_year').val();
            var $prev_year = $('#prev_year').val();
            //view ledger
            $(".view-ledger").click(function () {
                window.document.location = $(this).data("url");
            });

            //view group items
            $('.clickable-row').click(function () {
                var id = $(this).data('value');
                $.get($baseurl + '/setup/accounts/get-ledger-accounts/' + id + '?curr_year=' + $curr_year + '&prev_year=' + $prev_year, function (res) {
                    $('#list-items').empty();
                    $('#list-items').append(res);
                    $('#bsItemsModal').modal('show');
                });
            });
        });
    </script>
@stop
