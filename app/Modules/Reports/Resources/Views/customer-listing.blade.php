@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-8 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/customer-listing/export?type=pdf&search='.$search) }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/customer-listing/export?type=xls&search='.$search) }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                        <div class="col-sm-4 m-b-xs">
                            <form action="{{ url('reports/customer-listing') }}" >
                                {{ csrf_field() }}
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control" name="search" placeholder="Search by name" value="{{ $search }}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button">Go!</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped m-b-none">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Country</th>
                                        <th>Phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($customers) > 0)
                                        @foreach($customers as $customer)
                                            <tr>
                                                <td>{{ $customer->name }}</td>
                                                <td>{{ ($customer->email) ? $customer->email : 'N/A' }}</td>
                                                <td>{!! ($customer->address) ? $customer->address : 'N/A '!!}</td>
                                                <td>{{ ($customer->country_id) ? getCountry($customer->country_id) : 'N/A' }}</td>
                                                <td>{{ ($customer->phone_no) ? $customer->phone_no : 'N/A' }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="6">You have not added any customer.</td></tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(function(){
            $('#filter_ledger_stmt').validate();

            $(document).on('click', '.pdf', function(evt){
                evt.preventDefault();

                if($('#ledger_no').length){
                    var url = $('#baseurl').val()+'/reports/vendors-ledger/'+$('#ledger_no').val()+'/pdf?from_date='+$('#from_date').val()+'&to_date='+$('#to_date').val();

                    window.location.replace(url);
                }
            });

            $(document).on('click', '.xls', function(evt){
                evt.preventDefault();

                if($('#ledger_no').length){
                    var url = $('#baseurl').val()+'/reports/vendors-ledger/'+$('#ledger_no').val()+'/xls?from_date='+$('#from_date').val()+'&to_date='+$('#to_date').val();

                    window.location.replace(url);
                }
            });
        });
    </script>
@stop
