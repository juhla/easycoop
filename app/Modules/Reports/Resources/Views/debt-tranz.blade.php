@extends('app.layouts.app')

@section('content')
    <link href="{{ asset('app/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen">

    <style>
        th{
            text-align: left;
        }
        .group-form{
            display: none;
        }
        .ledger-form{
            display: none;
        }
    </style>
 <div id="content"> 
    <!-- START JUMBOTRON -->
    
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><p>Accounting</p></li>
                    <li><a href="#">Age Analysis</a></li>
                    </li>
                </ul>
                <!-- END BREADCRUMB -->
            </div>
        </div>
    </div>
        <!-- END JUMBOTRON -->
     <div class="container-fluid container-fixed-lg m-t-20" id="top">
         <div class="col-lg-10 bg-white">
             <div class="row">
               <div class="panel panel-transparent">
                 <div class="panel-heading">
                    <div class="panel-title"><h4>Age Analysis</h4></div>
                    <div class="clearfix"></div>
                 </div>
                 <div class="panel-body">
                     <table class="table table-hover">
                         <thead>
                         <tr>
                             <th>Ref Code</th>
                             <th>Amount</th>
                             <th>Balance Owed</th>
                             <th>Age</th>
                         </tr>
                         </thead>
                         <tbody>
                         @if (count($tranz) > 0)
                         @foreach($tranz as $tranz)
                         <b>Debtor</b> - {{$tranz->name}}
                         <div class="btn-group  pull-right">
                            <a href="{{ URL::previous() }}" class="btn bg-theme-inverse" title=" "><i class="fa fa-backward"></i> Debtors List</a>
                        </div>
                                 <tr>
                             <td ><span class="">Not Decided</span></td>
                             <td><i>{{ $tranz->amount }}</i></td>

                             <td>
                             @if (Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::totalC($tranz->ledger_id)-$tranz->amount < 0)
                             <i>{{ ltrim(number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::totalC($tranz->ledger_id)-$tranz->amount, 2, '.', ','),'-') }}</i>
                             @else 0.00 @endif
                             </td>

                             @if (Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a') < 31)
                             <td style="background:rgba(19, 245, 36, 0.53);"><b>{{Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a')}}&nbsp;Days</b></td>
                             @endif
                             @if (Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a') > 30 & Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a') < 90)
                             <td style="background:rgba(226, 241, 8, 0.53);"><b>{{Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a')}}&nbsp;Days</b></td>
                             @endif
                             @if (Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a') > 90)
                             <td style="background:rgba(241, 120, 108, 0.53);"><b>{{Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::nowx()->diff(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::datex($tranz->created_at), true)->format('%a')}}&nbsp;Days</b></td>
                             @endif
<!--                          <td style="background:#ffcc33;"><a href="#"></a></td>
                             
                             <td style="background:rgba(243, 89, 88, 0.53);"><a href="#"></a></td>
                             
                             <td style="background:rgba(243, 89, 88, 0.53);"><a href="#"></a></td>
-->                      </tr>
                     @endforeach
                         @endif

                         </tbody>
                     </table>
                     <table class="table table-hover">
                         <thead>
                         <tr>
                             <th><i>Total Balance Owed</i></th>
                             <th width="75%"><u>{{ number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::balance($tranz->ledger_id), 2, '.', ',') }}</u>
                             </th>
                             <th></th>
                         </tr>
                         </thead>
                         <tbody>
                     </table>
                 </div>
                </div>
             </div>
         </div>
         <div class="col-lg-2">
             <img src="{{ asset('app/img/advertise-bannar.jpg') }}" alt="Ads">
        </div>
     </div>
 </div>
@stop
@section('scripts')
    
    <script type="text/javascript">
        $(function(){
            $('#from_date').datepicker();
            $('#to_date').datepicker();
        });
    </script>
@stop