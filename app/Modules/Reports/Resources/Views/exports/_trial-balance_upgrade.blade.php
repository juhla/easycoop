<div>
    <table>
        <thead>
        <tr>
            <th>Account</th>
            {{--<th>Debit</th>--}}
            {{--<th>Credit</th>--}}
            {{--<th>Debit</th>--}}
            {{--<th>Credit</th>--}}
            <th>Debit</th>
            <th>Credit</th>
        </tr>
        </thead>
        <tbody>

        <?php  $debitBalance = 0 ?>
        <?php  $creditBalance = 0 ?>

        @if(!empty($end_date))
            <?php  $end_date = \Carbon\Carbon::createFromFormat('dS F, Y', $report_date)->format('Y-m-d') ?>
            <?php  $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
        @else
            <?php  $end_date = ''   ?>
            <?php  $start_date = ''   ?>
        @endif


        @foreach($all_group as $groups)
            <tr>
                <td colspan="3"><h5> {{ $groups->name }}</h5></td>

            </tr>
            {{--For Equity, show ledgers--}}
            @if($groups->id == 3)
                @foreach($groups->equityLedgers() as $equity)
                    <?php  $accountBalance = $equity->getLedgerBalanceByCode($equity->code, $start_date, $end_date) ?>
                    @if($accountBalance['balance'] > 0)
                        <tr>
                            <td><b>{{ $equity->code.' '.$equity->name }}</b>

                            </td>
                            @if($accountBalance['dc'] === 'D')
                                <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                <td> -</td>
                                <?php  $debitBalance = bcadd($accountBalance['balance'], $debitBalance, 2) ?>
                            @elseif($accountBalance['dc'] === 'C')
                                <td> -</td>
                                <td style="font-family: DejaVu Sans;">{{$currency."".formatNumber($accountBalance['balance']) }}</td>

                                <?php  $creditBalance = bcadd($accountBalance['balance'], $creditBalance, 2) ?>
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                    @endif
                @endforeach
            @endif


            @if($groups->hasDescendants())
                @foreach($groups->directChildren() as $group)
                    <?php  $accountBalance = $group->getGroupBalance($group->id, $start_date, $end_date) ?>
                    @if($accountBalance['balance'] > 0)
                        <tr>
                            <td>{{ $group->code .' '.$group->name }}</td>

                            @if(!$group->hasDescendants() && !$group->hasLedgers() )
                                @if($accountBalance['dc'] === 'D')
                                    <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                    <?php  $debitBalance = bcadd($accountBalance['balance'], $debitBalance, 2) ?>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <?php  $creditBalance = bcadd($accountBalance['balance'], $creditBalance, 2) ?>
                                @endif
                            @endif

                        </tr>

                        @foreach($group->allLedgers() as $theLedger)
                            <?php  $accountBalance = $theLedger->getLedgerBalanceByCode($theLedger->code, $start_date, $end_date) ?>
                            @if($accountBalance['balance'] > 0)
                                <tr>
                                    <td><b>{{ $theLedger->code.' '.$theLedger->name }}</b>
                                    </td>
                                    @if($accountBalance['dc'] === 'D')
                                        <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                        <td> -</td>
                                        <?php  $debitBalance = bcadd($accountBalance['balance'], $debitBalance, 2) ?>
                                    @elseif($accountBalance['dc'] === 'C')
                                        <td> -</td>
                                        <td style="font-family: DejaVu Sans;">{{$currency."".formatNumber($accountBalance['balance']) }}</td>
                                        <?php  $creditBalance = bcadd($accountBalance['balance'], $creditBalance, 2) ?>
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                            @endif

                        @endforeach

                        @if($group->hasDescendants())
                            @foreach($group->directChildren() as $child)
                                <?php  $accountBalance = $child->getGroupBalance($child->id, $start_date, $end_date) ?>
                                @if($accountBalance['balance'] > 0)
                                    <tr>
                                        <td>
                                            {{ $child->code.' '.$child->name }}
                                            <?php //pr($child->id); ?>
                                        </td>

                                        @if(!$child->hasDescendants())
                                            @if($accountBalance['dc'] === 'D')
                                                <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                                <td> -</td>
                                                <?php  $debitBalance = bcadd($accountBalance['balance'], $debitBalance, 2) ?>
                                            @elseif($accountBalance['dc'] === 'C')
                                                <td> -</td>
                                                <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                                <?php  $creditBalance = bcadd($accountBalance['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif

                                    </tr>
                                    @if($child->hasDescendants())
                                        @foreach($child->directChildren() as $subs)
                                            <?php  $accountBalance = $subs->getGroupBalance($subs->id, $start_date, $end_date) ?>
                                            @if($accountBalance['balance'] > 0)
                                                <tr>
                                                    <td>
                                                        <b>{{ $subs->code.' '.$subs->name }}</b>
                                                    </td>

                                                    <?php // pr($accountBalance); ?>
                                                    @if($accountBalance['dc'] === 'D')
                                                        <td style="font-family: DejaVu Sans;">{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                                        <td> -</td>
                                                        <?php  $debitBalance = bcadd($accountBalance['balance'], $debitBalance, 2) ?>
                                                    @elseif($accountBalance['dc'] === 'C')
                                                        <td> -</td>
                                                        <td style="font-family: DejaVu Sans;">{{$currency."".formatNumber($accountBalance['balance']) }}</td>
                                                        <?php  $creditBalance = bcadd($accountBalance['balance'], $creditBalance, 2) ?>
                                                    @else
                                                        <td></td>
                                                        <td></td>
                                                    @endif
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach
            @endif
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th><h4>&nbsp;&nbsp;&nbsp;&nbsp;Total</h4></th>
            {{--<th></th>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
            <th style="font-family: DejaVu Sans;"><h4>{{ $currency."".formatNumber($debitBalance) }}</h4></th>
            <th style="font-family: DejaVu Sans;"><h4>{{ $currency."".formatNumber($creditBalance) }}</h4></th>
        </tr>
        </tfoot>
    </table>
</div>