<div class="hbox stretch">
    <div class="scrollable wrapper w-f">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="">
                    <h4>{{getBusinessOwnerAuth()->company_name }} - Statement of Financial Position</h4>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed" style="font-family: DejaVu Sans;">
                        <thead>
                        <tr>
                            <th>Account Name</th>
                            <th></th>
                            <th>{{ $curr_year }}</th>
                            <th>{{ $prev_year }}</th>
                        </tr>
                        </thead>
                        <?php  $currYrNonCurrAssetsTotal = 0 ?>
                        <?php  $prevYrNonCurrAssetsTotal = 0 ?>

                        <?php  $currYrCurrAssetsTotal = 0 ?>
                        <?php  $prevYrCurrAssetsTotal = 0 ?>

                        <?php  $currYrNonCurrliabilitiesTotal = 0 ?>
                        <?php  $prevYrNonCurrliabilitiesTotal = 0 ?>

                        <?php  $currYrCurrliabilitiesTotal = 0 ?>
                        <?php  $prevYrCurrliabilitiesTotal = 0 ?>

                        <?php  $currYrEquityTotal = 0 ?>
                        <?php  $prevYrEquityTotal = 0 ?>


                        @if(!empty($end_date)):
                        <?php  $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d') ?>
                        @else
                            <?php  $end_date = "" ?>
                        @endif


                        @if(!empty($start_date)):
                        <?php  $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
                        @else
                            <?php  $start_date = "" ?>
                        @endif


                        <tbody>
                        <tr>
                            <td colspan="4"
                                style="font-weight: 800; font-size: 16px"> {{ $non_current_assets->name }}</td>
                        </tr>
                        <?php  $ppe = $non_current_assets->groups()->find(7) ?>

                        <tr class="clickable-row" data-value="{{ $ppe->id }}">
                            <td>{{ $ppe->name }}</td>

                            <td></td>
                            <td>
                                <?php  $currYrResult = $ppe->getBalance($start_date, $end_date, null, true) ?>
                                <?php  $currPpeTotal = bcsub($currYrResult['balance'], $curr_accum_depreciation['ppeTotal'], 2) ?>
                                {{ $currency."".formatNumber($currPpeTotal) }}
                                <?php  $currYrNonCurrAssetsTotal = bcadd($currPpeTotal, $currYrNonCurrAssetsTotal, 2) ?>
                            </td>
                            <td>
                                <?php  $prevYrResult = $ppe->getBalance(null, null, $prev_year, true) ?>
                                <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $prev_accum_depreciation['ppeTotal'], 2) ?>
                                {{ $currency."".formatNumber($prevPpeTotal) }}
                                <?php  $prevYrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                            </td>
                        </tr>
                        <?php  $ip = $non_current_assets->groups()->find(8) ?>
                        <tr class="clickable-row" data-value="{{ $ip->id }}">
                            <td>{{ $ip->name }}</td>
                            <td></td>
                            <td>
                                <?php  $currYrResult = $ip->getBalance($start_date, $end_date, null, true) ?>
                                <?php  $currIpTotal = bcsub($currYrResult['balance'], $curr_accum_depreciation['ipTotal'], 2) ?>
                                {{ $currency."".formatNumber($currIpTotal) }}
                                <?php  $currYrNonCurrAssetsTotal = bcadd($currIpTotal, $currYrNonCurrAssetsTotal, 2) ?>
                            </td>
                            <td>
                                <?php  $prevYrResult = $ip->getBalance(null, null, $prev_year, true) ?>
                                <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $prev_accum_depreciation['ipTotal'], 2) ?>
                                {{ $currency."".formatNumber($prevIpTotal) }}
                                <?php  $prevYrNonCurrAssetsTotal = bcadd($prevIpTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                            </td>
                        </tr>
                        @if($non_current_assets->groups)
                            <?php  $groups = $non_current_assets->groups()->where('id', '!=', 7)->where('id', '!=', 8)->get() ?>
                            @foreach($groups as $group)
                                <tr class="clickable-row" data-value="{{ $group->id }}">
                                    <td style="font-weight: 800">{{ $group->name }}</td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @if($nestedGroup = $group->nestedGroups($group->id))
                                    @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                        <tr class="clickable-row" data-value="{{ $child->id }}">
                                            <td><span class="account-group-child">{{ $child->name }}</span>
                                            </td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @if($subChild = $child->nestedGroups($child->id))
                                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                    <td>
                                                        <span class="account-group-child">{{ $subs->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <thead>
                        <tr style="background-color: #cce9ff">
                            <th>Total Non-Current Assets</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber($currYrNonCurrAssetsTotal) }}</th>
                            <th>{{ $currency."".formatNumber($prevYrNonCurrAssetsTotal) }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td colspan="4"
                                style="font-weight: 800; font-size: 16px"> {{ $current_assets->name }}</td>
                        </tr>
                        @if($groups = $current_assets->groups)
                            @foreach($groups as $group)
                                <tr class="clickable-row" data-value="{{ $group->id }}">
                                    <td style="font-weight: 800">{{ $group->name }}  </td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $group->getBalancePlus($start_date, $end_date, null, true, 'D') ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $group->getBalancePlus(null, null, $prev_year, true, 'D') ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @if($nestedGroup = $group->nestedGroups($group->id))
                                    @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                        <tr class="clickable-row" data-value="{{ $child->id }}">
                                            <td>
                                                <span class="account-group-child">{{ $child->name }}  </span>
                                            </td>
                                            <td></td>
                                            <td>

                                                <?php  $currYrResult = $child->getBalancePlus($start_date, $end_date, null, true, 'D') ?>

                                                @if($currYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $child->getBalancePlus(null, null, $prev_year, true, 'D') ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @if($subChild = $child->nestedGroups($child->id))
                                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                    <td>
                                                        <span class="account-group-child">{{ $subs->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <thead>
                        <tr style="background-color: #cce9ff">
                            <th>Total Current Assets</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber($currYrCurrAssetsTotal) }}</th>
                            <th>{{ $currency."".formatNumber($prevYrCurrAssetsTotal) }}</th>
                        </tr>
                        <tr style="background-color: #cccccc">
                            <th>Total Assets</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber(bcadd($currYrCurrAssetsTotal, $currYrNonCurrAssetsTotal, 2)) }}</th>
                            <th>{{ $currency."".formatNumber(bcadd($prevYrCurrAssetsTotal, $prevYrNonCurrAssetsTotal, 2)) }}</th>
                        </tr>
                        </thead>
                        <tr>
                            <td colspan="4"></td>
                        </tr>

                        <tbody>
                        <tr>
                            <td colspan="4"><h4> EQUITY and LIABILITIES</h4></td>
                        </tr>
                        <tr>
                            <td colspan="4"
                                style="font-weight: 800; font-size: 16px"> {{ $non_curr_liabilities->name }}</td>
                        </tr>
                        @if($groups = $non_curr_liabilities->groups)
                            @foreach($groups as $group)
                                <tr class="clickable-row" data-value="{{ $group->id }}">
                                    <td style="font-weight: 800">{{ $group->name }}</td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @if($nestedGroup = $group->nestedGroups($group->id))
                                    @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                        <tr class="clickable-row" data-value="{{ $child->id }}">
                                            <td><span class="account-group-child">{{ $child->name }}</span>
                                            </td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @if($subChild = $child->nestedGroups($child->id))
                                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                    <td>
                                                        <span class="account-group-child">{{ $subs->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrNonCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrNonCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                        <thead>
                        <tr style="background-color: #cce9ff">
                            <th>Total Non-Current Liabilities</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber($currYrNonCurrliabilitiesTotal) }}</th>
                            <th>{{ $currency."".formatNumber($prevYrNonCurrliabilitiesTotal) }}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td colspan="4"
                                style="font-weight: 800; font-size: 16px"> {{ $curr_liabilities->name }}</td>
                        </tr>
                        @if($groups = $curr_liabilities->groups)
                            @foreach($groups as $group)
                                <tr class="clickable-row" data-value="{{ $group->id }}">
                                    <td style="font-weight: 800">{{ $group->name }}</td>
                                    <td></td>
                                    <td>

                                        <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @if($nestedGroup = $group->nestedGroups($group->id))
                                    @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                        <tr class="clickable-row" data-value="{{ $child->id }}">
                                            <td><span class="account-group-child">{{ $child->name }}</span>
                                            </td>
                                            <td></td>
                                            <td>

                                                <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @if($subChild = $child->nestedGroups($child->id))
                                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                    <td>
                                                        <span class="account-group-child">{{ $subs->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif

                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($curr_liabilities->ledgers->count())
                            @foreach($curr_liabilities->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                <tr class="view-ledger"
                                    data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                    <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true) ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrliabilitiesTotal = bcsub($currYrCurrliabilitiesTotal, $currYrResult['balance'], 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif

                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ledger->getBalance(true, null, null, $prev_year, true) ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrCurrliabilitiesTotal, $prevYrResult['balance'], 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <thead>
                        <tr style="background-color: #cce9ff">
                            <th>Total Current Liabilities</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber($currYrCurrliabilitiesTotal) }}</th>
                            <th>{{ $currency."".formatNumber($prevYrCurrliabilitiesTotal) }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrCurrliabilitiesTotal, $accrualsForYears['currentYearAccruals'], 2) ?>
                            <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrCurrliabilitiesTotal, $accrualsForYears['previousYearAccruals'], 2) ?>
                            <td style="font-weight: 800; font-size: 16px"> {{ $accruals->name }}</td>
                            <td></td>
                            <td>  {{ $accrualsForYears['currentYearAccruals'] }} </td>
                            <td>  {{ $accrualsForYears['previousYearAccruals'] }} </td>
                        </tr>
                        </tbody>
                        <thead>
                        <tr style="background-color: #cccccc">
                            <th>Total Liabilities</th>
                            <th></th>
                            <?php  $currTotalLiability = bcadd($currYrCurrliabilitiesTotal, $currYrNonCurrliabilitiesTotal, 2) ?>
                            <?php  $prevTotalLiability = bcadd($prevYrCurrliabilitiesTotal, $prevYrNonCurrliabilitiesTotal, 2) ?>
                            <th>{{ $currency."".formatNumber($currTotalLiability) }}</th>
                            <th>{{ $currency."".formatNumber($prevTotalLiability) }}</th>
                        </tr>
                        </thead>

                        <tr>
                            <td colspan="4"></td>
                        </tr>

                        <tbody>
                        <tr>
                            <td colspan="4"
                                style="font-weight: 800; font-size: 16px"> {{ $equity->name }}</td>
                        </tr>
                        @if($groups = $equity->groups)
                            @foreach($groups as $group)
                                <tr class="clickable-row" data-value="{{ $group->id }}">
                                    <td style="font-weight: 800">{{ $group->name }}</td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $group->getBalance($start_date, $end_date, null, true) ?>
                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $group->getBalance(null, null, $prev_year, true) ?>
                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>

                                </tr>
                                @if($nestedGroup = $group->nestedGroups($group->id))
                                    @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                        <tr class="clickable-row" data-value="{{ $child->id }}">
                                            <td><span class="account-group-child">{{ $child->name }}</span>
                                            </td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $child->getBalance($start_date, $end_date, null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $child->getBalance(null, null, $prev_year, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                        @if($subChild = $child->nestedGroups($child->id))
                                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                <tr class="clickable-row" data-value="{{  $subs->id }}">
                                                    <td>
                                                        <span class="account-group-child">{{ $subs->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $subs->getBalance($start_date, $end_date, null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrEquityTotal = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $subs->getBalance(null, null, $prev_year, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($equity->ledgers->count())
                            @foreach($equity->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                <tr class="view-ledger"
                                    data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                    <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                    <td></td>
                                    <td>
                                        @if( $ledger->name == 'Retained Earnings' )
                                            <?php  $one = $currYearProfitBeforeTax ?>
                                            <?php  //$two = $ledger->getBalance(true, $start_date, $end_date, null, true) ?>
                                            <?php  $two = $ledger->getBalance(true, null, null, $curr_year, true) ?>
                                            <?php
                                            $retainedEarning = ($two['dc'] == "D") ? -$two['balance'] : $two['balance'];
                                            $prevYr = ($one['balance']);
                                            $currYrResult['balance'] = bcadd($retainedEarning, $prevYr, 2);
                                            $currYrResult['dc'] = ($currYrResult['balance'] < 0) ? "D" : "C";
                                            $retainedEarning =   $currYrResult;
                                            ?>





                                        @else
                                            <?php  $currYrResult = $ledger->getBalance(true, null, null, $curr_year, true) ?>
                                        @endif



                                        <?php //pr($currYrResult); ?>
                                        <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>


                                        @if($currYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrEquityTotalzz = bcsub($currYrEquityTotal, $currYrResult['balance'], 2) ?>
                                        @elseif($currYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($currYrResult['balance']) }}
                                            <?php  $currYrEquityTotalzz = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                        @else
                                            -
                                        @endif


                                    </td>
                                    <td>
                                        @if( $ledger->name == 'Retained Earnings' )
                                            <?php
                                            $theRes = $prevYearProfitBeforeTax;
                                            $retainedEarnings = $ledger->getBalance(true, null, null, $prev_year, true);
                                            $balance = bcadd($theRes['balance'], $retainedEarnings['balance'], 2);
                                            $dc = ($balance < 0) ? "D" : "C";
                                            $prevYrResult['balance'] = $balance;
                                            $prevYrResult['dc'] = $dc;
                                            ?>
                                        @else
                                            <?php  $prevYrResult = $ledger->getBalance(true, null, null, $prev_year, true) ?>
                                        @endif
                                        <?php

                                        //pr($prevYrResult);
                                        ?>

                                        @if($prevYrResult['dc'] === 'D')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrEquityTotal = bcsub($prevYrEquityTotal, $prevYrResult['balance'], 2) ?>
                                        @elseif($prevYrResult['dc'] === 'C')
                                            {{ $currency."".formatNumber($prevYrResult['balance']) }}
                                            <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <thead>
                        <tr style="background-color: #cce9ff">
                            <th>Total Equity</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber($currYrEquityTotal) }}</th>
                            <th>{{ $currency."".formatNumber($prevYrEquityTotal) }}</th>
                        </tr>
                        </thead>
                        <thead>
                        <tr style="background-color: #ccc">
                            <th>Total Equity and Liabilities</th>
                            <th></th>
                            <th>{{ $currency."".formatNumber(bcadd($currYrEquityTotal, $currTotalLiability, 2)) }}</th>
                            <th>{{ $currency."".formatNumber(bcadd($prevYrEquityTotal, $prevTotalLiability, 2)) }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>