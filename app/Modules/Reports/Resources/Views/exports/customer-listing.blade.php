<div class="scrollable wrapper w-f">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>List of Customers</h3>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="font-family: DejaVu Sans;">
            <div class="table-responsive">
                <table class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>Phone</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($customers) > 0)
                        @foreach($customers as $customer)
                            <tr>
                                <td>{{ $customer->name }}</td>
                                <td>{{ ($customer->email) ? $customer->email : 'N/A' }}</td>
                                <td>{!! ($customer->address) ? $customer->address : 'N/A '!!}</td>
                                <td>{{ ($customer->country_id) ? getCountry($customer->country_id) : 'N/A' }}</td>
                                <td>{{ ($customer->phone_no) ? $customer->phone_no : 'N/A' }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">You have not added any vendor.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>