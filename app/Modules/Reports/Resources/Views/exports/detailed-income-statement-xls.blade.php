<div>
    <div>
        <div>
            <div>
                <div>
                    <div>
                        <div>
                            <h4>
                                {{getBusinessOwnerAuth()->company_name }} - Detailed Income Statement
                            </h4>
                        </div>
                        <div></div>
                    </div>
                    <div>
                        <div style="font-family: DejaVu Sans;">
                            <table>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Current Month <br>{{$currency}}</th>
                                    <th>Year to Date <br>{{$currency}}</th>
                                </tr>
                                </thead>
                                <?php  $currMonthIncome = 0 ?>
                                <?php  $incomeYearToDate = 0 ?>
                                <?php  $currMonthDirectCost = 0 ?>
                                <?php  $yearToDateDirectCost = 0 ?>
                                <?php  $currMonthOIncome = 0 ?>
                                <?php  $oIncomeYearToDate = 0 ?>
                                <?php  $currMonthExp = 0 ?>
                                <?php  $yearToDateExp = 0 ?>


                                @if($search)

                                    <?php

                                    try {
                                        $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d');
                                        $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d');
                                        $month = new Carbon\Carbon('first day of this month');
                                        $current_month = $month->format('Y-m-d');
                                    } catch (Exception $ex) {
                                        $end_date = $start_date = $current_month = "";
                                    }
                                    ?>
                                @else
                                    <?php  $month = new Carbon\Carbon('first day of this month') ?>
                                    <?php  $year = new Carbon\Carbon('first day of January ' . date('Y')) ?>
                                    <?php  $current_month = $month->format('Y-m-d') ?>
                                    <?php  $start_date = $year->format('Y-m-d') ?>
                                    <?php  $end_date = date('Y-m-d') ?>
                                @endif

                                <tbody>
                                <tr>
                                    <td> {{ $income->name }}</td>
                                </tr>
                                @if($income->ledgers->count())
                                    @foreach($income->ledgers()->orderBy('code', 'asc')->where('flag', 'Active')->get() as $ledger)
                                        <tr>
                                            <td><span>{{ $ledger->name }}</span></td>
                                            <td></td>
                                            <td>
                                                <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                @if($result['dc'] === 'D')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthIncome = bcsub($currMonthIncome, $result['balance'], 2) ?>
                                                @elseif($result['dc'] === 'C')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthIncome = bcadd($result['balance'], $currMonthIncome, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                @if($yeartodate['dc'] === 'D')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $incomeYearToDate = bcsub($incomeYearToDate, $yeartodate['balance'], 2) ?>
                                                @elseif($yeartodate['dc'] === 'C')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $incomeYearToDate = bcadd($yeartodate['balance'], $incomeYearToDate, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Total Income</th>
                                    <th></th>
                                    <th>{{ formatNumber($currMonthIncome) }}</th>
                                    <th>{{ formatNumber($incomeYearToDate) }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> {{ $direct_cost->name }}</td>
                                </tr>
                                @if($direct_cost->ledgers->count())
                                    @foreach($direct_cost->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                        <tr>
                                            <td><span>{{ $ledger->name }}</span></td>
                                            <td></td>
                                            <td>
                                                <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                @if($result['dc'] === 'D')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthDirectCost = bcadd($currMonthDirectCost, $result['balance'], 2) ?>
                                                @elseif($result['dc'] === 'C')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthDirectCost = bcsub($currMonthDirectCost, $result['balance'], 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                @if($yeartodate['dc'] === 'D')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $yearToDateDirectCost = bcadd($yearToDateDirectCost, $yeartodate['balance'], 2) ?>
                                                @elseif($yeartodate['dc'] === 'C')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $yearToDateDirectCost = bcsub($yearToDateDirectCost, $yeartodate['balance'], 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Total Direct Cost</th>
                                    <th></th>
                                    <th>{{ formatNumber($currMonthDirectCost) }}</th>
                                    <th>{{ formatNumber($yearToDateDirectCost) }}</th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th>Gross Profit</th>
                                    <th></th>
                                    <?php  $grossProfitMonth = bcsub($currMonthIncome, $currMonthDirectCost, 2)?>
                                    <th>{{ formatNumber($grossProfitMonth) }}</th>
                                    <?php  $grossProfitYTD = bcsub($incomeYearToDate, $yearToDateDirectCost, 2)?>
                                    <th>{{ formatNumber($grossProfitYTD) }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> {{ $other_income->name }}</td>
                                </tr>
                                @if($other_income->ledgers->count())
                                    @foreach($other_income->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                        <tr>
                                            <td><span>{{ $ledger->name }}</span></td>
                                            <td></td>
                                            <td>
                                                <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                @if($result['dc'] === 'D')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthOIncome = bcsub($currMonthOIncome, $result['balance'], 2) ?>
                                                @elseif($result['dc'] === 'C')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthOIncome = bcadd($result['balance'], $currMonthOIncome, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                @if($yeartodate['dc'] === 'D')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $oIncomeYearToDate = bcsub($oIncomeYearToDate, $yeartodate['balance'], 2) ?>
                                                @elseif($yeartodate['dc'] === 'C')
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                    <?php  $oIncomeYearToDate = bcadd($yeartodate['balance'], $oIncomeYearToDate, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Other Income Total</th>
                                    <th></th>
                                    <th>{{ formatNumber($currMonthOIncome) }}</th>
                                    <th>{{ formatNumber($oIncomeYearToDate) }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td> {{ $expenses->name }}</td>
                                </tr>
                                @if($groups = $expenses->groups)
                                    @foreach($groups as $group)
                                        <tr>
                                            <td>{{ $group->name }}</td>
                                        </tr>
                                        @if($group->ledgers->count())
                                            @foreach($group->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                <tr>
                                                    <td><span>{{ $ledger->name }}</span>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                        @if($result['dc'] === 'D')
                                                            {{ formatNumber($result['balance']) }}
                                                            <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                        @elseif($result['dc'] === 'C')
                                                            {{ formatNumber($result['balance']) }}
                                                            <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                        @if($yeartodate['dc'] === 'D')
                                                            <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                            {{ formatNumber($yeartodate['balance']) }}
                                                        @elseif($yeartodate['dc'] === 'C')
                                                            <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                            {{ formatNumber($yeartodate['balance']) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr>
                                                    <td><span>{{ $child->name }}
                                                            dd</span></td>
                                                </tr>
                                                @if($child->ledgers->count())
                                                    @foreach($child->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                        <tr>
                                                            <td>
                                                                <span>{{ $ledger->name }}</span>
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                                @if($result['dc'] === 'D')
                                                                    {{ formatNumber($result['balance']) }}
                                                                    <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                                @elseif($result['dc'] === 'C')
                                                                    {{ formatNumber($result['balance']) }}
                                                                    <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                                @if($yeartodate['dc'] === 'D')
                                                                    <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                    {{ formatNumber($yeartodate['balance']) }}
                                                                @elseif($yeartodate['dc'] === 'C')
                                                                    <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                    {{ formatNumber($yeartodate['balance']) }}
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr>
                                                            <td><span>{{ $subs->name }}</span>
                                                            </td>
                                                        </tr>
                                                        @if($subs->ledgers->count())
                                                            @foreach($subs->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                                <tr>
                                                                    <td>
                                                                        <span>{{ $ledger->name }} </span>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>
                                                                        <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                                        @if($result['dc'] === 'D')
                                                                            {{ formatNumber($result['balance']) }}
                                                                            <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                                        @elseif($result['dc'] === 'C')
                                                                            {{ formatNumber($result['balance']) }}
                                                                            <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                                        @if($yeartodate['dc'] === 'D')
                                                                            <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                            {{ formatNumber($yeartodate['balance']) }}
                                                                        @elseif($yeartodate['dc'] === 'C')
                                                                            <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                            {{ formatNumber($yeartodate['balance']) }}
                                                                        @else
                                                                            -
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                @if($expenses->ledgers->count())
                                    @foreach($expenses->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                        <tr>
                                            <td><span>{{ $ledger->name }}</span></td>
                                            <td></td>
                                            <td>
                                                <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                @if($result['dc'] === 'D')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                @elseif($result['dc'] === 'C')
                                                    {{ formatNumber($result['balance']) }}
                                                    <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                @if($yeartodate['dc'] === 'D')
                                                    <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                @elseif($yeartodate['dc'] === 'C')
                                                    <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                    {{ formatNumber($yeartodate['balance']) }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr>
                                    <th>Total Expenses</th>
                                    <th></th>
                                    <th>{{ formatNumber($currMonthExp) }}</th>
                                    <th>{{ formatNumber($yearToDateExp) }}</th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th>Net Profit</th>
                                    <th></th>
                                    <th>
                                        <?php  $currMonthPBT = bcadd($grossProfitMonth, $currMonthOIncome, 2) ?>
                                        <?php  $currMonthPBT = bcsub($currMonthPBT, $currMonthExp, 2)?>
                                        {{ formatNumber($currMonthPBT) }}
                                    </th>
                                    <th>
                                        <?php  $yearToDatePBT = bcadd($grossProfitYTD, $oIncomeYearToDate, 2) ?>
                                        <?php  $yearToDatePBT = bcsub($yearToDatePBT, $yearToDateExp, 2) ?>
                                        {{ formatNumber($yearToDatePBT) }}
                                    </th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
