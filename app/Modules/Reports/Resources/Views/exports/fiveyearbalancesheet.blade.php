<div class="hbox stretch">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>{{getBusinessOwnerAuth()->company_name }} - 5 year Financial Summary</h4></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive" style="font-family: DejaVu Sans;">
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Account Name</th>
                                    <th></th>
                                    <th>{{ $firstYear [ 'year' ] }}<br><span style="font-family: DejaVu Sans;">{{$currency}}</span></th>
                                    <th>{{ $secondYear [ 'year' ] }}<br><span style="font-family: DejaVu Sans;">{{$currency}}</span></th>
                                    <th>{{ $thirdYear[ 'year' ] }}<br><span style="font-family: DejaVu Sans;">{{$currency}}</span></th>
                                    <th>{{ $fourthYear[ 'year' ] }}<br><span style="font-family: DejaVu Sans;">{{$currency}}</span></th>
                                    <th>{{ $fifthYear[ 'year' ] }}<br><span style="font-family: DejaVu Sans;">{{$currency}}</span></th>
                                </tr>
                                </thead>
                                <?php  $currYrNonCurrAssetsTotal = 0 ?>
                                <?php  $prevYrNonCurrAssetsTotal = 0 ?>
                                <?php  $prev2YrNonCurrAssetsTotal = 0 ?>
                                <?php  $prev3YrNonCurrAssetsTotal = 0 ?>
                                <?php  $prev4YrNonCurrAssetsTotal = 0 ?>

                                <?php  $currYrCurrAssetsTotal = 0 ?>
                                <?php  $prevYrCurrAssetsTotal = 0 ?>
                                <?php  $prev2YrCurrAssetsTotal = 0 ?>
                                <?php  $prev3YrCurrAssetsTotal = 0 ?>
                                <?php  $prev4YrCurrAssetsTotal = 0 ?>

                                <?php  $currYrNonCurrliabilitiesTotal = 0 ?>
                                <?php  $prevYrNonCurrliabilitiesTotal = 0 ?>
                                <?php  $prev2YrNonCurrliabilitiesTotal = 0 ?>
                                <?php  $prev3YrNonCurrliabilitiesTotal = 0 ?>
                                <?php  $prev4YrNonCurrliabilitiesTotal = 0 ?>

                                <?php  $currYrCurrliabilitiesTotal = 0 ?>
                                <?php  $prevYrCurrliabilitiesTotal = 0 ?>
                                <?php  $prev2YrCurrliabilitiesTotal = 0 ?>
                                <?php  $prev3YrCurrliabilitiesTotal = 0 ?>
                                <?php  $prev4YrCurrliabilitiesTotal = 0 ?>

                                <?php  $currYrEquityTotal = 0 ?>
                                <?php  $prevYrEquityTotal = 0 ?>
                                <?php  $prev2YrEquityTotal = 0 ?>
                                <?php  $prev3YrEquityTotal = 0 ?>
                                <?php  $prev4YrEquityTotal = 0 ?>

                                <tbody>
                                <tr>
                                    <td colspan="8" style="font-weight: 800; font-size: 16px"> {{ $non_current_assets->name }}</td>
                                </tr>
                                <?php  $ppe = $non_current_assets->groups()->find(7) ?>

                                <tr class="clickable-row" data-value="{{ $ppe->id }}">
                                    <td>{{ $ppe->name }}</td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $ppe->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                        <?php  $currPpeTotal = bcsub($currYrResult['balance'], $firstYearDepreciation['ppeTotal'], 2) ?>
                                        {{ formatNumber($currPpeTotal) }}
                                        <?php  $currYrNonCurrAssetsTotal = bcadd($currPpeTotal, $currYrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ppe->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                        <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $secondYearDepreciation['ppeTotal'], 2) ?>
                                        {{ formatNumber($prevPpeTotal) }}
                                        <?php  $prevYrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ppe->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                        <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $thirdYearDepreciation['ppeTotal'], 2) ?>
                                        {{ formatNumber($prevPpeTotal) }}
                                        <?php  $prev2YrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prev2YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ppe->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                        <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $fourthYearDepreciation['ppeTotal'], 2) ?>
                                        {{ formatNumber($prevPpeTotal) }}
                                        <?php  $prev3YrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prev3YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ppe->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                        <?php  $prevPpeTotal = bcsub($prevYrResult['balance'], $fifthYearDepreciation['ppeTotal'], 2) ?>
                                        {{ formatNumber($prevPpeTotal) }}
                                        <?php  $prev4YrNonCurrAssetsTotal = bcadd($prevPpeTotal, $prev4YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                </tr>
                                <?php  $ip = $non_current_assets->groups()->find(8) ?>
                                <tr class="clickable-row" data-value="{{ $ip->id }}">
                                    <td>{{ $ip->name }}</td>
                                    <td></td>
                                    <td>
                                        <?php  $currYrResult = $ip->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                        <?php  $currIpTotal = bcsub($currYrResult['balance'], $firstYearDepreciation['ipTotal'], 2) ?>
                                        {{ formatNumber($currIpTotal) }}
                                        <?php  $currYrNonCurrAssetsTotal = bcadd($currIpTotal, $currYrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ip->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                        <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $secondYearDepreciation['ipTotal'], 2) ?>
                                        {{ formatNumber($prevIpTotal) }}
                                        <?php  $prevYrNonCurrAssetsTotal = bcadd($prevIpTotal, $prevYrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ip->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                        <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $thirdYearDepreciation['ipTotal'], 2) ?>
                                        {{ formatNumber($prevIpTotal) }}
                                        <?php  $prev2YrNonCurrAssetsTotal = bcadd($prevIpTotal, $prev2YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ip->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                        <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $fourthYearDepreciation['ipTotal'], 2) ?>
                                        {{ formatNumber($prevIpTotal) }}
                                        <?php  $prev3YrNonCurrAssetsTotal = bcadd($prevIpTotal, $prev3YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                    <td>
                                        <?php  $prevYrResult = $ip->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                        <?php  $prevIpTotal = bcsub($prevYrResult['balance'], $fifthYearDepreciation['ipTotal'], 2) ?>
                                        {{ formatNumber($prevIpTotal) }}
                                        <?php  $prev4YrNonCurrAssetsTotal = bcadd($prevIpTotal, $prev4YrNonCurrAssetsTotal, 2) ?>
                                    </td>
                                </tr>
                                @if($non_current_assets->groups)
                                    <?php  $groups = $non_current_assets->groups()->where('id', '!=', 7)->where('id', '!=', 8)->get() ?>
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 800">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $group->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr class="clickable-row" data-value="{{ $child->id }}">
                                                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $child->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $currYrResult = $subs->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                                @if($currYrResult['dc'] === 'D')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrNonCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                                @elseif($currYrResult['dc'] === 'C')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrNonCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrNonCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrNonCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrNonCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrNonCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr style="background-color: #cce9ff">
                                    <th>Total Non-Current Assets</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber($currYrNonCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prevYrNonCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2YrNonCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev3YrNonCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4YrNonCurrAssetsTotal) }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td colspan="8" style="font-weight: 800; font-size: 16px"> {{ $current_assets->name }}</td>
                                </tr>
                                @if($groups = $current_assets->groups)
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 800">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $group->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr class="clickable-row" data-value="{{ $child->id }}">
                                                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $child->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $currYrResult = $subs->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                                @if($currYrResult['dc'] === 'D')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrCurrAssetsTotal = bcadd($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                                @elseif($currYrResult['dc'] === 'C')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrCurrAssetsTotal = bcsub($currYrResult['balance'], $currYrCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prevYrCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev2YrCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev3YrCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrCurrAssetsTotal = bcadd($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrCurrAssetsTotal = bcsub($prevYrResult['balance'], $prev4YrCurrAssetsTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr style="background-color: #cce9ff">
                                    <th>Total Current Assets</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber($currYrCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prevYrCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2YrCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev3YrCurrAssetsTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4YrCurrAssetsTotal) }}</th>
                                </tr>
                                <tr style="background-color: #cccccc">
                                    <th>Total Assets</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber(bcadd($currYrCurrAssetsTotal, $currYrNonCurrAssetsTotal, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prevYrCurrAssetsTotal, $prevYrNonCurrAssetsTotal, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev2YrCurrAssetsTotal, $prev2YrNonCurrAssetsTotal, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev3YrCurrAssetsTotal, $prev3YrNonCurrAssetsTotal, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev4YrCurrAssetsTotal, $prev4YrNonCurrAssetsTotal, 2)) }}</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>

                                <tbody>
                                <tr>
                                    <td colspan="8"><h4> EQUITY and LIABILITIES</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="8" style="font-weight: 800; font-size: 16px"> {{ $non_curr_liabilities->name }}</td>
                                </tr>
                                @if($groups = $non_curr_liabilities->groups)
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 800">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $group->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr class="clickable-row" data-value="{{ $child->id }}">
                                                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $child->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $currYrResult = $subs->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                                @if($currYrResult['dc'] === 'D')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrNonCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                                @elseif($currYrResult['dc'] === 'C')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrNonCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrNonCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrNonCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrNonCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrNonCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr style="background-color: #cce9ff">
                                    <th>Total Non-Current Liabilities</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber($currYrNonCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prevYrNonCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2YrNonCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev3YrNonCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4YrNonCurrliabilitiesTotal) }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td colspan="8" style="font-weight: 800; font-size: 16px"> {{ $curr_liabilities->name }}</td>
                                </tr>
                                @if($groups = $curr_liabilities->groups)
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 800">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $group->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr class="clickable-row" data-value="{{ $child->id }}">
                                                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $child->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $currYrResult = $subs->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                                @if($currYrResult['dc'] === 'D')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrCurrliabilitiesTotal = bcsub($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                                @elseif($currYrResult['dc'] === 'C')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrCurrliabilitiesTotal = bcadd($currYrResult['balance'], $currYrCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prevYrCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev2YrCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev3YrCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrCurrliabilitiesTotal = bcsub($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrCurrliabilitiesTotal = bcadd($prevYrResult['balance'], $prev4YrCurrliabilitiesTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr style="background-color: #cce9ff">
                                    <th>Total Current Liabilities</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber($currYrCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prevYrCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2YrCurrliabilitiesTotal) }}</th>
                                    <th>{{  $currency."".formatNumber($prev3YrCurrliabilitiesTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4YrCurrliabilitiesTotal) }}</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <?php  $currYrCurrliabilitiesTotal = bcadd($currYrCurrliabilitiesTotal, $firstYearAccruals, 2) ?>
                                    <?php  $prevYrCurrliabilitiesTotal = bcadd($prevYrCurrliabilitiesTotal, $secondYearAccruals, 2) ?>
                                    <?php  $prev2YrCurrliabilitiesTotal = bcadd($prev2YrCurrliabilitiesTotal, $thirdYearAccruals, 2) ?>
                                    <?php  $prev3YrCurrliabilitiesTotal = bcadd($prev3YrCurrliabilitiesTotal, $fourthYearAccruals, 2) ?>
                                    <?php  $prev4YrCurrliabilitiesTotal = bcadd($prev4YrCurrliabilitiesTotal, $fifthYearAccruals, 2) ?>

                                    <td style="font-weight: 800; font-size: 16px"> Accruals</td>
                                    <td></td>
                                    <td>  {{ formatNumber( $firstYearAccruals ) }} </td>
                                    <td>  {{ formatNumber( $secondYearAccruals ) }} </td>
                                    <td>  {{ formatNumber( $thirdYearAccruals ) }} </td>
                                    <td>  {{ formatNumber( $fourthYearAccruals ) }} </td>
                                    <td>  {{ formatNumber( $fifthYearAccruals ) }} </td>
                                </tr>
                                </tbody>

                                <thead>
                                <tr style="background-color: #cccccc">
                                    <th>Total Liabilities</th>
                                    <th></th>
                                    <?php  $currTotalLiability = bcadd($currYrCurrliabilitiesTotal, $currYrNonCurrliabilitiesTotal, 2) ?>
                                    <?php  $prevTotalLiability = bcadd($prevYrCurrliabilitiesTotal, $prevYrNonCurrliabilitiesTotal, 2) ?>
                                    <?php  $prev2TotalLiability = bcadd($prev2YrCurrliabilitiesTotal, $prev2YrNonCurrliabilitiesTotal, 2) ?>
                                    <?php  $prev3TotalLiability = bcadd($prev3YrCurrliabilitiesTotal, $prev3YrNonCurrliabilitiesTotal, 2) ?>
                                    <?php  $prev4TotalLiability = bcadd($prev4YrCurrliabilitiesTotal, $prev4YrNonCurrliabilitiesTotal, 2) ?>
                                    <th>{{ $currency."".formatNumber($currTotalLiability) }}</th>
                                    <th>{{ $currency."".formatNumber($prevTotalLiability) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2TotalLiability) }}</th>
                                    <th>{{ $currency."".formatNumber($prev3TotalLiability) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4TotalLiability) }}</th>
                                </tr>
                                </thead>

                                <tr>
                                    <td colspan="8"></td>
                                </tr>

                                <tbody>
                                <tr>
                                    <td colspan="8" style="font-weight: 800; font-size: 16px"> {{ $equity->name }}</td>
                                </tr>
                                @if($groups = $equity->groups)
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 800">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $currYrResult = $group->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcsub($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcsub($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrEquityTotal = bcsub($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrEquityTotal = bcadd($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrEquityTotal = bcsub($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrEquityTotal = bcadd($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                <?php  $prevYrResult = $group->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrEquityTotal = bcsub($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrEquityTotal = bcadd($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                <tr class="clickable-row" data-value="{{ $child->id }}">
                                                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                                                    <td></td>
                                                    <td>
                                                        <?php  $currYrResult = $child->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                        @if($currYrResult['dc'] === 'D')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrEquityTotal = bcsub($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                        @elseif($currYrResult['dc'] === 'C')
                                                            {{ formatNumber($currYrResult['balance']) }}
                                                            <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrEquityTotal = bcsub($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrEquityTotal = bcsub($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev2YrEquityTotal = bcadd($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrEquityTotal = bcsub($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev3YrEquityTotal = bcadd($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <?php  $prevYrResult = $child->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                        @if($prevYrResult['dc'] === 'D')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrEquityTotal = bcsub($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                        @elseif($prevYrResult['dc'] === 'C')
                                                            {{ formatNumber($prevYrResult['balance']) }}
                                                            <?php  $prev4YrEquityTotal = bcadd($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                        @else
                                                            0.00
                                                        @endif
                                                    </td>
                                                </tr>
                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                        <tr class="clickable-row" data-value="{{  $subs->id }}">
                                                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <?php  $currYrResult = $subs->getBalance($firstYear['starts'], $firstYear['ends'], null, true) ?>
                                                                @if($currYrResult['dc'] === 'D')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrEquityTotal = bcsub($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                                @elseif($currYrResult['dc'] === 'C')
                                                                    {{ formatNumber($currYrResult['balance']) }}
                                                                    <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($secondYear['starts'], $secondYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrEquityTotal = bcsub($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($thirdYear['starts'], $thirdYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrEquityTotal = bcsub($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev2YrEquityTotal = bcadd($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs > getBalance($fourthYear['starts'], $fourthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrEquityTotal = bcsub($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev3YrEquityTotal = bcadd($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <?php  $prevYrResult = $subs->getBalance($fifthYear['starts'], $fifthYear['ends'], null, true) ?>
                                                                @if($prevYrResult['dc'] === 'D')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrEquityTotal = bcsub($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                                @elseif($prevYrResult['dc'] === 'C')
                                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                                    <?php  $prev4YrEquityTotal = bcadd($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                                @else
                                                                    0.00
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                @if($equity->ledgers->count())
                                    @foreach($equity->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        <tr class="view-ledger" data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                            <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                            <td></td>
                                            <td>

                                                @if( $ledger->name == 'Retained Earnings' )
                                                    <?php  $currYrResult = $firstYearProfitBeforeTax ?>
                                                @else
                                                    <?php  $currYrResult = $ledger->getBalance(true, $firstYear['starts'], $firstYear['ends'], null, true, $equity->name) ?>
                                                @endif

                                                @if($currYrResult['dc'] === 'D')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcsub($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                @elseif($currYrResult['dc'] === 'C')
                                                    {{ formatNumber($currYrResult['balance']) }}
                                                    <?php  $currYrEquityTotal = bcadd($currYrResult['balance'], $currYrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                @if( $ledger->name == 'Retained Earnings' )
                                                    <?php  $prevYrResult = $secondYearProfitBeforeTax ?>
                                                @else
                                                    <?php  $prevYrResult = $ledger->getBalance(true, $secondYear['starts'], $secondYear['ends'], null, true, $equity->name) ?>
                                                @endif

                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcsub($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prevYrEquityTotal = bcadd($prevYrResult['balance'], $prevYrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                @if( $ledger->name == 'Retained Earnings' )
                                                    <?php  $prevYrResult = $thirdYearProfitBeforeTax ?>
                                                @else
                                                    <?php  $prevYrResult = $ledger->getBalance(true, $thirdYear['starts'], $thirdYear['ends'], null, true, $equity->name) ?>
                                                @endif

                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrEquityTotal = bcsub($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev2YrEquityTotal = bcadd($prevYrResult['balance'], $prev2YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                @if( $ledger->name == 'Retained Earnings' )
                                                    <?php  $prevYrResult = $fourthYearProfitBeforeTax ?>
                                                @else
                                                    <?php  $prevYrResult = $ledger->getBalance(true, $fourthYear['starts'], $fourthYear['ends'], null, true, $equity->name) ?>
                                                @endif

                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrEquityTotal = bcsub($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev3YrEquityTotal = bcadd($prevYrResult['balance'], $prev3YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                            <td>
                                                @if( $ledger->name == 'Retained Earnings' )
                                                    <?php  $prevYrResult = $fifthYearProfitBeforeTax ?>
                                                @else
                                                    <?php  $prevYrResult = $ledger->getBalance(true, $fifthYear['starts'], $fifthYear['ends'], null, true, $equity->name) ?>
                                                @endif

                                                @if($prevYrResult['dc'] === 'D')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrEquityTotal = bcsub($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                @elseif($prevYrResult['dc'] === 'C')
                                                    {{ formatNumber($prevYrResult['balance']) }}
                                                    <?php  $prev4YrEquityTotal = bcadd($prevYrResult['balance'], $prev4YrEquityTotal, 2) ?>
                                                @else
                                                    0.00
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <thead>
                                <tr style="background-color: #cce9ff">
                                    <th>Total Equity</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber($currYrEquityTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prevYrEquityTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev2YrEquityTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev3YrEquityTotal) }}</th>
                                    <th>{{ $currency."".formatNumber($prev4YrEquityTotal) }}</th>
                                </tr>
                                </thead>
                                <thead>
                                <tr style="background-color: #ccc">
                                    <th>Total Equity and Liabilities</th>
                                    <th></th>
                                    <th>{{ $currency."".formatNumber(bcadd($currYrEquityTotal, $currTotalLiability, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prevYrEquityTotal, $prevTotalLiability, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev2YrEquityTotal, $prev2TotalLiability, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev3YrEquityTotal, $prev3TotalLiability, 2)) }}</th>
                                    <th>{{ $currency."".formatNumber(bcadd($prev4YrEquityTotal, $prev4TotalLiability, 2)) }}</th>
                                </tr>
                                </thead>
                            </table>
                            <br>
                            <table class="table table-hover ">

                                <tr>
                                    <td colspan="8"><h4> Income Statement</h4></td>
                                </tr>
                                <?php  $yr1Income = $income->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                <?php  $yr2Income = $income->getBalance($secondYear['starts'], $secondYear['ends'])  ?>
                                <?php  $yr3Income = $income->getBalance($thirdYear['starts'], $thirdYear['ends'])  ?>
                                <?php  $yr4Income = $income->getBalance($fourthYear['starts'], $fourthYear['ends'])  ?>
                                <?php  $yr5Income = $income->getBalance($fifthYear['starts'], $fifthYear['ends'])  ?>

                                <?php  $yr1DirectCost = $direct_cost->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                <?php  $yr2DirectCost = $direct_cost->getBalance($secondYear['starts'], $secondYear['ends'])  ?>
                                <?php  $yr3DirectCost = $direct_cost->getBalance($thirdYear['starts'], $thirdYear['ends'])  ?>
                                <?php  $yr4DirectCost = $direct_cost->getBalance($fourthYear['starts'], $fourthYear['ends'])  ?>
                                <?php  $yr5DirectCost = $direct_cost->getBalance($fifthYear['starts'], $fifthYear['ends'])  ?>

                                <?php  $yr1OtherIncome = $other_income->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                <?php  $yr2OtherIncome = $other_income->getBalance($secondYear['starts'], $secondYear['ends'])  ?>
                                <?php  $yr3OtherIncome = $other_income->getBalance($thirdYear['starts'], $thirdYear['ends'])  ?>
                                <?php  $yr4OtherIncome = $other_income->getBalance($fourthYear['starts'], $fourthYear['ends'])  ?>
                                <?php  $yr5OtherIncome = $other_income->getBalance($fifthYear['starts'], $fifthYear['ends'])  ?>

                                <?php  $yr1Exp = 0 ?>
                                <?php  $yr2Exp = 0 ?>
                                <?php  $yr3Exp = 0 ?>
                                <?php  $yr4Exp = 0 ?>
                                <?php  $yr5Exp = 0 ?>

                                <tr class="clickable-row" data-value="{{ $income->id }}">
                                    <td style="font-weight: 800; font-size: 16px"> {{ $income->name }}</td>

                                    <td></td>
                                    <td>
                                        {{ formatNumber($yr1Income['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr2Income['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr3Income['balance']) }}
                                    </td>
                                    <td>
                                    {{ formatNumber($yr4Income['balance']) }}
                                    <td>
                                        {{ formatNumber($yr5Income['balance']) }}
                                    </td>
                                </tr>
                                <tr class="clickable-row" data-value="{{ $direct_cost->id }}">
                                    <td style="font-weight: 800; font-size: 15px"> {{ $direct_cost->name }}</td>

                                    <td></td>
                                    <td>
                                        {{ formatNumber($yr1DirectCost['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr2DirectCost['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr3DirectCost['balance']) }}
                                    </td>
                                    <td>
                                    {{ formatNumber($yr4DirectCost['balance']) }}
                                    <td>
                                        {{ formatNumber($yr5DirectCost['balance']) }}
                                    </td>
                                </tr>

                                <tr style="background-color: #cce9ff">
                                    <th>Gross Profit</th>
                                    <th></th>
                                    <?php  $grossProfitYr1 = bcsub($yr1Income['balance'], $yr1DirectCost['balance'], 2)?>
                                    <th>{{ $currency."".formatNumber($grossProfitYr1) }}</th>
                                    <?php  $grossProfitYr2 = bcsub($yr2Income['balance'], $yr2DirectCost['balance'], 2)?>
                                    <th>{{  $currency."".formatNumber($grossProfitYr2) }}</th>
                                    <?php  $grossProfitYr3 = bcsub($yr3Income['balance'], $yr3DirectCost['balance'], 2)?>
                                    <th>{{  $currency."".formatNumber($grossProfitYr3) }}</th>
                                    <?php  $grossProfitYr4 = bcsub($yr4Income['balance'], $yr4DirectCost['balance'], 2)?>
                                    <th>{{  $currency."".formatNumber($grossProfitYr4) }}</th>
                                    <?php  $grossProfitYr5 = bcsub($yr5Income['balance'], $yr5DirectCost['balance'], 2)?>
                                    <th>{{  $currency."".formatNumber($grossProfitYr5) }}</th>
                                </tr>
                                <tr>
                                    <td style="font-weight: 800; font-size: 15px"> {{ $other_income->name }}</td>
                                    <td></td>
                                    <td>
                                        {{ formatNumber($yr1OtherIncome['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr2OtherIncome['balance']) }}
                                    </td>
                                    <td>
                                        {{ formatNumber($yr3OtherIncome['balance']) }}
                                    </td>
                                    <td>
                                    {{ formatNumber($yr4OtherIncome['balance']) }}
                                    <td>
                                        {{ formatNumber($yr5OtherIncome['balance']) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="font-weight: 600; font-size: 15px"> {{ $expenses->name }}</td>
                                </tr>
                                @if($groups = $expenses->groups)
                                    @foreach($groups as $group)
                                        <tr class="clickable-row" data-value="{{ $group->id }}">
                                            <td style="font-weight: 600">{{ $group->name }}</td>
                                            <td></td>
                                            <td>
                                                <?php  $result = $group->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                                {{ formatNumber($result['balance']) }}
                                                <?php  $yr1Exp = bcadd($result['balance'], $yr1Exp, 2) ?>
                                            </td>
                                            <td>
                                                <?php  $result = $group->getBalance($secondYear['starts'], $secondYear['ends']) ?>
                                                {{ formatNumber($result['balance']) }}
                                                <?php  $yr2Exp = bcadd($result['balance'], $yr2Exp, 2) ?>
                                            </td>
                                            <td>
                                                <?php  $result = $group->getBalance($thirdYear['starts'], $thirdYear['ends']) ?>
                                                {{ formatNumber($result['balance']) }}
                                                <?php  $yr3Exp = bcadd($result['balance'], $yr3Exp, 2) ?>
                                            </td>
                                            <td>
                                                <?php  $result = $group->getBalance($fourthYear['starts'], $fourthYear['ends']) ?>
                                                {{ formatNumber($result['balance']) }}
                                                <?php  $yr4Exp = bcadd($result['balance'], $yr4Exp, 2) ?>
                                            </td>
                                            <td>
                                                <?php  $result = $group->getBalance($fifthYear['starts'], $fifthYear['ends']) ?>
                                                {{ formatNumber($result['balance']) }}
                                                <?php  $yr5Exp = bcadd($result['balance'], $yr5Exp, 2) ?>
                                            </td>
                                        </tr>
                                        @if($nestedGroup = $group->nestedGroups($group->id))
                                            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)

                                                <?php  $yr1Res = $child->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                                <?php  $yr1Exp = bcadd($yr1Res['balance'], $yr1Exp, 2) ?>

                                                <?php  $yr2Res = $child->getBalance($secondYear['starts'], $secondYear['ends']) ?>
                                                <?php  $yr2Exp = bcadd($yr1Res['balance'], $yr2Exp, 2) ?>

                                                <?php  $yr3Res = $child->getBalance($thirdYear['starts'], $thirdYear['ends']) ?>
                                                <?php  $yr3Exp = bcadd($yr1Res['balance'], $yr3Exp, 2) ?>
                                                <?php  $yr4Res = $child->getBalance($fourthYear['starts'], $fourthYear['ends']) ?>
                                                <?php  $yr4Exp = bcadd($yr4Res['balance'], $yr4Exp, 2) ?>
                                                <?php  $yr5Res = $child->getBalance($fifthYear['starts'], $fifthYear['ends']) ?>
                                                <?php  $yr5Exp = bcadd($yr5Res['balance'], $yr5Exp, 2) ?>

                                                @if($subChild = $child->nestedGroups($child->id))
                                                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)

                                                        <?php  $yr1Res = $subs->getBalance($firstYear['starts'], $firstYear['ends']) ?>
                                                        <?php  $yr1Exp = bcadd($yr1Res['balance'], $yr1Exp, 2) ?>

                                                        <?php  $yr2Res = $subs->getBalance($secondYear['starts'], $secondYear['ends']) ?>
                                                        <?php  $yr2Exp = bcadd($yr1Res['balance'], $yr2Exp, 2) ?>

                                                        <?php  $yr3Res = $subs->getBalance($thirdYear['starts'], $thirdYear['ends']) ?>
                                                        <?php  $yr3Exp = bcadd($yr1Res['balance'], $yr3Exp, 2) ?>
                                                        <?php  $yr4Res = $subs->getBalance($fourthYear['starts'], $fourthYear['ends']) ?>
                                                        <?php  $yr4Exp = bcadd($yr4Res['balance'], $yr4Exp, 2) ?>
                                                        <?php  $yr5Res = $subs->getBalance($fifthYear['starts'], $fifthYear['ends']) ?>
                                                        <?php  $yr5Exp = bcadd($yr5Res['balance'], $yr5Exp, 2) ?>

                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                                @if($expenses->ledgers->count())
                                    @foreach($expenses->ledgers()->where('id', '!=', 44)->orderBy('code', 'asc')->get() as $ledger)

                                        <?php  $yr1Res = $ledger->getBalance(true, $firstYear['starts'], $secondYear['ends']) ?>
                                        @if($yr1Res['dc'] === 'D')
                                            <?php  $yr1Exp = bcadd($yr1Exp, $yr1Res['balance'], 2) ?>
                                        @elseif($yr1Res['dc'] === 'C')
                                            <?php  $yr1Exp = bcsub($yr1Exp, $yr1Res['balance'], 2) ?>
                                        @else
                                            -
                                        @endif

                                        <?php  $yr2Res = $ledger->getBalance(true, $secondYear['starts'], $secondYear['ends']) ?>
                                        @if($yr2Res['dc'] === 'D')
                                            <?php  $yr2Exp = bcadd($yr2Exp, $yr2Res['balance'], 2) ?>
                                        @elseif($yr2Res['dc'] === 'C')
                                            <?php  $yr2Exp = bcsub($yr2Exp, $yr2Res['balance'], 2) ?>
                                        @else
                                            -
                                        @endif

                                        <?php  $yr3Res = $ledger->getBalance(true, $thirdYear['starts'], $thirdYear['ends']) ?>
                                        @if($yr3Res['dc'] === 'D')
                                            <?php  $yr3Exp = bcadd($yr3Exp, $yr3Res['balance'], 2) ?>
                                        @elseif($yr3Res['dc'] === 'C')
                                            <?php  $yr3Exp = bcsub($yr3Exp, $yr3Res['balance'], 2) ?>
                                        @else
                                            -
                                        @endif

                                        <?php  $yr4Res = $ledger->getBalance(true, $fourthYear['starts'], $fourthYear['ends']) ?>
                                        @if($yr4Res['dc'] === 'D')
                                            <?php  $yr4Exp = bcadd($yr4Exp, $yr4Res['balance'], 2) ?>
                                        @elseif($yr4Res['dc'] === 'C')
                                            <?php  $yr4Exp = bcsub($yr4Exp, $yr4Res['balance'], 2) ?>
                                        @else
                                            -
                                        @endif

                                        <?php  $yr5Res = $ledger->getBalance(true, $fifthYear['starts'], $fifthYear['ends']) ?>
                                        @if($yr5Res['dc'] === 'D')
                                            <?php  $yr5Exp = bcadd($yr5Exp, $yr5Res['balance'], 2) ?>
                                        @elseif($yr5Res['dc'] === 'C')
                                            <?php  $yr5Exp = bcsub($yr5Exp, $yr5Res['balance'], 2) ?>
                                        @else
                                            -
                                        @endif
                                    @endforeach
                                @endif

                                <tr style="background-color: #cce9ff">
                                    <th>Total Expenses</th>
                                    <th></th>
                                    <th>{{  $currency."".formatNumber($yr1Exp) }}</th>
                                    <th>{{  $currency."".formatNumber($yr2Exp) }}</th>
                                    <th>{{  $currency."".formatNumber($yr3Exp) }}</th>
                                    <th>{{  $currency."".formatNumber($yr4Exp) }}</th>
                                    <th>{{  $currency."".formatNumber($yr5Exp) }}</th>
                                </tr>


                                <tr style="background-color: #cce9ff">
                                    <th>Net Profit</th>
                                    <th></th>
                                    <th>
                                        <?php  $yr1PBT = ($grossProfitYr1 + $yr1OtherIncome['balance']) - $yr1Exp ?>
                                        {{  $currency."".formatNumber($yr1PBT) }}
                                    </th>
                                    <th>
                                        <?php  $yr2PBT = ($grossProfitYr2 + $yr2OtherIncome['balance']) - $yr2Exp ?>
                                        {{  $currency."".formatNumber($yr2PBT) }}
                                    </th>
                                    <th>
                                        <?php  $yr3PBT = ($grossProfitYr3 + $yr3OtherIncome['balance']) - $yr3Exp ?>
                                        {{  $currency."".formatNumber($yr3PBT) }}
                                    </th>
                                    <th>
                                        <?php  $yr4PBT = ($grossProfitYr4 + $yr4OtherIncome['balance']) - $yr4Exp ?>
                                        {{  $currency."".formatNumber($yr4PBT) }}
                                    </th>
                                    <th>
                                        <?php  $yr5PBT = ($grossProfitYr5 + $yr5OtherIncome['balance']) - $yr5Exp ?>
                                        {{  $currency."".formatNumber($yr5PBT) }}
                                    </th>
                                </tr>

                                {{--<tr>
                                    <td>Company/Income Tax</td>
                                    <td></td>
                                    <td>
                                        @if($yr1PBT < 0)
                                            -
                                            --}}<?php  $yr1IncomeTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr1IncomeTax = (30 / 100) * $yr1PBT ?>{{--
                            {{ formatNumber($yr1IncomeTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr2PBT < 0)
                            -
                            --}}<?php  $yr2IncomeTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr2IncomeTax = (30 / 100) * $yr2PBT ?>{{--
                            {{ formatNumber($yr2IncomeTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr3PBT < 0)
                            -
                            --}}<?php  $yr3IncomeTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr3IncomeTax = (30 / 100) * $yr3PBT ?>{{--
                            {{ formatNumber($yr3IncomeTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr4PBT < 0)
                            -
                            --}}<?php  $yr4IncomeTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr4IncomeTax = (30 / 100) * $yr4PBT ?>{{--
                            {{ formatNumber($yr4IncomeTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr5PBT < 0)
                            -
                            --}}<?php  $yr5IncomeTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr5IncomeTax = (30 / 100) * $yr5PBT ?>{{--
                            {{ formatNumber($yr5IncomeTax) }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Education Tax</td>
                    <td></td>
                    <td>
                        @if($yr1PBT < 0)
                            -
                            --}}<?php  $yr1EduTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr1EduTax = (2 / 100) * $yr1PBT ?>{{--
                            {{ formatNumber($yr1EduTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr2PBT < 0)
                            -
                            --}}<?php  $yr2EduTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr2EduTax = (2 / 100) * $yr2PBT ?>{{--
                            {{ formatNumber($yr2EduTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr3PBT < 0)
                            -
                            --}}<?php  $yr3EduTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr3EduTax = (2 / 100) * $yr3PBT ?>{{--
                            {{ formatNumber($yr3EduTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr4PBT < 0)
                            -
                            --}}<?php  $yr4EduTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr4EduTax = (2 / 100) * $yr4PBT ?>{{--
                            {{ formatNumber($yr4EduTax) }}
                        @endif
                    </td>
                    <td>
                        @if($yr5PBT < 0)
                            -
                            --}}<?php  $yr5EduTax = 0 ?>{{--
                        @else
                            --}}<?php  $yr5EduTax = (2 / 100) * $yr5PBT ?>{{--
                            {{ formatNumber($yr5EduTax) }}
                        @endif
                    </td>
                </tr>

                <tr style="background-color: #cce9ff">
                    <th>Net Profit</th>
                    <th></th>
                    --}}<?php  $yr1Tax = bcadd($yr1IncomeTax, $yr1EduTax, 2) ?>{{--
                    --}}<?php  $yr2Tax = bcadd($yr2IncomeTax, $yr2EduTax, 2) ?>{{--
                    --}}<?php  $yr3Tax = bcadd($yr3IncomeTax, $yr3EduTax, 2) ?>{{--
                    --}}<?php  $yr4Tax = bcadd($yr4IncomeTax, $yr4EduTax, 2) ?>{{--
                    --}}<?php  $yr5Tax = bcadd($yr5IncomeTax, $yr5EduTax, 2) ?>{{--
                    <th>{{ formatNumber(bcsub($yr1PBT, $yr1Tax )) }}</th>
                    <th>{{ formatNumber(bcsub($yr2PBT, $yr2Tax )) }}</th>
                    <th>{{ formatNumber(bcsub($yr3PBT, $yr3Tax )) }}</th>
                    <th>{{ formatNumber(bcsub($yr4PBT, $yr4Tax )) }}</th>
                    <th>{{ formatNumber(bcsub($yr5PBT, $yr5Tax )) }}</th>
                </tr>--}}
                            </table>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </aside>
</div>