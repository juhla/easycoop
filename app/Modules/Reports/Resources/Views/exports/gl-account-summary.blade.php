<div class="hbox stretch">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>GL Account Summary</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Accounts</th>
                                    <th>Type</th>
                                    <th>Opening Balance</th>
                                    <th>Closing Balance</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($classes as $class)
                                    <tr>
                                        <td><h4> {{ $class->name }}</h4></td>
                                        <td>Class</td>
                                        <td>-</td>
                                        <td>-</td>

                                    </tr>
                                    @if($groups = $class->groups)
                                        @foreach($groups as $group)
                                            <tr>
                                                <td style="font-weight: 800">{{ $group->code.' - &nbsp;'.$group->name }}</td>
                                                <td>Group</td>
                                                <td>-</td>
                                                <td>-</td>

                                            </tr>
                                            @if($nestedChild = $group->nestedGroups($group->id))
                                                {!! displayReportItem($nestedChild->get(), 'gl-account-summary') !!}
                                            @endif
                                            @if($group->ledgers->count())
                                                @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                    <tr>
                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                                    href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a>
                                                        </td>
                                                        <td>Ledger</td>
                                                        <td>
                                                            <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ formatNumber($ledger->op_balance) }}</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->getClosingBalance() }}</a>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($class->ledgers->count())
                                        @foreach($class->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                            <tr>
                                                <td>
                                                    <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a>
                                                </td>
                                                <td>Ledger</td>
                                                <td>
                                                    <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ formatNumber($ledger->op_balance) }}</a>
                                                </td>
                                                <td>
                                                    <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->getClosingBalance() }}</a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>