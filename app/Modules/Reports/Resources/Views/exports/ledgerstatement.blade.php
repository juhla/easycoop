<div class="hbox stretch">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>
                                <strong>Ledger</strong> Statement {{ ($ledger) ? 'for '. $ledger->code .' - '.$ledger->name : '' }}
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @if($ledger !== '')

                            <table class="table table-hover" style="font-family: DejaVu Sans;">
                                <thead>
                                <tr>
                                    <th align="left">Date</th>
                                    <th>Ref #</th>
                                    {{--<th>Type</th>--}}
                                    <th>Description</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody align="center">
                                @if($transactions)
                                    <input type="hidden" name="ledger" id="ledger" value="{{ $ledger->id }}">
                                    <tr>
                                        <td align="left" colspan="5"><i><strong>Opening Balance</strong></i></td>
                                        <td><i><strong>{{ $currency."".formatNumber( abs( $openingBalance ) ) }}</strong></i></td>
                                    </tr>
                                    @if($profitLossLedger)
                                        {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType ) !!}
                                    @endif
                                    @if($balanceSheetLedger)
                                        {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType ) !!}
                                    @endif
                                @else
                                    <tr>
                                        <td colspan="7">
                                            <div class="alert alert-warning">
                                                No transactions found for the selected account.
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        @else
                            <div class="row">
                                <div class="alert alert-info">
                                    Please select a GL Account above and submit to view all transaction for the selected account.
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>