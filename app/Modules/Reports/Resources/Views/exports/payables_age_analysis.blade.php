<div class="scrollable wrapper w-f">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Payables Age Analysis</h3>
            <div class="clearfix"></div>
        </div>

        <div class="panel-body" style="font-family: DejaVu Sans;">
            <table class="table table-hover table-bordered" border="1" >
                <thead>
                <tr>
                    <th align="">Supplier</th>
                    <th style="text-align:left">Total Amount.</th>
                    <th style="text-align:left">Below 30 Days</th>
                    <th style="text-align:left">30 - 60 Days</th>
                    <th style="text-align:left">60 - 90 Days</th>
                    <th style="text-align:left">90 - 180 Days</th>
                    <th style="text-align:left">180 Days and Above</th>
                </tr>
                </thead>
                <tbody align="center">
                @foreach($rec as $ledger)
                    @if($ledger['total'] != 0):
                    <tr>
                        <td>{{ $ledger['supplier'] }}</td>
                        <td>{{ $ledger['total'] }}</td>
                        <td class="text-primary">{{ $ledger['30_days'] }}</td>
                        <td class="text-info">{{ $ledger['30_60_days'] }}</td>
                        <td class="text-info">{{ $ledger['60_90_days'] }}</td>
                        <td class="text-warning">{{ $ledger['90_180_days'] }}</td>
                        <td class="text-danger">{{ $ledger['180_above_days'] }}</td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
