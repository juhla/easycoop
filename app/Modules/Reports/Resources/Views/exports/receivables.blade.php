<div class="scrollable wrapper w-f">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Receivables Account</h3>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="font-family: DejaVu Sans;">

            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="text-align:left">Account Name</th>
                    <th style="text-align:left">Total Amount Due</th>
                    <th style="text-align:left">Total Amount Received</th>
                    <th style="text-align:left">Total Balance</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($ledgers))
                    @foreach( $ledgers as $ledger )
                        <tr>

                            <td><a class="ledger"
                                   href="payables-age-analysis/{{  $ledger['id']  }}">{{ $ledger['name'] }}</a>
                            </td>
                            <td style="text-align:left">{{ formatNumber( $ledger['due'] ) }}</td>
                            <td style="text-align:left">{{ formatNumber( $ledger['paid'] ) }}</td>
                            <td style="text-align:left">{{ formatNumber( $ledger['balance'] ) }}</td>

                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>


            {{--<div class="row">
                <div class="alert alert-info">
                    Please click on any of the accounts to view transaction age analysis.
                </div>
            </div>--}}
        </div>
    </div>
</div>
