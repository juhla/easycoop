<table class="table table-hover " id="inflowTable" style="font-family: DejaVu Sans;">
    <thead>
    <tr>
        <th width="15%">Date</th>
        {{-- <th width="15%">Inv./PCV/PV No.</th> --}}
        <th width="18%">Description</th>
        <th width="18%">Ledger Account</th>
        <th width="18%">Debit Account</th>
        <th width="18%">Credit Account</th>
    </tr>
    </thead>
    <tbody>
		@forelse($transaction->item as $trans)
            <tr>
                <td>{{ date('m/d/Y', strtotime($transaction->transaction_date)) }}</td>
                <td></td>                                               
                <td>{{ getLedgerName($trans->ledger_id) }}</td>
                @if($trans->dc === 'D')
                    <td>{{ $trans->amount }}</td>
                    <td>0.00</td>
                @elseif($trans->dc === 'C')
                    <td>0.00</td>
                    <td>{{ $trans->amount }}</td>
                @endif
            </tr>
        @empty

        @endforelse
    </tbody>
</table>
