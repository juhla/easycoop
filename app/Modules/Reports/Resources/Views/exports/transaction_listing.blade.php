<div class="hbox stretch" style="font-family: DejaVu Sans;">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>
                                Transaction Listing
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed m-b-none">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Ref #</th>
                                    <th>Description</th>
                                    <th>Debit</th>
                                    <th>Credit</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <?php  $range = [] ?>
                                @if($start_date !== '')
                                    <?php  $range['start_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
                                @endif
                                @if($end_date !== '')
                                    <?php  $range['end_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d') ?>
                                @endif
                                @foreach($classes as $class)
                                    <tr>
                                        <td colspan="6"><h4> {{ $class->name }}</h4></td>
                                    </tr>
                                    @if($groups = $class->groups)
                                        @foreach($groups as $group)
                                            <tr>
                                                <td colspan="6"
                                                    style="font-weight: 800">{{ $group->code.' - &nbsp;'.$group->name }}</td>
                                            </tr>
                                            @if($nestedChild = $group->nestedGroups($group->id))
                                                {!! displayReportItem($nestedChild->get(), 'transaction-listing',0,0, $range) !!}
                                            @endif
                                            @if($group->ledgers->count())
                                                @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                    <thead>
                                                    <tr>
                                                        <th colspan="6"><a
                                                                    href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a>
                                                        </th>
                                                    </tr>
                                                    </thead>


                                                    <?php  $transactions = $ledger->getTransactionsForLedger($ledger->id, null, $range) ?>
                                                    <?php
                                                    $op = $ledger->calculateOpeningBalance(null);
                                                    $openingBalance = $op[0];
                                                    $openingBalanceType = $op[1];
                                                    ?>
                                                    @if($transactions)
                                                        <tbody>
                                                        <tr>
                                                            <td colspan="5"><strong>Opening Balance</strong></td>
                                                            <td>
                                                                <i><strong>{{ formatNumber( abs( $openingBalance ) ) }}</strong></i>
                                                            </td>
                                                        </tr>

                                                        {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType, true) !!}
                                                        </tbody>
                                                    @else

                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($class->ledgers->count())
                                        @foreach($class->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                            <thead>
                                            <tr>
                                                <th colspan="6"><a
                                                            href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a>
                                                </th>
                                            </tr>
                                            </thead>

                                            <?php  $transactions = $ledger->getTransactionsForLedger($ledger->id, null, $range) ?>
                                            <?php
                                            $op = $ledger->calculateOpeningBalance(null);
                                            $openingBalance = $op[0];
                                            $openingBalanceType = $op[1];
                                            ?>
                                            @if($transactions)
                                                <tbody>
                                                <tr>
                                                    <td colspan="5"><strong>Opening Balance</strong></td>
                                                    <td><i><strong>{{ formatNumber( abs( $openingBalance ) ) }}</strong></i>
                                                    </td>
                                                </tr>

                                                {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType) !!}
                                                </tbody>
                                            @else

                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>