<?php
$type = request()->get('type');
?>
<div class="hbox stretch">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>
                                {{getBusinessOwnerAuth()->company_name }} - Trial Balance as
                                at {{ $report_date }}
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @if($type == 'xls')
                            @include('reports::exports._trial-balance_upgrade')
                        @elseif($type == 'pdf')
                            @include('reports::partials._trial-balance_upgrade')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
