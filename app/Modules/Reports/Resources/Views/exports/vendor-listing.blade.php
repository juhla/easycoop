<div class="hbox stretch" style="font-family: DejaVu Sans;">
    <div>
        <div class="vbox">
            <div class="scrollable wrapper w-f">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="">
                            <h4>List of Vendors</h4>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>Phone</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($vendors) > 0)
                                    @foreach($vendors as $vendor)
                                        <tr>
                                            <td>{{ $vendor->name }}</td>
                                            <td>{{ ($vendor->email) ? $vendor->email : 'N/A' }}</td>
                                            <td>{!! ($vendor->address) ? $vendor->address : 'N/A '!!}</td>
                                            <td>{{ ($vendor->country_id) ? getCountry($vendor->country_id) : 'N/A' }}</td>
                                            <td>{{ ($vendor->phone_no) ? $vendor->phone_no : 'N/A' }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="6">You have not added any vendor.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>