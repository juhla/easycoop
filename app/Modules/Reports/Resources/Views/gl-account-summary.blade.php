@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/gl-account-summary/export?type=pdf') }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/gl-account-summary/export?type=xls') }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title"><h4>GL Account Summary</h4></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="inflowTable">
                                    <thead>
                                    <tr>
                                        <th width="40%">Accounts</th>
                                        <th>Type</th>
                                        <th>Opening Balance</th>
                                        <th>Closing Balance</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($classes as $class)
                                        <tr>
                                            <td><h4> {{ $class->name }}</h4></td>
                                            <td>Class</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td></td>
                                        </tr>
                                        @if($groups = $class->groups)
                                            @foreach($groups as $group)
                                                <tr>
                                                    <td style="font-weight: 800">{!! $group->code.' - &nbsp;'.$group->name !!}  </td>
                                                    <td>Group</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td></td>
                                                </tr>
                                                @if($nestedChild = $group->nestedGroups($group->id))
                                                    {!! displayReportItem($nestedChild->get(), 'gl-account-summary') !!}
                                                @endif
                                                @if($group->ledgers->count())
                                                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                        <tr>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{!! $ledger->code.' - &nbsp;'.$ledger->name !!}</a></td>
                                                            <td>Ledger</td>
                                                            <td>
                                                                <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ formatNumber($ledger->op_balance) }}</a>
                                                            </td>
                                                            <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->getClosingBalance() }}</a></td>
                                                            <td>
                                                                <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}"> <i class="fa fa-eye"></i>View</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                        @if($class->ledgers->count())
                                            @foreach($class->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                <tr>
                                                    <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{!! $ledger->code.' - &nbsp;'.$ledger->name !!}</a></td>
                                                    <td>Ledger</td>
                                                    <td>
                                                        <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ formatNumber($ledger->op_balance) }}</a>
                                                    </td>
                                                    <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->getClosingBalance() }}</a></td>
                                                    <td>
                                                        <a href="{{ url('reports/ledger-statement/'.$ledger->id) }}"> <i class="fa fa-eye"></i>View</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop