@extends('layouts.main')

@section('content')
    <style>
        tr.clickable-row {
            cursor: pointer;
        }

        tr.view-ledger {
            cursor: pointer;
        }
    </style>
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/detailed-income-statement/export?type=pdf&from_date='. $start_date.'&to_date='.$end_date) }}"
                                   class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/detailed-income-statement/export?type=xls&from_date='. $start_date.'&to_date='.$end_date) }}"
                                   class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                        <form id="filter_ledger" action="{{ url('reports/detailed-income-statement') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" id="from_date"
                                           name="from_date" value="{{ $start_date }}" placeholder="From:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" name="to_date"
                                           id="to_date" value="{{ $end_date }}" placeholder="To:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i
                                                    class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="btn-group col-md-3">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Filter</button>
                                <a href="{{ url('reports/detailed-income-statement') }}"
                                   class="btn btn-default">Clear</a>
                            </div>
                        </form>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>{{getBusinessOwnerAuth()->company_name }} - Detailed Income Statement</h4></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="inflowTable">
                                    <thead>
                                    <tr>
                                        <th width="40%"></th>
                                        <th></th>
                                        <th>Current Month <br>₦</th>
                                        <th>Year to Date <br>₦</th>
                                    </tr>
                                    </thead>
                                    <?php  $currMonthIncome = 0 ?>
                                    <?php  $incomeYearToDate = 0 ?>
                                    <?php  $currMonthDirectCost = 0 ?>
                                    <?php  $yearToDateDirectCost = 0 ?>
                                    <?php  $currMonthOIncome = 0 ?>
                                    <?php  $oIncomeYearToDate = 0 ?>
                                    <?php  $currMonthExp = 0 ?>
                                    <?php  $yearToDateExp = 0 ?>


                                    @if($search)

                                        <?php

                                        try {
                                            $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d');
                                            $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d');
                                            $month = new Carbon\Carbon('first day of this month');
                                            $current_month = $month->format('Y-m-d');
                                        } catch (Exception $ex) {
                                            $end_date = $start_date = $current_month = "";
                                        }
                                        ?>
                                    @else
                                        <?php  $month = new Carbon\Carbon('first day of this month') ?>
                                        <?php  $year = new Carbon\Carbon('first day of January ' . date('Y')) ?>
                                        <?php  $current_month = $month->format('Y-m-d') ?>
                                        <?php  $start_date = $year->format('Y-m-d') ?>
                                        <?php  $end_date = date('Y-m-d') ?>
                                    @endif

                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $income->name }}</td>
                                    </tr>
                                    @if($income->ledgers->count())
                                        @foreach($income->ledgers()->orderBy('code', 'asc')->where('flag', 'Active')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                    @if($result['dc'] === 'D')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthIncome = bcsub($currMonthIncome, $result['balance'], 2) ?>
                                                    @elseif($result['dc'] === 'C')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthIncome = bcadd($result['balance'], $currMonthIncome, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                    @if($yeartodate['dc'] === 'D')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $incomeYearToDate = bcsub($incomeYearToDate, $yeartodate['balance'], 2) ?>
                                                    @elseif($yeartodate['dc'] === 'C')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $incomeYearToDate = bcadd($yeartodate['balance'], $incomeYearToDate, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Income</th>
                                        <th></th>
                                        <th>{{ formatNumber($currMonthIncome) }}</th>
                                        <th>{{ formatNumber($incomeYearToDate) }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $direct_cost->name }}</td>
                                    </tr>
                                    @if($direct_cost->ledgers->count())
                                        @foreach($direct_cost->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                    @if($result['dc'] === 'D')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthDirectCost = bcadd($currMonthDirectCost, $result['balance'], 2) ?>
                                                    @elseif($result['dc'] === 'C')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthDirectCost = bcsub($currMonthDirectCost, $result['balance'], 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                    @if($yeartodate['dc'] === 'D')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $yearToDateDirectCost = bcadd($yearToDateDirectCost, $yeartodate['balance'], 2) ?>
                                                    @elseif($yeartodate['dc'] === 'C')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $yearToDateDirectCost = bcsub($yearToDateDirectCost, $yeartodate['balance'], 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Direct Cost</th>
                                        <th></th>
                                        <th>{{ formatNumber($currMonthDirectCost) }}</th>
                                        <th>{{ formatNumber($yearToDateDirectCost) }}</th>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Gross Profit</th>
                                        <th></th>
                                        <?php  $grossProfitMonth = bcsub($currMonthIncome, $currMonthDirectCost, 2)?>
                                        <th>{{ formatNumber($grossProfitMonth) }}</th>
                                        <?php  $grossProfitYTD = bcsub($incomeYearToDate, $yearToDateDirectCost, 2)?>
                                        <th>{{ formatNumber($grossProfitYTD) }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $other_income->name }}</td>
                                    </tr>
                                    @if($other_income->ledgers->count())
                                        @foreach($other_income->ledgers()->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                    @if($result['dc'] === 'D')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthOIncome = bcsub($currMonthOIncome, $result['balance'], 2) ?>
                                                    @elseif($result['dc'] === 'C')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthOIncome = bcadd($result['balance'], $currMonthOIncome, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                    @if($yeartodate['dc'] === 'D')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $oIncomeYearToDate = bcsub($oIncomeYearToDate, $yeartodate['balance'], 2) ?>
                                                    @elseif($yeartodate['dc'] === 'C')
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                        <?php  $oIncomeYearToDate = bcadd($yeartodate['balance'], $oIncomeYearToDate, 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    <tr>
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Other Income Total</th>
                                        <th></th>
                                        <th>{{ formatNumber($currMonthOIncome) }}</th>
                                        <th>{{ formatNumber($oIncomeYearToDate) }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4"
                                            style="font-weight: 800; font-size: 16px"> {{ $expenses->name }}</td>
                                    </tr>
                                    @if($groups = $expenses->groups)
                                        @foreach($groups as $group)
                                            <tr>
                                                <td colspan="4" style="font-weight: 800">{{ $group->name }}</td>
                                            </tr>
                                            @if($group->ledgers->count())
                                                @foreach($group->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                    <tr class="view-ledger"
                                                        data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                        <td><span class="account-group-child">{{ $ledger->name }}

                                                            </span>
                                                        </td>
                                                        <td></td>
                                                        <td>
                                                            <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                            @if($result['dc'] === 'D')
                                                                {{ formatNumber($result['balance']) }}
                                                                <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                            @elseif($result['dc'] === 'C')
                                                                {{ formatNumber($result['balance']) }}
                                                                <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                            @else
                                                                -
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                            @if($yeartodate['dc'] === 'D')
                                                                <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                {{ formatNumber($yeartodate['balance']) }}
                                                            @elseif($yeartodate['dc'] === 'C')
                                                                <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                {{ formatNumber($yeartodate['balance']) }}
                                                            @else
                                                                -
                                                            @endif
                                                                <?php //pr($yeartodate); ?>
                                                                <?php //pr($yearToDateExp); ?>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            @if($nestedGroup = $group->nestedGroups($group->id))
                                                @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                                                    <tr>
                                                        <td colspan="4"><span class="account-group-child">{{ $child->name }}
                                                                </span></td>
                                                    </tr>
                                                    @if($child->ledgers->count())
                                                        @foreach($child->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                            <tr class="view-ledger"
                                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                                <td><span class="account-group-child">{{ $ledger->name }}
                                                                         </span></td>
                                                                <td></td>
                                                                <td>
                                                                    <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                                    @if($result['dc'] === 'D')
                                                                        {{ formatNumber($result['balance']) }}
                                                                        <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                                    @elseif($result['dc'] === 'C')
                                                                        {{ formatNumber($result['balance']) }}
                                                                        <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                                    @if($yeartodate['dc'] === 'D')
                                                                        <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                        {{ formatNumber($yeartodate['balance']) }}
                                                                    @elseif($yeartodate['dc'] === 'C')
                                                                        <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                        {{ formatNumber($yeartodate['balance']) }}
                                                                    @else
                                                                        -
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                    @if($subChild = $child->nestedGroups($child->id))
                                                        @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                                            <tr class="clickable-row" data-value="{{ $subs->id }}">
                                                                <td colspan="4"><span
                                                                            class="account-group-child">{{ $subs->name }}</span>
                                                                </td>
                                                            </tr>
                                                            @if($subs->ledgers->count())
                                                                @foreach($subs->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                                                    <tr class="view-ledger"
                                                                        data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                                        <td>
                                                                            <span class="account-group-child">{{ $ledger->name }} </span>
                                                                        </td>
                                                                        <td></td>
                                                                        <td>
                                                                            <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                                            @if($result['dc'] === 'D')
                                                                                {{ formatNumber($result['balance']) }}
                                                                                <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                                            @elseif($result['dc'] === 'C')
                                                                                {{ formatNumber($result['balance']) }}
                                                                                <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                                            @if($yeartodate['dc'] === 'D')
                                                                                <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                                {{ formatNumber($yeartodate['balance']) }}
                                                                            @elseif($yeartodate['dc'] === 'C')
                                                                                <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                                                {{ formatNumber($yeartodate['balance']) }}
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($expenses->ledgers->count())
                                        @foreach($expenses->ledgers()->where('id', '!=', 44)->where('flag', 'Active')->orderBy('code', 'asc')->get() as $ledger)
                                            <tr class="view-ledger"
                                                data-url="{{ url('reports/ledger-statement/'.$ledger->id) }}">
                                                <td><span class="account-group-child">{{ $ledger->name }}</span></td>
                                                <td></td>
                                                <td>
                                                    <?php  $result = $ledger->getBalance(true, $current_month, $end_date) ?>
                                                    @if($result['dc'] === 'D')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthExp = bcadd($currMonthExp, $result['balance'], 2) ?>
                                                    @elseif($result['dc'] === 'C')
                                                        {{ formatNumber($result['balance']) }}
                                                        <?php  $currMonthExp = bcsub($currMonthExp, $result['balance'], 2) ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    <?php  $yeartodate = $ledger->getBalance(true, $start_date, $end_date) ?>
                                                    @if($yeartodate['dc'] === 'D')
                                                        <?php  $yearToDateExp = bcadd($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                    @elseif($yeartodate['dc'] === 'C')
                                                        <?php  $yearToDateExp = bcsub($yearToDateExp, $yeartodate['balance'], 2) ?>
                                                        {{ formatNumber($yeartodate['balance']) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Total Expenses</th>
                                        <th></th>
                                        <th>{{ formatNumber($currMonthExp) }}</th>
                                        <th>{{ formatNumber($yearToDateExp) }}</th>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Net Profit</th>
                                        <th></th>
                                        <th>
                                            <?php  $currMonthPBT = bcadd($grossProfitMonth, $currMonthOIncome, 2) ?>
                                            <?php  $currMonthPBT = bcsub($currMonthPBT, $currMonthExp, 2)?>
                                            {{ formatNumber($currMonthPBT) }}
                                        </th>
                                        <th>
                                            <?php  $yearToDatePBT = bcadd($grossProfitYTD, $oIncomeYearToDate, 2) ?>
                                            <?php  $yearToDatePBT = bcsub($yearToDatePBT, $yearToDateExp, 2) ?>
                                            {{ formatNumber($yearToDatePBT) }}
                                        </th>
                                    </tr>
                                    </thead>
                                    {{--<tbody>
                                    <tr>
                                        <td>Company/Income Tax</td>
                                        <td></td>
                                        <td>
                                            @if($currMonthPBT < 0)
                                                -
                                                --}}<?php  $currMonthIncomeTax = 0 ?>{{--
                                            @else
                                                --}}<?php  $currMonthIncomeTax = (30 / 100) * $currMonthPBT ?>{{--
                                                {{ formatNumber($currMonthIncomeTax) }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($yearToDatePBT < 0)
                                                -
                                                --}}<?php  $yearToDateIncomeTax = 0 ?>{{--
                                            @else
                                                --}}<?php  $yearToDateIncomeTax = (30 / 100) * $yearToDatePBT ?>{{--
                                                {{ formatNumber($yearToDateIncomeTax) }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Education Tax</td>
                                        <td></td>
                                        <td>
                                            @if($currMonthPBT < 0)
                                                -
                                                --}}<?php  $currMonthEduTax = 0 ?>{{--
                                            @else
                                                --}}<?php  $currMonthEduTax = (2 / 100) * $currMonthPBT ?>{{--
                                                {{ formatNumber($currMonthEduTax) }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($yearToDatePBT < 0)
                                                -
                                                --}}<?php  $yearToDateEduTax = 0 ?>{{--

                                            @else
                                                --}}<?php  $yearToDateEduTax = (2 / 100) * $yearToDatePBT ?>{{--
                                                {{ formatNumber($yearToDateEduTax) }}
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                    <thead>
                                    <tr style="background-color: #cce9ff">
                                        <th>Net Profit</th>
                                        <th></th>
                                        --}}<?php  $currMonthTax = bcadd($currMonthIncomeTax, $currMonthEduTax, 2) ?>{{--
                                        --}}<?php  $yearToDateTax = bcadd($yearToDateIncomeTax, $yearToDateEduTax, 2) ?>{{--
                                        <th>{{ formatNumber(bcsub($currMonthPBT, $currMonthTax )) }}</th>
                                        <th>{{ formatNumber(bcsub($yearToDatePBT, $yearToDateTax )) }}</th>
                                    </tr>
                                    </thead>--}}
                                </table>
                            </div>
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
    @include('reports::partials._bs-items')
@stop
@section('scripts')
    <script>
        $(document).ready(function ($) {
            var $baseurl = $('#baseurl').val();
            //view ledger
            $(".view-ledger").click(function () {
                window.document.location = $(this).data("url");
            });

            //view group items
            $('.clickable-row').click(function () {
                var id = $(this).data('value');
                $.get($baseurl + '/setup/accounts/get-ledger-accounts/' + id, function (res) {
                    $('#list-items').empty();
                    $('#list-items').append(res);
                    $('#bsItemsModal').modal('show');
                });
            });
        });
    </script>
@stop