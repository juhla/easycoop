@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                @if($ledger)
                                    <a href="{{ url('reports/ledger-statement?type=pdf&ledger_id='.$ledger->id.'&from_date='.$start_date.'&to_date='.$end_date) }}"
                                       class="btn btn-sm btn-default pdf" title="Export to PDF">PDF</a>
                                    <a href="{{ url('reports/ledger-statement?type=xls&ledger_id='.$ledger->id.'&from_date='.$start_date.'&to_date='.$end_date)}}"
                                       class="btn btn-sm btn-default xls" title="Export to Excel">EXCEL</a>
                                @else
                                    <a href="#" class="btn btn-sm btn-default pdf" title="Export to PDF">PDF</a>
                                    <a href="#" class="btn btn-sm btn-default xls" title="Export to Excel">EXCEL</a>
                                @endif
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>--}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h3><strong>Ledger</strong>
                                Statement {{ ($ledger) ? 'for '. $ledger->code .' - '.$ledger->name : '' }}</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" align="">
                            <div class="col-md-12 col-lg-12 m-t-md">
                                <form id="filter_ledger_stmt" action="{{ url('reports/ledger-statement') }}"
                                      method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    @if($ledger)
                                        <input type="hidden" name="ledger_no" id="ledger_no" value="{{ $ledger->id }}">
                                    @endif
                                    <div class="form-group col-md-3">
                                        <select class="select2-option" name="ledger_id" id="ledger_id"
                                                style="width: 100%" required>
                                            <option value="">Select Ledger Account</option>
                                            @foreach($ledgers as $ledgers)
                                                <option value="{{ $ledgers->id }}"
                                                @if($ledger)
                                                    {{ ($ledgers->id === $ledger->id)? "selected":"" }}
                                                        @endif>
                                                    {{ $ledgers->code. ' - ' .$ledgers->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group date form_datetime" data-picker-position="bottom-left">
                                            <input type="text" class="form-control datepicker" autocomplete="off" name="from_date"
                                                   id="from_date" placeholder="From:" value="{{ $start_date }}">
                                            <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i
                                                         class="fa fa-calendar"></i></button>
                                         </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="input-group date form_datetime" data-picker-position="bottom-left">
                                            <input type="text" class="form-control datepicker" autocomplete="off" name="to_date"
                                                   id="to_date" placeholder="To:" value="{{ $end_date }}">
                                            <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i
                                                         class="fa fa-calendar"></i></button>
                                         </span>
                                        </div>
                                    </div>
                                    <div class="btn-group col-md-3">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Submit
                                        </button>
                                        <a href="{{ url('reports/ledger-statement') }}"
                                           class="btn btn-default">Clear</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <div class="panel-body">
                            @if($ledger !== '')

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th align="left">Date</th>
                                        <th>Ref #</th>
                                        {{--<th>Type</th>--}}
                                        <th>Description</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @if($transactions)
                                        <input type="hidden" name="ledger" id="ledger" value="{{ $ledger->id }}">
                                        <tr>
                                            <td align="left" colspan="5"><i><strong>Opening Balance</strong></i></td>
                                            <?php
                                            //$openingBalance = ($profitLossLedger) ? 0 : $openingBalance;
                                            ?>
                                            <td>
                                                <i><strong>{{ $currency."".formatNumber( abs( $openingBalance ) ) }}</strong></i>
                                            </td>
                                        </tr>
                                        @if($profitLossLedger)
                                            {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType ) !!}
                                        @endif
                                        @if($balanceSheetLedger)
                                            {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType ) !!}
                                        @endif
                                    @else
                                        <tr>
                                            <td colspan="7">
                                                <div class="alert alert-warning">
                                                    No transactions found for the selected account.
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            @else
                                <div class="row">
                                    <div class="alert alert-info">
                                        Please select a GL Account above and submit to view all transaction for the
                                        selected account.
                                    </div>
                                </div>
                            @endif
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#filter_ledger_stmt').validate();

            $(document).on('click', '.pdf', function (evt) {
                evt.preventDefault();

                if ($('#ledger_no').length) {
                    var url = $('#baseurl').val() + '/reports/ledger-statement/' + $('#ledger_no').val() + '/pdf?from_date=' + $('#from_date').val() + '&to_date=' + $('#to_date').val();

                    window.location.replace(url);
                }
            });

            $(document).on('click', '.xls', function (evt) {
                evt.preventDefault();

                if ($('#ledger_no').length) {
                    var url = $('#baseurl').val() + '/reports/ledger-statement/' + $('#ledger_no').val() + '/xls?from_date=' + $('#from_date').val() + '&to_date=' + $('#to_date').val();

                    window.location.replace(url);
                }
            });
        });
    </script>
@stop
