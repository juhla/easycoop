<!-- Modal -->
<div class="modal fade slide-up disable-scroll" id="acidTestModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5 id="groupName"> </h5>
                    <p class="p-b-10">Parameters for <strong>Acid Test Ratio</strong></p>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="">Ledger Account</th>
                            <th width="">Amount</th>
                        </tr>
                        </thead>
                        <tbody align="">
                            <tr>
                                <td >Total Current Asset</td>
                                <td id='totalCurrentAsset'></td>
                            </tr>
                            <tr>
                                <td >Total Current Liabilities</td>
                                <td id='totalCurrentLiabilities'></td>
                            <tr>
                            </tr>
                                <td >Total Inventory</td>
                                <td id='totalInventory'></td>
                            <tr>
                            </tr>
                                <td ><strong>Acid Test Ratio:</strong> (Asset-Inventory)/Liabilities</td>
                                <td id='acidTestRatio'></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- /.modal-dialog -->