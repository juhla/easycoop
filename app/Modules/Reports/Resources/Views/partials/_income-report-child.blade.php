
<tr>
    <td style="font-weight: 800">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $group->code.' - &nbsp;'.$group->name !!}</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
@if($nestedChild = $group->nestedGroups($group->id))
    {!! displayIncomeItem($nestedChild->get(), 'income') !!}
@endif
@if($group->ledgers->count())
    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{!! $ledger->code.' - &nbsp;'.$ledger->name !!}</a></td>
            
            <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">@if(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerM($ledger->id) < '0') ({{ ltrim(number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerM($ledger->id), 2, '.', ','),'-') }})
                @else {{ number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerM($ledger->id), 2, '.', ',') }} @endif</a></td>
             <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">%{{ Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerMperc($ledger->id) }}</a></td>

             <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">@if(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerY($ledger->id) < '0') ({{ ltrim(number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerY($ledger->id), 2, '.', ','),'-') }})
                @else {{ number_format(Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerY($ledger->id), 2, '.', ',') }} @endif</a></td>
             <td><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">%{{ Ikooba\Modules\Accounting\Modules\Reports\Apmx\Values::revenueLedgerYperc($ledger->id) }}</a></td>
        </tr>
    @endforeach
@endif
