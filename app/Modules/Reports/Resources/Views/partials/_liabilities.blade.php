<tbody>

<tr>
    <td colspan="4"><h4> {{ $liabilities->name }}</h4></td>
</tr>
@if($groups = $liabilities->groups)
    @foreach($groups as $group)
        <tr>
            <td style="font-weight: 800">{{ $group->name }}</td>
            <td></td>
            <td></td>
            <td>
                <?php  $result = $group->getBalance() ?>
                @if($result['dc'] === 'D')
                    {{ formatNumber($result['balance']) }}
                    <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>
                @elseif($result['dc'] === 'C')
                    {{ formatNumber($result['balance']) }}
                    <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>
                @else
                @endif
            </td>
        </tr>
        @if($nestedGroup = $group->nestedGroups($group->id))
            @foreach($nestedGroup->orderBy('code', 'asc')->get() as $child)
                <tr>
                    <td><span class="account-group-child">{{ $child->name }}</span></td>
                    <td></td>
                    <td></td>
                    <td>
                        <?php  $result = $child->getBalance() ?>
                        @if($result['dc'] === 'D')
                            {{ formatNumber($result['balance']) }}
                            <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>
                        @elseif($result['dc'] === 'C')

                            {{ formatNumber($result['balance']) }}
                            <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>
                        @else
                        @endif
                    </td>
                </tr>
                @if($subChild = $child->nestedGroups($child->id))
                    @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                        <tr>
                            <td><span class="account-group-child">{{ $subs->name }}</span></td>
                            <td></td>
                            <td></td>
                            <td>
                                <?php  $result = $subs->getBalance() ?>
                                @if($result['dc'] === 'D')
                                    {{ formatNumber($result['balance']) }}
                                    <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>

                                @elseif($result['dc'] === 'C')

                                    {{ formatNumber($result['balance']) }}
                                    <?php  $liabilitiesTotal = bcadd($result['balance'], $liabilitiesTotal, 2) ?>
                                @else
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
@endif
</tbody>
<thead>
<tr>
    <th>Total Liabilities</th>
    <th></th>
    <th></th>
    <th>{{ formatNumber($liabilitiesTotal) }}</th>
</tr>
</thead>