<div class="wrapper b-b header">Reports</div>
<ul class="nav">
    <li class="b-b b-light">
        <a href="{{ url('reports/trial-balance') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Trial Balance
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/statement-of-financial-position') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Stmt. of Financial Position
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/5-year-statement-of-financial-position') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>5 year Financial Summary
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/income-statement') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Income Statement
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/detailed-income-statement') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Detailed Income Statement
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/gl-account-summary') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>GL Account Summary
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/ledger-statement') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Ledger Statement
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/transaction-listing') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Transaction Listing
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/vendor-listing') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Vendor Listing
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/payables-accounts') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Payables
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/payables-age-analysis') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Aged Payables
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/customer-listing') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Customer Listing
        </a>
    </li>
    <li class="b-b b-light">
        <a href="{{ url('reports/receivables') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Receivables
        </a>
    </li>

    <li class="b-b b-light">
        <a href="{{ url('reports/receivables-age-analysis') }}">
            <i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Aged Receivables
        </a>
    </li>
</ul>