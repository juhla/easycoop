<div class="table-responsive">
    <table class="table table-hover table-condensed" id="inflowTable">
        <thead>
        <tr>
            <th>Account</th>
            {{--<th>Debit</th>--}}
            {{--<th>Credit</th>--}}
            {{--<th>Debit</th>--}}
            {{--<th>Credit</th>--}}
            <th>Debit</th>
            <th>Credit</th>
        </tr>
        </thead>
        <tbody>

        <?php  $debitBalance = 0 ?>
        <?php  $creditBalance = 0 ?>

        @if(!empty($end_date))
            <?php  $end_date = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d') ?>
            <?php  $start_date = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
        @else
            <?php  $end_date = ''   ?>
            <?php  $start_date = ''   ?>
        @endif
        <tr>
            <td colspan="3" style="text-transform: uppercase; background-color: #ccc;"><h5
                        style="font-weight: 800"> {{ $asset->name }}</h5></td>
        </tr>
        @if($groups = $asset->groups)
            @foreach($groups as $group)
                <tr>
                    <td colspan="3"
                        style="font-weight: 800;  font-size: 16px;">{{ $group->code .' '.$group->name }}</td>
                </tr>
                @if($nestedGroups = $group->nestedGroups($group->id))
                    @foreach($nestedGroups->orderBy('code', 'asc')->get() as $child)
                        <tr>
                            <td colspan=""
                                style="padding-left: 40px; font-weight: 600; font-size: 14px;">{{ $child->code.' '.$child->name }}</td>
                            @if($child->id === 12 || $child->id === 28)
                                <?php  $accountBalance = $child->getBalance($start_date, $end_date, null, true) ?>
                                @if($accountBalance['dc'] === 'D')
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                @endif
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        @if($subChild = $child->nestedGroups($child->id))
                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)

                                <tr>
                                    @if( $child->code == 1220 )
                                        <td style="padding-left: 70px; "><b>{{ $subs->code.' '.$subs->name }}</b></td>
                                    @else
                                        <td colspan="" style="padding-left: 70px; ">
                                            <b>{{ $subs->code.' '.$subs->name }}</b></td>
                                    @endif
                                    @if($subs->id === 12 || $subs->id === 28 || $child->code == 1220 )
                                        <?php  $accountBalance = $subs->getBalance($start_date, $end_date, null, true) ?>

                                        @if($accountBalance['dc'] === 'D')
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                            <td> -</td>
                                        @elseif($accountBalance['dc'] === 'C')
                                            <td> -</td>
                                            <td>{{$currency."".formatNumber($accountBalance['balance']) }}</td>
                                        @else
                                            <td></td>
                                            <td></td>
                                        @endif
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                                @if($subs->ledgers->count())
                                    @foreach($subs->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $asset->name) ?>

                                        @if($result['balance'] > 0)
                                            @if($result['dc'] === 'D')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td>{{ formatNumber($result['balance']) }}</td>
                                                        <td> -</td>
                                                    </tr>
                                                @endif
                                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                            @elseif($result['dc'] === 'C')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td> -</td>
                                                        <td> {{ formatNumber($result['balance']) }} </td>
                                                    </tr>
                                                @endif
                                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $asset->name) ?>

                                @if($result['balance'] > 0)
                                    @if($result['dc'] === 'D')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                <td> -</td>
                                            </tr>
                                        @endif
                                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                    @elseif($result['dc'] === 'C')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td> -</td>
                                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                            </tr>
                                        @endif
                                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $asset->name) ?>
                        @if($result['balance'] > 0)
                            @if($result['dc'] === 'D')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                        <td> -</td>
                                    </tr>
                                @endif
                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                            @elseif($result['dc'] === 'C')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td> -</td>
                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                    </tr>
                                @endif
                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($asset->ledgers->count())
            @foreach($asset->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $asset->name) ?>
                @if($result['balance'] > 0)
                    @if($result['dc'] === 'D')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                <td> -</td>
                            </tr>
                        @endif
                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                    @elseif($result['dc'] === 'C')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td> -</td>
                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                            </tr>
                        @endif
                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                    @endif
                @endif
            @endforeach
        @endif

        <tr>
            <td colspan="3" style="text-transform: uppercase; background-color: #ccc;"><h5
                        style="font-weight: 800"> {{ $liability->name }}</h5></td>
        </tr>
        @if($groups = $liability->groups)
            @foreach($groups as $group)
                <tr>
                    <td colspan="3"
                        style="font-weight: 800;  font-size: 16px;">{{ $group->code .' '.$group->name }}</td>
                </tr>
                @if($nestedGroups = $group->nestedGroups($group->id))
                    @foreach($nestedGroups->orderBy('code', 'asc')->get() as $child)
                        <tr>
                            <td style="padding-left: 40px; font-weight: 600; font-size: 14px;">{{ $child->code.' '.$child->name }}</td>
                            @if($child->id === 12 || $child->id === 28)
                                <?php  $accountBalance = $child->getBalance($start_date, $end_date, null, true) ?>
                                @if($accountBalance['dc'] === 'D')
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                @endif
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        @if($subChild = $child->nestedGroups($child->id))
                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                <tr>
                                    <td style="padding-left: 70px; "><b>{{ $subs->code.' '.$subs->name }}</b></td>
                                    @if($subs->id === 12 || $subs->id === 28)
                                        <?php  $accountBalance = $subs->getBalance($start_date, $end_date, null, true) ?>
                                        @if($accountBalance['dc'] === 'D')
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                            <td> -</td>
                                        @elseif($accountBalance['dc'] === 'C')
                                            <td> -</td>
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                        @endif
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                                @if($subs->ledgers->count())
                                    @foreach($subs->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $liability->name) ?>

                                        @if($result['balance'] > 0)
                                            @if($result['dc'] === 'D')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td>{{$currency."".formatNumber($result['balance']) }}</td>
                                                        <td> -</td>
                                                    </tr>
                                                @endif
                                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                            @elseif($result['dc'] === 'C')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td> -</td>
                                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                                    </tr>
                                                @endif
                                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $liability->name) ?>
                                @if($result['balance'] > 0)

                                    @if($result['dc'] === 'D')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                <td> -</td>
                                            </tr>
                                        @endif
                                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                    @elseif($result['dc'] === 'C')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td> -</td>
                                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                            </tr>
                                        @endif
                                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $liability->name) ?>
                        @if($result['balance'] > 0)
                            @if($result['dc'] === 'D')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                        <td> -</td>
                                    </tr>
                                @endif
                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                            @elseif($result['dc'] === 'C')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td> -</td>
                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                    </tr>
                                @endif
                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($liability->ledgers->count())
            @foreach($liability->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $liability->name) ?>
                @if($result['balance'] > 0)
                    @if($result['dc'] === 'D')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                <td> -</td>
                            </tr>
                        @endif
                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                    @elseif($result['dc'] === 'C')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td> -</td>
                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                            </tr>
                        @endif
                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                    @endif
                @endif
            @endforeach
        @endif

        <tr>
            <td colspan="3" style="text-transform: uppercase; background-color: #ccc;"><h5
                        style="font-weight: 800"> {{ $equity->name }}</h5></td>
        </tr>
        @if($groups = $equity->groups)
            @foreach($groups as $group)
                <tr>
                    <td colspan="3"
                        style="font-weight: 800;  font-size: 16px;">{{ $group->code .' '.$group->name }}</td>
                </tr>
                @if($nestedGroups = $group->nestedGroups($group->id))
                    @foreach($nestedGroups->orderBy('code', 'asc')->get() as $child)
                        <tr>
                            <td colspan=""
                                style="padding-left: 40px; font-weight: 600; font-size: 14px;">{{ $child->code.' '.$child->name }}</td>
                            @if($child->id === 12 || $child->id === 28)
                                <?php  $accountBalance = $child->getBalance($start_date, $end_date, null, true) ?>
                                @if($accountBalance['dc'] === 'D')
                                    <td>{{$currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                @endif
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        @if($subChild = $child->nestedGroups($child->id))
                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                <tr>
                                    <td colspan="" style="padding-left: 70px; "><b>{{ $subs->code.' '.$subs->name }}</b>
                                    </td>
                                    @if($subs->id === 12 || $subs->id === 28)
                                        <?php  $accountBalance = $subs->getBalance($start_date, $end_date, null, true) ?>
                                        @if($accountBalance['dc'] === 'D')
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                            <td> -</td>
                                        @elseif($accountBalance['dc'] === 'C')
                                            <td> -</td>
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                        @endif
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                                @if($subs->ledgers->count())
                                    @foreach($subs->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                                        @if($result['balance'] > 0)
                                            @if($result['dc'] === 'D')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                        <td> -</td>
                                                    </tr>
                                                @endif
                                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                            @elseif($result['dc'] === 'C')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td> -</td>
                                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                                    </tr>
                                                @endif
                                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                                @if($result['balance'] > 0)
                                    @if($result['dc'] === 'D')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                <td> -</td>
                                            </tr>
                                        @endif
                                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                    @elseif($result['dc'] === 'C')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td> -</td>
                                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                            </tr>
                                        @endif
                                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                        @if($result['balance'] > 0)
                            @if($result['dc'] === 'D')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                        <td> -</td>
                                    </tr>
                                @endif
                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                            @elseif($result['dc'] === 'C')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td> -</td>
                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                    </tr>
                                @endif
                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($equity->ledgers->count())
            @foreach($equity->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                <?php  $result = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                @if( $ledger->name == 'Retained Earnings' )
                    <?php  $one = $profitBeforeTax ?>
                    <?php  $two = $ledger->getBalance(true, $start_date, $end_date, null, true, $equity->name) ?>
                    <?php  $result = empty($one['balance']) ? $two : $one ?>
                    <?php  $result['balance'] = bcadd($one['balance'], $two['balance'], 2) ?>
                @endif


                @if($result['balance'] > 0)
                    @if($result['dc'] === 'D')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                <td> -</td>
                            </tr>
                        @endif
                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                    @elseif($result['dc'] === 'C')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td> -</td>
                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                            </tr>
                        @endif
                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                    @endif
                @endif
            @endforeach
        @endif

        <tr>
            <td colspan="3" style="text-transform: uppercase; background-color: #ccc;"><h5
                        style="font-weight: 800"> {{ $revenue->name }}</h5></td>
        </tr>
        @if($groups = $revenue->groups)
            @foreach($groups as $group)
                <tr>
                    <td colspan="3"
                        style="font-weight: 800;  font-size: 16px;">{{ $group->code .' '.$group->name }}</td>
                </tr>
                @if($nestedGroups = $group->nestedGroups($group->id))
                    @foreach($nestedGroups->orderBy('code', 'asc')->get() as $child)
                        <tr>
                            <td colspan=""
                                style="padding-left: 40px; font-weight: 600; font-size: 14px;">{{ $child->code.' '.$child->name }}</td>
                            @if($child->id === 12 || $child->id === 28)
                                @if($search)
                                    <?php  $accountBalance = $child->getBalance($start_date, $end_date) ?>
                                @else
                                    <?php  $accountBalance = $child->getBalance(null, null, date('Y')) ?>
                                @endif
                                @if($accountBalance['dc'] === 'D')
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td>{{$currency."". formatNumber($accountBalance['balance']) }}</td>
                                @endif
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        @if($subChild = $child->nestedGroups($child->id))
                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                <tr>
                                    <td colspan="" style="padding-left: 70px; "><b>{{ $subs->code.' '.$subs->name }}</b>
                                    </td>
                                    @if($subs->id === 12 || $subs->id === 28)
                                        @if($search)
                                            <?php  $accountBalance = $subs->getBalance($start_date, $end_date) ?>
                                        @else
                                            <?php  $accountBalance = $subs->getBalance(null, null, date('Y')) ?>
                                        @endif
                                        @if($accountBalance['dc'] === 'D')
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                            <td> -</td>
                                        @elseif($accountBalance['dc'] === 'C')
                                            <td> -</td>
                                            <td>{{$currency."".formatNumber($accountBalance['balance']) }}</td>
                                        @endif
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                                @if($subs->ledgers->count())
                                    @foreach($subs->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        @if($search)
                                            <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                                        @else
                                            <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                                        @endif
                                        @if($result['balance'] > 0)
                                            @if($result['dc'] === 'D')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td>{{ formatNumber($result['balance']) }}</td>
                                                        <td> -</td>
                                                    </tr>
                                                @endif
                                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                            @elseif($result['dc'] === 'C')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td> -</td>
                                                        <td> {{ formatNumber($result['balance']) }} </td>
                                                    </tr>
                                                @endif
                                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                @if($search)
                                    <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                                @else
                                    <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                                @endif
                                @if($result['balance'] > 0)
                                    @if($result['dc'] === 'D')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                <td> -</td>
                                            </tr>
                                        @endif
                                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                    @elseif($result['dc'] === 'C')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td> -</td>
                                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                            </tr>
                                        @endif
                                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        @if($search)
                            <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                        @else
                            <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                        @endif
                        @if($result['balance'] > 0)
                            @if($result['dc'] === 'D')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                        <td> -</td>
                                    </tr>
                                @endif
                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                            @elseif($result['dc'] === 'C')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td> -</td>
                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                    </tr>
                                @endif
                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($revenue->ledgers->count())
            @foreach($revenue->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                @if($search)
                    <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                @else
                    <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                @endif
                @if($result['balance'] > 0)
                    @if($result['dc'] === 'D')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                <td> -</td>
                            </tr>
                        @endif
                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                    @elseif($result['dc'] === 'C')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td> -</td>
                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                            </tr>
                        @endif
                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                    @endif
                @endif
            @endforeach
        @endif

        <tr>
            <td colspan="3" style="text-transform: uppercase; background-color: #ccc;"><h5
                        style="font-weight: 800"> {{ $cost->name }}</h5></td>
        </tr>
        @if($groups = $cost->groups)
            @foreach($groups as $group)
                <tr>
                    <td colspan="3"
                        style="font-weight: 800;  font-size: 16px;">{{ $group->code .' '.$group->name }}</td>
                </tr>
                @if($nestedGroups = $group->nestedGroups($group->id))
                    @foreach($nestedGroups->orderBy('code', 'asc')->get() as $child)
                        <tr>
                            <td colspan=""
                                style="padding-left: 40px; font-weight: 600; font-size: 14px;">{{ $child->code.' '.$child->name }}</td>
                            @if($child->id === 12 || $child->id === 28)
                                @if($search)
                                    <?php  $accountBalance = $child->getBalance($start_date, $end_date) ?>
                                @else
                                    <?php  $accountBalance = $child->getBalance(null, null, date('Y')) ?>
                                @endif
                                @if($accountBalance['dc'] === 'D')
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                    <td> -</td>
                                @elseif($accountBalance['dc'] === 'C')
                                    <td> -</td>
                                    <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                @endif
                            @else
                                <td></td>
                                <td></td>
                            @endif
                        </tr>
                        @if($subChild = $child->nestedGroups($child->id))
                            @foreach($subChild->orderBy('code', 'asc')->get() as $subs)
                                <tr>
                                    <td colspan="" style="padding-left: 70px; "><b>{{ $subs->code.' '.$subs->name }}</b>
                                    </td>
                                    @if($subs->id === 12 || $subs->id === 28)
                                        @if($search)
                                            <?php  $accountBalance = $subs->getBalance($start_date, $end_date) ?>
                                        @else
                                            <?php  $accountBalance = $subs->getBalance(null, null, date('Y')) ?>
                                        @endif
                                        @if($accountBalance['dc'] === 'D')
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                            <td> -</td>
                                        @elseif($accountBalance['dc'] === 'C')
                                            <td> -</td>
                                            <td>{{ $currency."".formatNumber($accountBalance['balance']) }}</td>
                                        @endif
                                    @else
                                        <td></td>
                                        <td></td>
                                    @endif
                                </tr>
                                @if($subs->ledgers->count())
                                    @foreach($subs->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        @if($search)
                                            <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                                        @else
                                            <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                                        @endif
                                        @if($result['balance'] > 0)
                                            @if($result['dc'] === 'D')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td>{{$currency."".formatNumber($result['balance']) }}</td>
                                                        <td> -</td>
                                                    </tr>
                                                @endif
                                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2) ?>
                                            @elseif($result['dc'] === 'C')
                                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                                    <tr>
                                                        <td style="padding-left: 100px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        {{--<td></td>--}}
                                                        <td> -</td>
                                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                                    </tr>
                                                @endif
                                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                @if($search)
                                    <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                                @else
                                    <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                                @endif
                                @if($result['balance'] > 0)
                                    @if($result['dc'] === 'D')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                                <td> -</td>
                                            </tr>
                                        @endif
                                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2);
                                        ?>
                                    @elseif($result['dc'] === 'C')
                                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                            <tr>
                                                <td style="padding-left: 60px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                {{--<td></td>--}}
                                                <td> -</td>
                                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                            </tr>
                                        @endif
                                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        @if($search)
                            <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                        @else
                            <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                        @endif
                        @if($result['balance'] > 0)
                            @if($result['dc'] === 'D')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                        <td> -</td>
                                    </tr>
                                @endif
                                <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2);
                                ?>
                            @elseif($result['dc'] === 'C')
                                @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                                    <tr>
                                        <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td></td>--}}
                                        <td> -</td>
                                        <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                                    </tr>
                                @endif
                                <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2) ?>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($cost->ledgers->count())
            @foreach($cost->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                @if($search)
                    <?php  $result = $ledger->getBalance(true, $start_date, $end_date) ?>
                @else
                    <?php  $result = $ledger->getBalance(true, null, null, date('Y')) ?>
                @endif
                @if($result['balance'] > 0)
                    @if($result['dc'] === 'D')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{ $currency."".formatNumber($result['balance']) }}</td>
                                <td> -</td>
                            </tr>
                        @endif
                        <?php  $debitBalance = bcadd($result['balance'], $debitBalance, 2);
                        ?>
                    @elseif($result['dc'] === 'C')
                        @if(($ledger->group_id !== 12) && ($ledger->group_id !== 28))
                            <tr>
                                <td style="padding-left: 40px;">{{ $ledger->code.' '.$ledger->name }}</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td> -</td>
                                <td> {{ $currency."".formatNumber($result['balance']) }} </td>
                            </tr>
                        @endif
                        <?php  $creditBalance = bcadd($result['balance'], $creditBalance, 2);

                        ?>
                    @endif
                @endif
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>
            <th><h4>&nbsp;&nbsp;&nbsp;&nbsp;Total</h4></th>
            {{--<th></th>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
            <th><h4>{{ $currency."".formatNumber($debitBalance) }}</h4></th>
            <th><h4>{{ $currency."".formatNumber($creditBalance) }}</h4></th>
        </tr>
        </tfoot>
    </table>
</div>