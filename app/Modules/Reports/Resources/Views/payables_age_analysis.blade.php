@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/aged-payables/export?type=pdf') }}"
                                   class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/aged-payables/export?type=xls') }}"
                                   class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <h3>Payables Age Analysis</h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" align="">
                            <div class="col-md-12 col-lg-12">
                                <form id="filter_ledger" action="{{ url('payables-age-analysis' ) }}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="form-group col-md-3">
                                        {{-- <div class="input-group date form_datetime" data-picker-position="bottom-left"  >
                                            <input type="text" class="form-control" required name="from_date" id="from_date" placeholder="From:" value="">
                                         <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i class="fa fa-times"></i></button>
                                             <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                         </span>
                                        </div> --}}
                                    </div>
                                    <div class="form-group col-md-3">
                                        {{-- <div class="input-group date form_datetime" data-picker-position="bottom-left">
                                            <input type="text" class="form-control" name="to_date" id="to_date" placeholder="To:" value="">
                                         <span class="input-group-btn">
                                             <button class="btn btn-default" type="button"><i class="fa fa-times"></i></button>
                                             <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                         </span>
                                        </div> --}}
                                    </div>
                                    <div class="btn-group col-md-3">
                                        {{-- <button type="submit" class="btn btn-complete"><i class="fa fa-search"></i> Submit</button> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th align="">Supplier</th>
                                    <th style="text-align:left">Total Amount.</th>
                                    <th style="text-align:left">Below 30 Days</th>
                                    <th style="text-align:left">30 - 60 Days</th>
                                    <th style="text-align:left">60 - 90 Days</th>
                                    <th style="text-align:left">90 - 180 Days</th>
                                    <th style="text-align:left">180 Days and Above</th>
                                </tr>
                                </thead>
                                <tbody align="center">
                                <?php
                                $_30_days = 0;
                                $_30_60_days = 0;
                                $_60_90_days = 0;
                                $_90_180_days = 0;
                                $_180_above_days = 0;

                                ?>

                                @if(!empty($rec))

                                    @foreach(@$rec as $ledger)
                                        @if($ledger['total'] != 0)
                                            <tr>
                                                <td>{{ $ledger['supplier'] }}</td>
                                                <td>{{ $ledger['total'] }}</td>
                                                <td class="text-primary">{{ $ledger['30_days'] }}</td>
                                                <td class="text-info">{{ $ledger['30_60_days'] }}</td>
                                                <td class="text-info">{{ $ledger['60_90_days'] }}</td>
                                                <td class="text-warning">{{ $ledger['90_180_days'] }}</td>
                                                <td class="text-danger">{{ $ledger['180_above_days'] }}</td>


                                                <?php
                                                $_30_days += $ledger['30_days'];
                                                $_30_60_days += $ledger['30_60_days'];
                                                $_60_90_days += $ledger['60_90_days'];
                                                $_90_180_days += $ledger['90_180_days'];
                                                $_180_above_days += $ledger['180_above_days'];
                                                ?>
                                            </tr>
                                        @endif
                                    @endforeach

                                    <tr>
                                        <td colspan="2">Total Amount.</td>
                                        <td class="text-primary">{{ @$_30_days }}</td>
                                        <td class="text-info">{{ @$_30_60_days }}</td>
                                        <td class="text-info">{{ @$_60_90_days }}</td>
                                        <td class="text-warning">{{ @$_90_180_days }}</td>
                                        <td class="text-danger">{{ @$_180_above_days }}</td>
                                    </tr>

                                @endif
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
            </section>
        </aside>
    </section>
@stop
