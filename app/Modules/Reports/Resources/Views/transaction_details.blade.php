@extends('app.layouts.app')

@section('content')
    {{-- <link href="{{ asset('app/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen"> --}}
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li><p>Accounting</p></li>
                        <li><a href="#" class="active">Transactions</a></li>
                        <li><a href="{{ url('reports/transaction-listing') }}">Listing</a></li>
                        <li><a href="javascript:;">ID: {{ $trans_id }}</a></li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        
        <div class="container-fluid container-fixed-lg m-t-20">
            <div class="row">
              <div class="col-sm-4 pull-right">                
                    <a href="{{ url('/reports/transaction-listing/'.$trans_id.'/details/pdf') }}" class="p-r-10">
                        <img width="25" height="25" alt="" class="icon-pdf" data-src-retina="{{ asset('app/img/pdf2x.png') }}" data-src="{{ asset('app/img/pdf2x.png') }}" src="{{ asset('app/img/pdf2x.png') }}"  title="Export to PDF">
                    </a>
                    <a href="{{ url('/reports/transaction-listing/'.$trans_id.'/details/xls') }}" class="p-r-10">
                        <img width="25" height="25" alt="" class="icon-image" data-src-retina="{{ asset('app/img/excel.png') }}" data-src="{{ asset('app/img/excel.png') }}" src="{{ asset('app/img/excel.png') }}" title="Export to excel">
                    </a>
              </div>
            </div>
        </div>
        <div class="container-fluid container-fixed-lg m-t-20">
            @include('flash::message')

            <div class="col-lg-10 bg-white">
                <div class="row">
                    <div class="panel panel-transparent">
                        <div class="panel-heading">
                            <div class="panel-title">Transaction Details </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row" align="">
                            
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover " id="inflowTable">
                                    <thead>
                                    <tr>
                                        <th width="15%">Date</th>
                                        {{-- <th width="15%">Inv./PCV/PV No.</th> --}}
                                        <th width="18%">Description</th>
                                        <th width="18%">Ledger Account</th>
                                        <th width="18%">Debit Account</th>
                                        <th width="18%">Credit Account</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($transaction)
										@forelse($transaction->item as $trans)
                                            <tr>
                                                <td>{{ date('m/d/Y', strtotime($transaction->transaction_date)) }}</td>
                                                <td></td>                                               
                                                <td>{{ getLedgerName($trans->ledger_id) }}</td>
                                                @if($trans->dc === 'D')
                                                    <td>{{ $trans->amount }}</td>
                                                    <td>0.00</td>
                                                @elseif($trans->dc === 'C')
                                                    <td>0.00</td>
                                                    <td>{{ $trans->amount }}</td>
                                                @endif
                                            </tr>
                                        @empty

                                        @endforelse
                                    @else
                                        <tr><td colspan=5>Nothing found.</td></tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <img src="{{ asset('app/img/advertise-bannar.jpg') }}" alt="Ads">
            </div>
        </div>
    </div>
@stop