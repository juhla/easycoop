@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                {{--<a href="#"  class="btn btn-sm btn-default" title="Export to PDF">PDF</a>--}}

                                <a href="{{ url('reports/transaction-listing/export?type=pdf&from_date='.$start_date.'&to_date='.$end_date) }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>

                                <a href="{{ url('reports/transaction-listing/export?type=xls&from_date='.$start_date.'&to_date='.$end_date) }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                        <form id="filter_ledger" action="{{ url('reports/transaction-listing') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" id="from_date" name="from_date" value="{{ $start_date }}" placeholder="From:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" name="to_date" id="to_date" value="{{ $end_date }}" placeholder="To:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="btn-group col-md-3">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Filter</button>
                                <a href="{{ url('reports/transaction-listing') }}" class="btn btn-default">Clear</a>
                            </div>
                        </form>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="">
                                <h4>
                                    Transaction Listing
                                </h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed m-b-none" id="inflowTable">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Ref #</th>
                                        <th>Description</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Balance</th>
                                    </tr>
                                    </thead>
                                    <?php  $range = [] ?>
                                    @if($start_date !== '')
                                    <?php  $range['start_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d') ?>
                                    @endif
                                    @if($end_date !== '')
                                        <?php  $range['end_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d') ?>
                                    @endif
                                    @foreach($classes as $class)
                                        <tr>
                                            <td colspan="6"><h4> {{ $class->name }} </h4></td>
                                        </tr>
                                        @if($groups = $class->groups)
                                            @foreach($groups as $group)
                                                <tr>
                                                    <td colspan="6" style="font-weight: 800">{{ $group->code.' - &nbsp;'.$group->name }}</td>
                                                </tr>
                                                @if($nestedChild = $group->nestedGroups($group->id))
                                                    {!! displayReportItem($nestedChild->get(), 'transaction-listing',0,0, $range) !!}
                                                @endif
                                                @if($group->ledgers->count())
                                                    @foreach($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                        <thead>
                                                        <tr>
                                                            <th colspan="6"><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a></th>
                                                        </tr>
                                                        </thead>


                                                        <?php  $transactions = $ledger->getTransactionsForLedger( $ledger->id, null, $range ) ?>
                                                        <?php
                                                        $op = $ledger->calculateOpeningBalance( null );
                                                        $openingBalance = $op[0];
                                                        $openingBalanceType = $op[1];
                                                        ?>
                                                        @if($transactions)
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="5"><strong>Opening Balance</strong></td>
                                                                <td><i><strong>{{ formatNumber( abs( $openingBalance ) ) }}</strong></i></td>
                                                            </tr>

                                                            {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType) !!}
                                                            </tbody>
                                                        @else

                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                        @if($class->ledgers->count())
                                            @foreach($class->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                                <thead>
                                                <tr>
                                                    <th colspan="6"><a href="{{ url('reports/ledger-statement/'.$ledger->id) }}">{{ $ledger->code.' - &nbsp;'.$ledger->name }}</a></th>
                                                </tr>
                                                </thead>

                                                <?php  $transactions = $ledger->getTransactionsForLedger( $ledger->id, null, $range ) ?>
                                                <?php
                                                $op = $ledger->calculateOpeningBalance( null );
                                                    //pr($transactions);
                                                $openingBalance = $op[0];
                                                $openingBalanceType = $op[1];
                                                ?>
                                                @if($transactions)
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="5"><strong>Opening Balance</strong></td>
                                                            <td><i><strong>{{ formatNumber( abs( $openingBalance ) ) }}</strong></i></td>
                                                        </tr>

                                                        {!! bsBuilder( $ledger, $transactions, $openingBalance, $openingBalanceType) !!}
                                                    </tbody>
                                                @else

                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </section>
                </section>
            </section>
        </aside>
    </section>
@stop

@section('scripts')
@stop