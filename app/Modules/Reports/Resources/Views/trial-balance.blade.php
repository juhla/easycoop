@extends('layouts.main')

@section('content')
    @include('flash::message')
    <section class="hbox stretch">
        <aside class="aside-md bg-white b-r" id="subNav">
            @include('reports::partials._menu')
        </aside>
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            <a href="#subNav" data-toggle="class:hide" class="btn btn-sm btn-default active">
                                <i class="fa fa-caret-right text fa-lg"></i>
                                <i class="fa fa-caret-left text-active fa-lg"></i>
                            </a>
                            <div class="btn-group">
                                <a href="{{ url('reports/trial-balance/export?type=pdf&from_date='. $start_date.'&to_date='.$end_date) }}" class="btn btn-sm btn-default" title="Export to PDF">PDF</a>
                                <a href="{{ url('reports/trial-balance/export?type=xls&from_date='. $start_date.'&to_date='.$end_date) }}" class="btn btn-sm btn-default" title="Export to Excel">EXCEL</a>
                                {{--<button type="button" class="btn btn-sm btn-default" title="Filter" data-toggle="dropdown"><i class="fa fa-filter"></i> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>--}}
                            </div>
                        </div>
                        <form id="filter_ledger" action="{{ url('reports/trial-balance') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" id="from_date" name="from_date" value="{{ $start_date }}" placeholder="From:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3 m-b-xs">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control datepicker" autocomplete="off" name="to_date" id="to_date" value="{{ $end_date }}" placeholder="To:">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-calendar"></i> </button>
                                    </span>
                                </div>
                            </div>
                            <div class="btn-group col-md-3">
                                <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Filter</button>
                                <a href="{{ url('reports/trial-balance') }}" class="btn btn-default">Clear</a>
                            </div>
                        </form>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    <section class="panel panel-default">
                        <div class="panel-heading">
                            <div class="">
                                <h4>
                                    {{getBusinessOwnerAuth()->company_name }} - Trial Balance as at {{ $report_date }}
                                </h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            {{--@include('reports::partials._trial-balance')--}}
                            @include('reports::partials._trial-balance_upgrade')
                        </div>
                    </section>
                </section>
                {{--<footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        <div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </footer>--}}
            </section>
        </aside>
    </section>
@stop
@section('scripts')
@stop
