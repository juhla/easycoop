<?php

Route::group(['prefix' => 'reports', 'middleware' => ['web','auth:web']], function() {
//    Route::get('/', function(){
//        $title = 'Reports';
//        return view('reports.index', compact('title'));
//    });
    //ledger statement
    Route::get('/', 'ReportsController@index');
    Route::any('/ledger-statement/', 'ReportsController@ledgerStatement');
    Route::get('/ledger-statement/{id}', 'ReportsController@ledgerStatement');
    // Trial Balance Routes
    Route::any('/trial-balance', 'ReportsController@trialBalance');
    //gl account summary
    Route::get('/gl-account-summary', 'ReportsController@glAccountSummary');
    //statement of financial position
    Route::any('/statement-of-financial-position', 'ReportsController@balanceSheet');
    Route::get('/5-year-statement-of-financial-position', 'ReportsController@balanceSheetExtended');

    Route::any('/detailed-income-statement', 'ReportsController@detailedIncomeStatement');
    Route::any('/income-statement', 'ReportsController@incomeStatement');
    Route::any('/receivables', 'ReportsController@receivable');
    Route::get('/receivables-age-analysis', 'ReportsController@agedReceivables');

    //Vendors Listing Route
    Route::any('/vendor-listing', 'ReportsController@vendorListing');
    Route::any('/customer-listing', 'ReportsController@customerListing');
    //Payables Age Analysis
    Route::any('/payables-accounts', 'ReportsController@payablesAccounts');
    Route::any('/payables-age-analysis', 'ReportsController@payablesAgeAnalysis');

    // Transaction Listing Routes
    Route::any('/transaction-listing', 'ReportsController@transactionListing');
    Route::get('/transaction-listing/{id}/details', 'ReportsController@transactionDetails');

    // Data export(Excel/PDF) routes
    Route::get('/trial-balance/export', 'ReportsController@trialBalance');
    Route::get('/statement-of-financial-position/export', 'ReportsController@balanceSheet');
    Route::get('/five-year-financial-summary/export', 'ReportsController@balanceSheetExtended');
    Route::get('/income-statement/export', 'ReportsController@incomeStatement');
    Route::get('/detailed-income-statement/export', 'ReportsController@detailedIncomeStatement');
    Route::get('/gl-account-summary/export', 'ReportsController@glAccountSummary');
    Route::get('/transaction-listing/export', 'ReportsController@transactionListing');
    Route::get('/ledger-statement', 'ReportsController@ledgerStatement');
    Route::get('/payables-accounts/export', 'ReportsController@payablesAccounts');
    Route::get('/aged-payables/export', 'ReportsController@payablesAgeAnalysis');
    Route::get('/receivables/export', 'ReportsController@receivable');
    Route::get('/aged-receivables/export', 'ReportsController@agedReceivables');
    Route::get('/vendor-listing/export', 'ReportsController@vendorListing');
    Route::get('/customer-listing/export', 'ReportsController@customerListing');

});