<?php

namespace App\Modules\Transaction\DataTables;

use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

class TransactionDataTable extends DataTable
{
    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        $dataTable
            ->addColumn('debit', function ($__res) {
                return getLedgerName('D', $__res->id);
            })
            ->filterColumn('debit', function ($query, $keyword) {
                $query->whereHas('item.ledger', function ($query) use ($keyword) {
                    $sql = "name  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->editColumn('approve_status', function ($__res) {
                if ($__res->approve_status == 0) {
                    $approve_status = 'Pending';
                } elseif ($__res->approve_status == 1) {
                    $approve_status = 'Approved';
                } else {
                    $approve_status = 'Rejected';
                }
                return $approve_status;
            })
            ->editColumn('amount', function ($__res) {
                return formatNumber($__res->amount);
            })
            ->addColumn('credit', function ($__res) {
                return getLedgerName('C', $__res->id);
            })
            ->filterColumn('credit', function ($query, $keyword) {
                $query->whereHas('item.ledger', function ($query) use ($keyword) {
                    $sql = "name  like ? ";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                });
            })
            ->editColumn('date', function ($__res) {
                return date('d/m/Y', strtotime($__res->transaction_date));
            })
            ->orderColumn('date', 'ca_transactions.transaction_date $1')
            ->addColumn('action', function ($transaction) {
                return view('transaction::datatables_actions', compact('transaction'))->render();
            })
            ->rawColumns(['action']);

        return $dataTable;

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaction $model)
    {

        $type = $this->getType();
        $start_date = session('start_date');
        $end_date = session('end_date');
        $approve_status = session('approve_status');

        $condition = array(
            'flag' => 'Active',
            'transaction_type' => $type,
        );

        $query = $model->with(['item.ledger'])->withoutGlobalScope('active')->where($condition)->newQuery();

        if (!empty($start_date)) {
            $start = Carbon::createFromFormat('d/m/Y', $start_date);
            $start = $start->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '>=', $start);
        }
        if (!empty($end_date)) {
            $end = Carbon::createFromFormat('d/m/Y', $end_date);
            $end = $end->format('Y-m-d');
            $query->where('ca_transactions.transaction_date', '<=', $end);
        }

        if (empty($start_date) && empty($end_date)) {

            $fiscalYear = FiscalYear::getCurrentFiscalYear();
            $query->whereBetween('ca_transactions.transaction_date', [$fiscalYear->begins, $fiscalYear->ends]);
        }

        if (!empty($approve_status)) {
            if ($approve_status == 'Pending') {
                $__status = '0';
            } else if ($approve_status == 'Approved') {
                $__status = '1';
            } else if ($approve_status == 'Rejected') {
                $__status = '-1';
            } else {
                $__status = '';
            }
            if ($__status != '') {
                $query->where('ca_transactions.approve_status', '=', $__status);
            }
        }

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('inflowDT')
            ->columns($this->getColumns())
            ->minifiedAjax('', null, request()->only(['start_date', 'end_date']))
            ->addAction(['width' => '80px'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['title' => 'Date', 'data' => 'date', 'footer' => 'date', 'searchable' => false],
            ['title' => 'Status', 'data' => 'approve_status', 'footer' => 'approve_status'],
            ['title' => 'Description', 'data' => 'description', 'footer' => 'description'],
            ['title' => 'Amount', 'data' => 'amount', 'footer' => 'amount'],
            ['title' => 'Debit', 'data' => 'debit', 'footer' => 'debit'],
            ['title' => 'Credit', 'data' => 'credit', 'footer' => 'credit'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Inflow_' . date('YmdHis');
    }
}
