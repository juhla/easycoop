<?php

namespace App\Modules\Transaction\Http\Controllers;


use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\TrialBalance;
use App\Modules\Transaction\DataTables\TransactionDataTable;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\Transaction\Models\TransactionItem;
use App\Modules\Transaction\Models\AccountGroup;

use App\Modules\Notebook\Repositories\CashReceiptRepository;
use App\Modules\Notebook\Repositories\CreditPurchaseRepository;
use App\Modules\Notebook\Repositories\CreditSalesRepository;
use App\Modules\Notebook\Repositories\PettyCashRepository;
use App\Modules\Notebook\Repositories\ChequePaymentRepository;
use App\Modules\Notebook\Repositories\ChequeReceiptRepository;
use App\Modules\Base\Traits\Acl;
use Illuminate\Support\Facades\Session;
use Auth;
use Zizaco\Entrust\EntrustFacade as Entrust;

class TransactionsController extends Controller
{
    use TrialBalance, Balance;

    public function __construct()
    {
        $this->middleware(['tenant']);
    }

    public function index()
    {
        return redirect()->back();
    }

    /**
     * @param TransactionDataTable $table
     * @return mixed
     */
    public function inflow(TransactionDataTable $table)
    {


        $inputs = request()->all();
        $this->checkIsBalanced();
        $this->hasAccess(['view_receipt', 'add_receipt']);
        $title = 'Inflow Transactions';
        $type = 'inflow';

        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }

        $start_date = session('start_date');
        $end_date = session('end_date');

        $table->setType($type);
        return $table->render('transaction::index', compact('type', 'title', 'start_date', 'end_date'));
    }


    public function outflow(TransactionDataTable $table)
    {

        $this->hasAccess(['view_payment', 'add_payment', 'edit_payment']);
        $this->checkIsBalanced();
        $title = 'Outflow Transactions';
        $type = 'outflow';
        $inputs = request()->all();
        if (request()->isMethod('post')) {
            Session::put('start_date', $inputs['start_date']);
            Session::put('end_date', $inputs['end_date']);
        }
        $start_date = session('start_date');
        $end_date = session('end_date');

        $table->setType($type);
        return $table->render('transaction::index', compact('type', 'title', 'start_date', 'end_date'));
    }

    /**
     * update or add transaction
     * @return mixed
     */
    public function save()
    {
        $this->hasAccess(['add_receipt']);
        $input = request()->all();

        $verifyDate = Transaction::verifyTransactionDate($input['trans_date']);

        if ($verifyDate === 'yes') {
            $result = new Transaction();
            $result->saveTransaction($input);
            if ($result->getisError()) {
                if (request()->ajax()) {
                    return response()->json(['result' => 'no', 'message' => $result->getErrorMessage()]);
                } else {
                    flash()->error($result->getErrorMessage());
                    return redirect()->back();
                }
            }
            if (request()->ajax()) {
                return response()->json($result);
            } else {
                flash()->success('Transaction has been added.');
                return redirect()->back();
            }
        } elseif ($verifyDate === 'no') {
            if (request()->ajax()) {
                return response()->json(['result' => 'no', 'message' => 'Transaction date must be between current Fiscal Year']);
            } else {
                flash()->error('Transaction date must be between current Fiscal Year');
                return redirect()->back();
            }
        } else {
            if (request()->ajax()) {
                return response()->json(['result' => 'no-f-year', 'message' => 'Please setup a Fiscal Year before entering transactions']);
            } else {
                flash()->success('Please setup a Fiscal Year before entering transactions');
                return redirect()->back();
            }
        }
    }

    /**
     * method to view a transaction detail
     * @param $item_id
     * @return mixed
     */
    public function view($item_id)
    {

        $this->hasAccess(['view_receipt']);
        $trans = Transaction::where('id', $item_id)->first();
        $res = TransactionItem::where('transaction_id', $item_id)->get();

        $table = '';
        foreach ($res as $res) {
            if ($res->dc === 'D') {
                $td = '<td>' . formatNumber($res->amount) . '</td>
                            <td>&nbsp;</td>';
            } else {
                $td = '<td>&nbsp;</td>
                            <td>' . formatNumber($res->amount) . '</td>';
            }
            $table .= '<tr><td>' . $res->ledger->code . ' ' . $res->ledger->name . '</td>' . $td . '</tr>';
        }
        //
        $return_arr['trans_date'] = $trans->trans_date;
        $return_arr['description'] = $trans->description;
        $return_arr['tableRow'] = $table;
        return response()->json($return_arr);

        //get transaction

    }

    /**
     * method to get credit purchases to be posted
     * @return $this
     */
    public function creditPurchase()
    {
        $input = request()->all();
        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Credit Purchase';
        $data['min_side'] = true;
        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-cp')->with($data);
    }

    public function postCP()
    {
        $input = request()->all();
        CreditPurchaseRepository::postCP($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/outflow');
        }
    }

    /**
     * method to get credit sales to be posted
     * @return $this
     */
    public function creditSales()
    {
        $input = request()->all();
        $data['min_side'] = true;

        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Credit Sales';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-cs')->with($data);
    }

    /**
     * method to handle post action for credit sales
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postCS()
    {
        $input = request()->all();
        CreditSalesRepository::postCS($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/inflow');
        }
    }

    /**
     * method to get credit purchases to be posted
     * @return $this
     */
    public function pettyCash()
    {
        $input = request()->all();
        $data['min_side'] = true;
        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Cash Payment';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-pc')->with($data);
    }

    public function postPC()
    {
        $input = request()->all();
        PettyCashRepository::postPC($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/outflow');
        }
    }

    /**
     * method to get credit purchases to be posted
     * @return $this
     */
    public function chequePayment()
    {
        $input = request()->all();
        $data['min_side'] = true;
        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Cheque Payment';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-chqp')->with($data);
    }

    /**
     * method to post Cheque payment
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postChqP()
    {
        $input = request()->all();
        ChequePaymentRepository::postChqP($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/outflow');
        }
    }

    /**
     * method to get cheque receipt to be posted
     * @return $this
     */
    public function chequeReceipt()
    {
        $input = request()->all();
        $data['min_side'] = true;

        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Cheque Receipt';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();
        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-chqr')->with($data);
    }

    /**
     * method to post Cheque Receipt
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postChqR()
    {
        $input = request()->all();
        ChequeReceiptRepository::postChqR($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/inflow');
        }
    }

    /**
     * method to get cash receipt to be posted
     * @return $this
     */
    public function cashReceipt()
    {
        $input = request()->all();
        $data['min_side'] = true;

        if (!isset($input['ids'])) {
            flash()->error('Please select at least one item to post');
            return redirect()->back();
        }
        $data['title'] = 'Post Cash Receipt';

        $data['accounts'] = AccountGroup::with(['groups' => function ($q) {
            $q->orderBy('code', 'asc');
        }])->whereNull('parent_id')->orWhere('parent_id', null)->get();

        $data['ids'] = $input['ids'];

        return view('transaction::registers.post-cr')->with($data);
    }

    /**
     * method to post Cash Receipt
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postCR()
    {
        $input = request()->all();
        CashReceiptRepository::postCR($input);
        if (request()->ajax()) {
            return response()->json(request()->segment(3));
        } else {
            flash()->success('Transactions has been added.');
            return redirect()->to('transactions/inflow');
        }
    }


    /**
     * method to get transaction to be edited
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransaction($id)
    {
        $res = Transaction::with(['item'])->find($id);
        $response = $res;
        if ($res->item) {
            foreach ($res->item as $item) {
                if ($item->dc === 'D') {
                    $response['debit_account'] = $item->ledger_id;
                } else {
                    $response['credit_account'] = $item->ledger_id;
                }
            }
        }
        $response['transaction_date'] = date('d/m/Y', strtotime($res->transaction_date));
        return response()->json($response);
    }

    /**
     * method to delete transaction
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            $this->hasAccess(['delete_payment']);
            TransactionItem::where('transaction_id', $id)->delete();
            Transaction::find($id)->delete();
            return response()->json(['status' => 200]);
        } catch (\Exception $exception) {
            app('sentry')->captureException($exception);
        }
    }
}
