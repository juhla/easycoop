<?php

namespace App\Modules\Transaction\Models;

use App\Modules\Base\Events\AccountGroupCreated;
use App\Modules\Base\Models\FiscalYear;
use App\Modules\Base\Traits\Core\Balance;
use App\Modules\Base\Traits\Core\Tree;
use App\Modules\Base\Traits\MySoftDeletes;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use App\Modules\Base\Models\TransactionItem;
use Kalnoy\Nestedset\NodeTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

use Auth;
use Illuminate\Database\Eloquent\Builder;

class AccountGroup extends Model implements AuditableContract
{
    use Auditable, NodeTrait, NullableFields, Balance, Tree, MySoftDeletes;
    //set connection
    protected $connection = 'tenant_conn';
    //set db table
    protected $table = 'ca_groups';
    //mass assignment fields
    protected $fillable = ['id', 'name', 'code', 'parent_id', 'is_system_account'];
    protected $nullable = ['parent_id'];

    protected $attributes = [
        'flag' => 'Active',
        'is_system_account' => '0',
        '_rgt' => '0',
        '_lft' => '0',
    ];


    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('ca_groups.flag', 'Active');
        });

    }

    protected $dispatchesEvents = [
        'created' => AccountGroupCreated::class,
    ];

    public function getTableName()
    {
        return $this->table;
    }

    /**
     * establish relationship between parent and child group
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function groups()
    {
        return $this->hasMany('App\Modules\Base\Models\AccountGroup', 'parent_id')->where('flag', 'Active');
    }

    /**
     * establish relationship between group children
     * @param $parent_id
     * @return mixed
     */
    public function nestedGroups($parent_id)
    {
        return $this->hasMany('App\Modules\Base\Models\AccountGroup', 'parent_id')->where('parent_id', $parent_id)->where('flag', 'Active');
    }

    /**
     * establish parent child relationship between groups
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Modules\Base\Models\AccountGroup', 'parent_id')->where('flag', 'Active');
    }

    /**
     * establish relationship between groups and leger accunt
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ledgers()
    {
        return $this->hasMany('App\Modules\Base\Models\Ledger', 'group_id')->where('flag', 'Active');
    }


    public function getDistinctGroups()
    {
        $groups = AccountGroup::distinct()->select('ca_groups.name')
            ->join('ca_ledgers', 'ca_groups.id', '=', 'ca_ledgers.group_id')
            ->orderBy('ca_groups.parent_id', 'asc')
            ->get();

        return $groups;
    }

    /**
     * get debit balance
     * @param $start_date
     * @param $end_date
     * @param $year
     * @param $accumulate
     * @return int|string
     */
    public function getDebitBalance($start_date = null, $end_date = null, $year = null, $accumulate = false)
    {
        $dc = '';
        $balance = 0;
        if (is_null($end_date)) {
            $end_date = date('Y-m-d');
        }
        if ($this->ledgers->count()) {
            foreach ($this->ledgers()->get() as $ledger) {
                if (!is_null($start_date)) {

                    //get debit total
                    $total = $ledger->transactionItems()
                        ->where('dc', 'D')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where(function ($q) use ($accumulate, $start_date, $end_date) {
                            if ($accumulate) {
                                $q->where('ca_transactions.transaction_date', '<=', $end_date);
                            } else {
                                $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                            }
                        })
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                } elseif (!is_null($year)) {
                    //get debit total
                    $total = $ledger->transactionItems()
                        ->where('dc', 'D')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where(function ($q) use ($accumulate, $year) {
                            if ($accumulate) {
                                $q->where('ca_transactions.transaction_date', '<=', $year . '-12-31');
                            } else {
                                $q->whereBetween('ca_transactions.transaction_date', [$year . '-01-01', $year . '-12-31']);
                            }
                        })
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                } else {
                    $total = $ledger->transactionItems()->where('dc', 'D')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where('ca_transactions.transaction_date', '<=', $end_date)
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                }
            }
        }
        return $balance;
    }

    /**
     * get credit balance
     * @param $start_date
     * @param $end_date
     * @param $year
     * @param $accumulate
     * @return int|string
     */
    public function getCreditBalance($start_date = null, $end_date = null, $year = null, $accumulate = false)
    {
        $dc = '';
        $balance = 0;

        if (is_null($end_date)) {
            $end_date = date('Y-m-d');
        }

        if ($this->ledgers->count()) {
            foreach ($this->ledgers()->get() as $ledger) {
                if (!is_null($start_date)) {

                    //get debit total
                    $total = $ledger->transactionItems()
                        ->where('dc', 'C')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where(function ($q) use ($accumulate, $start_date, $end_date) {
                            if ($accumulate) {
                                $q->where('ca_transactions.transaction_date', '<=', $end_date);
                            } else {
                                $q->whereBetween('ca_transactions.transaction_date', [$start_date, $end_date]);
                            }
                        })
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                } elseif (!is_null($year)) {
                    //get debit total
                    $total = $ledger->transactionItems()
                        ->where('dc', 'C')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where(function ($q) use ($accumulate, $year) {
                            if ($accumulate) {
                                $q->where('ca_transactions.transaction_date', '<=', $year . '-12-31');
                            } else {
                                $q->whereBetween('ca_transactions.transaction_date', [$year . '-01-01', $year . '-12-31']);
                            }
                        })
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                } else {
                    $total = $ledger->transactionItems()->where('dc', 'C')
                        ->join('ca_transactions', 'ca_transaction_items.transaction_id', '=', 'ca_transactions.id')
                        ->where('ca_transactions.transaction_date', '<=', $end_date)
                        ->sum('ca_transaction_items.amount');
                    $balance = bcadd($balance, $total, 2);
                }
            }
        }
        return $balance;
    }

    /**
     * get total balance
     * @return array
     */
    public function getBalance($start_date = null, $end_date = null, $year = null, $accumulate = null)
    {
        $dr_total = $this->getDebitBalance($start_date, $end_date, $year, $accumulate);
        $cr_total = $this->getCreditBalance($start_date, $end_date, $year, $accumulate);
        //pr($dr_total);pr($dr_total);

        $cl = 0;
        if ($dr_total > $cr_total) {
            $cl = bcsub($dr_total, $cr_total, 2);
            return ['dc' => 'D', 'balance' => $cl];
        } else if ($cr_total === $dr_total) {
            return ['dc' => '', 'balance' => 0];
        } else {
            $cl = bcsub($cr_total, $dr_total, 2);
            return ['dc' => 'C', 'balance' => $cl];

        }
    }


    /**
     * get total balance
     * @return array
     */
    public function getBalancePlus($start_date = null, $end_date = null, $year = null, $accumulate = null, $accountType = null)
    {

        if (empty($accountType)) {
            return $this->getBalance($start_date, $end_date, $year, $accumulate);
        }
        $dr_total = $this->getDebitBalance($start_date, $end_date, $year, $accumulate);
        $cr_total = $this->getCreditBalance($start_date, $end_date, $year, $accumulate);
        $cl = 0;
        if ($cr_total === $dr_total) {
            return ['dc' => '', 'balance' => 0, 'debit' => $dr_total, 'credit' => $cr_total];
        } elseif ($accountType === "D") {
            $cl = bcsub($dr_total, $cr_total, 2);
            return ['dc' => 'D', 'balance' => $cl];
        } else {
            $cl = bcsub($cr_total, $dr_total, 2);
            return ['dc' => 'C', 'balance' => $cl];
        }


    }


    /**  This function is approved to move */
    public function getDeepBalance($start_date = null, $end_date = null)
    {
        $account = $this;
        $total = 0;
        if ($account->groups) {
            foreach ($account->groups as $group) {
                $result = $group->getBalance($start_date, $end_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);

                if ($group->nestedGroups($group->id)) {
                    foreach ($group->nestedGroups($group->id)->get() as $child) {
                        $result = $child->getBalance($start_date, $end_date);
                        formatNumber($result['balance']);
                        $total = bcadd($result['balance'], $total, 2);
                        if ($child->nestedGroups($child->id)) {
                            foreach ($child->nestedGroups($group->id)->get() as $sub) {
                                $result = $sub->getBalance($start_date, $end_date);
                                formatNumber($result['balance']);
                                $total = bcadd($result['balance'], $total, 2);
                            }
                        }
                    }
                }
            }
        }
        if ($account->ledgers->count()) {
            foreach ($account->ledgers()->get() as $ledger) {
                $result = $ledger->getBalance($start_date, $end_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);
            }
        }
        return $total;
    }

    public static function getAccountBalance($account, $start_date = null)
    {
        $total = 0;
        if ($account->groups) {
            foreach ($account->groups as $group) {
                $result = $group->getBalance($start_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);

                if ($group->nestedGroups($group->id)) {
                    foreach ($group->nestedGroups($group->id)->get() as $child) {
                        $result = $child->getBalance($start_date);
                        formatNumber($result['balance']);
                        $total = bcadd($result['balance'], $total, 2);
                        if ($child->nestedGroups($child->id)) {
                            foreach ($child->nestedGroups($group->id)->get() as $sub) {
                                $result = $sub->getBalance($start_date);
                                formatNumber($result['balance']);
                                $total = bcadd($result['balance'], $total, 2);
                            }
                        }
                    }
                }
            }
        }
        if ($account->ledgers->count()) {
            foreach ($account->ledgers()->get() as $ledger) {
                $result = $ledger->getBalance(true, $start_date);
                formatNumber($result['balance']);
                $total = bcadd($result['balance'], $total, 2);
            }
        }

        return $total;
    }


    /**
     * calculate total accumulated depreciation
     * @return int|string
     */
    public static function calculateAccumulatedDepreciation($start_date = null, $end_date = null, $year = null)
    {
        $res = AccountGroup::find(20);

        //find property, plant & equipment group
        $ppe = $res->groups()->find(21);
        //get balance for property, plant & equipment
        if (!is_null($start_date)) {
            $start_date = date('Y-m-d', strtotime($start_date));

            $end_date = date('Y-m-d', strtotime($end_date));

            $ppeResult = $ppe->getBalance($start_date, $end_date, $year, true);
        } else {
            $ppeResult = $ppe->getBalance(null, null, $year, true);
        }
        //total
        $ppeTotal = $ppeResult['balance'];
        //find investment property group
        $ip = $res->groups()->find(22);
        //get balance for investment property
        if (!is_null($start_date)) {
            $start_date = date('Y-m-d', strtotime($start_date));

            $end_date = date('Y-m-d', strtotime($end_date));
            $ipResult = $ip->getBalance($start_date, $end_date, $year, true);
        } else {
            $ipResult = $ip->getBalance(null, null, $year, true);
        }
        //total
        $ipTotal = $ipResult['balance'];

        return ['ppeTotal' => $ppeTotal, 'ipTotal' => $ipTotal];
    }
}