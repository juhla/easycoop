<?php

use App\Modules\Base\Traits\Acl;

?>
<div class="btn-group btn-group-sm">
    <button type="button" class="btn btn-complete dropdown-toggle"
            data-toggle="dropdown"><span class="caret"></span></button>
    <ul class="dropdown-menu"ser
        style="right: 0; left: auto; overflow: visible !important">
        @if(Acl::can('view_receipt'))
            <li><a href="#" class="view" data-value="{{$transaction->id}}">View Transaction</a></li>
        @endif
        @if(Acl::can('edit_receipt'))
            <li><a href="#" class="edit"
                   data-value="{{$transaction->id}}">Edit
                    Transaction</a></li>
        @endif

        @if($transaction->approve_status == '-1') 
                    <li><a href="#" class="viewReason" data-value="{{$transaction->id}}">View Rejection Reason</a></li>
                @endif
        {{--<li><a href="#">Split Transaction</a></li>--}}
        {{--<li><a href="#">Create Invoice payment</a></li>--}}
        {{--<li><a href="#">Move Entry</a></li>--}}
        @if(Acl::can(['delete_receipt']))
            <li class="divider"></li>
            <li><a href="javascript:;" class="delete-transaction"
                   data-value="{{ $transaction->id }}">Delete</a></li>
        @endif
    </ul>
</div>