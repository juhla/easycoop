<?php

use App\Modules\Base\Traits\Acl;

?>

@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            @if(Acl::can(['add_receipt']) && Request::is('transactions/inflow'))
                                <button type="button" class="btn btn-success add-new" data-type="{{ $type }}">
                                    Add {{ ucfirst($type) }}</button>
                            @endif
                            @if(Acl::can(['add_payment']) && Request::is('transactions/outflow'))
                                <button type="button" class="btn btn-success add-new" data-type="{{ $type }}">
                                    Add {{ ucfirst($type) }}</button>
                            @endif
                        </div>
                        {{-- inflow form --}}
                        @if(Acl::can(['view_receipt'])  && Request::is('transactions/inflow'))
                            @include('transaction::actions.search')
                        @endif
                        {{-- /inflow form --}}

                        @if(Acl::can(['view_payment'])  && Request::is('transactions/outflow'))
                            @include('transaction::actions.search')
                        @endif

                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @if($type === 'inflow')
                        @include('transaction::inflow-form')
                    @else
                        @include('transaction::outflow-form')
                    @endif

                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            @section('css')
                                @include('layouts.datatables_css')
                            @endsection
                            {!! $dataTable->table(['width' => '100%']) !!}
                            @section('scripts_dt')
                                @include('layouts.datatables_js')
                                {!! $dataTable->scripts() !!}
                            @endsection
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {{--<div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>--}}
                    
                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('transaction::view')
@stop
@section('scripts')

    <script>
        $('#clearBTN').click(function () {
            $('#start_date').val('');
            $('#end_date').val('');
        });
    </script>
    <script src="{{ asset('js/scripts/transactions.js') }}" type="text/javascript"></script>


@stop
