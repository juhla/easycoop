<?php

use App\Modules\Base\Traits\Acl;

?>

@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">
                            @if(Acl::can(['add_receipt']) && Request::is('transactions/inflow'))
                                <button type="button" class="btn btn-success add-new" data-type="{{ $type }}">
                                    Add {{ ucfirst($type) }}</button>
                            @endif
                            @if(Acl::can(['add_payment']) && Request::is('transactions/outflow'))
                                <button type="button" class="btn btn-success add-new" data-type="{{ $type }}">
                                    Add {{ ucfirst($type) }}</button>
                            @endif
                        </div>
                        {{-- inflow form --}}
                        @if(Acl::can(['view_receipt'])  && Request::is('transactions/inflow'))
                            <form id="filter_ledger" action="{{ url('transactions/'.$type) }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="col-sm-3 m-b-xs">
                                    <div class="input-group">
                                        <input type="text" class="input-sm form-control datepicker" id="start_date"
                                               name="start_date" autocomplete="off" value="{{ $start_date }}" placeholder="From:">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button"><i
                                                        class="fa fa-calendar"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3 m-b-xs">
                                    <div class="input-group">
                                        <input type="text" class="input-sm form-control datepicker" name="end_date"
                                               id="end_date" autocomplete="off" value="{{ $end_date }}" placeholder="To:">
                                        <span class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button"><i
                                                        class="fa fa-calendar"></i> </button>
                                        </span>
                                    </div>
                                </div>
                                <div class="btn-group col-md-3">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Submit</button>
                                    <a href="{{ url('transactions') }}" class="btn btn-default">Clear</a>
                                </div>
                            </form>  
                        @endif
                        {{-- /inflow form --}}

                        {{-- outflow form --}}
                                
                            @if(Acl::can(['view_payment'])  && Request::is('transactions/outflow'))
                                <form id="filter_ledger" action="{{ url('transactions/'.$type) }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <div class="col-sm-3 m-b-xs">
                                        <div class="input-group">
                                            <input type="text" class="input-sm form-control datepicker" id="start_date"
                                                   name="start_date" value="{{ $start_date }}" placeholder="From:">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm btn-default" type="button"><i
                                                            class="fa fa-calendar"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 m-b-xs">
                                        <div class="input-group">
                                            <input type="text" class="input-sm form-control datepicker" name="end_date"
                                                   id="end_date" value="{{ $end_date }}" placeholder="To:">
                                            <span class="input-group-btn">
                                                <button class="btn btn-sm btn-default" type="button"><i
                                                            class="fa fa-calendar"></i> </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="btn-group col-md-3">
                                        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> Submit</button>
                                        <a href="{{ url('transactions') }}" class="btn btn-default">Clear</a>
                                    </div>
                                </form>  
                            @endif

                        {{-- /outflow form --}}
                        
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @if($type === 'inflow')
                        @include('transactions.inflow-form')
                    @else
                        @include('transactions.outflow-form')
                    @endif

                    @include('flash::message')
                    <section class="panel panel-default">
                        <div class="table-responsive">
                            {{-- showing inflow table --}}
                            @if(Request::is('transactions/inflow'))
                                        <table class="table table-striped m-b-none">
                                            <thead>
                                            <tr>
                                                <th width="5%"><input type="checkbox" id="checkAll"/></th>
                                                <th width="13%">Date</th>
                                                <th width="18%">Description</th>
                                                <th width="13%">Amount</th>
                                                <th width="20%">Debit Account</th>
                                                <th width="20%">Credit Account</th>
                                                <th width="10%"></th>
                                            </tr>
                                            </thead>
                                            <tbody id="transactionTable">
                                            @if(count($transactions) > 0)
                                                @foreach($transactions as $transaction)
                                                    <tr class="edit-row" data-value="{{$transaction->id}}" valign="left">
                                                        <td><input type="checkbox" name="ids" value="{{$transaction->id}}"/></td>
                                                        <td>{{ date('d/m/Y', strtotime($transaction->transaction_date))}}</td>
                                                        <td>{{ $transaction->description}}</td>
                                                        <td>
                                                            <span class="{{ ($transaction->transaction_type === 'inflow') ? 'text-success' : 'text-danger' }}">{{ formatNumber($transaction->amount)}}</span>
                                                        </td>
                                                        <td>{{ getLedgerName('D', $transaction->id) }}</td>
                                                        <td>{{ getLedgerName('C', $transaction->id) }}</td>
                                                        <td>
                                                            <div class="btn-group btn-group-sm">
                                                                <button type="button" class="btn btn-complete dropdown-toggle"
                                                                        data-toggle="dropdown"><span class="caret"></span></button>
                                                                <ul class="dropdown-menu"ser
                                                                    style="right: 0; left: auto; overflow: visible !important">
                                                                    @if(Acl::can('view_receipt'))
                                                                    <li><a href="#" class="view" data-value="{{$transaction->id}}">View Transaction</a></li>
                                                                    @endif
                                                                    @if(Acl::can('edit_receipt'))
                                                                        <li><a href="#" class="edit"
                                                                               data-value="{{$transaction->id}}">Edit
                                                                                Transaction</a></li>
                                                                    @endif
                                                                    {{--<li><a href="#">Split Transaction</a></li>--}}
                                                                    {{--<li><a href="#">Create Invoice payment</a></li>--}}
                                                                    {{--<li><a href="#">Move Entry</a></li>--}}
                                                                    @if(Acl::can(['delete_receipt']))
                                                                        <li class="divider"></li>
                                                                        <li><a href="javascript:;" class="delete-transaction"
                                                                               data-value="{{ $transaction->id }}">Delete</a></li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr class="no-income">
                                                    <td colspan="8">No record found.</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                            @endif
                            {{-- /inflow table --}}

                            {{-- outflow table --}}

                                  @if(Request::is('transactions/outflow'))
                                              <table class="table table-striped m-b-none">
                                                  <thead>
                                                  <tr>
                                                      <th width="5%"><input type="checkbox" id="checkAll"/></th>
                                                      <th width="13%">Date</th>
                                                      <th width="18%">Description</th>
                                                      <th width="13%">Amount</th>
                                                      <th width="20%">Debit Account</th>
                                                      <th width="20%">Credit Account</th>
                                                      <th width="10%"></th>
                                                  </tr>
                                                  </thead>
                                                  <tbody id="transactionTable">
                                                  @if(count($transactions) > 0)
                                                      @foreach($transactions as $transaction)
                                                          <tr class="edit-row" data-value="{{$transaction->id}}" valign="left">
                                                              <td><input type="checkbox" name="ids" value="{{$transaction->id}}"/></td>
                                                              <td>{{ date('d/m/Y', strtotime($transaction->transaction_date))}}</td>
                                                              <td>{{ $transaction->description}}</td>
                                                              <td>
                                                                  <span class="{{ ($transaction->transaction_type === 'inflow') ? 'text-success' : 'text-danger' }}">{{ formatNumber($transaction->amount)}}</span>
                                                              </td>
                                                              <td>{{ getLedgerName('D', $transaction->id) }}</td>
                                                              <td>{{ getLedgerName('C', $transaction->id) }}</td>
                                                              <td>
                                                                  <div class="btn-group btn-group-sm">
                                                                      <button type="button" class="btn btn-complete dropdown-toggle"
                                                                              data-toggle="dropdown"><span class="caret"></span></button>
                                                                      <ul class="dropdown-menu"
                                                                          style="right: 0; left: auto; overflow: visible !important">
                                                                          @if(Acl::can('view_payment'))
                                                                          <li><a href="#" class="view" data-value="{{$transaction->id}}">View
                                                                                  Transaction</a></li>@endif
                                                                          @if(Acl::can('edit_payment'))
                                                                              <li><a href="#" class="edit"
                                                                                     data-value="{{$transaction->id}}">Edit
                                                                                      Transaction</a></li>
                                                                          @endif
                                                                          {{--<li><a href="#">Split Transaction</a></li>--}}
                                                                          {{--<li><a href="#">Create Invoice payment</a></li>--}}
                                                                          {{--<li><a href="#">Move Entry</a></li>--}}
                                                                          @if(Acl::can('delete_payment'))
                                                                              <li class="divider"></li>
                                                                              <li><a href="javascript:;" class="delete-transaction"
                                                                                     data-value="{{ $transaction->id }}">Delete</a></li>
                                                                          @endif
                                                                      </ul>
                                                                  </div>
                                                              </td>
                                                          </tr>
                                                      @endforeach
                                                  @else
                                                      <tr class="no-income">
                                                          <td colspan="8">No record found.</td>
                                                      </tr>
                                                  @endif
                                                  </tbody>
                                              </table>
                                  @endif  

                            {{-- /outflow table --}}
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    <div class="row text-center-xs">
                        {{--<div class="col-md-6 hidden-sm">
                            <p class="text-muted m-t">Showing 20-30 of 50</p>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right text-center-xs">
                            <ul class="pagination pagination-sm m-t-sm m-b-none">
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>--}}
                        {!! $transactions->render() !!}
                    </div>
                </footer>
            </section>
        </aside>
    </section>
    @include('transactions.view')
@stop
@section('scripts')
    <script src="{{ asset('js/scripts/transactions.js') }}" type="text/javascript"></script>
@stop
