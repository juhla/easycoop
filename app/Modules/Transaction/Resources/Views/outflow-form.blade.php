<?php
use App\Modules\Base\Traits\Acl;
?>
<div class="row">
    <div class="col-sm-12" id="displayForm" style="display: none">
        <section class="panel panel-info portlet-item">
            <header class="panel-heading"> Enter New Transaction </header>
            @if(Acl::can('add_payment'))

                <form role="form" action="{{ url('transactions/save') }}" method="post" id="transactionForm" data-validate="parsley">
                    <section class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>PV No.</label>
                                    <input type="text" class="form-control" name="pv_receipt_no" id="pv_receipt_no" value="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Date <span class="required">*</span> </label>
                                    <input type="text" class="form-control datepicker" autocomplete="off" name="trans_date" id="trans_date" placeholder="dd/mm/yyyy" data-required>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group form-group-default form-group-default-select2">
                                    <label>Type (means of payment/receipt)</label>
                                    <select name="payment_type" id="payment_type" class="select2-option" style="width: 100%" required>
                                        <?php $payment_mode = config('constants.payment_mode'); ?>
                                        @foreach($payment_mode as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-group-default">
                                    <label>Customer/Vendor Name</label>
                                    <input type="text" class="form-control" name="customer_vendor" id="customer_vendor">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="showBank" style="display: none">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Bank</label>
                                    <input type="text" class="form-control" name="bank" id="bnak">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Cheque Number</label>
                                    <input type="text" class="form-control" name="cheque_number" id="cheque_number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Description <span class="required">*</span> </label>
                                    <input type="text" class="form-control" name="description" id="description" data-required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Amount <span class="required">*</span> </label>
                                    <input type="text" class="form-control" name="amount" id="amount" data-required data-type="number" onkeypress="return numbersonly(event)">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="">Debit Account <span class="required">*</span></label>
                                    <select class="select2-option gl_accounts" name="debit_account" id="debit_account" style="width: 100%" data-required>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="">Credit Account <span class="required">*</span></label>
                                    <select class="select2-option gl_accounts" name="credit_account" id="credit_account" style="width: 100%" data-required>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="alert alert-error" id="date-error" style="display: none">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <span class="error-message" style="color: red"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 m-t-10 sm-m-t-10">
                                <input type="hidden" name="transaction_type" class="" id="transaction_type" value=""/>
                                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="transaction_id" id="transaction_id" value="">
                                <input type="hidden" name="" id="row_index" value="">
                                <button type="reset" class="btn btn-default m-t-5 close-modal">Close</button>
                                <button type="submit" class="btn btn-info m-t-5" id="saveBtn">Save</button>
                            </div>
                        </div>
                    </section>
                </form>
                
            @endif

           
        </section>
    </div>
</div>

