@foreach($accounts as $account)
    <optgroup label="{{$account->name}}">
        @if($account->groups)
            @foreach($account->groups as $group)
                <option value="{{$group->id}}" style="font-weight: 500"
                        disabled="disabled">{{$group->code.' '.$group->name}}</option>
                @if($group->nestedGroups($group->id)->count())
                    @foreach($group->nestedGroups($group->id)->get() as $child)
                        <option value="{{$child->id}}" style="font-weight: 500"
                                disabled="disabled">{{$child->code.' '.$child->name}}</option>
                        @if($child->nestedGroups($child->id)->count())
                            @foreach($child->nestedGroups($child->id)->get() as $sub)
                                <option value="{{$sub->id}}" style="font-weight: 500"
                                        disabled="disabled">{{$sub->code.' '.$sub->name}}</option>
                                @if($sub->ledgers->count())
                                    @foreach ($sub->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                        <option value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                        @if($child->ledgers->count())
                            @foreach ($child->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                                <option value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                            @endforeach
                        @endif
                    @endforeach
                @endif
                @if($group->ledgers->count())
                    @foreach ($group->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                        <option value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
                    @endforeach
                @endif
            @endforeach
        @endif
        @if($account->ledgers->count())
            @foreach ($account->ledgers()->orderBy('code', 'asc')->get() as $ledger)
                <option value="{{$ledger->code}}">{{$ledger->code.' '.$ledger->name}}</option>
            @endforeach
        @endif
    </optgroup>
@endforeach
@section('scripts')
    <script>
        $('.accounts').select2({
            placeholder: 'Select an option'
        });

    </script>
@stop