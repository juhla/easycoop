@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">

                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')

                    <section class="panel panel-default">
                        <?php //pr($accounts); ?>
                        <form method="post" action="{{url('transactions/registers/post-cr')}}" role="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="transaction-table">
                                        <thead>
                                        <tr>
                                            <th width="10%">Date</th>
                                            <th width="10%">Invoice</th>
                                            <th width="10%">Customer</th>
                                            <th width="20%">Description</th>
                                            <th width="10%">Bank Lodged</th>
                                            <th width="13%">Amount</th>
                                            <th width="20%">Debit Account</th>
                                            <th width="20%">Credit Account</th>


                                        </tr>
                                        </thead>
                                        <tbody align="">
                                        @if(count($ids) > 0)
                                            @foreach($ids as $key => $id)
                                                <?php
                                                $entry = App\Modules\Notebook\Models\CashReceipt::find($id);
                                                ?>
                                                <tr class="edit-row" data-value="">
                                                    <input type="hidden" name="register_id[]" value="{{$entry->id}}"/>
                                                    <input type="hidden" name="transaction_type[]" value="inflow"/>
                                                    <td>
                                                        {{ date('m/d/Y', strtotime($entry->date)) }}
                                                        <input type="hidden" name="date[]"
                                                               value="{{ date('m/d/Y', strtotime($entry->date)) }}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->invoice_no }}
                                                        <input type="hidden" name="reference_no[]"
                                                               value="{{$entry->invoice_no}}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->customer }}
                                                        <input type="hidden" name="customer[]"
                                                               value="{{$entry->customer}}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->description }}
                                                        <input type="hidden" name="description[]"
                                                               value="{{$entry->description}}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->bank }}
                                                    </td>
                                                    <td>
                                                        {{ formatNumber($entry->amount) }}
                                                        <input type="hidden" name="amount[]"
                                                               value="{{$entry->amount}}"/>
                                                    </td>
                                                    <td>
                                                        <select name="debit_account[]"
                                                                class="select2 accounts"
                                                                style="width: 100%" id="debit_account">
                                                            @include('transaction::registers.accounts.list')
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="credit_account[]"
                                                                class="select2 accounts"
                                                                style="width: 100%" id="credit_account">
                                                            @include('transaction::registers.accounts.list')
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <footer class="panel-footer  align-lg-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-default">Cancel</button>
                            </footer>
                        </form>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                </footer>
            </section>
        </aside>
    </section>
@endsection
@section('scripts')

@stop
