@extends('layouts.main')

@section('content')
    <section class="hbox stretch">
        {{--<aside class="aside-md bg-white b-r" id="subNav">
            <div class="wrapper b-b header">Submenu Header</div>
            <ul class="nav">
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Phasellus at ultricies</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Malesuada augue</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Donec eleifend</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dapibus porta</a>
                </li>
                <li class="b-b b-light">
                    <a href="#"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Dacus eu neque</a>
                </li>
            </ul>
        </aside>--}}
        <aside>
            <section class="vbox">
                <header class="header bg-white b-b clearfix">
                    <div class="row m-t-sm">
                        <div class="col-sm-3 m-b-xs">

                        </div>
                    </div>
                </header>
                <section class="scrollable wrapper w-f">
                    @include('flash::message')
                    <section class="panel panel-default">
                        <form method="post" action="{{url('transactions/registers/post-pc')}}" role="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped m-b-none" id="transaction-table">
                                        <thead>
                                        <tr>
                                            <th width="10%">Date</th>
                                            <th width="10%">PVC No</th>
                                            <th width="10%">Payee</th>
                                            <th width="20%">Description</th>
                                            <th width="10%">Amount</th>
                                            <th width="20%">Debit Account</th>
                                            <th width="20%">Credit Account</th>
                                        </tr>
                                        </thead>
                                        <tbody align="">
                                        @if(count($ids) > 0)
                                            @foreach($ids as $id)
                                                <?php
                                                $entry = App\Modules\Notebook\Models\PettyCash::find($id);
                                                ?>
                                                <tr class="edit-row" data-value="">
                                                    <input type="hidden" name="register_id[]" value="{{$entry->id}}" />
                                                    <input type="hidden" name="transaction_type[]" value="outflow" />
                                                    <td>
                                                        {{ date('m/d/Y', strtotime($entry->date)) }}
                                                        <input type="hidden" name="date[]" value="{{ date('m/d/Y', strtotime($entry->date)) }}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->pvc_no }}
                                                        <input type="hidden" name="reference_no[]" value="{{ $entry->pvc_no }}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->supplier }}
                                                        <input type="hidden" name="payee[]" value="{{ $entry->supplier }}"/>
                                                    </td>
                                                    <td>
                                                        {{ $entry->description}}
                                                        <input type="hidden" name="description[]" value="{{ $entry->description}}"/>
                                                    </td>
                                                    <td>
                                                        {{ formatNumber($entry->amount) }}
                                                        <input type="hidden" name="amount[]" class="form-control" id="amount" value="{{ $entry->amount }}" />
                                                    </td>
                                                    <td>
                                                        <select name="debit_account[]" class="select2 accounts" id="debit_account" style="width: 100%" >
                                                            @include('transaction::registers.accounts.list')
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="credit_account[]" class="select2 accounts" id="credit_account" style="width: 100%">
                                                            @include('transaction::registers.accounts.list')
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <footer class="panel-footer  align-lg-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{ route('cash-payment') }}" class="btn btn-default">Cancel</a>
                            </footer>
                        </form>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                </footer>
            </section>
        </aside>
    </section>
@endsection
@section('scripts')
@stop
