<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

// Route::group(['prefix' => 'transaction'], function () {
//     Route::get('/', function () {
//         dd('This is the Transaction module index page. Build something great!');
//     });
// });

Route::group(['prefix' => 'transactions', 'middleware' => ['web','verify_license', 'auth:web']], function () {
    Route::any('/', 'TransactionsController@index');

    Route::any('/inflow', 'TransactionsController@inflow');

    Route::any('/outflow', 'TransactionsController@outflow');
    Route::post('/save', 'TransactionsController@save');
    Route::get('/view/{id}', 'TransactionsController@view');
    Route::get('/delete/{id}', 'TransactionsController@delete');
    Route::get('/rejection-reason/{id}', 'TransactionsController@rejectionReason');
    Route::post('/delete', 'TransactionsController@delete');
    Route::get('/get-transaction/{id}', 'TransactionsController@getTransaction');

    //Registers routes
    Route::post('/registers/credit-purchase', 'TransactionsController@creditPurchase');
    Route::post('/registers/post-cp', 'TransactionsController@postCP');

    Route::post('/registers/credit-sales', 'TransactionsController@creditSales');
    Route::post('/registers/post-cs', 'TransactionsController@postCS');

    Route::post('/registers/petty-cash', 'TransactionsController@pettyCash');
    Route::post('/registers/post-pc', 'TransactionsController@postPC');

    Route::post('/registers/cash-receipt', 'TransactionsController@cashReceipt');
    Route::post('/registers/post-cr', 'TransactionsController@postCR');

    Route::post('/registers/cheque-payment', 'TransactionsController@chequePayment');
    Route::post('/registers/post-chqp', 'TransactionsController@postChqP');

    Route::post('/registers/cheque-receipt', 'TransactionsController@chequeReceipt');
    Route::post('/registers/post-chqr', 'TransactionsController@postChqR');

});
