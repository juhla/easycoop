<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Create hint path for invoice blade
        $this->loadViewsFrom(__DIR__ . '/../Modules/Invoices/Resources/Views', 'invoice');
        $this->loadViewsFrom(__DIR__ . '/../Modules/Journal/Resources/Views', 'journal');
        $this->loadViewsFrom(__DIR__ . '/../Modules/BatchPosting/Resources/Views', 'batch_posting');
        $this->loadViewsFrom(__DIR__ . '/../Modules/NotebookAdv/Resources/Views', 'notebookadv');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('setDbConnection', function ($conf, $db) {
            Config::set('database.connections.tenant_conn', [
                'driver' => 'mysql',
                'host' => Config::get('database.connections.mysql.host'),
                'database' => $db['db'],
                'username' => Config::get('database.connections.mysql.username'),
                'password' => Config::get('database.connections.mysql.password'),
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
            ]);
        });
    }

}
