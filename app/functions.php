<?php

use App\Modules\Base\Traits\Tenant;
use MathPHP\Functions\Map;

if (!function_exists('bcadd')) {
    function bcadd($Num1, $Num2, $Scale = null)
    {
        // check if they're valid positive numbers, extract the whole numbers and decimals
        if (!preg_match("/^\+?(\d+)(\.\d+)?$/", $Num1, $Tmp1) ||
            !preg_match("/^\+?(\d+)(\.\d+)?$/", $Num2, $Tmp2)) return ('0');

        // this is where the result is stored
        $Output = array();

        // remove ending zeroes from decimals and remove point
        $Dec1 = isset($Tmp1[2]) ? rtrim(substr($Tmp1[2], 1), '0') : '';
        $Dec2 = isset($Tmp2[2]) ? rtrim(substr($Tmp2[2], 1), '0') : '';

        // calculate the longest length of decimals
        $DLen = max(strlen($Dec1), strlen($Dec2));

        // if $Scale is null, automatically set it to the amount of decimal places for accuracy
        if ($Scale == null) $Scale = $DLen;

        // remove leading zeroes and reverse the whole numbers, then append padded decimals on the end
        $Num1 = strrev(ltrim($Tmp1[1], '0') . str_pad($Dec1, $DLen, '0'));
        $Num2 = strrev(ltrim($Tmp2[1], '0') . str_pad($Dec2, $DLen, '0'));

        // calculate the longest length we need to process
        $MLen = max(strlen($Num1), strlen($Num2));

        // pad the two numbers so they are of equal length (both equal to $MLen)
        $Num1 = str_pad($Num1, $MLen, '0');
        $Num2 = str_pad($Num2, $MLen, '0');

        // process each digit, keep the ones, carry the tens (remainders)
        for ($i = 0; $i < $MLen; $i++) {
            $Sum = ((int)$Num1{$i} + (int)$Num2{$i});
            if (isset($Output[$i])) $Sum += $Output[$i];
            $Output[$i] = $Sum % 10;
            if ($Sum > 9) $Output[$i + 1] = 1;
        }

        // convert the array to string and reverse it
        $Output = strrev(implode($Output));

        // substring the decimal digits from the result, pad if necessary (if $Scale > amount of actual decimals)
        // next, since actual zero values can cause a problem with the substring values, if so, just simply give '0'
        // next, append the decimal value, if $Scale is defined, and return result
        $Decimal = str_pad(substr($Output, -$DLen, $Scale), $Scale, '0');
        $Output = (($MLen - $DLen < 1) ? '0' : substr($Output, 0, -$DLen));
        $Output .= (($Scale > 0) ? ".{$Decimal}" : '');
        return ($Output);
    }
}


if (!function_exists('bcsub')) {
    function bcsub($Num1, $Num2)
    {
        return $Num1 - $Num2;
    }
}


function lastDayOf($period, DateTime $date = null)
{
    $period = strtolower($period);
    $validPeriods = array('year', 'quarter', 'month', 'week');

    if (!in_array($period, $validPeriods))
        throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

    $newDate = ($date === null) ? new DateTime() : clone $date;

    switch ($period) {
        case 'year':
            $newDate->modify('last day of december ' . $newDate->format('Y'));
            break;
        case 'quarter':
            $month = $newDate->format('n');

            if ($month < 4) {
                $newDate->modify('last day of march ' . $newDate->format('Y'));
            } elseif ($month > 3 && $month < 7) {
                $newDate->modify('last day of june ' . $newDate->format('Y'));
            } elseif ($month > 6 && $month < 10) {
                $newDate->modify('last day of september ' . $newDate->format('Y'));
            } elseif ($month > 9) {
                $newDate->modify('last day of december ' . $newDate->format('Y'));
            }
            break;
        case 'month':
            $newDate->modify('last day of this month');
            break;
        case 'week':
            $newDate->modify(($newDate->format('w') === '0') ? 'now' : 'sunday this week');
            break;
    }

    return $newDate;
}

function firstDayOf($period, DateTime $date = null)
{
    $period = strtolower($period);
    $validPeriods = array('year', 'quarter', 'month', 'week');

    if (!in_array($period, $validPeriods))
        throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

    $newDate = ($date === null) ? new DateTime() : clone $date;

    switch ($period) {
        case 'year':
            $newDate->modify('first day of january ' . $newDate->format('Y'));
            break;
        case 'quarter':
            $month = $newDate->format('n');

            if ($month < 4) {
                $newDate->modify('first day of january ' . $newDate->format('Y'));
            } elseif ($month > 3 && $month < 7) {
                $newDate->modify('first day of april ' . $newDate->format('Y'));
            } elseif ($month > 6 && $month < 10) {
                $newDate->modify('first day of july ' . $newDate->format('Y'));
            } elseif ($month > 9) {
                $newDate->modify('first day of october ' . $newDate->format('Y'));
            }
            break;
        case 'month':
            $newDate->modify('first day of this month');
            break;
        case 'week':
            $newDate->modify(($newDate->format('w') === '0') ? 'monday last week' : 'monday this week');
            break;
    }

    return $newDate;
}

/**
 * Flattens a deep array into an array of strings.
 * Opposite of expandList().
 *
 * So `array('Some' => array('Deep' => array('Value')))` becomes `Some.Deep.Value`.
 *
 * Note that primarily only string should be used.
 * However, boolean values are casted to int and thus
 * both boolean and integer values also supported.
 *
 * @param array $data
 * @return array
 */
function flattenList(array $data, $separator = '.')
{
    $result = array();
    $stack = array();
    $path = null;

    reset($data);
    while (!empty($data)) {
        $key = key($data);
        $element = $data[$key];
        unset($data[$key]);

        if (is_array($element) && !empty($element)) {
            if (!empty($data)) {
                $stack[] = array($data, $path);
            }
            $data = $element;
            reset($data);
            $path .= $key . $separator;
        } else {
            if (is_bool($element)) {
                $element = (int)$element;
            }
            $result[] = $path . $element;
        }

        if (empty($data) && !empty($stack)) {
            list($data, $path) = array_pop($stack);
            reset($data);
        }
    }
    return $result;
}


function arrayFlatten($array, $preserveKeys = false)
{
    if ($preserveKeys) {
        return _arrayFlatten($array);
    }
    if (!$array) {
        return array();
    }
    $result = array();

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, arrayFlatten($value));
        } else {
            $result[$key] = $value;
        }
    }
    return $result;
}

function _arrayFlatten($a, $f = array())
{
    if (!$a) {
        return array();
    }
    foreach ($a as $k => $v) {
        if (is_array($v)) {
            $f = _arrayFlatten($v, $f);
        } else {
            $f[$k] = $v;
        }
    }
    return $f;
}

/**
 *helper function to set active link
 *
 * @param url $path
 * @param string $active
 * @return mixed
 */
function set_active($path, $active = 'active')
{
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

/**
 * generate authentication code
 * @return string
 */
function generate_verification_code()
{
    $str = '012345678912abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $token = str_shuffle($str);
    return $token;
}

function pr($var)
{
    $template = php_sapi_name() !== 'cli' ? '<pre>%s</pre>' : "\n%s\n";
    printf($template, print_r($var, true));
}

/**
 * get member roles
 * @return mixed
 */
function getRoles($id = null)
{
    $role = \App\Modules\Base\Models\RoleUser::where('user_id', $id)->get();
    $role = $role->toArray();
    $role = array_pop($role);
    $id = $role['role_id'];
    if ($id) {
        $role = \App\Modules\Base\Models\Role::find($id);
        return $role->display_name;
    } else {
        $roles = \App\Modules\Base\Models\Role::where('is_admin', 0)->get();

        return $roles;
    }
}

function getAllUserRoles($user_id)
{
    $user_role = DB::table('role_user')
        ->where('user_id', $user_id)
        ->get();

    foreach ($user_role as $eachRole) {
        $role = \App\Modules\Base\Models\Role::find($eachRole->role_id);
        if ($role->display_name != 'NULL') {
            $theRole[] = $role->display_name;
        }
    }

    return implode("|", $theRole);
}

function getUserRole($user_id)
{
    $user_role = DB::table('role_user')
        ->where('user_id', $user_id)
        ->first();
//dd($user_role);
    $role = \App\Modules\Base\Models\Role::find($user_role->role_id);
//    pr($role);
    return $role;
}

function getLedgerId($ledger_code)
{
    $ledger_id = \App\Modules\Base\Models\Ledger::where('code', $ledger_code)->first()->id;
    return $ledger_id;
}

/**
 * helper function to remove whitespace and wildcards from string
 * @param $string
 * @return String
 */
function makeSlug($string)
{
    //remove wildcards from string
    //$z = strtolower($z);
    $string1 = preg_replace('/[^a-zA-Z0-9.-]+/', '', strtolower($string));
    //replace white space with underscore
    $string2 = str_replace(" ", "_", $string1);

    return trim($string2);
}


function get_default_msme_permission()
{
    return [

        'access-townhall' => 'Access Townhall',
        'view-thread' => 'View Thread',
        'create-thread' => 'Create Thread',
        'edit-thread' => 'Edit Thread',
        'delete-thread' => 'Delete Thread',
        'reply-thread' => 'Reply Thread',
        'can-connect' => 'Can Send Connection Request',
        'can-send-message' => 'Can Send Message',

        'access-accounting' => 'Access Accounting',
        'view-dashboard' => 'View Dashboard',
        'enter-transactions' => 'Enter Transactions',
        'edit-transactions' => 'Edit Transactions',
        'delete-transactions' => 'Delete Transactions',
        'approve-transactions' => 'Approve Transactions',
        'manage-invoice' => 'Manage Invoice',
        'create-journal' => 'Create Journal',
        'edit-journal' => 'Edit Journal',
        'delete-journal' => 'Delete Journal',
        'approve-journal' => 'Approve Journal',
        'manage-chart-of-account' => 'Manage Chart of Account',
        'manage-customers' => 'Manage Customers',
        'manage-vendors' => 'Manage Vendors',

        'access-notebook' => 'Access Notebook',
        'enter-notebook' => 'Enter Notebook',
        'edit-notebook' => 'Edit Notebook',
        'delete-notebook' => 'Delete Notebook',
        'post-notebook' => 'Post Notebook'
    ];
}


/**
 * helper method to format amount
 * @param $num
 * @return string
 */
function formatNumber($num)
{
    //return '₦'.number_format($num, 2, '.', ',');
    return number_format($num, 2, '.', ',');
}

/**
 * helper function to get all countries
 * @param $country_id
 * @return mixed
 */
function getCountry($country_id = false)
{
    if ($country_id) {
        $country = DB::table('countries')
            ->where('id', $country_id)
            ->first();
        return $country->name;
    } elseif ($country_id === 0) {
        return '';
    } else {
        $country = DB::table('countries')->orderBy('name', 'asc')->get();
        return $country;
    }
}

/**
 *
 * @param $country_id
 * @return mixed
 */
function getCountryInfo($country_id)
{
    if ($country_id) {
        $country = DB::table('countries')
            ->where('id', $country_id)
            ->first();
        return $country->currency_name . '(' . $country->currency_symbol . ')';
    }
}

/**
 * @param $country_id
 * @return array
 */
function getStatesByCountry($country_id = false)
{
    $states = DB::table('states')->where('country_id', $country_id)->get();
    return $states;
}

function getState($state_id = null)
{
    if ($state_id > 0) {
        $state = DB::table('states')
            ->where('id', $state_id)
            ->first();
        return $state->name;
    } else {
        $state = DB::table('states')->get();
        return $state;
    }
}

/**
 * helper function to get all industries
 * @param  $industry_id
 * @return mixed
 */
function getIndustry($industry_id = 0)
{
    if ($industry_id > 0) {
        $industry = DB::table('industries')
            ->where('id', $industry_id)
            ->first();
    } else {
        $industry = DB::table('industries')->get();
    }
    return $industry;
}

/**
 * helper function to get all banks
 * @param $country_id
 * @return mixed
 */
function getBanks($bank_id = 0)
{
    if ($bank_id > 0) {
        $bank = DB::table('banks')
            ->where('id', $bank_id)
            ->first();
        return $bank->bank_name;
    } else {
        $banks = DB::table('banks')->orderBy('bank_name', 'asc')->get();
        return $banks;
    }
}


/**
 * helper method to dispaly nested chart of account groups
 * @param $groups
 * @param string $view
 * @return string
 */
function dumpGroups($groups, $view = 'business-setup::partials._list-group-child')
{
    $groupList = '';
    foreach ($groups as $group) {
        $groupList .= View::make($view)->with('group', $group);
    }
    return $groupList;
}

function getLedgerName($dc, $id)
{
    $trans = \App\Modules\Base\Models\TransactionItem::where('transaction_id', $id)
        ->where('dc', $dc)->withoutGlobalScopes()->first();
    if ($trans) {
        $account = \App\Modules\Base\Models\Ledger::find($trans->ledger_id);

        if ($account) {
            return $account->name;
        }
    }
    return 'NA';
}

function getGLNamebyCode($code)
{
    $account = \App\Modules\Base\Models\Ledger::where('code', $code)->first();

    return $account->name;
}

/**
 * display report child view
 * @param $groups
 * @param $report
 * @param int $debit
 * @param int $credit
 * @param $range
 * @return string
 */
function displayReportItem($groups, $report, $debit = 0, $credit = 0, $range = null)
{
    $groupList = '';
    foreach ($groups as $group) {
        $groupList .= View::make('reports::partials._' . $report . '-report-child')->with(['group' => $group, 'debitBalance' => $debit, 'creditBalance' => $credit, 'range' => $range]);
    }
    return $groupList;
}

/**
 * helper function to get user comapany
 * @param $user_id
 * @return String
 */
function getUserCompany($user_id)
{
    $res = \App\Modules\Base\Models\Company::where('user_id', $user_id)->get();
    if ($res) {
        return $res;
    } else {
        return false;
    }
}

/**
 * Function to set status
 *
 * @param $status
 * @return string
 */
function getStatus($status)
{
    if ($status === 1) {
        $stat = "<span class='label label-success'>Active</span>";
    } elseif ($status === 0) {
        $stat = "<span class='label label-danger'>Inactive</span>";
    }
    return $stat;
}

function trimText($text, $length = 200)
{
    if (strlen($text) > $length):
        $string = substr($text, 0, strpos($text, ' ', $length)) . '&hellip;';
        return ltrim($string);
    else:
        return ltrim($text);
    endif;
}

/**
 * Helper function to display list of companies
 * a user is collaborating with
 * 1 => accepted not paid
 * 2 => accepted and paid
 * @return mixed
 */
function getCollaboratorCompanies()
{

    $res = \App\Modules\Base\Models\Collaborator::where('client_id', auth()->user()->id)
        ->whereIn('status', [2, 5])
        ->get();
    //$res = \App\Modules\Base\Models\Collaborator::where('client_id', auth()->user()->id)->get();
    return $res;
}

function getAgentBusinesses()
{

    $agentSme = new \App\Modules\Base\Models\Agents\AgentSme();
    return $agentSme->getAgentBusiness(auth()->user()->id);

}


/**
 * Helper method to calculate the percentage difference
 * between previous month and current month in dashboard financial statement
 * @param $number1
 * @param $number2
 * @return string
 */
function calculatePercentage($number1, $number2)
{
    if ($number2 == 0) {
        return '<td class="text-danger"> <i class="fa fa-level-down"></i>' . number_format($number2, 2) . '% </td>';
    } elseif ($number1 > $number2) {
        $per = bcsub($number1, $number2);
        $per = ($per * 100) / $number2;
        return '<td class="text-danger"> <i class="fa fa-level-down"></i>' . number_format($per, 2) . '% </td>';
    } elseif ($number2 > $number1) {
        $per = bcsub($number2, $number1);
        $per = ($per * 100) / $number2;
        return '<td class="text-success"> <i class="fa fa-level-up"></i>' . number_format($per, 2) . '% </td>';
    } else {
        return '<td class=""> 0% </td>';
    }
}

/**
 *Check if a user has edit right
 */
function canEdit()
{
    if (!auth()->user()->hasRole(['business-owner', 'advanced-business-owner', 'basic-business-owner'])) {
        //set user id
        $user_id = auth()->user()->id;
        //get current viewing company
        $company_db = session('company_db');
        //find company
        $company = \App\Modules\Base\Models\Company::where('database', $company_db)->first();
        //find permission from collaborator table
        $res = \App\Modules\Base\Models\Collaborator::where('client_id', $user_id)->where('company_id', $company->id)->first();

        if ($res->permission === 'edit') {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

/**
 * Appends arrays together
 * @param $base_array
 * @param ...$arrays_to_join
 * @return mixed - joined array
 */
function appendArray($base_array, ...$arrays_to_join)
{
    if (!is_array($base_array)) {
        return $base_array;
    }
    foreach ($arrays_to_join as $sec_array) {
        if (!is_array($sec_array))
            continue;
        foreach ($sec_array as $key => $value) {
            $base_array[$key] = $value;
        }
    }
    return $base_array;
}


function getLedgerIdForCustomer($customer_id)
{
    $ledger_code = \App\Modules\Base\Models\Customer::find($customer_id)->ledger_code;
    $ledger_id = \App\Modules\Base\Models\Ledger::whereRaw('code = ?', [$ledger_code])->first()['id'];
    return $ledger_id;
}

if (!function_exists('createInvoiceNo')) {
    function createInvoiceNo($calledBy)
    {
        return \App\Modules\Base\Helpers\Helpers::createInvoiceNo($calledBy);
    }
}

if (!function_exists('createPVCNo')) {
    function createPVCNo($calledBy)
    {
        return \App\Modules\Base\Helpers\Helpers::createPVCNo($calledBy);
    }
}

function createInvoiceNo_backup($calledBy)
{

    $transactionCode = "";
    try {

        $id = 1; //first entry
        if ($calledBy == 'Cash Receipt') {
            $result = \App\Modules\Notebook\Models\CashReceipt::all();
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CR";
        } else if ($calledBy == 'Cheque Receipt') {
            $result = \App\Modules\Notebook\Models\ChequeReceipt::all();
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CHR";
        } else if ($calledBy == 'Credit Sales') {
            $result = \App\Modules\Notebook\Models\CreditSales::all();
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CS";
        } else if ($calledBy == 'Credit Purchase') {
            $result = \App\Modules\Notebook\Models\CreditPurchase::all();
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CRP";
        }

        $string_number = str_pad($id, 6, "0", STR_PAD_LEFT);
        $transactionCode = $code . $string_number;
    } catch (Exception $ex) {
        app('sentry')->captureException($ex);
    }
    return $transactionCode;
}

function createPVCNo_backup($calledBy)
{
    $transactionCode = "";
    try {
        $id = 1; //first entry
        if ($calledBy == 'Cash Payment') {
            try {
                $result = \App\Modules\Notebook\Models\PettyCash::all();
            } catch (Exception $ex) {
                app('sentry')->captureException($ex);
            }
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CP";
        } else if ($calledBy == 'Cheque Payment') {
            $result = \App\Modules\Notebook\Models\ChequePayment::all();
            if (!$result->isEmpty()) {
                $id = $result->last()->id;
            }
            $code = "#CHP";
        }

        $string_number = str_pad($id, 6, "0", STR_PAD_LEFT);
        $transactionCode = $code . $string_number;
    } catch (Exception $e) {
        app('sentry')->captureException($e);
    }

    return $transactionCode;
}

// get month name from number
function month_name($month_number)
{
    return date('F', mktime(0, 0, 0, $month_number, 10));
}

// get get last date of given month (of year)
//function month_end_date($year, $month_number)
//{
//    return date("t", strtotime("$year-$month_number-0"));
//}

function month_end_date($year, $month_number)
{
    return date("t", strtotime("$year-$month_number-1"));
}

// return two digit month or day, e.g. 04 - April
function zero_pad($number)
{
    if ($number < 10)
        return "0$number";

    return "$number";
}

/**
 * Generic random strings generator for passkeys, pins, serials, etc
 *
 * @param int $xlenght length of character to generate
 * @param string $xters character base (NUM, ALPHA, ALNUM)
 * @return string xstring the generated string
 *
 */
function GenerateRandomString($xlenght = 10, $xters = 'NUM')
{
    //$xters can be NUM, ALPHA, ALPHANUM
    $xters_xbase = "";
    $xstring = "";

    if (strcasecmp($xters, 'NUM') == 0) {
        //Numerics only
        $xters_xbase = "123456789";
    } elseif (strcasecmp($xters, 'ALPHA') == 0) {
        //Alphabetic Only - certain xters omitted cos they look like nums e.g. S & 5
        $xters_xbase = "ABCDEFGHJKLMNPQRTUVWXY";
    } else {
        //Alpha-Numerics
        $xters_xbase = "ABCDEFGHJKLMNPQRTUVWXY" . "123456789";
    }

    $xter_xbase_len = strlen($xters_xbase);

    $xstring = "";
    for ($i = 1; $i <= $xlenght; $i++) {
        //use a random number to get a random alpha from $xters-xbase
        $xstring .= $xters_xbase{mt_rand(0, ($xter_xbase_len - 1))};
    }

    return $xstring;
}

// Return quarters between tow dates. Array of objects
function get_quarters($start_date, $end_date)
{

    $quarters = array();

    $start_month = date('m', strtotime($start_date));
    $start_year = date('Y', strtotime($start_date));

    $end_month = date('m', strtotime($end_date));
    $end_year = date('Y', strtotime($end_date));

    $start_quarter = ceil($start_month / 3);
    $end_quarter = ceil($end_month / 3);
    $quarter = $start_quarter; // variable to track current quarter

    // Loop over years and quarters to create array
    for ($y = $start_year; $y <= $end_year; $y++) {
        if ($y == $end_year)
            $max_qtr = $end_quarter;
        else
            $max_qtr = 4;

        for ($q = $quarter; $q <= $max_qtr; $q++) {

            $current_quarter = new stdClass();

            $end_month_num = zero_pad($q * 3);
            $start_month_num = ($end_month_num - 2);
            $q_start_month = month_name($start_month_num);
            $q_end_month = month_name($end_month_num);

            if ($start_month_num < 10) {
                $start_month_num = "0" . $start_month_num;
            }

            $current_quarter->period = "Qtr $q ($q_start_month - $q_end_month) $y";
            $current_quarter->periodOnly = "Qtr $q";
            $current_quarter->period_start = "$y-$start_month_num-01";      // yyyy-mm-dd
            $current_quarter->period_end = "$y-$end_month_num-" . month_end_date($y, $end_month_num);

            $quarters[] = $current_quarter;
            unset($current_quarter);
        }
        $quarter = 1; // reset to 1 for next year
    }

    return $quarters;

}


function migrateDB()
{
    $user = auth()->user();
    event(new \App\Modules\Base\Events\BmacPatch());
}


function getMainUserID()
{
    $userId = auth()->user()->id;
    $user = auth()->user();
    //employee
    if ($user->hasRole(['admin-payroll', 'employee-ims', 'employee-payroll', 'employee'])) {
        //get payroll admin business it is collaborating with
        $session = session('company_db');
        $result = \App\Modules\Base\Models\Collaborator::whereClient_id($userId)
            ->whereHas('company', function ($query) use ($session) {
                if (!empty($session)) {
                    $query->where('database', $session);
                }
            })
            ->get()->first();
        $userId = $result->user_id;
    }
    return $userId;
}

function getBusinessOwnerID()
{
    $userId = auth()->user()->id;
    $user = auth()->user();
    //employee
    if ($user->hasRole(['admin-payroll', 'employee-ims', 'employee-payroll', 'employee', 'professional', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
        //get payroll admin business it is collaborating with
        $session = session('company_db');
        $result = \App\Modules\Base\Models\Collaborator::whereClient_id($userId)
            ->whereHas('company', function ($query) use ($session) {
                if (!empty($session)) {
                    $query->where('database', $session);
                }
            })
            ->get()->first();
        $userId = $result->user_id;
    }
    return $userId;
}

function getBusinessOwnerAuth()
{

    $user = auth()->user();
    if ($user->hasRole(['admin-payroll', 'employee-payroll', 'employee', 'professional', 'assemblyline-professional', 'assemblyline-professional-agent'])) {
        //get payroll admin business it is collaborating with
        $session = session('company_db');
        $result = \App\Modules\Base\Models\Collaborator::with(['employer'])
            ->whereHas('company', function ($query) use ($session) {
                $query->where('database', $session);
            })
            ->whereClient_id($user->id)->get()->first();
        $user = $result->employer;

        // dd($user);
    }
    return $user;
}

function getBusinessOwner()
{
    $userId = \Illuminate\Support\Facades\Auth::user()->id;
    $user = \Illuminate\Support\Facades\Auth::user();
    //employee
    if ($user->hasRole(['admin-payroll', 'employee-payroll', 'employee'])) {
        //get payroll admin business it is collaborating with
        $result = \App\Modules\Base\Models\Collaborator::whereClient_id($userId)->get()->first();
        $userId = $result->user_id;
    }
    return $userId;
}

function getAccountCurrency()
{
    return "₦";
}

function getTransactionIdByLedger($code)
{
    $ledger = new \App\Modules\Base\Models\Ledger();
    $res = $ledger->where('code', $code)->first();
    return $res->id;
}
