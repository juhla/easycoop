<?php
function plBuilder($ledger, $transactions)
{
    $result = "";
    foreach ($transactions as $date => $trans) {
        $result .= buildTransactionRow($ledger->opening_balance_type, $date, $trans)[0];
    }
    return $result;
}

;

function bsBuilder($ledger, $transactions, $openingBalanceFromPreviousYear, $openingBalanceFromPreviousYearType, $export = false)
{
    $result = "";
    $bf = $openingBalanceFromPreviousYear;
    $bft = $openingBalanceFromPreviousYearType;
    $currency = "N";
//    if ($export) {
//        $currency = "N";
//    } else {
//        $currency = getAccountCurrency();
//    }

    foreach ($transactions as $date => $trans) {

        $tmp = buildTransactionRow($ledger->opening_balance_type, $date, $trans, $bf, $bft);

        $bf = $tmp[1];
        // $bft = $ledger->opening_balance_type;
        $bft = $tmp[2];
        $result .= $tmp[0];
    }
    //pr($bft);
    if ($bft === "C") {
        $result .= '<tr><td align="left" colspan="3"><i><strong>Balance c/d</strong></i></td><td> ' . $currency . "" . formatNumber(abs($bf)) . '</td><td></td> <td>0</td></tr>';
    } else {
        $result .= '<tr><td align="left" colspan="3"><i><strong>Balance c/d</strong></i></td><td> </td><td>' . $currency . "" . formatNumber(abs($bf)) . ' </td> <td>0</td></tr>';
    }
    //pr($tmp[1]);
    //$result .= '<tr><td align="left" colspan="3"><i><strong>Balance c/d</strong></i></td><td>' .($bft == "D")? formatNumber(abs($bf)):0 . '</td><td>' . ($bft == "C")? formatNumber(abs($bf)): 0 . ' </td><td>0</td></tr>'
    return $result;

}


function calculateBalance($balance, $transactionType, $value)
{
    if (strtolower($transactionType) == 'c') {
        $balance = $balance - abs($value);
    } elseif (strtolower($transactionType) == 'd') {
        $balance = $balance + $value;
    }
    return $balance;
}

/**
 *
 *
 * The buildTransactionRow utility function helps in the view for ledger statements
 * to build the transaction rows and fill in the appropriate transaction amount as well as the balances
 *
 * Parameters
 *   accountType => used to determine how which side to display the transaction->amount in
 *   month => the month in view
 *   transactions => the transactions for that period
 *   balanceBroughtForward => helps in calculating the appropriate balance for accounts that have balances from an earlier period.
 *   balanceBroughtForwardType => VERY IMPORTANT helps to identify if the balance that was brought foward was a credit or debit???
 *   IMPORTANT NOTE: IT IS MEANT TO BE USED FOR PURPOSE STATED ABOVE... NEED TO CONFIRM THAT THIS CHANGE WITH RETURNING THE balanceBroughtForwardType
 *   AS DONE BELOW AS NO EFFECT COMPARED TO RETRIEVING THE VALUES FROM $ledger->opening_balance_type AS PREVIOUSLY DONE!!!
 *
 *
 */

function buildTransactionRow($accountType, $month, $transactions, $balanceBroughtForward = False, $balanceBroughtForwardType = False)
{
    $debit = 0;
    $credit = 0;
    $balance = 0;
    $result = "";

    // if ( $balanceBroughtForward ) {
    $balance = calculateBalance($balance, $balanceBroughtForwardType, $balanceBroughtForward);
    if ($balanceBroughtForwardType == 'D') {
        $debit = $debit + $balanceBroughtForward;
        //    $result .= '<tr><td align="left">' . date('d/m/Y', strtotime($month . '-01') ) . '</td><td><strong>Balance b/f</strong></td><td></td><td></td><td>'  . formatNumber( abs( $balanceBroughtForward ) ) . '</td><td></td> <td>' . formatNumber( abs( $balanceBroughtForward ) ) . '</td></tr>';
    } elseif ($balanceBroughtForwardType == 'C') {
        $credit = $credit - $balanceBroughtForward;
        //  $result .= '<tr><td align="left">' . date('d/m/Y', strtotime($month . '-01') ) . '</td><td><strong> Balance b/f</strong></td><td></td><td></td><td></td><td>' . formatNumber( abs( $balanceBroughtForward ) ) . ' </td> <td>' . formatNumber( abs( $balanceBroughtForward ) )  . '</td></tr>';
    }
    //}


    foreach ($transactions as $transaction) {
        $description = empty($transaction->transactions->description) ? $transaction->item_description : $transaction->transactions->description;
        $tmp = '<tr><td align="left">' . date('d/m/Y', strtotime($transaction->transactions->transaction_date)) . '</td>
   <td>' . $transaction->transactions->reference_no . '</td>
   <td>' . $description . '</td>';

        $balance = calculateBalance($balance, $transaction->dc, $transaction->amount);

        if ($transaction->dc === 'D') {
            $debit += $transaction->amount;

            $tmp .= '<td>' . formatNumber($transaction->amount) . '</td><td> - </td>';
            $tmp .= '<td>' . formatNumber($balance) . '</td>';
        } else {
            $credit += $transaction->amount;

            $tmp .= '<td> - </td><td>' . formatNumber($transaction->amount) . '</td>';
            $tmp .= '<td>' . formatNumber($balance) . '</td>';
        }
        //pr($transaction->dc . " >>" . $balance . ">> debit >>" . $debit . ">> credit >> " . $credit);

        $tmp .= ' </tr>';
        $result .= $tmp;
    }

    if ($debit > $credit) {
        $balanceBroughtForwardType = 'D';
        // $result .= '<tr><td align="left">' . date('d/m/Y', strtotime($month . '-01') ) . '</td><td><i><strong>Balance c/d</strong></i></td><td></td><td></td><td>' . formatNumber( abs( $balance ) ) . '</td><td></td> <td>0</td></tr>';
    } else {
        $balanceBroughtForwardType = 'C';
        // $result .= '<tr><td align="left">' . date('d/m/Y', strtotime($month . '-01') ) . '</td><td><i><strong>Balance c/d</strong></i></td><td></td><td></td><td></td><td>' . formatNumber( abs( $balance ) ) . ' </td> <td>0</td></tr>';
    }

    // dd( [ $accountType, $credit, $debit, $balance, $balanceBroughtForward, $balanceBroughtForwardType ] );
    return [$result, $balance, $balanceBroughtForwardType];
}
