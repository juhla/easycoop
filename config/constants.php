<?php

return [

    'ims_url' => env('IMS_URL'),
    'payroll_url' => env('PAYROLL_URL'),
    'support_url' => env('SUPPORT_URL'),
    'api_url' => env('API_URL'),
    'sme_path' => env("SME_URL"),
    'business_path' => env("BUSINESS_URL"),
    'live_username' => env("IKOOBA_USERNAME"),
    'test_username' => env("BMAC_USERNAME"),
    'live_password' => env("IKOOBA_PASSWORD"),
    'test_password' => env("BMAC_PASSWORD"),
    'welcomeBcc' => [],

    'assemblyLineEmail' => [
        'assemblyline@ikooba.com'
    ],


    'payment_mode' => [
        '1' => "Cash",
        '2' => "Cheque",
        '3' => "Bank Transfer",
        '4' => "POS Payment",
        '5' => "Card Payment",
        '6' => "Others",
    ],

    'transaction_type' => [
        '1' => "Money in",
        '2' => "Money Out",
    ],

    'transaction_mode' => [
        '1' => "Cash",
        '2' => "Bank",
        '3' => "Credit",
    ],

    'payment_mode_bank' => [
        '1' => "Cheque",
        '2' => "Bank Transfer",
        '3' => "POS",
    ],

];
