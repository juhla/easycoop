<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Keys
    |--------------------------------------------------------------------------
    |
    | Set the public and private API keys as provided by reCAPTCHA.
    |
    | In version 2 of reCAPTCHA, public_key is the Site key,
    | and private_key is the Secret key.
    |
    */
    //'public_key'     => env('RECAPTCHA_PUBLIC_KEY', '6LfndAoTAAAAAHTl51N0-WD_Og-ttwrhQkkWmX8w'),
    'public_key'     => env('RECAPTCHA_PUBLIC_KEY', '6LeT6EEUAAAAALsZOaJyPZs_CtY26iwCKKXJHPSd'),
   // 'private_key'    => env('RECAPTCHA_PRIVATE_KEY', '6LfndAoTAAAAAAOrbDjK4KbU25e6w7-yvZ8VUvtd'),
    'private_key'    => env('RECAPTCHA_PRIVATE_KEY', '6LeT6EEUAAAAALvWApk1D5SWKjTfVM5Px1H8z3kr'),

    /*
    |--------------------------------------------------------------------------
    | Template
    |--------------------------------------------------------------------------
    |
    | Set a template to use if you don't want to use the standard one.
    |
    */
    'template'    => '',

    /*
    |--------------------------------------------------------------------------
    | Driver
    |--------------------------------------------------------------------------
    |
    | Determine how to call out to get response; values are 'curl' or 'native'.
    | Only applies to v2.
    |
    */
    'driver'      => 'curl',

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | Various options for the driver
    |
    */
    'options'     => [

        'curl_timeout' => 1,

    ],

    /*
    |--------------------------------------------------------------------------
    | Version
    |--------------------------------------------------------------------------
    |
    | Set which version of ReCaptcha to use.
    |
    */

    'version'     => 2,

];
