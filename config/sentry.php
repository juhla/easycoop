<?php

return [

    'dsn' => 'https://7a6c281a414d405c83bcc41fd86e4638@errors.ikoobaapp.com//10',

    // capture release as git sha
    // 'release' => trim(exec('git --git-dir ' . base_path('.git') . ' log --pretty="%h" -n1 HEAD')),
    'send_default_pii' => true,
    'breadcrumbs' => [

        // Capture bindings on SQL queries logged in breadcrumbs
        'sql_bindings' => true,

    ],

];
