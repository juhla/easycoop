<?php


$paystackLive = [
    'key' => 'pk_live_7505355c505d780e487e23eb2ae2911b8a42f09e',
    'secret' => 'sk_live_f04160f33a494477b2abb58deec216174820fa24',
    'plan_name' => 'plan',
];

$paystackTest = [
    'key' => 'pk_test_a2b9073718cd65cfa86c92afa4bfe26418b5f99c',
    'secret' => 'sk_test_ecd80e034b6f46ca65749b9844849e5998ffc2fc',
    'plan_name' => 'plan_code',
];

$paystack = $paystackTest;
if (config("app.env") == "production") {
    $paystack = $paystackLive;
}


return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paystack' => $paystack,

];
