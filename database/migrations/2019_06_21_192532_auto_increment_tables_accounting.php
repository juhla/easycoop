<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class AutoIncrementTablesAccounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $DB = Config::get('database.tenant_conn.database');
        //DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'char');
        $nonColumns = ['permission_role', 'role_user'];
        $tables = DB::getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $table) {
            if (!in_array($table, $nonColumns)) {
                try {
                    $columns = $this->getTableColumns($table);
                    foreach ($columns as $eachColumns) {
                        $res = $this->getNullableColumns($eachColumns, $table);
                        switch ($res->DATA_TYPE) {
                            case 'enum':
                                {
                                    DB::statement(" ALTER TABLE  $this->table  MODIFY  $eachColumns varchar(191) NULL default $res->COLUMN_DEFAULT");
                                }
                                break;
                        }
                    }
                    //$isAutoColumn = $this->getAutoIncrementColumns('id', $table);
                    $hasColumn = Schema::hasColumn($table, 'id');
                    if ($hasColumn) {
                        $col = DB::getSchemaBuilder()->getColumnType($table, 'id');
                        $isColumnType = ($col == 'integer') ? true : false;
                        if ($isColumnType) {
                            Schema::table($table, function (Blueprint $table) {
                                $table->bigIncrements('id')->change();
                            });
                        }
                    }
                } catch (\Exception $e) {
                    app('sentry')->captureException($e);
                }
            }

        }
    }

    public function getTableColumns($table)
    {
        return Schema::getColumnListing($table);
    }

    public function getNullableColumns($columnName, $model)
    {
        $DB = Config::get('database.tenant_conn.database');

        try {
            $table = $model->table;
            $results = DB::select("SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE
                    FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '$DB' AND TABLE_NAME = '$table' AND COLUMN_NAME = '$columnName'   ");

            return @$results[0];
        } catch (\Exception $exception) {
            return null;
        }

    }

    public function getAutoIncrementColumns($columnName, $model)
    {
        $DB = Config::get('database.tenant_conn.database');
        try {
            $table = $model->table;
            $results = DB::select("SELECT *
                    FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = '$DB' 
                AND TABLE_NAME = '$table' 
                AND DATA_TYPE = 'int' 
                AND COLUMN_DEFAULT IS NULL
                AND IS_NULLABLE = 'NO'
                AND EXTRA like '%auto_increment%'
                AND COLUMN_NAME = '$columnName'   ");
            return !empty(@$results[0]);
        } catch (\Exception $exception) {
            return false;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
