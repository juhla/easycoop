<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewPriceBundles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('user_price_bundles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('description')->nullable();
                $table->integer('user_license_package_id');
                $table->enum('type_id', ['0', '1'])->comment('0:starter, 1:enterprise')->default(0);
                $table->string('annual_price');
                $table->timestamps();
            });

            Artisan::call('db:seed', [
                    '--class' => 'NewLicensePackageSeeder',
                    '--force' => true
                ]
            );
        } catch (Exception $ex) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_price_bundles');
    }
}
