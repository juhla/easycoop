<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create user roles table
        Schema::create('roles', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('display_name');
            $table->string('description');
            $table->tinyInteger('is_admin')->default(0);
            $table->enum('flag', ['Active', 'Inactive'])->default('Active');
            $table->timestamps();
        });

        //create user license table
        Schema::create('user_licences', function(Blueprint $table){
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->string('title');
            $table->string('no_of_users');
            $table->string('price');
            $table->string('duration');
            $table->string('duration_type');
            $table->integer('as_default')->unsigned();
            $table->integer('recommend')->unsigned();
            $table->enum('flag', ['Active', 'Inactive'])->default('Active');
            $table->timestamps();
        });

        //create users table
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('security_question');
            $table->string('security_answer');
            $table->tinyInteger('active');
            $table->integer('verified');
            $table->integer('last_active_time')->unsigned();
            $table->tinyInteger('online_status');
            $table->tinyInteger('banned');
            $table->integer('role_id')->unsigned();
            $table->integer('user_licence')->unsigned();
            $table->string('verification_code');
            $table->text('permissions');
            $table->string('profile_pic');
            $table->dateTime('last_changed_password');
            $table->dateTime('last_changed_security_question');
            $table->enum('flag', ['Active', 'Deactivated']);
            $table->enum('is_first_login', ['0', '1'])->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
        Schema::drop('users');
        Schema::drop('user_licences');
    }
}
