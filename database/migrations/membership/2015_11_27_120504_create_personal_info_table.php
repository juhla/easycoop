<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create personal info table
        Schema::create('personal_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('title', ['Mr', 'Mrs', 'Miss']);
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->enum('gender', ['Male', 'Female']);
            $table->enum('marital_status', ['Married', 'Unmarried']);
            $table->date('date_of_birth');
            $table->string('city');
            $table->integer('state')->unsigned();
            $table->integer('country')->unsigned();
            $table->string('phone_no');
            $table->text('bio');
            $table->string('interests');
            $table->string('languages');
            $table->enum('visibility', ['Only Me', 'My Connections', 'Public']);
            $table->enum('flag', ['Active', 'Inactive']);
            $table->timestamps();
        });

        //create qualification table
//        Schema::create('qualifications', function(Blueprint $table){
//            $table->increments('id');
//            $table->string('qualification');
//            $table->enum('qualification_type', ['Graduate','Certification','Post-Graduate','Diploma','Doctorate']);
//            $table->timestamps();
//        });

        //create education table
//        Schema::create('education', function(Blueprint $table){
//            $table->increments('id');
//            $table->integer('user_id')->unsigned();
//            $table->integer('qualification_id')->unsigned();
//            $table->string('qualification_reference');
//            $table->string('institution');
//            $table->string('year_obtained');
//            $table->enum('visibility', ['Only Me', 'My Connections', 'Public']);
//            $table->enum('flag', ['Active', 'Inactive']);
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('qualifications');
        //Schema::drop('education');
        Schema::drop('personal_info');
    }
}
