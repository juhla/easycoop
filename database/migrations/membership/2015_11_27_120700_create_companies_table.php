<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for industries
        Schema::create('industries', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        //create table for companies
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('company_name');
            $table->string('slug');
            $table->string('database');
            $table->integer('industry_no')->unsigned();
            $table->string('rc_number');
            $table->text('about_company');
            $table->string('address');
            $table->string('phone_no');
            $table->string('email');
            $table->string('website');
            $table->string('city');
            $table->integer('state')->unsigned();
            $table->integer('country')->unsigned();
            $table->integer('date_incorporated');
            $table->string('no_of_staff');
            $table->enum('visibility', ['Only Me', 'My Connections', 'Public']);
            $table->enum('flag', ['Active', 'Inactive']);
            $table->timestamps();
        });

      /*  Schema::create('msme_users', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->integer('client_id');
            $table->tinyInteger('visibility');
            $table->tinyInteger('flag');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('industries');
        //Schema::drop('msme_users');
        Schema::drop('companies');
    }
}
