<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create contries table
        Schema::create('countries', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('country_code_long')->nullable();
            $table->string('country_code')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('currency_name')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('country_flag')->nullable();
        });

        //create states table
        Schema::create('states', function(Blueprint $table){
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->string('name');
        });

        //create contact info table
        Schema::create('contact_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('address');
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->string('city');
            $table->string('mobile_no');
            $table->string('email');
            $table->enum('visibility', ['Only Me', 'My Connections', 'Public']);
            $table->enum('flag', ['Active', 'Inactive']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_info');
        Schema::drop('countries');
        Schema::drop('states');
    }
}
