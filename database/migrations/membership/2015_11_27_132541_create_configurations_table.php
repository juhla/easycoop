<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->text('value');
            $table->timestamps();
        });

        //create notification settings table
        Schema::create('notifications_settings', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('new_message', ['On', 'Off'])->default('On');
            $table->enum('new_post', ['Yes', 'No'])->default('Yes');
            $table->enum('new_comment', ['Yes', 'No'])->default('Yes');
            $table->enum('upcoming_events', ['Yes', 'No'])->default('Yes');
            $table->enum('upcoming_tasks', ['Yes', 'No'])->default('Yes');
            $table->enum('changed_password', ['Yes', 'No'])->default('Yes');
            $table->enum('on_login', ['Yes', 'No'])->default('Yes');
            $table->enum('connection_request', ['Yes', 'No'])->default('Yes');
            $table->enum('news_feed', ['Yes', 'No'])->default('Yes');
            $table->enum('settings', ['Yes', 'No'])->default('Yes');
            $table->enum('profile_update', ['Yes', 'No'])->default('Yes');
            $table->enum('rank_update', ['Yes', 'No'])->default('Yes');
            $table->enum('when_tagged', ['Yes', 'No'])->default('Yes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configurations');
        Schema::drop('notifications_settings');
    }
}
