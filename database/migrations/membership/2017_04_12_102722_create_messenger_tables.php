<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessengerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->string('message');
            $table->integer('status')->comment('0=unseen, 1=seen');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('message_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('message_id');
            $table->integer('sender_id')->unsigned();
            $table->integer('recipient_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('message');
        Schema::drop('message_ids');
    }
}
