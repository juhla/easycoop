<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            ['bank_name' => 'Access Bank Plc'],
            ['bank_name' => 'Citibank Nigeria Limited'],
            ['bank_name' => 'Diamond Bank Limited'],
            ['bank_name' => 'Ecobank Nigeria Plc'],
            ['bank_name' => 'Enterprise Trust Bank Limited'],
            ['bank_name' => 'Fidelity Bank Plc'],
            ['bank_name' => 'First Bank of Nigeria Plc'],
            ['bank_name' => 'First City Monument Bank Limited'],
            ['bank_name' => 'Guaranty Trust Bank Plc.'],
            ['bank_name' => 'Heritage Banking Company Ltd'],
            ['bank_name' => 'Key Stone Bank'],
            ['bank_name' => 'MainStreet Bank'],
            ['bank_name' => 'Skye Bank Plc'],
            ['bank_name' => 'Stanbic IBTC Bank Ltd'],
            ['bank_name' => 'Standard Chartered Bank Nigeria Ltd'],
            ['bank_name' => 'Sterling Bank Plc'],
            ['bank_name' => 'Union Bank of Nigeria Plc'],
            ['bank_name' => 'United Bank For Africa Plc'],
            ['bank_name' => 'Unity Bank Plc'],
            ['bank_name' => 'Wema Bank Plc'],
            ['bank_name' => 'Zenith Bank Plc']
        ]);
    }
}
