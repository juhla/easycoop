<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BmacPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            [
                "category" => "Receipt",
                "name" => "add_receipt",
                "display_name" => "Add Receipt",
                "module" => "bmac",
            ],
            [
                "category" => "Receipt",
                "name" => "view_receipt",
                "display_name" => "View Receipt",
                "module" => "bmac",
            ],
            [
                "category" => "Receipt",
                "name" => "edit_receipt",
                "display_name" => "Edit Receipt",
                "module" => "bmac",
            ],
            [
                "category" => "Receipt",
                "name" => "delete_receipt",
                "display_name" => "Delete Receipt",
                "module" => "bmac",
            ],
            [
                "category" => "Payment",
                "name" => "add_payment",
                "display_name" => "Add Payment",
                "module" => "bmac",
            ],
            [
                "category" => "Payment",
                "name" => "view_payment",
                "display_name" => "View Payment",
                "module" => "bmac",
            ],
            [
                "category" => "Payment",
                "name" => "edit_payment",
                "display_name" => "Edit Payment",
                "module" => "bmac",
            ],
            [
                "category" => "Payment",
                "name" => "delete_payment",
                "display_name" => "Delete Payment",
                "module" => "bmac",
            ],
            [
                "category" => "Journal-Entry",
                "name" => "add_journal_entry",
                "display_name" => "Add journal Entry",
                "module" => "bmac",
            ],
            [
                "category" => "Journal-Entry",
                "name" => "view_journal_entry",
                "display_name" => "View Journal Entry",
                "module" => "bmac",
            ],
            [
                "category" => "Journal-Entry",
                "name" => "edit_journal_entry",
                "display_name" => "Edit Journal Entry",
                "module" => "bmac",
            ],
            [
                "category" => "Journal-Entry",
                "name" => "delete_journal_entry",
                "display_name" => "Delete Journal Entry",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "add_invoice",
                "display_name" => "Add Invoice",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "view_invoice",
                "display_name" => "View Invoice",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "edit_invoice",
                "display_name" => "Edit Invoice",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "delete_invoice",
                "display_name" => "Delete Invoice",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "chart_of_account",
                "display_name" => "Chart of Account",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "fiscal_year",
                "display_name" => "Fiscal Year",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "customer",
                "display_name" => "Customer",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "vendor",
                "display_name" => "Vendor",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "Bank_Account",
                "display_name" => "Bank Account",
                "module" => "bmac",
            ],
            [
                "category" => "Business-Setup",
                "name" => "Payment-Types",
                "display_name" => "Payment Type",
                "module" => "bmac",
            ],
            [
                "category" => "Quickview",
                "name" => "view-quickview",
                "display_name" => "View Quickview",
                "module" => "bmac",
            ],
            [
                "category" => "Quickview",
                "name" => "Print-quickview",
                "display_name" => "Print Quickview",
                "module" => "bmac",
            ],
            [
                "category" => "IMS",
                "name" => "ims",
                "display_name" => "IMS",
                "module" => "bmac",
            ],
            [
                "category" => "HRM",
                "name" => "payroll",
                "display_name" => "HRM",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "print_invoice",
                "display_name" => "Print Invoice",
                "module" => "bmac",
            ],
            [
                "category" => "Invoice",
                "name" => "send_email",
                "display_name" => "Send Email",
                "module" => "bmac",
            ]
        ];
        try {
            DB::connection('tenant_conn')->table('permissions')->insert($array); //->insert($array);
            // dd(DB::table('permissions'));
        } catch (\Exception $e) {
            // app('sentry')->captureException($e);
            throw $e;
        }
    }
}
