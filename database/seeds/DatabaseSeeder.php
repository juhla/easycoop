<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserRolesSeeder::class);
        $this->call(UserLicenceTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(WidgetDashboardRolesTableSeeder::class);
        $this->call(WidgetDashboardsTableSeeder::class);
        $this->call(WidgetRolesTableSeeder::class);
        $this->call(WidgetsTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
    }
}
