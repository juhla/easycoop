<?php

use Illuminate\Database\Seeder;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default days of the week
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '1', 'day' => 'Sunday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '2', 'day' => 'Monday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '3', 'day' => 'Tueday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '4', 'day' => 'Wednesday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '5', 'day' => 'Thursday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '6', 'day' => 'Friday']);
        \App\Modules\Base\Models\Payroll\Settings\DaysofWeek::create(['day_id' => '7', 'day' => 'Saturday']);

       //create default working days
        \App\Modules\Base\Models\Payroll\Settings\WorkingDay::create(['working_days_id' => '15', 'day_id' => '1', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '16', 'day_id' => '2', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '17', 'day_id' => '3', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '18', 'day_id' => '4', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '19', 'day_id' => '5', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '20', 'day_id' => '6', 'flag' => '0']);
        \App\Modules\Base\Models\Payroll\Settings\Workingday::create(['working_days_id' => '21', 'day_id' => '7', 'flag' => '0']);

       //create default leave category
        \App\Modules\Base\Models\Payroll\Settings\LeaveCategory::create(['leave_category_id' => '2', 'category' => 'Sick']);
        \App\Modules\Base\Models\Payroll\Settings\LeaveCategory::create(['leave_category_id' => '3', 'category' => 'Wedding']);
        \App\Modules\Base\Models\Payroll\Settings\LeaveCategory::create(['leave_category_id' => '4', 'category' => 'Work']);

         //create default values for tax settings
        \App\Modules\Base\Models\Payroll\Settings\TaxSetting::create(['consolidated_relief' => '1', 'consolidated_relief_cap' => '200000', 'gross_income_relief' => '20', 'tax_rate1' => '7', 'tax_rate2' => '11', 'tax_rate3' => '15', 'tax_rate4' => '19', 'tax_rate5' => '21', 'tax_rate6' => '24', 'tax_value1' => '25000', 'tax_value2' => '25000', 'tax_value3' => '41666', 'tax_value4' => '41666', 'tax_value5' => '133333', 'tax_value6' => '266666', 'pension' => '8', 'housing_fund' => '2.5', 'health_insurance' => '5']);
    }
}
