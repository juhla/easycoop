<?php

use Illuminate\Database\Seeder;

class ImsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ImsComponent::class);
        //$this->call(InvoiceSettingsSeeder::class);
        //$this->call(TaxTypesSeeder::class);
    }
}
