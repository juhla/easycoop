<?php

use Illuminate\Database\Seeder;

class MsmeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ChartOfAccountSeeder::class);
        //$this->call(InvoiceSettingsSeeder::class);
        //$this->call(TaxTypesSeeder::class);
    }
}
