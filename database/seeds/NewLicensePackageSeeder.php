<?php

use Illuminate\Database\Seeder;

class NewLicensePackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            [
                'role_id' => 4,
                'title' => 'BMAC Starter',
                'slug' => 'bmac-starter',
                'no_of_users' => '5',
                'is_default' => 0,
                'description' => "BMAC Accounting 3 users + Bonus 2 Users Free (30 days free trial) ",
                'price' => 30000,
                'duration' => 1,
                'duration_type' => 'year',
                'status' => 1,
                'annual_price' => 0,
            ],
            [
                'role_id' => 4,
                'title' => 'IMS Web Starter',
                'slug' => 'ims-web-starter',
                'no_of_users' => '1000',
                'is_default' => 0,
                'description' => "BMAC IMS-Web",
                'price' => 50000,
                'duration' => 1,
                'duration_type' => 'year',
                'status' => 1,
                'annual_price' => 0,
            ],
            [
                'role_id' => 4,
                'title' => 'BMAC IMS Web & Desktop Starter',
                'slug' => 'bmac-ims-web-desktop-starter',
                'no_of_users' => '1000',
                'is_default' => 0,
                'description' => "BMAC IMS-Web & Desktop",
                'price' => 80000,
                'duration' => 1,
                'duration_type' => 'year',
                'status' => 1,
                'annual_price' => 0,
            ],
            [
                'role_id' => 4,
                'title' => 'BAMC HR Payroll Starter',
                'slug' => 'bmac-hr-payroll-starter',
                'no_of_users' => '30',
                'is_default' => 0,
                'description' => "BMAC HR Payroll (Up to 30 Employees)",
                'price' => 60000,
                'duration' => 1,
                'duration_type' => 'year',
                'status' => 1,
                'annual_price' => 0,
            ],
        ];
        foreach ($array as $key => $value) {
            try {
                $res = \App\Modules\Base\Models\UserLicense::create($value);
                if ($key == 0) {
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Basic", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 15000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Silver", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 30000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Gold", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 45000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Diamond", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 50000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Platinum", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 70000]);
                } else if ($key == 1) {
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Silver", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 30000]);
                } else if ($key == 2) {
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Gold", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 45000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Platinum", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 70000]);
                } else if ($key == 3) {
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Diamond", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 50000]);
                    \App\Modules\Base\Models\UserPriceBundle::create(['name' => "Platinum", 'user_license_package_id' => $res->id, 'type_id' => "0", 'annual_price' => 70000]);
                }

            } catch (\Exception $e) {
                echo "\n";
                echo $e->getMessage();
                echo "\n";
            }
        }

    }
}
