<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $array = [
                [
                    "category" => "branch",
                    "name" => "create_branch",
                    "display_name" => "Create Branch",
                    "module" => "ims",
                ],
                [
                    "category" => "branch",
                    "name" => "edit_branch",
                    "display_name" => "Edit Branch",
                    "module" => "ims",
                ],
                [
                    "category" => "branch",
                    "name" => "view_branch",
                    "display_name" => "View Branch",
                    "module" => "ims",
                ],
                [
                    "category" => "branch",
                    "name" => "delete_branch",
                    "display_name" => "Delete Branch",
                    "module" => "ims",
                ],
                [
                    "category" => "department",
                    "name" => "create_department",
                    "display_name" => "Create Department",
                    "module" => "ims",
                ],
                [
                    "category" => "department",
                    "name" => "edit_department",
                    "display_name" => "Edit Department",
                    "module" => "ims",
                ],
                [
                    "category" => "department",
                    "name" => "view_department",
                    "display_name" => "View Department",
                    "module" => "ims",
                ],
                [
                    "category" => "department",
                    "name" => "delete_department",
                    "display_name" => "Delete Department",
                    "module" => "ims",
                ],
                [
                    "category" => "designation",
                    "name" => "create_designation",
                    "display_name" => "Create Designation",
                    "module" => "ims",
                ],
                [
                    "category" => "designation",
                    "name" => "edit_designation",
                    "display_name" => "Edit Designation",
                    "module" => "ims",
                ],
                [
                    "category" => "designation",
                    "name" => "view_designation",
                    "display_name" => "View Designation",
                    "module" => "ims",
                ],
                [
                    "category" => "designation",
                    "name" => "delete_designation",
                    "display_name" => "Delete Designation",
                    "module" => "ims",
                ],
                [
                    "category" => "location",
                    "name" => "create_location",
                    "display_name" => "Create Location",
                    "module" => "ims",
                ],
                [
                    "category" => "location",
                    "name" => "edit_location",
                    "display_name" => "Edit Location",
                    "module" => "ims",
                ],
                [
                    "category" => "location",
                    "name" => "view_location",
                    "display_name" => "View Location",
                    "module" => "ims",
                ],
                [
                    "category" => "location",
                    "name" => "delete_location",
                    "display_name" => "Delete Location",
                    "module" => "ims",
                ],
                [
                    "category" => "purpose",
                    "name" => "create_purpose",
                    "display_name" => "Create Purpose",
                    "module" => "ims",
                ],
                [
                    "category" => "purpose",
                    "name" => "edit_purpose",
                    "display_name" => "Edit Purpose",
                    "module" => "ims",
                ],
                [
                    "category" => "purpose",
                    "name" => "view_purpose",
                    "display_name" => "View Purpose",
                    "module" => "ims",
                ],
                [
                    "category" => "purpose",
                    "name" => "delete_purpose",
                    "display_name" => "Delete Purpose",
                    "module" => "ims",
                ],
                [
                    "category" => "unit_of_measurement",
                    "name" => "create_unit_of_measurement",
                    "display_name" => "Create unit of measurement",
                    "module" => "ims",
                ],
                [
                    "category" => "unit_of_measurement",
                    "name" => "edit_unit_of_measurement",
                    "display_name" => "Edit unit of measurement",
                    "module" => "ims",
                ],
                [
                    "category" => "unit_of_measurement",
                    "name" => "view_unit_of_measurement",
                    "display_name" => "View unit of measurement",
                    "module" => "ims",
                ],
                [
                    "category" => "unit_of_measurement",
                    "name" => "delete_unit_of_measurement",
                    "display_name" => "Delete unit of measurement",
                    "module" => "ims",
                ],
                [
                    "category" => "currency",
                    "name" => "create_currency",
                    "display_name" => "Create currency",
                    "module" => "ims",
                ],
                [
                    "category" => "currency",
                    "name" => "edit_currency",
                    "display_name" => "Edit currency",
                    "module" => "ims",
                ],
                [
                    "category" => "currency",
                    "name" => "view_currency",
                    "display_name" => "View currency",
                    "module" => "ims",
                ],
                [
                    "category" => "currency",
                    "name" => "delete_currency",
                    "display_name" => "Delete currency",
                    "module" => "ims",
                ],
                [
                    "category" => "tax",
                    "name" => "create_tax",
                    "display_name" => "Create Tax",
                    "module" => "ims",
                ],
                [
                    "category" => "tax",
                    "name" => "edit_tax",
                    "display_name" => "Edit Tax",
                    "module" => "ims",
                ],
                [
                    "category" => "tax",
                    "name" => "view_tax",
                    "display_name" => "View Tax",
                    "module" => "ims",
                ],
                [
                    "category" => "tax",
                    "name" => "delete_tax",
                    "display_name" => "Delete Tax",
                    "module" => "ims",
                ],
                [
                    "category" => "discount",
                    "name" => "create_discount",
                    "display_name" => "Create Discount",
                    "module" => "ims",
                ],
                [
                    "category" => "discount",
                    "name" => "edit_discount",
                    "display_name" => "Edit Discount",
                    "module" => "ims",
                ],
                [
                    "category" => "discount",
                    "name" => "view_discount",
                    "display_name" => "View Discount",
                    "module" => "ims",
                ],
                [
                    "category" => "discount",
                    "name" => "delete_discount",
                    "display_name" => "Delete Discount",
                    "module" => "ims",
                ],
                [
                    "category" => "category",
                    "name" => "create_category",
                    "display_name" => "Create Category",
                    "module" => "ims",
                ],
                [
                    "category" => "category",
                    "name" => "edit_category",
                    "display_name" => "Edit Category",
                    "module" => "ims",
                ],
                [
                    "category" => "category",
                    "name" => "view_category",
                    "display_name" => "View Category",
                    "module" => "ims",
                ],
                [
                    "category" => "category",
                    "name" => "delete_category",
                    "display_name" => "Delete Category",
                    "module" => "ims",
                ],
                [
                    "category" => "product",
                    "name" => "create_product",
                    "display_name" => "Create Product",
                    "module" => "ims",
                ],
                [
                    "category" => "product",
                    "name" => "edit_product",
                    "display_name" => "Edit Product",
                    "module" => "ims",
                ],
                [
                    "category" => "product",
                    "name" => "view_product",
                    "display_name" => "View Product",
                    "module" => "ims",
                ],
                [
                    "category" => "product",
                    "name" => "delete_product",
                    "display_name" => "Delete Product",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock",
                    "name" => "create_product_stock",
                    "display_name" => "Create Product Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock",
                    "name" => "edit_product_stock",
                    "display_name" => "Edit Product Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock",
                    "name" => "view_product_stock",
                    "display_name" => "View Product Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock",
                    "name" => "delete_product_stock",
                    "display_name" => "Delete Product Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock_locaton",
                    "name" => "create_product_stock_location",
                    "display_name" => "Create Product Stock Location",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock_locaton",
                    "name" => "edit_product_stock_location",
                    "display_name" => "Edit Product Stock Location",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock_locaton",
                    "name" => "view_product_stock_location",
                    "display_name" => "View Product Stock Location",
                    "module" => "ims",
                ],
                [
                    "category" => "product_stock_locaton",
                    "name" => "delete_product_stock_location",
                    "display_name" => "Delete Product Stock Location",
                    "module" => "ims",
                ],
                [
                    "category" => "variant",
                    "name" => "create_variant",
                    "display_name" => "Create Variant",
                    "module" => "ims",
                ],
                [
                    "category" => "variant",
                    "name" => "edit_variant",
                    "display_name" => "Edit Variant",
                    "module" => "ims",
                ],
                [
                    "category" => "variant",
                    "name" => "view_variant",
                    "display_name" => "View Variant",
                    "module" => "ims",
                ],
                [
                    "category" => "variant",
                    "name" => "delete_variant",
                    "display_name" => "Delete Variant",
                    "module" => "ims",
                ],
                [
                    "category" => "stock_requisition",
                    "name" => "create_stock_requisition",
                    "display_name" => "Create Stock Requisition",
                    "module" => "ims",
                ],
                [
                    "category" => "stock_requisition",
                    "name" => "edit_stock_requisition",
                    "display_name" => "Edit Stock Requisition",
                    "module" => "ims",
                ],
                [
                    "category" => "stock_requisition",
                    "name" => "view_stock_requisition",
                    "display_name" => "View Stock Requisition",
                    "module" => "ims",
                ],
                [
                    "category" => "stock_requisition",
                    "name" => "delete_stock_requisition",
                    "display_name" => "Delete Stock Requisition",
                    "module" => "ims",
                ],
                [
                    "category" => "issue_stock",
                    "name" => "create_issue_stock",
                    "display_name" => "Create Issue Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "issue_stock",
                    "name" => "edit_issue_stock",
                    "display_name" => "Edit Issue Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "issue_stock",
                    "name" => "view_issue_stock",
                    "display_name" => "View Issue Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "issue_stock",
                    "name" => "delete_issue_stock",
                    "display_name" => "Delete Issue Stock",
                    "module" => "ims",
                ],
                [
                    "category" => "adjustment_journal",
                    "name" => "create_adjustment_journal",
                    "display_name" => "Create Adjustment Journal",
                    "module" => "ims",
                ],
                [
                    "category" => "adjustment_journal",
                    "name" => "edit_adjustment_journal",
                    "display_name" => "Edit Adjustment Journal",
                    "module" => "ims",
                ],
                [
                    "category" => "adjustment_journal",
                    "name" => "view_adjustment_journal",
                    "display_name" => "View Adjustment Journal",
                    "module" => "ims",
                ],
                [
                    "category" => "adjustment_journal",
                    "name" => "delete_adjustment_journal",
                    "display_name" => "Delete Adjustment Journal",
                    "module" => "ims",
                ],
                [
                    "category" => "sales",
                    "name" => "create_sales",
                    "display_name" => "Create Sales",
                    "module" => "ims",
                ],
                [
                    "category" => "sales",
                    "name" => "edit_sales",
                    "display_name" => "Edit Sales",
                    "module" => "ims",
                ],
                [
                    "category" => "sales",
                    "name" => "view_sales",
                    "display_name" => "View Sales",
                    "module" => "ims",
                ],
                [
                    "category" => "sales",
                    "name" => "delete_sales",
                    "display_name" => "Delete Sales",
                    "module" => "ims",
                ],
                [
                    "category" => "purchase_order",
                    "name" => "create_purchase_order",
                    "display_name" => "Create Purchase Order",
                    "module" => "ims",
                ],
                [
                    "category" => "purchase_order",
                    "name" => "edit_purchase_order",
                    "display_name" => "Edit Purchase Order",
                    "module" => "ims",
                ],
                [
                    "category" => "purchase_order",
                    "name" => "view_purchase_order",
                    "display_name" => "View Purchase Order",
                    "module" => "ims",
                ],
                [
                    "category" => "purchase_order",
                    "name" => "delete_purchase_order",
                    "display_name" => "Delete Purchase Order",
                    "module" => "ims",
                ]
        ];
        try {
            DB::connection('tenant_conn')->table('permissions')->insert($array); //->insert($array);
            // dd(DB::table('permissions'));
        } catch (\Exception $e) {
            // app('sentry')->captureException($e);
            throw $e;
        }
    }
}
