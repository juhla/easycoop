<?php

use Illuminate\Database\Seeder;

class UserLicenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $arry = [
            [
                'role_id' => 2,
                'title' => 'Starter',
                'no_of_users' => 'Starter',
                'as_default' => 1,
                'recommend' => 0,
            ],
            [
                'role_id' => 4,
                'title' => 'Social Member License',
                'no_of_users' => '',
                'as_default' => 1,
                'recommend' => 0,
            ],
            [
                'role_id' => 5,
                'title' => 'Financial Provider License',
                'no_of_users' => '',
                'as_default' => 1,
                'recommend' => 0,
            ],
            [
                'role_id' => 6,
                'title' => 'Professional License',
                'no_of_users' => '',
                'as_default' => 1,
                'recommend' => 0,
            ],
            [
                'role_id' => 7,
                'title' => 'Trade Promoter License',
                'no_of_users' => '',
                'as_default' => 1,
                'recommend' => 0,
            ]
        ];
        foreach ( $arry as $k ) {
            try {
                \App\Modules\Base\Models\UserLicence::create($k);
            } catch (\Exception $e) {
                echo "\n";
                echo $e->getMessage();
                echo "\n";
            }
        }
    }
}
