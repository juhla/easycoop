<?php

use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        //create membership roles
        $arry = [
            [ 'id' => 1, 'name' => 'admin', 'display_name' => 'Admin', 'is_admin' => 1 ],
            [ 'id' => 2, 'name' => 'msme_owner', 'display_name' => 'Business Owner'],
            [ 'id' => 3, 'name' => 'employee', 'display_name' => 'Employee'],
            [ 'id' => 4, 'name' => 'social_member', 'display_name' => 'Social Member'],
            [ 'id' => 5, 'name' => 'financial_provider', 'display_name' => 'Financial Provider'],
            [ 'id' => 6, 'name' => 'professional', 'display_name' => 'Professional'],
            [ 'id' => 7, 'name' => 'trade_promoter', 'display_name' => 'Trade Promoter']
        ];
        foreach ( $arry as $k ) {
            try {
                \App\Modules\Base\Models\Role::create($k);
            } catch (\Exception $e) {
                echo "\n";
                echo $e->getMessage();
                echo "\n";
            }
        }

    }
}
