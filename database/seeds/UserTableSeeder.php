<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create membership roles
        \App\Modules\Base\Models\Role::create(['name' => 'admin', 'display_name' => 'Admin', 'is_admin' => 1]);
        \App\Modules\Base\Models\Role::create(['name' => 'msme_owner', 'display_name' => 'Msme Owner']);
        \App\Modules\Base\Models\Role::create(['name' => 'employee', 'display_name' => 'Employee']);
        \App\Modules\Base\Models\Role::create(['name' => 'social_member', 'display_name' => 'Social Member']);
        \App\Modules\Base\Models\Role::create(['name' => 'financial_provider', 'display_name' => 'Financial Provider']);
        \App\Modules\Base\Models\Role::create(['name' => 'professional', 'display_name' => 'Professional']);


        //create default admin user
        $user = \App\Modules\Base\Models\User::create([
            'email' => 'admin@mail.com',
            'password' => bcrypt('password'),
            'role_id' => '1',
            'active' => '1',
            'verified' => 1
        ]);
        \App\Modules\Base\Models\PersonalInfo::create([
            'first_name' => 'Admin',
            'last_name' => 'Administrator',
            'user_id' => $user->id
        ]);
    }
}
