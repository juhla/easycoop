/*
 Navicat Premium Data Transfer

 Source Server         : mysql_conn
 Source Server Type    : MySQL
 Source Server Version : 50709
 Source Host           : localhost
 Source Database       : ikooba_max_and_steel

 Target Server Type    : MySQL
 Target Server Version : 50709
 File Encoding         : utf-8

 Date: 07/04/2016 13:10:39 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ca_bank_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `ca_bank_accounts`;
CREATE TABLE `ca_bank_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ledger_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_type` enum('Savings Account','Chequing Account','Current Account','Cash Account') COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_id` int(10) unsigned NOT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` tinyint(4) DEFAULT NULL,
  `last_reconciled_date` date DEFAULT NULL,
  `ending_reconciled_balance` double DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_bank_accounts`
-- ----------------------------
BEGIN;
INSERT INTO `ca_bank_accounts` VALUES ('1', '1244', 'fdas fdasfd', '3423423', null, '7', null, null, null, null, 'Active', '2016-07-02 04:20:01', '2016-07-02 04:20:01');
COMMIT;

-- ----------------------------
--  Table structure for `ca_customers`
-- ----------------------------
DROP TABLE IF EXISTS `ca_customers`;
CREATE TABLE `ca_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ledger_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_customers`
-- ----------------------------
BEGIN;
INSERT INTO `ca_customers` VALUES ('1', '1151.01', 'Sample Name', 'asdfsd@asdf.com', 'sdfasdf sd', 'asdfsd', 'NGN', null, '324324', null, '166', '232 asdf def pdf as', 'Active', '2016-07-01 10:45:32', '2016-07-02 10:41:53'), ('2', '1151.02', 'asdfadsfas', 'adsfsd@ads.com', 'asdfasfs', 'asdfdsf', 'USD', null, '342342', null, '12', 'asdf sdfs', 'Active', '2016-07-01 15:08:09', '2016-07-01 15:08:09'), ('3', '1151.03', 'asdfdsfds', 'adsffasd@dafs.com', 'asd ads ', 'adsfaf ', 'DZD', null, '', null, '63', 'dfa fdsf ', 'Active', '2016-07-02 04:14:20', '2016-07-02 04:14:20'), ('4', '1151.04', 'a dafdasfasd', 'asdfasd@asd.com', 'asdf dsf', 'adsfsad', 'ALL', null, '32432432', null, '6', 'adsf sdf df', 'Active', '2016-07-02 04:15:58', '2016-07-02 04:15:58'), ('5', '1151.05', 'Academic Certificates', 'dsaf@asdf.com', 'asdf ds ', 'asdfasd ', 'BBD', null, '423423432', null, '19', 'asdf asdfasdfas', 'Active', '2016-07-02 04:32:43', '2016-07-02 04:32:43');
COMMIT;

-- ----------------------------
--  Table structure for `ca_fiscal_year`
-- ----------------------------
DROP TABLE IF EXISTS `ca_fiscal_year`;
CREATE TABLE `ca_fiscal_year` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `begins` date NOT NULL,
  `ends` date NOT NULL,
  `status` enum('Open','Closed') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Open',
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_fiscal_year`
-- ----------------------------
BEGIN;
INSERT INTO `ca_fiscal_year` VALUES ('1', '2016-01-01', '2016-12-01', 'Open', 'Active', '2016-07-02 04:04:27', '2016-07-02 04:04:27');
COMMIT;

-- ----------------------------
--  Table structure for `ca_groups`
-- ----------------------------
DROP TABLE IF EXISTS `ca_groups`;
CREATE TABLE `ca_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `is_system_account` tinyint(4) NOT NULL DEFAULT '0',
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_groups`
-- ----------------------------
BEGIN;
INSERT INTO `ca_groups` VALUES ('1', 'Assets', '1000', '0', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('2', 'Liabilities', '2000', '0', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('3', 'Equity', '3000', '0', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('4', 'Revenue', '4000', '0', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('5', 'Cost (Expenses)', '5000', '0', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('6', 'Non-Current Assets', '1100', '1', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('7', 'Property, Plant & Equipment', '1110', '6', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('8', 'Investment Property', '1120', '6', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('9', 'Current Assets', '1200', '1', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('10', 'Inventory', '1210', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-02 10:49:48'), ('11', 'Account Receivables', '1220', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('12', 'Receivables - Trade', '1221', '11', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('13', 'Receivables - Staff', '1222', '11', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('14', 'Receivables - Inter-Company', '1223', '11', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('15', 'Short Term Investment', '1230', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('16', 'Bank Balances', '1240', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('17', 'Dom Account Balances', '1250', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('18', 'Cash', '1260', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('19', 'Prepayments', '1270', '9', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('20', 'Accumulated Depreciation', '1300', '1', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('21', 'Property, Plant & Equipment', '1310', '20', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('22', 'Investment Property', '1320', '20', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('23', 'Non-current Liabilities', '2100', '2', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('24', 'Long Term Borrowing', '2110', '23', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('25', 'Deferred Tax', '2120', '23', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('26', 'Long Term Provisions', '2130', '23', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('27', 'Current Liabilities', '2200', '2', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('28', 'Payable - Trade', '2210', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('29', 'Payable - Staff', '2220', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('30', 'Payable - Inter-Company', '2230', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('31', 'Short Term Borrowing', '2240', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('32', 'Current Portion of Long Term Borrowing', '2250', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('33', 'Short Term Provisions', '2270', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('34', 'Accruals', '2300', '2', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('35', 'Income', '4100', '4', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('36', 'Other Income', '4200', '4', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('37', 'Direct Cost', '5100', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('38', 'Admin and Overhead', '5200', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('39', 'Provision for Depreciation', '5201', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('40', 'Marketing and Selling Expenses', '5300', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('41', 'Financial Expenses', '5400', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43');
COMMIT;

-- ----------------------------
--  Table structure for `ca_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `ca_invoice`;
CREATE TABLE `ca_invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `invoice_type` enum('sales','purchases') COLLATE utf8_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `due_date` date NOT NULL,
  `amount_due` double NOT NULL,
  `total_amount` double NOT NULL,
  `amount_paid` double DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_term_id` int(10) unsigned DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_invoice`
-- ----------------------------
BEGIN;
INSERT INTO `ca_invoice` VALUES ('2', 'INV1467373790', '1', 'sales', '2016-01-07', '2016-01-07', '400', '400', null, 'a dsfasdfd', null, null, '0', null, null, 'Active', '2016-07-01 11:49:50', '2016-07-01 11:49:50'), ('3', 'INV1467433706', '3', 'sales', '2016-02-07', '2016-02-07', '400', '400', null, 'asdf asdf asdfasdf', null, 'BHD', '0', null, null, 'Active', '2016-07-02 04:28:26', '2016-07-02 04:28:26');
COMMIT;

-- ----------------------------
--  Table structure for `ca_invoice_items`
-- ----------------------------
DROP TABLE IF EXISTS `ca_invoice_items`;
CREATE TABLE `ca_invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `amount` double NOT NULL,
  `tax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_invoice_items`
-- ----------------------------
BEGIN;
INSERT INTO `ca_invoice_items` VALUES ('1', '2', '1', 'dasf adsf asdfa dsf', '1', '400', '400', null, 'Active', '2016-07-01 11:49:50', '2016-07-01 11:49:50'), ('2', '3', '1', 'dasf adsf asdfa dsf', '1', '400', '400', null, 'Active', '2016-07-02 04:28:27', '2016-07-02 04:28:27');
COMMIT;

-- ----------------------------
--  Table structure for `ca_ledgers`
-- ----------------------------
DROP TABLE IF EXISTS `ca_ledgers`;
CREATE TABLE `ca_ledgers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `opening_balance` double DEFAULT NULL,
  `opening_balance_type` enum('D','C') COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `is_system_account` tinyint(4) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_ledgers`
-- ----------------------------
BEGIN;
INSERT INTO `ca_ledgers` VALUES ('1', 'Office Equipment', '1110.1', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('2', 'Furniture and Fixtures', '1110.2', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('3', 'Motor Vehicles', '1110.3', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('4', 'Building', '1110.4', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('5', 'Land', '1110.5', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('6', 'Computer Equipment', '1110.6', null, 'D', '7', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('7', 'Building', '1120.1', null, 'D', '8', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('8', 'Land', '1120.2', null, 'D', '8', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('9', 'Raw Materials', '1211', null, 'D', '10', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('10', 'WIP', '1212', null, 'D', '10', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('11', 'Finished Goods', '1213', null, 'D', '10', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('12', 'General Stock', '1214', null, 'D', '10', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('13', 'Investment Account - A', '1231', null, 'D', '15', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('14', 'Investment Account - B', '1232', null, 'D', '15', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('15', 'Investment Account - C', '1233', null, 'D', '15', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('16', 'Naira Bank Account - A', '1241', null, 'D', '16', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('17', 'Naira Bank Account - B', '1242', null, 'D', '16', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('18', 'Naira Bank Account - C', '1243', null, 'D', '16', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('19', 'Dom Bank Account - A', '1251', null, 'D', '17', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('20', 'Dom Bank Account - B', '1252', null, 'D', '17', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('21', 'Cash in Hand (Head Office)', '1261', null, 'D', '18', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('22', 'Cash in Hand (Branch Offices)', '1262', null, 'D', '18', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('23', 'Rent', '1271', null, 'D', '19', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('24', 'Others', '1272', null, 'D', '19', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('25', 'Accum. Dep - Office Equipment', '1310.1', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('26', 'Accum. Dep - Furniture and Fixtures', '1310.2', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('27', 'Accum. Dep - Motor Vehicles', '1310.3', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('28', 'Accum. Dep - Building', '1310.4', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('29', 'Accum. Dep - Computer Equipment', '1315', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('30', 'Accum. Dep - Others', '1310.5', null, 'C', '21', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('31', 'Building', '1320.1', null, 'C', '22', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('32', 'Current Tax Payables', '2260', null, 'C', '27', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('33', 'Accruals', '2310', null, 'C', '34', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('34', 'Shared Capital', '3100', null, 'C', '3', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('35', 'Deposit for Shares', '3200', null, 'C', '3', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('36', 'Retained Earnings', '3300', null, 'C', '3', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('37', 'Sales', '4110', null, 'C', '35', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('38', 'Sales Returns', '4120', null, 'D', '35', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('39', 'Finance Income', '4210', null, 'C', '36', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('40', 'Profit on Disposal of Fixed Assets', '4220', null, 'C', '36', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('41', 'Profit on Exchange Rate Conversion', '4230', null, 'C', '36', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('42', 'Purchases', '5101', null, 'D', '37', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('43', 'Return Outwards', '5102', null, 'C', '37', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('44', 'Cost of Sales', '5103', null, 'D', '37', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('45', 'Depreciation - Office Equipment', '5201.1', null, 'D', '39', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('46', 'Depreciation - Furniture and Fixtures', '5201.2', null, 'D', '39', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('47', 'Depreciation - Motor Vehicles', '5201.3', null, 'D', '39', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('48', 'Depreciation - Building', '5201.4', null, 'D', '39', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('49', 'Depreciation - Others', '5201.5', null, 'D', '39', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('50', 'Salaries and Wages', '5202', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('51', 'Repairs and Maintenance', '5203', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('52', 'Rent', '5204', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('53', 'Legal Expenses', '5205', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('54', 'Communication', '5206', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('55', 'Travelling', '5207', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('56', 'Training', '5208', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('57', 'Government Dues and Levies', '5209', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('58', 'Professional Dues', '5210', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('59', 'Others', '5211', null, 'D', '38', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('60', 'Salesmen\'s Salary', '5301', null, 'D', '40', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('61', 'Salesmen\'s Communication', '5302', null, 'D', '40', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('62', 'Advertising', '5303', null, 'D', '40', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('63', 'Sale\'s Traveling Expenses', '5304', null, 'D', '40', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('64', 'Branch and Store Expenses', '5305', null, 'D', '40', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('65', 'Bank Interest', '5401', null, 'D', '41', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('66', 'Bank Charges', '5402', null, 'D', '41', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('67', 'Other Financial Expenses', '5403', null, 'D', '41', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('68', 'Bad Debt', '5404', null, 'D', '41', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('69', 'Provision for Doubtful Debt', '5500', null, 'D', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('70', 'Loss on Disposal of Fixed Assets', '5600', null, 'D', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('71', 'Loss on Foreign Exchange Conversion', '5700', null, 'D', '5', '1', 'Active', '2016-07-01 09:16:43', '2016-07-01 09:16:43'), ('72', 'asdf df pdf pdf', '1151.01', null, 'C', '12', null, 'Active', '2016-07-01 10:45:32', '2016-07-01 10:45:32'), ('73', 'Test Grop', '1400', '0', 'D', '1', null, 'Inactive', '2016-07-01 11:54:03', '2016-07-01 11:54:09'), ('74', 'asdfadsfas', '1151.02', null, 'C', '12', null, 'Active', '2016-07-01 15:08:09', '2016-07-01 15:08:09'), ('75', 'asdfdsfds', '1151.03', null, 'C', '12', null, 'Active', '2016-07-02 04:14:20', '2016-07-02 04:14:20'), ('76', 'a dafdasfasd', '1151.04', null, 'C', '12', null, 'Active', '2016-07-02 04:15:58', '2016-07-02 04:15:58'), ('77', 'fdas fdasfd', '1244', null, 'D', '16', null, 'Active', '2016-07-02 04:20:01', '2016-07-02 04:20:01'), ('78', 'Academic Certificates', '1151.05', null, 'C', '12', null, 'Active', '2016-07-02 04:32:43', '2016-07-02 04:32:43'), ('79', 'asdf sdfsadfs', '2210.01', null, 'C', '28', null, 'Active', '2016-07-02 10:41:00', '2016-07-02 10:41:00');
COMMIT;

-- ----------------------------
--  Table structure for `ca_payment_terms`
-- ----------------------------
DROP TABLE IF EXISTS `ca_payment_terms`;
CREATE TABLE `ca_payment_terms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `days` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_payment_terms`
-- ----------------------------
BEGIN;
INSERT INTO `ca_payment_terms` VALUES ('1', 'asdfdsf fsdfasdf a', 'sadf sfsdf asfadsf', 'Cash', null, '2', null, 'Inactive', '2016-07-02 03:54:04', '2016-07-02 03:54:08'), ('2', 'adsf dfs asdf asdf ', 'as dfa fasdf asdf', 'Cash', null, '2', null, 'Inactive', '2016-07-02 03:54:44', '2016-07-02 03:54:49');
COMMIT;

-- ----------------------------
--  Table structure for `ca_products`
-- ----------------------------
DROP TABLE IF EXISTS `ca_products`;
CREATE TABLE `ca_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `tax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_products`
-- ----------------------------
BEGIN;
INSERT INTO `ca_products` VALUES ('1', null, 'afsf sdf sdf s', 'dasf adsf asdfa dsf', '400', null, 'Active', '2', null, '2016-07-01 11:39:41', '2016-07-01 11:39:41'), ('6', null, 'asa dfs fsd', 'adsfdsaf asdfasdfsadfsd', '300', '', 'Active', '2', null, '2016-07-02 11:31:17', '2016-07-02 11:31:17');
COMMIT;

-- ----------------------------
--  Table structure for `ca_settings_invoice`
-- ----------------------------
DROP TABLE IF EXISTS `ca_settings_invoice`;
CREATE TABLE `ca_settings_invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` enum('template_1','template_2','template_3') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'template_1',
  `company_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_logo` tinyint(4) NOT NULL,
  `product_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_payment_term` int(11) NOT NULL,
  `default_invoice_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_invoice_footer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_estimate_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_estimate_footer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `ca_tax_types`
-- ----------------------------
DROP TABLE IF EXISTS `ca_tax_types`;
CREATE TABLE `ca_tax_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abbreviation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sales_gl_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_gl_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `ca_transaction_items`
-- ----------------------------
DROP TABLE IF EXISTS `ca_transaction_items`;
CREATE TABLE `ca_transaction_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `ledger_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `dc` enum('D','C') COLLATE utf8_unicode_ci NOT NULL,
  `item_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reconciliation_date` date DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_transaction_items`
-- ----------------------------
BEGIN;
INSERT INTO `ca_transaction_items` VALUES ('1', '1', '1', '200', 'D', 'sdaf sdf sdf', null, 'Active', '2016-07-01 11:42:15', '2016-07-01 11:42:15'), ('2', '1', '3', '200', 'C', 'as dfsfsdf ', null, 'Active', '2016-07-01 11:42:15', '2016-07-01 11:42:15'), ('3', '2', '72', '3000', 'D', null, null, 'Active', '2016-07-02 04:05:15', '2016-07-02 04:05:15'), ('4', '2', '9', '3000', 'C', null, null, 'Active', '2016-07-02 04:05:15', '2016-07-02 04:05:15'), ('8', '4', '2', '4000', 'D', 'asdf sdfd sdf', null, 'Active', '2016-07-02 04:31:23', '2016-07-02 04:31:23'), ('9', '4', '8', '4000', 'C', 'dsfasfd', null, 'Active', '2016-07-02 04:31:23', '2016-07-02 04:31:23'), ('10', '5', '74', '3000', 'D', null, null, 'Active', '2016-07-03 12:28:31', '2016-07-03 12:28:31'), ('11', '5', '11', '3000', 'C', null, null, 'Active', '2016-07-03 12:28:31', '2016-07-03 12:28:31'), ('12', '6', '42', '3000', 'D', null, null, 'Active', '2016-07-03 12:54:07', '2016-07-03 12:54:07'), ('13', '6', '23', '3000', 'C', null, null, 'Active', '2016-07-03 12:54:07', '2016-07-03 12:54:07'), ('14', '7', '17', '7000', 'D', null, null, 'Active', '2016-07-03 13:29:49', '2016-07-03 13:29:49'), ('15', '7', '23', '7000', 'C', null, null, 'Active', '2016-07-03 13:29:49', '2016-07-03 13:29:49'), ('16', '8', '2', '600', 'D', null, null, 'Active', '2016-07-03 13:40:23', '2016-07-03 13:40:23'), ('17', '8', '1', '600', 'C', null, null, 'Active', '2016-07-03 13:40:23', '2016-07-03 13:40:23'), ('18', '9', '78', '2000', 'D', null, null, 'Active', '2016-07-03 13:45:51', '2016-07-03 13:45:51'), ('19', '9', '12', '2000', 'C', null, null, 'Active', '2016-07-03 13:45:51', '2016-07-03 13:45:51'), ('20', '10', '17', '1000', 'D', null, null, 'Active', '2016-07-03 13:46:41', '2016-07-03 13:46:41'), ('21', '10', '1', '1000', 'C', null, null, 'Active', '2016-07-03 13:46:41', '2016-07-03 13:46:41'), ('22', '11', '27', '50000', 'D', null, null, 'Active', '2016-07-03 13:49:09', '2016-07-03 13:49:09'), ('23', '11', '20', '50000', 'C', null, null, 'Active', '2016-07-03 13:49:09', '2016-07-03 13:49:09'), ('24', '12', '37', '3200', 'D', null, null, 'Active', '2016-07-03 14:07:37', '2016-07-03 14:07:37'), ('25', '12', '23', '3200', 'C', null, null, 'Active', '2016-07-03 14:07:37', '2016-07-03 14:07:37'), ('26', '13', '61', '5500', 'D', null, null, 'Active', '2016-07-03 14:09:18', '2016-07-03 14:09:18'), ('27', '13', '78', '5500', 'C', null, null, 'Active', '2016-07-03 14:09:18', '2016-07-03 14:09:18'), ('28', '14', '22', '4000', 'D', null, null, 'Active', '2016-07-03 14:10:12', '2016-07-03 14:10:12'), ('29', '14', '23', '4000', 'C', null, null, 'Active', '2016-07-03 14:10:12', '2016-07-03 14:10:12'), ('30', '15', '29', '3000', 'D', null, null, 'Active', '2016-07-03 14:10:42', '2016-07-03 14:10:42'), ('31', '15', '43', '3000', 'C', null, null, 'Active', '2016-07-03 14:10:42', '2016-07-03 14:10:42'), ('32', '16', '50', '5000', 'D', null, null, 'Active', '2016-07-03 14:11:04', '2016-07-03 14:11:04'), ('33', '16', '79', '5000', 'C', null, null, 'Active', '2016-07-03 14:11:04', '2016-07-03 14:11:04'), ('34', '17', '3', '7000', 'D', 'h rd fghd fgh', null, 'Active', '2016-07-03 14:36:22', '2016-07-03 14:36:22'), ('35', '18', '3', '7000', 'C', 'hgjfh', null, 'Active', '2016-07-03 14:36:59', '2016-07-03 14:36:59'), ('36', '18', '72', '7000', 'D', 'asd sdf', null, 'Active', '2016-07-03 14:36:59', '2016-07-03 14:36:59'), ('37', '19', '13', '5000', 'D', 'asdf sd fdsf', null, 'Active', '2016-07-03 14:37:42', '2016-07-03 14:37:42'), ('38', '19', '21', '5000', 'C', 'asdf asdf asdf', null, 'Active', '2016-07-03 14:37:42', '2016-07-03 14:37:42'), ('39', '20', '77', '7000', 'D', 'asd fadsfdsa', null, 'Active', '2016-07-03 14:39:08', '2016-07-03 14:39:08'), ('40', '20', '21', '12000', 'C', '', null, 'Active', '2016-07-03 14:39:08', '2016-07-03 14:39:08'), ('41', '20', '18', '5000', 'D', '', null, 'Active', '2016-07-03 14:39:08', '2016-07-03 14:39:08');
COMMIT;

-- ----------------------------
--  Table structure for `ca_transactions`
-- ----------------------------
DROP TABLE IF EXISTS `ca_transactions`;
CREATE TABLE `ca_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `transaction_date` date NOT NULL,
  `transaction_type` enum('inflow','outflow','journal','invoice') COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_type` tinyint(4) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `cheque_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_vendor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pv_receipt_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_transactions`
-- ----------------------------
BEGIN;
INSERT INTO `ca_transactions` VALUES ('1', 'RFJ1467373335', '200', '2016-06-29', 'journal', null, null, null, null, null, '42345435', null, null, 'Active', '2016-07-01 11:42:15', '2016-07-01 11:42:15'), ('2', 'TRA1467432315', '3000', '2016-06-29', 'outflow', 'asdf sdf sdfasdf', '2', '3', '2344234', 'asdf sdfad fds', '23423', '2', null, 'Active', '2016-07-02 04:05:15', '2016-07-02 04:05:15'), ('4', 'RFJ1467433883', '4000', '2016-06-30', 'journal', null, null, null, null, null, '324', null, null, 'Active', '2016-07-02 04:31:23', '2016-07-02 04:31:23'), ('5', 'adsfsd', '3000', '2016-06-30', 'outflow', 'asd fsadf', null, null, null, null, null, '9', null, 'Active', '2016-07-03 12:28:31', '2016-07-03 12:28:31'), ('6', '32432', '3000', '2016-06-29', 'outflow', 'asdf asdfadsfs', null, null, null, null, null, '9', null, 'Active', '2016-07-03 12:54:07', '2016-07-03 12:54:07'), ('7', '343243', '7000', '2016-06-29', 'outflow', 'asdf asdf sadf', null, null, null, null, null, '9', null, 'Active', '2016-07-03 13:29:49', '2016-07-03 13:29:49'), ('8', '34234', '600', '2016-05-29', 'outflow', 'asdf asdf sadfa sd', null, null, null, null, null, '9', null, 'Active', '2016-07-03 13:40:23', '2016-07-03 13:40:23'), ('9', '5324423', '2000', '2016-04-26', 'outflow', 'asdh adjksf asdfiu asdf asdkhfkas', null, null, null, null, null, '9', null, 'Active', '2016-07-03 13:45:51', '2016-07-03 13:45:51'), ('10', '4444', '1000', '2016-03-01', 'outflow', 'asdjkhfgds kjh jdsagkf jahsdf eiuwy qmnbsadmv asdf', null, null, null, null, null, '9', null, 'Active', '2016-07-03 13:46:41', '2016-07-03 13:46:41'), ('11', '42342', '50000', '2016-06-28', 'inflow', 'asdf sadf sadf asdf asdf', null, null, null, null, null, '9', null, 'Active', '2016-07-03 13:49:09', '2016-07-03 13:49:09'), ('12', '34534534', '3200', '2016-06-07', 'inflow', 'asf sdaf sadf fsadfljkhjksdh odsua fy iasfy', null, null, null, null, null, '9', null, 'Active', '2016-07-03 14:07:37', '2016-07-03 14:07:37'), ('13', '76328', '5500', '2016-05-25', 'inflow', 'as fsf yiey ysadf bdsafgj ew', null, null, null, null, null, '9', null, 'Active', '2016-07-03 14:09:18', '2016-07-03 14:09:18'), ('14', '34234', '4000', '2016-06-28', 'inflow', 'a sdf sdf sdf', null, null, null, null, null, '9', null, 'Active', '2016-07-03 14:10:12', '2016-07-03 14:10:12'), ('15', '32432', '3000', '2016-06-29', 'outflow', 'asdf asdfadsfs', null, null, null, null, null, '9', null, 'Active', '2016-07-03 14:10:42', '2016-07-03 14:10:42'), ('16', '34543', '5000', '2016-06-28', 'inflow', 'asdf asdfasdf sdfsd f', null, null, null, null, null, '9', null, 'Active', '2016-07-03 14:11:04', '2016-07-03 14:11:04'), ('17', 'RFJ1467556582', '7000', '2016-06-29', 'journal', null, null, null, null, null, '567578678', null, null, 'Active', '2016-07-03 14:36:22', '2016-07-03 14:36:22'), ('18', 'RFJ1467556619', '7000', '2016-06-28', 'journal', null, null, null, null, null, '5746754', null, null, 'Active', '2016-07-03 14:36:59', '2016-07-03 14:36:59'), ('19', 'RFJ1467556662', '5000', '2016-06-26', 'journal', null, null, null, null, null, '3243243', null, null, 'Active', '2016-07-03 14:37:42', '2016-07-03 14:37:42'), ('20', 'RFJ1467556748', '12000', '2016-07-14', 'journal', null, null, null, null, null, '2334', null, null, 'Active', '2016-07-03 14:39:08', '2016-07-03 14:39:08');
COMMIT;

-- ----------------------------
--  Table structure for `ca_vendors`
-- ----------------------------
DROP TABLE IF EXISTS `ca_vendors`;
CREATE TABLE `ca_vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ledger_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `ca_vendors`
-- ----------------------------
BEGIN;
INSERT INTO `ca_vendors` VALUES ('1', '2210.01', 'asdf sdfsadfs', 'adfs@asdf.com', 'asdfsdfsd ', 'sdafsda', 'NGN', null, '23423424', null, '166', 'das fsdfasdf sdf', 'Active', '2016-07-02 10:41:00', '2016-07-02 10:41:00');
COMMIT;

-- ----------------------------
--  Table structure for `cash_receipt`
-- ----------------------------
DROP TABLE IF EXISTS `cash_receipt`;
CREATE TABLE `cash_receipt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `cash_receipt`
-- ----------------------------
BEGIN;
INSERT INTO `cash_receipt` VALUES ('1', '2016-06-28', '34234', '1', 'asf fsdfsaf', 'a sdf sdf sdf', '4000', '1', 'Active', '2', null, '2016-07-01 12:47:59', '2016-07-03 14:10:12');
COMMIT;

-- ----------------------------
--  Table structure for `cheque_payment`
-- ----------------------------
DROP TABLE IF EXISTS `cheque_payment`;
CREATE TABLE `cheque_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `pv_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheque_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `cheque_payment`
-- ----------------------------
BEGIN;
INSERT INTO `cheque_payment` VALUES ('1', '2016-06-29', '32432', 'First Bank', '3214', 'adsf sadfas', 'asdf asdfadsfs', '3000', '1', '2', null, 'Active', '2016-07-03 12:40:22', '2016-07-03 12:54:07'), ('2', '2016-06-29', '32432', 'First Bank', '3214', 'adsf sadfas', 'asdf asdfadsfs', '3000', '1', '2', null, 'Active', '2016-07-03 12:40:36', '2016-07-03 14:10:42');
COMMIT;

-- ----------------------------
--  Table structure for `cheque_receipt`
-- ----------------------------
DROP TABLE IF EXISTS `cheque_receipt`;
CREATE TABLE `cheque_receipt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_lodged` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue_bank` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheque_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `cheque_receipt`
-- ----------------------------
BEGIN;
INSERT INTO `cheque_receipt` VALUES ('1', '2016-06-28', '34543', 'Fidelity Bank', 'Frank Green', 'GTB', 'asdf asdfasdf sdfsd f', '534534', '5000', '1', '2', null, 'Active', '2016-07-03 13:00:39', '2016-07-03 14:11:04');
COMMIT;

-- ----------------------------
--  Table structure for `credit_purchase`
-- ----------------------------
DROP TABLE IF EXISTS `credit_purchase`;
CREATE TABLE `credit_purchase` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `credit_purchase`
-- ----------------------------
BEGIN;
INSERT INTO `credit_purchase` VALUES ('1', '2016-06-29', '343243', 'dsa asf sdfsad', 'asdf asdf sadf', '7000', '1', '2', null, 'Active', '2016-07-03 13:29:23', '2016-07-03 13:29:49'), ('2', '2016-05-29', '34234', 'asd safsdf', 'asdf asdf sadfa sd', '600', '1', '2', null, 'Active', '2016-07-03 13:30:10', '2016-07-03 13:40:23'), ('3', '2016-04-26', '5324423', 'dsaljadhsf ', 'asdh adjksf asdfiu asdf asdkhfkas', '2000', '1', '2', null, 'Active', '2016-07-03 13:42:01', '2016-07-03 13:45:51'), ('4', '2016-03-01', '4444', 'asdnmf ajsdhkfg jhksdfv', 'asdjkhfgds kjh jdsagkf jahsdf eiuwy qmnbsadmv asdf', '1000', '1', '2', null, 'Active', '2016-07-03 13:42:17', '2016-07-03 13:46:41');
COMMIT;

-- ----------------------------
--  Table structure for `credit_sales`
-- ----------------------------
DROP TABLE IF EXISTS `credit_sales`;
CREATE TABLE `credit_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `credit_sales`
-- ----------------------------
BEGIN;
INSERT INTO `credit_sales` VALUES ('1', '2016-06-28', '42342', 'asdf asdf asdf sdaf', 'asdf sadf sadf asdf asdf', '50000', '1', 'Active', '2', null, '2016-07-03 13:21:28', '2016-07-03 13:49:09'), ('2', '2016-06-07', '34534534', 'asd fasd fa sf sad', 'asf sdaf sadf fsadfljkhjksdh odsua fy iasfy', '3200', '1', 'Active', '2', null, '2016-07-03 13:49:36', '2016-07-03 14:07:37'), ('3', '2016-05-25', '76328', 'sdjkh kjhads jh ', 'as fsf yiey ysadf bdsafgj ew', '5500', '1', 'Active', '2', null, '2016-07-03 13:49:55', '2016-07-03 14:09:18');
COMMIT;

-- ----------------------------
--  Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `migrations`
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES ('2015_12_14_142810_create_ca_groups_table', '1'), ('2015_12_14_142820_create_ca_ledgers_table', '1'), ('2015_12_14_143848_create_ca_bank_accounts_table', '1'), ('2015_12_14_144721_create_ca_customers_table', '1'), ('2015_12_14_144743_create_ca_fiscal_year_table', '1'), ('2015_12_14_144758_create_ca_invoice_table', '1'), ('2015_12_14_144829_create_ca_invoice_items_table', '1'), ('2015_12_14_145015_create_ca_payment_terms_table', '1'), ('2015_12_14_145036_create_ca_products_table', '1'), ('2015_12_14_145136_create_ca_settings_invoice_table', '1'), ('2015_12_14_145154_create_ca_tax_types_table', '1'), ('2015_12_14_145217_create_ca_transactions_table', '1'), ('2015_12_14_145234_create_ca_vendors_table', '1'), ('2015_12_23_113835_create_credit_purchase_table', '1'), ('2015_12_23_113902_create_credit_sales_table', '1'), ('2015_12_24_113835_create_cash_receipt_table', '1'), ('2015_12_24_113835_create_cheque_payment_table', '1'), ('2015_12_24_113835_create_cheque_receipt_table', '1'), ('2015_12_24_113835_create_petty_cash_table', '1');
COMMIT;

-- ----------------------------
--  Table structure for `petty_cash`
-- ----------------------------
DROP TABLE IF EXISTS `petty_cash`;
CREATE TABLE `petty_cash` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `pvc_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `petty_cash`
-- ----------------------------
BEGIN;
INSERT INTO `petty_cash` VALUES ('1', '2016-06-30', 'adsfsd', 'ads sdfsf sdf', 'asd fsadf', '3000', '1', '2', null, 'Active', '2016-07-01 12:46:08', '2016-07-03 12:28:31');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
