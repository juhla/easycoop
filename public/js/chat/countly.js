/**
 * Created by sherriff on 30/11/2017.
 */


//some default pre init
var Countly = Countly || {};
Countly.q = Countly.q || [];

//provide countly initialization parameters
Countly.app_key = 'b699437ed91f83b8c7bb8d62f3904db22f935271';
Countly.url = 'http://ikoobaapp.com:40000';

Countly.q.push(['track_sessions']);
Countly.q.push(['track_pageview']);
Countly.q.push(['track_clicks']);
Countly.q.push(['track_errors']);
Countly.q.push(['track_links']);
Countly.q.push(['track_forms']);
Countly.q.push(['collect_from_forms']);

//load countly script asynchronously
(function () {
    var cly = document.createElement('script');
    cly.type = 'text/javascript';
    cly.async = true;
    //enter url of script here
    cly.src = 'https://cdnjs.cloudflare.com/ajax/libs/countly-sdk-web/17.9.0/countly.min.js';
    cly.onload = function () {
        Countly.init()
    };
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(cly, s);
})();

