$(function () {

    var theLink = $('ul li');
    theLink.on('click', doIt);

    theLink.removeClass('active');
    $('.y-nav').removeClass('y-nav-onscroll');


    $(window).on('scroll', debounce(doingScroll, 15));
    
    function doingScroll(e){
    
        console.log('doing scroll');
        console.count(e);
        var hasTheClass = $('.y-nav').hasClass('y-nav-onscroll');
        console.log('hasTheClass: '+hasTheClass);
        if(!hasTheClass){
            $('.y-nav').addClass('y-nav-onscroll');
        }
        //$('.y-nav').addClass('y-nav-onscroll');
    

    }

    function doIt(){
        console.log('doing it');
        theLink.removeClass('active');
        $('.y-nav').removeClass('y-nav-onscroll');
    


        var theHref = $(this).find('a').attr('href');
        $(this).addClass('active');
        $('.y-nav').addClass('y-nav-onscroll');
        console.log('target: '+theHref);

        $('html, body').animate({
            scrollTop: $(theHref).offset().top
        }, 1000); 
    }




function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};    

})