$(document).on("click", ".approve", function(event) {
  var ID = $(this).attr("data-value");
  var url = $(this).attr("data-url-approve");
  var baseurl = $("#baseurl").val();

  var $this = $(this);
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to reverse this transaction once approved!",
      type: "warning",
      showCancelButton: !0,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, approve it!",
      closeOnConfirm: !1
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "GET",
          url: baseurl + url + ID,
          context: this,
          success: function() {
            swal(
              {
                title: "Confirmed!",
                text: "Transaction was confirmed successfully",
                confirmButtonColor: "#66BB6A",
                type: "success"
              },
              function(isConfirm) {
                if (isConfirm) {
                  $this.fadeOut("slow", function() {
                    $this.closest("tr").remove();
                  });
                }
              }
            );
          }
        });
      }
    }
  );
});

$(document).on("click", ".reject_comment", function() {
  var trnx_id = $(this).attr("data-value");
  $("#trnx_id").val(trnx_id);
  $("#commentModal").modal("show");
});

$(document).on("click", ".reject-transaction", function(event) {
  var ID = $(this).attr("data-value");
  var url = $(this).attr("data-url-reject");
  var baseurl = $("#baseurl").val();

  var $this = $(this);
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to reverse this transaction once rejected!",
      type: "warning",
      showCancelButton: !0,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, reject it!",
      closeOnConfirm: !1
    },
    function(isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "GET",
          url: baseurl + url + ID,
          context: this,
          success: function() {
            swal(
              {
                title: "Rejected!",
                text: "Transaction was rejected successfully",
                confirmButtonColor: "#66BB6A",
                type: "success"
              },
              function(isConfirm) {
                if (isConfirm) {
                  $this.fadeOut("slow", function() {
                    $this.closest("tr").remove();
                  });
                }
              }
            );
          }
        });
      }
    }
  );
});

$(document).on("submit", "#rejection_comment", function(e) {
  e.preventDefault();
  // var trnx_id = $(this).attr("data-value");
  // console.log($(this).serialize());
  trnx_id = document.getElementById("trnx_id").value;
  reject_comment = document.getElementById("reject_comment").value;

  var reject_data = {
    trnx_id: trnx_id,
    reject_comment: reject_comment
  };
  $.ajax({
    url: $(this).attr("action"),
    type: "POST",
    cache: false,
    context: this,
    data: reject_data,
    success: function() {
      $("#commentModal").modal("hide");
      var $this = $(this);

      swal(
        {
          title: "Rejected!",
          text: "Transaction was rejected successfully",
          confirmButtonColor: "#66BB6A",
          type: "success"
        },
        function(isConfirm) {
          if (isConfirm) {
            $this.fadeOut("slow", function() {
              $this.closest("tr").remove();
              location.reload();
            });
          }
        }
      );
    },
    error: function(request, errorType, errorMessage) {
      displayNotification(
        "Unable to reject transaction. Please try again",
        "Error!",
        "error"
      );
      $("#commentModal").modal("hide");
    },
    timeout: 3000
  });
});
