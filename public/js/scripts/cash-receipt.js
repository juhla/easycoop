$(function(){
    //set base url variable
    var baseurl = $('#baseurl').val();
    var checkboxes = $("input[type='checkbox']"),
        deleteBtn = $(".delete-selected"),
        postBtn = $('.post-entries');
    //checkall
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
        deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        postBtn.attr("disabled", !checkboxes.is(":checked"));
    });


    //enable/disable delete button
    $(document).on("click", checkboxes, function() {
        deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        postBtn.attr("disabled", !checkboxes.is(":checked"));
    });

    //post entries to gl
    postBtn.on('click', function(){
        if(!checkboxes.is(':checked')){
            alert('Please select an entry to post to GL');
        }else{
            $('#post-register').submit();
        }
    });

    //display form modal
    $('.add-new').click(function(){
        $('#displayForm').slideDown();
    });

    //hide and empty form
    $('.close-modal').click(function(){
        $('#cashReceiptForm')[0].reset();
        $('#displayForm').slideUp();
        $('#item_id').val('');
    });

    ////////////BEGIN CREDIT SALES\\\\\\\\\\\\\\\
    //submit details on click
    $(document).on('submit','#cashReceiptForm', function (e) {
        e.preventDefault();
        console.log('ddd');

        var transaction_id = $('#item_id').val();
        var row_index = $('#row_index').val();
        var $btn = $('#saveBtn');
        $btn.attr('disabled', true);
        $btn.html('Saving...');
        $.ajax({
            type: 'POST',
            url:  $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function (res) {
                if(transaction_id !== ''){
                    $('#cashReceiptForm')[0].reset();
                    $('#displayForm').slideUp();
                    $('#entries > tr').eq(row_index).html(res);
                    $('#item_id').val('');
                    displayNotification('Entry has been updated', 'Success!', 'success');
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                }else{
                    $('.no-income').hide();
                    $('#cashReceiptForm')[0].reset();
                    displayNotification('Entry has been saved','Success!', 'success');
                    $('#entries').prepend(res);
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                }
            },error: function(res){
                displayNotification('An unknown error occurred', 'Error!', 'error');
            }
        });
    });
    //edit entry
    $(document).on('click', '.edit', function(e){
        var id = $(this).attr('data-value');
        var row_index = $(this).closest('tr').index();
        $('#row_index').val(row_index);
        $.ajax({
            type: 'GET',
            url:  baseurl+'/notebook/cash-register/get-cr/'+id,
            dataType: 'json',
            success: function (res) {
                for (var key in res) {
                    $("input[name=" + key + "]").val(res[key]);
                }
                $('#title').html('Update');
                $('#item_id').val(id);
                $('#displayForm').slideDown();
            }
        });
    });
    //delete inflow entry
    $(document).on("click", ".delete", function (event) {

        var ID = $(this).attr('data-value');
        var $this = $(this);
        swal({
            title:"Are you sure?",
            text:"You will not be able to recover the data once deleted!",
            type:"warning",
            showCancelButton:!0,
            confirmButtonColor:"#DD6B55",
            confirmButtonText:"Yes, delete it!",
            closeOnConfirm:!1
        },function(isConfirm){
            if(isConfirm){
                $.ajax({
                    type: 'GET',
                    url: baseurl+'/notebook/cash-register/delete-cr/' + ID,
                    context: this,
                    success: function () {
                        swal({
                            title: "Deleted!",
                            text: "item was deleted successfully",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        }, function(isConfirm){
                            if(isConfirm){
                                $this.fadeOut('slow', function () {
                                    $this.closest("tr").remove();
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    //bulk delete
    deleteBtn.click(function(){
        var ids = [];
        var _token = "{{csrf_token()}}";
        $.each($("input[name='ids']:checked"), function() {
            ids.push($(this).val());
        });
        if(confirm('Are you sure you want to delete '+ ids.length +' items?')){
            $('body').loader('show');
            $.ajax({
                type: 'POST',
                url: baseurl + '/notebook/cash-register/delete-selected-cr',
                data: {ids:ids, _token:_token},
                context: this,
                success: function (e) {
                    $("input[name='ids']:checked").each(function(){
                        $(this).fadeOut('slow', function () {
                            $(this).closest("tr").remove();
                        });
                    });
                    $("input:checkbox").prop('checked', false);
                    $('body').loader('hide');
                },
                error: function(e){
                    //console.log(e)
                }
            });

        };
    });
});
