$(function(){

	var removeNotice = function( el ){
		el.fadeOut('slow');
	}

	var baseurl = $('#baseurl').val();
	var uid = '';
	var cid = '';
	var req_btn = '';

	$('.c-connect').on('click', function(evt){
		evt.preventDefault();
		mid = $(this).data('codex');
		url = baseurl+'/app/town-hall/connections/urlrequest';

		req_btn = $(this);
		req_btn.attr('disabled', true).text('wait...')
		$.ajax({
			type: 'POST',
			url: url,
			data: {_token: $('#_token').val(), mid: mid},
			success: function(res){
				
				if(res.statusCode === 2000)
				{
					req_btn.text(res.message);
				}
			},
			error: function(res) {
				req_btn.removeAttribute('disabled').text('Connect');
				displayNotification('An unknown error occurred while trying to send request. Please again later.')
			}
		});
	});

	// Accept connection request
	$('.c-accept').on('click', function(evt){
		
		evt.preventDefault();
		uid = $(this).data('u');
		cid = $(this).data('c');
		var el = $(this).closest('.notification-item');
		url = baseurl+'/app/town-hall/connections/acceptconn/'+cid;

		req_btn = $(this);
		req_btn.attr('disabled', true).text('connecting...')
		$.ajax({
			type: 'POST',
			url: url,
			data: {_token: $('#_token').val(), uid: uid, cid: cid},
			success: function(res){
				
				if(res.statusCode === 2000)
				{
					req_btn.text(res.message);
					setTimeout(removeNotice(el), 2000);
				}
			},
			error: function(res) {
				$(this).removeAttribute('disabled');
				displayNotification('An unknown error occurred while trying to send request. Please again later.')
			}
		});
	});

	// Reject connection request
	$('.c-reject').on('click', function(evt){
		
		evt.preventDefault();
		var uid = $(this).data('u');
		var cid = $(this).data('cr');
		var el = $(this).closest('.notification-item');
		var url = baseurl+'/app/town-hall/connections/reject-connection/'+cid;

		req_btn = $(this);
		req_btn.attr('disabled', true).text('wait...')
		$.ajax({
			type: 'POST',
			url: url,
			data: {_token: $('#_token').val(), uid: uid, cid: cid},
			success: function(res){
				
				if(res.statusCode === 2000)
				{
					req_btn.text(res.message);
					setTimeout(removeNotice(el), 10000);
				}
			},
			error: function() {
				req_btn.removeAttribute('disabled');
				displayNotification('An unknown error occurred while trying to send request. Please again later.')
			}
		});
	});


	var mid = '';
	var req_btn = '';
	$(document).on('click', '.c-disconnect', function(evt){
		evt.preventDefault();
		cid = $(this).data('ccodex');
		url = baseurl+'/app/town-hall/connections/disconnect/'+cid;

		$.ajax({
			type: 'POST',
			url: url,
			data: {_token: $('#_token').val(), cid: cid},
			success: function(res){
				
				if(res.statusCode === 2000)
				{
					displayNotification(res.message)
				}
			},
			error: function(res) {
				displayNotification('An unknown error occurred while trying to send request. Please again later.')
			}
		})
	});
});