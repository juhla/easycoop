

var displayNotification = function(message, title, type){
    swal({
        title: title,
        text: message,
        confirmButtonColor: "#66BB6A",
        type: type
    })
}

/**
 * This loads all the accounts in the GL fields
 */
function loadGLAccounts()
{
    var glp = "";
    var gls = "";
    $.ajax($('#baseurl').val()+"/api-v1/get-gl-accounts-for-select", {
        type    :   'GET',
        success :   function(data){
            $('#credit_account').append(data);
            $('#debit_account').append(data);

            // Used in journal module
            $('.ledger').append(data);
            $('.select2-option').select2();
        }
    });
}

/**
 *
 */
function loadTaxes()
{
    var tax = "";

    $.ajax(baseurl+"/setup/get-taxes", {
        type    :   'GET',
        success :   function(data){
            if(data.status === 200){
                container.html("");
                container.append(data.taxes);
                //paginate();
            }else{
                container.html("<tr><td colspan='5'> You have not created any tax yet.</td></tr>");
            }
        },error: function(err){
            console.log(err)
        }
    });
}

// trigger on click
function add_new_customer() {
    var invoice_type = $('#invoice_type').val();
    if(invoice_type === 'sales') {
        $('#customerFormModal').modal('show');
    }else{
        $('#vendorFormModal').modal('show');
    }
}

function add_new_product() {
var taxes = [];
    $('#productFormModal').modal('show');
    $.ajax($('#baseurl').val()+"/api-v1/get-taxes", {
        type    :   'GET',
        success :   function(res){
            $('select#tax').empty();
            $.each(res.data, function(index, tax){
                taxes += '<option value="'+tax.id +'">'+tax.name+'</option>';
            });
            $('select#tax').append(taxes).select2("destroy").select2();
            //console.log(taxes);
        }
    });
}

function numbersonly(e){
    var unicode = e.charCode? e.charCode : e.keyCode
    if (unicode != 8 && unicode != 46 && unicode !=9){ //if the key isn't the backspace key (which we should allow)
        if (unicode < 48 || unicode > 57) //if not a number
            return false //disable key press
    }
}

$(document).ready(function () {
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
});

if ($.fn.select2) {
    $(".select2-option").select2();
}

$('input#amount').keyup(function(e) {

    // skip for arrow keys
    if ((e.keyCode >= 16 && e.keyCode <= 18) || (e.keyCode >= 33 && e.keyCode <= 40) || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {return;}

    $(this).val(function(index, value) {
        value = value.replace(/,/g,'');
        return numberWithCommas(value);
    });
});

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}