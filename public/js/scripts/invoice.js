/**
 * Created by Sopi on 10/12/2015.
 */


/* Prototyping
 /* ========================================================================== */

(function (window, ElementPrototype, ArrayPrototype, polyfill) {
    function NodeList() {
        [polyfill]
    }

    NodeList.prototype.length = ArrayPrototype.length;

    ElementPrototype.matchesSelector = ElementPrototype.matchesSelector ||
        ElementPrototype.mozMatchesSelector ||
        ElementPrototype.msMatchesSelector ||
        ElementPrototype.oMatchesSelector ||
        ElementPrototype.webkitMatchesSelector ||
        function matchesSelector(selector) {
            return ArrayPrototype.indexOf.call(this.parentNode.querySelectorAll(selector), this) > -1;
        };

    ElementPrototype.ancestorQuerySelectorAll = ElementPrototype.ancestorQuerySelectorAll ||
        ElementPrototype.mozAncestorQuerySelectorAll ||
        ElementPrototype.msAncestorQuerySelectorAll ||
        ElementPrototype.oAncestorQuerySelectorAll ||
        ElementPrototype.webkitAncestorQuerySelectorAll ||
        function ancestorQuerySelectorAll(selector) {
            for (var cite = this, newNodeList = new NodeList; cite = cite.parentElement;) {
                if (cite.matchesSelector(selector)) ArrayPrototype.push.call(newNodeList, cite);
            }

            return newNodeList;
        };

    ElementPrototype.ancestorQuerySelector = ElementPrototype.ancestorQuerySelector ||
        ElementPrototype.mozAncestorQuerySelector ||
        ElementPrototype.msAncestorQuerySelector ||
        ElementPrototype.oAncestorQuerySelector ||
        ElementPrototype.webkitAncestorQuerySelector ||
        function ancestorQuerySelector(selector) {
            return this.ancestorQuerySelectorAll(selector)[0] || null;
        };
})(this, Element.prototype, Array.prototype);

/* Helper Functions
 /* ========================================================================== */

function generateTableRow(counter) {

    var emptyColumn = document.createElement('tr');

    emptyColumn.innerHTML = '<td>' +
        '<input type="text" class="form-control" name="description[]" id="description" value=""></td>' +
        '<td><input type="text" class="form-control" name="price[]" id="price" value="0" /></td>' +
        '<td><a href="javascript:;" class="cut" style="color: red;"><i class="fa fa-times"></i> </a> ' +
        '<input type="hidden" name="item_amount[]" id="item_amount" value="" />' +
        '<input type="hidden" name="item_id[]" id="item_id" value="" /></td>';
    //loadProducts( counter );
    return emptyColumn;
}

loadProducts = function (prod_no) {
    var baseurl = $('#baseurl').val();
    var prd = '';
    $.ajax(baseurl + "/setup/get-products", {
        type: 'GET',
        success: function (data) {
            if (data.status === 200) {
                $.each(data.data, function (index, val) {
                    prd += "<option data-product-price='" + val.price + "' value='" + val.id + "'>" + val.name + "</option>";
                });
                if (prod_no) {
                    $("#products").empty();
                    $("#product_" + prod_no).append(prd);
                } else {
                    $(".products").empty();
                    $(".products").append(prd);
                }
            }
        },
        error: function (response, errorType, errorMessage) {
            if (errorType === 'Unauthorized') {
                $.notific8('Could not load products. Please login to the application again.', {
                    'life': 5000,
                    'theme': 'danger',
                    'heading': 'Error'
                })
            }
        },
        timeout: 9000
    });
};

function parseFloatHTML(element) {
    return parseFloat(element.innerHTML.replace(/[^\d\.\-]+/g, '')) || 0;
}

function parsePrice(number) {
    return number.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
}

/**
 * This loads all the accounts in the GL fields
 */
function loadGLAccounts() {
    $.ajax($('#baseurl').val() + "/api-v1/get-gl-accounts-for-select", {
        type: 'GET',
        success: function (data) {
            $('#income_gl_account').append(data).selectpicker('refresh');
            $('#expense_gl_account').append(data).selectpicker('refresh');

        }
    });
}

/* Update Invoice
 /* ========================================================================== */

function updateInvoice() {
    var total = 0;
    var inputCells, textCells, price, total, a, i;

    // update inventory cells
    // ======================

    for (var a = document.querySelectorAll('table.inventory tbody tr'), i = 0; a[i]; ++i) {
        inputCells = a[i].querySelectorAll('input[value]');
        //console.log(inputCells[1].value);
        // get inventory row cells
        /*       inputCells = a[i].querySelectorAll('input[value]');
               textCells  = a[i].querySelectorAll('span.rowTotal');
               // set price as cell[2] * cell[3]
               console.log(inputCells[0].value);
               console.log(inputCells[1].value);
               parseFloat(inputCells[1].value);
               // add price to total
               total += parseFloat(inputCells[1].value);
               // set row total
               textCells[0].innerHTML = parsePrice(price);
               inputCells[2].value = price;*/
        total += parseFloat(inputCells[1].value);
    }


    // update sub total
    // ====================

    // set sub total


    $('.sub-total').text(parsePrice(total));
    changeVAT(total);
    $("#vat").click(function () {
        changeVAT(total);
    });

    //$('#subTotal').val(total);
    // set grand total
    //cells[2].innerHTML = document.querySelector('table.meta tr:last-child td:last-child span:last-child').innerHTML = parsePrice(total - parseFloatHTML(cells[1]));
    //$('.grand-total').text(parsePrice(total));
    //$('#total').val(total);

}


function changeVAT(total) {
    if ($('#vat').prop("checked")) {
        $('#vat').val(1);
        var tax = total * 0.05;
        var grand = total + tax;
        $('.vat-total').text(parsePrice(tax));
        $('.sub-total').text(parsePrice(total));
        $('.grand-total').text(parsePrice(grand));

        console.log(parsePrice(total));
        console.log(parsePrice(grand));
    }
    else {
        $('#vat').val(0);
        $('.vat-total').text(parsePrice(0));
        $('.sub-total').text(parsePrice(total));
        $('.grand-total').text(parsePrice(total));
    }
    $('#total').val(total);
}


/* On Content Load
 /* ========================================================================== */

function onContentLoad() {
    // $('#invoiceForm').validate();
    //set datepicker plugin for date fields
    //$(".datepicker").datepicker({format: 'yyyy-mm-dd', minView: 2, autoclose: 1});
    // append footer at the end of products dropdown list
    // $('.products').select2("container").find("div.select2-drop").append('<a href="javascript:void(0)" class="btn btn-info btn-block" onclick="add_new_product()"><i class="fa fa-plus"></i> Add New Product</a>');
    // append footer at the end of customers dropdown list
    // $('.customers').select2("container").find("div.select2-drop").append('<a href="javascript:void(0)" class="btn btn-info btn-block" onclick="add_new_customer()"><i class="fa fa-plus"></i> Add New Customer</a>');

    var counter = 2;
    //
    //loadProducts(1);
    //updateInvoice();

    //get product info
    $(document).on('change', '.products', function () {
        var prd_id = 0;
        var dis = $(this);
        prd_id += $(this).val();
        $.ajax($('#baseurl').val() + "/setup/get-product/" + prd_id, {
            type: 'GET',
            cache: false,
            context: this,
            success: function (data) {
                if (data.status === 200) {
                    dis.closest('tr').find('#description').val(data.data.description);
                    dis.closest('tr').find('#price').val(data.data.price);
                    dis.closest('tr').find('#tax').val(data.data.tax);
                    dis.closest('tr').find('.amount').text(data.data.price);
                    updateInvoice();
                } else {
                    dis.closest('tr').find('.price-label').text('NGN 0.00');
                    dis.closest('tr').find('.Price').val('');
                }
            }
        });
    });

    function onClick(e) {
        var row;

        if (e.target.matchesSelector('#add-row')) {
            console.log(counter);
            document.querySelector('table.inventory tbody').appendChild(generateTableRow(counter));
            //$("#product_" + counter).select2();
            // append footer at the end of products dropdown list
            // $('#product_'+ counter).select2("container").find("div.select2-drop").append('<a href="javascript:void(0)" class="btn btn-info btn-block" onclick="add_new_product()"><i class="fa fa-plus"></i> Add New Product</a>');

            counter++;
        }
        else if (e.target.className == 'add-p') {
            loadGLAccounts();
            $('#sell_this').val(1);
            $('.income_account').removeClass('hidden');
            $('#new-product').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
        $('.cut').on('click', function () {
            $(this).closest('tr').remove();
        });


        // updateInvoice();
    }


    //add new customer
    $(document).on('submit', '#customerForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            cache: false,
            context: this,
            data: $(this).serialize(),
            success: function (e) {
                $('#customerFormModal').modal('hide');
                displayNotification('Customer has been added', 'Success!', 'success');
                $('#customer_id').append('<option value=' + e.data.id + '>' + e.data.name + '</option>');
                $('#customer_id').select2('val', e.data.name);
            },
            error: function (request, errorType, errorMessage) {
                displayNotification('Unable to add product. Please try again', 'Error!', 'error');
                $('#customerFormModal').modal('hide');
            },
            timeout: 3000
        });
    });

    //add new customer
    $(document).on('submit', '#vendorForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            cache: false,
            context: this,
            data: $(this).serialize(),
            success: function (e) {
                $('#vendorFormModal').modal('hide');
                displayNotification('Customer has been added', 'success');
                $('#customer_id').append('<option value=' + e.data.id + '>' + e.data.name + '</option>');
                $('#customer_id').select2('val', e.data.name);
            },
            error: function (request, errorType, errorMessage) {
                displayNotification('Unable to add product. Please try again', 'error');
                $('#vendorFormModal').modal('hide');
            },
            timeout: 3000
        });
    });

    //add new product
    $(document).on('submit', '#productForm', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            cache: false,
            context: this,
            data: $(this).serialize(),
            success: function (e) {
                displayNotification('Product has been added', 'Success!', 'success');
                $('.products').append("<option data-product-price='" + e.data.price + "' value='" + e.data.id + "'>" + e.data.name + "</option>");
                $('#productFromModal').modal('hide');
            },
            error: function (request, errorType, errorMessage) {
                displayNotification('Unable to add product. Please try again', 'Error!', 'error');
                $('#productFromModal').modal('hide');

            },
            timeout: 3000
        });
    });

    if (window.addEventListener) {
        document.addEventListener('click', onClick);

        //document.addEventListener('mousewheel', updateNumber);
        //document.addEventListener('keydown', updateNumber);

        document.addEventListener('keydown', updateInvoice);
        document.addEventListener('keyup', updateInvoice);

    }
}

window.addEventListener && document.addEventListener('DOMContentLoaded', onContentLoad);