$(function(){
            //set base url variable
            var baseurl = $('#baseurl').val();
            var deleteBtn = $('.jbulk-del');
            $('#transaction_date').datepicker();

            //checkall
            $("#jcheckAll").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
                deleteBtn.toggleClass('hidden');
            });

            //Add another line of transaction
            var newLineOfEntry = function(){
                var row = '<tr>'+
                            '<td>'+
                            '<div class="form-group form-group-default ">'+
                            '<input type="text" class="form-control transaction_date" name="transaction_date[]" placeholder="Date" id="transaction_date" required>'+
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<div class="form-group form-group-default">'+
                            '<input type="text" class="form-control description" name="description[]" id="description" placeholder="Description">'+
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<div class="form-group form-group-default required">'+
                            '<input type="text" class="form-control amount" name="amount[]" placeholder="Amount" id="amount" required>'+
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<div class="form-group form-group-default requied">'+
                            '<select class="form-control gl_accounts debit_account" name="debit_account[]" data-placeholder="Select a Debit Account" required>'+
                            '<option value="">Select Account</option>'+
                            '</select>'+
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<div class="form-group form-group-default required">'+
                            '<select class="form-control gl_accounts credit_account" name="credit_account[]" data-placeholder="Select a Credit Account" required>'+
                            '<option value="">Select Account</option>'+
                            '</select>'+
                            '</div>'+
                            '</td>'+
                        '</tr>';
                        return row;
            }

            //submit details on click
            $(document).on('submit','#journalForm', function (e) {
                e.preventDefault();
                
                var $btn = $('#saveBtn');
                $btn.attr('disabled', true);
                $btn.html('Saving...');
                $.ajax({
                    type: 'POST',
                    url:  $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function (res) {
                        displayNotification(res.message);
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                    },error: function(res){
                        displayNotification(res.message);
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                    }
                });
            });
    
            $(document).on('click', '.view', function(){
                var trans_no = $(this).attr('data-value');

                $.ajax({
                    url: baseurl + '/transactions/view/'+trans_no,
                    type: 'GET',
                    success: function(res){
                        $('.view_date').html(res.trans_date);
                        $('.view-transaction').empty();
                        $('.view-transaction').append(res.tableRow);
                        $('.view_description').html(res.description);
                        $('#detailsModal').modal('show');
                    }
                });

            });

            //delete single journal entry
           $(document).on("click", ".delete-journal", function (event) {

               var ID = $(this).attr('data-value');
               var _token = $('#_token').val();
               if(confirm('All items associated with these transaction will be deleted. Are you sure you want to delete this item?')){
                   $.ajax({
                       type: 'POST',
                       url: baseurl + '/journal/delete/'+ID,
                       data: {'_token': _token },
                       context: this,
                       success: function (res) {
                           if(res.statusCode === 200)
                           {                        
                                $(this).fadeOut('slow', function () {
                                   displayNotification(res.message, 'success');
                                   $(this).closest("tr").remove();
                               });

                           }else{
                                displayNotification(res.message, 'error');
                           }
                       }
                   });
               }
           });

            //bulk delete journal entries
            deleteBtn.click(function(){
                var ids = [];
                var _token = "{{csrf_token()}}";
                $.each($("input[name='ids']:checked"), function() {
                    ids.push($(this).val());
                });
                if(confirm('Are you sure you want to delete '+ ids.length +' items?')){
                    $.ajax({
                        type: 'POST',
                        url: baseurl + '/journal/delete/',
                        data: {ids:ids, _token:_token},
                        context: this,
                        success: function (e) {
                            $("input[name='ids']:checked").each(function(){
                                $(this).fadeOut('slow', function () {
                                    $(this).closest("tr").remove();
                                });
                            });
                            $("input:checkbox").prop('checked', false);
                            deleteBtn.toggle();
                        },
                        error: function(e){
                            //console.log(e)
                        }
                    });

                };
            });
});