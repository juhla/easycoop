$(function(){
    //set base url variable
    var baseurl = $('#baseurl').val();
    var deleteBtn = $('.bulk-del');
    $('#transaction_date').datepicker();

    //validate form
    $('#journalEditForm').validate();
    

    //load general ledger accounts
    loadGLAccounts();

    // Add new entry to the form
    $('.add-new').on('click', function(e){
        e.preventDefault();
        $('.fieldset').append(newLineOfEntry());
        $('.transaction_date').datepicker();

        loadGLAccounts();
    });

    $(document).on('click', '.rem_entry', function(ev){
        ev.preventDefault();
        $(this).closest('section').empty().hide();
    });

    var compareDC = function(){
        var debit = 0;
        var credit = 0;

        $('input[name^=debit]').each(function(){
            debit += +$(this).val();
        });

        $('input[name^=credit]').each(function(){
            credit += +$(this).val();
        });

        if(debit === credit){
            return true;
        }else{
            displayNotification('Your journl is not balanced.', 'error');
            return false;
        }
    }

    //Add another line of transaction
    var newLineOfEntry = function(){
        var row = '<section>'+                                        
                    '<div class="col-md-4">'+
                        '<div class="form-group form-group-default requied">'+
                            '<label for="ledger">Ledger Account</label>'+
                            '<select class="form-control ledger" name="ledger[]" required>'+
                                '<option value=""></option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group form-group-default required">'+
                            '<label for="debit">Debit (Dr)</label>'+
                            '<input type="number" class="form-control debit" name="debit[]" value="0" required>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group form-group-default required">'+
                            '<label for="credit">Credit (Cr)</label>'+
                            '<input type="number" class="form-control credit" name="credit[]" value="0" required>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-1">'+
                        '<button class="btn btn-default btn-sm rem_entry">'+
                            '<i class="fa fa-minus"></i>'+
                        '</button>'+
                    '</div>'+
                    '<div class="clearfix"></div>'+
                    '<input type="hidden" name="entry_id[]" value="">'+
                '</section>';

                 //validate form
                $('#journalEditForm').validate();

                return row;
    }

    //submit details on click
    $(document).on('submit','#journalEditForm', function (e) {
        e.preventDefault();
        $('#journalEditForm').validate();

        if(compareDC())
        { 
            var $btn = $('#saveBtn');
            $btn.attr('disabled', true);
            $btn.html('Saving...');
            $.ajax({
                type: 'POST',
                url:  $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function (res) {
                    if(res.statusCode === 200){
                        displayNotification(res.message);
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                        window.location.replace(baseurl+'/journal');
                    }

                    if(res.statusCode(500)){
                        displayNotification(res.message);
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                    }
                },
                error: function(res){
                    displayNotification(res.message);
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                }
            });
        }else{
            
        }
    });
});