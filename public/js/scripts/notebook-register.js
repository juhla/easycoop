$(function () {
    //set base url variable
    var baseurl = $('#baseurl').val();
    var checkboxes = $("input[type='checkbox']"),
        deleteBtn = $(".delete-selected"),
        postBtn = $('.post-entries');
    //checkall
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
        deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        postBtn.attr("disabled", !checkboxes.is(":checked"));
    });


    //enable/disable delete button
    $(document).on("click", checkboxes, function () {
        deleteBtn.attr("disabled", !checkboxes.is(":checked"));
        postBtn.attr("disabled", !checkboxes.is(":checked"));
    });

    //post entries to gl
    postBtn.on('click', function () {
        if (!checkboxes.is(':checked')) {
            alert('Please select an entry to post to GL');
        } else {
            $('#post-register').submit();
        }
    });

    var transactionType = $("#transaction_type");
    var transactionMode = $("#transaction_mode");
    transactionMode.change(function () {
        getRegisterMode(transactionType.val(), transactionMode.val());
    });
    transactionType.change(function () {
        getRegisterMode(transactionType.val(), transactionMode.val());
    });
    var hideAll = function () {
        $('#cashReceipt').slideUp();
        $('#bankReceipt').slideUp();
        $('#creditSales').slideUp();
        $('#cashPayment').slideUp();
        $('#bankPayment').slideUp();
        $('#creditPurchase').slideUp();
    };

    var getRegisterMode = function (transactionType, transactionMode, calledBy) {
        switch (true) {
            case ( (transactionType == 1) && (transactionMode == 1)) : {
                hideAll();
                $('#cashReceipt').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnCashR').html('Update');
                }

            }
                break;

            case ( (transactionType == 1) && (transactionMode == 2)) : {
                hideAll();
                $('#bankReceipt').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnChqR').html('Update');
                }

            }
                break;

            case ( (transactionType == 1) && (transactionMode == 3)) : {
                hideAll();
                $('#creditSales').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnCS').html('Update');
                }
            }
                break;


            case ( (transactionType == 2) && (transactionMode == 1)) : {
                hideAll();
                $('#cashPayment').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnCPay').html('Update');
                }

            }
                break;

            case ( (transactionType == 2) && (transactionMode == 2)) : {
                hideAll();
                $('#bankPayment').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnChqP').html('Update');
                }
            }
                break;

            case ( (transactionType == 2) && (transactionMode == 3)) : {
                hideAll();
                $('#creditPurchase').slideDown();
                if(calledBy=="edit"){
                    $('#saveBtnCP').html('Update');
                }
            }
                break;

        }
    };
    hideAll();
    getRegisterMode(transactionType.val(), transactionMode.val());


    //display form modal
    $('.add-new').click(function () {
        $('#displayForm').slideDown();
    });
    //hide and empty form
    $('.close-modal').click(function () {
        $('#notebookRegisterForm')[0].reset();
        $('#displayForm').slideUp();
        $('#item_id').val('');
    });


    $(document).on('submit', '#notebookRegisterForm', function (e) {
        e.preventDefault();
        var transaction_id = $('#item_id').val();
        var row_index = $('#row_index').val();
        var $btn = $('#saveBtn');
        $btn.attr('disabled', true);
        $btn.html('Saving...');


        var data = $(this).serializeArray(); // convert form to array
        data.push({name: "item_id", value: transaction_id});
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $.param(data),
            dataType: 'json',
            success: function (res) {

                if (transaction_id !== '') {
                    $('#notebookRegisterForm').closest('form').find("input[type=text], textarea").val("");
                    $('#notebookRegisterForm')[0].reset();
                    $('#entries > tr').eq(row_index).html(res);
                    $('#item_id').val('');
                    displayNotification('Entry has been updated', 'Success', 'success');
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                    window.location.href = baseurl + '/notebook-v2';

                } else {
                    $('.no-income').hide();
                    $('#notebookRegisterForm').closest('form').find("input[type=text], textarea").val("");
                    $('#notebookRegisterForm')[0].reset();
                    displayNotification('Entry has been saved', 'Success', 'success');
                    $('#entries').prepend(res);
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                    window.location.href = baseurl + '/notebook-v2';
                }

            }, error: function (res) {
                displayNotification('An unknown error occurred', 'Error', 'error');
            }
        });
    });
    //edit entry
    $(document).on('click', '.edit', function (e) {
        $('#loaderDIV').show();
        var id = $(this).attr('data-value');
        var url = $(this).attr('data-url-edit');
        var trans_type = $(this).attr('data-trans-type');
        var trans_mode = $(this).attr('data-trans-mode');
        var row_index = $(this).closest('tr').index();
        $('#row_index').val(row_index);
        $.ajax({
            type: 'GET',
            url: baseurl + url + id,
            dataType: 'json',
            success: function (res) {
                for (var key in res) {
                    $("input[name=" + key + "]").val(res[key]);
                    if (key == 'mode_of_payment') {
                        $('#mode_of_payment').select2('val', res[key]);
                    }
                }
                //$('#customer').select2('val', res['customer']);
                $('#saveBtn').html('Update');
                $('#item_id').val(id);
                getRegisterMode(trans_type, trans_mode, 'edit');
                $('#loaderDIV').hide();
                $("html, body").animate({scrollTop: $('#loaderDIV').offset().top}, 1000);
                //$(jQuery.browser.webkit ? "body": "html").animate({ scrollTop: $('#loaderDIV').offset().top }, 1000);
                // $(window).scrollTo('#loaderDIV', 500, {
                //     offset: {
                //         top: 108,
                //         left: 0
                //     }
                // });
                //$(window).scrollTo('#section2');
            }
        });
    });
    //delete inflow entry
    $(document).on("click", ".delete", function (event) {

        var ID = $(this).attr('data-value');
        var url = $(this).attr('data-url-delete');
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the data once deleted!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'GET',
                    url: baseurl + url + ID,
                    context: this,
                    success: function () {
                        swal({
                            title: "Deleted!",
                            text: "item was deleted successfully",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $this.fadeOut('slow', function () {
                                    $this.closest("tr").remove();
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    //bulk delete
    deleteBtn.click(function () {
        var ids = [];
        var _token = "{{csrf_token()}}";
        $.each($("input[name='ids']:checked"), function () {
            ids.push($(this).val());
        });
        if (confirm('Are you sure you want to delete ' + ids.length + ' items?')) {
            $('body').loader('show');
            $.ajax({
                type: 'POST',
                url: baseurl + '/app/notebook/non-cash-register/delete-selected-cs',
                data: {ids: ids, _token: _token},
                context: this,
                success: function (e) {
                    $("input[name='ids']:checked").each(function () {
                        $(this).fadeOut('slow', function () {
                            $(this).closest("tr").remove();
                        });
                    });
                    $("input:checkbox").prop('checked', false);
                    $('body').loader('hide');
                },
                error: function (e) {
                    //console.log(e)
                }
            });

        }
        ;
    });
});
