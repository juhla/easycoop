$(function(){
	
	var url = $('#baseurl').val();

	// View notification process
	$(document).on('click', '.item', function(ev){
	    ev.preventDefault();
	    var id = $(this).data('code');
	    var item = $(this);

	    $.ajax({
	        url: url + '/seen',
	        type: 'POST',
	        data: {_token: $('#_token').val(), id: id},
	        success: function(res){
	            item.empty().remove();
	        },
	        error: function(xhr, type, error){
	            
	        }
	    });
	});

	// $(document).on('click', '.badge-notify', function(){
	// 	$('.dropdown-menu').toggle();
	// })
});