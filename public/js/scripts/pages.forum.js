(function($) {
    'use strict';

    // Wysiwyg editor custom options

    var editorTemplate = {
        "font-styles": function(locale) {
            return '<li class="dropdown dropup">' + '<a data-toggle="dropdown" class="btn btn-default dropdown-toggle ">    <span class="glyphicon glyphicon-font"></span>    <span class="current-font">Normal text</span>    <b class="caret"></b>  </a>' + '<ul class="dropdown-menu">    <li><a tabindex="-1" data-wysihtml5-command-value="p" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Normal text</a></li>     <li><a tabindex="-1" data-wysihtml5-command-value="h1" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 1</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h2" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 2</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h3" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 3</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h4" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 4</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h5" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 5</a></li>    <li><a tabindex="-1" data-wysihtml5-command-value="h6" data-wysihtml5-command="formatBlock" href="javascript:;" unselectable="on">Heading 6</a></li>  </ul>' + '</li>';
        },
        emphasis: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="CTRL+B" data-wysihtml5-command="bold" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-bold"></i></a>' + '<a tabindex="-1" title="CTRL+I" data-wysihtml5-command="italic" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-italic"></i></a>' + '<a tabindex="-1" title="CTRL+U" data-wysihtml5-command="underline" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-underline"></i></a>' + '</div>' + '</li>';
        },
        blockquote: function(locale) {
            return '<li>' + '<a tabindex="-1" data-wysihtml5-display-format-name="false" data-wysihtml5-command-value="blockquote" data-wysihtml5-command="formatBlock" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-quote"></i>' + '</a>' + '</li>'
        },
        lists: function(locale) {
            return '<li>' + '<div class="btn-group">' + '<a tabindex="-1" title="Unordered list" data-wysihtml5-command="insertUnorderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ul"></i></a>' + '<a tabindex="-1" title="Ordered list" data-wysihtml5-command="insertOrderedList" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-ol"></i></a>' + '<a tabindex="-1" title="Outdent" data-wysihtml5-command="Outdent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-outdent"></i></a>' + '<a tabindex="-1" title="Indent" data-wysihtml5-command="Indent" class="btn  btn-default" href="javascript:;" unselectable="on"><i class="editor-icon editor-icon-indent"></i></a>' + '</div>' + '</li>'
        },
        image: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-image-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert image</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-image-url form-control" value="http://">' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary">Insert image</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert image" data-wysihtml5-command="insertImage" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-image"></i>' + '</a>' + '</li>'
        },
        link: function(locale) {
            return '<li>' + '<div class="bootstrap-wysihtml5-insert-link-modal modal fade">' + '<div class="modal-dialog ">' + '<div class="modal-content">' + '<div class="modal-header">' + '<a data-dismiss="modal" class="close">×</a>' + '<h3>Insert link</h3>' + '</div>' + '<div class="modal-body">' + '<input class="bootstrap-wysihtml5-insert-link-url form-control" value="http://">' + '<div class="checkbox check-success"> <input type="checkbox" class="bootstrap-wysihtml5-insert-link-target" checked="checked" value="1" id="link-checkbox"> <label for="link-checkbox">Open link in new window</label></div>' + '</div>' + '<div class="modal-footer">' + '<a data-dismiss="modal" class="btn btn-default">Cancel</a>' + '<a data-dismiss="modal" class="btn btn-primary" href="#">Insert link</a>' + '</div>' + '</div>' + '</div>' + '</div>' + '<a tabindex="-1" title="Insert link" data-wysihtml5-command="createLink" class="btn  btn-default" href="javascript:;" unselectable="on">' + '<i class="editor-icon editor-icon-link"></i>' + '</a>' + '</li>'
        }
    }

    var editorOptions = {
        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
        "emphasis": true, //Italics, bold, etc. Default true
        "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
        "html": false, //Button which allows you to edit the generated HTML. Default false
        "link": true, //Button to insert a link. Default true
        "image": true, //Button to insert an image. Default true,
        "color": false, //Button to change color of font  
        "blockquote": true, //Blockquote  
        customTemplates: editorTemplate
    };


    $('#mark-forum').click(function() {
        $('.item .checkbox').toggle();
    });

    var cache = { };
    var lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

    // Load list of forums
    $('.forum-list').length && $.ajax({
        dataType: "json",
        url: "http://localhost:8000/app/town-hall/forum/api/threads",
        success: function(data) {
            var listViewGroupCont = $('<div/>', {
                "class": "list-view-group-container"
            });
            var ul = $('<ul/>', {
                "class": "no-padding"
            });

            $.each(data.data, function(i) {
                var obj = data.data[i];
                var id = obj.id;
                var dp = obj.dp;
                var subject = obj.title;
                // var body = obj.body.replace(/<(?:.|\n)*?>/gm, '');
                var body = lorem;
                var time = obj.created_at;

                var li = '<li class="item padding-15" data-forum-id="' + id + '"> \
                            <div class="inline m-l-15"> \
                                <p class="subject no-margin">' + subject + '</p> \
                                <p class="body no-margin"> \
                                 ' + body + ' \
                                </p> \
                            </div> \
                            <div class="datetime">' + time + '</div> \
                            <div class="clearfix"></div> \
                        </li>';
                ul.append(li);
            });
            listViewGroupCont.append(ul);
            $('#forumList').append(listViewGroupCont);
            $("#forumList").ioslist();
        }
    });

    $('body').on('click', '.item .checkbox', function(e) {
        e.stopPropagation();
    });


    $('body').on('click', '.item', function(e) {
        e.stopPropagation();

        var postsTable = $('<table/>', {
            "class": "table table-hover",
            "id": "tableWithSearch"
        });
        postsTable.html("<tr><th>Author</th><th>Content</th><th colspan='2'>Last Update</th></tr>")
        var tbody = $('<tbody/>', {
            "class": "no-padding"
        });


        var id = $(this).attr('data-forum-id');
        var forum = null;
        // if id is in cache use cache value else go to api!!!
        console.log(cache);

        $.ajax({
            dataType: "json",
            url: "http://localhost:8000/app/town-hall/forum/api/thread/" + id,
            success: function(data) {
                forum = data.thread;

                console.log(forum);

                cache[id] = forum;
                var forumOpened = $('.forum-opened');

                forumOpened.find('.sender .name').text(forum.from);
                forumOpened.find('.sender .datetime').text(forum.updated_at);
                forumOpened.find('.subject').text(forum.title);
                forumOpened.find('.forum-content-body').html( lorem );

                $.each(forum.posts, function(i) {
                    var tr = '<tr><td class="v-align-middle semi-bold">' + forum.posts[i].author_id + '</td>\
                        <td class="v-align-middle thread-post-content">' + forum.posts[i].content  + '</td>\
                        <td class="v-align-middle">' + forum.posts[i].updated_at + '</td></tr>';
                    tbody.append(tr);
                });

                postsTable.append(tbody);
                forumOpened.find('.forum-content-body').append( postsTable );
    

                $('.no-forum').hide();
                $('.actions-dropdown').toggle();
                $('.actions, .forum-content-wrapper').show();
                if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
                    $('.forum-list').toggleClass('slideLeft');
                }
                $(".wysiwyg5-wrapper").html(data.html);

                !$('.forum-reply').data('wysihtml5') && $('.forum-reply').wysihtml5(editorOptions);


                $(".forum-content-wrapper").scrollTop(0);

                // Initialize forum action menu 
                $('.menuclipper').menuclipper({
                    bufferWidth: 20
                });
            }
        });

        $('.item').removeClass('active');
        $(this).addClass('active');

    });


    // Toggle forum sidebar on mobile view
    $('.toggle-forum-sidebar').click(function(e) {
        e.stopPropagation();
        $('.forum-sidebar').toggle();
    });

    $('.forum-list-toggle').click(function() {
        $('.forum-list').toggleClass('slideLeft');
    });

    $('.forum-sidebar').click(function(e) {
        e.stopPropagation();
    })

    $(window).resize(function() {

        if ($(window).width() <= 1024) {
            $('.forum-sidebar').hide();

        } else {
            $('.forum-list').length && $('.forum-list').removeClass('slideLeft');
            $('.forum-sidebar').show();
        }
    });



})(window.jQuery);