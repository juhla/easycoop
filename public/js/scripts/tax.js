(function($){

    /***** SOME GLOBAL VARIABLES****/
    var baseurl = $("#baseurl").val();
    var gl_pur = $("#purchase_gl_code");
    var gl_sales = $("#sales_gl_code");
    var container = $("#taxBody");
    var save_btn = $("#saveBtn");
    var clear_btn = $("#clear");
    var header = $("#title");
    var tax_form = $("#taxForm");
    var taxContainer = $("#taxContainer");

    //display tax form modal
    $('.add-new').on('click', function(){
        $('#taxFormModal').modal('show');
    });

    $('.close-modal').click(function(){
        $('#taxForm')[0].reset();
        $('#taxFormModal').modal('hide');
    });

    /**
     * Save Tax Function
     */
    function saveTax(url, data)
    {
        $.ajax(url, {
            type    :   'POST',
            cache   :   false,
            context :   this,
            data    :   data,
            success :   function(e){
                save_btn.removeAttr('disabled');
                save_btn.text('Save');
                displayNotification(e.message, 'Success',  'success');
                clearForm();
                loadTaxes();
            }
        });
    }



    /**
     * This loads all the accounts in the GL fields
     */
    function loadGLAccounts()
    {
        var glp = "";
        var gls = "";
        $.ajax(baseurl+"/api-v1/get-gl-accounts-for-select", {
            type    :   'GET',
            success :   function(data){
                gl_pur.append(data);
                gl_sales.append(data);
                $('.select2-option').select2();
                //gl_pur.select2('updateResults');
                //gl_sales.select2('updateResults');
            }
        });
    }

    /**
     *
     */
    function loadTaxes()
    {
        var tax = "";

        $.ajax(baseurl+"/setup/get-taxes", {
            type    :   'GET',
            success :   function(data){
                if(data.status === 200){
                    container.html("");
                    container.append(data.taxes);
                    //paginate();
                }else{
                    container.html("<tr><td colspan='5'> You have not created any tax yet.</td></tr>");
                }
            },error: function(err){
                console.log(err)
            }
        });
    }

    function populateForm( id )
    {
        var url = baseurl+"/setup/get-tax/"+id;

        $.ajax(url, {
            type    :   'GET',
            success :   function(data){
                if(data.status === 200)
                {
                    for (var key in data.tax) {
                        $("input[name=" + key + "]").val(data.tax[key]);
                    }
                    $('#sales_gl_code').select2('val', data.tax.sales_gl_code);
                    $('#purchase_gl_code').select2('val', data.tax.purchase_gl_code);
                    $('#tax_id').val(data.tax.id);
                    save_btn.text('Update');
                    $('#taxFormModal').modal('show');
                }else{
                    displayNotification(e.message, 'Error', 'error');
                }
            }
        });
    }

    /****For clearing form fields*****/
    function clearForm()
    {
        save_btn.text('Save').data('action', 'save');
        $('#name').val("");
        $('#abbreviation').val("");
        $('#rate').val('');
        $('#sales_gl_code').select2('val', '');
        $("#purchase_gl_code").select2('val', '');
        header.text("New");
    }

    $(document).ready(function(){

        /**
         * Load resources
         */
        loadGLAccounts();
        loadTaxes();

        /***Actions*****/
        $(document).on('submit', tax_form, function(e){
            e.preventDefault();

            if($('#tax_id').val() === ''){
                save_btn.attr('disabled', 'true').text('wait...');
                var data = tax_form.serialize();
                var url = baseurl+"/setup/add-tax";
                saveTax(url, data);
            }else{
                save_btn.attr('disabled', 'true').text('wait...');
                var data = tax_form.serialize();
                var url = baseurl+"/setup/update-tax";
                saveTax(url, data);
            }
        });

        $(document).on('click', 'button', function(e){
            if($(this).data('action') === 'edit'){
                header.text('Update');
                populateForm($(this).data('value'));
            }

            if($(this).data('action') === 'delete'){
                var row = $(this).closest('tr');
                var tax_no = $(this).data('value');

                swal({
                    title:"Are you sure?",
                    text:"You will not be able to recover the data once deleted!",
                    type:"warning",
                    showCancelButton:!0,
                    confirmButtonColor:"#DD6B55",
                    confirmButtonText:"Yes, delete it!",
                    closeOnConfirm:!1
                },function(isConfirm){
                    if(isConfirm){
                        $.ajax({
                            type: 'GET',
                            url: baseurl+"/setup/delete-tax/" + tax_no,
                            context: this,
                            success: function (e) {
                                if( e.status === 200 )
                                {
                                    displayNotification(e.message, 'Success!', 'success');
                                    row.empty();
                                } else {
                                    displayNotification(e.message, 'Error!', 'error');
                                }
                            }
                        });
                    }
                });
            }
        });

        /***Clear form action***/
        $(document).on('click', '#clear', function(){ clearForm(); });
    });
})(jQuery);