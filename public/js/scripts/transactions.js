$(function () {
    //set base url variable
    var baseurl = $('#baseurl').val();
    //checkall
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    //load general ledger accounts
    loadGLAccounts();

    var checkboxes = $("input[type='checkbox']"),
        deleteBtn = $(".bulk");
    //enable/disable delete button
    $(document).on("click", checkboxes, function () {
        deleteBtn.attr("disabled", !checkboxes.is(":checked"));
    });
    //display form modal
    $('.add-new').click(function () {
        $('#transactionForm')[0].reset();
        $('#transaction_type').val($(this).data('type'));
        $('#transacition_id').val('')
        $('#displayForm').slideDown();
    });
    //hide and empty form
    $('.close-modal').click(function () {
        $('#transactionForm')[0].reset();
        $('#debit_accuont').select2('val', '');
        $('#bank_id').select2('val', '');
        $('#credit_accuont').select2('val', '');
        $('#displayForm').slideUp();
        $('#transacition_id').val('');
    });
    //submit details on click
    $(document).on('submit', '#transactionForm', function (e) {
        e.preventDefault();

        var transaction_id = $('#transaction_id').val();
        var row_index = $('#row_index').val();
        var $btn = $('#saveBtn');
        $btn.attr('disabled', true);
        $btn.html('Saving...');
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function (res) {
                //console.log(res);
                if (res.result === 'no') {
                    $('.error-message').html(res.message);
                    $('#date-error').show();
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                } else if (res.result === 'no-f-year') {
                    $('.error-message').html(res.message);
                    $('#date-error').show();
                    $btn.attr('disabled', false);
                    $btn.html('Save');
                } else {
                    if (transaction_id !== '') {
                        $('#displayForm').slideUp('hide');
                        $('#transactionForm')[0].reset();
                        $('#transaction_id').val(res.id);
                        $('#row_index').val(row_index);
                        $('#transactionTable > tr').eq(row_index).html(res);
                        displayNotification('Transaction has been updated', 'Success', 'success');
                        $('#date-error').hide();
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                    } else {
                        $('.no-income').hide();
                        $('#transactionForm')[0].reset();
                        $('#debit_account').select2('val', '');
                        $('#payment_type').select2('val', '');
                        $('#credit_account').select2('val', '');
                        $('#transaction_id').val(res.id);
                        $('#row_index').val(row_index);
                        $('#date-error').hide();
                        displayNotification('Transaction has been saved', 'Success', 'success');
                        $('#transactionTable').prepend(res);
                        $btn.attr('disabled', false);
                        $btn.html('Save');
                    }
                    setTimeout(function () {
                        location.reload()
                    }, 300)
                }
            }, error: function (res) {
                displayNotification('An unknown error occurred', 'Error!', 'error');
                $btn.attr('disabled', false);
                $btn.html('Save');
            }
        });
    });
    //edit entry
    $(document).on('click', '.edit', function (e) {
        var id = $(this).attr('data-value');
        var row_index = $(this).closest('tr').index();

        $.ajax({
            type: 'GET',
            url: baseurl + '/transactions/get-transaction/' + id,
            dataType: 'json',
            success: function (res) {
                $('#pv_receipt_no').val(res.pv_receipt_no);
                $('#trans_date').val(res.transaction_date);
                $('#payment_type').select2('val', res.payment_type);
                if (res.payment_type === 2) {
                    $('#showBank').removeClass('hidden');
                    $('#bank_id').select2('val', res.bank_id);
                    $('#cheque_number').val(res.cheque_number);
                }
                $('#customer_vendor').val(res.customer_vendor);
                $('#description').val(res.description);
                $('#amount').val(res.amount);
                $('#debit_account').select2('val', res.debit_account);
                $('#credit_account').select2('val', res.credit_account);

                $('#transaction_id').val(res.id);
                $('#row_index').val(row_index);

                $('#displayForm').slideDown();
            }
        });
    });
    //delete inflow entry
    $(document).on("click", ".delete-transaction", function (event) {

        var ID = $(this).attr('data-value');
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the data once deleted!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: !1
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: 'GET',
                    url: baseurl + '/transactions/delete/' + ID,
                    context: this,
                    success: function () {
                        swal({
                            title: "Deleted!",
                            text: "item was deleted successfully",
                            confirmButtonColor: "#66BB6A",
                            type: "success"
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $this.fadeOut('slow', function () {
                                    $this.closest("tr").remove();
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    //bulk delete
    deleteBtn.click(function () {
        var ids = [];
        var _token = "{{csrf_token()}}";
        $.each($("input[name='ids']:checked"), function () {
            ids.push($(this).val());
        });
        if (confirm('Are you sure you want to delete ' + ids.length + ' items?')) {
            $('body').loader('show');
            $.ajax({
                type: 'POST',
                url: baseurl + '/notebook/delete',
                data: {ids: ids, _token: _token},
                context: this,
                success: function (e) {
                    $("input[name='ids']:checked").each(function () {
                        $(this).fadeOut('slow', function () {
                            $(this).closest("tr").remove();
                        });
                    });
                    $("input:checkbox").prop('checked', false);
                    $('body').loader('hide');
                },
                error: function (e) {
                    //console.log(e)
                }
            });

        }
        ;
    });

    $(document).on('click', '.view', function () {
        var trans_no = $(this).attr('data-value');

        $.ajax({
            url: baseurl + '/transactions/view/' + trans_no,
            type: 'GET',
            success: function (res) {
                $('.view_date').html(res.trans_date);
                $('.view-transaction').empty();
                $('.view-transaction').append(res.tableRow);
                $('.view_description').html(res.description);
                $('#detailsModal').modal('show');
            }
        });

    });

    $(document).on('change', '#payment_type', function (e) {
        if ($('#payment_type').val() === '1') {
            $('#showBank').hide();
        } else {
            $('#showBank').show();
        }
    });

});
