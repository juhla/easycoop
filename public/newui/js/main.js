// preloader starts here
// $(window).load(function() {
//     var preloaderDelay = 350,
//         preloaderFadeOutTime = 800;

//     function hidePreloader() {
//         var loadingAnimation = $('#loading-animation'),
//             preloader = $('#preloader');
//         loadingAnimation.fadeOut();
//         preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime);
//     }

//     hidePreloader();
// });

/* ---------------------------------------------
       page  Prealoader
   --------------------------------------------- */
// $(document).ready(function($) {

$(window).on("load", function() {
    $("#loading-center-page").fadeOut();
    $("#loading-page")
        .delay(100)
        .fadeOut("slow");
    document.getElementById("loading-page").style.display = "none"
});